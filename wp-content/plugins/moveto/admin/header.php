<?php

session_start();
global $current_user;
$moveto_sampledata =get_option('moveto_sample_dataids');
$mp_currency_symbol = get_option('moveto_currency_symbol');
	$user_sp = '';
	$user_sp_manager = '';
	$current_user = wp_get_current_user();
	$info = get_userdata( $current_user->ID );

	if(current_user_can('mp_staff') && !current_user_can('manage_options')) {
	  $user_sp = 'Y';
	}if(current_user_can('mp_manager') && !current_user_can('manage_options')) {
	  $user_sp_manager = 'Y';
	}
	if ( class_exists( 'WooCommerce' ) && current_user_can('mp_staff') ) {
		$user_sp = 'Y';	
	}
	if ( class_exists( 'WooCommerce' ) && current_user_can('mp_manager') ) {
		$user_sp_manager = 'Y';				
	}
	$plugin_url_for_ajax = plugins_url('',  dirname((__FILE__)));

	
	$_SESSION['mp_location'] = 0;	
/* room_type_title Validation Messages */
	$room_type_title = __('Please Enter Room-Type Name','mp');
	$article_category_title= __('Please Enter Article Category Name','mp');
	
	/* movesize Validation Messages */
	$movesize_title = __('Please Enter Move Size Name','mp');
	
	/* Article Validation Messages */
	$article_msg = __('Please Enter Article Name','mp');
	$article_price_msg = __('Please Enter Distance Price','mp');
	$article_number_price_msg  = __('Please Enter Distance In Digits Only','mp');
/* Distance,Floors,Loading/unloading,Discount %age, Validation Messages */
	$distance_msg = __('Please Enter Distance','mp');			
	$distance_number_msg = __('Please Enter Distance In Digits Only','mp');			
	$distance_price_msg = __('Please Enter Distance Price','mp');
	$distance_number_price_msg  = __('Please Enter Distance In Digits Only','mp');		
	$floor_price_msg = __('Please Enter Price Of Floor','mp');			
	$floor_number_price_msg  = __('Please Enter Floor In Digits Only','mp');	
	$floor_no_msg = __('Please Enter Floor Number','mp');			
	$floor_no_valid_msg  = __('Please Enter Floor Number In Digits Only','mp');	
	
	$elevator_discount_msg = __('Please Enter Discount','mp');			
	$elevator_number_price_msg  = __('Please Enter Discount In Digits Only','mp');	

	$packing_msg = __('Please Enter Cost Of Packing','mp');			
	$packing_number_msg = __('Please Enter Cost Of Packing In Digits Only','mp');			
	$unpacking_price_msg = __('Please Enter Cost Of Unpacking','mp');
	$unpacking_number_price_msg  = __('Please Enter Cost Of Unpacking In Digits Only','mp');		
	
	

/* Service Validation Messages */				
	$categorytitle_err_msg = __('Please Enter Category Title','mp');			
	$servicetitle_err_msg = __('Please Enter Service Title','mp');			
	$servicedescription_err_msg = __('Please Enter Service Description','mp');			
	$serviceprice_err_msg = __('Please Enter Service Price','mp');			
	$servicepricedigit_err_msg = __('Please Enter Price In Digits','mp');		
	$serviceofferpricegreater_err_msg = __('Offered Price Should Be Less Then Default Price','mp');	
	$servicecategory_err_msg = __('Please Select Service Category','mp');		
	$servicehrsrange_err_msg = __('Please Enter Minimum 1 Hours Maximum 23 Hours','mp');		
	$servicemins_err_msg = __('Please Enter Value Minimum 5 Minutes','mp');		
	$serviceminsrange_err_msg = __('Please Enter Minimum 5 Mintues Maximum 59 Mintues','mp');	
	$servicenumpatt_err_msg = __('Please Enter Value In Digits Only','mp');	
	
	/* Service Addon Validation Messages */					
	$serviceaddontitle_err_msg = __('Please Enter Article Name','mp');				
	$serviceaddonmaxqty_err_msg = __('Please Enter Valid Max Addon Quantity','mp');			
	$serviceaddon_price_err_msg = __('Please Enter Article Unit Price','mp');	
	$serviceaddon_validprice_err_msg = __('Please Enter Valid Article Price','mp');	
	$serviceaddon_qty_err_msg = __('Please Enter Article Pricing Quantity','mp');	
	$serviceaddon_validqty_err_msg = __('Please Enter Valid Article Pricing Quantity','mp');	

/* City Validation Messages */	
	$source_city_err_msg = __('Please Enter Source City','mp');			
	$destination_city_err_msg = __('Please Enter Destination City','mp');			
	$city_price_err_msg = __('Please Enter Route Price','mp');			
	$city_price_validprice_err_msg = __('Please Enter Valid Route Price','mp');	
	$additional_info_err_msg = __('Please Enter Additional Info Title','mp');			
	$additional_info_unit_price_err_msg = __('Please Enter Price','mp');			
	$additional_info_unit_price_digit_err_msg = __('Please Enter Price In Digits Only','mp');			

/* Send Quote Validation Messages */	
	$quoteprice_err_msg = __('Please Enter Quote Price','mp');			
	$quotepricevalid_err_msg = __('Please enter valid quote price','mp');				
			
	
/* Staff Validation Messages */	
	$staffusername_err_msg = __('Please Enter Username','mp');	
	$staffusernameexist_err_msg = __('Username Exist Or Not Valid','mp');	
	$staffpassword_err_msg = __('Please Enter Password','mp');	
	$staffemail_err_msg = __('Please Enter Email','mp');	
	$staffemailexist_err_msg = __('Email Exist Or Not Valid','mp');	
	$stafffullname_err_msg = __('Please Enter Fullname','mp');	
	$staffselect_err_msg = __('Please Select Existing User','mp');	
	$staffvalidphone_err_msg = __('Please Enter Valid Phone Number.','mp');	
/* Coupon Validation Messages */
	$cuponcode_err_msg = __('Please Enter Promocode','mp');	
	$cuponvalue_err_msg = __('Please Enter Promocode Value','mp');	
	$cuponvalueinvalid_err_msg = __('Please Enter Valid Promocode Value','mp');	
	$cuponlimit_err_msg = __('Please Enter Promocode Limit','mp');	
	$cuponlimitinvalid_err_msg = __('Please Enter Valid Promocode Limit','mp');	
/* SMS Notifications Validation Messages */
	$twilliosid_err_msg = __('Please Enter Twillio Account SID','mp');	
	$twillioauthtoken_err_msg = __('Please Enter Twillio Auth Token','mp');	
	$twilliosendernum_err_msg = __('Please Enter Twillio Sender Number','mp');	
	$twillioadminnum_err_msg = __('Please Enter Twillio Admin Account Number','mp');	
	
	$plivosid_err_msg = __('Please Enter Plivo Account SID','mp');	
	$plivoauthtoken_err_msg = __('Please Enter Plivo Auth Token','mp');	
	$plivosendernum_err_msg = __('Please Enter Plivo Sender Number','mp');	
	$plivoadminnum_err_msg = __('Please Enter Plivo Admin Account Number','mp');
	
	$nexmoapi_err_msg = __('Please Enter Nexmo API','mp');	
	$nexmoapisecert_err_msg = __('Please Enter Nexmo API Secert','mp');	
	$nexmofromnum_err_msg = __('Please Enter Nexmo From Number','mp');	
	$nexmoadminnum_err_msg = __('Please Enter Nexmo Admin Account Number','mp');
			
	
/* Object Content For Quote Calender */
$language = get_locale();
$ak_wplang = explode('_',$language);
$wpTimeFormatorg = get_option('time_format'); 
$arr = str_split($wpTimeFormatorg);
$slashcounter = 0;
$wpTimeFormat='';
	foreach($arr as $singlechar){
		if($singlechar=='\\'){
			$slashcounter=1;
			$wpTimeFormat .="[";
			continue;
		}elseif($slashcounter!=1 && ($singlechar=='g' || $singlechar=='G' || $singlechar=='i')){
			if($singlechar=='g'){ $wpTimeFormat .='h'; }
			if($singlechar=='G'){ $wpTimeFormat .='H'; }
			if($singlechar=='i'){ $wpTimeFormat .='mm'; }
		}elseif($slashcounter==1){
			$wpTimeFormat .=$singlechar."]";
			$slashcounter=0;
		}else{
			$wpTimeFormat .=$singlechar;  
		}
   } 	
	
/* Existing Custom Form Fields */ 
$moveto_custom_formfields = json_decode(stripslashes(get_option('moveto_custom_form')),true); 
$moveto_custom_formfields_val = '';
$totallength = 0;
if(isset($moveto_custom_formfields)){
$totallength = sizeof((array)$moveto_custom_formfields);
}
if($totallength>0){
	$lengthcounter = 1;
	foreach($moveto_custom_formfields as $moveto_custom_formfield){
		if($totallength==$lengthcounter){
			$moveto_custom_formfields_val .= json_encode($moveto_custom_formfield);
		}else{
			$moveto_custom_formfields_val .= json_encode($moveto_custom_formfield).',';
		}
		$lengthcounter++;
	} 
}	

function wp_date_format() {
	$dateFormat = get_option('date_format');
	
	$chars = array(
		// Day
		'd' => 'DD',
		'j' => 'DD',
		// Month
		'm' => 'MM',
		'F' => 'MMMM',
		// Year
		'Y' => 'YYYY',
		'y' => 'YYYY',
	);
	return strtr( (string) $dateFormat, $chars );
}

function wp_time_format() {
	$timeFormat = get_option('time_format');
	
	$chars = array(
		// Day
		'g' => 'hh',
		'H' => 'h',
		// Month
		'a' => 'a',
		'A' => 'A',
		// Year
		'i' => 'mm',
	);
	return strtr( (string) $timeFormat, $chars );
}

$Today = __('Today',"bd");
$Yesterday = __('Yesterday',"bd");
$Last_7_Days = __('Last 7 Days',"bd");
$Last_30_Days = __('Last 30 Days',"bd");
$This_Month = __('This Month',"bd");
$Last_Month = __('Last Month',"bd");

$Apply = __('Apply',"bd");
$Cancel = __('Cancel',"bd");
$From = __('From',"bd");
$To = __('To',"bd");
$Custom_range = __('Custom Range',"bd");

$dateformatsss= wp_date_format();
$timeformatsss= wp_time_format();
$date_time_format = wp_date_format()." ".wp_time_format();
?>	

<script>
var date_format_for_js = '<?php  echo $dateformatsss; ?>';
var time_format_for_js = '<?php  echo $timeformatsss; ?>';
var date_time_format_for_js = '<?php  echo $date_time_format; ?>';

var labels_for_daterange_picker = {
        'Today': '<?php echo $Today; ?>',
        'Yesterday': '<?php echo $Yesterday; ?>',
        'Last_7_Days': '<?php echo $Last_7_Days; ?>',
        'Last_30_Days': '<?php echo $Last_30_Days; ?>',
        'This_Month': '<?php echo $This_Month; ?>',
        'Last_Month': '<?php echo $Last_Month; ?>',
        'applyLabel': '<?php echo $Apply; ?>',
        'cancelLabel': '<?php echo $Cancel; ?>',
        'fromLabel': '<?php echo $From; ?>',
        'toLabel': '<?php echo $To; ?>',
        'customRangeLabel': '<?php echo $Custom_range; ?>'
    };

var header_object ={'plugin_path':'<?php echo $plugin_url_for_ajax; ?>','site_url':'<?php echo site_url();?>','defaultmedia':'<?php echo $plugin_url_for_ajax.'/assets/images/';?>','ak_wp_lang':'<?php echo $ak_wplang[0]; ?>','cal_first_day':'<?php echo get_option('start_of_week'); ?>','time_format':'<?php echo $wpTimeFormat; ?>','mb_status':'<?php echo get_option('moveto_guest_user_checkout'); ?>','multilocation_st':'<?php echo get_option('moveto_multi_location');?>','reviews_st':'<?php echo get_option('moveto_reviews_status');?>','full_cal_defaultdate':'<?php echo date_i18n('Y-m-d');?>','moveto_plivo_ccode_alph':'<?php echo get_option('moveto_plivo_ccode_alph');?>','moveto_twilio_ccode_alph':'<?php echo get_option('moveto_twilio_ccode_alph');?>','moveto_nexmo_ccode_alph':'<?php echo get_option('moveto_nexmo_ccode_alph');?>','moveto_textlocal_ccode_alph':'<?php echo get_option('moveto_textlocal_ccode_alph');?>','moveto_custom_formfields_val':'<?php echo $moveto_custom_formfields_val;?>'};

var admin_validation_err_msg = {	
								'article_category_title':'<?php echo $article_category_title;?>',
								'room_type_title':'<?php echo $room_type_title;?>',
								'movesize_title':'<?php echo $movesize_title;?>',
								'article_msg':'<?php echo $article_msg;?>',
								'article_price_msg':'<?php echo $article_price_msg;?>',
								'article_number_price_msg':'<?php echo $article_number_price_msg;?>',
								'distance_msg':'<?php echo $distance_msg;?>',
								'distance_number_msg':'<?php echo $distance_number_msg;?>',
								'distance_price_msg':'<?php echo $distance_price_msg;?>',
								'distance_number_price_msg':'<?php echo $distance_number_price_msg;?>',
								'floor_price_msg':'<?php echo $floor_price_msg;?>',
								'floor_number_price_msg':'<?php echo $floor_number_price_msg;?>',
								'floor_no_msg':'<?php echo $floor_no_msg;?>',
								'floor_no_valid_msg':'<?php echo $floor_no_valid_msg;?>',
								'elevator_discount_msg':'<?php echo $elevator_discount_msg;?>',
								'elevator_number_price_msg':'<?php echo $elevator_number_price_msg;?>',
								'packing_msg':'<?php echo $packing_msg;?>',
								'packing_number_msg':'<?php echo $packing_number_msg;?>',
								'unpacking_price_msg':'<?php echo $unpacking_price_msg;?>',
								'unpacking_number_price_msg':'<?php echo $unpacking_number_price_msg;?>',
								
								'categorytitle_err_msg':'<?php echo $categorytitle_err_msg;?>',
								'servicetitle_err_msg':'<?php echo $servicetitle_err_msg;?>',
								'servicedescription_err_msg':'<?php echo $servicedescription_err_msg;?>',
								'serviceprice_err_msg':'<?php echo $serviceprice_err_msg;?>',
								'servicepricedigit_err_msg':'<?php echo $servicepricedigit_err_msg;?>',
								'serviceofferpricegreater_err_msg':'<?php echo $serviceofferpricegreater_err_msg;?>',
								'servicecategory_err_msg':'<?php echo $servicecategory_err_msg;?>',
								'servicehrsrange_err_msg':'<?php echo $servicehrsrange_err_msg;?>',
								'serviceminsrange_err_msg':'<?php echo $serviceminsrange_err_msg;?>',
								'servicemins_err_msg':'<?php echo $servicemins_err_msg;?>',
								'servicenumpatt_err_msg':'<?php echo $servicenumpatt_err_msg;?>',
								'serviceaddontitle_err_msg':'<?php echo $serviceaddontitle_err_msg;?>',
								'serviceaddonmaxqty_err_msg':'<?php echo $serviceaddonmaxqty_err_msg;?>',
								'serviceaddon_price_err_msg':'<?php echo $serviceaddon_price_err_msg;?>',
								'serviceaddon_validprice_err_msg':'<?php echo $serviceaddon_validprice_err_msg;?>',
								'serviceaddon_qty_err_msg':'<?php echo $serviceaddon_qty_err_msg;?>',
								'serviceaddon_validqty_err_msg':'<?php echo $serviceaddon_validqty_err_msg;?>',						
														
								'source_city_err_msg':'<?php echo $source_city_err_msg;?>',
								'destination_city_err_msg':'<?php echo	$destination_city_err_msg;?>',
								'city_price_err_msg':'<?php echo $city_price_err_msg;?>', 	
								'city_price_validprice_err_msg':'<?php echo	$city_price_validprice_err_msg;?>',
								'additional_info_err_msg':'<?php echo	$additional_info_err_msg;?>',
								'additional_info_unit_price_err_msg':'<?php echo	$additional_info_unit_price_err_msg;?>',
								'additional_info_unit_price_digit_err_msg':'<?php echo	$additional_info_unit_price_digit_err_msg;?>',
										

										
								'quoteprice_err_msg':'<?php echo $quoteprice_err_msg;?>',
								'quotepricevalid_err_msg':'<?php echo	$quotepricevalid_err_msg;?>',
								
								
								'staffusername_err_msg':'<?php echo $staffusername_err_msg;?>',
								'staffusernameexist_err_msg':'<?php echo $staffusernameexist_err_msg;?>',
								'staffpassword_err_msg':'<?php echo $staffpassword_err_msg;?>',
								'staffemail_err_msg':'<?php echo $staffemail_err_msg;?>',
								'staffemailexist_err_msg':'<?php echo $staffemailexist_err_msg;?>',
								'stafffullname_err_msg':'<?php echo $stafffullname_err_msg;?>',
								'staffselect_err_msg':'<?php echo $staffselect_err_msg; ?>',
								'staffvalidphone_err_msg':'<?php echo $staffvalidphone_err_msg; ?>',
								
								'cuponcode_err_msg':'<?php echo $cuponcode_err_msg; ?>',
								'cuponvalue_err_msg':'<?php echo $cuponvalue_err_msg; ?>',
								'cuponvalueinvalid_err_msg':'<?php echo $cuponvalueinvalid_err_msg; ?>',
								'cuponlimit_err_msg':'<?php echo $cuponlimit_err_msg; ?>',
								'cuponlimitinvalid_err_msg':'<?php echo $cuponlimitinvalid_err_msg; ?>',
								
								'twilliosid_err_msg':'<?php echo $twilliosid_err_msg; ?>',
								'twillioauthtoken_err_msg':'<?php echo $twillioauthtoken_err_msg; ?>',
								'twilliosendernum_err_msg':'<?php echo $twilliosendernum_err_msg; ?>',
								'twillioadminnum_err_msg':'<?php echo $twillioadminnum_err_msg; ?>',
								
								'plivosid_err_msg':'<?php echo $plivosid_err_msg; ?>',
								'plivoauthtoken_err_msg':'<?php echo $plivoauthtoken_err_msg; ?>',
								'plivosendernum_err_msg':'<?php echo $plivosendernum_err_msg; ?>',
								'plivoadminnum_err_msg':'<?php echo $plivoadminnum_err_msg; ?>',
								
								'nexmoapi_err_msg':'<?php echo $nexmoapi_err_msg; ?>',
								'nexmoapisecert_err_msg':'<?php echo $nexmoapisecert_err_msg; ?>',
								'nexmofromnum_err_msg':'<?php echo $nexmofromnum_err_msg; ?>',
								'nexmoadminnum_err_msg':'<?php echo $nexmoadminnum_err_msg; ?>'
								
			
}
var appearance_setting = {"default_country_code":"<?php echo get_option('moveto_default_country_short_code'); ?>"};	
/* var formoptions = { "formhtml": }; */
</script>	
<script>
jQuery(document).bind('ready ajaxComplete', function(){
	jQuery('#add1').hide();
});
</script>	
<?php echo '<style>
#mp #mp-top-nav .navbar .nav > li > a{
	color: '.get_option('moveto_admin_color_text').' !important;
}
/* Primary Color */
#mp #mp-main-navigation{
	background: '.get_option('moveto_admin_color_primary').' !important;
}
#mp .loader .TruckLoader, 
	#mp .loader .TruckLoader:before, 
	#mp .TruckLoader:after, 
	#mp .loader .TruckLoader-cab, 
	#mp .loader .TruckLoader-cab:before, 
	#mp .TruckLoader-cab:after, 
	#mp .loader .TruckLoader-cab:after{
	background: '.get_option('moveto_admin_color_secondary').' !important;
	background-color: '.get_option('moveto_admin_color_secondary').' !important;
}
/*code to be added in demo*/
.mp-services-right-details .custom_article_checkbox input[type="checkbox"]:checked + label{
	border: 1px solid '.get_option('moveto_admin_color_secondary').' !important;
}
#mp .loader .mp-second{
	border: 3px solid '.get_option('moveto_admin_color_primary').' !important;
		border-bottom-color: transparent !important;
}
#mp .mp-notification-main .notification-header #mp-close-notifications:hover{
	background-color: '.get_option('moveto_admin_color_primary').' !important;
}
#mp .tooltip-arrow{
	border-right-color: '.get_option('moveto_admin_color_primary').' !important;
}

/* calendar page */
#mp .fc-toolbar {
	border-top: 1px solid '.get_option('moveto_admin_color_primary').' !important;
	border-left: 1px solid '.get_option('moveto_admin_color_primary').' !important;
	border-right: 1px solid '.get_option('moveto_admin_color_primary').' !important;
}
#mp .fc-toolbar {
	background-color: '.get_option('moveto_admin_color_primary').' !important;
}
#mp #mp-dashboard .mp-dash-icon.today{
	background-color: '.get_option('moveto_admin_color_primary').' !important;
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}
#mp .mp-notification-main .notification-header a.mp-clear-all{
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}
/* Secondary color */
#mp #mp-top-nav .navbar .nav > li > a:hover,
#mp #mp-top-nav .navbar .nav > .active > a,
#mp #mp-top-nav .navbar .nav > .active > a:focus{
	background: '.get_option('moveto_admin_color_secondary').' !important;
}
#mp a#mp-notifications i.icon-bell.mp-pulse.mp-new-booking{
	color: '.get_option('moveto_admin_color_secondary').' !important;
}
#mp .loader .mp-third{
	border: 3px solid '.get_option('moveto_admin_color_secondary').' !important;
		border-top-color: transparent !important; 
}
#mp  #mp-main-navigation .navbar .nav.mp-nav-tab li:before,
#mp  #mp-main-navigation .navbar .nav.mp-nav-tab li:after{
	border-left-color: '.get_option('moveto_admin_color_secondary').' !important;
	border-right-color: '.get_option('moveto_admin_color_secondary').' !important;
}	

#mp a#mp-notifications:hover i.fa-angle-down,
#mp .fc button:hover,
#mp button.fc-today-button:hover{
	color: '.get_option('moveto_admin_color_secondary').' !important;
}


/* admin color bg text  and  Secondary color */
#mp #mp-dashboard .mp-dash-icon.this-year,
#mp .mp-notification-main .notification-header{
	background-color: '.get_option('moveto_admin_color_secondary').' !important;
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}

#mp #mp-top-nav .navbar .nav > .active > a,
#mp #mp-top-nav .navbar .nav > .active > a:focus{
	background-color: '.get_option('moveto_admin_color_secondary').' !important;
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}
#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a:focus{
	background-color: '.get_option('moveto_admin_color_secondary').' !important;
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}

/* admin color bg text */
#mp #mp-main-navigation .navbar .nav > li > a{
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}

#mp .fc button,
#mp .mp-notification-main .notification-header #mp-close-notifications{
	color: '.get_option('moveto_admin_color_bg_text').' !important;
}

#mp .loader .mp-first{
	border: 3px solid '.get_option('moveto_admin_color_bg_text').' !important;
		border-right-color: transparent !important;
}


/* Desktops and laptops ----------- */
@media only screen  and (min-width : 768px) and (max-width : 1250px) {
	#mp #mp-main-navigation .navbar{
		background-color: '.get_option('moveto_admin_color_primary').' !important;
	}
	
	#mp #mp-main-navigation .navbar-header,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		color: '.get_option('moveto_admin_color_secondary').' !important;
		background-color: unset !important;
	}
		
}


/* iPads (portrait and landscape) ----------- */
@media only screen and (min-width : 768px) and (max-width : 1024px) {
	#mp #mp-main-navigation .navbar-header,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		color: '.get_option('moveto_admin_color_secondary').' !important;
	}
	
}
/* iPads (landscape) ----------- */
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : landscape) {
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		background-color: '.get_option('moveto_admin_color_secondary').' ;
		color: '.get_option('moveto_admin_color_text').' !important;
	}

}
/* iPads (portrait) ----------- */
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) {
	#mp #mp-top-nav .navbar-header,
	#mp #mp-main-navigation .navbar-header,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus,
	#mp #mp-top-nav .navbar-nav > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		color: '.get_option('moveto_admin_color_secondary').' !important;
	}
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus{
		background: unset !important;
	}
}	
/********** iPad 3 **********/
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : landscape) and (-webkit-min-device-pixel-ratio : 2) {
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		background-color: '.get_option('moveto_admin_color_secondary').' ;
		color: '.get_option('moveto_admin_color_text').' !important;
	}
}
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : portrait) and (-webkit-min-device-pixel-ratio : 2) {	
	#mp #mp-top-nav .navbar-header,
	#mp #mp-main-navigation .navbar-header,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus,
	#mp #mp-top-nav .navbar-nav > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		color: '.get_option('moveto_admin_color_secondary').' !important;
	}
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus{
		background: unset !important;
	}
}
/* Smartphones (landscape) ----------- */
@media only screen and (max-width: 767px) {
	#mp #mp-main-navigation .navbar{
		background-color: '.get_option('moveto_admin_color_primary').' !important;
	}
	
	#mp #mp-top-nav .navbar-header,
	#mp #mp-main-navigation .navbar-header,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus,
	#mp #mp-top-nav .navbar-nav > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		color: '.get_option('moveto_admin_color_secondary').' !important;
		background-color: unset !important;
	}
	/*
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus{
		background: unset !important;
	}
	*/
	
}	
/* Smartphones (portrait and landscape) ----------- */
@media only screen and (min-width : 320px) and (max-width : 480px) {
	
	#mp #mp-top-nav .navbar-header,
	#mp #mp-main-navigation .navbar-header,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus,
	#mp #mp-top-nav .navbar-nav > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > li > a:hover,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > li > a:hover {
		color: '.get_option('moveto_admin_color_secondary').' !important;
		background-color: unset !important;
	}
	/*
	#mp #mp-main-navigation .navbar .nav.mp-nav-tab > .active > a,
	#mp #mp-main-navigation .navbar .nav.user-nav-bar > .active > a,
	#mp #mp-top-nav .navbar .nav > .active > a:focus{
		background: unset !important;
	}
	*/
}
</style>' ;

if(is_rtl()){
	echo "<script type='text/javascript'>
	jQuery(document).ready(function(){
		jQuery('#mp').addClass('mpdbrtl');
	});
	
	</script>";	
}




 
?>

<!-- main wrapper -->
<div class="mp-wrapper" id="mp">

<!-- all alerts, success messages -->
<!--<div class="mp-alert-msg-show-main mainheader_message">		
	<div class="mp-all-alert-messags alert alert-success mainheader_message_inner">
		<a href="#" class="close" data-dismiss="alert">&times;</a>
		<strong><?php echo __("Success!","mp");?></strong> <span id="mp_sucess_message"><?php echo __("Updated successfully","mp");?></span>
	</div>
</div>	-->
<!--<div class="mp-alert-msg-show-main mainheader_message_fail">		
	<div class="mp-all-alert-messags alert alert-danger mainheader_message_inner_fail">
		<a href="#" class="close" data-dismiss="alert">&times;</a>
		<strong><?php echo __("Failed!","mp");?></strong> <span id="mp_sucess_message_fail"><?php echo __("Updated successfully","mp");?></span>
	</div>
</div>	-->
<!-- loader -->
<div class="mp-loading-main" style="display: block;" >
	<div class="loader">
		<div class="mp-loader">
			<div class="TruckLoader">
				<div class="TruckLoader-cab"></div>
				<div class="TruckLoader-smoke"></div>
			</div>
		
		</div>
	</div>
</div>

	<header class="mp-header">
	<?php if(!isset($current_user->caps['mp_client']) || isset($current_user->caps['administrator'])){ ?>
		


		<!-- recent notifications listing -->
		<div class="mp-overlay-notification"></div>
		<div id="mp-notification-container">
			<div class="mp-notifications-inner">
				<div class="mp-notification-main">
					<div class="mp-notification-main">
						<h4 class="notification-header"><a data-booking_id="All" href="javascript:void(0)" class="btn btn-link pull-left mp-clear-all mp_unread_notification"><?php echo __("Clear All","mp");?></a><?php echo __("Lead notifications","mp");?>
						<a id="mp-close-notifications" class="pull-right" href="javascript:void(0);" title="<?php echo __("Close Notifications","mp");?>"><i>×</i></a></h4>
						<div class="mp-recent-booking-container">
							<ul class="mp-recent-booking-list ">
								<div class="mp-load-bar">
									  <div class="mp-bar"></div>
									  <div class="mp-bar"></div>
									  <div class="mp-bar"></div>
								</div>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end recent notifications -->
		
		<div id="mp-main-navigation" class="navbar-inner">
			<nav role="navigation" class="navbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
						<span class="sr-only"><?php echo __("Toggle navigation","mp");?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- <a href="mp-admin1.html" class="navbar-brand"><img src="images/logo-moveto.png" /></a> -->
				</div>
				<!-- Collection of nav links and other content for toggling -->
				<div id="navbarCollapse" class="collapse navbar-collapse np">
		<?php  
		if($user_sp=='Y' && $user_sp_manager=='') {	?>
		<ul class="nav navbar-nav mp-nav-tab pl-20">
		<?php if(is_rtl()){  ?>

			<li class="<?php if($_GET['page']=='appointments_submenu' || $_GET['page']=='moveto_menu'){ echo 'active'; } ?>"><a href="?page=appointments_submenu"><i class="lni lni-calendar"></i><span> <?php echo __('Quote Requests',"mp"); ?></span></a></li>
			
			<li class="<?php if($_GET['page']=='all_booking_submenu'){ echo 'active'; } ?>"><a href="?page=all_booking_submenu"><i class="lni lni-list"></i><span> <?php echo __('All Leads',"mp"); ?></span></a></li>
			
			<li class="<?php if($_GET['page']=='provider_submenu'){ echo 'active'; } ?>"><a href="?page=sp_settings_submenu"><i class="lni-cog"></i><span> <?php echo __('Settings',"mp"); ?></span></a></li> 
		<?php }else{ ?>
			<li class="<?php if($_GET['page']=='provider_submenu'){ echo 'active'; } ?>"><a href="?page=provider_submenu"><i class="lni-cog"></i><span> <?php echo __('Settings',"mp"); ?></span></a></li>
			
			<li class="<?php if($_GET['page']=='appointments_submenu' || $_GET['page']=='moveto_menu'){ echo 'active'; } ?>"><a href="?page=appointments_submenu"><i class="lni lni-calendar custom-staff-tab-width"> </i><span><?php echo __('Quote Requests',"mp"); ?></span></a></li>
		<?php } ?>
	</ul>
	<?php } else { ?>
	<ul class="nav navbar-nav mp-nav-tab pl-20"> 
		<?php if(is_rtl()){ 
		global $wpdb;		
			if(isset($moveto_sampledata) && $moveto_sampledata!=''){ ?>
				<li ><a id="moveto_sampledata" data-method="Remove" href="javascript:void(0)"><i class="lni lni-close"></i><span><?php echo __('Remove Sample Data',"mp"); ?></span></a></li>
			<?php }else{ ?>
				<li ><a id="moveto_sampledata" data-method="Add"  href="javascript:void(0)"><i class="lni lni-download"></i><span> <?php echo __('Sample Data',"mp"); ?></span></a></li>		
				<?php 
				}	?>
		
		
		<li class="<?php if($_GET['page']=='export_submenu'){ echo 'active'; } ?>"><a href="?page=export_submenu"><i class="lni lni-files"></i><span><?php echo __('Export',"mp"); ?></span></a></li>
		<?php if(current_user_can('manage_options')){ ?>
		<li class="<?php if($_GET['page']=='settings_submenu'){ echo 'active'; } ?>"><a href="?page=settings_submenu"><i class="lni lni-cog"></i><span><?php echo __('Settings',"mp"); ?></span></a></li>
		
		<?php } ?>
		
					
		<li class="<?php if($_GET['page']=='clients_submenu' || $_GET['page']=='guest_clients_submenu'){ echo 'active'; } ?>"><a href="?page=clients_submenu"><i class="lni lni-users"></i><span> <?php echo __('Customers',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='availability_submenu'){ echo 'active'; } ?>"><a href="?page=availability_submenu"><i class="lni lni-user"></i><span> <?php echo __('Availability',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='additional_info_submenu' || $_GET['page']=='additional_info_submenu'){ echo 'active'; } ?>"><a href="?page=additional_info_submenu"><i class="lni lni-information"></i><span> <?php echo __('Additional Info',"mp"); ?></span></a></li>	
		
		
		<li class="<?php if($_GET['page']=='services_submenu' || $_GET['page']=='service_addons'){ echo 'active'; } ?>"><a href="?page=services_submenu"><i class="lni lni-agenda"></i><span> <?php echo __('Services',"mp"); ?></span></a></li>
		
						
		<li class="<?php if($_GET['page']=='appointments_submenu'){ echo 'active'; } ?>"><a href="?page=appointments_submenu"><i class="lni lni-calendar"></i><span> <?php echo __('Quote Requests',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='dashboard_submenu'){ echo 'active'; } ?>"><a href="?page=dashboard_submenu"><i class="lni lni-dashboard"></i><span> <?php echo __('Dashboard',"mp"); ?></span></a></li>
	  
		<?php }else{ ?>
		 
		<li class="<?php if($_GET['page']=='dashboard_submenu'){ echo 'active'; } ?>"><a href="?page=dashboard_submenu"><i class="lni lni-dashboard"></i><span> <?php echo __('Dashboard',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='appointments_submenu'){ echo 'active'; } ?>"><a href="?page=appointments_submenu"><i class="lni lni-calendar"></i><span> <?php echo __('Quote Requests',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='all_booking_submenu'){ echo 'active'; } ?>"><a href="?page=all_booking_submenu"><i class="lni lni-list"></i><span> <?php echo __('All Leads',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='movesize_submenu'){ echo 'active'; } ?>"><a href="?page=movesize_submenu"><i class="lni lni-home"></i><span> <?php echo __('Move Size',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='services_submenu' || $_GET['page']=='service_addons'){ echo 'active'; } ?>"><a href="?page=services_submenu"><i class="lni lni-agenda"></i><span> <?php echo __('Article',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='distance'){ echo 'active'; } ?>"><a href="?page=distance"><i class="lni lni-money-protection"></i><span> <?php echo __('Pricing',"mp"); ?></span></a></li>
		
		<li class="<?php if($_GET['page']=='package_addons'){ echo 'active'; } ?>"><a href="?page=package_addons"><i class="lni lni-agenda"></i><span> <?php echo __('Package',"mp"); ?></span></a></li>
		
		
		
		<li class="<?php if($_GET['page']=='additional_info_submenu' || $_GET['page']=='additional_info_submenu'){ echo 'active'; } ?>"><a href="?page=additional_info_submenu"><i class="lni lni-information"></i><span> <?php echo __('Additional Info',"mp"); ?></span></a></li>	
		
		
		<li class="<?php if($_GET['page']=='availability_submenu'){ echo 'active'; } ?>"><a href="?page=availability_submenu"><i class="lni lni-user"></i><span> <?php echo __('Availability',"mp"); ?></span></a></li>
		
		
		<li class="<?php if($_GET['page']=='clients_submenu' ){ echo 'active'; } ?>"><a href="?page=clients_submenu"><i class="lni lni-users"></i><span> <?php echo __('Customers',"mp"); ?></span></a></li>
		
		
		<?php if(current_user_can('manage_options')){ ?>		
		
		<li class="<?php if($_GET['page']=='settings_submenu'){ echo 'active'; } ?>"><a href="?page=settings_submenu"><i class="lni lni-cog"></i><span> <?php echo __('Settings',"mp"); ?></span></a></li>
		<?php } ?>
		
		<li class="<?php if($_GET['page']=='export_submenu'){ echo 'active'; } ?>"><a href="?page=export_submenu"><i class="lni lni-files"></i><span> <?php echo __('Export',"mp"); ?></span></a></li>
		
		
		
		<?php 
			global $wpdb;
			
			if(isset($moveto_sampledata) && $moveto_sampledata!=''){
				?>
				<li ><a class="cd-popup-trigger" href="javascript:void(0)"><i class="lni lni-close"></i><span> <?php echo __('Remove Sample Data',"mp"); ?></span></a></li>
			<?php }else{ ?>
				<li ><a id="moveto_sampledata" data-method="Add"  href="javascript:void(0)"><i class="lni lni-download"></i><span> <?php echo __('Sample Data',"mp"); ?></span></a></li>		
				<?php 
				}
		} ?>
	</ul>
<?php } ?>
		
				</div>	
			</nav>
		</div> <!-- top bar end here -->			
		<?php } ?>
		<!-- Alert Box For Remove Sample Data -->
		<div class="cd-popup" role="alert" style="z-index: 999;">
			<div class="cd-popup-container">
				<p><?php echo __("Are you sure you want to delete Sample data, It will remove all data related to sample data?","mp");?></p>
				<ul class="cd-buttons">
					<li><a id="moveto_sampledata" data-method="Remove" href="#0"><?php echo __("Yes","mp");?></a></li>
					<li><a class="remove_popup_sample_data" href="#0"><?php echo __("No","mp");?></a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->
		
		<!-- Alert Box For Remove Sample Data -->
				<!-- show pop details click on appointment from listing -->
		<div id="booking-details" class="modal fade booking-details-calendar" tabindex="-1" role="dialog" aria-hidden="true"> <!-- modal pop up start -->
		<div class="vertical-alignment-helper">
			<div class="modal-dialog modal-md vertical-align-center">
				<div class="modal-content">
					<div class="modal-header bg-info">
					
						<button type="button" class="close close_booking_detail_modal" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" style="margin-top: 10px; margin-bottom: 0; color:#fff"><?php echo __("Lead Details","mp");?> </h4>
						
					</div>
					<div class="modal-body">
						<ul class="list-unstyled mp-cal-booking-details">
							<li>
								<label><?php echo __("Lead Status","mp");?></label>
								<div class="mp-booking-status"><span class="badge animated pulse span-scroll" style="background-color: #31bf57;"></span></div>
							</li>							
							
							
							<li>
								<label><?php echo __("Lead Date","mp");?></label>
								<span class="mp_booking_date span-scroll span_indent"> </span>
							</li>
							<li>
								<label><?php echo __("Source","mp");?></label>
								<span class="mp_source_city span-scroll span_indent"></span>
							</li>
							<li>
								<label><?php echo __("Via","mp");?></label>
								<span class="mp_via_city span-scroll span_indent"></span>
							</li>
							<li>
								<label><?php echo __("Destination","mp");?></label>
								<span class="mp_destination_city span-scroll span_indent"></span>
							</li>
							<div  class="mp_service" >
							</div>
							
							<li class="service_info"></li>
							<li class="articles_info"></li>
							<li class="additional_info"></li>
							<li class="loading_info"></li>
							<li class="unloading_info"></li>
							
							<li><h5 class="mp-customer-details-hr" style="font-weight: 500;font-size:18px;color:#000;"><?php echo __("Customer","mp");?></h5>
							</li>
							<li>
								<label><?php echo __("Name","mp");?></label>
								<span class="client_name span-scroll span_indent"></span>
							</li>
							<li>
								<label><?php echo __("Email","mp");?></label>
								<span class="client_email span-scroll span_indent"></span>
							</li>
							<!-- <li>
								<label><?php //echo __("Address","mp");?></label>
								<span class="client_address span-scroll span_indent"></span>
							</li> -->
							<!-- <li>
							<label><?php// echo __("Postal Code","mp");?></label>
							<span class="postal_code span-scroll span_indent"></span>
							</li> -->
							<li>
								<label><?php echo __("Phone","mp");?></label>
								<span class="client_phone span-scroll span_indent"></span>
							</li>							
							<!-- <li>
								<label><?php// echo __("Notes","mp");?></label>
								<span class="client_notes span-scroll span_indent"></span>
							</li> -->
							<li class="quo_price">
								<label><?php echo __("Quote Price","mp");?></label>
								<span class="quote_price span-scroll span_indent"></span>
							</li>
							<li>
								<label><?php echo __("Payment","mp");?></label>
								<span class="client_payment span-scroll span_indent"></span>
							</li>
							<span class="custom_info">								

							</span>
								
							
						</ul>
					</div>
					<div class="modal-footer">
						<div class="mp-col3 mp-footer-popup-btn">												
							<span id="mp_delete_btn" class="col-md-2 col-sm-2 col-xs-4 np mp-w-32">
								<a id="mp-delete-appointment-cal-popup" class="btn btn-link mp-small-btn" rel="popover" data-placement='top' title="<?php echo __("Delete this Quote Request ?","mp");?>"><i class="lni lni-trash fa-2x"></i><br /> <?php echo __("Delete","mp");?></a>
							
							<div id="popover-delete-appointment-cal-popup" style="display: none;">
								<div class="arrow"></div>
								<table class="form-horizontal" cellspacing="0">
									<tbody>
										<tr>
											<td>
												<button id="mp_booking_delete"  value="Delete" class="btn btn-danger btn-sm" type="submit"><?php echo __("Delete","mp");?></button>
												<button id="mp-close-del-appointment-cal-popup" class="btn btn-default btn-sm" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
											</td>
										</tr>
									</tbody>
								</table>
							</div><!-- end pop up -->
						  </span>	
						</div>
						<div class="mp-col3 mp-footer-popup-btn">												
							<span id="mp_delete_btn" class="col-md-2 col-sm-2 col-xs-4 np mp-w-32">
								<a href="#pam_send_quote" role="button" class="btn btn-link mp-small-btn pam_send_quote" data-toggle="modal"><i class="lni lni-share fa-2x"></i><br /> <?php echo __("Send Quote","mp");?></a>
						  </span>	
						</div>
					</div>
				</div>
			</div>
		
		</div>
		<div id="pam_send_quote" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><?php echo __("Send Quote","mp");?></h4>
					</div>
					<form id="pam_send_quote_form" method="post">
						<input type="hidden" class="form-control" id="pam_quote_booking_id" value="" />
						<div class="modal-body">
						<div class='mp_source_cityct'></div>
							<div class="update_appointment_res my_response">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pam_quote_cancel" data-dismiss="modal"><?php echo __("Cancel","mp");?></button>
							<button type="button" id="pam_submit_quote" class="btn btn-primary"><?php echo __("Send Now","mp");?></button>
						</div>
					</form>
				</div>
			</div>	
		</div>
	</div><!-- end details of booking -->
	</header>