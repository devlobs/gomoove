
/* range slider */
var rangeSlider = function(){
  var slider = jQuery('.range-slider'),
      range = jQuery('.range-slider__range'),
      value = jQuery('.range-slider__value');
    
  slider.each(function(){

    value.each(function(){
      var value = jQuery(this).prev().attr('value');
      jQuery(this).html(value);
    });

    range.on('input', function(){
      jQuery(this).next(value).html(this.value);
    });
  });
};

rangeSlider();

/*  plus minus */

jQuery(document).ready(function() {

	jQuery('.minus').on( 'click', function() {
		//alert("hello");
		var ajaxurl = mpmain_obj.plugin_path;
		var $input = jQuery(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();		

		var article_id = jQuery(this).data("id");
		var cat_id = jQuery(this).data("catid");
		var room_id = jQuery(this).data("roomid");
		var postdata = {article_id:article_id,cat_id:cat_id,room_id:room_id,qty:count,action:'add_to_cart'};	
		jQuery.ajax({
			type:"POST",
			async:false,
			url  : ajaxurl+"/assets/lib/add_to_cart.php",
			dataType : 'html',			
			data:postdata,
			success:function(data){	
				 console.log(data);
			},
			error: function(data) {
      			  console.log(data);
    		}
		});
		return false;
	});
	jQuery('.plus').on( 'click', function() {
	//	alert("hello");
		var ajaxurl = mpmain_obj.plugin_path;
		var $input = jQuery(this).parent().find('input');
		$input.val(parseInt($input.val()) + 1);
		$input.change();

		var article_id = jQuery(this).data("id");
		var cat_id = jQuery(this).data("catid");
		var room_id = jQuery(this).data("roomid");
		var postdata = {article_id:article_id,cat_id:cat_id,room_id:room_id,qty:count,action:'add_to_cart'};	
		jQuery.ajax({
			type:"POST",
			async:false,
			url  : ajaxurl+"/assets/lib/add_to_cart.php",
			dataType : 'html',			
			data:postdata,
			success:function(data){	
				 console.log(data);
			},
			error: function(data) {
      			  console.log(data);
    		}
		});
		return false;
	});

	/* jQuery('ul.sideways').first().find("li.room_type_cat").trigger("click"); */
	jQuery("#first_cat").trigger("click");
	jQuery("#first_cat").addClass("active");
});

/* Front page Form Steps show/hide */

jQuery(document).ready(function () { 
	
	jQuery('#first_step_submit').on( "click", function() {
		jQuery('#mp_first_step').slideUp(500);
		jQuery('#mp_second_step').slideDown(500);
	});
	jQuery('#second_step_submit').on( "click", function() {
		jQuery('#mp_second_step').slideUp(500);
		jQuery('#mp_third_step').slideDown(500);
	});
	jQuery('#third_step_submit').on( "click", function() {
		jQuery('#mp_third_step').slideUp(500);
		jQuery('#mp_forth_step').slideDown(500);
	});
	jQuery('#forth_step_submit').on( "click", function() {
		jQuery('#mp_forth_step').slideUp(500);
		jQuery('#mp_fifth_step').slideDown(500);
	});

	/*Code By Ajay Prajapati-START(26/3/19)*/
  
	jQuery('#source_city, #destination_city').keyup(function() {
		var ajaxurl = mpmain_obj.plugin_path;
		var source_city = jQuery("#source_city").val();
		var destination_city = jQuery("#destination_city").val();

		var postdata = {source_city:source_city,destination_city:destination_city,action:'mp_get_km_distance'};		
		jQuery.ajax({
				type:"POST",
				async:false,
				url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
				dataType : 'html',			
				data:postdata,
				success:function(data){	
					 console.log(data);
				},
				error: function(data) {
          			  console.log(data);
        }
		});
	});

	/*Code By Ajay Prajapati-END(26/3/19)*/
});

		jQuery(".loader").show();
jQuery(window).load(function() {
		// Animate loader off screen
		jQuery(".loader").hide(10);
	});

/* Front page Form Steps show/hide */
jQuery(document).ready(function () { 
	
	jQuery('#btn-more-bookings').on( "click", function() {
		jQuery('#mp_first_step').addClass('show-data');
		jQuery('#mp_first_step').removeClass('hide-data');        
		jQuery('#mp_second_step').addClass('hide-data');
		jQuery('#mp_second_step').removeClass('show-data');
		jQuery('#mp_third_step').addClass('hide-data');
		jQuery('#mp_third_step').removeClass('show-data');
	});
	jQuery('.mp-cart-items-count').on( "click", function() {
		jQuery('#mp_first_step').addClass('hide-data');
		jQuery('#mp_first_step').removeClass('show-data');        
		jQuery('#mp_second_step').addClass('show-data');
		jQuery('#mp_second_step').removeClass('hide-data');		
	});
	
});	

	
/* scroll to top when on second step */
jQuery(document).ready(function(){
	jQuery('#btn-more-bookings, .mp-cart-items-count, #btn-third-step').on('click',function(){
		jQuery('html, body').stop().animate({
			'scrollTop': jQuery('#mp-main').offset().top - 80
		}, 800, 'swing', function () {});
	});
});


jQuery(document).ready(function () { 
	if (jQuery("#mp").width() >= 380 && jQuery("#mp").width() < 600){
		jQuery( ".mp-main-left").addClass( "active-left-xs12" );
		jQuery( ".mp-main-right").addClass( "active-right-xs12" );
	}	
	if (jQuery("#mp").width() >= 601 && jQuery("#mp").width() < 850){
		jQuery( ".mp-main-left").addClass( "active-left-res100" );
		jQuery( ".mp-main-right").addClass( "active-right-res57" );
	}	
	
	if (!jQuery('.mp_remove_left_sidebar_class').hasClass("no-sidebar-right")) {
		jQuery('.mp_remove_left_sidebar_class').addClass('mp-asr');
	}
	if (!jQuery('.mp_remove_right_sidebar_class').hasClass("no-cart-item-sidebar")) {
		jQuery('.mp_remove_right_sidebar_class').addClass('mp-cis');
	}
});   



/* Booking summary delete extra service NS */
jQuery(document).ready(function () { 
	jQuery(document).on("click",".mp-delete-icon",function() {
		if(jQuery('.mp-es').hasClass('delete-toggle')){
			jQuery(".mp-es").removeClass('delete-toggle'); 
		}
		jQuery(this).parent(".mp-es").addClass('delete-toggle');
	});
	jQuery(document).on("click",".mp-delete-confirm",function() {
		jQuery(this).parent(".mp-es").slideUp();
	});
	
	/* Booking summary delete booking full list */
	jQuery(document).on("click",".mp-delete-booking",function() {
		if(jQuery('.booking-list').hasClass('delete-list')){
			jQuery(".booking-list").removeClass('delete-list'); 
		}
		jQuery(this).parent(".booking-list").addClass('delete-list');
	});
	jQuery(document).on("click",".mp-delete-booking-box",function() {
		jQuery(this).parent(".booking-list").slideUp();
	});
	
	/* Remove delete booking button on ESC key */
	jQuery( document ).on( 'keydown', function ( e ) {
		if ( e.keyCode === 27 )  {
			jQuery(".booking-list").removeClass('delete-list'); 
			jQuery(".mp-es").removeClass('delete-toggle'); 
		}
	});

	/* var elem = jQuery( '.sidebar-box' );
	jQuery( document ).on( 'click', function ( e ) {
		if (jQuery( e.target ).closest( elem ).length === 0 ) {
			jQuery(".booking-list").removeClass('delete-list'); 
			jQuery(".mp-es").removeClass('delete-toggle'); 
		}
	});  */
	
});
jQuery(document).ready(function() {
	jQuery('.mp-slots-count').tooltipster({
		animation: 'grow',
		delay: 10,
		side: 'top',
		theme: 'tooltipster-shadow',
		trigger: 'hover'
	});
});



/* custom dropdown show hide list */

jQuery(document).ready(function () { 
	
	
	
	/* Location */
	jQuery(document).on("click",".select-location",function() {
		jQuery(".service-selection").removeClass('clicked');
		jQuery(".service-dropdown").removeClass('bounceInUp');	
		jQuery(".staff-selection").removeClass('clicked');
		jQuery(".staff-dropdown").removeClass('bounceInUp');
		
		jQuery(".cus-location").addClass('focus');
		jQuery(".location-selection").toggleClass('clicked');
		jQuery(".location-dropdown").toggleClass('bounceInUp');	
		
	});
	jQuery(document).on("click",".select-location-clone",function() {
		
		jQuery(".service-selection").removeClass('clicked');
		jQuery(".service-dropdown").removeClass('bounceInUp');	
		jQuery(".staff-selection").removeClass('clicked');
		jQuery(".staff-dropdown").removeClass('bounceInUp');
		
		jQuery(".cus-location-clone").addClass('focus');
		jQuery(".location-selection-clone").toggleClass('clicked');
		jQuery(".location-dropdown-clone").toggleClass('bounceInUp');	
	});
	
	jQuery(document).on("click",".select_location",function() {		
		jQuery('#bungalow_unloading_type').val(jQuery(this).find('.mp-value').text());
		jQuery('#selected_location').html(jQuery(this).html());
		jQuery(".location-selection").removeClass('clicked');
		jQuery(".location-dropdown").removeClass('bounceInUp');		
	});
	jQuery(document).on("click",".select_location-clone",function() {
		jQuery('#bungalow_loading_type').val(jQuery(this).find('.mp-value').text());
		jQuery('#selected_location-clone').html(jQuery(this).html());
		jQuery(".location-selection-clone").removeClass('clicked');
		jQuery(".location-dropdown-clone").removeClass('bounceInUp');		
	});
	
	/* Car Type */
	jQuery(document).on("click",".select-location-cartype_sig",function() {	
		jQuery(".cus-location-cartype_sig").addClass('focus');
		jQuery(".location-selection-cartype_sig").addClass('clicked');
		jQuery(".location-dropdown-cartype_sig").addClass('bounceInUp');	
		
	});
	
	jQuery(document).on("click",".select_location-cartype_sig",function() {
		jQuery('#vehicle_service_car_type').val(jQuery(this).find('.mp-value').text());
		jQuery(".cus-location-cartype_sig").removeClass('focus');
		jQuery(".location-selection-cartype_sig").removeClass('clicked');
		jQuery(".location-dropdown-cartype_sig").removeClass('bounceInUp');
		jQuery('#selected_location-cartype').html(jQuery(this).html());
	});	
	/* Service */
	jQuery(document).on("click",".select-custom",function() {
		jQuery(".staff-selection").removeClass('clicked');
		jQuery(".staff-dropdown").removeClass('bounceInUp');
		jQuery(".location-selection").removeClass('clicked');
		jQuery(".location-dropdown").removeClass('bounceInUp');
		
		jQuery(".cus-select").addClass('focus');	
		jQuery(".service-selection").toggleClass('clicked');
		jQuery(".service-dropdown").toggleClass('bounceInUp');
	});
	jQuery(document).on("click",".select_custom",function() {
		jQuery(".service-selection").removeClass('clicked');
		jQuery(".service-dropdown").removeClass('bounceInUp');		
	});
	
	/* Addon service counting */
	jQuery(function () {
		jQuery('#add').on('click',function(){
			var $qty=jQuery(this).closest('.mp-btn-group').find('.addon_qty');
			var currentVal = parseInt($qty.val());
			if (!isNaN(currentVal)) {
				$qty.val(currentVal + 1);
			}
		});
		jQuery('#minus').on('click',function(){
			var $qty=jQuery(this).closest('.mp-btn-group').find('.addon_qty');
			var currentVal = parseInt($qty.val());
			if (!isNaN(currentVal) && currentVal > 0) {
				$qty.val(currentVal - 1);
			}
		});
	});
});
/* Calendar click date to show slots */
jQuery(document).ready(function () { 
	/* user new and existing radio show hide fields */
	/* jQuery(document).on('click', '#mp-existing-user', function(){			
		jQuery('.existing-user-login').show( "blind", {direction: "vertical"}, 1000 );
		jQuery('.mp-new-user-area').hide( "blind", {direction: "vertical"}, 500 );
		
	});
	jQuery(document).on('click', '#mp-new-user', function(){			
		jQuery('.new-user-area').show( "blind", {direction: "vertical"}, 1000 );
		jQuery('.existing-user-login').hide( "blind", {direction: "vertical"}, 500 );
		
	});  */
	
	jQuery(document).on('ready ajaxComplete', function(){
		var allowed_country_alpha_code = mpmain_obj.moveto_selected_flags;
		if(allowed_country_alpha_code != ""){
			var array_code = allowed_country_alpha_code.split(',');
			array_code_length = array_code.length;
			if(array_code_length == 1){
				jQuery("#mp-front-phone").intlTelInput({
			 /*   allowDropdown: false,
			   autoHideDialCode: false,
			   autoPlaceholder: false,
			   dropdownContainer: "body",
			   excludeCountries: ["us"],
			   geoIpLookup: function(callback) {
				 $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
				   var countryCode = (resp && resp.country) ? resp.country : "";
				   callback(countryCode);
				 });
			   },
			   initialCountry: "auto",
			   nationalMode: false,
			   numberType: "MOBILE",
			   preferredCountries: ['cn', 'jp'], */
			   onlyCountries: array_code,
			   separateDialCode: true,
			   utilsScript: "js/utils.js"
			 });
			 jQuery('.iti-arrow').hide();
			 jQuery('.country-list').hide();
			}else{
			jQuery("#mp-front-phone").intlTelInput({
			 /*   allowDropdown: false,
			   autoHideDialCode: false,
			   autoPlaceholder: false,
			   dropdownContainer: "body",
			   excludeCountries: ["us"],
			   geoIpLookup: function(callback) {
				 $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
				   var countryCode = (resp && resp.country) ? resp.country : "";
				   callback(countryCode);
				 });
			   },
			   initialCountry: "auto",
			   nationalMode: false,
			   numberType: "MOBILE",
			   preferredCountries: ['cn', 'jp'], */
			   onlyCountries: array_code,
			   separateDialCode: true,
			  utilsScript: "js/utils.js"
			});
			}
		}else{
			jQuery("#mp-front-phone").intlTelInput({
			 /*   allowDropdown: false,
			   autoHideDialCode: false,
			   autoPlaceholder: false,
			   dropdownContainer: "body",
			   excludeCountries: ["us"],
			   geoIpLookup: function(callback) {
				 $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
				   var countryCode = (resp && resp.country) ? resp.country : "";
				   callback(countryCode);
				 });
			   },
			   initialCountry: "auto",
			   nationalMode: false,
			   numberType: "MOBILE",
			   onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
			   preferredCountries: ['cn', 'jp'], */
			   separateDialCode: true,
			  utilsScript: "js/utils.js"
			});
		}
	});
	/* payment methods */
	jQuery(document).on('click','.payment_checkbox',function() {
		if(jQuery('#stripe-payments').is(':checked')) { jQuery('#stripe-payment-main').fadeIn("slow"); } else {
			 jQuery('#stripe-payment-main').fadeOut("slow");
		}		

	});
});

/* see more instructions in service popup */
jQuery(document).ready(function() {
    jQuery(".show-more-toggler").click(function() {
		jQuery(".bullet-more").toggle( "blind", {direction: "vertical"}, 500);
        jQuery(".show-more-toggler").toggleClass('rotate');
    });
});


/*********************************************************************************************/
/********************************** OCT Front JS Function ********************************** / 
/*********************************************************************************************/

/* Get Location by Zip Code/Postal Code If Multisite is Enabled */
jQuery(document).on('keyup','#mp_zip_code',function(event){
	var ajaxurl = mpmain_obj.plugin_path;
	var location_err_msg = mpmain_obj.location_err_msg;
	var location_search_msg = mpmain_obj.location_search_msg;
	var Choose_service_msg = mpmain_obj.Choose_service;
	
	var zipcode = jQuery('#mp_zip_code').val();	
	jQuery('#mp_selected_service').val(0);
	jQuery('#mp_selected_staff').val(0);
	jQuery('#mp_selected_location').val('X');
	jQuery('#mp_service_addons').html('');
	jQuery('#mp_service_addon_st').val('D');
	jQuery('#mp_selected_datetime').val('');
	jQuery('#mp_datetime_error').hide();
	jQuery('.mp-selected-date-view').addClass('mp-hide');
	
	if(zipcode!=''){
		jQuery('#mp .loader').show();	
		jQuery('#mp_location_success').hide();			
		jQuery('#close_service_details').trigger('click');	
		jQuery('#selected_custom .mp-value').html(Choose_service_msg);
		
		jQuery('#mp_location_error').html(location_search_msg);
		var postdata = {zipcode:zipcode,action:'mp_get_location'};		
		jQuery.ajax({
				type:"POST",
				async:false,
				url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
				dataType : 'html',			
				data:postdata,
				success:function(response){	
					jQuery('#mp .loader').hide();
					if(jQuery.trim(response)=='notfound'){
						jQuery('#mp_location_success').hide();	
						jQuery('#mp_location_error').show();
						jQuery('#mp_location_error').html(location_err_msg);
					}else{	
						jQuery('#mp_selected_location').val(0);
						jQuery('#mp_location_success').show();						
						jQuery('#mp_location_error').hide();
						/* Get Services By Found Location */
						var location_id = 0;
						var servicedata = {location_id:location_id,action:'mp_get_location_services'};	
						jQuery.ajax({
							type:"POST",
							url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
							dataType : 'html',			
							data:servicedata,
							success:function(response){					
								jQuery('#mp_services').html(response);								
							}
						});
					}				
				}
		});
	}
});

jQuery(document).on('click','.select_location',function(event){
	var ajaxurl = mpmain_obj.plugin_path;
	var location_err_msg = mpmain_obj.location_err_msg;
	var location_search_msg = mpmain_obj.location_search_msg;
	var Choose_service_msg = mpmain_obj.Choose_service;
	jQuery('#mp_location_error').hide();	
	jQuery('#close_service_details').trigger('click');	
	/* jQuery('#selected_custom .mp-value').html(Choose_service_msg);	 */
	
	jQuery('#mp_selected_service').val(0);
	jQuery('#mp_selected_staff').val(0);
	jQuery('#mp_selected_location').val('X');
	/* jQuery('#mp_service_addons').html(''); */
	jQuery('#mp_service_addon_st').val('D');
	jQuery('#mp_selected_datetime').val('');
	jQuery('#mp_datetime_error').hide();
	jQuery('.mp-selected-date-view').addClass('mp-hide');
	
	jQuery('#mp .loader').show();
	
	/* Get Services By Found Location */
	var location_id = jQuery(this).attr('value');
	jQuery('#mp_selected_location').val(location_id);	
	var servicedata = {location_id:location_id,action:'mp_get_location_services'};	
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',			
			data:servicedata,
			success:function(response){					
				jQuery('#mp_services').html(response);	
				jQuery('#mp .loader').hide();	
			}
		});	
});


/* Hide Service Desciption On Click of Close */
jQuery(document).on("click","#close_service_details",function() {
		jQuery(".service-details").removeClass('mp-show');
		jQuery(".service-details").addClass('mp-hide');
		
});

/* Get Service Detail On Select Of Service */
jQuery(document).on('click','#mp_services .select_custom',function(event){
	var ajaxurl = mpmain_obj.plugin_path;
	var sid = jQuery(this).data('sid');			
	var multilmpion_status = mpmain_obj.multilocation_status;
	var zipwise_status = mpmain_obj.zipwise_status;
	var selected_location = jQuery('#mp_selected_location').val();
	jQuery('#mp_service_addon_st').val('D');				
	jQuery('#mp_service_addons').html('');	
	jQuery('#mp_selected_datetime').val('');
	jQuery('#mp_datetime_error').hide();
	jQuery('.mp-selected-date-view').addClass('mp-hide');
	
	jQuery('#mp_service_error').hide();
	if(multilmpion_status=='E' && selected_location=='X'){		
		jQuery('#mp_location_error').show();
		jQuery(".common-selection-main").removeClass('clicked');
		jQuery('html, body').stop().animate({
			'scrollTop': jQuery('#mp_location_error').offset().top - 80
		}, 800, 'swing', function () {});
		return false;
	}
	if(zipwise_status=='E' && selected_location=='X'){
		var Choose_zipcode_msg = mpmain_obj.Choose_zipcode;		
		jQuery('#mp_location_success').hide();
		jQuery('#mp_location_error').show();
		jQuery('#mp_location_error').html(Choose_zipcode_msg);
		jQuery(".common-selection-main").removeClass('clicked');
		
		jQuery('html, body').stop().animate({
			'scrollTop': jQuery('#mp_location_error').offset().top - 80
		}, 800, 'swing', function () {});
		return false;
	}	
	jQuery('#mp .loader').show();
	jQuery('#selected_custom').html(jQuery(this).html());	
		
	var servicedata = {sid:sid,action:'mp_get_service_detail'};
	jQuery('#mp_selected_service').val(sid);
	/* Get Services By Found Location */	
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',			
			data:servicedata,
			success:function(response){	
				var service_details = jQuery.parseJSON(response);
				if(service_details.description!=''){
					jQuery('#mp_service_detail').html(service_details.description);				
					jQuery(".common-selection-main").removeClass('clicked');
					jQuery(".custom-dropdown").slideUp();
					jQuery(".service-details").removeClass('mp-hide');
					jQuery(".service-details").addClass('mp-show');
					if (jQuery("#mp").width() >= 600 && jQuery("#mp").width() < 800){
						jQuery( ".service-duration, .service-price" ).addClass( "active-xs-12" );
					}
				}
				if(service_details.addonsinfo!=''){
					jQuery('#mp_service_addon_st').val('E');				
					jQuery('#mp_service_addons').html(service_details.addonsinfo);				
					jQuery(".common-selection-main").removeClass('clicked');
					jQuery(".custom-dropdown").slideUp();
					jQuery("#mp_service_addons").removeClass('mp-hide');
					jQuery("#mp_service_addons").addClass('mp-show');
				}			
										
			}
		});
		
	/* Get Provider By Service Provider */	
	var servicestaffdata = {sid:sid,action:'mp_get_service_providers'};
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',			
			data:servicestaffdata,
			success:function(response){	
				jQuery('#mp .loader').hide();
				jQuery('#mp_staff_info').html(response);
				if (jQuery("#mp").width() >= 600 && jQuery("#mp").width() < 800){
					jQuery( ".mp-staff-box" ).addClass( "active-sm-6" );
				}
			}
		});	
	
});


/* Select Staff */
jQuery(document).on('click','.mp-staff-box,#cus-select-staff .select_staff',function(event){
	
	jQuery('#mp_service_error').hide();
	jQuery('#mp_staff_error').hide();
	jQuery('#mp_staff_error').addClass('mp-hide');
	
	jQuery('#mp_selected_datetime').val('');
	jQuery('#mp_datetime_error').hide();
	jQuery('.mp-selected-date-view').addClass('mp-hide');
	
	
	jQuery(".service-selection").removeClass('clicked');
	jQuery(".service-dropdown").removeClass('bounceInUp');
	jQuery(".location-selection").removeClass('clicked');
	jQuery(".location-dropdown").removeClass('bounceInUp');	
	
	
	var selserviceid = jQuery('#mp_selected_service').val();	
	if(selserviceid==0){
		var Choose_service_msg = mpmain_obj.Choose_service;
		jQuery('#mp_service_error').html(Choose_service_msg);
		jQuery('#mp_service_error').show();
		jQuery('#mp_service_error').removeClass('mp-hide');
		jQuery('html, body').stop().animate({
			'scrollTop': jQuery('#mp_service_error').offset().top - 80
		}, 800, 'swing', function () {});
		return false;
	}
	
	var staffid = jQuery(this).data('staffid');
	jQuery('#mp_selected_staff').val(staffid);
	jQuery('#selected_custom_staff').html(jQuery(this).html());
	
});



/* Show Provider Time Slot*/
jQuery(document).on('click','.mp-week,.by_default_today_selected', function() {
	if(jQuery(this).hasClass('inactive')){
		return false;
	}
	
	var ajaxurl = mpmain_obj.plugin_path;
	
	var selstaffid = jQuery('#mp_selected_staff').val();	
	if(selstaffid==0){
		jQuery('#mp_staff_error').show();
		jQuery('#mp_staff_error').removeClass('mp-hide');
		jQuery('html, body').stop().animate({
			'scrollTop': jQuery('#mp_staff_error').offset().top - 80
		}, 800, 'swing', function () {});
		return false;
	}else{
		jQuery('#mp .loader').show();
		var calrowid = jQuery(this).data('calrowid');
		var seldate = jQuery(this).data('seldate');
		var calenderdata = {selstaffid:selstaffid,seldate:seldate,action:'mp_get_provider_slots'};
		
		jQuery('.mp-week').each(function(){
			jQuery(this).removeClass('active');				
			
		});
		jQuery('.mp-show-time').each(function(){	
			jQuery(this).removeClass('shown');			
			jQuery(this).removeAttr('style');			
			
		});
		jQuery(this).addClass('active');		
		
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',			
			data:calenderdata,
			success:function(response){	
				jQuery('#mp .loader').hide();
				jQuery('.curr_selected_row'+calrowid).addClass('shown');
				jQuery('.curr_selected_row'+calrowid).css('display','block');
				jQuery('.curr_selected_row'+calrowid+' .mp_day_slots').html(response);
				
			}
		});	
	}	
});

/* Select Time Slot*/
jQuery(document).on('click','.mp_select_slot', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	var slotdate = jQuery(this).data('slot_db_date');
	var slottime = jQuery(this).data('slot_db_time');
	var displaydate = jQuery(this).data('displaydate');
	var displaytime = jQuery(this).data('displaytime');
	
	jQuery('#mp_datetime_error').hide();
	jQuery('.mp-selected-date-view').removeClass('mp-hide');
	jQuery('.time-slot').each(function(){
			jQuery(this).removeClass('mp-slot-selected');				
			
	});
	jQuery(this).addClass('mp-slot-selected');
	jQuery('.mp-selected-date-view').removeClass('mp-hide');
	jQuery('#mp_selected_datetime').val(slotdate+' '+slottime);
	jQuery('.mp-date-selected').html(displaydate);
	jQuery('.mp-time-selected').html(displaytime);
	jQuery('.mp-show-time').hide();

});
	
/* Goto Today */
jQuery(document).on('click','.today_btttn', function(){
	
	var calmonth = jQuery('.previous-date').data('curmonth');
	var calyear = jQuery('.previous-date').data('curyear');
	
	var selmonth = jQuery(this).data('smonth');
	var selyear = jQuery(this).data('syear');
	
	if(selmonth==calmonth && calyear==selyear){
		jQuery('.by_default_today_selected').trigger('click');	
	}else{
		jQuery('#mp .loader').show();
		var ajaxurl = mpmain_obj.plugin_path;
		var calenderdata = {calmonth:calmonth,calyear:calyear,action:'mp_cal_next_prev'};
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',			
			data:calenderdata,
			success:function(response){	
				jQuery('#mp .loader').hide();
				jQuery('.calendar-wrapper').html(response);
				jQuery('.by_default_today_selected').trigger('click');
			}
		});		
	}	
});

/* Get Calender Next Previous Month */
jQuery(document).on('click','.mp_month_change', function() {
	
	jQuery('#mp_selected_datetime').val('');
	jQuery('#mp_datetime_error').hide();
	jQuery('.mp-selected-date-view').addClass('mp-hide');
	
	var ajaxurl = mpmain_obj.plugin_path;
	var calmonth = jQuery(this).data('calmonth');
	var calyear = jQuery(this).data('calyear');
	var calenderdata = {calmonth:calmonth,calyear:calyear,action:'mp_cal_next_prev'};
	jQuery('#mp .loader').show();
	
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',			
		data:calenderdata,
		success:function(response){				
			jQuery('.calendar-wrapper').html(response);
			jQuery('#mp .loader').hide();
		}
	});
});


/********Code For Register booking complete and login and logout***************/


/* Validate Card Fields */
jQuery(document).ready(function() {
	jQuery('input.cc-number').payment('formatCardNumber');
	jQuery('input.cc-cvc').payment('formatCardCVC');
	jQuery('input.cc-exp-month').payment('restrictNumeric');
	jQuery('input.cc-exp-year').payment('restrictNumeric');

});


jQuery(document).on( "click",'.mp-termcondition-area',function() {
		jQuery('.mp_terms_and_condition_error').hide();
});

jQuery(document).on( "click", '#mp_log_out_user', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	var dataString = { 'action':'mp_logout_user' };
	jQuery('#mp .loader').show();
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',
		data:dataString,
		success:function(response){
			jQuery('#mp .loader').hide();
			jQuery('.user-login-main').show();
			jQuery('.user-login-main').show();
			jQuery('.existing-user-success-login-message').hide();
			jQuery('#mp-new-user').trigger('click');
			
			jQuery(".mp-main-left label.custom").removeClass('focus'); 
			jQuery(".mp-main-left .custom-input").removeClass('focus'); 
			jQuery('#new_user_preferred_password').val('');
			jQuery('#new_user_preferred_username').val('');
			jQuery('#new_user_firstname').val('');
			jQuery('#new_user_lastname').val('');
			jQuery('#mp-front-phone').val('');
			jQuery('#new_user_street_address').val('');
			jQuery('#zipcode').val('');
			jQuery('#new_user_city').val('');
			jQuery('#new_user_state').val('');
			jQuery('#new_user_notes').val('');
			
			jQuery('#mp-male').prop('checked',true);
			
		}
	});
});

/* Display Country Code on click flag on phone*/
jQuery(window).load(function(){
	
	if(jQuery("#mp-front-phone").data('ccode') != ''){
		jQuery('.country').removeClass('active');
		jQuery('.country').each(function(){
			if('+'+jQuery(this).data("dial-code") == jQuery("#mp-front-phone").data('ccode')){
				jQuery(this).addClass('active');
				var get_phoneno = jQuery(this).val();
				jQuery('#mp-front-phone').intlTelInput("setNumber", '+'+jQuery(this).data("dial-code")+''+get_phoneno);
			}
		});	
	}else{
		var country_code=jQuery('.country.active').data("dial-code");
		if(country_code === undefined){
			country_code = '1';
		}
		var get_phoneno = jQuery('#mp-front-phone').val();
		if(get_phoneno == ''){
			jQuery('#mp-front-phone').intlTelInput("setNumber", '+'+country_code);
		}
		jQuery("#mp-front-phone").attr('data-ccode','+'+country_code);
	}
});
jQuery(document).on('click','.country',function() {
	var country_code=jQuery(this).data("dial-code");
	var get_phoneno = jQuery('#mp-front-phone').val();
	jQuery('#mp-front-phone').intlTelInput("setNumber", '+'+country_code);
	jQuery("#mp-front-phone").attr('data-ccode','+'+country_code);
});

/* On focus transform label */
jQuery(document).ready(function () { 
	function checkForInput(element) {
	  /* element is passed to the function ^ */
		if(jQuery(element).hasClass('mp-phone-input')){
			var $label = jQuery('.mp-phone-label'); 
		}else{
			var $label = jQuery(element).siblings('label'); 
		}
				
		if (jQuery(element).val().length > 0) {
			$label.addClass('focus');
			jQuery(this).addClass( "focus" );
		} else {
			$label.removeClass('focus');
			jQuery(this).removeClass( "focus" );
		}
		/* user login then show the label at top */
		if (jQuery('.custom-input').val().length > 0) {
			//jQuery(".mp-main-left label.custom").addClass('focus'); 
		}else{
			//jQuery('.label.custom').removeClass('focus');
		}
		/* user login then show the label at top */
		if (jQuery('#mp-front-phone').val().length > 0) {
			jQuery("label.mp-phone-label").addClass('focus'); 
		}else{
			jQuery('#mp-front-phone').removeClass('focus');
		}
		
	}	
	
	/* The lines below are executed on page load */
	jQuery('.custom-input').each(function() {
		checkForInput(this);	
		if (jQuery(this).val().length > 0) {
			jQuery(this).addClass('focus'); 
		}else{
			jQuery(this).removeClass('focus'); 
		}
		
	});

	 /* The lines below (inside) are executed on change & keyup */
	jQuery('.custom-input').on('change keyup', function() {
		checkForInput(this);  
		jQuery(this).addClass( "focus" );	
	});
});

 jQuery(document).ready(function () {
        jQuery('.sub_cat_bang input[type=radio]').is('checked',function () {
          jQuery('.appartment_no').addClass("with_bungalow");
          

        });
    });
	 
	
	
/*****************************************************************************************************************/	
/************************************************* PAM CODE FRONT ************************************************/
/*****************************************************************************************************************/	



jQuery(document).ready(function() {
	var pam_disabledates = mpmain_obj.pam_disabledates;
	var pamstart_date = mpmain_obj.pamstart_date;
	var pamoffdays = pam_disabledates.split('##');
	var pam_disabledatesarr = [];
	
	var pamintialdate = new Date(pamstart_date);
	pamintialdate.setDate(pamintialdate.getDate());
		
	for(var i=0;i<pamoffdays.length;i++){		
		pam_disabledatesarr.push(pamoffdays[i]);		
	}
	jQuery('#front-datepicker input, #from_cab #from_cab_date').datepicker({
		autoclose: true,
		todayHighlight: true,
		startDate: pamintialdate,
		datesDisabled : pam_disabledatesarr,		
	});
});

jQuery(document).ready(function() {
	jQuery('.selectpicker1, .selectpicker2, .selectpicker').selectpicker();
});


/* Get Destination Cities */	
 jQuery(document).on( "change",'#pam_sc_city', function() {	
	var ajaxurl = mpmain_obj.plugin_path;
	var source_city = jQuery(this).val();
	
	
	var city_type = jQuery(this).data('ct');	
	if(source_city==''){
		jQuery('#pam_sc_city_error').show();
	}else{
		jQuery('#pam_sc_city_error').hide();	
	}
	
	if(source_city!=''){
		var dataString = { 
				'source_city':source_city,
				'city_type':city_type,				
				'action':'get_destination_city' 
			};
		jQuery('#mp .loader').show();
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',
			data:dataString,
			success:function(response){
				jQuery('#mp .loader').hide();
				jQuery('#pam_dc_city').html(response);
				jQuery('.selectpicker2').selectpicker('refresh');
				jQuery('.destination_ct').hide();
			}
		});
	}
});	

jQuery(document).on( "change",'#pam_dc_city', function() {
	/* var destination_city = jQuery(this).val(); */
	if(jQuery(this).val()=='Other'){
		jQuery('.destination_ct').show();	
	}else{
		var destination_city = jQuery(this).val();
		jQuery('.destination_ct').hide();
	}	
	if(destination_city==''){
		jQuery('#pam_dc_city_error').show();
	}else{
		jQuery('#pam_dc_city_error').hide();	
	}
});

/* Go To Back */
jQuery(document).on('click','#btn-first-step', function() {
	
	jQuery('#mp .loader').show();
	
	if(jQuery('.mp-booking-step #second').hasClass('active')){
		var requested_step = 'first';
	}
	
	if(requested_step=='first'){
		jQuery('#mp_first_step').show();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').hide();
		jQuery('#mp_second_step').hide();
	}
	jQuery('#mp .loader').hide();
	
	
});
jQuery(document).on('click','#first', function() {
	
	jQuery('#mp .loader').show();
	
	if(jQuery('.mp-booking-step #second').hasClass('active')){
		var requested_step = 'first';
	}
	
	if(requested_step=='first'){
		jQuery('#mp_first_step').show();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').hide();
		jQuery('#mp_second_step').hide();
	}
	jQuery('#mp .loader').hide();
	
	
});
jQuery(document).on('click','#btn-second-step', function() {
	
	jQuery('#mp .loader').show();
	
	if(jQuery('.mp-booking-step #fourth').hasClass('active')){
		var requested_step = 'second';
	}
	
	if(requested_step=='first'){
		jQuery('#mp_first_step').hide();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').hide();
		jQuery('#mp_second_step').show();
	}
	jQuery('#mp .loader').hide();
	
	
});
jQuery(document).on('click','#second', function() {
	
	jQuery('#mp .loader').show();
	
	if(jQuery('.mp-booking-step #fourth').hasClass('active')){
		var requested_step = 'second';
	}
	
	if(requested_step=='first'){
		jQuery('#mp_first_step').hide();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').hide();
		jQuery('#mp_second_step').show();
	}
	jQuery('#mp .loader').hide();
	
});

/*
jQuery(document).on('click','#fourth', function() {
	
	jQuery('#mp .loader').show();
	
	if(jQuery('.mp-booking-step #second').hasClass('active')){
		var requested_step = 'first';
	}
	
	if(requested_step=='first'){
		jQuery('#mp_first_step').hide();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').show();
		jQuery('#mp_second_step').hide();
	}
	jQuery('#mp .loader').hide();
	
	
});*/

/* Packer & Movers 1st Step */
jQuery(document).on('click','#second', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	if(jQuery('.mp-booking-step #first').hasClass('active')){
		var selectcategory = jQuery('input[name="main_stuff_selection"]:checked').val();	
		var source_city = jQuery('#pam_sc_city').val();
		var destination_city = jQuery('#pam_dc_city').val();
		var desctination_city_text="";
		if(destination_city=='Other'){
			desctination_city_text = jQuery('#destination_ct_input').val();
		}else{
			desctination_city_text = jQuery('#pam_dc_city').val();
		}
		var selected_date = jQuery('#pam_bookingdate').val();
		if(source_city=='' && destination_city==''){
			jQuery('#pam_sc_city_error').show();
			jQuery('#pam_dc_city_error').show();
			return false;		
		}
		if(source_city==''){
			jQuery('#pam_sc_city_error').show();
			return false;
		}
		if(destination_city==''){
			jQuery('#pam_dc_city_error').show();
			return false;
		}
				
		var current_stepdata = {'selectcategory':selectcategory,'source_city':source_city,'destination_city':destination_city,'desctination_city_text':desctination_city_text,'selected_date':selected_date,action:'set_stepone_session'};		
		var requested_step = 'second';
	}
	
	/* Set First Step Values In Session */
	jQuery('#mp .loader').show();	
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',			
		data:current_stepdata,
		success:function(response){	
			jQuery('#mp .loader').hide();
		}
	});	
	
	/* Load Service Articles */
	var get_second_stepdata = {'selectcategory':selectcategory,action:'load_service_articles'};		
	jQuery('#mp .loader').show();	
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',			
		data:get_second_stepdata,
		success:function(response){			
			jQuery('#mp .loader').hide();
			jQuery('#mp_service_addons').html(response);
			
		}
	});	
	
	
	
	if(selectcategory=='1'){
		jQuery('.pam_home_service_ls').show();
		jQuery('.pam_home_service_rs').show();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_office_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_pates_service_form').hide();
		jQuery('.pam_other_service').hide();
		jQuery('.pam_pets_service').hide();
		jQuery('.caps_form').hide();
	}
	if(selectcategory=='2'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_office_service').show();		
		jQuery('.pam_pates_service_form').hide();	
		jQuery('.pam_other_service').hide();	
		jQuery('.pam_pets_service').hide();
		jQuery('.caps_form').hide();		
		
	}
	if(selectcategory=='3'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').show();
		jQuery('.pam_office_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_pates_service_form').hide();
		jQuery('.pam_other_service').hide();
jQuery('.pam_pets_service').hide();
jQuery('.caps_form').hide();
	}
	if(selectcategory=='4'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_pets_service').show();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_office_service').hide();
		jQuery('#mp_service_addons').hide();
		jQuery('.pam_other_service').hide();
		jQuery('.caps_form').hide();
	}
	if(selectcategory=='5'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_pets_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_office_service').hide();
		jQuery('#mp_service_addons').hide();
		jQuery('.pam_other_service').hide();
		jQuery('.caps_form').hide();
	}
	if(selectcategory=='6'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_pets_service').hide();
		jQuery('.other_service_ls').show();
		jQuery('.pam_office_service').hide();
		jQuery('#mp_service_addons').hide();
		jQuery('.pam_other_service').show();
		jQuery('.caps_form').hide();
	}
	
	if(requested_step=='second'){
		jQuery('#mp_first_step').hide();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').hide();
		jQuery('#mp_second_step').show();		
	}
	
});
jQuery(document).on('click','#btn-second-step', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	if(jQuery('.mp-booking-step #first').hasClass('active')){
		var selectcategory = jQuery('input[name="main_stuff_selection"]:checked').val();	
		var source_city = jQuery('#pam_sc_city').val();
		var destination_city = jQuery('#pam_dc_city').val();
		var desctination_city_text="";
		if(destination_city=='Other'){
			desctination_city_text = jQuery('#destination_ct_input').val();
		}else{
			desctination_city_text = jQuery('#pam_dc_city').val();
		}
		var selected_date = jQuery('#pam_bookingdate').val();
		if(source_city=='' && destination_city==''){
			jQuery('#pam_sc_city_error').show();
			jQuery('#pam_dc_city_error').show();
			return false;		
		}
		if(source_city==''){
			jQuery('#pam_sc_city_error').show();
			return false;
		}
		if(destination_city==''){
			jQuery('#pam_dc_city_error').show();
			return false;
		}
				
		var current_stepdata = {'selectcategory':selectcategory,'source_city':source_city,'destination_city':destination_city,'desctination_city_text':desctination_city_text,'selected_date':selected_date,action:'set_stepone_session'};		
		var requested_step = 'second';
	}
	
	/* Set First Step Values In Session */
	jQuery('#mp .loader').show();	
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',			
		data:current_stepdata,
		success:function(response){	
			jQuery('#mp .loader').hide();
		}
	});	
	
	/* Load Service Articles */
	var get_second_stepdata = {'selectcategory':selectcategory,action:'load_service_articles'};		
	jQuery('#mp .loader').show();	
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',			
		data:get_second_stepdata,
		success:function(response){			
			jQuery('#mp .loader').hide();
			jQuery('#mp_service_addons').html(response);
			
		}
	});	
	
	
	
	if(selectcategory=='1'){
		jQuery('.pam_home_service_ls').show();
		jQuery('.pam_home_service_rs').show();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_office_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_pates_service_form').hide();
		jQuery('.pam_other_service').hide();
		jQuery('.pam_pets_service').hide();
		jQuery('.caps_form').hide();
	}
	if(selectcategory=='2'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_office_service').show();		
		jQuery('.pam_pates_service_form').hide();	
		jQuery('.pam_other_service').hide();	
		jQuery('.pam_pets_service').hide();
		jQuery('.caps_form').hide();		
		
	}
	if(selectcategory=='3'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').show();
		jQuery('.pam_office_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_pates_service_form').hide();
		jQuery('.pam_other_service').hide();
jQuery('.pam_pets_service').hide();
jQuery('.caps_form').hide();
	}
	if(selectcategory=='4'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_pets_service').show();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_office_service').hide();
		jQuery('#mp_service_addons').hide();
		jQuery('.pam_other_service').hide();
		jQuery('.caps_form').hide();
	}
	if(selectcategory=='5'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_pets_service').hide();
		jQuery('.other_service_ls').hide();
		jQuery('.pam_office_service').hide();
		jQuery('#mp_service_addons').hide();
		jQuery('.pam_other_service').hide();
		jQuery('.caps_form').hide();
	}
	if(selectcategory=='6'){
		jQuery('.pam_home_service_ls').hide();
		jQuery('.pam_home_service_rs').hide();
		jQuery('.pam_vehicle_service').hide();
		jQuery('.pam_pets_service').hide();
		jQuery('.other_service_ls').show();
		jQuery('.pam_office_service').hide();
		jQuery('#mp_service_addons').hide();
		jQuery('.pam_other_service').show();
		jQuery('.caps_form').hide();
	}
	
	if(requested_step=='second'){
		jQuery('#mp_first_step').hide();
		jQuery('#mp_third_step').hide();
		jQuery('#mp_fourth_step').hide();
		jQuery('#mp_second_step').show();		
	}
	
});
/* pats select service statis options  */
jQuery(document).on('change','.pets_service', function() {
	  jQuery('.pets_service_gender').show();
	  jQuery('.mp_service_addons').hide();
});
/* pats select gender options  */
jQuery(document).on('change','.pets_service_gen', function() {
	  jQuery('.mp_service_addons').hide();
	  /* jQuery('.pam_pates_service_form').show(); */
});

/* Packer & Movers commercial Service Loading Options */
jQuery(document).on('change','.commercial_main_stuff_selection', function() {
	  jQuery('.pam_home_service_ls').hide();
	  jQuery('.pam_home_service_rs').hide();

});
/* Packer & Movers other Service Options */
jQuery(document).on('change','.other_services', function() {
	  jQuery('.pam_home_service_ls').hide();
	  jQuery('.pam_home_service_rs').hide();
	 /*  jQuery('.other_service_ls').show(); */

});

/* Packer & Movers Home Service Loading Options */
jQuery(document).on('change','.home_service_loading', function() {
	var home_loading_type = jQuery(this).val();
	if(home_loading_type == 'Appartment'){	
	  jQuery('.appartment_loading_section').show();
	  jQuery('.bungalow_loading_section').hide();		  
	}else if(home_loading_type == 'Bungalow'){
	  jQuery('.bungalow_loading_section').show();
	  jQuery('.appartment_loading_section').hide();
	}		 	
});
/* Packer & Movers Home Service unLoading Options */
jQuery(document).on('change','.home_service_unloading', function() {
	var home_loading_type = jQuery(this).val();
	if(home_loading_type == 'Appartment'){
	  jQuery('.appartment_unloading_section').show();
	  jQuery('.bungalow_unloading_section').hide();			  
	}else if(home_loading_type == 'Bungalow'){
	  jQuery('.bungalow_unloading_section').show();
	  jQuery('.appartment_unloading_section').hide();			  
	}		 	
});
/* Packer & Movers Vehicle Service Options */
jQuery(document).on('change','.vehicle_service', function() {
	var vehicle_type = jQuery(this).val();
	if(vehicle_type == 'Car'){
	  jQuery('.vehicle_service_car').show();
	  jQuery('.vehicle_service_bike').show();	
	  jQuery('.append_vehicle').show();
	  jQuery('.no_of_vehicle').val("");
	}else if(vehicle_type == 'Bike'){
	  jQuery('.vehicle_service_bike').show();
	  jQuery('.vehicle_service_car').hide();
	  jQuery('.append_vehicle').hide();
	  jQuery('.no_of_vehicle').val("");
	}		 	
});
jQuery(document).on('change','.pets_service',function(){

var value = jQuery(this).prop('checked');
var data_did = jQuery(this).data('div_id');

if(value){ 
jQuery('#app'+data_did).show();

}else{
jQuery('#app'+data_did).hide();
}

});
/* Show Addon Quantity */
	jQuery(document).on('click','.addon-checkbox',function(){
		var saddonid = jQuery(this).data('saddonid');
		jQuery('.mp-addon-count'+saddonid).toggle();
		var value = jQuery(this).prop('checked');
	});
/* Addon Quantity Increment/Decrement */
jQuery(document).on('click','.mp_addonqty', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	var addon_id = jQuery(this).data('addonid');
	var addon_qty_action = jQuery(this).data('qtyaction');
	var addon_maxqty = jQuery(this).data('addonmax');	
	var currentqtyvalue = jQuery('#addonqty_'+addon_id).val();
	if(addon_qty_action=='minus'){
		if(parseInt(currentqtyvalue)>1){
			jQuery('#addonqty_'+addon_id).val(parseInt(currentqtyvalue)-1);
		}
	}else{
		if(parseInt(currentqtyvalue)<parseInt(addon_maxqty)){
			jQuery('#addonqty_'+addon_id).val(parseInt(currentqtyvalue)+1);
		}
	}
});
/* Hide Error Messages */
jQuery(document).on('click','.custom-input', function() {
	jQuery('#bungalow_loading_floor_err').hide();
	jQuery('#appartment_loading_floor_err').hide();
	jQuery('#bungalow_unloading_floor_err').hide();
	jQuery('#appartment_unloading_floor_err').hide();
	jQuery('#bungalow_loading_type_err').hide();
	jQuery('#bungalow_unloading_type_err').hide();
	jQuery('#office_service_area_err').hide();
	jQuery('#vehicle_service_no_err').hide();
	jQuery('#vehicle_service_car_type_err').hide();
	jQuery('#pets_name_errs').hide();
	jQuery('#pets_breed_errs').hide();
	jQuery('#pets_age_errs').hide();
	jQuery('#pets_weight_errs').hide();
	jQuery('#other_name_errs').hide();
	jQuery('#other_floor_errs').hide();
	jQuery('#other_article_errs').hide();
	jQuery('#other_description_errs').hide();
});

/* Proceed TO Step 3rd */
jQuery(document).on('click','#fourth', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	var additional_info_id = mpmain_obj.pam_enabled_additionalinfo;
	
	var service_info = {};
	var loading_info = {};
	var unloading_info = {};
	var cabs_info = {};
	var additional_info = {};
	var articles_info = {};
	var other_info={};
	var vehicle_car_typess=[];
	var vehicle_no_info_vehicle=[];
	
	
	
	if(jQuery('.pam_home_service_ls').is(':visible')){
		var home_loading_type = jQuery('input[name="home_service_loading"]:checked').val();
		var home_unloading_type = jQuery('input[name="home_service_unloading"]:checked').val();
		var home_service_err = ''; 
		
		if(home_loading_type=='Bungalow'){
			var bungalow_loading_floor = jQuery('#bungalow_loading_floor').val();
			var bungalow_loading_type = jQuery('#bungalow_loading_type').val();			
			service_info['service'] = 'Home';
			service_info['loading_floor_number'] = bungalow_loading_floor;
			service_info['loading_bunglow_type'] = bungalow_loading_type;
			
			if(bungalow_loading_floor==''){
				home_service_err +='Y';
				jQuery('#bungalow_loading_floor_err').show();
			}
			if(bungalow_loading_type==''){
				home_service_err +='Y';
				jQuery('#bungalow_loading_type_err').show();
			}
		}else{
			var appartment_loading_floor = jQuery('#appartment_loading_floor').val();
			service_info['service'] = 'Home';
			service_info['loading_floor_number'] = appartment_loading_floor;
			service_info['loading_bunglow_type'] = '';
			
			if(appartment_loading_floor==''){
				home_service_err +='Y';
				jQuery('#appartment_loading_floor_err').show();
			}
		}
		if(home_unloading_type=='Bungalow'){
			var bungalow_unloading_floor = jQuery('#bungalow_unloading_floor').val();
			var bungalow_unloading_type = jQuery('#bungalow_unloading_type').val();
			service_info['unloading_floor_number'] = bungalow_unloading_floor;
			service_info['unloading_bunglow_type'] = bungalow_unloading_type;
			
			if(bungalow_unloading_floor==''){
				home_service_err +='Y';
				jQuery('#bungalow_unloading_floor_err').show();
			}
			if(bungalow_unloading_type==''){
				home_service_err +='Y';
				jQuery('#bungalow_unloading_type_err').show();
			}
		}else{
			var appartment_unloading_floor = jQuery('#appartment_unloading_floor').val();
			service_info['unloading_floor_number'] = appartment_unloading_floor;
			service_info['unloading_bunglow_type'] = '';
			
			if(appartment_unloading_floor==''){
				home_service_err +='Y';
				jQuery('#appartment_unloading_floor_err').show();
			}
		}
		if(home_service_err!=''){
			return false;
		}
		
	}
	/* Other Service Validate */
	if(jQuery('.pam_other_service').is(':visible')){
		var other_service_err = '';
   		var other_service_article = jQuery("input[name='other_service_article[]']").map(function(){return jQuery(this).val();}).get();
		
		var other_service_description = jQuery('#other_service_description').val();
		var other_service_floor_no = jQuery('#other_service_floor_no').val();
			
			articles_info['other_service_article'] = other_service_article;
			service_info['service'] = 'Other';
			service_info['other_service_description'] = other_service_description;
			service_info['other_service_floor_no'] = other_service_floor_no;

				if(other_service_floor_no == ''){
					other_service_err +='Y';
					jQuery('#other_floor_errs').show();
				} 
				if(other_service_article == ''){
					other_service_err +='Y';
					jQuery('#other_article_errs').show();
				}
				if(other_service_description == ''){
					other_service_err +='Y';
					jQuery('#other_description_errs').show();
				} 						
				if(other_service_err!=''){
					return false;
				}					
	}
	
	
	/* Office Service Validate */
	if(jQuery('.pam_office_service').is(':visible')){
		var office_service_err = ''; 	
		var office_service_area = jQuery('#office_service_area').val();
		service_info['service'] = 'office';
		service_info['office_area'] = office_service_area;
		if(office_service_area=='' || isNaN(office_service_area)){
			office_service_err +='Y';
			jQuery('#office_service_area_err').show();
		}	
		if(office_service_err!=''){
			return false;
		}
	}
	/* Vehicle Service Validate */
	if(jQuery('.pam_vehicle_service').is(':visible')){
		jQuery('.vehicle_appdivs').each(function(){
		vehicle_car_typess.push(jQuery(this).find('.location-selection-cartype .data-list .mp-value').html());
		vehicle_no_info_vehicle.push(jQuery(this).find('.no_of_vehicle').val());
		 /* myarray.push($("#drop").val()); */
		});
				
		var vehicle_service_type = jQuery('input[name="vehicle_service"]:checked').val();
		var vehicle_service_err = ''; 	
		var vehicle_service_quantity = jQuery('#vehicle_service_quantity').val();
		service_info['service'] = 'Vehicle';
		service_info['vehicle_qty'] = vehicle_service_quantity;
		
		if(vehicle_service_quantity=='' || isNaN(vehicle_service_quantity)){
			vehicle_service_err +='Y';
			jQuery('#vehicle_service_no_err').show();
		}
		
		if(vehicle_service_type=='Car'){
			var vehicle_service_car_type = jQuery('#vehicle_service_car_type').val();
			/* service_info['vehicle_type'] = vehicle_service_car_type; */
			service_info['vehicle_type'] = vehicle_car_typess;
			service_info['no_of_vehicle'] = vehicle_no_info_vehicle;
			if(vehicle_service_car_type==''){
				vehicle_service_err +='Y';
				jQuery('#vehicle_service_car_type_err').show();
			}
		}
			
		if(vehicle_service_err!=''){
			return false;
		}
	}
	/* Pets Service Validate */
	if(jQuery('.pam_pets_service').is(':visible')){
			/* var pet_multiple_div  = {}; */
			var count_var  = 0;
			
		 jQuery.each(jQuery(".pets_service"), function(){ 
			var value = jQuery(this).prop('checked');
			if(value){
				var pet_multiple_div  = {};
				var pets_service_err = ''; 
				var data_did = jQuery(this).data('div_id');	
				
				var pet_service_value = jQuery(this).val();
				var pet_service_error = "pet_service_"+pet_service_value.toLowerCase();
				var pets_service_gen = jQuery('.pets_service_gen').val();
				var pet_name = jQuery('#pet_name'+data_did).val();
				var pet_breed = jQuery('#pet_breed'+data_did).val();
				var pets_gender = jQuery('input[name="pets_gender_1'+data_did+'"]:checked').val();
				var pet_age = jQuery('#pet_age'+data_did).val();
				var pets_age_radio = jQuery('#age_value'+data_did).val();
			
				var pet_weight = jQuery('#pet_weight'+data_did).val();
				var pet_weight_gen = jQuery('#weight_value'+data_did).val();
				
				pet_multiple_div['pets_service_gen'] = pets_service_gen;
				pet_multiple_div['pets_service'] = pet_service_value;
				pet_multiple_div['pet_name'] = pet_name;
				pet_multiple_div['pet_breed'] = pet_breed;
				pet_multiple_div['pets_gender'] = pets_gender;
				pet_multiple_div['pet_age'] = pet_age;
				pet_multiple_div['pets_age_radio'] = pets_age_radio;
				pet_multiple_div['pet_weight'] = pet_weight;
				pet_multiple_div['pet_weight_gen'] = pet_weight_gen;			
				pet_multiple_div['service'] = 'Pets';
				
				/* var intRegex = "/^\d+$/"; */
					
				if(pet_name == ''){
					pets_service_err +='Y';
					jQuery('#pets_name_errs'+pet_service_error).show();
				}
				if(pet_breed==''){
					pets_service_err +='Y';
					jQuery('#pets_breed_errs'+pet_service_error).show();
				}
				if(pet_age=='' && !(intRegex.test(pet_age))){
				/* if(pet_age==''){ */
					pets_service_err +='Y';
					jQuery('#pets_age_errs'+pet_service_error).show();
				}
				if(pet_weight=='' && !(intRegex.test(pet_age))){
					pets_service_err +='Y';
					jQuery('#pets_weight_errs'+pet_service_error).show();
				}			
				if(pets_service_err!=''){
					return false;
				} 
			count_var++;
			var service_count = "pet_info"+count_var;
			/* console.log(pet_multiple_div); */
			service_info[service_count] = pet_multiple_div;
			/* service_info.push(pet_multiple_div); */				
			}
			
		});		
		console.log(service_info);		
	}
jQuery(document).ready(function(){
	jQuery("#pet_namepet_service_dog").keyup(function(){
		jQuery("#pets_name_errspet_service_dog").hide();
	});
	jQuery("#pet_breedpet_service_dog").keyup(function(){
		jQuery("#pets_breed_errspet_service_dog").hide();
	});
	jQuery("#pet_agepet_service_dog").keyup(function(){
		jQuery("#pets_age_errspet_service_dog").hide();
	});
	jQuery("#pet_weightpet_service_dog").keyup(function(){
		jQuery("#pets_weight_errspet_service_dog").hide();
	});
	
	
	jQuery("#pet_namepet_service_cat").keyup(function(){
		jQuery("#pets_name_errspet_service_cat").hide();
	});
	jQuery("#pet_breedpet_service_cat").keyup(function(){
		jQuery("#pets_breed_errspet_service_cat").hide();
	});
	jQuery("#pet_agepet_service_cat").keyup(function(){
		jQuery("#pets_age_errspet_service_cat").hide();
	});
	jQuery("#pet_weightpet_service_cat").keyup(function(){
		jQuery("#pets_weight_errspet_service_cat").hide();
	});
	
	
	jQuery("#pet_namepet_service_birds").keyup(function(){
		jQuery("#pets_name_errspet_service_birds").hide();
	});
	jQuery("#pet_breedpet_service_birds").keyup(function(){
		jQuery("#pets_breed_errspet_service_birds").hide();
	});
	jQuery("#pet_agepet_service_birds").keyup(function(){
		jQuery("#pets_age_errspet_service_birds").hide();
	});
	jQuery("#pet_weightpet_service_birds").keyup(function(){
		jQuery("#pets_weight_errspet_service_birds").hide();
	});
	
	
	jQuery("#pet_namepet_service_other").keyup(function(){
		jQuery("#pets_name_errspet_service_other").hide();
	});
	jQuery("#pet_breedpet_service_other").keyup(function(){
		jQuery("#pets_breed_errspet_service_other").hide();
	});
	jQuery("#pet_agepet_service_other").keyup(function(){
		jQuery("#pets_age_errspet_service_other").hide();
	});
	jQuery("#pet_weightpet_service_other").keyup(function(){
		jQuery("#pets_weight_errspet_service_other").hide();
	});
	
	
});

	
	/* Validate Loading/Unloading Address Section */
	jQuery('#pam_loading_unloading_address').validate({
		rules: {
			pam_loading_street_address: { required: true },
			pam_loading_city: { required: true },
			pam_loading_state: { required: true },
			/* pam_loading_country: { required: true }, */
			pam_unloading_street_address: { required: true },
			pam_unloading_city: { required: true },
			pam_unloading_state: { required: true }
			/* pam_unloading_country: { required: true }			 */
			},
		messages: {
			pam_loading_street_address: { required: mpmain_error_obj.pam_loading_street_address },
			pam_loading_city: { required: mpmain_error_obj.pam_loading_city },
			pam_loading_state: { required: mpmain_error_obj.pam_loading_state },
			/* pam_loading_country: { required: mpmain_error_obj.pam_loading_country }, */
			pam_unloading_street_address: { required: mpmain_error_obj.pam_loading_street_address },
			pam_unloading_city: { required: mpmain_error_obj.pam_loading_city },
			pam_unloading_state: { required: mpmain_error_obj.pam_loading_state }
			/* pam_unloading_country: { required: mpmain_error_obj.pam_loading_country } */
			}
	});
	if(!jQuery('#pam_loading_unloading_address').valid()){
		return false;
	}
	loading_info['pam_loading_street_address'] = jQuery('#pam_loading_street_address').val();
	loading_info['pam_loading_city'] = jQuery('#pam_loading_city').val();
	loading_info['pam_loading_state'] = jQuery('#pam_loading_state').val();
	/* loading_info['pam_loading_country'] = jQuery('#pam_loading_country').val(); */
	
	
	
	/* cabs Info by trupal added by pradeep*/
	if(jQuery('#cab_want').is(':checked')){
	var cab_lable_err = ""
	var to_cab_lable_err = ""
	var pam_street_address_cab = cabs_info['pam_street_address_cab']=jQuery('#pam_loading_street_address_cab').val();
	var pam_loading_city_cab = cabs_info['pam_city_cab'] = jQuery('#pam_loading_city_cab').val();
	var pam_loading_state_cab = cabs_info['pam_state_cab'] = jQuery('#pam_loading_state_cab').val();
	var pam_member_cab = cabs_info['pam_member_cab'] = jQuery('#pam_member_cab').val();
	
	var cab_date=cabs_info['cab_date'] = jQuery('#from_cab_date').val();
	var to_pam_street_address_cab = cabs_info['to_pam_street_address_cab']=jQuery('#to_pam_loading_street_address_cab').val();
	var to_pam_loading_city_cab = cabs_info['to_pam_city_cab'] = jQuery('#to_pam_loading_city_cab').val();
	var to_pam_loading_state_cab = cabs_info['to_pam_state_cab'] = jQuery('#to_pam_loading_state_cab').val();
		
			if(pam_street_address_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_address_err').show();
			}if(pam_loading_city_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_city_err').show();
			}if(pam_loading_state_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_state_err').show();
			}if(pam_member_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_member_err').show();
			}
			if(to_pam_street_address_cab == ''){
				to_cab_lable_err +='Y';
				jQuery('#to_cab_address_err').show();
			}if(to_pam_loading_city_cab == ''){
				to_cab_lable_err +='Y';
				jQuery('#to_cab_city_err').show();
			}if(to_pam_loading_state_cab == ''){
				to_cab_lable_err +='Y';
				jQuery('#to_cab_state_err').show();
			}				
			if(cab_lable_err!=''){
				return false;
			} 
			if(to_cab_lable_err!=''){
				return false;
			} 
		}
	
	
	
	unloading_info['pam_unloading_street_address'] = jQuery('#pam_unloading_street_address').val();
	unloading_info['pam_unloading_city'] = jQuery('#pam_unloading_city').val();
	unloading_info['pam_unloading_state'] = jQuery('#pam_unloading_state').val();
	/* unloading_info['pam_unloading_country'] = jQuery('#pam_unloading_country').val(); */
	
	
	/* Fetch Additional Info */
	if(additional_info_id!=''){
		var additional_info_ids = additional_info_id.split(',');
		for(var ids=0;ids<additional_info_ids.length;ids++){
			var additional_info_id = additional_info_ids[ids];
			
			if(jQuery('#additional_info'+additional_info_id).is(':checked')){				
				additional_info[additional_info_id]	= 'Y';
			}else{
				additional_info[additional_info_id]	= 'N';
			}
		}
	}
	
	/* Fetch Service Articles Info */ 
	if(jQuery('#mp_service_addons').is(':visible')){
		jQuery('.addon-checkbox').each(function(){
			var curraddonid = jQuery(this).data('saddonid');
			if(jQuery(this).is(':checked')){
				articles_info[curraddonid] = jQuery('#addonqty_'+curraddonid).val();
			}
		});
	}
	
	var current_stepdata ={'service_info':service_info,'loading_info':loading_info,'cabs_info':cabs_info,'unloading_info':unloading_info,'articles_info':articles_info,'additional_info':additional_info,action:'set_steptwo_session'}
	
	/* Set Second Step Values In Session */
	jQuery("input").blur(function(){
		jQuery('#compare_error_msg').hide();
		});
		if(loading_info['pam_loading_street_address']==unloading_info['pam_unloading_street_address']){
		jQuery('#compare_error_msg').show();
		return false;
		}else{
		jQuery('#mp .loader').show();	
		jQuery.ajax({
		type:"POST",
		url : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',	
		data:current_stepdata,
		success:function(response){	
		jQuery('#mp .loader').hide();
		}
		});	
		}	
		jQuery('#mp_fourth_step').show();
		jQuery('#mp_second_step').hide(); 


});

jQuery(document).on('click','#btn-fourth-step', function() {
	var ajaxurl = mpmain_obj.plugin_path;
	var additional_info_id = mpmain_obj.pam_enabled_additionalinfo;
	
	var service_info = {};
	var loading_info = {};
	var unloading_info = {};
	var cabs_info = {};
	var additional_info = {};
	var articles_info = {};
	var other_info={};
	var vehicle_car_typess=[];
	var vehicle_no_info_vehicle=[];
	
	
	
	if(jQuery('.pam_home_service_ls').is(':visible')){
		var home_loading_type = jQuery('input[name="home_service_loading"]:checked').val();
		var home_unloading_type = jQuery('input[name="home_service_unloading"]:checked').val();
		var home_service_err = ''; 
		
		if(home_loading_type=='Bungalow'){
			var bungalow_loading_floor = jQuery('#bungalow_loading_floor').val();
			var bungalow_loading_type = jQuery('#bungalow_loading_type').val();			
			service_info['service'] = 'Home';
			service_info['loading_floor_number'] = bungalow_loading_floor;
			service_info['loading_bunglow_type'] = bungalow_loading_type;
			
			if(bungalow_loading_floor==''){
				home_service_err +='Y';
				jQuery('#bungalow_loading_floor_err').show();
			}
			if(bungalow_loading_type==''){
				home_service_err +='Y';
				jQuery('#bungalow_loading_type_err').show();
			}
		}else{
			var appartment_loading_floor = jQuery('#appartment_loading_floor').val();
			service_info['service'] = 'Home';
			service_info['loading_floor_number'] = appartment_loading_floor;
			service_info['loading_bunglow_type'] = '';
			
			if(appartment_loading_floor==''){
				home_service_err +='Y';
				jQuery('#appartment_loading_floor_err').show();
			}
		}
		if(home_unloading_type=='Bungalow'){
			var bungalow_unloading_floor = jQuery('#bungalow_unloading_floor').val();
			var bungalow_unloading_type = jQuery('#bungalow_unloading_type').val();
			service_info['unloading_floor_number'] = bungalow_unloading_floor;
			service_info['unloading_bunglow_type'] = bungalow_unloading_type;
			
			if(bungalow_unloading_floor==''){
				home_service_err +='Y';
				jQuery('#bungalow_unloading_floor_err').show();
			}
			if(bungalow_unloading_type==''){
				home_service_err +='Y';
				jQuery('#bungalow_unloading_type_err').show();
			}
		}else{
			var appartment_unloading_floor = jQuery('#appartment_unloading_floor').val();
			service_info['unloading_floor_number'] = appartment_unloading_floor;
			service_info['unloading_bunglow_type'] = '';
			
			if(appartment_unloading_floor==''){
				home_service_err +='Y';
				jQuery('#appartment_unloading_floor_err').show();
			}
		}
		if(home_service_err!=''){
			return false;
		}
		
	}
	/* Other Service Validate */
	if(jQuery('.pam_other_service').is(':visible')){
		var other_service_err = '';
   		var other_service_article = jQuery("input[name='other_service_article[]']").map(function(){return jQuery(this).val();}).get();
		
		var other_service_description = jQuery('#other_service_description').val();
		var other_service_floor_no = jQuery('#other_service_floor_no').val();
			
			articles_info['other_service_article'] = other_service_article;
			service_info['service'] = 'Other';
			service_info['other_service_description'] = other_service_description;
			service_info['other_service_floor_no'] = other_service_floor_no;

				if(other_service_floor_no == ''){
					other_service_err +='Y';
					jQuery('#other_floor_errs').show();
				} 
				if(other_service_article == ''){
					other_service_err +='Y';
					jQuery('#other_article_errs').show();
				}
				if(other_service_description == ''){
					other_service_err +='Y';
					jQuery('#other_description_errs').show();
				} 						
				if(other_service_err!=''){
					return false;
				}					
	}
	
	
	/* Office Service Validate */
	if(jQuery('.pam_office_service').is(':visible')){
		var office_service_err = ''; 	
		var office_service_area = jQuery('#office_service_area').val();
		service_info['service'] = 'office';
		service_info['office_area'] = office_service_area;
		if(office_service_area=='' || isNaN(office_service_area)){
			office_service_err +='Y';
			jQuery('#office_service_area_err').show();
		}	
		if(office_service_err!=''){
			return false;
		}
	}
	/* Vehicle Service Validate */
	if(jQuery('.pam_vehicle_service').is(':visible')){
		jQuery('.vehicle_appdivs').each(function(){
		vehicle_car_typess.push(jQuery(this).find('.location-selection-cartype .data-list .mp-value').html());
		vehicle_no_info_vehicle.push(jQuery(this).find('.no_of_vehicle').val());
		 /* myarray.push($("#drop").val()); */
		});
				
		var vehicle_service_type = jQuery('input[name="vehicle_service"]:checked').val();
		var vehicle_service_err = ''; 	
		var vehicle_service_quantity = jQuery('#vehicle_service_quantity').val();
		service_info['service'] = 'Vehicle';
		service_info['vehicle_qty'] = vehicle_service_quantity;
		
		if(vehicle_service_quantity=='' || isNaN(vehicle_service_quantity)){
			vehicle_service_err +='Y';
			jQuery('#vehicle_service_no_err').show();
		}
		
		if(vehicle_service_type=='Car'){
			var vehicle_service_car_type = jQuery('#vehicle_service_car_type').val();
			/* service_info['vehicle_type'] = vehicle_service_car_type; */
			service_info['vehicle_type'] = vehicle_car_typess;
			service_info['no_of_vehicle'] = vehicle_no_info_vehicle;
			if(vehicle_service_car_type==''){
				vehicle_service_err +='Y';
				jQuery('#vehicle_service_car_type_err').show();
			}
		}
			
		if(vehicle_service_err!=''){
			return false;
		}
	}
	/* Pets Service Validate */
	if(jQuery('.pam_pets_service').is(':visible')){
			/* var pet_multiple_div  = {}; */
			var count_var  = 0;
			
		 jQuery.each(jQuery(".pets_service"), function(){ 
			var value = jQuery(this).prop('checked');
			if(value){
				var pet_multiple_div  = {};
				var pets_service_err = ''; 
				var data_did = jQuery(this).data('div_id');	
				
				var pet_service_value = jQuery(this).val();
				var pet_service_error = "pet_service_"+pet_service_value.toLowerCase();
				var pets_service_gen = jQuery('.pets_service_gen').val();
				var pet_name = jQuery('#pet_name'+data_did).val();
				var pet_breed = jQuery('#pet_breed'+data_did).val();
				var pets_gender = jQuery('input[name="pets_gender_1'+data_did+'"]:checked').val();
				var pet_age = jQuery('#pet_age'+data_did).val();
				var pets_age_radio = jQuery('#age_value'+data_did).val();
			
				var pet_weight = jQuery('#pet_weight'+data_did).val();
				var pet_weight_gen = jQuery('#weight_value'+data_did).val(); 
				
				pet_multiple_div['pets_service_gen'] = pets_service_gen;
				pet_multiple_div['pets_service'] = pet_service_value;
				pet_multiple_div['pet_name'] = pet_name;
				pet_multiple_div['pet_breed'] = pet_breed;
				pet_multiple_div['pets_gender'] = pets_gender;
				pet_multiple_div['pet_age'] = pet_age;
				pet_multiple_div['pets_age_radio'] = pets_age_radio;
				pet_multiple_div['pet_weight'] = pet_weight;
				pet_multiple_div['pet_weight_gen'] = pet_weight_gen;			
				pet_multiple_div['service'] = 'Pets';
				
				/* var intRegex = "/^\d+$/"; */
					
				if(pet_name == ''){
					pets_service_err +='Y';
					jQuery('#pets_name_errs'+pet_service_error).show();
				}
				if(pet_breed==''){
					pets_service_err +='Y';
					jQuery('#pets_breed_errs'+pet_service_error).show();
				}
				if(pet_age=='' && !(intRegex.test(pet_age))){
				/* if(pet_age==''){ */
					pets_service_err +='Y';
					jQuery('#pets_age_errs'+pet_service_error).show();
				}
				if(pet_weight=='' && !(intRegex.test(pet_age))){
					pets_service_err +='Y';
					jQuery('#pets_weight_errs'+pet_service_error).show();
				}			
				if(pets_service_err!=''){
					return false;
				} 
			count_var++;
			var service_count = "pet_info"+count_var;
			/* console.log(pet_multiple_div); */
			service_info[service_count] = pet_multiple_div;
			/* service_info.push(pet_multiple_div); */				
			}
			
		});		
		console.log(service_info);		
	}
jQuery(document).ready(function(){
	jQuery("#pet_namepet_service_dog").keyup(function(){
		jQuery("#pets_name_errspet_service_dog").hide();
	});
	jQuery("#pet_breedpet_service_dog").keyup(function(){
		jQuery("#pets_breed_errspet_service_dog").hide();
	});
	jQuery("#pet_agepet_service_dog").keyup(function(){
		jQuery("#pets_age_errspet_service_dog").hide();
	});
	jQuery("#pet_weightpet_service_dog").keyup(function(){
		jQuery("#pets_weight_errspet_service_dog").hide();
	});
	
	
	jQuery("#pet_namepet_service_cat").keyup(function(){
		jQuery("#pets_name_errspet_service_cat").hide();
	});
	jQuery("#pet_breedpet_service_cat").keyup(function(){
		jQuery("#pets_breed_errspet_service_cat").hide();
	});
	jQuery("#pet_agepet_service_cat").keyup(function(){
		jQuery("#pets_age_errspet_service_cat").hide();
	});
	jQuery("#pet_weightpet_service_cat").keyup(function(){
		jQuery("#pets_weight_errspet_service_cat").hide();
	});
	
	
	jQuery("#pet_namepet_service_birds").keyup(function(){
		jQuery("#pets_name_errspet_service_birds").hide();
	});
	jQuery("#pet_breedpet_service_birds").keyup(function(){
		jQuery("#pets_breed_errspet_service_birds").hide();
	});
	jQuery("#pet_agepet_service_birds").keyup(function(){
		jQuery("#pets_age_errspet_service_birds").hide();
	});
	jQuery("#pet_weightpet_service_birds").keyup(function(){
		jQuery("#pets_weight_errspet_service_birds").hide();
	});
	
	
	jQuery("#pet_namepet_service_other").keyup(function(){
		jQuery("#pets_name_errspet_service_other").hide();
	});
	jQuery("#pet_breedpet_service_other").keyup(function(){
		jQuery("#pets_breed_errspet_service_other").hide();
	});
	jQuery("#pet_agepet_service_other").keyup(function(){
		jQuery("#pets_age_errspet_service_other").hide();
	});
	jQuery("#pet_weightpet_service_other").keyup(function(){
		jQuery("#pets_weight_errspet_service_other").hide();
	});
	
	
});

	
	/* Validate Loading/Unloading Address Section */
	jQuery('#pam_loading_unloading_address').validate({
		rules: {
			pam_loading_street_address: { required: true },
			pam_loading_city: { required: true },
			pam_loading_state: { required: true },
			/* pam_loading_country: { required: true }, */
			pam_unloading_street_address: { required: true },
			pam_unloading_city: { required: true },
			pam_unloading_state: { required: true }
			/* pam_unloading_country: { required: true }			 */
			},
		messages: {
			pam_loading_street_address: { required: mpmain_error_obj.pam_loading_street_address },
			pam_loading_city: { required: mpmain_error_obj.pam_loading_city },
			pam_loading_state: { required: mpmain_error_obj.pam_loading_state },
			/* pam_loading_country: { required: mpmain_error_obj.pam_loading_country }, */
			pam_unloading_street_address: { required: mpmain_error_obj.pam_loading_street_address },
			pam_unloading_city: { required: mpmain_error_obj.pam_loading_city },
			pam_unloading_state: { required: mpmain_error_obj.pam_loading_state }
			/* pam_unloading_country: { required: mpmain_error_obj.pam_loading_country } */
			}
	});
	if(!jQuery('#pam_loading_unloading_address').valid()){
		return false;
	}
	loading_info['pam_loading_street_address'] = jQuery('#pam_loading_street_address').val();
	loading_info['pam_loading_city'] = jQuery('#pam_loading_city').val();
	loading_info['pam_loading_state'] = jQuery('#pam_loading_state').val();
	/* loading_info['pam_loading_country'] = jQuery('#pam_loading_country').val(); */
	
	
	
	/* cabs Info by trupal added by pradeep*/
	if(jQuery('#cab_want').is(':checked')){
	var cab_lable_err = ""
	var to_cab_lable_err = ""
	var pam_street_address_cab = cabs_info['pam_street_address_cab']=jQuery('#pam_loading_street_address_cab').val();
	var pam_loading_city_cab = cabs_info['pam_city_cab'] = jQuery('#pam_loading_city_cab').val();
	var pam_loading_state_cab = cabs_info['pam_state_cab'] = jQuery('#pam_loading_state_cab').val();
	var pam_member_cab = cabs_info['pam_member_cab'] = jQuery('#pam_member_cab').val();
	
	var cab_date=cabs_info['cab_date'] = jQuery('#from_cab_date').val();
	var to_pam_street_address_cab = cabs_info['to_pam_street_address_cab']=jQuery('#to_pam_loading_street_address_cab').val();
	var to_pam_loading_city_cab = cabs_info['to_pam_city_cab'] = jQuery('#to_pam_loading_city_cab').val();
	var to_pam_loading_state_cab = cabs_info['to_pam_state_cab'] = jQuery('#to_pam_loading_state_cab').val();
		
			if(pam_street_address_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_address_err').show();
			}if(pam_loading_city_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_city_err').show();
			}if(pam_loading_state_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_state_err').show();
			}if(pam_member_cab == ''){
				cab_lable_err +='Y';
				jQuery('#cab_member_err').show();
			}
			if(to_pam_street_address_cab == ''){
				to_cab_lable_err +='Y';
				jQuery('#to_cab_address_err').show();
			}if(to_pam_loading_city_cab == ''){
				to_cab_lable_err +='Y';
				jQuery('#to_cab_city_err').show();
			}if(to_pam_loading_state_cab == ''){
				to_cab_lable_err +='Y';
				jQuery('#to_cab_state_err').show();
			}				
			if(cab_lable_err!=''){
				return false;
			} 
			if(to_cab_lable_err!=''){
				return false;
			} 
		}
	
	
	
	unloading_info['pam_unloading_street_address'] = jQuery('#pam_unloading_street_address').val();
	unloading_info['pam_unloading_city'] = jQuery('#pam_unloading_city').val();
	unloading_info['pam_unloading_state'] = jQuery('#pam_unloading_state').val();
	/* unloading_info['pam_unloading_country'] = jQuery('#pam_unloading_country').val(); */
	
	
	/* Fetch Additional Info */
	if(additional_info_id!=''){
		var additional_info_ids = additional_info_id.split(',');
		for(var ids=0;ids<additional_info_ids.length;ids++){
			var additional_info_id = additional_info_ids[ids];
			
			if(jQuery('#additional_info'+additional_info_id).is(':checked')){				
				additional_info[additional_info_id]	= 'Y';
			}else{
				additional_info[additional_info_id]	= 'N';
			}
		}
	}
	
	/* Fetch Service Articles Info */ 
	if(jQuery('#mp_service_addons').is(':visible')){
		jQuery('.addon-checkbox').each(function(){
			var curraddonid = jQuery(this).data('saddonid');
			if(jQuery(this).is(':checked')){
				articles_info[curraddonid] = jQuery('#addonqty_'+curraddonid).val();
			}
		});
	}
	
	var current_stepdata ={'service_info':service_info,'loading_info':loading_info,'cabs_info':cabs_info,'unloading_info':unloading_info,'articles_info':articles_info,'additional_info':additional_info,action:'set_steptwo_session'}
	
	/* Set Second Step Values In Session */
	jQuery("input").blur(function(){
		jQuery('#compare_error_msg').hide();
		});
		if(loading_info['pam_loading_street_address']==unloading_info['pam_unloading_street_address']){
		jQuery('#compare_error_msg').show();
		return false;
		}else{
		jQuery('#mp .loader').show();	
		jQuery.ajax({
		type:"POST",
		url : ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType : 'html',	
		data:current_stepdata,
		success:function(response){	
		jQuery('#mp .loader').hide();
		}
		});	
		}	
		jQuery('#mp_fourth_step').show();
		jQuery('#mp_second_step').hide(); 


});
	
	
jQuery(document).on( "click",'#btn-checkout-step',function() {
	jQuery('.mp_terms_and_condition_error').hide();
	var errObj = mpmain_error_obj;
	var ajaxurl = mpmain_obj.plugin_path;
	var thankyou_url = mpmain_obj.thankyou_url;
	var mp_terms_and_condition_status = mpmain_obj.mp_terms_and_condition_status;
	var currstep = jQuery('.mp-booking-step').data('current');
	var terms_condition = jQuery("#mp-accept-conditions").prop("checked");
    	
	if(mp_terms_and_condition_status=='E' && terms_condition !== true){  
		jQuery('.mp_terms_and_condition_error').show();
		jQuery('html, body').stop().animate({
								'scrollTop': jQuery('.mp_terms_and_condition_error').offset().top - 80
						}, 800, 'swing', function () {});		
		
		return false; 
	}
	
	jQuery.validator.addMethod("pattern_phone", function(value, element) {
        return this.optional(element) || /^[0-9+]*$/.test(value);
    }, "Enter Only Numerics");
	
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	jQuery.validator.addMethod("pattern_email", function(value, element) {
	return this.optional(element) || re.test(value);
	}, "Enter Your Valid E-Mail");
	
	jQuery('#mp_newuser_form_validate').validate({
		rules:{
			'new_user_preferred_username':{
											required:true, 
											pattern_email:true,
											remote: {
												url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
												type: "POST",
												async: false,
												data: {
													email: function(){ 
														return jQuery("#new_user_preferred_username").val(); 
													},
													action:"check_existing_username"
												}
											}
										},
			'new_user_preferred_email':{
								required:true, 
								pattern_email:true							
							},
			'new_user_preferred_password':{
											required:true,
											minlength:8,
											maxlength:30
										},
			'new_user_firstname':{
									required:true
								},
			'new_user_lastname':{
									required:true
								},
			'mp-phone':{
							required:true,
							minlength:10,
							maxlength:14
						},
			'new_user_street_address':{
										required:true
									},
			'zipcode':{
						required:true,
						minlength: 5,
						maxlength:7
						},
			'new_user_city':{
								required:true
							},
			'new_user_state':{
								required:true
							},
			'new_user_notes':{
								required:true
							},
		},
		messages:{
			'new_user_preferred_username':{
					required : errObj.Please_Enter_Email, 
					pattern_email : errObj.Please_Enter_Valid_Email, 
					remote : errObj.Email_already_exist
			},
			'new_user_preferred_email':{
					required : errObj.Please_Enter_Email, 
					pattern_email: errObj.Please_Enter_Valid_Email
			},
			'new_user_preferred_password':{
					required : errObj.Please_Enter_Password,
					minlength:errObj.Please_enter_minimum_8_Characters, 
					maxlength:errObj.Please_enter_maximum_30_Characters
			},
			'new_user_firstname':{
				required : errObj.Please_Enter_First_Name
			},
			'new_user_lastname':{
				required : errObj.Please_Enter_Last_Name
			},
			'mp-phone':{
				required : errObj.Please_Enter_Phone_Number,
				minlength:errObj.Please_enter_minimum_10_Characters, 
				maxlength:errObj.Please_enter_maximum_14_Characters
			},
			'new_user_street_address':{
				required : errObj.Please_Enter_Address
			},
			'zipcode':{
				required : "Please Enter Zipcode",
				minlength: "Please Input 5 To 7 Characters",
				maxlength:"Please Input 5 To 7 Characters"
			},
			'new_user_city':{
				required : errObj.Please_Enter_City
			},
			'new_user_state':{
				required : errObj.Please_Enter_State
			},
			'new_user_notes':{
				required : errObj.Please_Enter_Notes
			},
		}
	});
	
	jQuery('.get_custom_field').each(function(){
		var name_field = jQuery(this).attr('name');
		var required_field = jQuery(this).data('required');
		var fieldlabel = jQuery(this).data('fieldlabel');
		if(required_field == "Y"){
			jQuery(this).rules("add",{ required : true, messages : { required : errObj.Please_Enter+" "+fieldlabel+""}});
		}
	});
	 
	
	
	
	if(jQuery('#mp_newuser_form_validate').valid()){
		jQuery('#mp .loader').show();
		jQuery('.mp_terms_and_condition_error').hide();
				
		var pwd = jQuery('#new_user_preferred_password').val();
		var fname = jQuery('#new_user_firstname').val();
		var lname = jQuery('#new_user_lastname').val();
		var phone = jQuery('#mp-front-phone').val();
		var address = jQuery('#new_user_street_address').val();
		var city = jQuery('#new_user_city').val();
		var zipcode = jQuery('#zipcode').val();
		var state = jQuery('#new_user_state').val();
		var notes = jQuery('#new_user_notes').val();
		var user_type = jQuery('#pam_user_type').val();		
		var check_gender = jQuery('.new_user_gender').prop('checked');
		var ccode = jQuery('#mp-front-phone').data('ccode');
		if(user_type == 'N'){
			var username = jQuery('#new_user_preferred_username').val();
		}else{
			var username = jQuery('#new_user_preferred_email').val();
		}
		
		if(user_type == 'G'){
			var mp_user_type = 'guest';
		}else if(user_type == 'E'){
			var mp_user_type = 'existing';
			if(jQuery('#mp_existing_login_btn').is(':visible')){
				jQuery('#invalid_un_pwd').show();
				jQuery('html, body').stop().animate({
						'scrollTop': jQuery('#invalid_un_pwd').offset().top - 80
				}, 800, 'swing', function () {});
				jQuery('#mp .loader').hide();
				return false;
			}			
		}else{
			var mp_user_type = 'new';
		}
		
		if(check_gender){
			var gender = 'M';
		}else{
			var gender = 'F';
		}
		
		var dynamic_field_add = {};
		jQuery('.get_custom_field').each(function(){
			if(jQuery(this).data('fieldname') == "radio_group"){
				var radionames = jQuery(this).attr('name');				
				dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery('input[name="'+radionames+'"]:checked').val();
			}else if(jQuery(this).data('fieldname') == "checkbox_group"){
				var checkboxnames = jQuery(this).attr('name');
				var checkvalue = '';
				jQuery('input[name="'+checkboxnames+'"]:checked').each(function(){
					checkvalue += jQuery(this).val()+',';
				});
				dynamic_field_add[jQuery(this).data('fieldlabel')] = checkvalue;
			}else{
				dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery(this).val();
			}
		});
		
		var dataString = { 'username':username, 'pwd':pwd, 'fname':fname, 'lname':lname, 'phone':phone, 'address':address, 'zipcode':zipcode,'city':city, 'state':state, 'notes':notes, 'mp_user_type':mp_user_type,'gender':gender, 'dynamic_field_add':dynamic_field_add, 'ccode':ccode, 'action':'mp_booking_complete' };
				
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',
			data:dataString,
			success:function(response){
				jQuery('#mp .loader').hide();	
				if(thankyou_url!=''){
					window.location.href = thankyou_url;
					
				}else{
					jQuery('#mp_fourth_step').hide();
					jQuery('#mp_third_step').show();
				}
			}	
		});
	}
});	
/* User Type Selection */
jQuery(document).on('click', '.new_and_existing_user_radio_btn', function(){	
	var select_user = jQuery(this).val();	
	jQuery('#pam_user_type').val(select_user);		
	
	if(select_user=='E'){
		jQuery('#guestexisting_user_email').show( "blind", {direction: "vertical"}, 700 );
		jQuery('.existing-user-login').show( "blind", {direction: "vertical"}, 700 );
		jQuery('.new-user-area').hide( "blind", {direction: "vertical"}, 300 );
		jQuery('.new-user-personal-detail-area').show( "blind", {direction: "vertical"}, 300 );
	}else if(select_user=='G'){
		jQuery('#guestexisting_user_email').show( "blind", {direction: "vertical"}, 700 );
		jQuery('.existing-user-login').hide( "blind", {direction: "vertical"}, 700 );
		jQuery('.new-user-area').hide( "blind", {direction: "vertical"}, 300 );
		jQuery('.new-user-personal-detail-area').show( "blind", {direction: "vertical"}, 300 );
	}else{
		jQuery('.new-user-area').show( "blind", {direction: "vertical"}, 700 );
		jQuery('#guestexisting_user_email').hide( "blind", {direction: "vertical"}, 300 );
		jQuery('.existing-user-login').hide( "blind", {direction: "vertical"}, 300 );
		jQuery('.new-user-personal-detail-area').show( "blind", {direction: "vertical"}, 700 );
	}		
});

/* Login Existing User */
jQuery(document).ready(function(){
	var errObj = mpmain_error_obj;
	jQuery('#existing-user').validate({
		rules:{
			'mp_existing_login_username_input':{required:true,email:true},
			'mp_existing_login_password_input':{required:true},
		},
		messages:{
			'mp_existing_login_username_input':{required : errObj.Please_Enter_Email, email : errObj.Please_Enter_Valid_Email},
			'mp_existing_login_password_input':{required : errObj.Please_Enter_Password},
			
		}
	});
});
jQuery(document).on('click','#mp_existing_login_btn', function() {
	if(jQuery('#existing-user').valid()){
		jQuery('#mp .loader').show();
		var ajaxurl = mpmain_obj.plugin_path;
		var uname = jQuery('#mp_existing_login_username').val();
		var pwd = jQuery('#mp_existing_login_password').val();
		var dataString = { 'uname':uname, 'pwd':pwd, 'action':'get_existing_user_data' };
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/mp_front_ajax.php",
			dataType : 'html',
			data:dataString,
			success:function(response){
				jQuery('#mp .loader').hide();
				if(jQuery.trim(response) != "Invalid Username or Password"){
					var getdata = jQuery.parseJSON(response);
					jQuery('.user-login-main').hide();
					jQuery('.existing-user-login').hide();
					jQuery('.existing-user-success-login-message').show();
					jQuery('.new-user-personal-detail-area').show();
					jQuery("#invalid_un_pwd").css("display","none");
					jQuery('.hide_new_user_login_details').hide();
					jQuery('#logged_in_user_name').html(getdata.first_name+" "+getdata.last_name);
					
					jQuery('#new_user_firstname').addClass("focus");
					jQuery('#new_user_lastname').addClass("focus");
                    jQuery('#new_user_preferred_email').val(getdata.user_email);
					jQuery('#mp-front-phone').addClass("focus");
					jQuery('#new_user_street_address').addClass("focus");
					jQuery('#zipcode').addClass("focus");
					jQuery('#new_user_city').addClass("focus");
					jQuery('#new_user_state').addClass("focus");
					jQuery('#new_user_notes').addClass("focus");
					
					jQuery('#new_user_preferred_password').val(getdata.password);
					jQuery('#new_user_preferred_username').val(getdata.user_email);
					jQuery('#new_user_firstname').val(getdata.first_name);
					jQuery('#new_user_lastname').val(getdata.last_name);
					/* jQuery('#mp-front-phone').val(getdata.phone); */
					jQuery('#mp-front-phone').intlTelInput("setNumber", getdata.phone);
					
					jQuery('#mp-front-phone').attr('data-ccode',getdata.ccode);
					jQuery('#new_user_street_address').val(getdata.address);
					jQuery('#zipcode').val(getdata.user_zip);
					jQuery('#new_user_city').val(getdata.city);
					jQuery('#new_user_state').val(getdata.state);
					jQuery('#new_user_notes').val(getdata.notes);
					
					if(getdata.gender == 'M'){
						jQuery('#mp-male').prop('checked',true);
					}else{
						jQuery('#mp-female').prop('checked',true);
					}
					
					jQuery('.error').each(function(){
						jQuery(this).hide();
					});
				}else{
					jQuery("#invalid_un_pwd").css("display","block");
				}
			}
		});
	}
});




/* other service validation focus */
jQuery(".flno_vali").focus(function(){
jQuery("#other_floor_errs").hide();
});
jQuery(".art_err").focus(function(){
jQuery("#other_article_errs").hide();
});
jQuery(".des_err").focus(function(){
jQuery("#other_description_errs").hide();
});	
jQuery(".des_err").focus(function(){
jQuery("#other_description_errs").hide();
});

jQuery(document).ready(function(){
var count_add = 0;
var count_toc_icon_add = 0;

var i=2;
jQuery('#add_plus').click(function(){
count_add++;
// var field_dynamic = jQuery('#add_field').data('id');

jQuery('#append_here').append('<div id="add_div'+count_add+'" class="mp-md-12 mp-lg-12 mp-sm-12 mp-xs-12"><div class="mp-form-row mp-md-3 mp-lg-3 mp-sm-11 mp-xs-11" style="margin-left: -8px;"><div class="pr margin_top_text"><input type="text" maxlength="5" name="other_service_article[]" class="custom-input other_service_article mt-13 art_err" id="other_service_article" value="" /><label class="custom">Article</label><i class="bottom-line"></i></div></div><div class="mp-form-row mp-md-1 mp-lg-1 mp-sm-1 mp-xs-1"><i class="fa fa-minus-circle add_plus fa-icon com-col" data-count_id='+count_add+' id="add_minus" style="margin-top: 65px;"></i></div></div>');
i++;
});
});
jQuery(document).ready(function(){
	var count_add = 0;
	var count_toc_icon_add = 0;
	
    var i=2;
    jQuery('#add_vehicle').click(function(){
		count_add++;
		
		
       // var field_dynamic = jQuery('#add_field').data('id');
	jQuery('#append_vehicle').append('<div class="vehicle_appdivs appand_sec" id="appdivs'+count_add+'"><div class="row mp-common-inputs fullwidth new-user-personal-detail-area car_sec" id="car_secc"><div class="mp-form-row mp-md-6 mp-lg-6 mp-sm-12 mp-xs-12 vehicle_service_car"><div class="mp-form-row p-7"><h3 class="block-title fs-18"><i class="vehicle-color-icon icons fs-18"></i> Car Type</h3><span id="vehicle_service_car_type_err" class="mp-error">Please choose car type</span><input type="hidden" value="" class="vehicle_service_car_type'+count_add+'" /><div id="cartype-select1" class="cus-location-cartype custom-input nmt cus-location-cartype_multi'+count_add+'"><div class="common-selection-main location-selection-cartype location-selection-cartype_multi'+count_add+'"><div class="selected-is select-location-cartype'+count_add+'" title="Choose Your Selection"><div style="padding: 18px 10px 10px 5px !important;" class="data-list selected_location-cartype'+count_add+'"><div class="mp-value">Please choose Car Type<input type="hidden" class="custom-input mt-13 input-new no_of_vehicle" name="vehicle_service_quantity[]" id="vehicle_service_quantity" value=""></div></div></div><ul class="common-data-dropdown location-dropdown-cartype location-dropdown-cartype_multi'+count_add+'"><li class="data-list select_location-cartype'+count_add+'" data-vals="XUV"><div class="mp-value" >SUV</div></li><li class="data-list select_location-cartype'+count_add+'" data-vals="Sedan"><div class="mp-value" >Sedan</div></li><li class="data-list select_location-cartype'+count_add+'" data-vals="Hatch back"><div class="mp-value" >Hatch back</div></li><li class="data-list select_location-cartype'+count_add+'" data-vals="MPV"><div class="mp-value" >MPV</div></li><li class="data-list select_location-cartype'+count_add+'" data-vals="Crossover"><div class="mp-value" >Crossover</div></li><li class="data-list select_location-cartype'+count_add+'" data-vals="Coupe"><div class="mp-value" >Coupe</div></li><li class="data-list select_location-cartype'+count_add+'" data-vals="Convertible"><div class="mp-value" >Convertible</div></li></ul></div></div><i class="bottom-line-car-type"></i></div></div><div class="mp-form-row mp-md-5 mp-lg-5 mp-sm-12 mp-xs-12 vehicle_service_bike"><div class="pr margin_top_text"><input type="text" class="custom-input mt-13 no_of_vehicle" name="vehicle_service_quantity[]" id="vehicle_service_quantity" value="" /><label class="custom">No. Of Vehicle</label><i class="bottom-line"></i></div><label id="vehicle_service_no_err" class="mp-relative mp-error">Please Enter Number Of Vehicle</label></div><div class="mp-form-row mp-md-1 mp-lg-1 mp-sm-12 mp-xs-12 vehicle_service_bike" style="line-height: 70px;!important"><i class="fa fa-minus-circle add_plus fa-icon com-col" data-count_id='+count_add+' id="append_vehicle_minus"></i></div></div></div>');
	i++;
	/* Car Type */
	jQuery(document).on("click",".select-location-cartype"+count_add,function() {
		jQuery(".cus-location-cartype_multi"+count_add).addClass('focus');
		jQuery(".location-selection-cartype_multi"+count_add).addClass('clicked');
		jQuery(".location-dropdown-cartype"+count_add).addClass('bounceInUp');	
		
	});
	
	jQuery(document).on("click",".select_location-cartype"+count_add,function() {
		jQuery(".vehicle_service_car_type"+count_add).val(jQuery(this).find('.mp-value').text());
		jQuery(".cus-location-cartype_multi"+count_add).removeClass('focus');
		jQuery(".location-selection-cartype_multi"+count_add).removeClass('clicked');
		jQuery(".location-dropdown-cartype"+count_add).removeClass('bounceInUp');
		jQuery(".selected_location-cartype"+count_add).html(jQuery(this).html());
	});	
    });
	
	


});
jQuery(document).on('click', '#add_minus', function () {
		var app_div_id = jQuery(this).data('count_id');
		jQuery('#add_div'+app_div_id).remove();
       
});
jQuery(document).on('click', '#append_vehicle_minus', function () {
		var app_div_id = jQuery(this).data('count_id');
		jQuery('#appdivs'+app_div_id).remove();
       
});
/* jQuery(document).bind('ready ajaxComplete', function () {
	jQuery(".phone_mask").mask("999-999-9999");
}); */
/* From add as above for cabs info*/
jQuery(function(){
	jQuery("#add_as_above").click(function(){	
		if(jQuery(this).is(':checked')){ 
			var pam_loading_street_address=jQuery("#pam_loading_street_address").val();
			var pam_loading_city=jQuery("#pam_loading_city").val();
			var pam_loading_state=jQuery("#pam_loading_state").val();
			jQuery("#pam_loading_street_address_cab").val(pam_loading_street_address);
			jQuery("#pam_loading_city_cab").val(pam_loading_city);
			jQuery("#pam_loading_state_cab").val(pam_loading_state);
			jQuery("#cab_address_err").hide();
			jQuery("#cab_city_err").hide();
			jQuery("#cab_state_err").hide();
		}else{
			var pam_loading_street_address="";
			var pam_loading_city="";
			var pam_loading_state="";
			jQuery("#pam_loading_street_address_cab").val(pam_loading_street_address);
			jQuery("#pam_loading_city_cab").val(pam_loading_city);
			jQuery("#pam_loading_state_cab").val(pam_loading_state);
		}
	});
});
/* To add as above for cabs info*/
jQuery(function(){
	jQuery("#to_add_as_above").click(function(){	
		if(jQuery(this).is(':checked')){ 
			var pam_unloading_street_address=jQuery("#pam_unloading_street_address").val();
			var pam_unloading_city=jQuery("#pam_unloading_city").val();
			var pam_unloading_state=jQuery("#pam_unloading_state").val();
			jQuery("#to_pam_loading_street_address_cab").val(pam_unloading_street_address);
			jQuery("#to_pam_loading_city_cab").val(pam_unloading_city);
			jQuery("#to_pam_loading_state_cab").val(pam_unloading_state);
			jQuery("#to_cab_address_err").hide();
			jQuery("#to_cab_city_err").hide();
			jQuery("#to_cab_state_err").hide();
		}else{
			var pam_unloading_street_address="";
			var pam_unloading_city="";
			var pam_unloading_state="";
			jQuery("#to_pam_loading_street_address_cab").val(pam_unloading_street_address);
			jQuery("#to_pam_loading_city_cab").val(pam_unloading_city);
			jQuery("#to_pam_loading_state_cab").val(pam_unloading_state);
		}
	});
});
jQuery(".ad_err").focus(function(){
	jQuery("#cab_address_err").hide();
});
jQuery(".city_err").focus(function(){
	jQuery("#cab_city_err").hide();
});
jQuery(".state_err").focus(function(){
	jQuery("#cab_state_err").hide();
});
jQuery(".to_ad_err").focus(function(){
	jQuery("#to_cab_address_err").hide();
});
jQuery(".to_city_err").focus(function(){
	jQuery("#to_cab_city_err").hide();
});
jQuery(".to_state_err").focus(function(){
	jQuery("#to_cab_state_err").hide();
});	
jQuery(".mem_err").focus(function(){
	jQuery("#cab_member_err").hide();
});

/* check or uncheck want cabs info */
jQuery(function(){
	jQuery("#cab_want").click(function(){	
		if(jQuery(this).is(':checked')){ 
			jQuery(".caps_form").show(); 
		}else{
			jQuery(".caps_form").hide();
		}
	});
});
/* Hide Show add button of vehicle type */
jQuery(document).on('click','#vehicle_service_bike', function() {
	jQuery('#add_vehicle').hide();
});

jQuery(document).on('click','#vehicle_service_car', function() {
	jQuery('#add_vehicle').show();
});

jQuery(document).ready(function(){	
jQuery('#btn-fourth-step').on('click',function(){
jQuery('html, body').stop().animate({
'scrollTop': jQuery('#mp-main').offset().top - 30
}, 800, 'swing', function () {});
});
});
jQuery(document).ready(function(){	
jQuery('#fourth').on('click',function(){
jQuery('html, body').stop().animate({
'scrollTop': jQuery('#mp-main').offset().top - 30
}, 800, 'swing', function () {});
});
});
jQuery(document).ready(function(){	
jQuery('#btn-second-step').on('click',function(){
jQuery('html, body').stop().animate({
'scrollTop': jQuery('#mp-main').offset().top - 30
}, 800, 'swing', function () {});
});
});
jQuery(document).ready(function(){	
jQuery('#second').on('click',function(){
jQuery('html, body').stop().animate({
'scrollTop': jQuery('#mp-main').offset().top - 30
}, 800, 'swing', function () {});
});
});
jQuery(document).ready(function(){	
jQuery('#btn-first-step').on('click',function(){
jQuery('html, body').stop().animate({
'scrollTop': jQuery('#mp-main').offset().top - 30
}, 800, 'swing', function () {});
});
});
jQuery(document).ready(function(){	
jQuery('#first').on('click',function(){
jQuery('html, body').stop().animate({
'scrollTop': jQuery('#mp-main').offset().top - 30
}, 800, 'swing', function () {});
});
});


/* Code By Ajay Prajapati */

jQuery(document).on('click','#first_step_submit',function(event){
	
	var ajaxurl = mpmain_obj.plugin_path;

	var source_city = jQuery("#source_city").val();
	var destination_city = jQuery("#destination_city").val();
	var via = jQuery("#via").val();
	var bhk = jQuery('bhk').val();
	var name = jQuery("#name").val();
	var email = jQuery("#email").val();
	var phone = jQuery("#phone").val();
	var date = jQuery("#date").val();
	jQuery('#mp .loader').show();
	
	var servicedata = {
						source_city:source_city,
						destination_city:destination_city,
						via:via,
						bhk:bhk,
						name:name,
						email:email,
						phone:phone,
						date:date,
						action:'first_step_data'};	
	jQuery.ajax({
		type     :"POST",
		url      :ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType :'html',			
		data     :servicedata,
		success:function(response){					
				
				jQuery('#mp .loader').hide();	
			}
		});	
});


jQuery(document).on('click','.room_type_cat', function() {
	
	var ajaxurl = mpmain_obj.plugin_path;
	var id = jQuery(this).attr('data-id');
	var name = jQuery(this).attr('data-name');
	
	var postdata = {
						id:id,
						name:name,
						action:'get_article_type'
					};	
	jQuery.ajax({
		type     :"POST",
		url      :ajaxurl+"/assets/lib/mp_front_ajax.php",
		dataType :'html',			
		data     :postdata,
		success:function(response){					
				jQuery('.mp-loading-main').hide();
				jQuery('.articles_tab').html(response);
				var check_li = true;

				jQuery(".cart_li").find('li').each(function(){
					var li_id = jQuery(this).attr("data-li_id");
					if(id == li_id){
						check_li = false;
					}
				});
			
				if(check_li){
					var return_string = '<li class="cart-listing cart_check_'+id+'" data-li_id="'+id+'">'+
								'<h3 class="cart-title">'+name+'</h3>'+
									'<h4 class="cart-value"></h4>'+
										'<ul class="articles_ul_'+id+'"></ul>'+
									'</li>';
					jQuery(".cart_li").append(return_string);
				}
			}
		});	
});



jQuery(document).on('click','.minus',function(event){

		var $input = jQuery(this).parent().find('input');
		var count = parseInt($input.val()) - 1;
		count = count < 1 ? 1 : count;
		$input.val(count);
		$input.change();
		//return false;
		var get_article_room_id = jQuery(this).attr('data-room_id');
		var get_article_name = jQuery(this).attr('data-name');
		var get_article_id = jQuery(this).attr('data-id');
		var get_article_price = jQuery(this).attr('data-price');
	
	  jQuery('.get_'+get_article_id).prop('checked', true);
		var check_li = true;
		jQuery(".articles_ul_"+get_article_room_id).find('li').each(function()
		{
				var li_id = jQuery(this).attr("data-li_id");
				if(get_article_id == li_id)
				{
						check_li = false;
				}
		});

		if(check_li){

			var return_string = '<li class="cart-listing cart_check_'+get_article_id+''+get_article_room_id+'" data-li_id="'+get_article_id+'">'+
													'<h3 class="cart-title">'+get_article_name+'</h3>'+
													'<h4 class="cart-value_'+get_article_id+''+get_article_room_id+'">'+count+'</h4>'+
													//'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+get_article_price+'</h4>'+
												'</li>';
			jQuery(".articles_ul_"+get_article_room_id).append(return_string);

		}else{

			var return_string =	'<h4 class="cart-value_'+get_article_id+''+get_article_room_id+'">'+count+'</h4>'+
													'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+'$'+count*get_article_price+'</h4>'+
													'</li>';
			jQuery(".cart-value_"+get_article_id+get_article_room_id).html(return_string);

		}

});



jQuery(document).on('click','.plus',function(event){
		
		var $input = jQuery(this).parent().find('input');
		var count = parseInt($input.val()) + 1;
		$input.val(count);
		$input.change();

		var get_article_room_id = jQuery(this).attr('data-room_id');
		var get_article_name = jQuery(this).attr('data-name');
		var get_article_id = jQuery(this).attr('data-id');
		var get_article_price = jQuery(this).attr('data-price');
	
	  jQuery('.get_'+get_article_id).prop('checked', true);
		var check_li = true;
		jQuery(".articles_ul_"+get_article_room_id).find('li').each(function()
		{
				var li_id = jQuery(this).attr("data-li_id");
				if(get_article_id == li_id)
				{
						check_li = false;
				}
		});
		if(check_li){

			var return_string = '<li class="cart-listing cart_check_'+get_article_id+''+get_article_room_id+'" data-li_id="'+get_article_id+'">'+
													'<h3 class="cart-title">'+get_article_name+'</h3>'+
													'<h4 class="cart-value_'+get_article_id+''+get_article_room_id+'">'+count+'</h4>'+
													//'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+get_article_price+'</h4>'+
												'</li>';
			//alert(return_string);
			jQuery(".articles_ul_"+get_article_room_id).append(return_string);

		}else{

			var return_string =	'<h4 class="cart-value_'+get_article_id+''+get_article_room_id+'">'+count+'</h4>'+
													'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+'$'+count*get_article_price+'</h4>'+
													'</li>';
												//	alert(return_string);
			var return_total =	'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+'$'+count*get_article_price+'</h4>'+
			jQuery(".cart-value_"+get_article_id+get_article_room_id).html(return_string);
		 jQuery(".cart-cost").html(return_total);
		}

	});
	


jQuery(document).on('click','.get_article',function(event){

		var get_article_room_id = jQuery(this).attr('data-room_id');
		var get_article_id = jQuery(this).attr('data-id');
		var get_article_name = jQuery(this).attr('data-name');

    if(jQuery('.get_'+get_article_id).prop("checked") == true){
         
          	var counts = 1;
   				  jQuery("#plus"+get_article_room_id+get_article_id).trigger("click");
            jQuery(".num_"+get_article_room_id+get_article_id).val(counts);
    }
    else if(jQuery('.get_'+get_article_id).prop("checked") == false){

					 jQuery(".cart_check_"+get_article_id+get_article_room_id).remove();  
					 var counts = 0; 	  
					 jQuery(".num_"+get_article_room_id+get_article_id).val(counts);	
    }			
});
