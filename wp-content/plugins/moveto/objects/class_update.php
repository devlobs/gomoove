<?php 
class moveto_update {

	public function __construct() {

		$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));

		if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
		}
		global $wpdb; 	  

		$version1_0 = get_option('moveto_version');
		if(!isset($version1_0)) {
			add_option('moveto_version','1.0');
		}
		$version1_1 = get_option('moveto_version');
		if($version1_1<1.1) {
			update_option('moveto_version','1.1');
		}
		$version2_0 = get_option('moveto_version');
		if($version2_0<2.0) {
			update_option('moveto_version','2.0');
			$wpdb->query("ALTER TABLE ".$wpdb->prefix."mp_additional_info ADD type ENUM('P','F') NOT NULL, ADD price INT(20) NULL;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_articles(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`article_name` varchar(50) NOT NULL,
			`type` enum('P','F') NOT NULL,
			`price` varchar(100) NOT NULL,
			`image` varchar(250) NOT NULL,
			`predefinedimage` varchar(250) NULL, 
			`max_qty` int(5) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			`position` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_article_category(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(50) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			`article_id` varchar(50) NOT NULL,
			`room_type_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_pricing_rules(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`distance` int(11) NOT NULL,
			`rules` enum('G','E') NOT NULL,
			`price` int(11) NOT NULL,
			`distance_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_distance(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`type` enum('K','M') NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_elevator(
			`id` int(11) NOT NULL,
			`discount` double NOT NULL,
			`distance_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_packaging_unpackagin(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`cost_packing` varchar(50) NOT NULL,
			`cost_unpacking` varchar(50) NOT NULL,
			`distance_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_floors(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`price` int(11) NOT NULL,
			`distance_id` int(11) NOT NULL,
			`floor_no` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_movesize(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(50) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			`position` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_room_type(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(50) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

			$wpdb->query("ALTER TABLE ".$wpdb->prefix."mp_bookings DROP `service_id`,DROP `articles_info`,DROP `cabs_info`;");
			$wpdb->query("ALTER TABLE ".$wpdb->prefix."mp_bookings  ADD via_city VARCHAR(500) NOT NULL, ADD move_size VARCHAR(100) NULL;");
		}
		$version2_1 = get_option('moveto_version');
		if($version2_1<2.1) {
			update_option('moveto_version','2.1');
		}
		$version3_0= get_option('moveto_version');
		if($version3_0<3.0) {
			update_option('moveto_version','3.0');
			add_option('moveto_distance_unit','km');	
			add_option('moveto_google_api_key','');	
			add_option('moveto_max_distance','10000');	
			add_option('moveto_tax','');	
			update_option('moveto_email_sender_name','Moveto');	
			$wpdb->query("ALTER TABLE ".$wpdb->prefix."mp_bookings ADD payment_method VARCHAR(18) NOT NULL, ADD transaction_id TEXT NULL;");
			
		}
		$version3_1= get_option('moveto_version');
		if($version3_1<3.1) {
			update_option('moveto_version','3.1');
			add_option('moveto_google_map_country','');	
			add_option('moveto_google_map_center_restrictions','');	
			add_option('moveto_company_origin_destination_distance_status','');	
			add_option('moveto_destination_company_distance','');	
			add_option('moveto_company_origin_address','');	
			add_option('moveto_company_destination_address','');	
			add_option('moveto_discount_type','');	
			add_option('moveto_discount','');	
			add_option('moveto_distance_unit','km');
		}
		$version4_0= get_option('moveto_version');
		if($version4_0<4.0) {
			update_option('moveto_version','4.0');
			add_option('moveto_min_rate_service','');	
			add_option('moveto_insurance_rate','3');	
			add_option('moveto_insurance_permission','0');	
			add_option('moveto_cubic_meter','ft_3');
			add_option('moveto_distance_unit','km');

			$wpdb->query("ALTER TABLE ".$wpdb->prefix."mp_bookings  ADD insurance_rate VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL, ADD cubic_feets_article longtext COLLATE utf8_unicode_ci NOT NULL, ADD cubic_feets_volume varchar(500) COLLATE utf8_unicode_ci NOT NULL;");

			$wpdb->query("ALTER TABLE ".$wpdb->prefix."mp_articles ADD cubicarea VARCHAR(255) COLLATE utf8_general_ci NOT NULL;");

			$wpdb->query("CREATE TABLE IF NOT EXISTS ".$wpdb->prefix."mp_cubicfeets(
			`id` int(11) NOT NULL AUTO_INCREMENT,
				`cubicfeets_range` int(11) NOT NULL,
				`cubicfeets_range_price` int(11) NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

		}	
		$version4_1 = get_option('moveto_version');
		if($version4_1<4.1) {
			update_option('moveto_version','4.1');
			update_option('move_pricing_status','N');
		  global $wpdb;
        
      $moveto_email_templates1 = array(
						/* Customer email template Quote Sent without price */
					'Mover Quote without price' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; font-size: 14px;">Quote for you request is as follow:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="width:400px; padding:12px 5px;"></div><div style="width:400px; padding:12px 5px;"></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source  :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}{{insurance_rate}}<span></span>{{user_info}}</td></tr></table><br/>Thank You, <br /> {{company_name}}</div>'
			);
			
			$moveto_email_templates2 = array(
				/* customer email template quote Sent without price */
				'Quote Requests without price' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; font-size: 14px;">Quote for you request is as follow:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="width:400px; padding:12px 5px;"></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source  :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}{{insurance_rate}}<span></span>{{user_info}}</td></tr></table><br/>Thank You, <br /> {{company_name}}</div>',
			
		);
		
		$wpdb->query("INSERT INTO ".$wpdb->prefix." mp_email_templates (`id`,`location_id`,`email_template_name`,`email_subject`,`email_message`, `default_message`, `email_template_status`,`user_type`) VALUES ('','','ACW','Mover Quote without price','','".$moveto_email_templates1[0]."','e','C'");
			
		$wpdb->query("INSERT INTO ".$wpdb->prefix." mp_email_templates (`id`,`location_id`,`email_template_name`,`email_subject`,`email_message`, `default_message`, `email_template_status`,`user_type`) VALUES ('','','QCW','Quote Requests without price','','".$moveto_email_templates2[0]."','e','C'");

		}
	}

} ?>