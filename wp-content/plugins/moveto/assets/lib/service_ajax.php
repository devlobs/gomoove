<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	$service = new moveto_service();
	$general = new moveto_general();
	$staff = new moveto_staff();
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	
	$mp_currency_symbol = get_option('moveto_currency_symbol');
/* insert addons */	
if(!empty($_POST['action']) && $_POST['action']=='article_inst_data'){
	$service->article_name = $_POST['article_title'];
	/*$service->type = $_POST['article_type'];
	$service->price = $_POST['article_price'];*/
	$service->cubicfeets = $_POST['article_cubicfeets'];
	$service->image = $_POST['article_img_service'];
	$service->article_image_name = $_POST['article_image_name'];
	$service->max_qty = $_POST['article_max_qty'];
	$service->insert_article();  
}
/* update addons */	

if(!empty($_POST['action']) && $_POST['action']=='addons_update'){	
	 $service->article_update_id = $_POST['article_update_id'];
	 $service->title = $_POST['article_title'];
	 /*$service->type = $_POST['article_type'];*/
	 $service->cubicfeets = $_POST['article_cubicfeets'];
	 $service->image = $_POST['article_image'];
	 $service->article_icon_name = $_POST['article_icon_name'];
	 /*$service->price = $_POST['article_price'];*/
	 $service->max_qty = $_POST['article_max_qty'];
	 $rate = $service->addon_update(); 
}


/* Delete Addon Permanently */		
if(isset($_POST['action'],$_POST['addon_service_id']) && $_POST['action']=='delete_addons' && $_POST['addon_service_id']!=''){
		$service->id= $_POST['addon_service_id'];
		$service->addon_delete(); 
}	

/* Remove Article Image */
if(isset($_POST['action'],$_POST['mediaid'],$_POST['mediapath']) && $_POST['action']=='delete_image'){
		$service->id= $_POST['mediaid'];
		$service->image=''; 
		$service->remove_article_image(); 	
		unlink($root.'/wp-content/uploads'.$_POST['mediapath']);
}	

if(isset($_POST['action']) && $_POST['action'] == "check_service_title"){
	$service_title = $_POST['title'];
	global $wpdb;
	$query = "select count(id) as count_title from ".$wpdb->prefix."mp_services where service_title='".$service_title."' and location_id='".$_SESSION['mp_location']."'";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}

if(isset($_POST['action']) && $_POST['action'] == "check_location_title"){
	$location_title = $_POST['title'];
	global $wpdb;
	$query = "select count(id) as count_title from ".$wpdb->prefix."mp_locations where location_title='".$location_title."'";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}

if(isset($_POST['action']) && $_POST['action'] == "check_category_title"){
	$category_title = $_POST['title'];
	global $wpdb;
	$query = "select count(id) as count_title from ".$wpdb->prefix."mp_categories where category_title='".$category_title."' and location_id='".$_SESSION['mp_location']."'";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}

/* update addons status */
if(isset($_POST['action']) && $_POST['action'] == "article_update_status"){
	$article_update_id = $_POST['article_update_id'];
	$status = $_POST['status'];	
	
	global $wpdb;
	$query = "UPDATE ".$wpdb->prefix."mp_articles SET status ='$status' WHERE	id =$article_update_id";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}


/* update addons status */
if(isset($_POST['action'],$_POST['service_id']) && $_POST['action'] == "get_articles_right" && $_POST['service_id']!=''){
?>
<div class="mp-service-top-header">
	<div class="pull-right">
		<table>
			<tbody>
				<tr>
					<td>
						<button id="mp-add-new-service-addons" class="btn btn-success" value="add new service"><i class="fa fa-plus icon-space "></i><?php echo __("Add Article","mp");?></button>
					</td>
				</tr>							
			</tbody>
		</table>
		</form>
	</div>
</div>
<div id="hr"></div>
<div class="tab-pane active" id="">
	<div class="tab-content mp-services-right-details">
		<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div id="accordion" class="panel-group">
			<ul class="nav nav-tab nav-stacked" id="sortable-services" > <!-- sortable-services -->
				<?php 
				$service->service_id = $_POST['service_id'];
				$mp_service_addons = $service->readAll_addons();							
				if(sizeof((array)$mp_service_addons)>0){
				foreach($mp_service_addons as $mp_addon){  ?>
				<li id="service_detail_<?php echo $mp_addon->id; ?>" class="panel panel-default mp-services-panel" >							
					<div class="panel-heading">
						<h4 class="panel-title">
							<div class="col-lg-6 col-sm-7 col-xs-12 np">
								<div class="pull-left">
									<i class="fa fa-th-list"></i>
								</div>	
								<span class="mp-service-title-name f-letter-capitalize"><?php echo $mp_addon->addon_service_name; ?></span>
								
							</div>
							<div class="col-lg-6 col-sm-5 col-xs-12 np">											
								<div class="col-lg-2 col-sm-2 col-xs-4 np">
									<label for="sevice-endis-<?php echo $mp_addon->id; ?>">
										<input data-id="<?php echo $mp_addon->id; ?>" type="checkbox" class="update_service_addon_status" id="sevice-endis-<?php echo $mp_addon->id; ?>" <?php if($mp_addon->status=='E'){echo 'checked'; } ?> data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" >
									</label>
								</div>
								<div class="pull-right">
									<div class="col-lg-2 col-sm-2 col-xs-4 np">
									<a data-poid="mp-popover-delete-service<?php echo $mp_addon->id; ?>" id="mp-delete-service<?php echo $mp_addon->id; ?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this Addon?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete Addon","mp");?>"></i></a>
										<div class="mp-popover" id="mp-popover-delete-service<?php echo $mp_addon->id; ?>" style="display: none;">
											<div class="arrow"></div>
											<table class="form-horizontal" cellspacing="0">
												<tbody>
													<tr>
														<td>
															<?php /* if($service->total_service_bookings()>0){ */if(0>0){?>
															<span class="mp-popover-title"><?php echo __("Unable to delete article,having bookings","mp");?></span>
															<?php }else{?>		
															<button data-id="<?php echo $mp_addon->id; ?>" value="Delete" class="btn btn-danger btn-sm mr-10 delete_addon" type="submit"><?php echo __("Yes","mp");?></button>
															<button data-poid="mp-popover-addon<?php echo $mp_addon->id; ?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button><?php } ?>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>											
									<div class="mp-show-hide pull-right">
										<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $mp_addon->id; ?>" ><!--Added Serivce Id-->
										<label class="mp-show-hide-label" for="<?php echo $mp_addon->id; ?>"></label>
									</div>
								</div>
							</div>										
						</h4>
					</div>
					<div id="" class="service_detail panel-collapse collapse detail-id_<?php echo $mp_addon->id; ?>">
						<div class="panel-body">
							<div class="mp-service-collapse-div col-sm-5 col-md-5 col-lg-5 col-xs-12">
								<form data-sid="<?php echo $mp_addon->id; ?>" id="mp_update_service_addon_<?php echo $mp_addon->id; ?>" method="post" type="" class="slide-toggle mp_update_service_addon" >
									<table class="mp-create-service-table">
										<tbody>
											<input type="hidden" name="addon_service_id" id="addon_service_id" value="<?php echo $_POST['service_id'];?>" />
											<tr>
												<td><label for="mp-service-title<?php echo $mp_addon->id; ?>"><?php echo __("Article Title","mp");?></label></td>
												<td><input type="text" name="u_service_title" class="form-control" id="mp-service-title<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->addon_service_name; ?>" /></td>
											</tr>
											
											<tr>
												<td><label for="mp-service-desc"><?php echo __("Article Image","mp");?></label></td>
												<td>
													<div class="mp-service-image-uploader">
														<img id="bdscad<?php echo $mp_addon->id; ?>addimage" src="<?php if($mp_addon->image==''){ echo $plugin_url_for_ajax.'/assets/images/addon.png';}else{
														echo site_url()."/wp-content/uploads".$mp_addon->image;
														}?>" class="mp-service-image br-100" height="100" width="100">
														
														<label <?php if($mp_addon->image==''){ echo "style='display:block'"; }else{ echo "style='display:none'"; } ?> for="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>" class="mp-service-img-icon-label show_image_icon_add<?php echo $mp_addon->id; ?>">
															<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
															<i class="pull-left fa fa-plus-circle fa-2x"></i>
														</label>
														<input data-us="bdscad<?php echo $mp_addon->id; ?>" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>"  />
														<a  id="mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" <?php if($mp_addon->image==''){ echo "style='display:none;'";}else{ echo "style='display:block'"; }  ?> class="pull-left br-100 btn-danger mp-remove-service-img btn-xs mp_remove_image" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove Service Image","mp");?>"></i></a>
														
														
														
														<div id="popover-mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" style="display: none;">
															<div class="arrow"></div>
															<table class="form-horizontal" cellspacing="0">
																<tbody>
																	<tr>
																		<td>
																			<a href="javascript:void(0)" value="Delete" data-mediaid="<?php echo $mp_addon->id; ?>" data-mediasection='service' data-mediapath="<?php echo $mp_addon->image;?>" data-imgfieldid="bdscad<?php echo $mp_addon->id;?>uploadedimg" class="btn btn-danger btn-sm mp_delete_image"><?php echo __("Yes","mp");?></a>
																			<a href="javascript:void(0)" id="popover-mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" class="btn btn-default btn-sm close_delete_popup" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>	
								<div id="mp-image-upload-popupbdscad<?php echo $mp_addon->id; ?>" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
									<div class="vertical-alignment-helper">
										<div class="modal-dialog modal-md vertical-align-center">
											<div class="modal-content">
												<div class="modal-header">
													<div class="col-md-12 col-xs-12">
														<a data-us="bdscad<?php echo $mp_addon->id; ?>" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>" ><?php echo __("Crop & Save","mp");?></a>
														<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
													</div>	
												</div>
												<div class="modal-body">
													<img id="mp-preview-imgbdscad<?php echo $mp_addon->id; ?>" />
												</div>
												<div class="modal-footer">
													<div class="col-md-12 np">
														<div class="col-md-4 col-xs-12">
															<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>filesize" name="filesize" />
														</div>	
														<div class="col-md-4 col-xs-12">	
															<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>h" name="h" /> 
														</div>
														<div class="col-md-4 col-xs-12">	
															<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>w" name="w" />
														</div>
														<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>x1" name="x1" />
														 <input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>y1" name="y1" />
														<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>x2" name="x2" />
														<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>y2" name="y2" />
														<input id="bdscad<?php echo $mp_addon->id; ?>bdimagetype" type="hidden" name="bdimagetype"/>
														<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>bdimagename" name="bdimagename" value="" />
														</div>
												</div>							
											</div>		
										</div>			
									</div>			
								</div>
								</td>
							<input name="image" id="bdscad<?php echo $mp_addon->id;?>uploadedimg" type="hidden" value="<?php echo $mp_addon->image;?>" />
							</tr>
											<tr>
											
												<td><label for="mp-service-maxqty"><?php echo __("Max Qty","mp");?></label></td>
												<td><input type="text" name="service_maxqty" class="form-control maxqty<?php echo $mp_addon->id; ?>" id="mp-service-addons-maxqty<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->maxqty; ?>" /></td>
											</tr>
										<tr>
											<td><label><?php echo __("Multiple Qty","mp");?></label></td>
										<td>
											<div class="form-group">
											<label for="service_addons_multiple_qty">
												<input type="checkbox" class="addon_multipleqty<?php echo $mp_addon->id;?>" <?php if ($mp_addon->multipleqty == 'Y') { echo"checked"; } ?> id="service_addons_multiple_qty" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" value="<?php echo $mp_addon->multipleqty; ?>"/>
										</label>
										</div>
									</td>
									</tr>
									<tr>
									<td></td>
										<td>
											<button data-service_id="<?php echo $mp_addon->id; ?>" name="" class="btn btn-success mp-btn-width update_service_addon" type="button"><?php echo __("Save","mp");?></button>
											<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
										</td>
									</tr>
										
										</tbody>
									</table>
								</form>
								
							</div>										
						</div>
					</div>
				</li>
				<?php }
				}else{
					echo __("No Article Found.","mp");
				} ?>
				<!-- add new service pop up -->
				<li>
				<div class="panel panel-default mp-services-panel mp-add-new-service-addons">
					<div class="panel-heading">
						<h4 class="panel-title">
							<div class="mp-col6">
								<span class="mp-service-title-name"><?php echo __("Add New Article","mp");?></span>		
							</div>
							<div class="pull-right mp-col6">					
								<div class="pull-right">
										<div class="mp-show-hide pull-right">
										<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addservice" ><!--Added Serivce Id-->
										<label class="mp-show-hide-label" for="addservice"></label>
									</div>
								</div>
							</div>										
						</h4>
					</div>
					<div id="" class="service_detail panel-collapse collapse in detail-id_addservice">
						<div class="panel-body">
							<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
								<form id="mp_create_service_addon" method="post" type="" class="slide-toggle" >
									<table class="mp-create-service-table">
										<tbody>
											<input type="hidden" name="addon_service_id" id="addon_service_id" value="<?php echo $_POST['service_id'];?>" />
											<tr>
												<td><label for="mp-service-title"><?php echo __("Article Title","mp");?></label></td>
												<td><input type="text" name="service_title" class="form-control" id="mp-service-addons-title" /></td>
											</tr>
											
											<tr>
												<td><label for="mp-service-desc"><?php echo __("Article Image","mp");?></label></td>
												<td>
													<div class="mp-service-image-uploader">
														<img id="bdscadlocimage" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/service.png" class="mp-service-image br-100" height="100" width="100">
														<label for="mp-upload-imagebdscad" class="mp-service-img-icon-label">
															<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
															<i class="pull-left fa fa-plus-circle fa-2x"></i>
														</label>
														<input data-us="bdscad" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdscad"  />
														
														<a style="display: none;" id="mp-remove-service-imagebdscad" class="pull-left br-100 btn-danger mp-remove-service-img btn-xs" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove Article Image","mp");?>"></i></a>
														<div id="popover-mp-remove-service-imagebdscad" style="display: none;">
															<div class="arrow"></div>
															<table class="form-horizontal" cellspacing="0">
																<tbody>
																	<tr>
																		<td>
																			<a href="javascript:void(0)" id="" value="Delete" class="btn btn-danger btn-sm" type="submit"><?php echo __("Yes","mp");?></a>
																			<a href="javascript:void(0)" id="mp-close-popover-service-imagebdscad" class="btn btn-default btn-sm" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div><!-- end pop up -->
													</div>
													<div id="mp-image-upload-popupbdscad" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
														<div class="vertical-alignment-helper">
															<div class="modal-dialog modal-md vertical-align-center">
																<div class="modal-content">
																	<div class="modal-header">
																		<div class="col-md-12 col-xs-12">
																			<a data-us="bdscad" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdscad"><?php echo __("Crop & Save","mp");?></a>
																			<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
																		</div>	
																	</div>
																	<div class="modal-body">
																		<img id="mp-preview-imgbdscad" />
																	</div>
																	<div class="modal-footer">
																		<div class="col-md-12 np">
																			<div class="col-md-4 col-xs-12">
																				<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="bdscadfilesize" name="filesize" />
																			</div>	
																			<div class="col-md-4 col-xs-12">	
																				<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="bdscadh" name="h" /> 
																			</div>
																			<div class="col-md-4 col-xs-12">	
																				<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="bdscadw" name="w" />
																			</div>
																			<input type="hidden" id="bdscadx1" name="x1" />
																			<input type="hidden" id="bdscady1" name="y1" />
																			<input type="hidden" id="bdscadx2" name="x2" />
																			<input type="hidden" id="bdscady2" name="y2" />
																			<input id="bdscadbdimagetype" type="hidden" name="bdimagetype"/>
																			<input type="hidden" id="bdscadbdimagename" name="bdimagename" value="" />
																		</div>
																	</div>							
																</div>		
															</div>			
														</div>			
													</div>
														<input name="service_image" id="bdscaduploadedimg" type="hidden" value="" />						
												</td>
											</tr>
											<tr>
												<td><label for="mp-service-maxqty"><?php echo __("Max Qty","mp");?></label></td>
												<td><input type="text" name="service_maxqty" class="form-control maxqty" id="mp-service-addons-maxqty" /></td>
											</tr>
											<tr>
												<td><label><?php echo __("Multiple Qty","mp");?></label></td>
												<td>
													<div class="form-group">
														<label for="service_addons_multiple_qty">
															<input type="checkbox" class="addon_multipleqty" id="service_addons_multiple_qty" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
														</label>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								
							</div>
							<table class="col-sm-7 col-md-7 col-lg-7 col-xs-12 mt-20 mb-20">
								<tbody>
									<tr>
										<td></td>
										<td>
											<button id="mp_create_service_addons" name="mp_create_service_addons" class="btn btn-success mp-btn-width col-md-offset-4" type="button" ><?php echo __("Save","mp");?></button>
											<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
										</td>
									</tr>
								</tbody>
							</table>
							
							
							</form>									
						</div>
					</div>
				</div>
				</li>
				
				</ul>
			</div>	
		</div>
	</div>
</div>
<?php }
if(isset($_POST['position'],$_POST['service_action']) && $_POST['position']!='' && $_POST['service_action']=='sort_service_position'){
		parse_str($_POST['position'], $output);
		$order_counter=0;
		foreach ($output as $order_no){
			foreach($order_no as $order_value){			 
			  $service->position = $order_counter;
			  $service->id = $order_value;
			  $service->sort_service_position();
			$order_counter++;
			}
		}
}	


 ?>			