<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	$city = new moveto_city();	
	set_time_limit(0);
	
/* Create New City */
if(isset($_POST['city_source']) && $_POST['city_source']!=''){
	
	if(isset($_FILES['bulk_add_routes']['name']) && isset($_FILES["bulk_add_routes"]["tmp_name"]) && $_FILES["bulk_add_routes"]["tmp_name"]!='' && $_FILES['bulk_add_routes']['name']!=''){
		
		$routesfile = fopen($_FILES["bulk_add_routes"]["tmp_name"],"r");
		$validroutes = array();  
		$invalidroutes = array();  
		
		while(($city_routes = fgetcsv($routesfile))!==false){		
			if($city_routes[0]!='' && $city_routes[1]!=''){
				$city->source_city = $city_routes[0];
				$city->destination_city = $city_routes[1];
				$routestatus = $city->check_route_csv();				
				if($routestatus==true){				
					$city->create(); 
					$validroutes[] = array('source'=>$city_routes[0],'destination'=>$city_routes[1]);		
				}else{
					$invalidroutes[] = array('source'=>$city_routes[0],'destination'=>$city_routes[1]);	
				}							
			}else{				
				$invalidroutes[] = array('source'=>$city_routes[0],'destination'=>$city_routes[1]);
			
			}
			
		}
		fclose($routesfile);
		if(sizeof((array)$validroutes)>0){ ?>
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?php echo __((sizeof((array)$validroutes)-1)." routes created successfully!","mp");?></strong>
		</div>
			
		<?php }
		if(sizeof((array)$invalidroutes)>0){ ?>
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?php echo __((sizeof((array)$invalidroutes)-1)." routes not created or exist already!","mp");?></strong>
		</div>		
		<?php }
	}else{	
		$city->source_city = $_POST['city_source'];
		$city->destination_city = $_POST['city_destination'];
		$city->price = $_POST['city_price'];
		
		if($city->create()) { ?>	
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?php echo __("Route created successfully!","mp");?></strong>
		</div>
		<?php }else{ ?>
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong><?php echo __("Unable to create route!","mp");?></strong>
		</div>
		<?php } 
	}
	
}

	
	/* Get All city */
	$mp_cities = $city->readAll();
	$all_cities = $city->countAll();

?>
<div id="mp-city-panel" class="panel tab-content table-fixed">
	
	<div class="panel-body table-cell col-md-9 col-sm-9 col-xs-12 col-lg-9">
		<div class="mp-city-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<!-- right side common menu for city -->
			<div class="mp-city-top-header">
				<span class="mp-city-city-name pull-left" id="mp-category-title"></span>
				
				<div class="pull-right">
					<table>
						<tbody>
							<tr>								
								<td>
									<button id="mp-add-new-city" class="btn btn-success" value="add new city"><i class="fa fa-plus icon-space "></i><?php echo __("Add Route","mp");?></button>
								</td>
							
							</tr>
						</tbody>
					</table>
			</div>
			</div>
			<div id="hr"></div>

			<div class="tab-pane active" id=""><!-- city list -->
				<div class="tab-content mp-city-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-city" > <!-- sortable-city -->
							<?php foreach($mp_cities as $mp_city){ ?>
							<li id="city_detail_<?php echo $mp_city->id; ?>" class="panel panel-default mp-city-panel" >
								<div class="panel-heading">
									<h4 class="panel-title">
									
										<div class="col-lg-5 col-sm-12 col-xs-12 np">
											<span class="custom-width-auto mp-city-title-name f-letter-capitalize"><?php echo $mp_city->source_city; ?> <i class="fa fa-exchange" aria-hidden="true"></i> <?php echo $mp_city->destination_city; ?></span> 
										</div>
										<div class="col-lg-7 col-sm-12 col-xs-12 np">								
											<div class="pull-right">
												<div class="col-lg-2 col-sm-1 col-xs-4 np">
												<a data-poid="mp-popover-delete-city<?php echo $mp_city->id; ?>" id="mp-delete-city<?php echo $mp_city->id; ?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this route?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete city","mp");?>"></i></a>
													<div class="mp-popover" id="mp-popover-delete-city<?php echo $mp_city->id; ?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<button data-id="<?php echo $mp_city->id; ?>" value="Delete" class="btn btn-danger btn-sm mr-10 delete_city" type="submit"><?php echo __("Yes","mp");?></button>
																		<button data-poid="mp-popover-city<?php echo $mp_city->id; ?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $mp_city->id; ?>" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="<?php echo $mp_city->id; ?>"></label>
												</div>
											</div>
										</div>
										
									</h4>
								</div>
								<div id="" class="city_detail panel-collapse collapse detail-id_<?php echo $mp_city->id; ?>">
									<div class="panel-body">
										<div class="mp-city-collapse-div col-sm-7 col-md-7 col-lg-7 col-xs-12">
											<form data-cid="<?php echo $mp_city->id; ?>" id="mp_update_city_<?php echo $mp_city->id; ?>" method="post" type="" class="slide-toggle mp_update_city" >
												<table class="mp-create-city-table">
													<tbody>
														<tr>
															<td><label for="mp-city-source<?php echo $mp_city->id; ?>"><?php echo __("Source","mp");?></label></td>
															<td><input type="text" id="mp-city-source<?php echo $mp_city->id; ?>" name="u_city_source" class="form-control" value="<?php echo $mp_city->source_city; ?>"></td>
														</tr>
														<tr>
															<td><label for="mp-city-destination<?php echo $mp_city->id; ?>"><?php echo __("Destination","mp");?></label></td>
															<td><input type="text" name="u_city_destination" class="form-control" id="mp-city-destination<?php echo $mp_city->id; ?>" value="<?php echo $mp_city->destination_city; ?>" /></td>
														</tr>
														
													</tbody>
													<tfoot>
														<tr>
															<td></td>
															<td>
																<a href="javascript:void(0);" data-city_id="<?php echo $mp_city->id; ?>" name="" class="btn btn-success mp-btn-width col-md-offset-4 update_city" type="submit"><?php echo __("Update","mp");?></a>
																<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
															</td>
														</tr>
													</tfoot>
												</table>
											</form>
										</div>									
										
									</div>
								</div>
							
							</li>
							<?php } ?>
							<!-- add new city pop up -->
							<li>
							<div class="panel panel-default mp-city-panel mp-add-new-city mp-location-panel">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="mp-col6">
											<span class="mp-city-title-name"><?php echo __("Add New route","mp");?></span>		
										</div>
										<div class="pull-right mp-col6">					
											<div class="pull-right">
													<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addcity" ><!--  Added Serivce Id-->
													<label class="mp-show-hide-label" for="addcity"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div class="city_detail panel-collapse collapse in detail-id_addcity" id="citydropnew">
									<div class="panel-body">
										<div class="mp-city-collapse-div col-sm-7 col-md-7 col-lg-7 col-xs-12"  style="display:block">
											<form id="mp_add_city" enctype="multipart/form-data" action="?page=routes_submenu"  method="post" class="slide-toggle" >
												<table id="mp_create_city_single" class="mp-create-city-table">
													<tbody>														
														<tr>
															<td><label for="mp-city-title"><?php echo __("Source","mp");?></label></td>
															<td><input type="text" name="city_source" class="form-control" id="mp-city-cource" /></td>
														</tr>
														
														<tr>
															<td><label for="mp-city-desc"><?php echo __("Destination","mp");?></label></td>
															<td><input type="text" name="city_destination" class="form-control" id="mp-city-destination" /></td>
														</tr>
																										
													</tbody>
												</table>

												<table id="mp_create_city_bulk" class="mp-create-city-table">
													<tbody>														
														<tr>
															<td><label for="mp-city-title"><?php echo __("Choose CSV File","mp");?></label></td>
															<td><input type="file" name="bulk_add_routes" class="form-control" accept=".csv" id="mp-city-routes" />
															<input type="hidden" id="bulk_add_status" name="bulk_add_status" class="form-control" value="N" />
															</td>
														</tr>
														
														<tr>
															<td><label for="mp-city-desc"><?php echo __("Download Sample CSV File","mp");?></label></td>
															<td><a href="<?php echo $plugin_url_for_ajax;?>/admin/route-sample.csv"><?php echo __("Download File","mp");?></a></td>
														</tr>													
														<tr>
															<td></td>
															<td>
																<button id="mp_create_city" name="mp_create_city" class="btn btn-success mp-btn-width col-md-offset-4" type="button"><?php echo __("Save","mp");?></button>
																
																<button style="display:none;" id="mp_create_city_submit" name="mp_create_city" class="btn btn-success mp-btn-width col-md-offset-4" type="submit"><?php echo __("Save","mp");?></button>
																
																<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
															</td>
														</tr>
													</tbody>
												</table>
											</form>	
										</div>											
																	
									</div>
								</div>
							</div>
							</li>
							
							</ul>
						</div>	
					</div>
				</div>
			</div>
			
		</div>
			
	</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var cityObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>