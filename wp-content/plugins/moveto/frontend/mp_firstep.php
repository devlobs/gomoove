<?php
if (!session_id()) {
    @session_start();
}

$_SESSION['move_cart'] = array();
$_SESSION['distances'] = '';
$_SESSION['distance_price'] = '';
$_SESSION['additional_info_price'] = '';
$_SESSION['first_step_data'] = array();
$_SESSION['booking_id'] = '';
$_SESSION['final_amt'] = '';
$_SESSION['loadinginfo'] = array();
$_SESSION['loadinginfo']['load_elevator_amt'] = 0;
$_SESSION['loadinginfo']['load_elevator_checked'] = "No";
$_SESSION['loadinginfo']['packaging_amt'] = 0;
$_SESSION['loadinginfo']['packaging_checked'] = "No";
$_SESSION['unloadinginfo'] = array();
$_SESSION['unloadinginfo']['unload_elevator_amt'] = 0;
$_SESSION['unloadinginfo']['unload_elevator_checked'] = "No";
$_SESSION['unloadinginfo']['unpackaging_amt'] = 0;
$_SESSION['unloadinginfo']['unpackaging_checked'] = "No";
$_SESSION['total_amt'] = '';
$_SESSION['tax'] = '';
$_SESSION['sub_total'] = '';
$_SESSION['discount'] = '';
$_SESSION['per_quibcfeets_rate'] = 0;
$_SESSION['cubicfeets'] = 0;
$_SESSION['per_quibcfeets_volume'] = '';
$_SESSION['insurance_rate'] = 0;
$_SESSION['session_checked'] = "off";
$_SESSION['selected_insurance'] = 0;

$moveto_google_api_key = get_option('moveto_google_api_key');
$security = "";
if ($moveto_google_api_key != "") {
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
        $security = "https";
    } else if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'on') {
        $security = "http";
    } else if (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] === 'http') {
        $security = "http";
    } else if (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] !== 'http') {
        $security = "https";
    }
    ?>

    <script type="text/javascript"
            src="<?php echo $security; ?>://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $moveto_google_api_key; ?>"></script>
    <?php
}
$actual_link = $security . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$_SESSION['booking_home'] = $actual_link;

include_once(dirname(dirname(__FILE__)) . '/objects/class_general.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_moveto_size.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_cities.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_service.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_booking.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_category.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_provider.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_front_moveto_first_step.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_settings.php');
include_once(dirname(dirname(__FILE__)) . '/objects/class_distance.php');

$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));

$mp_mulitlocation_status = get_option('moveto_multi_location');
$mp_zipcode_booking_status = get_option('moveto_zipcode_booking');
$provider_avatar_view = get_option('moveto_show_provider_avatars');
$moveto_primary_color = get_option('moveto_primary_color');
$moveto_secondary_color = get_option('moveto_secondary_color');
$moveto_text_color = get_option('moveto_text_color');
$moveto_bg_text_color = get_option('moveto_bg_text_color');
$moveto_distance_unit = get_option('moveto_distance_unit');
if ($moveto_distance_unit == "km") {
    $moveto_distance_unit = "Km";
} else {
    $moveto_distance_unit = "Mi";
}
$insurance_permission = get_option('moveto_insurance_permission');


$move_pricing_status = get_option('move_pricing_status');


/* $mp_move_pricing_status = 'D';
if(get_option('move_pricing_status')=='E' || get_option('move_pricing_status')=='E'){
	$mp_move_pricing_status = 'E';
} */


$mp_general = new moveto_general();
$mp_movesize = new movesize();
$mp_city = new moveto_city();
$additional_info = new moveto_additional_info();
$first_step = new moveto_first_step();
$mp_booking = new moveto_booking();
$alloffdays = $first_step->get_off_days();
$allroutes = $mp_city->readAll();
$onload_location_id = 0;
$enabled_additionalinfo = array();
$alladditional_info = $additional_info->readAll_enable();

$read_room_type = new moveto_room_type();
$read_article = new moveto_room_type();
$additional_info = new moveto_distance();
$elevators = new moveto_distance();
$floor_no = new moveto_distance();

$mp_settings = new moveto_settings();
$mp_settings->readAll();

$moveto_cubic_meter = get_option('moveto_cubic_meter');
if ($moveto_cubic_meter == "ft_3") {
    $moveto_cubic_unit = "Cubic Feet";
} else {
    $moveto_cubic_unit = "Cubic Meter";
}

?>
<style>
    #mp .progress-meter .progress,
    #mp .bar:before,
    #mp .bar:after,
    #mp .loader .TruckLoader,
    #mp .loader .TruckLoader:before,
    #mp .TruckLoader:after,
    #mp .loader .TruckLoader-cab,
    #mp .loader .TruckLoader-cab:before,
    #mp .TruckLoader-cab:after,
    #mp .loader .TruckLoader-cab:after,
    #mp .mp_btn,
    #mp .mp_btn:hover,
    #mp input[type='range'].range-slider__range,
    #mp .range-slider__range::-webkit-slider-thumb,
    #mp .range-slider__range:active::-webkit-slider-thumb {
        background: <?php echo $moveto_secondary_color; ?> !important;
        background-color: <?php echo $moveto_secondary_color; ?> !important;
    }

    #mp .mp_btn {
        border-color: <?php echo $moveto_primary_color; ?> !important;
    }

    #mp .custom-control-input:checked ~ .custom-control-label::before,
    #mp input[type='date']:focus,
    #mp input[type='time']:focus,
    #mp input[type='datetime-local']:focus,
    #mp input[type='week']:focus,
    #mp input[type='month']:focus,
    #mp input[type='text']:focus,
    #mp input[type='email']:focus,
    #mp input[type='url']:focus,
    #mp input[type='password']:focus,
    #mp input[type='search']:focus,
    #mp input[type='tel']:focus,
    #mp input[type='number']:focus,
    #mp textarea:focus {
        border-color: <?php echo $moveto_secondary_color; ?> !important;
    }

    .toggle-button-wrapp#toggle_btn .checkbox:checked ~ .layer {
        background-color: <?php echo $moveto_secondary_color; ?>42 !important;
    }

    /* secondary color  */
    #mp .radio_boxed_wrapper input[type='radio']:checked + .custom_radio_box,
    #mp .radio_boxed_wrapper:hover .custom_radio_box,
    #mp .toggle-button-wrapp#toggle_btn .checkbox:checked + .knobs:before,
    #mp .tabs-left > li.active > a,
    #mp .tabs-left > li.active > a:hover,
    #mp .tabs-left > li.active > a:focus {
        background: <?php echo $moveto_secondary_color; ?> !important;
        background-color: <?php echo $moveto_secondary_color; ?> !important;
    }

    #mp .range-slider__range:focus::-webkit-slider-thumb {
        box-shadow: 0 0 0 3px #fff, 0 0 0 6px <?php echo $moveto_secondary_color; ?> !important;
    }

    #mp .radio_boxed_wrapper input[type='radio']:checked + .custom_radio_box,
    #mp .radio_boxed_wrapper:hover .custom_radio_box,
    {
        border-color: <?php echo $moveto_primary_color; ?> !important;
    }

    #mp .custom_addon_checkbox input[type="checkbox"]:checked + label:after {
        border-top-color: <?php echo $moveto_secondary_color; ?> !important;
    }

    #mp .custom_addon_checkbox input[type="checkbox"]:checked + label {
        border: 1px solid <?php echo $moveto_secondary_color; ?> !important;
    }

    #mp .radio_boxed_wrapper input[type="radio"]:checked + .custom_radio_box {
        box-shadow: 2px 2px 8px -2px <?php echo $moveto_secondary_color; ?> !important;
        border-color: <?php echo $moveto_secondary_color; ?> !important;
    }

    /* text color */

    body,
    #mp,
    #mp .step-nav li.active,
    #mp .progress-meter .progress-point.completed,
    #mp .progress-meter .progress-point.active,
    #mp .progress-meter .step-title,
    #mp .focused_label,
    #mp label.common_label,
    #mp .range-slider__value,
    #mp .range-slider__value:after,
    #mp .common_label,
    #mp .com-col,
    #mp .minus,
    #mp .plus,
    #mp .tabs-left > li a,
    #mp .custom_radio_box,
    #mp .common-inner .via_style_minus,
    #mp #booking_summary .summary_title,
    #mp #booking_summary .right-cart .cart-title,
    #mp #booking_summary .right-cart .cart-value,
    #mp .load-unload .main-title,
    #mp .load-unload .flool-count h4,
    #mp .load-unload .unflool-count h4,
    #mp .load-unload .extra_element .element_title,
    #mp .additional_info .additional_info_listing .info_title {
        color: <?php echo $moveto_text_color; ?> !important;
    }

    /* text color with bg */


    #mp .radio_boxed_wrapper input[type='radio']:checked + .custom_radio_box,
    #mp .mp_btn,
    #mp .tabs-left > li.active > a,
    #mp .tabs-left > li.active > a:hover,
    #mp .tabs-left > li.active > a:focus {
        color: <?php echo $moveto_bg_text_color; ?> !important;
    }

    #mp .range-slider__value:after {
        content: "<?php echo $moveto_distance_unit; ?>";
        color: #333;
        font-size: 16px;
        padding-left: 5px;
    }

    .pac-container:after {
        /* Disclaimer: not needed to show 'powered by Google' if also a Google Map is shown */

        background-image: none !important;
        height: 0px;
    }

    #add_amt_Val {
        background: #fd3943;
    }

    #add_amt_Val:hover {
        background: #fd3943;
    }

    .custom_insurance {
        padding: unset !important;
    }
</style>
<?php
if (get_option("moveto_payment_method_Stripe") == "E") { ?>
    <script src="https://js.stripe.com/v3/"></script>
<?php } ?>
<script>
    var mp_stripeObj = {
        'status_check': '<?php echo get_option('moveto_payment_method_Stripe'); ?>',
        'pubkey': '<?php echo get_option('moveto_stripe_publishableKey'); ?>'
    };
</script>

<div class="mp-wrapper" id="mp"> <!-- main wrapper -->
    <div class="loader">
        <div class="mp-loader">
            <div class="TruckLoader">
                <div class="TruckLoader-cab"></div>
                <div class="TruckLoader-smoke"></div>
            </div>

        </div>
    </div>
    <div class="containerd">
        <div class="mp-main-wrapper box-border">
            <section>
                <main id="mp-main">
                    <!-- main inner content -->
                    <section
                            class="mp-display-middle mp-main-left mp-lg-12 mp-md-12 mp-sm-12 mp-xs-12 np pull-left no-sidebar-right mp_remove_left_sidebar_class">
                        <div class="mp-main-inner fullwidth">
                            <div class="mp-booking-step" data-current="1">
                                <ul class="mp-list-inline nm">


                                    <!-- Start component -->
                                    <div class="progress-meter">
                                        <div class="track">
                                            <span class="progress"></span>
                                        </div>
                                        <ol class="progress-points" data-current="2">
                                            <li class="progress-point active" id="first_step_progress">
                                                <span class="step-title"><?php echo __("Move info", "mp"); ?></span>
                                            </li>
                                            <li class="progress-point" id="second_step_progress">
                                                <span class="step-title"><?php echo __("Articles", "mp"); ?></span>
                                            </li>
                                            <li class="progress-point" id="third_step_progress">
                                                <span class="step-title"><?php echo __("Location", "mp"); ?></span>
                                            </li>
                                            <li class="progress-point" id="forth_step_progress">
                                                <span class="step-title"><?php echo __("Done", "mp"); ?></span>
                                            </li>
                                        </ol>
                                    </div>

                                </ul>
                            </div>
                            <div class="visible cb" id="mp_first_step">
                                <div class="mp-first-step form-inner visible">

                                    <div class="mp-form-common mp-md-12 mp-lg-12 mp-sm-12 mp-xs-12 pull-left">
                                        <form method="post" action="" id="validate_form" class="validate_form_data">
                                            <div class="common-inner">
                                                <div class="heading-common mb-28 ml-10">
                                                    <h3> location </h3>
                                                </div>
                                                <div class="mp-md-12 mp-xs-12 mtb-10">
                                                    <div class="row">
                                                        <div class="mp-md-6">
                                                            <div class="mp-md-12 pt-5 pb-5">
                                                                <!-- <div class="mp-md-3 mp-xs-12">
                                                                    <label class="common_label">Source</label>
                                                                </div> -->
                                                                <div class="mp-md-10 mp-xs-9">

                                                                    <div class="row pl-5 pr-5 fancy_input_wrap2">

                                                                        <input class="fancy_input custom-input pl-0"
                                                                               type="text" id="source_city"
                                                                               name="valid_source" placeholder=""
                                                                               required>
                                                                        <span class="highlight"></span>
                                                                        <span class="bar"></span>

                                                                        <label class="fancy_label l-0"
                                                                               for="name">Source</label>

                                                                    </div>

                                                                </div>

                                                                <div class="mp-md-2 mp-xs-3" id="add_via">
                                                                    <i class="fa fa-plus via_style" aria-hidden="true"
                                                                       id="btn_plus"></i>
                                                                    <i class="fa fa-minus via_style_minus dp-n"
                                                                       aria-hidden="true" id="btn_minus"></i>
                                                                    <span class="common_label">Via</span>
                                                                </div>

                                                            </div>
                                                            <div class="mp-md-12  pt-5 pb-5 dp-n" id="via">

                                                                <div class="mp-md-10 mp-xs-9">

                                                                    <div class="row pl-5 pr-5 fancy_input_wrap2">

                                                                        <input class="fancy_input custom-input"
                                                                               type="text" id="via_city"
                                                                               name="valid_via" placeholder="" required>

                                                                        <span class="highlight"></span>
                                                                        <span class="bar"></span>

                                                                        <label class="fancy_label">Via</label>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="mp-md-12 pt-5 pb-5">

                                                                <div class="mp-md-10 mp-xs-9">

                                                                    <div class="row pl-5 pr-5 fancy_input_wrap2 nmb">

                                                                        <input class="fancy_input custom-input pl-0"
                                                                               type="text" id="destination_city"
                                                                               name="valid_destination" placeholder=""
                                                                               required>
                                                                        <span class="highlight"></span>
                                                                        <span class="bar"></span>

                                                                        <label class="fancy_label l-0">Destination</label>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                            <div class="mp-md-12">
                                                                <div class="mp-md-10 mp-xs-9">
                                                                    <span class="error_invalid_cities"
                                                                          style="color: #ff0000;margin-left: 2%;font-size: 11px;font-weight: 700;"></span>
                                                                </div>
                                                            </div>

                                                        </div>    <!-- main div mp-md-6 closed -->


                                                        <div class="mp-md-6">
                                                            <div id="dvMap"
                                                                 style="width: 100%; height: 250px; display: none;"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mp-md-12 pt-5 mb-20">
                                                    <div class="mp-md-3 mp-xs-12 pl-5">
                                                        <label class="common_label mt-30">Approx Distance</label>
                                                    </div>
                                                    <div class="mp-md-9 mp-xs-12 np">
                                                        <div class="range-slider pl-5">
                                                            <span id="span_counter" class="range-slider__value">0</span>
                                                            <input id="range_counter" class="range-slider__range"
                                                                   step="1" type="range" min="0"
                                                                   max="<?php echo get_option('moveto_max_distance'); ?>"
                                                                   value="1">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mp-md-12 mp-xs-12 pt-5 mtb-10 move_size_sh">
                                                    <div class="heading-common mb-28">
                                                        <h3> Move Size </h3>
                                                    </div>

                                                    <div class="row mp-md-12 mp-xs-12 mb-30">
                                                        <?php
                                                        $i = 1;
                                                        $movesize_record = $mp_movesize->read_movesize();

                                                        if (!empty($movesize_record)) {
                                                            foreach ($movesize_record as $movesize) {
                                                                ?>
                                                                <label for="<?php echo $movesize->name; ?>"
                                                                       class="radio_boxed_wrapper">
                                                                    <input type="radio"
                                                                           id="<?php echo $movesize->name; ?>"
                                                                           name="BHK_Radio"
                                                                           value="<?php echo $movesize->name; ?>"
                                                                           class=""/>
                                                                    <div class="custom_radio_box msize"><?php echo $movesize->name; ?></div>
                                                                </label>
                                                                <?php
                                                                $i++;
                                                            }

                                                        } else {
                                                            ?>
                                                            <!-- <div style="padding-top:4.5%;padding-left:6%;">
                                                                <span style=" color: #ff0000;font-size: 11px;font-weight: 700;">Please add move size</span>
                                                            </div> -->
                                                        <?php }
                                                        ?>
                                                        <div>
                                                            <span class="error_msg_movesize"
                                                                  style=" color: #ff0000;font-size: 11px;font-weight: 700;margin-left: 1%;"></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="">
                                                    <div class="heading-common mb-28 ml-10">
                                                        <h3> your contact </h3>
                                                    </div>
                                                    <div class="mp-md-12 pt-5 pb-5 mt-10">

                                                        <div class="mp-md-5 mp-xs-12">

                                                            <div class="row pl-5 pr-5 fancy_input_wrap2">

                                                                <input class="fancy_input custom-input" id="name"
                                                                       name="valid_name" type="text" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">Name</label>

                                                            </div>
                                                        </div>

                                                        <div class="mp-md-5 mp-xs-12 input_ml">

                                                            <div class="row pl-5 pr-5 fancy_input_wrap2 height_35"
                                                                 id="front-datepicker">

                                                                <input class="fancy_input custom-input" type="text"
                                                                       placeholder="" readonly="readonly" id="date"
                                                                       name="valid_date" required>

                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">Date</label>
                                                                <span class="error error_msg_date"
                                                                      style="display: inline-block;color: #ff0000;margin-left: 2%;font-size: 11px;font-weight: 700;"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="mp-md-12 pt-5 pb-5 mt-10">

                                                        <div class="mp-md-5 mp-xs-12">

                                                            <div class="row pl-5 pr-5 fancy_input_wrap2">

                                                                <input class="fancy_input custom-input" id="email"
                                                                       name="valid_email" type="text" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">Email</label>

                                                            </div>
                                                        </div>

                                                        <div class="mp-md-5 mp-xs-12 input_ml pd-0">
                                                            <span class="input-group-addon input_grpup_code custom-tel"><span
                                                                        class="company_country_code_value"><?php echo $mp_settings->moveto_company_country_code; ?></span></span>
                                                            <div class="row pl-5 pr-5 fancy_input_wrap2 mr_5">

                                                                <input class="fancy_input custom-input pl-0 "
                                                                       type="text" id="phone" name="valid_phone"
                                                                       required maxlength="10">
                                                                <!--<i class="lni-phone-handset common-icone"></i> -->

                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label l-0">Phone</label>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                    </div>
                                    <div class="mp-md-11 mp-xs-11 mtb-15">
                                        <a href="javascript:void(0)" id="first_step_submit"
                                           class="mp_btn article_btn f-right">Select Article <i
                                                    class="fa fa-angle-right"></i></a>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="visible cb" style="display: none;" id="mp_second_step">
                            <div class="mp-second-step form-inner visible">

                                <div class="mp-form-common mp-md-8 mp-lg-8 mp-sm-12 mp-xs-12">
                                    <div class="common-inner">
                                        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <ul class="nav nav-tabs tabs-left sideways">
                                                <?php
                                                $all_records = $read_room_type->readAll_room_type_for_front();
                                                ?>
                                                <input class="" type="hidden" id="last_id_trigger"
                                                       data-id="<?php echo $all_records[0]->id; ?>"> <?php
                                                $i = 0;
                                                foreach ($all_records as $room_type) {
                                                    ?>
                                                    <li class="room_type_cat"
                                                        id="<?php if ($i == 0) echo "first_cat"; ?>"
                                                        data-id="<?php echo $room_type->id; ?>"
                                                        data-name="<?php echo $room_type->name; ?>"><a
                                                                href="#<?php echo trim($room_type->name); ?>"
                                                                data-toggle="tab" id="<?php echo $room_type->id; ?>"
                                                                name="<?php echo $room_type->name; ?>"><?php echo $room_type->name; ?></a>
                                                    </li>
                                                    <?php $i++;
                                                } ?>
                                                <!-- Bhupendra Custom -->
                                                <li class="room_type_cat cust_quick" id="first_cat" data-id="4"
                                                    data-name="qucikbid"><a href="#" data-toggle="tab" id="4"
                                                                            name="qucikbid">Custom</a></li>
                                                <!-- Bhupendra Custom -->
                                            </ul>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="articles_tab tab-content">

                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 multi_quick"
                                             style="display: none;">

                                        </div>


                                    </div>
                                </div>
                                <!-- <div class="mp-form-common mp-md-4 mp-lg-4 mp-sm-4 mp-xs-12 r-cart-a">
                                    <div class="mp-xs-12" id="booking_summary">
                                                <div class="right-cart">
                                                <div class="summary_title">Estimated Cost</div>
                                                <div class="summary_list">
                                                <ul class="cart_li">
                                                </ul>
                                                </div>

                                                <div class="mp-xs-12 npl">
                                                <h3 class="estimate-cost cart_title lod_esti_title cart_details"></h3>
                                                </div>
                                                <div class="summary_list">
                                                <ul class="estimate_div">
                                                <h3 class="load_floor cart_details"></h3>
                                                <li class="estimate-cost third_step_val_div"></li>
                                                </ul>
                                                <ul>
                                                <h3 class="estimate-cost elevator cart_details"></h3>
                                                <li class="estimate-cost third_step_val_div_elevators"></li>
                                                </ul>
                                                <ul>
                                                <h3 class="estimate-cost packing cart_details"></h3>
                                                <li class="estimate-cost third_step_val_div_packing"></li>
                                                </ul>
                                                </div>

                                                <div class="mp-xs-12 npl">
                                                <h3 class="cart_title unlod_esti_title cart_details"></h3>
                                                </div>
                                                <div class="summary_list">
                                                <ul class="estimate_div">
                                                <h3 class="estimate-cost unload_floor cart_details"></h3>
                                                <h3 class="estimate-cost third_step_val_div_unload"></h3>
                                                </ul>
                                                <ul>
                                                <h3 class="estimate-cost unelevator cart_details"></h3>
                                                <li class="estimate-cost unload_ele"></li>
                                                </ul>
                                                <ul>
                                                <h3 class="estimate-cost unpacking cart_details"></h3>
                                                <li class="estimate-cost third_step_val_div_unpacking"></li>
                                                </ul>
                                                </div>

                                                <div class="mp-xs-12 npl">
                                                <h3 class="estimate-cost additional_info_title cart_details"></h3>
                                                </div>
                                                <div class="summary_list">
                                                <ul class="estimate_div">
                                                <h3 class="estimate-cost additional_info_sub"></h3>
                                                <h3 class="estimate-cost additional_info_amt"></h3>
                                                </ul>

                                                </div>
                                                <div class="mp-xs-12 npl">
                                                <h3 class="estimate-cost">  Estimated Cost  </h3>
                                                </div>
                                                <div class="mp-xs-12 text-center cart-bg">
                                                <label class="est_cost"></label>
                                                <h1 class="cart-cost total_cost"></h1>
                                                </div>
                                                </div>

                                            </div>
                                </div> -->
                                <div class="center-ipad-cart">
                                    <div class="mp-form-common mp-md-4 mp-lg-4 mp-sm-6 mp-xs-12 r-cart-a">
                                        <div class="mp-xs-12" id="booking_summary"
                                             style="padding-left: 0; padding-right: 0;">
                                            <div class="right-cart">
                                                <div class="summary_title">Estimated Cost</div>
                                                <div class="summary_list">
                                                    <ul class="cart_li">
                                                    </ul>
                                                </div>
                                                <!-- <div class="mp-xs-12 npl">
                                                    <h3 class="cart_title cubic_feets cart_details" style="width: 100%;"></h3>
                                                </div> -->
                                                <div class="mp-xs-12">
                                                    <div class="cart-listing">
                                                        <h3 class="cart-title cart_title_cubic"></h3>
                                                        <!-- <h4 class="cart_cubic_value"></h4> -->
                                                        <h4 class="cart_cubic_volume"></h4>
                                                    </div>
                                                </div>

                                                <?php
                                                if (get_option("move_pricing_status") == "Y") { ?>

                                                    <div class="mp-xs-12 npl" style="margin-top: 10px;">
                                                        <h3 class="estimate-cost cart_title lod_esti_title cart_details"
                                                            style="width: 100%;"></h3>
                                                    </div>
                                                    <div class="summary_list">
                                                        <ul class="estimate_div">
                                                            <h3 class="load_floor cart_details"></h3>
                                                            <li class="estimate-cost third_step_val_div"></li>
                                                        </ul>
                                                        <ul>
                                                            <h3 class="estimate-cost elevator cart_details"></h3>
                                                            <li class="estimate-cost third_step_val_div_elevators"></li>
                                                        </ul>
                                                        <ul>
                                                            <h3 class="estimate-cost packing cart_details"></h3>
                                                            <li class="estimate-cost third_step_val_div_packing"></li>
                                                        </ul>
                                                    </div>
                                                    <div class="mp-xs-12 npl">
                                                        <h3 class="cart_title unlod_esti_title cart_details"
                                                            style="width: 100%;"></h3>
                                                    </div>
                                                    <div class="summary_list">
                                                        <ul class="estimate_div">
                                                            <h3 class="estimate-cost unload_floor cart_details"></h3>
                                                            <h3 class="estimate-cost third_step_val_div_unload"></h3>
                                                        </ul>
                                                        <ul>
                                                            <h3 class="estimate-cost unelevator cart_details"></h3>
                                                            <li class="estimate-cost unload_ele"></li>
                                                        </ul>
                                                        <ul>
                                                            <h3 class="estimate-cost unpacking cart_details"></h3>
                                                            <li class="estimate-cost third_step_val_div_unpacking"></li>
                                                        </ul>
                                                    </div>
                                                    <div class="mp-xs-12 npl bottom-bdr">
                                                        <h3 class="estimate-cost"></h3>
                                                    </div>

                                                    <div class="mp-xs-12" style="padding: 5px 10px;">
                                                        <ul>
                                                            <h3 class="estimate-cost cart_details subtotal_label"></h3>
                                                            <li class="estimate-cost subtotal_amount"></li>
                                                        </ul>
                                                    </div>

                                                    <div class="mp-xs-12" style="padding: 0px 10px;">
                                                        <ul>
                                                            <h3 class="estimate-cost cart_details discount_label"></h3>
                                                            <li class="estimate-cost discounted_amount"></li>
                                                        </ul>
                                                    </div>

                                                    <div class="mp-xs-12 bottom-bdr" style="padding: 5px 10px;">
                                                        <ul>
                                                            <h3 class="estimate-cost cart_details tax_label"></h3>
                                                            <li class="estimate-cost taxed_amount"></li>
                                                        </ul>
                                                    </div>
                                                    <div class="mp-xs-12 text-center cart-bg">
                                                        <div class="mp-xs-6">
                                                            <label class="est_cost"></label>
                                                        </div>
                                                        <div class="mp-xs-6" style="padding-right: 5px;">
                                                            <h1 class="cart-cost total_cost"></h1>
                                                        </div>
                                                    </div>

                                                <?php } ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="mp-md-12 mp-xs-12 f-right mtb-15">
                                    <span class="error_msg_livingroom" style="color: red;"></span>
                                    <a href="#mp" id="second_step_submit"
                                       class="mp_btn article_btn f-right step_two_submit">Continue</a>
                                </div>

                            </div>
                        </div>

                        <form method="post" action="" id="mp_third_step_validate">
                            <div class="visible cb" style="display:none;" id="mp_third_step">
                                <div class="mp-third-step form-inner visible">

                                    <div class="mp-form-common mp-md-8 mp-lg-8 mp-sm-12 mp-xs-12 np">
                                        <div class="common-inner">
                                            <div class="mp-form-common mp-md-6 mp-lg-6 mp-sm-6 mp-xs-12">
                                                <div class="loading-sec load-unload">
                                                    <h3 class="main-title">Loading Point</h3>
                                                    <!--  -->
                                                    <?php
                                                    $all_records = $floor_no->readAll_floor();
                                                    ?>
                                                    <div class="flool-count">
                                                        <h4>Floor No.</h4>

                                                        <?php
                                                        $i = 0;
                                                        foreach ($all_records as $floor_info) {

                                                            ?>

                                                            <label for="lf-<?php echo $floor_info->floor_no; ?>"
                                                                   class="radio_boxed_wrapper">
                                                                <input type="radio" name="flool_count"
                                                                       id="lf-<?php echo $floor_info->floor_no; ?>"
                                                                       class="load_flool-count"
                                                                       data-floorno="<?php echo $floor_info->floor_no; ?>"
                                                                       value="<?php echo $floor_info->price; ?>"/>

                                                                <div class="custom_radio_box"><?php echo $floor_info->floor_no; ?></div>
                                                            </label>
                                                        <?php } ?>
                                                        <span class="error_msg_load_floor"
                                                              style="color: red;display: block;line-height: 0;"></span>
                                                    </div>
                                                    <div class="address_sec">
                                                        <h4 class="add_title mb-10">Address</h4>
                                                        <div class="mtb-15">


                                                            <label class="fancy_input_wrap mt-20">
                                                                <input class="fancy_input custom-input" placeholder=" "
                                                                       name="street_address" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">Street</label>
                                                            </label>

                                                            <label class="fancy_input_wrap">
                                                                <input class="fancy_input custom-input" placeholder=" "
                                                                       name="city_address" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">City</label>
                                                            </label>

                                                            <label class="fancy_input_wrap ">
                                                                <input class="fancy_input custom-input" placeholder=" "
                                                                       name="state_address" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">State</label>
                                                            </label>

                                                        </div>
                                                    </div>

                                                    <div class="mtb-15 extra_element">
                                                        <ul>
                                                            <?php
                                                            $all_records = $elevators->readAll_elevator();
                                                            ?>
                                                            <li class="elemet_listing loading_elevator">
                                                                <p class="element_title">Elevator ?</p>
                                                                <div class="toggle-button-wrapp r" id="toggle_btn">
                                                                    <input type="checkbox" name="elevators"
                                                                           id="load_elevator" class="checkbox"
                                                                           value="<?php echo $all_records[0]->discount; ?>"/>
                                                                    <div class="knobs"></div>
                                                                    <div class="layer"></div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="mtb-15 extra_element">
                                                        <ul>
                                                            <?php
                                                            $all_records = $elevators->readAll_pack_unpack_data();
                                                            ?>
                                                            <li class="elemet_listing loading_elevator">
                                                                <p class="element_title">Packaging ?</p>
                                                                <div class="toggle-button-wrapp r" id="toggle_btn">
                                                                    <input type="checkbox" name="packaging"
                                                                           id="packaging" class="checkbox"
                                                                           value="<?php echo $all_records[0]->cost_packing; ?>"/>
                                                                    <div class="knobs"></div>
                                                                    <div class="layer"></div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mp-form-common mp-md-6 mp-lg-6 mp-sm-6 mp-xs-12 border_left">
                                                <div class="loading-sec load-unload">
                                                    <h3 class="main-title">Unloading Point</h3>
                                                    <?php
                                                    $all_records = $floor_no->readAll_floor();
                                                    ?>
                                                    <div class="unflool-count">
                                                        <h4>Floor No.</h4>

                                                        <?php

                                                        foreach ($all_records as $floor_info) {

                                                            ?>

                                                            <label for="ulf-<?php echo $floor_info->floor_no; ?>"
                                                                   class="radio_boxed_wrapper">
                                                                <input type="radio" name="unflool_count"
                                                                       id="ulf-<?php echo $floor_info->floor_no; ?>"
                                                                       class="unload_floor"
                                                                       data-floorno="<?php echo $floor_info->floor_no; ?>"
                                                                       value="<?php echo $floor_info->price; ?>"/>
                                                                <div class="custom_radio_box"><?php echo $floor_info->floor_no; ?></div>
                                                            </label>
                                                        <?php } ?>
                                                        <span class="error_msg_unload_floor"
                                                              style="color: red;display: block;line-height: 0;"></span>
                                                    </div>
                                                    <div class="address_sec">
                                                        <h4 class="add_title mb-10">Address</h4>
                                                        <div class="mtb-15">

                                                            <label class="fancy_input_wrap mt-20">
                                                                <input class="fancy_input custom-input" placeholder=" "
                                                                       name="unload_street_addr" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">Street</label>
                                                            </label>

                                                            <label class="fancy_input_wrap">
                                                                <input class="fancy_input custom-input" placeholder=" "
                                                                       name="unload_city_addr" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">City</label>
                                                            </label>

                                                            <label class="fancy_input_wrap">
                                                                <input class="fancy_input custom-input" placeholder=" "
                                                                       name="unload_state_addr" required>
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>

                                                                <label class="fancy_label">State</label>
                                                            </label>


                                                        </div>
                                                    </div>
                                                    <div class="mtb-15 extra_element">
                                                        <ul>
                                                            <?php
                                                            $all_records = $elevators->readAll_elevator();
                                                            ?>
                                                            <li class="elemet_listing loading_elevator">
                                                                <p class="element_title">Elevator ?</p>
                                                                <div class="toggle-button-wrapp r" id="toggle_btn">
                                                                    <input type="checkbox" id="unload_elevator"
                                                                           name="unloaad_ele" class="checkbox"
                                                                           value="<?php echo $all_records[0]->discount; ?>">
                                                                    <div class="knobs"></div>
                                                                    <div class="layer"></div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="mtb-15 extra_element">
                                                        <ul>
                                                            <?php
                                                            $all_records = $elevators->readAll_pack_unpack_data();
                                                            ?>
                                                            <li class="elemet_listing loading_elevator">
                                                                <p class="element_title">Unpackaging ?</p>
                                                                <div class="toggle-button-wrapp r" id="toggle_btn">
                                                                    <input type="checkbox" id="unpackaging"
                                                                           name="unpackaging" class="checkbox"
                                                                           value="<?php echo $all_records[0]->cost_unpacking; ?>">
                                                                    <div class="knobs"></div>
                                                                    <div class="layer"></div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="additional_info mtb-15 mp-form-common mp-md-12 mp-lg-12 mp-sm-12 mp-xs-12">
                                                <h4 class="add_title">Additional Info</h4>
                                                <div class="additional_info_listing mtb-15">
                                                    <ul class="additional_info_ul">
                                                        <?php
                                                        $all_records = $additional_info->readAll_additional_info();

                                                        ?>
                                                        <?php

                                                        $i = 0;
                                                        foreach ($all_records as $additional_info) {

                                                            ?>
                                                            <li class="info_listing">
                                                                <p class="info_title"><?php echo $additional_info->additional_info; ?></p>
                                                                <div class="toggle-button-wrapp r" id="toggle_btn">
                                                                    <input type="checkbox"
                                                                           class="checkbox additional_infoo"
                                                                           id="additional_info_<?php echo $additional_info->id; ?>"
                                                                           name="additional_info"
                                                                           value="<?php echo $additional_info->price; ?>"
                                                                           data-id="<?php echo $additional_info->id; ?>"
                                                                           data-caltype="<?php echo $additional_info->type; ?>">
                                                                    <div class="knobs"></div>
                                                                    <div class="layer"></div>
                                                                </div>
                                                            </li>
                                                            <?php $i++;
                                                        } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if (get_option("moveto_payment_gateways_status") == "E") {
                                            ?>
                                            <div class="">
                                                <div class="mp-card-details">
                                                    <div class="mp-topheading">
                                                        <h4 class="add_title"><?php echo __("Preferred Payment Method", "mp"); ?></h4>
                                                    </div>
                                                    <div class="mp-main-card-details">
                                                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 ">
                                                            <div class="mp-radio-btn d-flex">
                                                                <div class="custom-radios w-100">
                                                                    <div class="mp-radio-costom-btn col-sm-12">

                                                                        <?php if (get_option("moveto_locally_payment_status") == "E") { ?>
                                                                            <div class="custom-control custom-radio col-sm-12 col-md-6 col-12 float-left">
                                                                                <input type="radio"
                                                                                       class="custom-control-input payment_class"
                                                                                       id="pay_locally"
                                                                                       name="payment_method"
                                                                                       value="pay_locally" checked/>
                                                                                <label class="custom-control-label pl-3 mp-payment-type"
                                                                                       for="pay_locally"><?php echo __("Pay Locally", "mp"); ?></label>
                                                                            </div>
                                                                        <?php }
                                                                        if (get_option("moveto_payment_method_Stripe") == "E") {
                                                                            $card_detail_show = "block"; ?>
                                                                            <div class="custom-control custom-radio col-sm-12 col-md-6 col-12 float-left">
                                                                                <input type="radio"
                                                                                       class="custom-control-input payment_class"
                                                                                       id="stripe_payment"
                                                                                       name="payment_method"
                                                                                       value="stripe_payment"/>
                                                                                <label class="custom-control-label pl-3 mp-payment-type"
                                                                                       for="stripe_payment"><?php echo __("Card Payment", "mp"); ?></label>
                                                                            </div>
                                                                        <?php } ?>
                                                                        <?php if (get_option("moveto_payment_method_Paypal") == "E") { ?>
                                                                            <div class="custom-control custom-radio col-sm-12 col-md-6 col-12 float-left">
                                                                                <input type="radio"
                                                                                       class="custom-control-input payment_class"
                                                                                       id="paypal_payment"
                                                                                       name="payment_method"
                                                                                       value="paypal_payment"/>
                                                                                <label class="custom-control-label pl-3 mp-payment-type"
                                                                                       for="paypal_payment"><?php echo __("Paypal", "mp"); ?></label>
                                                                            </div>
                                                                        <?php } ?>
                                                                        <?php if (get_option("moveto_payment_method_Paytm") == "E") { ?>
                                                                            <div class="custom-control custom-radio col-sm-12 col-md-6 col-12 float-left">
                                                                                <input type="radio"
                                                                                       class="custom-control-input payment_class"
                                                                                       id="paytm_payment"
                                                                                       name="payment_method"
                                                                                       value="paytm_payment"/>
                                                                                <label class="custom-control-label pl-3 mp-payment-type"
                                                                                       for="paytm_payment"><?php echo __("Paytm", "mp"); ?></label>
                                                                            </div>
                                                                        <?php } ?>
                                                                        <?php if (get_option("moveto_payment_method_Paystack") == "E" && get_option("moveto_currency") == "NGN") { ?>
                                                                            <div class="custom-control custom-radio col-sm-12 col-md-6 col-12 float-left">
                                                                                <input type="radio"
                                                                                       class="custom-control-input payment_class"
                                                                                       id="paystack_payment"
                                                                                       name="payment_method"
                                                                                       value="paystack_payment"/>
                                                                                <label class="custom-control-label pl-3 mp-payment-type"
                                                                                       for="paystack_payment"><?php echo __("Paystack", "mp"); ?></label>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="mp_error ml-5 mp_payment_error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php $card_detail_show = "none"; ?>
                                        <?php if (get_option("moveto_payment_method_Stripe") == "E") { ?>
                                            <div class="row card_payment_detail_form"
                                                 style="display: <?php echo $card_detail_show; ?>;">
                                                <div class="card_payment_container">
                                                    <div class="mp-debit-card-detais">
                                                        <div class="form-row d-block">
                                                            <form id="payment-form">
                                                                <label for="card-element">
                                                                    <?php echo __("Credit or debit card", "mp"); ?>
                                                                </label>
                                                                <div id="card-element">
                                                                    <!-- A Stripe Element will be inserted here. -->
                                                                </div>
                                                                <!-- Used to display form errors. -->
                                                                <div id="card-errors" role="alert"></div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="mp-debit-card-img">
                                                        <div class="mp-main-debit-card-img-left-side  p-1">
                                                            <img src="<?php echo $plugin_url_for_ajax; ?>/assets/images/credit_logo.png"
                                                                 height="100%" width="100%" class="img-fluid">
                                                        </div>
                                                        <div class="mp-main-debit-card-img-left-side">
                                                            <div class="mp-main-debit-lock-img ">
                                                                <img src="<?php echo $plugin_url_for_ajax; ?>/assets/images/locked.png"
                                                                     height="100%" width="100%">
                                                            </div>
                                                            <div class="mp-main-debit-lock-text">
                                                                <p> <?php echo __("SAFE AND SECURE 256-BIT", "mp"); ?>
                                                                    <br/> <?php echo __("SSL ENCRYPTED PAYMENT", "mp"); ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="mp-md-12 mp-xs-12 m-auto dt fn">
                                            <div class="mp-md-6 mp-xs-6 mtb-15 text-center">
                                                <a href="javascript:void(0)" id="forth_step_submit"
                                                   class="mp_btn article_btn dis-inline">Checkout</a>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- <div class="mp-form-common mp-md-4 mp-lg-4 mp-sm-4 mp-xs-12 r-cart-b">
                                        <div class="mp-xs-12" id="booking_summary">
                                            <div class="right-cart">
                                        <div class="summary_title">Estimated Cost</div>
                                            <div class="summary_list">
                                                <ul class="cart_li">
                                            </ul>
                                        </div>

                                        <div class="mp-xs-12 npl">
                                            <h3 class="estimate-cost cart_title lod_esti_title cart_details" style="font-style: normal;"></h3>
                                        </div>
                                        <div class="summary_list">
                                            <ul class="estimate_div">
                                                <h3 class="load_floor cart_details"></h3>
                                                <li class="estimate-cost third_step_val_div"></li>
                                            </ul>
                                            <ul>
                                                <h3 class="estimate-cost elevator cart_details"></h3>
                                            <li class="estimate-cost third_step_val_div_elevators"></li>
                                            </ul>
                                            <ul>
                                                <h3 class="estimate-cost packing cart_details"></h3>
                                            <li class="estimate-cost third_step_val_div_packing"></li>
                                            </ul>
                                        </div>

                                        <div class="mp-xs-12 npl">
                                            <h3 class="cart_title unlod_esti_title cart_details" style="font-style: normal;"></h3>
                                        </div>
                                        <div class="summary_list">
                                            <ul class="estimate_div">
                                                <h3 class="estimate-cost unload_floor cart_details"></h3>
                                                <h3 class="estimate-cost third_step_val_div_unload"></h3>
                                            </ul>
                                            <ul>
                                                <h3 class="estimate-cost unelevator cart_details"></h3>
                                            <li class="estimate-cost unload_ele"></li>
                                            </ul>
                                            <ul>
                                                <h3 class="estimate-cost unpacking cart_details"></h3>
                                            <li class="estimate-cost third_step_val_div_unpacking"></li>
                                            </ul>
                                        </div>

                                        <div class="mp-xs-12 npl">
                                            <h3 class="estimate-cost additional_info_title cart_details"></h3>
                                        </div>
                                        <div class="summary_list">
                                            <ul class="estimate_div">
                                                <h3 class="estimate-cost additional_info_sub"></h3>
                                                <h3 class="estimate-cost additional_info_amt"></h3>
                                            </ul>

                                        </div>
                                        <div class="mp-xs-12 npl">
                                            <h3 class="estimate-cost">  Estimated Cost  </h3>
                                        </div>
                                        <div class="mp-xs-12 text-center cart-bg">
                                            <label class="est_cost"></label>
                                            <h1 class="cart-cost total_cost"></h1>
                                        </div>
                                        </div>
                                        </div>
                                    </div> -->
                                    <div class="center-ipad-cart">
                                        <div class="mp-form-common mp-md-4 mp-lg-4 mp-sm-8 mp-xs-12 r-cart-b">
                                            <div class="mp-xs-12" id="booking_summary"
                                                 style="padding-right: 0px; padding-left: 0;">
                                                <div class="right-cart">
                                                    <div class="summary_title">Estimated Cost</div>
                                                    <div class="summary_list">
                                                        <ul class="cart_li">

                                                        </ul>
                                                    </div>
                                                    <!-- <div class="mp-xs-12 npl">
                                                            <h3 class="cart_title cubic_feets cart_details" style="width: 100%;"></h3>
                                                    </div> -->
                                                    <div class="mp-xs-12">
                                                        <div class="cart-listing">
                                                            <h3 class="cart-title cart_title_cubic"></h3>
                                                            <!-- <h4 class="cart_cubic_value"></h4> -->
                                                            <h4 class="cart_cubic_volume"></h4>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    if (get_option("move_pricing_status") == "Y") { ?>
                                                        <div class="mp-xs-12">
                                                            <h3 class="estimate-cost cart_title lod_esti_title cart_details"
                                                                style="font-style: normal; margin-top: 10px; width: 100%;"></h3>
                                                        </div>
                                                        <div class="summary_list">
                                                            <ul class="estimate_div">
                                                                <h3 class="load_floor cart_details"></h3>
                                                                <li class="estimate-cost third_step_val_div"></li>
                                                            </ul>
                                                            <ul>
                                                                <h3 class="estimate-cost elevator cart_details"></h3>
                                                                <li class="estimate-cost third_step_val_div_elevators"></li>
                                                            </ul>
                                                            <ul>
                                                                <h3 class="estimate-cost packing cart_details"></h3>
                                                                <li class="estimate-cost third_step_val_div_packing"></li>
                                                            </ul>
                                                        </div>

                                                        <div class="mp-xs-12">
                                                            <h3 class="cart_title unlod_esti_title cart_details"
                                                                style="font-style: normal; margin-top: 5px; width: 100%;"></h3>
                                                        </div>
                                                        <div class="summary_list">
                                                            <ul class="estimate_div">
                                                                <h3 class="estimate-cost unload_floor cart_details"></h3>
                                                                <h3 class="estimate-cost third_step_val_div_unload"></h3>
                                                            </ul>
                                                            <ul>
                                                                <h3 class="estimate-cost unelevator cart_details"></h3>
                                                                <li class="estimate-cost unload_ele"></li>
                                                            </ul>
                                                            <ul>
                                                                <h3 class="estimate-cost unpacking cart_details"></h3>
                                                                <li class="estimate-cost third_step_val_div_unpacking"></li>
                                                            </ul>
                                                        </div>

                                                        <div class="mp-xs-12 npl">
                                                            <h3 class="estimate-cost additional_info_title cart_details"></h3>
                                                        </div>
                                                        <div class="summary_list">
                                                            <ul class="estimate_div">
                                                                <h3 class="estimate-cost additional_info_sub"></h3>
                                                                <h3 class="estimate-cost additional_info_amt"></h3>
                                                            </ul>

                                                        </div>

                                                        <div class="mp-xs-12 npl bottom-bdr">
                                                            <h3 class="estimate-cost"></h3>
                                                        </div>
                                                        <div class="mp-xs-12" style="padding: 5px 10px;">
                                                            <ul>
                                                                <h3 class="estimate-cost cart_details subtotal_label"></h3>
                                                                <li class="estimate-cost subtotal_amount"></li>
                                                            </ul>
                                                        </div>

                                                        <div class="mp-xs-12" style="padding: 0px 10px;">
                                                            <ul>
                                                                <h3 class="estimate-cost cart_details discount_label"></h3>
                                                                <li class="estimate-cost discounted_amount"></li>
                                                            </ul>
                                                        </div>

                                                        <div class="mp-xs-12 bottom-bdr" style="padding: 5px 10px;">
                                                            <ul>
                                                                <h3 class="estimate-cost cart_details tax_label"></h3>
                                                                <li class="estimate-cost taxed_amount"></li>
                                                            </ul>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>

                                                    <?php if ($insurance_permission == '1') { ?>
                                                        <div class="mp-xs-12 text-center cart-bg custom_insurance">
                                                        <div class="mp-xs-5">
                                                            <label class="">Insurance Amount</label>
                                                        </div>
                                                        <div class="mp-xs-7">
                                                            <select name="amt_val" id="amt_val" class="form-control">
                                                                <option>Select Insurance</option>
                                                                <option value="50000">50000</option>
                                                                <option value="100000">100000</option>
                                                                <option value="150000">150000</option>
                                                                <option value="200000">200000</option>
                                                                <option value="other">Other</option>
                                                            </select>
                                                        </div>
                                                        <?php if (get_option("move_pricing_status") == "Y") { ?>
                                                            <div class="new_val mp-xs-12" style="display: none;">
                                                                <input type="text" id="amt"/>
                                                                <input type="button" id="add_amt_Val"
                                                                       class="form-control"
                                                                       value="Add Insurance Amount"/>
                                                            </div>
                                                            <div class="mp-xs-12 text-center cart-bg insurance_sec">

                                                            </div>
                                                            </div>
                                                        <?php }
                                                    } ?>
                                                    <?php
                                                    if (get_option("move_pricing_status") == "Y") {
                                                        ?>
                                                        <div class="mp-xs-12 text-center cart-bg">
                                                            <div class="mp-xs-6">
                                                                <label class="est_cost"></label>
                                                            </div>
                                                            <div class="mp-xs-6" style="padding-right: 5px;">
                                                                <h1 class="cart-cost total_cost"></h1>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }

                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <?php
                        if (get_option('moveto_payment_method_Paytm') == 'E') {
                            ?>
                            <form method="post" action="" name="mp_paytm_form" id="mp_paytm_form">
                            </form>
                            <?php
                        }
                        ?>
                        <div style="display:none;" class="visible cb" id="mp_forth_step">
                            <div class="mp-forth-step form-inner visible">

                                <div class="mp-md-12 mp-lg-12 mp-sm-12 mp-xs-12 pull-left">
                                    <!-- <h3>3. Done</h3> -->
                                    <div class="common-inner">
                                        <div class="booking-thankyou">
                                            <h1 class="header1"><?php echo __("Thank You", "mp"); ?></h1>
                                            <h3 class="header3"><?php echo $mp_settings->moveto_thankyou_page_message; ?></h3>
                                            <p class="thankyou-text"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>

        </div>
        </section> <!-- main view content end here -->
        </main>
        </section>
    </div>
</div>
</div>
</div>

<?php
$mp_terms_and_condition_status = 'D';
if (get_option('moveto_allow_terms_and_conditions') == 'E' || get_option('moveto_allow_privacy_policy') == 'E') {
    $mp_terms_and_condition_status = 'E';
}

$offdaysstring = '';
if (sizeof((array)$alloffdays) > 0) {
    $totalddates = sizeof((array)$alloffdays);
    $pamdcounter = 1;
    foreach ($alloffdays as $alloffday) {
        if ($totalddates == $pamdcounter) {
            $offdaysstring .= date_i18n('m/d/Y', strtotime($alloffday->off_date));
        } else {
            $offdaysstring .= date_i18n('m/d/Y', strtotime($alloffday->off_date)) . "##";
        }
        $pamdcounter++;
    }
} else {
    $offdaysstring = '##';
}

$thankyou = get_option('moveto_thankyou_page');
$thankyou_page_url = "";
if ($thankyou != '' && $thankyou != null) {
    $thankyou_page_url = $thankyou;
} else {
    $thankyou_page_url = $plugin_url_for_ajax . "/frontend/mp_thankyou.php";
}
?>
<script>

    var mpmain_obj = {
        "plugin_path": "<?php echo $plugin_url_for_ajax; ?>",
        "location_err_msg": "<?php echo __("We are not provide service in your area zipcode", "mp"); ?>",
        "location_search_msg": "<?php echo __("Searching...", "mp"); ?>",
        "multilication_status": "<?php echo $mp_mulitlocation_status; ?>",
        "zipwise_status": "<?php echo $mp_zipcode_booking_status; ?>",
        "multilocation_status": "<?php echo $mp_mulitlocation_status;?>",
        "Choose_service": "<?php echo __("Please Choose Destination City", "mp"); ?>",
        "Choose_zipcode": "<?php echo __("Please Check Your Area Zipcode", "mp"); ?>",
        "thankyou_url": "<?php echo get_option('moveto_thankyou_page'); ?>",
        "Choose_provider": "<?php echo __("Choose service provider", "mp");?>",
        "Choose_location": "<?php echo __("Please choose location", "mp");?>",
        "mp_payment_gateways_st": "<?php echo get_option('moveto_payment_gateways_status');?>",
        "mp_terms_and_condition_status": "<?php echo $mp_terms_and_condition_status;?>",
        "pam_disabledates": "<?php echo $offdaysstring;?>",
        "pamstart_date": "<?php echo date_i18n('m/d/Y');?>",
        "pam_enabled_additionalinfo": "<?php echo implode(',', $enabled_additionalinfo);?>",
        "moveto_selected_flags": "<?php echo get_option('moveto_country_flags'); ?>",
        "actual_link": "<?php echo $actual_link; ?>"
    };


    var mpmain_error_obj = {
        "Please_Enter_Email": "<?php echo __("Please Enter Email", "mp"); ?>",
        "Please_Enter_Valid_Email": "<?php echo __("Please Enter Valid Email", "mp"); ?>",
        "Email_already_exist": "<?php echo __("Email Already Exist", "mp"); ?>",
        "Please_Enter_Password": "<?php echo __("Please Enter Password", "mp"); ?>",
        "Please_enter_minimum_8_Characters": "<?php echo __("Please Enter Minimum 8 Characters", "mp"); ?>",
        "Please_enter_maximum_30_Characters": "<?php echo __("Please Enter Maximum 30 Characters", "mp"); ?>",
        "Please_Enter_First_Name": "<?php echo __("Please Enter First Name", "mp"); ?>",
        "Please_Enter_Last_Name": "<?php echo __("Please Enter Last Name", "mp"); ?>",
        "Please_Enter_Phone_Number": "<?php echo __("Please Enter Phone Number", "mp"); ?>",
        "Please_Enter_Valid_Phone_Number": "<?php echo __("Please Enter Valid Phone Number", "mp"); ?>",
        "Please_enter_minimum_10_Characters": "<?php echo __("Please Enter minimum 10 Characters", "mp"); ?>",
        "Please_enter_maximum_11_Characters": "<?php echo __("Please Enter maximum 11 Characters", "mp"); ?>",
        "Please_Enter_Address": "<?php echo __("Please Enter Address", "mp"); ?>",
        "Please_Enter_City": "<?php echo __("Please Enter City", "mp"); ?>",
        "Please_Enter_State": "<?php echo __("Please Enter State", "mp"); ?>",
        "Please_Enter_Notes": "<?php echo __("Please Enter Notes", "mp"); ?>",
        "Please_Enter": "<?php echo __("Please Enter", "mp"); ?>",

        "Please_Enter_Source_City": "<?php echo __("Please Enter Source City", "mp"); ?>",
        "Please_Enter_Destination_City": "<?php echo __("Please Enter Destination City", "mp"); ?>",
        "Please_Enter_Via_City": "<?php echo __("Please Enter Via City", "mp"); ?>",
        "Please_Enter_Name": "<?php echo __("Please Enter Name", "mp"); ?>",
        "Please_Enter_Email": "<?php echo __("Please Enter Email", "mp"); ?>",
        "Please_Enter_Phone_Number": "<?php echo __("Please Enter Phone Number", "mp"); ?>",
        "Please_Select_Date": "<?php echo __("Please Select Date", "mp"); ?>",
        "Please_Enter_Number": "<?php echo __("Please Enter Only Numbers", "mp"); ?>",

        "Please_Select_Loading_Floor": "<?php echo __("Please Select Floor Number", "mp"); ?>",
        "Please_Enter_Loading_Street": "<?php echo __("Please Enter Street", "mp"); ?>",
        "Please_Enter_Loading_City": "<?php echo __("Please Enter City", "mp"); ?>",
        "Please_Enter_Loading_State": "<?php echo __("Please Enter State", "mp"); ?>",

        "Please_Select_UnLoading_Floor": "<?php echo __("Please Select Floor Number", "mp"); ?>",
        "Please_Enter_UnLoading_Street": "<?php echo __("Please Enter Street", "mp"); ?>",
        "Please_Enter_UnLoading_City": "<?php echo __("Please Enter City", "mp"); ?>",
        "Please_Enter_UnLoading_State": "<?php echo __("Please Enter State", "mp"); ?>",

        "pam_loading_street_address": "<?php echo __("Please Enter Street Address", "mp"); ?>",
        "pam_loading_city": "<?php echo __("Please Enter City", "mp"); ?>",
        "pam_loading_state": "<?php echo __("Please Enter State", "mp"); ?>",

    };

    var obj_thankspage_url = {"thankspage_url": "<?php echo $thankyou_page_url;?>"};
    var moveto_currency_symbol = {"symbol": "<?php echo get_option('moveto_currency_symbol');?>"};
    var page = "first";
    var moveto_google_api_key = "<?php echo $moveto_google_api_key;?>";
    var moveto_distance_unit = "<?php echo $moveto_distance_unit;?>";
    var moveto_google_map_country = "<?php echo get_option('moveto_google_map_country'); ?>";
    var moveto_google_map_center_restrictions = "<?php echo get_option('moveto_google_map_center_restrictions'); ?>";
    var moveto_company_origin_destination_distance_status = "<?php echo get_option('moveto_company_origin_destination_distance_status'); ?>";
    var moveto_destination_company_distance = "<?php echo get_option('moveto_destination_company_distance'); ?>";
    var moveto_company_origin_address = "<?php echo get_option('moveto_company_origin_address'); ?>";
    var moveto_company_destination_address = "<?php echo get_option('moveto_company_destination_address'); ?>";
    var moveto_cubic_unit = "<?php echo get_option('moveto_cubic_meter') ?>";
</script>