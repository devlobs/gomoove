<!-- 
 Created By:- Ajay Prajapati 
 Created Date:- 27-3-19
 Used For:- Insert Distance Menu Data
 -->

<?php 
	session_start();
	$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
	if (file_exists($root.'/wp-load.php')){
				require_once($root.'/wp-load.php');
	}
	if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

		$distance = new Moveto_distance();
		$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
		
	/* Insert Distance Data */	
	if(!empty($_POST['action']) && $_POST['action']=='distance_insert_data')
	{
		$distance->unit = $_POST['unit_val'];
		$distance->distance = $_POST['distance'];
		$distance->moveto_distance_rule = $_POST['moveto_distance_rule'];
		$distance->price = $_POST['price'];
		$distance->insert_distance();  
	}
	
	/* Change Distance unit */	
	if(!empty($_POST['action']) && $_POST['action']=='update_distance_unit')
	{
		$distance->distance_unit = $_POST['selected_value'];
		$distance->update_distance_unit();  
	}
	/* Update Distance Data */		
	 if(!empty($_POST['action']) && $_POST['action']=='update_distance')
	 {		
	 	$distance->id = $_POST['id'];
		$distance->unit = $_POST['unit_val'];
		$distance->distance = $_POST['distance'];
		$distance->moveto_distance_rule = $_POST['moveto_distance_rule'];
		$distance->price = $_POST['price'];
		$distance->update_distance(); 
	 }

	 /* Insert Floor Data */	
	 if(!empty($_POST['action']) && $_POST['action']=='floor_insert_data')
	 {
	 	$distance->unit = $_POST['unit_val'];
		$distance->price = $_POST['price'];
		$distance->floor_no = $_POST['floor_no'];
		$distance->insert_floor();  
	 }

	/* Update Floor Data */		
	 if(!empty($_POST['action']) && $_POST['action']=='update_floor')
	 {		
	 	$distance->id = $_POST['id'];
		$distance->unit = $_POST['unit_val'];
		$distance->price = $_POST['price'];
		$distance->floor_no = $_POST['floor_no'];
		$distance->update_floor(); 
	 }

	 /* Insert Elevator Data */	
	 if(!empty($_POST['action']) && $_POST['action']=='elevator_insert_data')
	 {
	 	$distance->unit = $_POST['unit_val'];
		$distance->elevator = $_POST['elevator'];
		$distance->insert_elevator();  
	 }

	/* Update Floor Data */		
	 if(!empty($_POST['action']) && $_POST['action']=='update_elevator')
	 {		
	 	$distance->id = $_POST['id'];
		$distance->unit = $_POST['unit_val'];
		$distance->discount = $_POST['discount'];
		$distance->update_elevator(); 
	 }

	 /* Insert Packing/Unpacking Data */	
	 if(!empty($_POST['action']) && $_POST['action']=='pac_unpac_insert_data')
	 {
	 	$distance->unit = $_POST['unit_val'];
		$distance->cost_packing = $_POST['cost_packing'];
		$distance->cost_unpacking = $_POST['cost_unpacking'];
		$distance->insert_pac_unpac();  
	 }

	/* Update Packing/Unpacking Data */		
	 if(!empty($_POST['action']) && $_POST['action']=='update_pac_unpac')
	 {		
	 	$distance->id = $_POST['id'];
		$distance->unit = $_POST['unit_val'];
		$distance->cost_packing = $_POST['cost_packing'];
		$distance->cost_unpacking = $_POST['cost_unpacking'];
		$distance->update_pac_unpac(); 
	 }

	 /* Delete Distance Data */		
	 if(isset($_POST['action'],$_POST['id']) && $_POST['action']=='delete_distance')
	 {
		$distance->id= $_POST['id'];
		$distance->delete_distance();
	 }	
  

	/* Mover Price */		
	if(isset($_POST['action']) && $_POST['action']=='mover_min_rate')
	{
		$minrate = get_option('moveto_min_rate_service');
		if(isset($minrate)){
	    update_option('moveto_min_rate_service', $_POST['minrate']);
	    }else{
	    add_option('moveto_min_rate_service', $_POST['minrate']);
	    } 
	}	

	 /* Change Cubic Volume */	
	if(isset($_POST['action']) && $_POST['action']=='update_cubicvolume_set')
	{
		$distance->cubic_unit = $_POST['selected_value'];
		$distance->update_cubic_volume();  
	}

	 /* Insuance Percentage */	
	if(isset($_POST['action']) && $_POST['action']=='mover_insurance_rate')
	{
		$distance->insurance_rate = $_POST['insurance_rate'];
		$distance->insurance_rate();  
	}

	 /* Insuance Permission */	
	if(isset($_POST['action']) && $_POST['action']=='mover_insurance_permission')
	{
		$distance->check_val = $_POST['check_val'];
		$distance->insurance_permission();  
	}


    
?>