<?php
class moveto_service
{

    
    /* object properties */
    public $id;
	public $addon_title;
	public $addon_price;
	public $addon_id;
	public $addon_service_id;
	public $addon_update_id;
	public $selected_location;
	public $selected_service_id;
    public $status;
	public $qty;
	public $rules;
	public $rate;
	public $price;
	public $image;
	public $max_qty;
    
	/**
     * create addons services table
     */ 
	function create_table_addons() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_articles';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			  `article_name` varchar(50) NOT NULL,
			  `type` enum('P','F') NOT NULL,
			  `price` varchar(100) NOT NULL,
			  `image` varchar(250) NOT NULL,
			  `predefinedimage` varchar(250) NULL, 
			  `max_qty` int(5) NOT NULL,
			  `status` enum('E','D') NOT NULL DEFAULT 'E',
			  `position` int(11) NOT NULL,
			  `cubicarea` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	dbDelta($sql);     
			}
	}
	
	
	/* Add New article */
	function insert_article()
	{
		global $wpdb;	
		 /* "INSERT INTO ".$wpdb->prefix."mp_articles (article_name,type,price,image,status)values('".$this->article_name."','".$this->price."','".$this->image."','".$this->type."','E')"; */
		 $in_article = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_articles (article_name,type,price,image,predefinedimage,max_qty,status,cubicarea)values('".$this->article_name."','".$this->type."','".$this->price."','".$this->image."','".$this->article_image_name."','".$this->max_qty."','E','".$this->cubicfeets."')");
		 
		 return $in_article;
	}
	
	
	
	function readAll_addons()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix."mp_articles  ORDER BY position ASC";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    } function readAll_addonsss()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix."mp_articles where status='E' ORDER BY position ASC";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    } 
	/* ReadOne Addon Information */
	function readOne_addon()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix."mp_articles where id='".$this->addon_id."'";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    }
	
	function addon_update()
    {
        global $wpdb;
		$update_addons = $wpdb->query("UPDATE ".$wpdb->prefix."mp_articles SET article_name='".$this->title."', image = '".$this->image."', predefinedimage = '".$this->article_icon_name."', max_qty = '".$this->max_qty."', cubicarea = '".$this->cubicfeets."'  WHERE	id = " . $this->article_update_id);
       
    }
	function addon_delete()
	{
		global $wpdb;
		$delete_addons   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_articles  WHERE id =" . $this->id);
        $result = $delete_addons;
        if ($result) {
            return true;
        } else {
            return false;
        }
	}
	
	/* Sort Service Position */
	function sort_service_position(){
		global $wpdb;
		
			 $stmt = $wpdb->query("UPDATE ".$wpdb->prefix."mp_articles set position='".$this->position."' where id='".$this->id."';");
			 
			return $result;	
	}	






	
	function get_all_addons()
	{
		global $wpdb;
		
		$get_all_addons = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_articles where service_id = '".$this->selected_service_id."' AND status='E'");
		
        return $get_all_addons;
	}
	
	/* Remove Article Image */
	function remove_article_image()
    {
        global $wpdb;
        $stmt = $wpdb->query("UPDATE ".$wpdb->prefix."mp_articles 	SET	image='".$this->image."' WHERE id = ".$this->id);       
        if ($stmt) {
            return true;
        } else {
            return false;
        }
    }
	 
	/* addon status update */
	function update_addon_status() {        
		global $wpdb;  

		$result = $wpdb->query("UPDATE  ".$wpdb->prefix."services_addon 	SET 	status ='" . $this->status . "'  	WHERE	id = " . $this->id );       
		if ($result) {            
			return true;        
			} else {            
			return false;        
		}    
	}	
}
?>