/********************************/
/*****  header jquery  **********/
	/* Hide Success Message After 3 Seconds */
	function moveto_hide_success_msg(){
		setTimeout(function(){ jQuery('.mainheader_message_inner').hide(); }, 3000);
	}
		
	/*  scroll to active class service, location, setting  */
	jQuery(document).ready(function () {
		jQuery('.top-company-details, .top-general-setting, .top-appearance-setting, .top-payment-setting, .top-email-setting, .top-email-template, .top-sms-reminder, .top-sms-template, .top-custom-formfield, .top-promocode').on('click', function (e) {
			jQuery('html, body').stop().animate({
				'scrollTop': jQuery('.mp-setting-details').offset().top - 115
			}, 500, 'swing', function () {
			});
		});
	});
	
	/* menu auto hide in mobile when open a menu */
	 jQuery(document).on('click','.navbar-collapse.in',function(e) {
		if( jQuery(e.target).is('a') ) {
			jQuery(this).collapse('hide');
		}
	});
	
	jQuery(document).ready(function() {		
		var moveto_twilio_ccode_alph = header_object.moveto_twilio_ccode_alph;
		jQuery("#moveto_twilio_admin_phone_no").intlTelInput({initialCountry:moveto_twilio_ccode_alph});	
		jQuery(document).on("click",'.moveto_twillio_cd .country',function(){
			var ccode = jQuery(this).data('dial-code');
			var ccode_aplh = jQuery(this).data('country-code');
			jQuery('#moveto_twilio_ccode').val('+'+ccode);
			jQuery('#moveto_twilio_ccode_alph').val(ccode_aplh);
			var codeval = jQuery("#moveto_twilio_admin_phone_no").val();
			jQuery("#moveto_twilio_admin_phone_no").val(codeval.replace('+'+ccode,''));
		});
		
		
		
	});
	
	jQuery(document).ready(function() {
		var moveto_plivo_ccode_alph = header_object.moveto_plivo_ccode_alph;
		jQuery("#moveto_plivo_admin_phone_no").intlTelInput({initialCountry:moveto_plivo_ccode_alph});
		jQuery(document).on("click",'.moveto_plivo_cd .country',function(){
			var ccode = jQuery(this).data('dial-code');
			var ccode_aplh = jQuery(this).data('country-code');
			jQuery('#moveto_plivo_ccode').val('+'+ccode);
			jQuery('#moveto_plivo_ccode_alph').val(ccode_aplh);
			var codeval = jQuery("#moveto_plivo_admin_phone_no").val();
			jQuery("#moveto_plivo_admin_phone_no").val(codeval.replace('+'+ccode,''));
		});	
		
		var moveto_textlocal_ccode_alph = header_object.moveto_textlocal_ccode_alph;
		jQuery("#moveto_textlocal_admin_phone_no").intlTelInput({initialCountry:moveto_textlocal_ccode_alph});
		jQuery(document).on("click",'.moveto_textlocal_cd .country',function(){
			var ccode = jQuery(this).data('dial-code');
			var ccode_aplh = jQuery(this).data('country-code');
			jQuery('#moveto_textlocal_ccode').val('+'+ccode);
			jQuery('#moveto_textlocal_ccode_alph').val(ccode_aplh);
			var codeval = jQuery("#moveto_textlocal_admin_phone_no").val();
			jQuery("#moveto_textlocal_admin_phone_no").val(codeval.replace('+'+ccode,''));
		});	
		
	});
	jQuery(document).ready(function() {
		var moveto_nexmo_ccode_alph = header_object.moveto_nexmo_ccode_alph;
		jQuery("#moveto_nexmo_admin_phone_no").intlTelInput({initialCountry:moveto_nexmo_ccode_alph});
		jQuery(document).on("click",'.moveto_nexmo_cd .country',function(){
			var ccode = jQuery(this).data('dial-code');
			var ccode_aplh = jQuery(this).data('country-code');
			jQuery('#moveto_nexmo_ccode').val('+'+ccode);
			jQuery('#moveto_nexmo_ccode_alph').val(ccode_aplh);
			var codeval = jQuery("#moveto_nexmo_admin_phone_no").val();
			jQuery("#moveto_nexmo_admin_phone_no").val(codeval.replace('+'+ccode,''));
		});
	});
	jQuery(document).ready(function() {
		var moveto_textlocal_ccode_alph = header_object.moveto_textlocal_ccode_alph;
		jQuery("#moveto_textlocal_admin_phone_no").intlTelInput({initialCountry:moveto_textlocal_ccode_alph});
		jQuery(document).on("click",'.moveto_nexmo_cd .country',function(){
			var ccode = jQuery(this).data('dial-code');
			var ccode_aplh = jQuery(this).data('country-code');
			jQuery('#moveto_nexmo_ccode').val('+'+ccode);
			jQuery('#moveto_textlocal_ccode_alph').val(ccode_aplh);
			var codeval = jQuery("#moveto_textlocal_admin_phone_no").val();
			jQuery("#moveto_textlocal_admin_phone_no").val(codeval.replace('+'+ccode,''));
		});
	});
	
	jQuery(document).on("click",'.country',function(){
	var num_code=jQuery(this).data("dial-code");
	var country_code=jQuery(this).data("country-code");
	/* jQuery("#moveto_twilio_admin_phone_no").val('+'+num_code);
	jQuery("#moveto_plivo_admin_phone_no").val('+'+num_code);
	jQuery("#moveto_nexmo_admin_phone_no").val('+'+num_code); */
	jQuery(".company_country_code_value").html('');
	jQuery(".company_country_code_value").html('+'+num_code);
	jQuery(".company_country_code_value").html('+'+num_code);
	jQuery(".default_company_country_flag").val(country_code);
	
	});
	
	
	/* manage form fields min max counting */
	jQuery(function () {
		jQuery('.add').on('click',function(){
			var $qty=$(this).closest('.mp-min-max').find('.qty');
			var currentVal = parseInt($qty.val());
			if (!isNaN(currentVal)) {
				$qty.val(currentVal + 1);
			}
		});
		jQuery('.minus').on('click',function(){
			var $qty=$(this).closest('.mp-min-max').find('.qty');
			var currentVal = parseInt($qty.val());
			if (!isNaN(currentVal) && currentVal > 0) {
				$qty.val(currentVal - 1);
			}
		});
	});
	
	/* delete appointment in modal window */
	jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-delete-appointment').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-delete-appointment').html();
		}
	  });
	});
	/* hide delete appointment in modal window */
	jQuery(document).on('click', '#mp-close-del-appointment', function(){			
		jQuery('.popover').fadeOut();
	});
	/* customer phone number in popup */
	jQuery(document).ready(function() {
		jQuery("#mp_client_phone").intlTelInput({
			utilsScript: "utils.js"
		}); 
		jQuery("#mp_clientphone_manual").intlTelInput({
			utilsScript: "utils.js"
		}); 
	});
	
	
	/* reject appointment reason popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-reject-appointment').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-reject-appointment').html();
		}
	  });
	});
	/* hide add new service popover */
	jQuery(document).on('click', '#mp-close-reject-appointment', function(){			
		jQuery('.popover').fadeOut();
	});

	
	jQuery(document).on('click', '#mp-close-popover-delete-staff', function(){			
		jQuery('.popover').fadeOut();
	});
	
	



	
	

	
	/***************************************/
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#edit-mp-reject-appointment').popover({ 
		html : true,
		content: function() {
		  return jQuery('#edit-popover-reject-appointment').html();
		}
	  });
	});
	/* hide reject appointment reason popover */
	jQuery(document).on('click', '#edit-mp-close-reject-appointment', function(){			
		jQuery('.popover').fadeOut();
	});
	
	/* delete appointment in modal window */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#edit-mp-delete-appointment').popover({ 
		html : true,
		content: function() {
		  return jQuery('#edit-popover-delete-appointment').html();
		}
	  });
	});
	/* hide delete appointment in modal window */
	jQuery(document).on('click', '#edit-mp-close-del-appointment', function(){			
		jQuery('.popover').fadeOut();
	});


/******* LOCATIONS list *******/

	/* add new location button and popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-add-new-city-state').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-content-wrapper').html();
		}
	  });
	});
	/* hide location popover */
	jQuery(document).on('click', '#mp-close-popover-city-state', function(){			
		jQuery('.popover').fadeOut();
	});
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-delete-location-city-state').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-delete-location-city-state').html();
		}
	  });
	});
	/* hide delete  location popover */
	jQuery(document).on('click', '#mp-close-popover-location-city-state', function(){			
		jQuery('.popover').fadeOut();
	});
	
	jQuery(document).on('click', '#mp-add-new-location', function(){	
		jQuery( ".mp-show-hide-checkbox" ).trigger( "click" );	
		jQuery('.mp-add-new-location').fadeIn();
		jQuery('html, body').stop().animate({
		'scrollTop': jQuery('.mp-add-new-location').offset().top - 35
		}, 2000, 'swing', function () {});
		
	});
	
	jQuery(function() {
		jQuery( "#sortable-locations" ).sortable({ handle: '.fa-th-list' });
	}); 	
	
	jQuery(document).on('click', '#mp-close-popover-delete-location', function(){			
		jQuery('.popover').fadeOut();
	});
	
	
	/* locations toggle details */
	//jQuery(document).ajaxComplete(function() {
		jQuery(document).on('change','.mp-show-hide-checkbox',function() {
			 var toggle_id = jQuery(this).attr('id');
			 var sms_toggle_id = jQuery(this).data('id');
			 
			 jQuery('.service_detail').each(function(){
				if(!jQuery(this).hasClass('detail-id_'+toggle_id)){
					var service_edid = jQuery(this).attr('class').split('detail-id_');
					jQuery('#'+service_edid[1]).prop('checked',false);
					jQuery(this).fadeOut('slow');
				}
			 });
			  jQuery('.location_detail').each(function(){
				if(!jQuery(this).hasClass('detail-id_'+toggle_id)){				
					var location_edid = jQuery(this).attr('class').split('detail-id_');
					jQuery('#'+location_edid[1]).prop('checked',false);
					jQuery(this).fadeOut('slow');
				}
			 });
			  jQuery('.ssp_detail').each(function(){
				if(!jQuery(this).hasClass('detail-id_'+toggle_id)){				
					var location_edid = jQuery(this).attr('class').split('detail-id_');
					jQuery('#'+location_edid[1]).prop('checked',false);
					jQuery(this).fadeOut('slow');
				}
			 });
			 jQuery('.emailtemplatedetail').each(function(){
				if(!jQuery(this).hasClass('emaildetail_'+toggle_id)){
					var email_edid = jQuery(this).attr('class').split('emaildetail_');					
					jQuery('#'+email_edid[1]).prop('checked',false);
					jQuery(this).fadeOut('slow');
				}
			 });
			
			 jQuery('.smstemplatedetail').each(function(){
				if(!jQuery(this).hasClass('smsdetail_'+sms_toggle_id)){
					var sms_edid = jQuery(this).attr('class').split('smsdetail_');
					jQuery('#sms'+sms_edid[1]).prop('checked',false);
					jQuery(this).fadeOut('slow');
				}
			 });
			
			
			 jQuery('.emaildetail_'+toggle_id).toggle("blind", {direction: "vertical"}, 1000);
			 jQuery('.smsdetail_'+sms_toggle_id).toggle("blind", {direction: "vertical"}, 1000);
			 jQuery('.detail-id_'+toggle_id).toggle('slow');
		});
	//});
	/* location phone number 
	jQuery(document).ready(function() {
		jQuery("#location-phone-number").intlTelInput({
			utilsScript: "/utils.js"
		}); 
	});*/
	/* google map */
	 // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

	/* color tag for service*/
jQuery(document).bind('ready ajaxComplete', function(){
            jQuery('.demo').each( function() {
                jQuery(this).minicolors({
                    control: jQuery(this).attr('data-control') || 'hue',
                    defaultValue: jQuery(this).attr('data-defaultValue') || '',
                    format: jQuery(this).attr('data-format') || 'hex',
                    keywords: jQuery(this).attr('data-keywords') || '',
                    inline: jQuery(this).attr('data-inline') === 'true',
                    letterCase: jQuery(this).attr('data-letterCase') || 'lowercase',
                    opacity: jQuery(this).attr('data-opacity'),
                    position: jQuery(this).attr('data-position') || 'bottom left',
                    change: function(value, opacity) {
                        if( !value ) return;
                        if( opacity ) value += ', ' + opacity;
                        if( typeof console === 'object' ) {
                            console.log(value);
                        }
                    },
                    theme: 'bootstrap'
                });

            });

        });

jQuery(document).bind('ready ajaxComplete', function(){
		jQuery('.selectpicker').selectpicker({
		    container: 'body'
	   });

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			jQuery('.selectpicker').selectpicker('mobile');
		}
	});
	
jQuery(document).bind('ready ajaxComplete', function(){
    jQuery('.mp-show-hide-checkbox').change(function() {
        var toggle_id = jQuery(this).attr('id');
        jQuery('.mycollapse_'+toggle_id).toggle('slow');
	});

});
	
	

	
/**********************************/
/******   settings > General settings  *************/

  /* for toggle medium enable disable collapse */
jQuery(document).bind('ready', function(){
    jQuery('.mp-toggle-sh').change(function() {
		var toggle_id = jQuery(this).attr('id');
        jQuery('.collapse_'+toggle_id).slideToggle();
	});
	/************** Partial Deposit ****************/	
	jQuery('.mp-toggle-pd').change(function() {
		var toggle_id = jQuery(this).attr('id');
		if(toggle_id == 'moveto_partial_deposit_status'){
			var payment_gateway_st = general_setting_pd_ed.payment_gateway_status;
			if(payment_gateway_st=='D' ){
				jQuery('#mp_partial_depost_error').show();
				jQuery(this).attr('checked',false);
				jQuery(this).parent().prop('className','mp-toggle-pd toggle btn btn-danger btn-sm off');
			}else{
				jQuery('.collapse_'+toggle_id).toggle( "blind", {direction: "vertical"}, 1000 );
			}
        }else{
			jQuery('.collapse_'+toggle_id).toggle( "blind", {direction: "vertical"}, 1000 );
		}
	});
});

/*  phone number  */
jQuery(document).ready(function () {
  
	jQuery("#company_country_code").intlTelInput({
	numberType: "polite",
	autoPlaceholder: "off",
	utilsScript: "utils.js"
	});
	
});

	
jQuery(document).bind('ready', function(){
    jQuery('input[name="moveto_taxvat_type"]').click(function(){
        if(jQuery(this).attr("value")=="P"){
           jQuery(".mp-tax-percent").show();
         }
        if(jQuery(this).attr("value")=="F"){
           jQuery(".mp-tax-percent").hide();           
        }
        
		});
	 jQuery('input[name="moveto_partial_deposit_type"]').click(function(){
        if(jQuery(this).attr("value")=="P"){
           jQuery(".mp-partial-deposit-percent").show();
        }
        if(jQuery(this).attr("value")=="F"){
           jQuery(".mp-partial-deposit-percent").hide();
        }
        
		});	
		
	});
	jQuery(document).bind('ready ajaxComplete', function(){
		jQuery('[data-toggle="tooltip"]').tooltip({
			placement: 'bottom',
			trigger: 'hover'
		});
		jQuery('.mp-tooltip-link').each(function(){
			jQuery(this).attr('href','javascript:void(0)');
		});
	
	});
	 
	jQuery(document).ready(function () { 
		if (jQuery("#mp").width() >= 768 && jQuery("#mp").width() < 1024){
			jQuery('[data-toggle="tooltip"]').tooltip({'placement': 'left'});
		}	
		
	});   

	
	
	/******   settings > discount coupons  *************/
	
	/* delete promocode popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-delete-promocode').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-delete-promocode').html();
		}
	  });
	});
	/* hide delete promocode popover */
	jQuery(document).on('click', '#mp-close-popover-delete-promocode', function(){			
		jQuery('.popover').fadeOut();
	});
	
	jQuery(document).on('click', '.mp-edit-coupon', function(){		
		jQuery('.mp-update-promocode').css('display','block');
	});
	
	
	/**********************************/
	/******   payments  page   *************/
		
	/* data table for export data with excel, csv, pdf */
jQuery(document).ready(function(){
		jQuery('#payments-details').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	});
jQuery(document).bind('ready ajaxComplete', function(){
		
		/* Hide Past Dates on Coupon Expiry datepicker */
		jQuery(".mp_coupon_expiry").datepicker({
                minDate: 0
        });
		jQuery("#mp_booking_datetime").datepicker({
                minDate: 0
        });
		
		jQuery('#mp-promocode-list').DataTable();
		responsive: true
	} );
	
	/**********************************/

	
	/* data table for export data with excel, csv, pdf */
	
	/* staff member information export details */
jQuery(document).ready(function(){
		jQuery('#staff-info-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	/* services information export details */
		jQuery('#services-info-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	/* category information export details  */	
		jQuery('#category-info-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	/* registered customers booking details page */
		jQuery('#registered-client-booking-details').DataTable( {
			dom: 'frtipB',
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	/* registered customers listing page */
		jQuery('#registered-client-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				/* 'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5' */
				 {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                     columns: [ 0, 1, 2 ]
                }
            },
			{
                extend: 'csvHtml5',
                exportOptions: {
                     columns: [ 0, 1, 2 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2]
                }
            }
			]
		});

	/* guest customers listing page */
		jQuery('#guest-client-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	/* guest customers booking details page */

		jQuery('#guest-client-booking-details').DataTable( {
			dom: 'frtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
	/* Location Data Table in Export */	
		jQuery('#location-info-table').DataTable( {
			dom: 'frtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
		
		/* reviews all table */	
		jQuery('#mp-published-reviews-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
		jQuery('#mp-pending-reviews-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
		jQuery('#mp-hidden-reviews-table').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
		
		
		
	});
	
	/* dashboard chart */
	var doughnutData = [
		{
			value: 80,
			color:"#F7464A",
			highlight: "#FF5A5E",
			label: "<b>dsfkljfkdskfd<br />asaklfsdklfsdakl</b>"
		},
		{
			value: 50,
			color: "#46BFBD",
			highlight: "#5AD3D1",
			label: "Green"
		},
		{
			value: 100,
			color: "#FDB45C",
			highlight: "#FFC870",
			label: "Yellow"
		},
		{
			value: 40,
			color: "#949FB1",
			highlight: "#A8B3C5",
			label: "Grey"
		},
		{
			value: 120,
			color: "#4D5360",
			highlight: "#616774",
			label: "Dark Grey"
		}

	];

	/* Dashboard today and latest activity popup */
	jQuery(document).on('click','.mp-today-list',function(){
		jQuery('.modal').css('background','rgba(0,0,0,0.1)');
		
    });
	jQuery(document).on('click','.mp-activity-list',function(){
        jQuery('.modal').css('background','rgba(0,0,0,0.1)');
    });
	
	
	
	
/* custom form fields drag and drop */	
		
	/* jQuery(document).bind('ready ajaxComplete', function($){
		'use strict';
		var template = document.getElementById('form-builder-template'),
		  formContainer = document.getElementById('rendered-form'),
		  renderBtn = document.getElementById('render-form-button');
		jQuery(template).formBuilder();

		jQuery(renderBtn).click(function(e) {
		  e.preventDefault();
		  jQuery(template).formRender({
			container: jQuery(formContainer)
		  });
		});
	  });
	  */
	  
  
jQuery(document).ready(function($) {
  var buildWrap = document.querySelector('.build-wrap'),
    renderWrap = document.querySelector('.render-wrap'),
    editBtn = document.getElementById('edit-form'),
    formData = window.sessionStorage.getItem('formData'),	
	savedformdata = header_object.moveto_custom_formfields_val,
    editing = true,
    fbOptions = {
      dataType: 'json',
	  controlOrder: [
		'autocomplete',
		'button',
		'checkbox',
		'checkbox-group',
		'date',
		'file',
		'header',
		'hidden',
		'paragraph',
		'number',
		'radio-group',
		'select',
		'text',
		'textarea'
  ],
  disableFields: ['autocomplete','file','header','paragraph','button','hidden','date']
    };

  if(savedformdata!=''){
		fbOptions.formData = '['+savedformdata+']';
   }

  var toggleEdit = function() {
    document.body.classList.toggle('form-rendered', editing);
    editing = !editing;
  };

  var formBuilder = $(buildWrap).formBuilder(fbOptions).data('formBuilder');

  $('.form-builder-save').click(function() {
	   

	    var formdata = formBuilder.formData;
	    var ajax_url = general_settings_ajax_path.ajax_path;
		var postdata = { formdata:formdata,general_ajax_action:'save_custom_form' }
		  jQuery.ajax({					
						url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {	
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
			});	
	  
    toggleEdit();
    $(renderWrap).formRender({
      dataType: 'json',
      formData: formBuilder.formData
    });
	
    window.sessionStorage.setItem('formData', JSON.stringify(formBuilder.formData));
  });

  /* editBtn.onclick = function() {
    toggleEdit();
  }; */
});	  
	  
/* close form-builder */	
	
	  
	  
/* image upload in services */	  
// convert bytes into friendly format
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

// check for selected crop region
function checkForm() {
    if (parseInt(jQuery('#w').val())) return true;
    jQuery('.error').html('Please select a crop region and then press Upload').show();
    return false;
};


// clear info by cropping (onRelease event handler)
function clearInfo() {
    jQuery('.info #w').val('');
    jQuery('.info #h').val('');
};

// Create variables (in this scope) to hold the Jcrop API and image size
var jcrop_api, boundx, boundy;


jQuery(document).on('change','.mp-upload-images',function(){
//function fileSelectHandler(uploadsection) {
	var uploadsection=jQuery(this).attr('id');
	var bdus = jQuery(this).data('us');
	var oFile = jQuery('#'+uploadsection)[0].files[0];
	jQuery('#'+bdus+'bdimagename').val(oFile.name);
	
	// check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if (! rFilter.test(oFile.type)) {
        jQuery('.error').html('Please select a valid image file (jpg and png are allowed)').show();
        return;
    }

    /* check for file size
    if (oFile.size > 2500 * 5000) {  //if (oFile.size > 250 * 1024) {
        jQuery('.error').html('You have selected too big file, please select a one smaller image file').show();
        return;
    }*/

    // preview element
    var oImage = document.getElementById('mp-preview-img'+bdus);

    // prepare HTML5 FileReader
    var oReader = new FileReader();
        oReader.onload = function(e) {

        // e.target.result contains the DataURL which we can use as a source of the image
        oImage.src = e.target.result;
        oImage.onload = function () { // onload event handler

			// show image popup for image crop
			jQuery('#mp-image-upload-popup'+bdus).modal();
			
			/* display some basic image info*/
			var sResultFileSize = bytesToSize(oFile.size);
			jQuery('#'+bdus+'filesize').val(sResultFileSize);
			jQuery('#'+bdus+'bdimagetype').val(oFile.type);
			//jQuery('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

			// destroy Jcrop if it is existed
			if (typeof jcrop_api != 'undefined') {
				jcrop_api.destroy();
				jcrop_api = null;
				jQuery('#mp-preview-img'+bdus).width(oImage.naturalWidth);
				jQuery('#mp-preview-img'+bdus).height(oImage.naturalHeight);
			}

			setTimeout(function(){
				jQuery('#'+bdus+'w').val(oImage.naturalWidth);
				jQuery('#'+bdus+'h').val(oImage.naturalHeight);
				// initialize Jcrop
				jQuery('#mp-preview-img'+bdus).Jcrop({
					minSize: [32, 32], // min crop size
					/* aspectRatio : 1, */ // keep aspect ratio 1:1
					bgFade: true, // use fade effect
					bgOpacity: .3, // fade opacity
					//maxSize: [200, 200],
					boxWidth: 575,   //Maximum width you want for your bigger images
					boxHeight: 400,
					onChange: function(e){  jQuery('#'+bdus+'x1').val(e.x);
											jQuery('#'+bdus+'y1').val(e.y);
											jQuery('#'+bdus+'x2').val(e.x2);
											jQuery('#'+bdus+'y2').val(e.y2);
											jQuery('#'+bdus+'w').val(e.w);
											jQuery('#'+bdus+'h').val(e.h);} ,
					onSelect: function(e){ jQuery('#'+bdus+'x1').val(e.x);
											jQuery('#'+bdus+'y1').val(e.y);
											jQuery('#'+bdus+'x2').val(e.x2);
											jQuery('#'+bdus+'y2').val(e.y2);
											jQuery('#'+bdus+'w').val(e.w);
											jQuery('#'+bdus+'h').val(e.h);},
					onRelease: clearInfo
				}, function(){

					// use the Jcrop API to get the real image size
					var bounds = this.getBounds();
					boundx = bounds[0];
					boundy = bounds[1];

					// Store the Jcrop API in the jcrop_api variable
					jcrop_api = this;
				});
			},500);

        };
    };

    // read selected file as DataURL
    oReader.readAsDataURL(oFile);
});




	

	/* delete member image popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-remove-member-image').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-mp-remove-member-image').html();
		}
	  });
	});
	/* hide delete member image popover */
	jQuery(document).on('click', '#mp-close-popover-member-image', function(){			
		jQuery('.popover').fadeOut();
	});
	
	/* delete customer image popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-remove-customer-image').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-mp-remove-customer-image').html();
		}
	  });
	});
	/* hide delete customer image popover */
	jQuery(document).on('click', '#mp-close-popover-customer-image', function(){			
		jQuery('.popover').fadeOut();
	});
	
	/* delete new customer image popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-remove-new-customer-image').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-mp-remove-new-customer-image').html();
		}
	  });
	});
	/* hide delete new customer image popover */
	jQuery(document).on('click', '#mp-close-popover-new-customer-image', function(){			
		jQuery('.popover').fadeOut();
	});
	
/* delete company logo popover */
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-remove-company-logo').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-mp-remove-company-logo').html();
		}
	  });
	});
	/* hide delete company logo popover */
	jQuery(document).on('click', '#mp-close-popover-company-logo', function(){			
		jQuery('.popover').fadeOut();
	});
	
	/* user new and existing radio show hide fields */
	jQuery(document).on('click', '.mp-existing-usercl', function(){		
		jQuery('.mp-existing-user-data').show('slow');
		jQuery('.mp-new-user-data').hide('slow');
		jQuery('#mp-staff-fullname-error').hide();
		jQuery('#mp-staff-email-error').hide();
		jQuery('#mp-staff-username-error').hide();
		jQuery('#mp-staff-password-error').hide();
	});
	jQuery(document).on('click', '.mp-new-usercl', function(){		
		jQuery('.mp-new-user-data').show('slow');
		jQuery('.mp-existing-user-data').hide('slow');
		jQuery('#mp-selected-wp-user-error').hide();

	});	  
		
jQuery("#mp-select-service :checkbox").on('click', function(){
		jQuery(this).closest('li').toggleClass("mp-checked");
});

	
	
/*	Admin Area Functionality Js Code Start */	 
jQuery(document).on('ready', function(){
		function cb(start, end) {
			jQuery('#mp_reportrange span').html(start.format(date_format_for_js) + ' - ' + end.format(date_format_for_js));
		}
		cb(moment().subtract(29, 'days'), moment());

		jQuery('#mp_reportrange').daterangepicker({
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);
});

/*** All Staff Linking ON/OFF Code ***/
jQuery(document).on('change','.linkallstaff',function(){
	var service_id = jQuery(this).data('service_id');
	if(jQuery(this).is(":checked")){
			jQuery('.mp_all_staff'+service_id).each(function(){
			jQuery(this).prop('checked', true);			    
			jQuery(this).parent().prop('className','toggle btn btn-default on');
		}); 
	}else{
		var serv_id=jQuery(this).data('service_id');/*service id*/	
		jQuery('.mp_all_staff'+service_id).each(function(){
			jQuery(this).prop('checked', false);
			jQuery(this).parent().prop('className','toggle btn btn-default off');
		});
	}
});
/*** All Service Linking ON/OFF Code on Staff Service Schedule Price***/
jQuery(document).on('change','.linkallservices',function(){
	var staff_id = jQuery(this).data('staff_id');
	if(jQuery(this).is(":checked")){
			jQuery('.mp_all_service'+staff_id).each(function(){
			jQuery(this).prop('checked', true);
			jQuery(this).parent().prop('className','toggle btn btn-default on');
		}); 
	}else{
		var staff_id=jQuery(this).data('staff_id');/*service id*/	
		jQuery('.mp_all_service'+staff_id).each(function(){
			jQuery(this).prop('checked', false);
			jQuery(this).parent().prop('className','toggle btn btn-default off');
		});
	}
});


/** Add ShortTag into Email Template On Click of TagName **/
function insertAtCaret(areaId,text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
        "ff" : (document.selection ? "ie" : false ) );
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        strPos = range.text.length;
    }
    else if (br == "ff") strPos = txtarea.selectionStart;
    var front = (txtarea.value).substring(0,strPos);  
    var back = (txtarea.value).substring(strPos,txtarea.value.length); 
    txtarea.value=front+text+back;
    strPos = strPos + text.length;
    if (br == "ie") { 
        txtarea.focus();
        var range = document.selection.createRange();
        range.moveStart ('character', -txtarea.value.length);
        range.moveStart ('character', strPos);
        range.moveEnd ('character', 0);
        range.select();
    }
    else if (br == "ff") {
        txtarea.selectionStart = strPos;
        txtarea.selectionEnd = strPos;
        txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
}
jQuery(document).on('click','.tags',function(){
	var tag = jQuery(this).data('value');
	var email_id = jQuery(this).data('eid');
	insertAtCaret('email_editor'+email_id,tag);
	insertAtCaret('sms_editor'+email_id,tag);
	return false;
});


/***********************Location Jquery**********************/
/* On click Create Location Validate Form */
jQuery(document).on('click','#mp_create_location',function(){
		if(jQuery('#mp_create_location_cl').valid()){
			jQuery('#mp_create_location_cl').submit();
		}				
});						
						
/* Get Location By City/State */
jQuery(document).on('click','.getsorted_locations',function(){
		var ajax_url = locationObj.plugin_path;
		var sortingvalue = jQuery(this).data('location_sortby');
		var postdata = { sortingvalue:sortingvalue,
						 location_action:'sortbylocations'						 
		}
		jQuery('.loader').show();
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/location_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {	
							jQuery('.loader').hide();
							jQuery('#sortable-locations').html(response);
							jQuery('.mainheader_message_inner').show();
							moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});	
		
});

/* Update Location Status */	
jQuery(document).on('change','.update_location_status',function(){
		var ajax_url = locationObj.plugin_path;
		var location_id = jQuery(this).data('id');
		if(jQuery(this).is(':checked')){
			var location_status = 'E';
		}else{
			var location_status =  'D';
		}
		jQuery('.loader').show();
		var postdata = { location_id:location_id,
						 location_status:location_status,
						 location_action:'updatelocationstatus'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/location_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  

/* Delete Location Permanently */	
jQuery(document).on('click','.delete_location',function(){
		var ajax_url = locationObj.plugin_path;
		var location_id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { location_id:location_id,
						 location_action:'delete_location'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/location_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.mp-locations-container').html(response);
						jQuery('.loader').hide();
						jQuery('#location_detail_'+location_id).fadeOut('slow');
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  

/* Update Location Detail */	
jQuery(document).on('click','.update_location',function(){
		var ajax_url = locationObj.plugin_path;
		var location_id = jQuery(this).data('location_id');
		if(!jQuery("#mp_update_location_"+location_id).valid()){
				return false;
		}		
		
		var location_title = jQuery('#mp-location-name'+location_id).val();
		var description = jQuery('#mp-location-desc'+location_id).val();
		var image = jQuery('#bdll'+location_id+'uploadedimg').val();
		var email = jQuery('#location-email'+location_id).val();
		var phone = jQuery('#location-phone-number'+location_id).val();
		var address = jQuery('#mp-location-address'+location_id).val();
		var city = jQuery('#mp-location-city'+location_id).val();
		var state = jQuery('#mp-location-state'+location_id).val();
		var zip = jQuery('#mp-location-zip'+location_id).val();
		var country = jQuery('#mp-location-country'+location_id).val();
		jQuery('.loader').show();
		var postdata = { location_id:location_id,
						 location_title:location_title,						 
						 description:description,						 
						 image:image,						 
						 email:email,						 
						 phone:phone,						 
						 address:address,						 
						 city:city,						 
						 state:state,						 
						 zip:zip,						 
						 country:country,						 
						 location_action:'update_location'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/location_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mp-locations-container').html(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 

/* Upload Create Location Image */
	jQuery(document).on("click",".mp_upload_img",function(e) {
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;
		var file_data =jQuery("#"+jQuery(this).data('imageinputid')).prop("files")[0];
		var formdata = new FormData();
		var bdus = jQuery(this).data('us');
		var img_w = jQuery('#'+bdus+'w').val();
		var img_h = jQuery('#'+bdus+'h').val();
		var img_x1 = jQuery('#'+bdus+'x1').val();
		var img_x2 = jQuery('#'+bdus+'x2').val();
		var img_y1 = jQuery('#'+bdus+'y1').val();
		var img_y2 = jQuery('#'+bdus+'y2').val();
		formdata.append("image",file_data);
		formdata.append("w",img_w);
		formdata.append("h",img_h);
		formdata.append("x1",img_x1);
		formdata.append("x2",img_x2);
		formdata.append("y1",img_y1);
		formdata.append("y2",img_y2);

		var siteurl = ajax_url.split('wp-content');

			jQuery.ajax({
				url: ajax_url+"/assets/lib/upload.php", 
				type: "POST",           
				data:formdata, 
                cache: false,
                contentType: false, 
				processData:false,        
				success: function(data){
					jQuery('.loader').hide();
					jQuery('#'+bdus+'uploadedimg').val(data);
					jQuery('.hidemodal').trigger('click');
					jQuery('#'+bdus+'locimage').attr('src',siteurl[0]+'wp-content/uploads'+data);
					jQuery('#'+bdus+'addimage').attr('src',siteurl[0]+'wp-content/uploads'+data);
				}
			});
		});
		
/* Sort Location Position  */
jQuery(document).ready(function(){		
			jQuery("#sortable-locations").sortable({
				update : function(event,ui){
				var ajax_url = locationObj.plugin_path;
				var position = jQuery(this).sortable('serialize');
				
				var postdata = {
				position : position,
				location_action :'sort_location_position'
				};
				jQuery('.loader').show();
				jQuery.ajax({
					url  : ajax_url+"/assets/lib/location_ajax.php",	
					type : 'POST',
					data : postdata,	
					success : function(response){
					jQuery('.loader').hide();
					jQuery('.mp-locations-container').html(response);
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					}
				});
				}
			});

	});	
						/***********************Location Jquery End Here **********************/	
						
							/*********************** Category Jquery **********************/
/** Add new category button and popover **/
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-add-new-category').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-content-wrapper').html();
		}
	  });
	});
/** Hide add new service popover **/
jQuery(document).on('click', '#mp-close-popover-new-service-category', function(){			
		jQuery('.popover').fadeOut();
});

/** Delete service category popover **/
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-delete-service-category').popover({ 
		html : true,
		content: function() {		
		  return jQuery('#popover-delete-service-category').html();
		 
		}
	  });
	});
/** Hide delete service category popover **/
jQuery(document).on('click', '#mp-close-popover-delete-service-category', function(){			
		jQuery('.popover').fadeOut();
});		
							
/** Create Category **/
jQuery(document).on('click','.mp_create_category',function(){
	var ajax_url = serviceObj.plugin_path;
		var categorytitle_err_msg = admin_validation_err_msg.categorytitle_err_msg;
		jQuery('#mp_create_category').validate();
		jQuery("#mp_category_title").rules("add",{ required: true,remote: {
															url  : ajax_url+"/assets/lib/service_ajax.php",
															type: "POST",
															async: true,
															data: {
															title:function() {
																 return jQuery('#mp_category_title').val();
																},
																/* add_provider:'yes', */
																action:"check_category_title"
															}
														},  messages: { required: categorytitle_err_msg , remote:"Category Title Already Exists!!!"}});
			if(!jQuery('#mp_create_category').valid()){
				return false;
		}
	
		
		var category_title = jQuery('#mp_category_title').val();
		jQuery('.loader').show();
		var postdata = { category_title:category_title,
						 category_action:'create_category'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/category_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('#mp_category_listing').html(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					jQuery.ajax({					
							url  : ajax_url+"/assets/lib/category_ajax.php",					
							type : 'POST',					
							data : {category_action:'read_category_dd_options'},				
							dataType : 'html',					
							success  : function(response) {
								jQuery('#mp_service_categories').html(response);
								jQuery('#mp_service_categories').selectpicker('refresh');
								jQuery('.mainheader_message_inner').show();
								moveto_hide_success_msg();
								
							}
						});
										
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 
/* Sort Category Position  */
jQuery(document).ready(function(){		
			jQuery("#sortable-category-list").sortable({
				update : function(event,ui){
				var ajax_url = serviceObj.plugin_path;
				var position = jQuery(this).sortable('serialize');
				
				var postdata = {
				position : position,
				category_action :'sort_category_position'
				};
				jQuery('.loader').show();
				jQuery.ajax({
					url  : ajax_url+"/assets/lib/category_ajax.php",	
					type : 'POST',
					data : postdata,	
					success : function(response){
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					}
				});
				}
			});

	});	
	
						/*********************** Category Jquery End Here **********************/	
						
						   /*********************** City Jquery **********************/

/* Add City Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var source_city_err_msg = admin_validation_err_msg.source_city_err_msg;	
	var destination_city_err_msg = admin_validation_err_msg.destination_city_err_msg;
	var city_price_err_msg = admin_validation_err_msg.city_price_err_msg;	
	var city_price_validprice_err_msg = admin_validation_err_msg.city_price_validprice_err_msg;	
	var siteurl = header_object.site_url;	
	
		jQuery('#mp_add_city').validate({
			rules: {
				city_source: {
											required: true,
											remote: {
															url: siteurl+"/wp-admin/admin-ajax.php",
															type: "POST",
															async: true,
															data: {
																source_city:function(){
																 return jQuery('input[name="city_source"]').val();
																},
																destination_city:function(){
																 return jQuery('input[name="city_destination"]').val();
																},
																action:"check_route"
															}
														},

					},
				city_destination: {
											required: true,
											remote: {
															url: siteurl+"/wp-admin/admin-ajax.php",
															type: "POST",
															async: true,
															data: {
																source_city:function(){
																	return jQuery('input[name="city_source"]').val();
																},
																destination_city:function(){
																	return jQuery('input[name="city_destination"]').val();
																},
																action:"check_route"
															}
														},
								}				
				},
			messages: {
						city_source: { required:source_city_err_msg }, 					
						city_destination: { required:destination_city_err_msg }				
			}
		});		
});							   
						   
						   
/* Update City Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var source_city_err_msg = admin_validation_err_msg.source_city_err_msg;	
	var destination_city_err_msg = admin_validation_err_msg.destination_city_err_msg;
	var city_price_err_msg = admin_validation_err_msg.city_price_err_msg;	
	var city_price_validprice_err_msg = admin_validation_err_msg.city_price_validprice_err_msg;	
	var siteurl = header_object.site_url;
	
	jQuery('.mp_update_city').each(function(){
			var city_id = jQuery(this).data('cid');
				jQuery(this).validate({
					rules: {
						u_city_source: {
													required: true,
													remote: {
															url: siteurl+"/wp-admin/admin-ajax.php",
															type: "POST",
															async: true,
															data: {
																source_city:function() {
																 return jQuery('#mp-city-source'+city_id).val();
																},
																destination_city:function() {
																 return jQuery('#mp-city-destination'+city_id).val();
																},
																action:"check_route"
															}
														},

							},
						u_city_destination: {
													required: true,
													remote: {
															url: siteurl+"/wp-admin/admin-ajax.php",
															type: "POST",
															async: true,
															data: {
																source_city:function() {
																 return jQuery('#mp-city-source'+city_id).val();
																},
																destination_city:function() {
																 return jQuery('#mp-city-destination'+city_id).val();
																},
																action:"check_route"
															}
														},

							}
						},
					messages: {
								u_city_source: { required:source_city_err_msg }, 					
								u_city_destination: { required:destination_city_err_msg }			
								
								
						}
				});
		});			
});								   
						   
/* Add City Detail */	
jQuery(document).on('change','#mp-city-routes',function(){ 
	jQuery('#mp_create_city_single').hide();
	jQuery('#mp-city-cource').val('a');
	jQuery('#mp-city-cource').focusout();
	jQuery('#mp-city-destination').val('a');
	jQuery('#mp-city-destination').focusout();
	jQuery('#mp-city-price').val('1');
	jQuery('#mp-city-price').focusout();

});
						   

/* Add City Detail */	
jQuery(document).on('click','#mp_create_city',function(){ 
	var bulkuploadstatus = jQuery('#mp-city-routes').val();
	if(bulkuploadstatus!=''){
		jQuery('#mp_add_city').submit();	
	}else{
		if(jQuery('#mp_add_city').valid()){
			jQuery('#mp_add_city').submit();
			/* jQuery('#mp_create_city_submit').trigger('click'); */
		}
	}
});
	
/* Update City Detail */	
jQuery(document).on('click','.update_city',function(){
		var ajax_url = cityObj.plugin_path;	
		var city_id = jQuery(this).data('city_id');
				
		if(!jQuery("#mp_update_city_"+city_id).valid()){
				return false;
		}
		
		var source_city = jQuery('#mp-city-source'+city_id).val();		
		var destination_city = jQuery('#mp-city-destination'+city_id).val();
		var price = jQuery('#mp-city-price'+city_id).val();
		
		jQuery('.loader').show();
		var postdata = { city_id:city_id,
						 source_city:source_city,						 
						 destination_city:destination_city,							 
						 price:price,					 					 
						 city_action:'update_city'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/city_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();						
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 	

/* Delete City Permanently */	
jQuery(document).on('click','.delete_city',function(){
		var ajax_url = cityObj.plugin_path;
		var city_id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { city_id:city_id,
						 city_action:'delete_city'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/city_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('#city_detail_'+city_id).fadeOut('slow');
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();				
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  						   
						   
						   
						   
						   /*********************** City Jquery End Here **********************/	
						
						   /*********************** Services Jquery **********************/	
/** Add new service **/

jQuery(document).on('click', '#mp-add-new-service', function(){	
	jQuery( ".mp-show-hide-checkbox" ).trigger( "click" );		
	jQuery('.mp-add-new-service').fadeIn();
	jQuery('html, body').stop().animate({
	'scrollTop': jQuery('.mp-add-new-service').offset().top - 35
	}, 2000, 'swing', function () {});
});

/** Add new service addons **/

jQuery(document).on('click', '#mp-add-new-service-addons', function(){	
	jQuery('.mp-add-new-service-addons').fadeIn();
	jQuery('html, body').stop().animate({
	'scrollTop': jQuery('.mp-add-new-service-addons').offset().top - 35
	}, 2000, 'swing', function () {});
});
jQuery(document).on('click', '#mp-add-new-city', function(){	
	//jQuery( ".mp-show-hide-checkbox" ).trigger( "click" );		
	jQuery('.mp-add-new-city').fadeIn();
	jQuery( "#citydropnew" ).show();	
	jQuery('#mp_add_city').fadeIn();
	jQuery('html, body').stop().animate({
	'scrollTop': jQuery('.mp-add-new-city').offset().top - 35
	}, 2000, 'swing', function () {});
});



jQuery(function() {
	jQuery( "#sortable-services" ).sortable({ handle: '.fa-th-list' });
	jQuery( "#sortable-service-list" ).sortable();
}); 

jQuery(function() {
	jQuery( "#sortable-movesize" ).sortable({ handle: '.fa-th-list' });
	jQuery( "#sortable-service-list" ).sortable();
}); 												   
/** Delete service & location popover **/
jQuery(document).bind('ready ajaxComplete', function(){
		jQuery('.mp-delete-popover').popover({ 
				html : true,
				content: function() {
				  return jQuery('#'+jQuery(this).data('poid')).html();
				}
		});
});
/** Hide delete service & location popover **/
jQuery(document).on('click', '.mp-close-popover-delete', function(){			
	jQuery('.popover').fadeOut();
});						   

jQuery(document).on('click','.u_service_offeredprice,input[name="offered_price"]',function(){
	jQuery('label.error').each(function(){
		jQuery(this).hide();
	});
});		

/* Offered Price Should Less then Default On Save New Service */
jQuery(document).on('click','#mp_create_service',function(){	
	var serviceofferpricegreater_err_msg = admin_validation_err_msg.serviceofferpricegreater_err_msg;
	
	if(jQuery("#mp_create_service").valid()){
			
		var service_amount = jQuery('input[name="service_price"]').val();
		var service_offeredprice = jQuery('input[name="offered_price"]').val();
		jQuery('#offered_price-error').hide();
		if(service_offeredprice>=service_amount){
		jQuery('#offered_price-error').show();
		jQuery('#offered_price-error').text(serviceofferpricegreater_err_msg);
			return false;
		}
		
		
	}
	
});	   
						   
/* Update Service Detail */	
jQuery(document).on('click','.update_service',function(){
		var ajax_url = serviceObj.plugin_path;
		var serviceofferpricegreater_err_msg = admin_validation_err_msg.serviceofferpricegreater_err_msg;
		var service_id = jQuery(this).data('service_id');
				
		if(!jQuery("#mp_update_service_"+service_id).valid()){
				return false;
		}
		
		
		var color_tag = jQuery('#mp-service-color-tag'+service_id).val();
		var service_title = jQuery('#mp-service-title'+service_id).val();
		var image = jQuery('#bdls'+service_id+'uploadedimg').val();
		var service_description = jQuery('#mp-service-desc'+service_id).val();
		var service_category = jQuery('#mp-service-category'+service_id).val();
		var service_duration_hrs = jQuery('#mp-duration-hrs'+service_id).val();
		var service_duration_mins = jQuery('#mp-duration-mins'+service_id).val();
		var service_amount = jQuery('#mp-service-price'+service_id).val();
		var service_offeredprice = jQuery('#mp-service-offered-price'+service_id).val();
		jQuery('#mp-service-offered-price'+service_id+'-error').hide();
		if(service_offeredprice>=service_amount){
		jQuery('#mp-service-offered-price'+service_id+'-error').show();
		jQuery('#mp-service-offered-price'+service_id+'-error').text(serviceofferpricegreater_err_msg);
			return false;
		}		
		
		jQuery('.loader').show();
		var postdata = { service_id:service_id,
						 color_tag:color_tag,						 
						 service_title:service_title,						 
						 image:image,						 
						 service_description:service_description,						 
						 service_category:service_category,						 
						 service_duration_hrs:service_duration_hrs,						 
						 service_duration_mins:service_duration_mins,						 
						 service_amount:service_amount,						 					 
						 service_offeredprice:service_offeredprice,				 					 
						 service_action:'update_service'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/service_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('#mp_category_listing').html(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 	

/* Delete Service Permanently */	
jQuery(document).on('click','.delete_service',function(){
		var ajax_url = serviceObj.plugin_path;
		var service_id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { service_id:service_id,
						 service_action:'delete_service'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/service_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('#service_detail_'+service_id).fadeOut('slow');
						jQuery.ajax({					
									url  : ajax_url+"/assets/lib/category_ajax.php",			
									type : 'POST',					
									data : {category_action:'get_category_lsiting'},			
									dataType : 'html',					
									success  : function(response) {								
										jQuery('#mp_category_listing').html(response);
										jQuery('.mainheader_message_inner').show();
										moveto_hide_success_msg();
									}
						});						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  					

/* Delete Category */	
jQuery(document).on('click','#mp-delete-category',function(){
		var ajax_url = serviceObj.plugin_path;
		var category_id = jQuery('.mp_category_services.active').data('cid');
		if(jQuery(this).data('del')=='N'){		
			return false;
		}
		
		jQuery('.loader').show();
		var postdata = { category_id:category_id,
						 category_action:'delete_category'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/category_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('#mp-close-popover-delete-service-category').trigger('click');
						jQuery('#mp_category_listing').html(response);
						jQuery('.mp_category_all_service').trigger('click');
						jQuery('#category_detail_'+category_id).fadeOut('slow');
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 

/* Update Service Status */	
jQuery(document).on('change','.update_service_status',function(){
		var ajax_url = serviceObj.plugin_path;
		var service_id = jQuery(this).data('id');
		if(jQuery(this).is(':checked')){
			var service_status = 'Y';
		}else{
			var service_status =  'N';
		}
		jQuery('.loader').show();
		var postdata = { service_id:service_id,
						 service_status:service_status,
						 service_action:'updateservicestatus'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/service_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  	

/* Link Service With Provider */
jQuery(document).on('change','.link_staff',function(){
	var staff_id = jQuery(this).val();
	var service_id = jQuery(this).data('service_id');
	var ajax_url = serviceObj.plugin_path;
	if(jQuery(this).is(":checked")){
		var service_action = 'link_staff';
	}else{
		var service_action = 'unlink_staff';
	}
	
	if(staff_id!='all' && jQuery('.linkallstaff').is(':checked')){
		jQuery('.linkallstaff').prop('checked',false);
	}

	var postdata = { service_id:service_id,
					 staff_id:staff_id,
					 service_action:service_action						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/service_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});

							/***********************Service Jquery End Here **********************/	
						
							/*********************** Staff Jquery **********************/
/** Add new staff button and popover **/
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-add-new-staff').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-content-wrapper').html();
		}
	  });
});
/** Hide Create New Staff Popover **/	
jQuery(document).on('click', '#mp-close-popover-new-staff', function(){			
		jQuery('.popover').fadeOut();
});
/** delete staff member popover **/
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('#mp-delete-staff-member').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-delete-member').html();
		}
	  });
	});
/** Hide delete staff popover **/
	jQuery(document).on('click', '#mp-close-popover-delete-staff', function(){			
		jQuery('.popover').fadeOut();
	});
/** Sort StafF Members **/
jQuery(function() {
	jQuery( "#sortable" ).sortable();	
});
/** Staff Delete Breaks Popover**/
jQuery(document).bind('ready ajaxComplete', function(){
	jQuery('.staff_delete_break').popover({ 
		html : true,
		content: function() {
			var break_id = jQuery(this).data('bid');
			jQuery('.popover').each(function(){
				jQuery(this).fadeOut('slow');
			});
		  return jQuery('#popover-delete-breaks'+break_id).html();
		}
	});
});
/** hide delete staff popover **/
jQuery(document).on('click', '.close_break_del_popover', function(){
	jQuery('.popover').fadeOut('slow');
});
/** for staff off time **/
jQuery(document).bind('ready ajaxComplete', function(){
		function cb(start, end) {
			jQuery('#offtime-daterange span').html(start.format(date_time_format_for_js) + ' - ' + end.format(date_time_format_for_js));
		}
		cb(moment().subtract(29, 'days'), moment());

		jQuery('#offtime-daterange').daterangepicker({
			timePicker: true,
			timePickerIncrement: 1,
			locale: {
				format: 'MM/DD/YYYY h:mm A'
			}
		}, cb);
	});	
jQuery(document).bind('ready ajaxComplete', function(){
		jQuery('#mp-staff-member-offtime-list').DataTable({
			destroy: true,
			searching: false,
			dom: 'frtipB',
			buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5'
			]
		});
});
/** Staff Service Schedule Price Delete **/
jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('.delete_ssp_popover').popover({ 
		html : true,
		content: function() {
			var sspid= jQuery(this).data('sspid');
			jQuery('.popover').each(function(){
				jQuery(this).fadeOut('slow');
			});
		  return jQuery('#popover-delete-price'+sspid).html();
		}
	  });
jQuery(document).on('click', '.cancel_ssp_delete', function(){	
		jQuery('.popover').fadeOut('slow');
      });
});					
/* Make/Unmake Staff Member As Manager -- Add/Remove Manager Cap **/	
jQuery(document).on('change','.mp_staff_manager',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).data('staff_id');	
		if(jQuery(this).prop('checked')==true){ var method ='add';}else{ var method='remove';}	
			var postdata = { staff_id:staff_id,
							 method:method,
							 staff_action:'staff_as_manager'						 
			}
			 jQuery.ajax({					
						url  : ajax_url+"/assets/lib/staff_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					}
			});
	}); 				
/* Get Selected Info Click from Left Side */
jQuery(document).on('click','.staff-list',function(){
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).data('staff_id');
		var rso_staff = jQuery('.mp-staff-member-name').data('staff_id');
		if(staff_id!=rso_staff){
			jQuery('.loader').show();
			
			var postdata = { staff_id:staff_id,
							 staff_action:'get_staff_right'						 
			}
			jQuery.ajax({					
						url  : ajax_url+"/assets/lib/staff_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
							jQuery('.loader').hide();
							jQuery('.mp-staff-details').html(response);
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
			});
		}
});
							
/* Create Staff Member */	
jQuery(document).on('click','#mp_create_staff_btn',function(){
		var ajax_url = header_object.plugin_path;
		var staffusername_err_msg = admin_validation_err_msg.staffusername_err_msg;	
		var staffusernameexist_err_msg = admin_validation_err_msg.staffusernameexist_err_msg;	
		var staffpassword_err_msg = admin_validation_err_msg.staffpassword_err_msg;
		var staffemail_err_msg = admin_validation_err_msg.staffemail_err_msg;
		var staffemailexist_err_msg = admin_validation_err_msg.staffemailexist_err_msg;
		var stafffullname_err_msg = admin_validation_err_msg.stafffullname_err_msg;
		var staffselect_err_msg = admin_validation_err_msg.staffselect_err_msg;
		var siteurl = header_object.site_url;
	
		jQuery('#mp_create_staff').validate({
					rules: {
						mp_newuser_username: {
													required: true,
													remote: {
															url  : ajax_url+"/assets/lib/front_ajax.php",
															type: "POST",
															async: true,
															data: {
															username:function() {
																 return jQuery('input[name="mp_newuser_username"]').val();
																},
																/* add_provider:'yes', */
																action:"check_username"
															}
														}
							},
						mp_newuser_password: {
													required: true
							},
						mp_newuser_fullname: {
													required: true,
							},
						mp_newuser_email: {
													required: true,
													remote: {
														url: siteurl+"/wp-admin/admin-ajax.php",
														type: "POST",
														async: true,
														data: {
														email:function() {
															return jQuery('input[name="mp_newuser_email"]').val();
															},		
															action:'check_email_bd'
														}
													},
													customemail:true
							},	
						mp_selected_wpuser : { required :true
							},	
						},
					messages: {
								mp_newuser_username: { required: staffusername_err_msg }, 
								mp_newuser_password: { required: staffpassword_err_msg }, 
								mp_newuser_fullname: { required: stafffullname_err_msg }, 
								mp_newuser_email: { required: staffemail_err_msg , customemail: staffemailexist_err_msg},
								mp_selected_wpuser: {required: staffselect_err_msg}
						}
				});
		
		var ajax_url = header_object.plugin_path;
		
		if(jQuery('#mp_create_staff').valid()){
		var usertype = jQuery('.mp-new-usercl:checked').val();
		var existing_userid = jQuery('#mp-selected-wp-user').val();
		var staff_username = jQuery('#mp-staff-username').val();
		var staff_password = jQuery('#mp-staff-password').val();
		var staff_location = jQuery('#mp-staff-location').val();
		var staff_fullname = jQuery('#mp-staff-fullname').val();
		var staff_email = jQuery('#mp-staff-email').val();
		jQuery('.loader').show();
		
		var postdata = { usertype:usertype,
						 existing_userid:existing_userid,
						 staff_username:staff_username,
						 staff_password:staff_password,
						 staff_location:staff_location,
						 staff_fullname:staff_fullname,
						 staff_email:staff_email,
						 staff_action:'create_staff'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					/* dataType : 'html',	 */				
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('#mp-close-popover-new-staff').trigger('click');
						jQuery('#mp-staff-sortable').html(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
		}
});  							
/* Validate Staff Phone */
/* Update Location Validation */
jQuery(document).bind('ready ajaxComplete', function(){

	var staffvalidphone_err_msg = admin_validation_err_msg.staffvalidphone_err_msg;
		jQuery.validator.addMethod("numeric_pattern", function(value, element) {
	return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/.test(value);
	}, staffvalidphone_err_msg);

		jQuery('.staff_personal_detail').each(function(){
				jQuery(this).validate({
					rules: {						
						staff_phone: {
								numeric_pattern:true
							},					
						},
						messages: {								
								staff_phone: { numeric_pattern:staffvalidphone_err_msg}
							}
				});
		});			
});

		
/* Update Staff Member Detail*/	
jQuery(document).on('click','.update_staff_detail',function(){
		var ajax_url = header_object.plugin_path;
		var site_url = header_object.site_url;
		var staff_id = jQuery(this).data('staff_id');
		
		if(!jQuery("#staff_personal_detail"+staff_id).valid()){
				return false;
		}	
				
		
		var staff_name = jQuery('#staff_name_'+staff_id).val();
		var staff_description = jQuery('#staff_description_'+staff_id).val();
		var staff_image = jQuery('#bdsdu'+staff_id+'uploadedimg').val();
		var staff_phone = jQuery('#staff_phone_'+staff_id).val();
		var staff_timezone = jQuery('#staff_timezone_'+staff_id).val();
		var staff_timezoneID = jQuery('#staff_timezone_'+staff_id+' option:selected').attr('timezoneid');
		var existing_st = jQuery('#curr_staff_schedule_'+staff_id).val();
		if(jQuery('#staff_schedule_'+staff_id).is(':checked')){
		var staff_schedule_type = 'M';	}else{var staff_schedule_type = 'W';	}		
		if(jQuery('#staff_status_'+staff_id).is(':checked')){	var staff_status = 'E';	}else{var staff_status = 'D';}
		jQuery('.loader').show();
		
		var postdata = { staff_id:staff_id,
						 staff_name:staff_name,
						 staff_description:staff_description,
						 staff_image:staff_image,
						 staff_phone:staff_phone,
						 staff_timezone:staff_timezone,
						 staff_timezoneID:staff_timezoneID,
						 staff_schedule_type:staff_schedule_type,
						 staff_status:staff_status,
						 staff_action:'update_staff_detail'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						if(existing_st!=staff_schedule_type){location.reload();}
						if(jQuery('#bdsdu'+staff_id+'uploadedimg').val()!=''){
							var newimgpath = site_url+'/wp-content/uploads'+jQuery('#bdsdu'+staff_id+'uploadedimg').val();		
							jQuery('#staff_detail_'+staff_id+' img').removeAttr('src');
							jQuery('#staff_detail_'+staff_id+' img').attr('src',newimgpath);
						}
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  

/* Staff Add Service Pricing */
jQuery(document).on('click','.mp_add_ssp',function(){
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).data('staffid');
		var service_id = jQuery(this).data('serviceid');
		var service_amount = jQuery(this).data('serviceamout');
		var weekdayid = jQuery(this).data('mainid');
		var weekid = jQuery(this).data('weekid');
		var dayid = jQuery(this).data('dayid');

		//jQuery('.loader').show();
		var postdata = { staff_id:staff_id,
						 service_id:service_id,
						 weekid:weekid,
						 dayid:dayid,
						 service_amount:service_amount,
						 staff_action:'add_service_schedule_price'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('#mp_ssp_'+service_id+'_'+weekdayid).append(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});
/* Staff Service Schedule EndTime */
jQuery(document).on('change','.ssp_starttime',function(){
		if(jQuery(this).hasClass('selectpicker')){
		var ajax_url = header_object.plugin_path;
		var ssp_id = jQuery(this).data('sspid');
		var ssp_starttime = jQuery(this).val();
		
			var postdata = { 
						 ssp_starttime:ssp_starttime,
						 staff_action:'staff_get_ssp_end'						 
			}
			 jQuery.ajax({					
						url  : ajax_url+"/assets/lib/staff_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
							jQuery('[name="ssp_endtime_'+ssp_id+'"] option').each(function() { 
									ssp_options = new Date('2016/01/07 '+jQuery(this).val());
									ssp_edoptions = new Date('2016/01/07 '+response);
									jQuery(this).show();
									
									if(ssp_options.getTime()< ssp_edoptions.getTime()){jQuery(this).hide();}
									if(jQuery(this).val() == response){ jQuery(this).attr('selected', 'selected');} 
								});
							
							jQuery('#ssp_endtime_'+ssp_id).selectpicker('refresh');
							jQuery('#ssp_endtime_'+ssp_id).selectpicker('val',response);
						}
			});
		}
	}); 
/* Update Staff Service Schedule Price */
jQuery(document).on('click','.update_ssp_detail',function(){
		var ajax_url = header_object.plugin_path;
		var ssp_id = jQuery(this).data('sspid');
		var ssp_starttime = jQuery('#ssp_starttime_'+ssp_id).val();
		var ssp_endtime = jQuery('#ssp_endtime_'+ssp_id).val();
		var ssp_price = jQuery('#ssp_price_'+ssp_id).val();

		jQuery('.loader').show();
		var postdata = { ssp_id:ssp_id,
						 ssp_starttime:ssp_starttime,
						 ssp_endtime:ssp_endtime,
						 ssp_price:ssp_price,
						 staff_action:'update_service_schedule_price'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});
/*Delete Service Schedule Price*/
jQuery(document).on('click','.delete_ssp',function(){
		var ajax_url = header_object.plugin_path;
		var ssp_id = jQuery(this).attr('id');
		jQuery('.loader').show();
		var postdata = { ssp_id:ssp_id,
						 staff_action:'delete_ssp'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('#mp_ssp_detail_'+ssp_id).fadeOut('slow');
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 
/* Link Provider With Service -on Service Schedule Price Module */
jQuery(document).on('change','.link_service',function(){
	var service_id = jQuery(this).val();
	var staff_id = jQuery(this).data('staff_id');
	var ajax_url = header_object.plugin_path;
	var currentstaff_services = jQuery('.staff_servicecount_'+staff_id).text();
	var total_services = jQuery('.staff_servicecount_'+staff_id).data('total_service');
	if(jQuery(this).is(":checked")){
		var staff_action = 'link_service';
		if(service_id=='all'){
			jQuery('.staff_servicecount_'+staff_id).text(total_services);
		}else{
			jQuery('.staff_servicecount_'+staff_id).text(parseInt(currentstaff_services)+parseInt(1));
		}
	}else{
		var staff_action = 'unlink_service';
		if(service_id=='all'){
			jQuery('.staff_servicecount_'+staff_id).text(0);
		}else{
			jQuery('.staff_servicecount_'+staff_id).text(parseInt(currentstaff_services)-parseInt(1));
		}
	}
	if(service_id!='all' && jQuery('.linkallservices').is(':checked')){
		jQuery('.linkallservices').prop('checked',false);
	}
	
	jQuery('.loader').show();
	var postdata = { service_id:service_id,
					 staff_id:staff_id,
					 staff_action:staff_action						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});




/* On Select Day Off Show/Hide Start/End Time Dropdowns */
jQuery(document).on('change','.staff_dayoff',function(){
	var wd_id = jQuery(this).data('mainid');
	if(jQuery(this).is(':checked')){
		jQuery('#staff_st_et_'+wd_id).show("blind", {direction: "vertical"}, 1000 );
	}else{
		jQuery('#staff_st_et_'+wd_id).hide("blind", {direction: "vertical"}, 500 );
	}
});

/* Update Staff Availability Schedule*/	
jQuery(document).on('click','.update_staff_schedule',function(){
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).attr('id');
		var dayschdeule = [];
		var staff_schedule_type = jQuery(this).data('st');
		if(staff_schedule_type=='W'){
			for(d=1;d<=7;d++){
				var day_starttime = jQuery('#start_time_1_'+d).val();
				var day_endtime = jQuery('#end_time_1_'+d).val();
							
				if(jQuery('#off_day_1_'+d).is(':checked')){var off_day = 'N';}else{var off_day = 'Y';}
				var dayinfo = day_starttime+'##'+day_endtime+'##'+off_day;
				dayschdeule.push(dayinfo);
			}
		}else{
			for(w=1;w<=5;w++){
				for(d=1;d<=7;d++){
					var day_starttime = jQuery('#start_time_'+w+'_'+d).val();;
					var day_endtime = jQuery('#end_time_'+w+'_'+d).val();;
					if(jQuery('#off_day_'+w+'_'+d).is(':checked')){var off_day = 'N';}else{var off_day = 'Y';}
					var dayinfo = day_starttime+'##'+day_endtime+'##'+off_day;
					dayschdeule.push(dayinfo);
				}
			}	
		}
		
		
		
		var staff_schedule_type = jQuery(this).data('st');		
		
		jQuery('.loader').show();
		
		 var postdata = { staff_id:staff_id,
						  staff_schedule_type:staff_schedule_type,
						  dayschdeule:dayschdeule,
						  staff_action:'update_staff_schedule'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						//jQuery('#mp-staff-sortable').html(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  

/* Staff Add Schedule Break */	
jQuery(document).on('click','.staff_add_break',function(){
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).data('staff_id');
		var weekid = jQuery(this).data('weekid');
		var dayid = jQuery(this).data('dayid');
		var postdata = { staff_id:staff_id,
						 weekid:weekid,
						 dayid:dayid,
						 staff_action:'staff_add_break'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('#mp_staff_breaks_'+weekid+'_'+dayid).append(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 
/* Staff Update Schedule Break */	
jQuery(document).on('change','.staff_schedule_break',function(){
		var ajax_url = header_object.plugin_path;
		var break_id = jQuery(this).data('bid');
		var break_ut = jQuery(this).data('bv');
		var break_start = jQuery('#staff_breakstart_'+break_id).val();
		var break_end = jQuery('#staff_breakend_'+break_id).val();
		if(break_ut=='start'){
			var postdata = { 
						 break_start:break_start,
						 staff_action:'staff_get_break_end'						 
			}
			jQuery.ajax({					
						url  : ajax_url+"/assets/lib/staff_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
						
							jQuery('[name="staff_breakend_'+break_id+'"] option').each(function() { 
								options = new Date('2016/01/07 '+jQuery(this).val());
								edoptions = new Date('2016/01/07 '+response);
								jQuery(this).show();
								if(options.getTime()< edoptions.getTime()){jQuery(this).hide();}
								if(jQuery(this).val() == response){ jQuery(this).attr('selected', 'selected');} 
							});
							
							jQuery('#staff_breakend_'+break_id).selectpicker('refresh');
							jQuery('#staff_breakend_'+break_id).selectpicker('val',response);
							var postdata = { break_id:break_id,
									 break_start:break_start,
									 break_end:response,
									 staff_action:'staff_update_break'						 
									}
							jQuery.ajax({					
								url  : ajax_url+"/assets/lib/staff_ajax.php",					
								type : 'POST',					
								data : postdata,					
								dataType : 'html',					
								success  : function(response) {
									jQuery('.mainheader_message_inner').show();
									moveto_hide_success_msg();
								}
								});	
							}
						});
		
		}else{
		
			var postdata = { break_id:break_id,
							 break_start:break_start,
							 break_end:break_end,
							 staff_action:'staff_update_break'						 
								}
			jQuery.ajax({					
				url  : ajax_url+"/assets/lib/staff_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
				});	
			
		
		
		
		}
		
	
}); 	

/*Delete Staff Break */
jQuery(document).on('click','.delete_staff_break',function(){
		var ajax_url = header_object.plugin_path;
		var break_id = jQuery(this).attr('id');
		jQuery('.loader').show();
		var postdata = { break_id:break_id,
						 staff_action:'staff_delete_break'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('#staff_break_'+break_id).fadeOut('slow');
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 

/* Staff Add Offtime  */
jQuery(document).on('click','.add_staff_offtime',function(){
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).data('sid');
		var offtime_start=jQuery('#offtime-daterange').data('daterangepicker').startDate.format(date_time_format_for_js);
		var offtime_end=jQuery('#offtime-daterange').data('daterangepicker').endDate.format(date_time_format_for_js);
		jQuery('.loader').show();
		var postdata = { staff_id:staff_id,
						 offtime_start:offtime_start,
						 offtime_end:offtime_end,
						 staff_action:'add_staff_offtime'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {							
						var otpostdata = { staff_id:staff_id,
						 staff_action:'refresh_staff_offtimes'						 
						}
						jQuery.ajax({					
							url  : ajax_url+"/assets/lib/staff_ajax.php",					
							type : 'POST',					
							data : otpostdata,					
							dataType : 'html',					
							success  : function(otresponse) {
								jQuery('.loader').hide();
								jQuery('.mp-staff-member-offtime-list-main').html(otresponse);
								jQuery('.mainheader_message_inner').show();
								moveto_hide_success_msg();
							},
							error: function (xhr, ajaxOptions, thrownError) {
							}
						});						
					},
					error: function (xhr, ajaxOptions, thrownError) {
				}
		});
});
/* Staff Delete Offtime */
jQuery(document).on('click','.delete_staff_offtime',function(){
		var ajax_url = header_object.plugin_path;
		var staffid = jQuery(this).data('staffid');
		var offtime_id = jQuery(this).data('otid');
		jQuery('.loader').show();
		var postdata = { offtime_id:offtime_id,
						 staff_action:'delete_staff_offtime'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						var otpostdata = { staff_id:staffid,
						 staff_action:'refresh_staff_offtimes'						 
						}
						jQuery.ajax({					
							url  : ajax_url+"/assets/lib/staff_ajax.php",					
							type : 'POST',					
							data : otpostdata,					
							dataType : 'html',					
							success  : function(otresponse) {
								jQuery('.loader').hide();
								jQuery('.mp-staff-member-offtime-list-main').html(otresponse);
								jQuery('.mainheader_message_inner').show();
								moveto_hide_success_msg();
							},
							error: function (xhr, ajaxOptions, thrownError) {
							}
						});					
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});

/* Staff Offdays Full Month Off/On */
jQuery(document).on("click",".fullmonthoff",function(){
		var off_year_month = jQuery(this).attr('id');	
		var staff_id = jQuery('#staff_offdays_id').val();
		var ajaxurl = header_object.plugin_path;
		var postdata = {
				staff_id:staff_id,
				off_year_month:off_year_month,
				staff_action:'staff_add_offdays'
		}
		if(jQuery(this).is(':checked')) {
			
			jQuery(".selmonth_"+off_year_month+" td.RR").addClass("selectedDate");
				
				jQuery.ajax({
						url  : ajaxurl+"/assets/lib/availability_ajax.php",
						type : 'POST',
						data : postdata,
						dataType : 'html',
						success  : function(response) {
							jQuery('.mainheader_message_inner').show();
							moveto_hide_success_msg();
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
				});
			
		}else {
			jQuery(".selmonth_"+off_year_month+" td.RR").removeClass("selectedDate");
			jQuery(".selmonth_"+off_year_month+" td.RR").toggleClass("date_single");
			var postdata = {
				staff_id:staff_id,
				off_year_month:off_year_month,
				staff_action:'staff_delete_offdays'
			}
			jQuery.ajax({
						url  : ajaxurl+"/assets/lib/availability_ajax.php",
						type : 'POST',
						data : postdata,
						dataType : 'html',
						success  : function(response) {
							jQuery('.mainheader_message_inner').show();
							moveto_hide_success_msg();
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
				});
		}
		
});
/* Add/Remove Offdays jQuery */
 jQuery(document).ready(function(){
	jQuery(document).on("click",".dateline td.RR",function(){
	  var off_date = jQuery(this).attr('id');
      jQuery(this).toggleClass("selectedDate");
	  var str = jQuery(this).attr('class');
	  if (str.toLowerCase().indexOf("selecteddate") >= 0) {
				var staff_id = jQuery('#staff_offdays_id').val();
				var ajaxurl = header_object.plugin_path;
				var postdata = {
				staff_id:staff_id,
				off_date:off_date,
				staff_action:'staff_add_offdays'
				}

				jQuery.ajax({
						url  : ajaxurl+"/assets/lib/availability_ajax.php",
						type : 'POST',
						data : postdata,
						dataType : 'html',
						success  : function(response) {
							jQuery('.mainheader_message_inner').show();
							moveto_hide_success_msg();
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
				});
	  }else{
				var staff_id = jQuery('#staff_offdays_id').val();
				var ajaxurl = header_object.plugin_path;
				
				var postdata = {
				staff_id:staff_id,
				off_date:off_date,
				staff_action:'staff_delete_offdays'
				}

				jQuery.ajax({
						url  : ajaxurl+"/assets/lib/availability_ajax.php",
						type : 'POST',
						data : postdata,
						dataType : 'html',
						success  : function(response) {
							jQuery('.mainheader_message_inner').show();
							moveto_hide_success_msg();
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
				});
	  } 
});	
jQuery('.selectedDate').click(function(){
	   jQuery(this).toggleClass("date_single");
	});
});
/* Onchange Staff Schedule Start Time */
/* Staff Service Schedule EndTime */
jQuery(document).on('change','.schedule_day_start_time',function(){
		if(jQuery(this).hasClass('selectpicker')){
		var ajax_url = header_object.plugin_path;
		var schedule_id = jQuery(this).data('mainid').split('_');
		var schedule_starttime = jQuery(this).val();
		
			var postdata = { 
					schedule_starttime:schedule_starttime,
					staff_action:'staff_get_schedule_end'						 
			}
			 jQuery.ajax({					
						url  : ajax_url+"/assets/lib/staff_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
							jQuery('[name="end_time_['+schedule_id[0]+']_['+schedule_id[1]+'"] option').each(function() { 
									schedule_options = new Date('2016/01/07 '+jQuery(this).val());
									schedule_edoptions = new Date('2016/01/07 '+response);
									jQuery(this).show();
									
									if(schedule_options.getTime()< schedule_edoptions.getTime()){jQuery(this).hide();}
									if(jQuery(this).val() == response){ jQuery(this).attr('selected', 'selected');} 
								});
							
							jQuery('#end_time_'+schedule_id[0]+'_'+schedule_id[1]).selectpicker('refresh');
							jQuery('#end_time_'+schedule_id[0]+'_'+schedule_id[1]).selectpicker('val',response);
						}
			});
		}
}); 
/* Delete Staff Member */
jQuery(document).on('click','#delete_staff',function(){
		var ajax_url = header_object.plugin_path;
		var staff_id = jQuery(this).data('staff_id');
		jQuery('.loader').show();
		var postdata = { staff_id:staff_id,
						 staff_action:'delete_staff_member'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/staff_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();						
						//jQuery('#offtime_detail_'+offtime_id).fadeOut('slow');
						jQuery('.mp-staff-container').html(response);
						jQuery('#mp-staff-sortable li').first().addClass("active");
						jQuery('#mp-staff-sortable li').first().trigger("click");
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						window.location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});	
	



							/***********************Staff Jquery End Here **********************/	
						
							/*********************** General Admin Ajax Jquery **********************/

/*Set Selected Location Session */
jQuery(document).on('click','.mp_selected_location',function(){
		var ajax_url = header_object.plugin_path;
		var location_id = jQuery('select[name="mp_selected_location"]').val();
		jQuery('.loader').show();
		var postdata = { location_id:location_id,
						 general_ajax_action:'set_location_session'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						location.reload();
						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 


/** Delete service popover **/
jQuery(document).bind('ready ajaxComplete', function(){
		jQuery('.mp_remove_image').popover({ 
				html : true,
				content: function() {				
				  return jQuery('#popover-'+jQuery(this).attr('id')).html();
				}
		});
});
/** Hide delete service popover **/
jQuery(document).on('click', '.close_delete_popup', function(){			
	jQuery('.popover').fadeOut();
});	
	
	

/*** Common Code For Remove Image **/
jQuery(document).on('click','.mp_delete_image',function(){
		var ajax_url = header_object.plugin_path;
		var site_url = header_object.site_url;
		var defaultmedia = header_object.defaultmedia;		
		var mediaid = jQuery(this).data('mediaid');
		var mediapath = jQuery(this).data('mediapath');
		var mediasection = jQuery(this).data('mediasection');
		var defaultmedia_fullpath = defaultmedia+mediasection+'.png';
		var imagefieldid = jQuery(this).data('imgfieldid');
				
		jQuery('.loader').show();
		jQuery('.close_delete_popup').trigger('click');
		var postdata = { mediaid:mediaid,
						 mediapath:mediapath,
						 action:'delete_image' }
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/"+mediasection+"_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {						
						jQuery('.loader').hide();
						jQuery('#'+imagefieldid).val('');
						jQuery('#bdls'+mediaid+'locimage').attr('src',defaultmedia_fullpath);
						jQuery('#bdll'+mediaid+'locimage').attr('src',defaultmedia_fullpath);
						jQuery('#bdsdu'+mediaid+'locimage').attr('src',defaultmedia_fullpath);
						/* jQuery('.mp-'+mediasection+'-image').attr('src',defaultmedia_fullpath); */						
						jQuery('.mp_remove_image').hide();
						jQuery('#mp-remove-service-imagebdls'+mediaid).hide();
						jQuery('#staff_detail_'+mediaid+' img').attr('src',ajax_url+'/assets/images/'+mediasection+'.png');
						
						jQuery('#bdscad'+mediaid+'addimage').attr('src',ajax_url+'/assets/images/addon.png');
						jQuery('#bdscad'+mediaid+'uploadedimg').val('');
						jQuery('.show_image_icon_add'+mediaid).css('display','block');
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
}); 

/************************************ Admin Panel Validations ********************************/
	/********************** Location Form Validations ****************************/
jQuery.validator.addMethod('customemail', function (value, element) {
			return this.optional(element) || /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/.test(value);
											
});	
	
	/* Create Location Validate */
//jQuery(document).ready(function(){
jQuery(document).bind('ready ajaxComplete', function(){
	var ajax_url = header_object.plugin_path;
	var locationtiitle_err_msg = admin_validation_err_msg.locationtiitle_err_msg;	
	var locationemail_err_msg = admin_validation_err_msg.locationemail_err_msg;	
	var locationinvalidemail_err_msg = admin_validation_err_msg.locationinvalidemail_err_msg;
	var locationphone_err_msg = admin_validation_err_msg.locationphone_err_msg;
	var locationvalidphone_err_msg = admin_validation_err_msg.locationvalidphone_err_msg;
	var locationinvalidphone_err_msg = admin_validation_err_msg.locationinvalidphone_err_msg;
	var locationaddress_err_msg = admin_validation_err_msg.locationaddress_err_msg;
	var locationcity_err_msg = admin_validation_err_msg.locationcity_err_msg;
	var locationstate_err_msg = admin_validation_err_msg.locationstate_err_msg;
	var locationzip_err_msg = admin_validation_err_msg.locationzip_err_msg;
	var locationcountry_err_msg = admin_validation_err_msg.locationcountry_err_msg;
	
	jQuery.validator.addMethod("numeric_pattern", function(value, element) {
	return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/.test(value);
	}, locationvalidphone_err_msg);
	
	
	jQuery('#mp_create_location_cl').validate({
					rules: {
						location_title: {
							required: true,
							remote: {
									url  : ajax_url+"/assets/lib/service_ajax.php",
									type: "POST",
									async: true,
									data: {
									title:function() {
										 return jQuery('#mp-location-name').val();
										},
										action:"check_location_title"
									}
								}
							},
						email: {
													required: true
							},
						phone: {
													required: true,
													numeric_pattern:true,
							},
						address: {
													required: true
							},
						city: {
													required: true
							},	
						state: {
													required: true
							},
						zip: {
													required: true
							},
						country: {
													required: true
							},						
						},
					messages: {
								location_title: { required:locationtiitle_err_msg , remote: "Location Title Already exist!!!" }, 
								email: { required:locationemail_err_msg }, 
								phone: { required:locationphone_err_msg,numeric_pattern:locationvalidphone_err_msg},
								address: { required:locationaddress_err_msg }, 
								city: { required:locationcity_err_msg }, 
								state: { required:locationstate_err_msg }, 
								zip: { required:locationzip_err_msg }, 
								country: { required:locationcountry_err_msg } 
								
						}
				});
});

/* Update Location Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var locationtiitle_err_msg = admin_validation_err_msg.locationtiitle_err_msg;	
	var locationemail_err_msg = admin_validation_err_msg.locationemail_err_msg;	
	var locationinvalidemail_err_msg = admin_validation_err_msg.locationinvalidemail_err_msg;
	var locationphone_err_msg = admin_validation_err_msg.locationphone_err_msg;
	var locationvalidphone_err_msg = admin_validation_err_msg.locationvalidphone_err_msg;
	var locationinvalidphone_err_msg = admin_validation_err_msg.locationinvalidphone_err_msg;
	var locationaddress_err_msg = admin_validation_err_msg.locationaddress_err_msg;
	var locationcity_err_msg = admin_validation_err_msg.locationcity_err_msg;
	var locationstate_err_msg = admin_validation_err_msg.locationstate_err_msg;
	var locationzip_err_msg = admin_validation_err_msg.locationzip_err_msg;
	var locationcountry_err_msg = admin_validation_err_msg.locationcountry_err_msg;
	jQuery.validator.addMethod("numeric_pattern", function(value, element) {
	return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/.test(value);
	}, locationvalidphone_err_msg);

		jQuery('.mp_update_location').each(function(){
				jQuery(this).validate({
					rules: {
						location_title: {
													required: true
							},
						email: {
													required: true
							},
						phone: {
													required: true,
													numeric_pattern:true,
							},
						address: {
													required: true
							},
						city: {
													required: true
							},	
						state: {
													required: true
							},
						zip: {
													required: true
							},
						country: {
													required: true
							},						
						},
					messages: {
								location_title: { required:locationtiitle_err_msg }, 
								email: { required:locationemail_err_msg }, 
								phone: { required:locationphone_err_msg,numeric_pattern:locationvalidphone_err_msg},
								address: { required:locationaddress_err_msg }, 
								city: { required:locationcity_err_msg }, 
								state: { required:locationstate_err_msg }, 
								zip: { required:locationzip_err_msg }, 
								country: { required:locationcountry_err_msg } 
								
						}
				});
		});			
});


	
	
	
	/********************** Service Form Validations ****************************/
jQuery(document).bind('ready ajaxComplete', function(){
	var ajax_url = header_object.plugin_path;	
	var servicetitle_err_msg = admin_validation_err_msg.servicetitle_err_msg;	
	var servicedescription_err_msg = admin_validation_err_msg.servicedescription_err_msg;	
	var serviceprice_err_msg = admin_validation_err_msg.serviceprice_err_msg;
	var servicepricedigit_err_msg = admin_validation_err_msg.servicepricedigit_err_msg;
	var servicecategory_err_msg = admin_validation_err_msg.servicecategory_err_msg;
	var servicehrsrange_err_msg = admin_validation_err_msg.servicehrsrange_err_msg;
	var serviceminsrange_err_msg = admin_validation_err_msg.serviceminsrange_err_msg;
	var servicemins_err_msg = admin_validation_err_msg.servicemins_err_msg;
	var servicenumpatt_err_msg = admin_validation_err_msg.servicenumpatt_err_msg;
	
	jQuery.validator.addMethod("numeric_pattern", function(value, element) {
	return this.optional(element) || /^[0-9]\d*(\.\d+)?$/.test(value);
	}, "Enter Only Alphabets");
	
	jQuery('#mp_create_service').validate({
		rules: {
			service_title: {
										required: true,
										remote: {
											url: ajax_url+"/assets/lib/service_ajax.php",
											type: "POST",
											async: true,
											data: {
											title:function() {
												return jQuery('#mp-service-title').val();
												},		
												action:'check_service_title'
											}
										},
				},
			service_description: {
										required: true
				},
			service_price: {
										required: true,
										number:true
				},	
		    offered_price: {
										number:true
				},
			service_category: {
										required:true
				},	
				
		   service_duration_hrs:{		numeric_pattern:true,
										range:function(element){ 
													 if (parseInt(jQuery("#service_duration_mins").val()) > 0){return [0,23];
													 }else {return [0,24];}
												}
										},	
		   service_duration_mins:{		required:function(element){
													if (parseInt(jQuery("#service_duration_hrs").val()) > 0){return false;
													}else { return true;
													}
										},			
										numeric_pattern:true,
										range:function(element){
												if (parseInt(jQuery("#service_duration_hrs").val()) > 0){return [0,59];}else {return [5,59];}
												}
										}					
	
			
			},
		messages: {
					service_title: { required:servicetitle_err_msg , remote : "Service Title Already exist!!!" }, 
					service_description: { required:servicedescription_err_msg }, 
					service_price: { required:serviceprice_err_msg , number:servicepricedigit_err_msg},
					offered_price: { number:servicepricedigit_err_msg},
					service_category: {required:servicecategory_err_msg},
					service_duration_hrs: {numeric_pattern:servicenumpatt_err_msg,range:servicehrsrange_err_msg},
					service_duration_mins:{required:servicemins_err_msg,numeric_pattern:servicenumpatt_err_msg,range:serviceminsrange_err_msg}
			}
	});
});			
/* Update Service Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var servicetitle_err_msg = admin_validation_err_msg.servicetitle_err_msg;	
	var servicedescription_err_msg = admin_validation_err_msg.servicedescription_err_msg;	
	var serviceprice_err_msg = admin_validation_err_msg.serviceprice_err_msg;
	var servicepricedigit_err_msg = admin_validation_err_msg.servicepricedigit_err_msg;
	var servicecategory_err_msg = admin_validation_err_msg.servicecategory_err_msg;
	var servicehrsrange_err_msg = admin_validation_err_msg.servicehrsrange_err_msg;
	var serviceminsrange_err_msg = admin_validation_err_msg.serviceminsrange_err_msg;
	var servicemins_err_msg = admin_validation_err_msg.servicemins_err_msg;
	var servicenumpatt_err_msg = admin_validation_err_msg.servicenumpatt_err_msg;
	
		jQuery('.mp_update_service').each(function(){
		var service_id = jQuery(this).data('sid');
				jQuery(this).validate({
					rules: {
						u_service_title: {
													required: true
							},
						u_service_desc: {
													required: true
							},
						u_service_price: {
													required: true,
													number:true
							},
						u_service_offeredprice: {
													number:true
						},	
						service_category: {
													required:true
							},	
						u_duration_hrs:{		numeric_pattern:true,
													range:function(element){ 
																 if (parseInt(jQuery('#mp-duration-mins'+service_id).val()) > 0){return [0,23];
																 }else {return [0,24];}
															}
													},	
						u_duration_mins:{		required:function(element){
															if (parseInt(jQuery('#mp-duration-hrs'+service_id).val()) > 0){return false;
															}else { return true;
															}
													},			
													numeric_pattern:true,
													range:function(element){
															if (parseInt(jQuery('#mp-duration-hrs'+service_id).val()) > 0){return [0,59];}else {return [5,59];}
													}
										}	
						},
					messages: {
								u_service_title: { required:servicetitle_err_msg }, 
								u_service_desc: { required:servicedescription_err_msg }, 
								u_service_price: { required:serviceprice_err_msg , number:servicepricedigit_err_msg},
								u_service_offeredprice: { number:servicepricedigit_err_msg},
								service_category: {required:servicecategory_err_msg},
								u_duration_hrs: {numeric_pattern:servicenumpatt_err_msg,range:servicehrsrange_err_msg},
								u_duration_mins:{required:servicemins_err_msg,numeric_pattern:servicenumpatt_err_msg,range:serviceminsrange_err_msg}
						}
				});
		});			
});

					/*********************** Settings Jquery **********************/
/****************************************************************************************************************
/* Partial Deposit Enable/Disable According To Payment Methods*/ 


 /* multilocation  #### */
 jQuery(document).ready(function(){
    jQuery("input:checkbox[name=ck]").change(function(){
	if(jQuery('#moveto_multi_location').prop('checked')==true){ var moveto_multi_location ='E';}
	else{
	var moveto_multi_location='D'
	jQuery('.ml-popup').addClass('is-visible');
	alert("Are you sure you want to disable it, This option will remove all the services during multiple locations setup.");
	;}
	
	});
});

/* Update/Save General Settings */
jQuery(document).on('click','#mp_save_general_settings',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;	
        var multilocation_st = header_object.multilocation_st;		
		if(jQuery('#moveto_taxvat_status').prop('checked')==true){ var moveto_taxvat_status ='E';}else{ var moveto_taxvat_status='D';}
		if(jQuery('#moveto_partial_deposit_status').prop('checked')==true){ var moveto_partial_deposit_status ='E';}else{ var moveto_partial_deposit_status='D';}
		if(jQuery('#moveto_multiple_booking_sameslot').prop('checked')==true){ var moveto_multiple_booking_sameslot ='E';}else{ var moveto_multiple_booking_sameslot='D';}
		if(jQuery('#moveto_appointment_auto_confirm').prop('checked')==true){ var moveto_appointment_auto_confirm ='E';}else{ var moveto_appointment_auto_confirm='D';}
		if(jQuery('#moveto_dayclosing_overlap').prop('checked')==true){ var moveto_dayclosing_overlap ='E';}else{ var moveto_dayclosing_overlap='D';}
		if(jQuery('#moveto_multi_location').prop('checked')==true){ var moveto_multi_location ='E';}else{ var moveto_multi_location='D';}
		
		if(jQuery('#booking_cart_description').prop('checked')==true){ var booking_cart_description ='E';}else{ var booking_cart_description='D';}
	
		if(jQuery('#moveto_cancelation_policy_status').prop('checked')==true){ var moveto_cancelation_policy_status ='E';}else{ var moveto_cancelation_policy_status='D';}
		
		if(jQuery('#moveto_allow_terms_and_conditions').prop('checked')==true){ var moveto_allow_terms_and_conditions ='E';}else{ var moveto_allow_terms_and_conditions='D';}
	
		if(jQuery('#move_pricing_status').prop('checked')==true){ var move_pricing_status ='Y';}else{ var move_pricing_status='N';}
		//alert(move_pricing_status);
		if(jQuery('#moveto_allow_privacy_policy').prop('checked')==true){ var moveto_allow_privacy_policy ='E';}else{ var moveto_allow_privacy_policy='D';}
			
		if(jQuery('#moveto_zipcode_booking').prop('checked')==true){ 
			var moveto_zipcode_booking ='E';
			var moveto_booking_zipcodes_val = jQuery('#moveto_booking_zipcodes').val();
		}else{ 
			var moveto_zipcode_booking='D';
			var moveto_booking_zipcodes_val = jQuery('#moveto_booking_zipcodes_hidd').val();
		}

		selected_value = jQuery("input[name='disc_type']:checked").val();
		var postdata = { moveto_booking_time_interval:jQuery('#moveto_booking_time_interval').val(),
						 moveto_multi_location:moveto_multi_location,
						 moveto_zipcode_booking:moveto_zipcode_booking,
						 moveto_booking_zipcodes:moveto_booking_zipcodes_val,
						 moveto_minimum_advance_booking:jQuery('#moveto_minimum_advance_booking').val(),
						 moveto_maximum_advance_booking:jQuery('#moveto_maximum_advance_booking').val(),
						 moveto_booking_padding_time:jQuery('#moveto_booking_padding_time').val(),
						 moveto_cancellation_buffer_time:jQuery('#moveto_cancellation_buffer_time').val(),
						 moveto_reschedule_buffer_time:jQuery('#moveto_reschedule_buffer_time').val(),
						 moveto_currency:jQuery('#moveto_currency').val(),
						 moveto_currency_symbol_position:jQuery('#moveto_currency_symbol_position').val(),
						 moveto_price_format_decimal_places:jQuery('#moveto_price_format_decimal_places').val(),
						 moveto_price_format_comma_separator:jQuery('#moveto_price_format_comma_separator').val(),
						 moveto_location_sortby:jQuery('#moveto_location_sortby').val(),
						 moveto_taxvat_status:moveto_taxvat_status,
						 moveto_taxvat_amount:jQuery('#moveto_taxvat_amount').val(),
						 moveto_taxvat_type:jQuery('input[name="moveto_taxvat_type"]:checked').val(),
						 moveto_partial_deposit_status:moveto_partial_deposit_status,
						 moveto_partial_deposit_type:jQuery('input[name="moveto_partial_deposit_type"]:checked').val(),
						 moveto_partial_deposit_amount:jQuery('#moveto_partial_deposit_amount').val(),
						 moveto_partial_deposit_message:jQuery('#moveto_partial_deposit_message').val(),
						 moveto_thankyou_page:jQuery('#moveto_thankyou_page').val(),
						 moveto_thankyou_page_rdtime:jQuery('#moveto_thankyou_page_rdtime').val(),
						 moveto_multiple_booking_sameslot:moveto_multiple_booking_sameslot,
						 moveto_slot_max_booking_limit:jQuery('#moveto_slot_max_booking_limit').val(),
						 moveto_appointment_auto_confirm:moveto_appointment_auto_confirm,
						 moveto_dayclosing_overlap:moveto_dayclosing_overlap,
						 booking_cart_description:booking_cart_description,
						 moveto_datepicker_format:jQuery('#moveto_datepicker_format').val(),
						 
						 moveto_cancelation_policy_status:moveto_cancelation_policy_status,
						 moveto_cancelation_policy_header:jQuery('#moveto_cancelation_policy_header').val(),	
						 moveto_cancelation_policy_text:jQuery('#moveto_cancelation_policy_text').val(),
						 
												 
						 moveto_allow_terms_and_conditions:moveto_allow_terms_and_conditions,
						 moveto_allow_terms_and_conditions_url:jQuery('#moveto_allow_terms_and_conditions_url').val(),
						
						
						 move_pricing_status:move_pricing_status,
						 
						 
						
					
						
						 moveto_allow_privacy_policy:moveto_allow_privacy_policy,
						 moveto_allow_privacy_policy_url:jQuery('#moveto_allow_privacy_policy_url').val(),
						 moveto_thankyou_page_message:jQuery('#moveto_thankyou_page_message').val(),
						 moveto_max_distance:jQuery('#moveto_max_distance').val(),
						 moveto_tax:jQuery('#moveto_tax').val(),
						 moveto_discount_type:selected_value,
						 moveto_discount:jQuery('#moveto_discount').val(),

						 setting_action:'update_settings'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
		
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						if(multilocation_st!=moveto_multi_location){
							window.location.reload();
						}	
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	
/* Zipcode Wise Booking Enable/Disable */
jQuery(document).on('change','#moveto_zipcode_booking',function(){
	jQuery('#moveto_booking_zipcodesetting').toggle('slow');
	if(jQuery(this).prop('checked')==true){
		if(jQuery('#moveto_multi_location').prop('checked')==true){
			jQuery('#moveto_multi_location').attr('checked',false);			    
			jQuery('#moveto_multi_location').parent().prop('className','toggle btn btn-default off');	
		}	
	}
	
});
jQuery(document).on('change','#moveto_multi_location',function(){
	if(jQuery(this).prop('checked')==true){
		if(jQuery('#moveto_zipcode_booking').prop('checked')==true){
			jQuery('#moveto_booking_zipcodesetting').toggle('slow');
			jQuery('#moveto_zipcode_booking').attr('checked',false);			    
			jQuery('#moveto_zipcode_booking').parent().prop('className','toggle btn btn-default off');	
		}	
	}
	
});
	
/* Update/Save Company Settings */
jQuery(document).on('click','#mp_save_company_settings',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;	
		var postdata = { moveto_company_name:jQuery('#moveto_company_name').val(),				
						 moveto_company_email:jQuery('#moveto_company_email').val(),
						 moveto_company_address:jQuery('#moveto_company_address').val(),
						 moveto_company_city:jQuery('#moveto_company_city').val(),
						 moveto_company_state:jQuery('#moveto_company_state').val(),
						 moveto_company_zip:jQuery('#moveto_company_zip').val(),
						 moveto_company_country:jQuery('#moveto_company_country').val(),
						 moveto_company_logo:jQuery('#bdcsuploadedimg').val(),
						 moveto_company_country_code:jQuery('#moveto_company_country_code').val(),
						 moveto_company_phone:jQuery('#moveto_company_phone').val(),
						 default_company_country_flag:jQuery('.default_company_country_flag').val(),
						 
						 setting_action:'update_settings'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	

/** Delete Company Image **/
/*** Common Code For Remove Image **/
jQuery(document).on('click','.mp_delete_companyimage',function(){
		var ajax_url = header_object.plugin_path;
		var site_url = header_object.site_url;
		var defaultmedia = header_object.defaultmedia;		
		var mediapath = jQuery(this).data('mediapath');
		var defaultmedia_fullpath = defaultmedia+'company.png';
		jQuery('.loader').show();
		jQuery('.close_delete_popup').trigger('click');
		var postdata = { mediapath:mediapath,
						 setting_action:'delete_company_image' }
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {						
						jQuery('.loader').hide();
						jQuery('#bdcsuploadedimg').val('');
						jQuery('#bdcslocimage').attr('src',defaultmedia_fullpath);
						jQuery('.mp_remove_image').hide();	
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});
/* Update/Save Appearance Settings */
jQuery(document).on('click','#mp_save_appearance_settings',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var reviews_st = header_object.reviews_st;		
		if(jQuery('#moveto_show_provider').prop('checked')==true){ var moveto_show_provider ='E';}else{ var moveto_show_provider='D';}
		if(jQuery('#moveto_show_provider_avatars').prop('checked')==true){ var moveto_show_provider_avatars ='E';}else{ var moveto_show_provider_avatars='D';}
		if(jQuery('#moveto_show_services').prop('checked')==true){ var moveto_show_services ='E';}else{ var moveto_show_services='D';}
		if(jQuery('#moveto_show_service_desc').prop('checked')==true){ var moveto_show_service_desc ='E';}else{ var moveto_show_service_desc='D';}
		if(jQuery('#moveto_show_coupons').prop('checked')==true){ var moveto_show_coupons ='E';}else{ var moveto_show_coupons='D';}
		if(jQuery('#moveto_hide_booked_slot').prop('checked')==true){ var moveto_hide_booked_slot ='E';}else{ var moveto_hide_booked_slot='D';}
		if(jQuery('#moveto_guest_user_checkout').prop('checked')==true){ var moveto_guest_user_checkout ='E';}else{ var moveto_guest_user_checkout='D';}
		if(jQuery('#moveto_postalcode').prop('checked')==true){ var moveto_postalcode ='E';}else{ var moveto_postalcode='D';}
		//if(jQuery('#moveto_cabs').prop('checked')==true){ var moveto_cabs ='E';}else{ var moveto_cabs='D';}
		if(jQuery('#moveto_country_flag').prop('checked')==true){ var moveto_country_flag ='E';}else{ var moveto_country_flag='D';}
		if(jQuery('#moveto_cart').prop('checked')==true){ var moveto_cart ='E';}else{ var moveto_cart='D';}
		if(jQuery('#moveto_reviews_status').prop('checked')==true){ var moveto_reviews_status ='E';}else{ var moveto_reviews_status='D';}
		if(jQuery('#moveto_auto_confirm_reviews').prop('checked')==true){ var moveto_auto_confirm_reviews ='E';}else{ var moveto_auto_confirm_reviews='D';}
		var moveto_frontend_custom_css = jQuery('#moveto_frontend_custom_css').val();
		/* var moveto_frontend_custom_css = jQuery('#moveto_frontend_custom_css').val(); */
		var flag_coma_values = "";
		jQuery("#selected_country_code_display :selected").map(function(i, el) {
			
			flag_coma_values += jQuery(el).val() + ",";
		});
		if(flag_coma_values != ""){
			flag_coma_values = flag_coma_values.slice(0,-1);
		}
		var postdata = { moveto_primary_color:jQuery('#moveto_primary_color').val(),
						 moveto_secondary_color:jQuery('#moveto_secondary_color').val(),
						 moveto_text_color:jQuery('#moveto_text_color').val(),
						 moveto_bg_text_color:jQuery('#moveto_bg_text_color').val(),
						 moveto_admin_color_primary:jQuery('#moveto_admin_color_primary').val(),		 
						 moveto_admin_color_secondary:jQuery('#moveto_admin_color_secondary').val(),		 
						 moveto_admin_color_text:jQuery('#moveto_admin_color_text').val(),
						 moveto_admin_color_bg_text:jQuery('#moveto_admin_color_bg_text').val(),		 
						 moveto_show_provider:moveto_show_provider,						 
						 moveto_show_provider_avatars:moveto_show_provider_avatars,		 
						 moveto_show_services:moveto_show_services,						 
						 moveto_show_service_desc:moveto_show_service_desc,				 
						 moveto_show_coupons:moveto_show_coupons,						 
						 moveto_hide_booked_slot:moveto_hide_booked_slot,				 
						 moveto_guest_user_checkout:moveto_guest_user_checkout,		 
						 moveto_postalcode:moveto_postalcode,
						 moveto_country_flag:moveto_country_flag,		 
						 moveto_cart:moveto_cart,						 
						 moveto_max_cartitem_limit:jQuery('#moveto_max_cartitem_limit').val(),
						 moveto_reviews_status:moveto_reviews_status,					 
						 moveto_auto_confirm_reviews:moveto_auto_confirm_reviews,
						 moveto_frontend_custom_css:moveto_frontend_custom_css,	moveto_frontend_loader:jQuery('#moveto_frontend_loader').val(),
						 moveto_country_flags:flag_coma_values,
						 setting_action:'update_settings'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						if(reviews_st!=moveto_reviews_status){window.location.reload();}
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	
/* Update/Save Payment Settings */
jQuery(document).on('click','#mp_save_payment_settings',function(){
		var ajax_url = header_object.plugin_path;		
		if(jQuery('#moveto_payment_gateways_status').prop('checked')==true){
		 var moveto_payment_gateways_status ='E';
		 if(jQuery('#moveto_locally_payment_status').prop('checked')!=true && jQuery('#moveto_payment_method_Paypal').prop('checked')!=true && jQuery('#moveto_payment_method_Stripe').prop('checked')!=true && jQuery('#moveto_payment_method_Paytm').prop('checked')!=true && jQuery('#moveto_payment_method_Paystack').prop('checked')!=true){
			jQuery('#error_payment_method').show();
			jQuery('.error_payment_method_msg').html("Please select atleast one payment method");
		 	return false;
		 }else{
			jQuery('.error_payment_method_msg').html("");
			jQuery('#error_payment_method').hide();
		 }
		}else{ 
		 var moveto_payment_gateways_status='D';
		}
		if(jQuery('#moveto_locally_payment_status').prop('checked')==true){ var moveto_locally_payment_status ='E';}else{ var moveto_locally_payment_status='D';}

		if(jQuery('#moveto_payment_method_Paypal').prop('checked')==true){ 
			if(jQuery('#moveto_paypal_api_username').val() == "" || jQuery('#moveto_paypal_api_password').val() == "" || jQuery('#moveto_paypal_api_signature').val() == ""){				
			jQuery('#error_payment_method').show();
			jQuery('.error_payment_method_msg').html("Please fill required fields");
			return false;
			}else{
			 jQuery('#error_payment_method').show();
			 jQuery('.error_payment_method_msg').html("");
		}

			var moveto_payment_method_Paypal ='E';
		}else{
		 var moveto_payment_method_Paypal='D';
		 jQuery('#error_payment_method').show();
		 jQuery('.error_payment_method_msg').html("");
		}

		if(jQuery('#moveto_paypal_guest_checkout').prop('checked')==true){ var moveto_paypal_guest_checkout ='E';}else{ var moveto_paypal_guest_checkout='D';}

		if(jQuery('#moveto_paypal_testing_mode').prop('checked')==true){ var moveto_paypal_testing_mode ='E';}else{ var moveto_paypal_testing_mode='D';}
		
		if(jQuery('#moveto_payment_method_Stripe').prop('checked')==true){ 
			if(jQuery('#moveto_stripe_secretKey').val() == "" || jQuery('#moveto_stripe_publishableKey').val() == ""){			
			jQuery('#error_payment_method').show();
			jQuery('.error_payment_method_msg').html("Please fill required fields");
			return false;
			}else{
			 jQuery('#error_payment_method').show();
			 jQuery('.error_payment_method_msg').html("");
		}
			var moveto_payment_method_Stripe ='E';	
		}else{
		 var moveto_payment_method_Stripe='D';
		 jQuery('#error_payment_method').show();
		 jQuery('.error_payment_method_msg').html("");
		}
		
		if(jQuery('#moveto_payment_method_Authorizenet').prop('checked')==true){ var moveto_payment_method_Authorizenet ='E';}else{ var moveto_payment_method_Authorizenet='D';}
		
		if(jQuery('#moveto_authorizenet_testing_mode').prop('checked')==true){ var moveto_authorizenet_testing_mode ='E';}else{ var moveto_authorizenet_testing_mode='D';}
		if(jQuery('#moveto_payment_method_2Checkout').prop('checked')==true){ var moveto_payment_method_2Checkout ='E';}else{ var moveto_payment_method_2Checkout='D';}
		if(jQuery('#moveto_2checkout_testing_mode').prop('checked')==true){ var moveto_2checkout_testing_mode ='E';}else{ var moveto_2checkout_testing_mode='D';}
		if(jQuery('#moveto_payment_method_Payumoney').prop('checked')==true){ var moveto_payment_method_Payumoney ='E';}else{ var moveto_payment_method_Payumoney='D';}
		
		if(jQuery('#moveto_payment_method_Paytm').prop('checked')==true){
			if(jQuery('#moveto_paytm_merchantkey').val() == "" || jQuery('#moveto_paytm_website').val() == "" || jQuery('#moveto_paytm_channelid').val() == "" || jQuery('#moveto_paytm_industryid').val() == "" || jQuery('#moveto_paytm_merchantid').val() == ""){			
			jQuery('#error_payment_method').show();
			jQuery('.error_payment_method_msg').html("Please fill required fields");
			return false;
			}else{
			 jQuery('#error_payment_method').show();
			 jQuery('.error_payment_method_msg').html("");
		}
		 var moveto_payment_method_Paytm ='E';
		}else{
			 jQuery('#error_payment_method').show();
			 jQuery('.error_payment_method_msg').html("");
		 var moveto_payment_method_Paytm='D';
		}
		
		if(jQuery('#moveto_payment_method_Paystack').prop('checked')==true){ 
			if(jQuery('#moveto_paystack_public_key').val() == "" || jQuery('#moveto_paystack_secret_key').val() == ""){			
					jQuery('#error_payment_method').show();
					jQuery('.error_payment_method_msg').html("Please fill required fields");
					return false;
			}else{
				 jQuery('#error_payment_method').show();
				 jQuery('.error_payment_method_msg').html("");
			}
				var moveto_payment_method_Paystack ='E';	
		}else{
			
			 jQuery('#error_payment_method').show();
			 jQuery('.error_payment_method_msg').html("");
			  var moveto_payment_method_Paystack='D';
		}
		
		if(jQuery('#moveto_paytm_testing_mode').prop('checked')==true){ var moveto_paytm_testing_mode ='E';}else{ var moveto_paytm_testing_mode='D';}
		
		jQuery('.loader').show();
		
		var postdata = { moveto_payment_gateways_status:moveto_payment_gateways_status,
						 moveto_locally_payment_status:moveto_locally_payment_status,
						 moveto_payment_method_Paypal:moveto_payment_method_Paypal,
						 moveto_paypal_guest_checkout:moveto_paypal_guest_checkout,
						 moveto_paypal_testing_mode:moveto_paypal_testing_mode,
						 moveto_payment_method_Stripe:moveto_payment_method_Stripe,
						 moveto_payment_method_Paystack:moveto_payment_method_Paystack,
						 moveto_payment_method_Authorizenet:moveto_payment_method_Authorizenet,
						 moveto_authorizenet_testing_mode:moveto_authorizenet_testing_mode,
						 moveto_payment_method_2Checkout:moveto_payment_method_2Checkout,
						 moveto_payment_method_Payumoney:moveto_payment_method_Payumoney,
						 moveto_payment_method_Paytm:moveto_payment_method_Paytm,
						 moveto_paytm_testing_mode:moveto_paytm_testing_mode,
						 moveto_2checkout_testing_mode:moveto_2checkout_testing_mode,
						 moveto_paypal_api_username:jQuery('#moveto_paypal_api_username').val(),
						 moveto_paypal_api_password:jQuery('#moveto_paypal_api_password').val(),
						 moveto_paypal_api_signature:jQuery('#moveto_paypal_api_signature').val(),
						 moveto_stripe_secretKey:jQuery('#moveto_stripe_secretKey').val(),
						 moveto_stripe_publishableKey:jQuery('#moveto_stripe_publishableKey').val(),
						 moveto_paystack_public_key:jQuery('#moveto_paystack_public_key').val(),
						 moveto_paystack_secret_key:jQuery('#moveto_paystack_secret_key').val(),
						 moveto_authorizenet_api_loginid:jQuery('#moveto_authorizenet_api_loginid').val(),
						 moveto_authorizenet_transaction_key:jQuery('#moveto_authorizenet_transaction_key').val(),
						 moveto_authorizenet_transaction_key:jQuery('#moveto_authorizenet_transaction_key').val(),
						 moveto_2checkout_publishablekey:jQuery('#moveto_2checkout_publishablekey').val(),
						 moveto_2checkout_privateKey:jQuery('#moveto_2checkout_privateKey').val(),
						 moveto_2checkout_sellerid:jQuery('#moveto_2checkout_sellerid').val(),
						 moveto_payumoney_merchantkey:jQuery('#moveto_payumoney_merchantkey').val(),
						 moveto_payumoney_saltkey:jQuery('#moveto_payumoney_saltkey').val(),
						 moveto_paytm_merchantkey:jQuery('#moveto_paytm_merchantkey').val(),
						 moveto_paytm_merchantid:jQuery('#moveto_paytm_merchantid').val(),
						 moveto_paytm_website:jQuery('#moveto_paytm_website').val(),
						 moveto_paytm_channelid:jQuery('#moveto_paytm_channelid').val(),
						 moveto_paytm_industryid:jQuery('#moveto_paytm_industryid').val(),
						 setting_action:'update_settings'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();	
						window.location.reload();
												
					},

					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	
/* Update/Save Email Settings */
jQuery(document).on('click','#mp_save_email_settings',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		if(jQuery('#moveto_admin_email_notification_status').prop('checked')==true){ var moveto_admin_email_notification_status ='E';}else{ var moveto_admin_email_notification_status='D';}
		if(jQuery('#moveto_manager_email_notification_status').prop('checked')==true){ var moveto_manager_email_notification_status ='E';}else{ var moveto_manager_email_notification_status='D';}
		if(jQuery('#moveto_service_provider_email_notification_status').prop('checked')==true){ var moveto_service_provider_email_notification_status ='E';}else{ var moveto_service_provider_email_notification_status='D';}
		if(jQuery('#moveto_client_email_notification_status').prop('checked')==true){ var moveto_client_email_notification_status ='E';}else{ var moveto_client_email_notification_status='D';}
						
		var postdata = { moveto_admin_email_notification_status:moveto_admin_email_notification_status,
						 moveto_manager_email_notification_status:moveto_manager_email_notification_status,
						 moveto_service_provider_email_notification_status:moveto_service_provider_email_notification_status,
						 moveto_client_email_notification_status:moveto_client_email_notification_status,
						 moveto_email_sender_address:jQuery('#moveto_email_sender_address').val(),
						 moveto_email_sender_name:jQuery('#moveto_email_sender_name').val(),
						 moveto_email_reminder_buffer:jQuery('#moveto_email_reminder_buffer').val(),
						 setting_action:'update_settings'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();		
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	
	
/* Update/Save Google Api Key */
jQuery(document).on('click','#mp_save_api_key',function(){
		
		var ajax_url = header_object.plugin_path;	
		var moveto_api_key = jQuery('#moveto_google_api_key').val();
		var moveto_company_origin_address = jQuery('#moveto_company_origin_address').val();
		var moveto_company_destination_address = jQuery('#moveto_company_destination_address').val();
		if(jQuery('#moveto_company_origin_destination_distance_status').prop('checked')==true){ 
			var moveto_company_origin_destination_distance_status ='E';
		if(moveto_company_origin_address == ''){
			jQuery('#error_coa').show();
			return;
		}else{
			jQuery('#error_coa').hide();
		}
		}else{ 
			var moveto_company_origin_destination_distance_status='D';
		}
		if(jQuery('#moveto_destination_company_distance').prop('checked')==true){ 
			var moveto_destination_company_distance ='E';
			if(moveto_company_destination_address == ''){
			jQuery('#error_dca').show();
			return;
			}else{
				jQuery('#error_dca').hide();
			}
		}else{ 
			var moveto_destination_company_distance='D';}
		jQuery('.loader').show();
		country_code = "";
		country_name = "";
		jQuery("#google_map_country_code :selected").map(function(i, el) {
			if(jQuery(el).val() != 'All Countries'){
				country_code +=  jQuery(el).val();
				temp = jQuery(el).data('subtext').split(" (");
				country_name = temp[0];
			}else{
				country_code = '';
				country_name = 'New York';
			}
			
		});
		var postdata = { moveto_company_destination_address:moveto_company_destination_address,moveto_company_origin_address:moveto_company_origin_address,moveto_destination_company_distance:moveto_destination_company_distance,moveto_company_origin_destination_distance_status:moveto_company_origin_destination_distance_status,moveto_api_key:moveto_api_key,moveto_country_flags:country_code, country:country_name, setting_action:'update_api_key'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();		
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	
/* Update Email Template **/
jQuery(document).on('click','.mp_save_emailtemplate',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var template_id = jQuery(this).data('eid');		
		var email_subject = jQuery('input[name="email_subject'+template_id+'"]').val();
		var email_message = jQuery('textarea[name="email_message'+template_id+'"]').val();
		jQuery('#email_subject_label'+template_id).text(email_subject);
		var postdata = { template_id:template_id,
						 email_subject:email_subject,
						 email_message:email_message,
						 setting_action:'update_emailtemplate'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();		
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	

/* Update Email Template Status */
jQuery(document).on('change','.mp_update_emailstatus',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var template_id = jQuery(this).data('eid');
		if(jQuery(this).prop('checked')==true){ var email_status ='e';}else{ var email_status='d';}		
		var postdata = { template_id:template_id,
						 email_status:email_status,
						 setting_action:'update_emailtemplate_status'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});


/* Update/Save SMS Settings */
jQuery(document).on('click','#mp_update_smssettings',function(){
	
	
		if(jQuery('#moveto_sms_noti_twilio').prop('checked')==true){ var moveto_sms_noti_twilio ='E';}else{ var moveto_sms_noti_twilio='D';}
		if(jQuery('#moveto_twilio_admin_sms_notification_status').prop('checked')==true){ var moveto_twilio_admin_sms_notification_status ='E';}else{ var moveto_twilio_admin_sms_notification_status='D';}
		
		if(jQuery('#moveto_sms_noti_plivo').prop('checked')==true){ var moveto_sms_noti_plivo ='E';}else{ var moveto_sms_noti_plivo='D';}		
		if(jQuery('#moveto_plivo_admin_sms_notification_status').prop('checked')==true){ var moveto_plivo_admin_sms_notification_status ='E';}else{ var moveto_plivo_admin_sms_notification_status='D';}
		
		if(jQuery('#moveto_sms_noti_nexmo').prop('checked')==true){ var moveto_sms_noti_nexmo ='E';}else{ var moveto_sms_noti_nexmo='D';}
		if(jQuery('#moveto_nexmo_send_sms_admin_status').prop('checked')==true){ var moveto_nexmo_send_sms_admin_status ='E';}else{ var moveto_nexmo_send_sms_admin_status='D';}
		
		if(jQuery('#moveto_sms_noti_textlocal').prop('checked')==true){ var moveto_sms_noti_textlocal ='E';}else{ var moveto_sms_noti_textlocal='D';}
		if(jQuery('#moveto_textlocal_admin_sms_notification_status').prop('checked')==true){ var moveto_textlocal_admin_sms_notification_status ='E';}else{ var moveto_textlocal_admin_sms_notification_status='D';}
		
		/* Validate SMS Notification Settings Form */
		var twilliosid_err_msg = admin_validation_err_msg.twilliosid_err_msg;	
		var twillioauthtoken_err_msg = admin_validation_err_msg.twillioauthtoken_err_msg;	
		var twilliosendernum_err_msg = admin_validation_err_msg.twilliosendernum_err_msg;	
		var twillioadminnum_err_msg = admin_validation_err_msg.twillioadminnum_err_msg;	
		
		var plivosid_err_msg = admin_validation_err_msg.plivosid_err_msg;	
		var plivoauthtoken_err_msg = admin_validation_err_msg.plivoauthtoken_err_msg;	
		var plivosendernum_err_msg = admin_validation_err_msg.plivosendernum_err_msg;	
		var plivoadminnum_err_msg = admin_validation_err_msg.plivoadminnum_err_msg;	
		
		var nexmoapi_err_msg = admin_validation_err_msg.nexmoapi_err_msg;	
		var nexmoapisecert_err_msg = admin_validation_err_msg.nexmoapisecert_err_msg;	
		var nexmofromnum_err_msg = admin_validation_err_msg.nexmofromnum_err_msg;	
		var nexmoadminnum_err_msg = admin_validation_err_msg.nexmoadminnum_err_msg;
		
		if(moveto_sms_noti_twilio=="E"){	
			jQuery(".mp-sms-reminder").validate();
				jQuery("#moveto_twilio_sid").rules("add", { required: true,messages: { required: twilliosid_err_msg}});
				jQuery("#moveto_twilio_auth_token").rules("add", { required: true,messages: { required: twillioauthtoken_err_msg}});
				jQuery("#moveto_twilio_number").rules("add", { required: true,messages: { required: twilliosendernum_err_msg}});
				if(moveto_twilio_admin_sms_notification_status=="E"){	
				jQuery("#moveto_twilio_admin_phone_no").rules("add", { required: true,messages: { required: twillioadminnum_err_msg}});
			}
		}		
		
		if(moveto_sms_noti_plivo=="E"){	
			jQuery(".mp-sms-reminder").validate();
				jQuery("#moveto_plivo_sid").rules("add", { required: true,messages: { required: plivosid_err_msg}});
				jQuery("#moveto_plivo_auth_token").rules("add", { required: true,messages: { required: plivoauthtoken_err_msg}});
				jQuery("#moveto_plivo_number").rules("add", { required: true,messages: { required: plivosendernum_err_msg}});
				if(moveto_plivo_admin_sms_notification_status=="E"){	
				jQuery("#moveto_plivo_admin_phone_no").rules("add", { required: true,messages: { required: plivoadminnum_err_msg}});
			}
		}
		
		if(moveto_sms_noti_nexmo=="E"){	
			jQuery(".mp-sms-reminder").validate();
				jQuery("#moveto_nexmo_apikey").rules("add", { required: true,messages: { required: nexmoapi_err_msg}});
				jQuery("#moveto_nexmo_api_secret").rules("add", { required: true,messages: { required: nexmoapisecert_err_msg}});
				jQuery("#moveto_nexmo_form").rules("add", { required: true,messages: { required: nexmofromnum_err_msg}});
				if(moveto_nexmo_send_sms_admin_status=="E"){	
				jQuery("#moveto_nexmo_admin_phone_no").rules("add", { required: true,messages: { required: nexmoadminnum_err_msg}});
			}
		}
		/* Validate SMS Notification Settings Form End */ 	
	
	
	if(jQuery('.mp-sms-reminder').valid()) {
		
		jQuery('.loader').show(); 
		var ajax_url = header_object.plugin_path;	
		
		if(jQuery('#moveto_sms_reminder_status').prop('checked')==true){ var moveto_sms_reminder_status ='E';}else{ var moveto_sms_reminder_status='D';}
		/* Twillio */
		if(jQuery('#moveto_twilio_client_sms_notification_status').prop('checked')==true){ var moveto_twilio_client_sms_notification_status ='E';}else{ var moveto_twilio_client_sms_notification_status='D';}
		if(jQuery('#moveto_twilio_service_provider_sms_notification_status').prop('checked')==true){ var moveto_twilio_service_provider_sms_notification_status ='E';}else{ var moveto_twilio_service_provider_sms_notification_status='D';}		
		
		/* Plivo */		
		if(jQuery('#moveto_plivo_service_provider_sms_notification_status').prop('checked')==true){ var moveto_plivo_service_provider_sms_notification_status ='E';}else{ var moveto_plivo_service_provider_sms_notification_status='D';}
		if(jQuery('#moveto_plivo_client_sms_notification_status').prop('checked')==true){ var moveto_plivo_client_sms_notification_status ='E';}else{ var moveto_plivo_client_sms_notification_status='D';}		
		
		/* Nexmo */		
		if(jQuery('#moveto_nexmo_send_sms_client_status').prop('checked')==true){ var moveto_nexmo_send_sms_client_status ='E';}else{ var moveto_nexmo_send_sms_client_status='D';}
		if(jQuery('#moveto_nexmo_send_sms_sp_status').prop('checked')==true){ var moveto_nexmo_send_sms_sp_status ='E';}else{ var moveto_nexmo_send_sms_sp_status='D';}
	
		/* Textlocal */
		if(jQuery('#moveto_textlocal_service_provider_sms_notification_status').prop('checked')==true){ var moveto_textlocal_service_provider_sms_notification_status ='E';}else{ var moveto_textlocal_service_provider_sms_notification_status='D';}
		if(jQuery('#moveto_textlocal_client_sms_notification_status').prop('checked')==true){ var moveto_textlocal_client_sms_notification_status ='E';}else{ var moveto_textlocal_client_sms_notification_status='D';}
		if(jQuery('#moveto_textlocal_admin_sms_notification_status').prop('checked')==true){ var moveto_textlocal_admin_sms_notification_status ='E';}else{ var moveto_textlocal_admin_sms_notification_status='D';}	
		var postdata = {
						moveto_sms_reminder_status:moveto_sms_reminder_status,
						/* Twillio */
						moveto_sms_noti_twilio:moveto_sms_noti_twilio,
						moveto_twilio_number:jQuery('#moveto_twilio_number').val(),
						moveto_twilio_sid:jQuery('#moveto_twilio_sid').val(),
						moveto_twilio_auth_token:jQuery('#moveto_twilio_auth_token').val(),
						moveto_twilio_client_sms_notification_status:moveto_twilio_client_sms_notification_status,
						moveto_twilio_service_provider_sms_notification_status:moveto_twilio_service_provider_sms_notification_status,
						moveto_twilio_admin_sms_notification_status:moveto_twilio_admin_sms_notification_status,
						moveto_twilio_admin_phone_no:jQuery('#moveto_twilio_admin_phone_no').val(),
						moveto_twilio_ccode:jQuery('#moveto_twilio_ccode').val(),
						moveto_twilio_ccode_alph:jQuery('#moveto_twilio_ccode_alph').val(),
						/* Plivo */
						moveto_sms_noti_plivo:moveto_sms_noti_plivo,
						moveto_plivo_number:jQuery('#moveto_plivo_number').val(),
						moveto_plivo_sid:jQuery('#moveto_plivo_sid').val(),
						moveto_plivo_auth_token:jQuery('#moveto_plivo_auth_token').val(),
						moveto_plivo_service_provider_sms_notification_status:moveto_plivo_service_provider_sms_notification_status,
						moveto_plivo_client_sms_notification_status:moveto_plivo_client_sms_notification_status,
						moveto_plivo_admin_sms_notification_status:moveto_plivo_admin_sms_notification_status,
						moveto_plivo_admin_phone_no:jQuery('#moveto_plivo_admin_phone_no').val(),
						moveto_plivo_ccode:jQuery('#moveto_plivo_ccode').val(),
						moveto_plivo_ccode_alph:jQuery('#moveto_plivo_ccode_alph').val(),
						/* Nexmo */
						moveto_sms_noti_nexmo:moveto_sms_noti_nexmo,
						moveto_nexmo_apikey:jQuery('#moveto_nexmo_apikey').val(),
						moveto_nexmo_api_secret:jQuery('#moveto_nexmo_api_secret').val(),
						moveto_nexmo_form:jQuery('#moveto_nexmo_form').val(),
						moveto_nexmo_send_sms_client_status:moveto_nexmo_send_sms_client_status,
						moveto_nexmo_send_sms_sp_status:moveto_nexmo_send_sms_sp_status,
						moveto_nexmo_send_sms_admin_status:moveto_nexmo_send_sms_admin_status,
						moveto_nexmo_admin_phone_no:jQuery('#moveto_nexmo_admin_phone_no').val(),
						moveto_nexmo_ccode:jQuery('#moveto_nexmo_ccode').val(),
						moveto_nexmo_ccode_alph:jQuery('#moveto_nexmo_ccode_alph').val(),
						/* Textlocal */
						moveto_sms_noti_textlocal:moveto_sms_noti_textlocal,
						moveto_textlocal_apikey:jQuery('#moveto_textlocal_apikey').val(),
						moveto_textlocal_sender:jQuery('#moveto_textlocal_sender').val(),
						moveto_textlocal_service_provider_sms_notification_status:moveto_textlocal_service_provider_sms_notification_status,
						moveto_textlocal_client_sms_notification_status:moveto_textlocal_client_sms_notification_status,
						moveto_textlocal_admin_sms_notification_status:moveto_textlocal_admin_sms_notification_status,
						moveto_textlocal_admin_phone_no:jQuery('#moveto_textlocal_admin_phone_no').val(),
						moveto_textlocal_ccode:jQuery('#moveto_textlocal_ccode').val(),
						moveto_textlocal_ccode_alph:jQuery('#moveto_textlocal_ccode_alph').val(),
						
						setting_action:'update_settings'						 
		}
		
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();	
						moveto_hide_success_msg();	
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
	}		
});	

/* Update/Save SMS Settings */
jQuery(document).on('click','#mp_save_sms_settings',function(){
		jQuery('.loader').show();

		var ajax_url = header_object.plugin_path;
		if(jQuery('#moveto_admin_sms_notification_status').prop('checked')==true){ var moveto_admin_sms_notification_status ='E';}else{ var moveto_admin_sms_notification_status='D';}
		if(jQuery('#moveto_service_provider_sms_notification_status').prop('checked')==true){ var moveto_service_provider_sms_notification_status ='E';}else{ var moveto_service_provider_sms_notification_status='D';}
		if(jQuery('#moveto_client_sms_notification_status').prop('checked')==true){ var moveto_client_sms_notification_status ='E';}else{ var moveto_client_sms_notification_status='D';}
						
		var postdata = { moveto_admin_sms_notification_status:moveto_admin_sms_notification_status,
						 moveto_service_provider_sms_notification_status:moveto_service_provider_sms_notification_status,
						 moveto_client_sms_notification_status:moveto_client_sms_notification_status,
						 moveto_sms_sender_address:jQuery('#moveto_sms_sender_address').val(),
						 moveto_sms_sender_name:jQuery('#moveto_sms_sender_name').val(),
						 moveto_sms_reminder_buffer:jQuery('#moveto_sms_reminder_buffer').val(),
						 setting_action:'update_settings'						 
		}

		
		/* var postdata = { moveto_twilio_admin_sms_notification_status:moveto_twilio_admin_sms_notification_status,
						 moveto_twilio_service_provider_sms_notification_status:moveto_twilio_service_provider_sms_notification_status,
						 moveto_twilio_client_sms_notification_status:moveto_twilio_client_sms_notification_status,
						 
						 moveto_plivo_admin_sms_notification_status:moveto_plivo_admin_sms_notification_status,
						 moveto_plivo_service_provider_sms_notification_status:moveto_plivo_service_provider_sms_notification_status,
						 moveto_plivo_client_sms_notification_status:moveto_plivo_client_sms_notification_status,
						 
						 
						 moveto_sms_sender_address:jQuery('#moveto_sms_sender_address').val(),
						 moveto_sms_sender_name:jQuery('#moveto_sms_sender_name').val(),
						 moveto_sms_reminder_buffer:jQuery('#moveto_sms_reminder_buffer').val(),
						 setting_action:'update_settings'						 
		} */
		
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	
/* Update SMS Template **/
jQuery(document).on('click','.mp_save_smstemplate',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var template_id = jQuery(this).data('eid');		
		var sms_message = jQuery('textarea[name="sms_message'+template_id+'"]').val();
		var postdata = { template_id:template_id,
						 sms_message:sms_message,
						 setting_action:'update_smstemplate'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});	

/* Update SMS Template Status */
jQuery(document).on('change','.mp_update_smsstatus',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var template_id = jQuery(this).data('eid');
		if(jQuery(this).prop('checked')==true){ var sms_status ='e';}else{ var sms_status='d';}		
		var postdata = { template_id:template_id,
						 sms_status:sms_status,
						 setting_action:'update_smstemplate_status'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();	
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});




/* Create Coupons */
jQuery(document).on('click','#mp_create_coupon',function(){
		if(!jQuery('#mp_create_coupon_form').valid()){
			return false;
		}
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var coupon_code = jQuery('#mp_coupon_code').val();
		var coupon_type = jQuery('#mp_coupon_type').val();
		var coupon_value = jQuery('#mp_coupon_value').val();
		var coupon_limit = jQuery('#mp_coupon_limit').val();
		var coupon_expiry = jQuery("#mp_coupon_expiry").val();
		var postdata = { coupon_code:coupon_code,
						 coupon_type:coupon_type,
						 coupon_value:coupon_value,
						 coupon_limit:coupon_limit,
						 coupon_expiry:coupon_expiry,
						 setting_action:'create_coupon'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						
						//jQuery('a#mp_promocode_list').trigger('click');
						jQuery('.loader').hide();	
						jQuery('#mp_coupon_code').val('');
						jQuery('#mp_coupon_type').val('');
						jQuery('#mp_coupon_value').val('');
						jQuery('#mp_coupon_limit').val('');
						jQuery("#mp_coupon_expiry").val('');
						jQuery('.mp_promocode_list').addClass('active');
						jQuery('#mp_promocode_list').addClass('active in');
						jQuery('.mp_addnew_promocode').removeClass('active');
						jQuery('#mp_addnew_promocode').removeClass('active in');
						jQuery('#coupon_list .odd').hide();
						jQuery('#coupon_list').append(response);
						jQuery('.mainheader_message_inner').show();	
						moveto_hide_success_msg();						
						location.reload();						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});

/* Update Coupon Status */
jQuery(document).on('change','.mp_update_couponstatus',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var coupon_id = jQuery(this).data('cid');
		if(jQuery(this).prop('checked')==true){ var coupon_status ='e';}else{ var coupon_status='d';}		
		var postdata = { coupon_id:coupon_id,
						 coupon_status:coupon_status,
						 setting_action:'update_coupon_status'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();		
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});
/* Delete Coupon */
jQuery(document).on('click','.mp_delete_coupon',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var coupon_id = jQuery(this).data('id');
		var postdata = { coupon_id:coupon_id,
						 setting_action:'delete_coupon'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('#coupon_detail'+coupon_id).fadeOut('slow');
						jQuery('#coupon_detail'+coupon_id).remove();
						if(jQuery('#coupon_list tr').size()==0){
							jQuery('#coupon_list').append('<tr class="odd"><td class="dataTables_empty" colspan="5" valign="top">No data available in table</td></tr>');	
						}
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});
/* Update Coupon Information */
jQuery(document).on('click','.mp_update_promocode',function(){
		var coupon_id= jQuery(this).data('cid');	
		jQuery('.mp_promocode_list').removeClass('active');
		jQuery('#mp_promocode_list').removeClass('active in');
		jQuery('.mp_update_promocode_tab a').removeClass('hide-div');
		jQuery('.mp_update_promocode_tab').addClass('active');
		jQuery('#mp_update_promocode').addClass('active in');
		jQuery('.mp_coupon_update_info').each(function(){
			jQuery(this).addClass('hide-div');
		});
		jQuery('#mp_coupon_update_info'+coupon_id).removeClass('hide-div');
});
jQuery(document).on('click','.mp_update_coupon_info',function(){
		var coupon_id = jQuery(this).attr('id');
		if(!jQuery('#mp_update_promocode_info'+coupon_id).valid()){
			return false;
		}
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;
		var coupon_code = jQuery('#mp_uc_code'+coupon_id).val();
		var coupon_type = jQuery('#mp_uc_type'+coupon_id).val();
		var coupon_value = jQuery('#mp_uc_value'+coupon_id).val();
		var coupon_limit = jQuery('#mp_uc_limit'+coupon_id).val();
		var coupon_expiry = jQuery('#mp_uc_expiry'+coupon_id).val();
		var coupon_status = jQuery('#mp_uc_status'+coupon_id).val();
		var postdata = { coupon_id:coupon_id,
						 coupon_code:coupon_code,
						 coupon_type:coupon_type,
						 coupon_value:coupon_value,
						 coupon_limit:coupon_limit,
						 coupon_expiry:coupon_expiry,
						 coupon_status:coupon_status,
						 setting_action:'update_coupon'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/setting_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						jQuery('.mp_promocode_list').addClass('active');
						jQuery('#mp_promocode_list').addClass('active in');
						jQuery('.mp_update_promocode_tab').removeClass('active');
						jQuery('.mp_update_promocode_tab a').addClass('hide-div');
						jQuery('#mp_update_promocode').removeClass('active in');
						jQuery('#coupon_detail'+coupon_id).remove();
						jQuery('#coupon_list').append(response);
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
												
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});
/* Coupon Form validations */
jQuery(document).on('ready ajaxComplete', function(){
		var cuponcode_err_msg = admin_validation_err_msg.cuponcode_err_msg;	
		var cuponvalue_err_msg = admin_validation_err_msg.cuponvalue_err_msg;	
		var cuponvalueinvalid_err_msg = admin_validation_err_msg.cuponvalueinvalid_err_msg;	
		var cuponlimit_err_msg = admin_validation_err_msg.cuponlimit_err_msg;	
		var cuponlimitinvalid_err_msg = admin_validation_err_msg.cuponlimitinvalid_err_msg;	
	/** Add New Coupon Validations **/
	jQuery('#mp_create_coupon_form').validate({
			rules:{
				mp_coupon_code:{required:true},
				mp_coupon_value:{required:true,number:true},
				mp_coupon_limit:{required:true,number:true},
			},
		messages:{	mp_coupon_code:{required:cuponcode_err_msg},
					mp_coupon_value:{required:cuponvalue_err_msg,number:cuponvalueinvalid_err_msg},mp_coupon_limit:{required:cuponlimit_err_msg,number:cuponlimitinvalid_err_msg}
				}
		});
	/** Update Coupon Validations **/
	jQuery('.mp_update_promocode_info').each(function(){
		jQuery(this).validate({
			rules:{
				mp_uc_code:{required:true},
				mp_uc_value:{required:true,number:true},
				mp_uc_limit:{required:true,number:true},
			},
		messages:{	mp_uc_code:{required:cuponcode_err_msg},
					mp_uc_value:{required:cuponvalue_err_msg,number:cuponvalueinvalid_err_msg},mp_uc_limit:{required:cuponlimit_err_msg,number:cuponlimitinvalid_err_msg}
				}
		});	
	});		
});

					/***********************Settings Jquery End Here **********************/

/******************************** Payments Sextion Jquery ****************************************/
/* Get Payments By Selected Range */
jQuery(document).on('click','.mp_payments_byrange',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;
		var payment_table = jQuery('#payments-details').DataTable();
		var payment_start = jQuery('#mp_reportrange').data('daterangepicker').startDate.format(date_format_for_js);
		var payment_end = jQuery('#mp_reportrange').data('daterangepicker').endDate.format(date_format_for_js);
		var postdata = { payment_start:payment_start,
						 payment_end:payment_end,
						 general_ajax_action:'get_payments_byrange'						 
		}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						payment_table.destroy();
						
						jQuery('.loader').hide();	
						jQuery('#mp_payment_details').html(response);	
						jQuery('#payments-details').DataTable({
							dom: 'lfrtipB',							
							buttons: ['excelHtml5','pdfHtml5']
						});						
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
			});
});			 
/* Get All Locations Customer Registered/Guest */
jQuery(document).on('change','#mp_all_locations_payments',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		if(jQuery(this).prop('checked')==true){ var alp ='Y';}else{ var alp='N';}	
		var postdata = { alp:alp,
						 general_ajax_action:'get_all_locations_payments'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('.loader').hide();
					window.location.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
		});
});   
   
			 /*********************** Payments Jquery End Here **********************/
			 
/******************************** Clients  Jquery End Here ****************************************/
/* Show Client Bookings for Registerd & Guest User Modal Content Both */
 jQuery(document).on('click','.mp_show_bookings',function(){
			jQuery('.loader').show();
			var ajax_url = header_object.plugin_path;			
			var client_id = jQuery(this).attr('data-client_id');
			var method = jQuery(this).attr('data-method');
			var client_bookings = jQuery('#'+method+'-client-booking-details').DataTable();
			var postdata =  { 
				method:method,
				listing_client_id:client_id,
				general_ajax_action:'get_client_bookings'
			}
			jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',
				data : postdata,
				dataType : 'html',
				success  : function(response) {
					client_bookings.destroy();
					jQuery('.loader').hide();	
					jQuery('#mp_client_bookings'+method).html(response);
					jQuery('#'+method+'-client-booking-details').DataTable( {
						dom: 'frtipB',
						buttons: [
							'copyHtml5',
							'excelHtml5',
							'csvHtml5',
							'pdfHtml5'
						]
					});
					
				}	
			});
	 });	
	 
/* Delete Registered/Guest User & User Info Like Bookings,Payments,Order Client Info */   
 jQuery(document).on('click','.mp_delete_client' , function () {				
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var client_id = jQuery(this).data('client_id');		
		var method = jQuery(this).data('method');		
		var postdata =  { delete_id:client_id,
						  method:method,
						  general_ajax_action:'delete_'+method+'_client'						
		}		
		jQuery.ajax({				
			url  : ajax_url+"/assets/lib/admin_general_ajax.php",				
			type : 'POST',				
			data : postdata,				
			dataType : 'html',				
			success  : function(response) {	
				var otpostdata = { user_type:method,
				 general_ajax_action:'refresh_register_client_datatable'						 
				}
				jQuery.ajax({					
					url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
					type : 'POST',					
					data : otpostdata,					
					dataType : 'html',					
					success  : function(otresponse) {
						jQuery('.loader').hide();
						if(method=='registered'){
							jQuery('#registered-customers-listing').html(otresponse);
							jQuery('#registered-client-table').DataTable({
								dom: 'lfrtipB',								
								buttons: [
									 {
										extend: 'copyHtml5',
										exportOptions: {
											columns: [ 0, 1, 2 ]
										}
									},
									{
										extend: 'excelHtml5',
										exportOptions: {
											 columns: [ 0, 1, 2 ]
										}
									},
									{
										extend: 'csvHtml5',
										exportOptions: {
											 columns: [ 0, 1, 2 ]
										}
									},
									{
										extend: 'pdfHtml5',
										exportOptions: {
											columns: [ 0, 1, 2]
										}
									}
								]
							});
						}
						if(method=='guest'){
							jQuery('#guest-customers-listing').html(otresponse);
							jQuery('#guest-client-table').DataTable({						
							    dom: 'lfrtipB',								
								buttons: [
									/* 'copyHtml5',
									'excelHtml5',
									'csvHtml5',
									'pdfHtml5' */
									{
										extend: 'copyHtml5',
										exportOptions: {
											columns: [ 0, 1, 2 ]
										}
									},
									{
										extend: 'excelHtml5',
										exportOptions: {
											 columns: [ 0, 1, 2 ]
										}
									},
									{
										extend: 'csvHtml5',
										exportOptions: {
											 columns: [ 0, 1, 2 ]
										}
									},
									{
										extend: 'pdfHtml5',
										exportOptions: {
											columns: [ 0, 1, 2]
										}
									}
								]
							});
						}						
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
				});							
			}				
		});							 				
}); 

/* Get All Locations Customer Registered/Guest */
jQuery(document).on('change','#mp_all_locations_customers',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		if(jQuery(this).prop('checked')==true){ var alc ='Y';}else{ var alc='N';}	
		var postdata = { alc:alc,
						 general_ajax_action:'get_all_locations_customers'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('.loader').hide();
					window.location.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
		});
});


			 /*********************** Clients Jquery End Here **********************/
/********************************** Export Section Jquery ************************************/
	 
/* Get Filtered Bookings */ 
jQuery(document).on('click','.ranges li,.range_inputs .applyBtn' , function () {	
	jQuery('#mp_booking_startdate').val(jQuery('#mp_reportrange').data('daterangepicker').startDate.format(date_format_for_js));
	jQuery('#mp_booking_enddate').val(jQuery('#mp_reportrange').data('daterangepicker').endDate.format(date_format_for_js));
	
});
jQuery(document).ready(function(){
		jQuery('#mp_export_bookings').DataTable( {
			dom: 'lfrtipB',
			
			buttons: [
				/* 'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5' */
				 {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                }
            },
			{
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [ 0,1, 2,3,4,5,6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                }
            },
           
			]
		});
	});  
 jQuery(document).on('click','#mp_filtered_bookings' , function () {				
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;	
		var booking_start = jQuery('#mp_booking_startdate').val();
		var booking_end = jQuery('#mp_booking_enddate').val();
		var booking_service = jQuery('#mp_booking_service').val();
		var booking_staff = jQuery('#mp_booking_staff').val();
		var export_bookings = jQuery('#mp_export_bookings').DataTable();
		var method = jQuery(this).data('method');		
		var postdata =  { booking_start:booking_start,
						  booking_end:booking_end,
						  booking_service:booking_service,
						  booking_staff:booking_staff,
						  general_ajax_action:'filtered_bookings'							
		}		
		jQuery.ajax({				
			url  : ajax_url+"/assets/lib/admin_general_ajax.php",				
			type : 'POST',				
			data : postdata,				
			dataType : 'html',				
			success  : function(response) {	
				jQuery('.loader').hide();
				jQuery('#mp_booking_startdate').val('');
				jQuery('#mp_booking_enddate').val('');
				export_bookings.destroy();
					jQuery('#mp_export_bookings_data').html(response);
					jQuery('#mp_export_bookings').DataTable({
						dom: 'frtipB',
						buttons: [
							'copyHtml5',
							'excelHtml5',
							'csvHtml5',
							'pdfHtml5'
						]
					});								
			}				
		});							 				
}); 
/* Get Filtered Staff In Export Section */
jQuery(document).on('change','#mp_staff_filter' , function () {	
		jQuery('.loader').show();
		var staff_name = jQuery(this).val();
		jQuery('#staff-info-table_filter input').val(staff_name);
		jQuery('#staff-info-table_filter input').keyup();
		jQuery('.loader').hide();
});
/* Get Filtered Service In Export Section */
jQuery(document).on('change','#mp_service_filter' , function () {	
		jQuery('.loader').show();
		var service_name = jQuery(this).val();
		jQuery('#services-info-table_filter input').val(service_name);
		jQuery('#services-info-table_filter input').keyup();
		jQuery('.loader').hide();
});
/* Get Filtered Categories In Export Section */
jQuery(document).on('change','#mp_category_filter' , function () {	
		jQuery('.loader').show();
		var service_name = jQuery(this).val();
		jQuery('#category-info-table_filter input').val(service_name);
		jQuery('#category-info-table_filter input').keyup();
		jQuery('.loader').hide();
});
/* Get Filtered Locations In Export Section */
jQuery(document).on('change','#mp_location_filter' , function () {	
		jQuery('.loader').show();
		var service_name = jQuery(this).val();
		jQuery('#location-info-table_filter input').val(service_name);
		jQuery('#location-info-table_filter input').keyup();
		jQuery('.loader').hide();
});
/* Get All Locations Export Data */
jQuery(document).on('change','#mp_all_exportdata',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		if(jQuery(this).prop('checked')==true){ var aled ='Y';}else{ var aled='N';}	
		var postdata = { aled:aled,
						 general_ajax_action:'get_all_exportdata'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('.loader').hide();
					window.location.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
		});
});
			/*********************** Export Jquery End Here **********************/
/******************************* Appointments Section Jquery *********************************/
	
/* Show Booking Detail Modal On click Event */
jQuery(document).on('ready', function(){
		jQuery(".fc-day-grid-event").click(function(){
			jQuery("#booking-details").css('display','block');
		});
		jQuery(document).on('click','.add-new-customer-cal',function(){
				jQuery('#add_app_det').removeClass('active');	
				jQuery('#add_cust_det').addClass("active");
		});
		jQuery(document).on('click','#customer_add_new',function(){
				jQuery('#add_app_det').removeClass('active');	
				jQuery('#add_cust_det').addClass("active");
		});
});
/* Show Reschedule Booking modal On Booking Details Modal */
jQuery(document).on('click','#edit-booking-details',function(){
	jQuery('#edit-booking-details-view').modal();
	jQuery('#mp_booking_datetime').trigger('change');
});
jQuery(document).bind('ready ajaxComplete', function(){
	  /* Show Confirm Appointment Popover **/
		 jQuery('#mp-confirm-appointment-cal-popup').popover({ 
			html : true,
			content: function() {
			  return jQuery('#popover-confirm-appointment-cal-popup').html();
			}
		  });
	  /* Hide Confirm Appointment Popover */
		jQuery(document).on('click', '#mp-close-confirm-appointment-cal-popup', function(){		
			jQuery('.popover').fadeOut();
		});
	  /* Show Reject Appointment Popover **/
	  jQuery('#mp-reject-appointment-cal-popup').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-reject-appointment-cal-popup').html();
		}
	  });
	 /* Hide Reject Appointment Popover */
		jQuery(document).on('click', '#mp-close-reject-appointment-cal-popup', function(){		
			jQuery('.popover').fadeOut();
		});	
	
	 /* Show Cancel Appointment Popover **/
	 jQuery('#mp-cancel-appointment-cal-popup').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-cancel-appointment-cal-popup').html();
		}
	  });
	 /* Hide Cancel Appointment Popover */
		jQuery(document).on('click', '#mp-close-cancel-appointment-cal-popup', function(){		
			jQuery('.popover').fadeOut();
		});	
	 /* Show Delete Appointment Popover **/
	 jQuery('#mp-delete-appointment-cal-popup').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-delete-appointment-cal-popup').html();
		}
	 });
	/* Hide Delete Appointment Popover */
	jQuery(document).on('click', '#mp-close-del-appointment-cal-popup', function(){			
		jQuery('.popover').fadeOut();
	});
	
	/* Show Delete Past Booking Popover */
		jQuery('.mp_delete_past_booking').popover({ 
				html : true,
				content: function() {
					var booking_id = jQuery(this).data('id');
					jQuery('.popover').each(function(){
						jQuery(this).fadeOut('slow');
					});
				  return jQuery('#popover-'+booking_id).html();
				}
		});

		/* Hide Delete Past Booking popover */
		jQuery(document).on('click', '.mp_close_delete_booking_popover', function(){
			jQuery('.popover').fadeOut('slow');
		});
	
});
/* Render Booking Calender And Events On Calender */	
jQuery(document).ready(function() {
	
	if(jQuery('#mp_calendar').length){
	var ajax_url = header_object.plugin_path;
	var cal_first_day = header_object.cal_first_day;	 
	var ak_wp_lang = header_object.ak_wp_lang;
	var default_date = header_object.full_cal_defaultdate;
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var curdate = d.getFullYear() + '/' +
	(month<10 ? '0' : '') + month + '/' +
	(day<10 ? '0' : '') + day;
	jQuery('#mp_calendar').fullCalendar({
		header: {
			left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
		},
		lang: ak_wp_lang,
		refetch: false,
		firstDay: cal_first_day,
		eventLimit: 7, 
		defaultDate:default_date,
		editable: false,
		dayClick:  function(date, jsEvent, view) {
			var d =  new Date(date);
			var tdate = new Date();
			tdate.setDate(tdate.getDate() - 1);
			var curr_date = d.getDate();
			var curr_month = d.getMonth()+1;
			var curr_year = d.getFullYear();
			var newdateis=  curr_month+"/"+curr_date+"/"+curr_year;						
			var dataString={newdateis:newdateis,action:'add_manual_booking_popup'};
			if(d < tdate){
				jQuery('.mainheader_message_fail').show();
				jQuery('.mainheader_message_inner_fail').css('display','inline');	
				/* jQuery('#mp_sucess_message_fail').text('Not allowed booking in past date'); */
				jQuery('.mainheader_message_fail').fadeOut(6000);
				return false;			
			}		
        },
		setHeight: function(height, isAuto) {},		
		eventRender: function (event, element) {
				var event_st = event.event_status;
			
                if(event_st=='Completed'){
					element.find('.fc-title').hide();
					element.find('.fc-time').hide();
					element.find('.fc-title').before(jQuery("<i class='fa fa-check txt-success' title='Completed'></i>"));              
					element.find('.fc-title').after(jQuery("<div>"+event.title+"</div><div><hr id='hr' /></div><div>"+event.client_name+"</div><div>"+event.client_phone+"</div><div>"+event.client_email+"</div>")); 	
                }				
				if(event_st=='Pending' || event_st=='' ){
					element.find('.fc-title').hide();
					element.find('.fc-time').hide();
					element.find('.fc-title').before(jQuery("<i class='fa fa-info-circle txt-warning' title='Pending'></i>"));               
					element.find('.fc-title').after(jQuery("<div>"+event.title+"</div><div><hr id='hr' /></div><div>"+event.client_name+"</div><div>"+event.client_phone+"</div><div>"+event.client_email+"</div>"));
                }				
				
			element.css('background',event.color_tag);
			element.css('border-color',event.color_tag);
			element.attr('href', 'javascript:void(0);');
			element.click(function() {
                            jQuery('.loader').show();
                            var appointment_id = event.id;
                            var getdata =  {
                                appointment_id:appointment_id,
								general_ajax_action:'get_appointment_detail'	
							};
                            jQuery.ajax({                               
                                    type : 'POST',                                   
                                    url  : ajax_url+"/assets/lib/admin_general_ajax.php",
                                    data : getdata,
                                    dataType : 'html',
                                    success  : function(response) {							
                                    var app_details = jQuery.parseJSON(response); 
									
											/** Booking Detail Modal Content **/
											jQuery('.loader').hide();
											jQuery('#mp_booking_confirm').show();
											jQuery('#booking-details').modal();
											jQuery('.pam_send_quote').show();
                                            										
											if(app_details.booking_status=="Completed"){
											jQuery(".mp-booking-status").html("<i class='fa fa-check txt-success' title='Completed'><em>Completed</em></i>");
											jQuery('#mp_booking_confirm').hide();
											/* jQuery('.pam_send_quote').hide(); */
											}else{
											 jQuery(".mp-booking-status").html("<i class='fa fa-info-circle txt-warning' title='Pending'><em>Pending</em></i>");
											}
											jQuery('#mp_booking_delete').attr('data-booking_id',app_details.id);
											jQuery('.pam_send_quote').attr('data-id',app_details.id);
											jQuery(".mp_booking_date").html('<span><i class="fa fa-calendar"></i>'+app_details.booking_date+'</span>');
											
											jQuery(".mp_service").html(app_details.service_info);		
											jQuery(".mp_source_city").html(app_details.source_city);		
											jQuery(".mp_via_city").html(app_details.via_city);		
											jQuery(".mp_destination_city").html(app_details.destination_city);		
											/*jQuery(".service_info").html(app_details.service_info);	*/	
											jQuery(".articles_info").html(app_details.articles_info);		
											jQuery(".additional_info").html(app_details.additional_info);
											jQuery(".loading_info").html(app_details.loading_info);		
											jQuery(".unloading_info").html(app_details.unloading_info);			
                                            jQuery(".client_name").html(app_details.client_name);
                                            jQuery(".client_email").html(app_details.client_email);
                                            jQuery(".client_phone").html(app_details.client_phone);
																						jQuery(".client_payment").html(app_details.payment_method);
                           /*                 jQuery(".client_address").html(app_details.client_address);
											jQuery(".postal_code").html(app_details.client_zipcode);
											jQuery(".client_notes").html(app_details.client_notes);
											jQuery(".custom_info").html(app_details.custom_info);*/
											if(app_details.quote_price=="")
											{
												jQuery('.quo_price').hide();	
											}else{
												jQuery(".quote_price").html(app_details.quote_price);
											
											}
											/** End Booking Detail Modal **/
													
										
                                    }
                                });

                    });  
		},
		events: ajax_url+"/assets/lib/admin_general_ajax.php?general_ajax_action=get_upcoming_appointments"	
	});
	}
});		
/** Get Services By Staff - Manual Booking **/
jQuery(document).on('change','#mp_booking_provider_manual',function(){
		var ajax_url = header_object.plugin_path;		
		var staff_id = jQuery(this).val();
		var postdata = { staff_id:staff_id,
						 general_ajax_action:'get_services_by_staff'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('#mp_booking_service_manual').html(response);
					jQuery('#mp_booking_service_manual').selectpicker('refresh');
					jQuery('#mp_booking_service_manual').trigger('change');
				},
				error: function (xhr, ajaxOptions, thrownError) {
				}
		});
});
/** Get Services Duration,Price,Schedule on change service - Manual Booking **/	
jQuery(document).on('change','#mp_booking_service_manual',function(){
		var ajax_url = header_object.plugin_path;		
		var service_id = jQuery(this).val();
		var postdata = { service_id:service_id,
						 general_ajax_action:'get_services_info'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					var service_info = jQuery.parseJSON(response); 
					jQuery('#mp_service_price_manual').text(service_info.service_price);
					jQuery('#mp_service_duration_manual').text(service_info.service_duration);
					jQuery('#mp_service_duration_val_manual').val(service_info.service_duration_val);
				}
		});
});		

/** Reschedule Appointment **/	
jQuery(document).on('click','#mp_reschedule_booking',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var booking_id = jQuery(this).data('booking_id');
		var booking_date = jQuery('#mp_booking_datetime').val();
		var booking_time = jQuery('#mp_booking_time').val();
		var reschedule_note = jQuery('#mp_booking_rsnotes').val();
		var service_duration = jQuery('#mp_service_duration_val').val();
		var postdata = { booking_id:booking_id,
						 booking_date:booking_date,
						 booking_time:booking_time,
						 service_duration:service_duration,
						 reschedule_note:reschedule_note,
						 method:'RS',
						 general_ajax_action:'reschedule_appointment'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
				window.location.reload();
				}
		});
});		
/** Confirm,Reject,Cancel Appointment **/
jQuery(document).on('click','.mp_crc_appointment',function(){
	jQuery('.loader').show();
	var ajax_url = header_object.plugin_path;		
	var booking_id = jQuery(this).data('booking_id');
	var method = jQuery(this).data('method');
	if(method=='C'){
		var action_content = jQuery('#mp_booking_confirmnote').val();
	}else if(method=='R'){
		var action_content = jQuery('#mp_booking_rejectnote').val();
	}else{
		if(jQuery(this).data('sp')=='Y'){
			var action_content = jQuery('#mp_booking_cancelnote'+booking_id).val();
		}else{
			var action_content = jQuery('#mp_booking_cancelnote').val();
		}
	}
	var postdata = { booking_id:booking_id,
					 method:method,
					 action_content:action_content,
					 general_ajax_action:'c_r_cs_cc_appointment'						 
	}
	jQuery.ajax({
		url  : ajax_url+"/assets/lib/admin_general_ajax.php",
		type : 'POST',
		data : postdata,
		dataType : 'html',
		success  : function(response) {
			jQuery('.loader').hide();
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
			window.location.reload();
		}
	});
});	
/** Delete Appointment **/
jQuery(document).on('click','#mp_booking_delete',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;		
		var booking_id = jQuery(this).data('booking_id');
		var postdata = { booking_id:booking_id,
						 general_ajax_action:'delete_appointment'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
					window.location.reload();
				}
		});
});	
/** Filter Appointment **/
jQuery(document).on('click','#mp_filter_appointments',function(){
		jQuery('.loader').show();		
		var ajax_url = header_object.plugin_path;
		var startdate = jQuery('#mp_booking_startdate').val();
		var enddate = jQuery('#mp_booking_enddate').val();
		var service_id = jQuery('#mp_booking_filterservice').val();
		
		var postdata = { startdate:startdate,
						 enddate:enddate,
						 general_ajax_action:'filter_appointments'						 
		}
		jQuery.ajax({					
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
				jQuery('.loader').hide();
				window.location.reload();
				}
		});
});
/** Get Registerd Client Information - Manual Booking **/	
jQuery(document).on('change','#mp_booking_client_manual',function(){
		var ajax_url = header_object.plugin_path;		
		var client_id = jQuery(this).val();		
		jQuery('.mp-searching-customer').show();
		jQuery('#client_username').show('slow');
		jQuery('#client_password').show('slow');
		jQuery('#mp_clientname_manual').val('');
		jQuery('#mp_clientemail_manual').val('');
		jQuery('#mp_clientemail_manual').keyup();
		jQuery('#mp_clientemail_manual').keydown();
		jQuery('#mp_clientphone_manual').val('');
		jQuery('#mp_clientpassword_manual').val('');
		jQuery('#mp_clientaddress_manual').val('');
		jQuery('#mp_clientcity_manual').val('');
		jQuery('#mp_clientstate_manual').val('');
		jQuery('#mp_clientzip_manual').val('');
		jQuery('#mp_clientcountry_manual').val('');
		jQuery("#manual_booking_form").valid();
		if(client_id!=''){
		jQuery('#mp_clientname_manual-error').hide();	
		jQuery('#mp_clientemail_manual-error').hide();	
		jQuery('.loader').show();		
		jQuery('#client_username').hide('slow');
		jQuery('#client_password').hide('slow');
		
			var postdata = { client_id:client_id,
							 general_ajax_action:'get_client_info'						 
			}
			jQuery.ajax({					
					url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();	
						var client_info = jQuery.parseJSON(response);								
						jQuery('#mp_clientname_manual').val(client_info.client_name);
						jQuery('#mp_clientname_manual').keyup();
						jQuery('#mp_clientemail_manual').val(client_info.client_email);	
						jQuery('#mp_clientemail_manual').keyup();						
						jQuery('#mp_clientphone_manual').val(client_info.client_phone);
						jQuery('#mp_clientphone_manual').keyup();
						jQuery('#mp_clientpassword_manual').val(client_info.service_price);
						jQuery('#mp_clientaddress_manual').val(client_info.client_address);
						jQuery('#mp_clientcity_manual').val(client_info.client_city);
						jQuery('#mp_clientstate_manual').val(client_info.client_state);
						jQuery('#mp_clientzip_manual').val(client_info.client_zip);
						jQuery('#mp_clientcountry_manual').val(client_info.client_country);
						jQuery("#manual_booking_form").valid();
					}
			});
		}
		jQuery('.mp-searching-customer').hide();
});	
/** Book Appointment Manually Form Validations **/
jQuery(document).bind('ready ajaxComplete', function(){
	var staffusername_err_msg = admin_validation_err_msg.staffusername_err_msg;	
		var staffusernameexist_err_msg = admin_validation_err_msg.staffusernameexist_err_msg;	
		var staffpassword_err_msg = admin_validation_err_msg.staffpassword_err_msg;
		var staffemail_err_msg = admin_validation_err_msg.staffemail_err_msg;
		var staffemailexist_err_msg = admin_validation_err_msg.staffemailexist_err_msg;
		var stafffullname_err_msg = admin_validation_err_msg.stafffullname_err_msg;
		var staffselect_err_msg = admin_validation_err_msg.staffselect_err_msg;
		var siteurl = header_object.site_url;
		/* Validaing Manual Booking Form */
		jQuery('#manual_booking_form').validate({
					rules: {
						mp_mb_username: {
													required: true,
													remote: {
															url: siteurl+"/wp-admin/admin-ajax.php",
															type: "POST",
															async: true,
															data: {
															username:function() {
																 return jQuery('input[name="mp_mb_username"]').val();
																},
																action:'check_username_bd'
															}
														}
							},
						mp_mb_password: {
													required: true
							},
						mp_mb_clientname: {
													required: true,
							},
						mp_mb_clientemail: {
													required: true,
													remote: {
													
															url: siteurl+"/wp-admin/admin-ajax.php",
															type: "POST",
															async: true,
															data: {
															email:function() {								
																if(jQuery('#mp_booking_client_manual').val()==''){
																	return jQuery('#mp_clientemail_manual').val();
																	}else{ 
																		return true;
																	}
																},					
																action:'check_email_bd'
															}		
														},
													customemail:true
							},	
	
						},
					messages: {
								mp_mb_username: { required: staffusername_err_msg }, 
								mp_mb_password: { required: staffpassword_err_msg }, 
								mp_mb_clientname: { required: stafffullname_err_msg }, 
								mp_mb_clientemail: { required: staffemail_err_msg , customemail: staffemailexist_err_msg},
						}
						});
});
/** Validation End Here **/
/** Book Appointment Manually **/
jQuery(document).on('click','#mp_book_manual_appointment',function(){
	
		
		if(jQuery('#manual_booking_form').valid()){
				var ajax_url = header_object.plugin_path;
				jQuery('.loader').show();		
				var provider_id = jQuery('#mp_booking_provider_manual').val();
				var service_id = jQuery('#mp_booking_service_manual').val();
				var service_duration = jQuery('#mp_service_duration_val_manual').val();
				var service_price = jQuery('#mp_service_price_manual').text();
				var booking_date = jQuery('#mp_booking_date_manual').val();
				var booking_time = jQuery('#mp_booking_time_manual').val();
				var booking_note = jQuery('#mp_booking_note_manual').val();
				var client_id = jQuery('#mp_booking_client_manual').val();
				var client_name = jQuery('#mp_clientname_manual').val();
				var client_email = jQuery('#mp_clientemail_manual').val();
				var client_phone = jQuery('#mp_clientphone_manual').val();
				var client_username = jQuery('#mp_clientusername_manual').val();
				var client_password = jQuery('#mp_clientpassword_manual').val();
				var client_address = jQuery('#mp_clientaddress_manual').val();
				var client_city = jQuery('#mp_clientcity_manual').val();
				var client_state = jQuery('#mp_clientstate_manual').val();
				var client_zip = jQuery('#mp_clientzip_manual').val();
				var client_country = jQuery('#mp_clientcountry_manual').val();
				
				
				var postdata = { provider_id:provider_id,
								 service_id:service_id,
								 service_duration:service_duration,
								 service_price:service_price,
								 booking_date:booking_date,
								 booking_time:booking_time,
								 booking_note:booking_note,
								 client_id:client_id,
								 client_name:client_name,
								 client_email:client_email,
								 client_phone:client_phone,
								 client_username:client_username,
								 client_password:client_password,
								 client_address:client_address,
								 client_city:client_city,
								 client_state:client_state,
								 client_zip:client_zip,
								 client_country:client_country,
								 general_ajax_action:'book_manual_appointment'						 
				}
				jQuery.ajax({					
						url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
							var booking_id = response;					
							if(header_object.mb_status=='E'){
								var method = 'C';
							}else{
								var method = 'P';
							}
							var emaildata = { booking_id:booking_id,
											  method:method,
								 }
								jQuery.ajax({					
									url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
									type : 'POST',					
									data : emaildata,					
									dataType : 'html',					
									success  : function(response) {
										jQuery('.loader').hide();
										jQuery('.mainheader_message_inner').show();
										moveto_hide_success_msg();
										window.location.reload();
									}
								});
						}			
				});
		}
});
/************************************** Dashboard Section Jquery ************************************/

jQuery(document).on('click','.mp-today-list',function(){
		var ajax_url = header_object.plugin_path;
		jQuery('.loader').show();		
		jQuery('.mp_client_bookingclose').trigger('click');
		jQuery( "#mp-close-confirm-appointment-cal-popup" ).trigger( "click" );
		jQuery( "#mp-close-reject-appointment-cal-popup" ).trigger( "click" );
		jQuery( "#mp-close-del-appointment-cal-popup" ).trigger( "click" );
		var appointment_id = jQuery(this).data('bookingid');
		var getdata =  {
				appointment_id:appointment_id,
				general_ajax_action:'get_appointment_detail'	
			};
		jQuery.ajax({                               
				type : 'POST',                                   
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				data : getdata,
				dataType : 'html',
				success  : function(response) {
				var app_details = jQuery.parseJSON(response);
					jQuery('.loader').hide();
					/** Booking Detail Modal Content **/
					jQuery('#mp_confirm_btn').show();
					jQuery('#mp_reject_btn').show();
					jQuery('#mp_cancel_btn').show();
					jQuery('#mp_reschedule_btn').show();
					jQuery('#booking-details-calendar').modal();
					if(app_details.booking_status=="Completed"){
					jQuery(".mp-booking-status").html("<i class='fa fa-check txt-success' title='Completed'><em>Completed</em></i>");
					jQuery('#mp_confirm_btn').hide();
					/* jQuery('.pam_send_quote').hide(); */
					}else{
					 jQuery(".mp-booking-status").html("<i class='fa fa-info-circle txt-warning' title='Pending'><em>Pending</em></i>");
					 jQuery('.pam_send_quote').show();
					}
					jQuery('#mp_booking_delete').attr('data-booking_id',app_details.id);
					jQuery('.pam_send_quote').attr('data-id',app_details.id);
					jQuery(".mp_booking_date").html('<span><i class="fa fa-calendar"></i>'+app_details.booking_date+'</span>');
					
					jQuery(".mp_service").html(app_details.service_info);
					jQuery(".mp_source_city").html(app_details.source_city);		
					jQuery(".mp_destination_city").html(app_details.destination_city);		
					/*ss*/		
					jQuery(".articles_info").html(app_details.articles_info);		
					jQuery(".additional_info").html(app_details.additional_info);
					jQuery(".loading_info").html(app_details.loading_info);		
					jQuery(".unloading_info").html(app_details.unloading_info);						
					jQuery(".client_name").html(app_details.client_name);
					jQuery(".client_email").html(app_details.client_email);
					jQuery(".client_phone").html(app_details.client_phone);
					jQuery(".client_payment").html(app_details.payment_method);
					/*jQuery(".client_address").html(app_details.client_address);
					jQuery(".postal_code").html(app_details.client_zipcode);
					jQuery(".client_notes").html(app_details.client_notes);
					jQuery(".custom_info").html(app_details.custom_info);*/
					
					if(app_details.quote_price=="")
					{
						jQuery('.quo_price').hide();	
					}else{
						jQuery(".quote_price").html(app_details.quote_price);
					
					}
										
			}
		});
});

/* Service View,Provider View,Coupon View Analytics */
jQuery(document).ready(function(){
	jQuery('.mp_service_chart').trigger('click');
});
jQuery(document).on('click','.mp_view_chart_analytics',function(){
		var ajax_url = header_object.plugin_path;	
		var method = jQuery(this).data('method');
		var chartid = document.getElementById("chart-area-"+method).getContext("2d");
		jQuery('.mp_nodata_'+method).hide();
		jQuery('.chart_view_content').each(function(){
			if(jQuery(this).attr('id')== method+'-view-tab'){
				jQuery(this).show();
			}else{
				jQuery(this).hide();
			}
		});
		var postdata={ 
					method:method,
					general_ajax_action:'view_chart_analytics'
					};
		
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					if(response=='[]'){
						jQuery('.mp_nodata_'+method).show();
						jQuery('#chart-area-'+method).hide();
					}else{
						jQuery('#chart-area-'+method).show();
						var jsondata=jQuery.parseJSON(response);	
						window.myDoughnut = new Chart(chartid).Doughnut(jsondata,{responsive : true
						});
					}
				}
		});		
	});
	
/* Update Notification Count */
jQuery(document).ready(function(){
	
		var ajax_url = header_object.plugin_path;	
		var postdata = { 
						  general_ajax_action:"get_notification_count"
					     };
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('.mp_notification_count').html(response);
					if(jQuery('.get_notification_rem').html() >= 1){
						jQuery('.icon-bell').addClass('mp-new-booking mp-pulse');
					}else{
						jQuery('.icon-bell').removeClass('mp-new-booking mp-pulse');
					}
				}
		   });
		/* }   
		get_notification_count();
		setInterval(function(){
			get_notification_count();
	    },10000); */
});	


/* Get Notification Bookings */
jQuery(document).ready(function () {
	jQuery(document).on('click','#mp-notifications',function(){
		jQuery('#mp-notification-container').removeAttr('style');	
		/* jQuery("#mp-notification-container").show( "blind", {direction: "vertical"}, 500 ); */
		var ajax_url = header_object.plugin_path;	
		var postdata = { 
						  general_ajax_action:"get_notification_bookings"
						 };
		jQuery.ajax({
			url  : ajax_url+"/assets/lib/admin_general_ajax.php",
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {
				jQuery('.mp-recent-booking-list').html(response);
			}
	   });
		jQuery(".mp-notifications-inner").addClass("visible");  
			
	});
	jQuery("#mp-close-notifications").click(function () {
        jQuery(".mp-notifications-inner").removeClass("visible");
	});
						
	jQuery( document ).on( 'keydown', function ( e ) {
		if ( e.keyCode === 27 ) {
			 jQuery(".mp-notifications-inner").removeClass("visible");
		}
	});
});
/* Make Notification/All Notifications Readed */
jQuery(document).on('click','.mp_unread_notification',function(){
		jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;	
		var booking_id = jQuery(this).data('booking_id');
		var postdata = { 
						  booking_id:booking_id,
						  general_ajax_action:"remove_notifications_bookings"
					     };
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',	
				async:false,
				data : postdata,					
				dataType : 'html',					
				success  : function(response){
					if(booking_id=='All'){
							jQuery('#mp-notifications').trigger('click');
							jQuery('.mp_notification_count').html('');
					}else{
							jQuery('#mp_notification'+booking_id).fadeOut('slow');
							if(parseInt(jQuery('#mp-notification-top').text())-1 ==0){
								jQuery('.mp_notification_count').html('');
							}else{	
								jQuery('#mp-notification-top').text(parseInt(jQuery('#mp-notification-top').text())-1);
							}
					}
					jQuery('.loader').hide();	
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
				}
		   });
});
/*moveto Add/Remove Sample Data*/
jQuery(document).on('click','#moveto_sampledata',function(){
		var ajax_url = header_object.plugin_path;	
		jQuery('.loader').show();		
		var method = jQuery(this).data('method');
		
		var postdata = {  method:method,
						  general_ajax_action:"moveto_sampledata"
					     };
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					window.location.reload();
				}
		   });
});




/********************************* Client Admin Area jQuery ******************************************/
/*****************************************************************************************************/
/* Client Cancel Appointment Popover */
	jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('.mp_client_cancel_appointmentpopup').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-'+jQuery(this).attr('id')).html();
		}
	  });
	});
	/* hide delete appointment in modal window */
	jQuery(document).on('click', '.mp_cancel_clientcancel', function(){			
		jQuery('.popover').fadeOut();
	});
	
	
	
	/* review  Popover */
	jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('.mp-add-review-client-btn').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-'+jQuery(this).attr('id')).html();
		}
	  });
	});
	/* hide review in modal window */
	jQuery(document).on('click', '.mp_cancel_review_pop', function(){			
		jQuery('.popover').fadeOut();
	});
	
	/* delete review  Popover */
	jQuery(document).bind('ready ajaxComplete', function(){
	  jQuery('.mp-delete-review').popover({ 
		html : true,
		content: function() {
		  return jQuery('#popover-'+jQuery(this).attr('id')).html();
		}
	  });
	});
	/* hide review in modal window */
	jQuery(document).on('click', '.mp-close-popover-mp-delete-review', function(){			
		jQuery('.popover').fadeOut();
	});
	
	
	
	

/* Get Client Order Bookings */
jQuery(document).on('click','.moveto_client_bookings',function(){
		var ajax_url = header_object.plugin_path;	
		jQuery('.loader').show();		
		var appointment_id = jQuery(this).data('bid');
		var postdata = { 
						appointment_id:appointment_id,
						general_ajax_action:'get_appointment_detail'
					};
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
				jQuery('.loader').hide();	
					var app_details = jQuery.parseJSON(response);
					/** Booking Detail Modal Content **/
					jQuery('.loader').hide();
															
					if(app_details.booking_status=="Completed"){
						jQuery(".mp-booking-status").html("<i class='fa fa-check txt-success' title='Completed'><em>Completed</em></i>");					
					}else{
						jQuery(".mp-booking-status").html("<i class='fa fa-info-circle txt-warning' title='Pending'><em>Pending</em></i>");
					}
					
					jQuery(".mp_booking_date").html('<span><i class="fa fa-calendar"></i>'+app_details.booking_date+'</span>');
					
					jQuery(".mp_service").html(app_details.service_info);		
					jQuery(".mp_source_city").html(app_details.source_city);		
					jQuery(".mp_destination_city").html(app_details.destination_city);		
					/*jQuery(".service_info").html(app_details.);	*/	
					jQuery(".articles_info").html(app_details.articles_info);		
					jQuery(".additional_info").html(app_details.additional_info);		
					jQuery(".loading_info").html(app_details.loading_info);		
					jQuery(".unloading_info").html(app_details.unloading_info);		
					jQuery(".client_name").html(app_details.client_name);
					jQuery(".client_email").html(app_details.client_email);
					jQuery(".client_phone").html(app_details.client_phone);
					jQuery(".client_payment").html(app_details.payment_method);
					/*jQuery(".client_address").html(app_details.client_address);
					jQuery(".postal_code").html(app_details.client_zipcode);
					jQuery(".client_notes").html(app_details.client_notes);	
					jQuery(".custom_info").html(app_details.custom_info);*/
					
					if(app_details.quote_price=="")
					{
						jQuery('.quo_price').hide();	
					}else{
						jQuery(".quote_price").html(app_details.quote_price);
					
					}
					
				}
		   });
});

/*moveto Add/Update Review */
jQuery(document).on('click','#mp_booking_submitreview',function(){
		var ajax_url = header_object.plugin_path;	
		jQuery('.loader').show();		
		var rating = '0';
		var booking_id = jQuery(this).data('booking_id');
		var provider_id = jQuery(this).data('pid');
		var client_id = jQuery(this).data('cid');
		var method = jQuery(this).data('method');
		var review_id = jQuery(this).data('review_id');
		if(jQuery('input[name="moveto_rating'+booking_id+'"]').is(':checked')){
		var rating = jQuery('input[name="moveto_rating'+booking_id+'"]:checked').val();
		}
		var description = jQuery('#moveto_review_desc'+booking_id).val();

		var postdata = {  review_booking_id:booking_id,
						  provider_id:provider_id,
						  client_id:client_id,
						  method:method,
						  review_id:review_id,
						  rating:rating,
						  description:description,
						  general_ajax_action:"moveto_clientreview"
					     };
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
					jQuery('.loader').hide();	
					jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
				}
		   });
});
/********************************* Review Admin Area jQuery ******************************************/
/*****************************************************************************************************/
/* Publish,hide,Delete review */
jQuery(document).on('click','.mp_review_phd',function(){
		var ajax_url = header_object.plugin_path;	
		jQuery('.loader').show();		
		var review_id = jQuery(this).data('review_id');
		var method = jQuery(this).data('method');
		var postdata = {  review_id:review_id,
						  method:method,
						  general_ajax_action:"publish_hide_delete_review"
					     };
		jQuery.ajax({
				url  : ajax_url+"/assets/lib/admin_general_ajax.php",
				type : 'POST',					
				data : postdata,					
				dataType : 'html',					
				success  : function(response) {
				jQuery('.loader').hide();	
				jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
					window.location.reload();
				}
		   });
});

/* Toggle Code */	
jQuery(document).ready(function(){
 jQuery("[data-toggle='toggle']").bootstrapToggle('destroy')                 
    jQuery("[data-toggle='toggle']").bootstrapToggle();
});
jQuery(document).ajaxComplete(function(){
 jQuery("[data-toggle='toggle']").bootstrapToggle('destroy')                 
    jQuery("[data-toggle='toggle']").bootstrapToggle();
});

/* Below Code For popup remove sample data */
jQuery(document).ready(function($){
	//open popup
	jQuery('.cd-popup-trigger').on('click', function(event){
		event.preventDefault();
		jQuery('.cd-popup').addClass('is-visible');
	});
	
	//close popup
	jQuery('.cd-popup').on('click', function(event){
		if( jQuery(event.target).is('.cd-popup-close') || jQuery(event.target).is('.cd-popup') ) {
			event.preventDefault();
			jQuery(this).removeClass('is-visible');
		}
	});
	//close popup when clicking the esc keyboard button
	jQuery(document).keyup(function(event){
    	if(event.which=='27'){
    		jQuery('.cd-popup').removeClass('is-visible');
	    }
    });
	
	jQuery(document).on('click','.remove_popup_sample_data',function(){
		jQuery('.cd-popup').removeClass('is-visible');
	});
});

jQuery(document).bind('ready ajaxComplete',function(){
	jQuery("#moveto_company_country_code").intlTelInput();
	jQuery(".staff_phone_number").intlTelInput({
		numberType: "polite",
		autoPlaceholder: "off",
		utilsScript: "utils.js"
	});
	 /* jQuery("#mp-phone").intlTelInput({'initialCountry':'nz'}); */
	
});

/* loader GIF upload */
 jQuery(document).ready(function(){

            jQuery(".moveto_frontend_loader_file").click(function(){

                var fd = new FormData();
                var files = jQuery('#file')[0].files[0];
                fd.append('file',files);
                jQuery.ajax({
                    url:ajax_url+"/assets/lib/admin_general_ajax.php",
                    type:'post',
                    data:fd,
                    contentType: false,
                    processData: false,
                    success:function(response){
                        if(response != 0){
                            jQuery("#img").attr("src",response);
                        }
                    },
                    error:function(response){
                        alert('error : ' + JSON.stringify(response));
                    }
                });
            });
        });

	/********************** Service Addon Form Validations ****************************/
jQuery(document).bind('ready ajaxComplete', function(){
	var ajax_url = header_object.plugin_path;	
	var article_msg = admin_validation_err_msg.article_msg;	
	var article_price_msg = admin_validation_err_msg.article_price_msg;	
	var article_number_price_msg = admin_validation_err_msg.article_number_price_msg;	
	
	jQuery('#mp_create_service_addon').validate({
		rules: {
			service_title: {
										required: true
				},
			service_maxqty: {
										required: true,
										number: true,
				}
			
			},
		messages: {
					service_title: { required:article_msg },
					service_maxqty:{ required:article_price_msg,
									 number:article_number_price_msg
									}	
			}
	});
});			
/* Update Service Addon Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var serviceaddontitle_err_msg = admin_validation_err_msg.serviceaddontitle_err_msg;	
	var serviceaddonmaxqty_err_msg = admin_validation_err_msg.serviceaddonmaxqty_err_msg;
	var serviceaddon_price_err_msg = admin_validation_err_msg.serviceaddon_price_err_msg;	
	var serviceaddon_validprice_err_msg = admin_validation_err_msg.serviceaddon_validprice_err_msg;
	
	
	jQuery.validator.addMethod("numeric_pattern", function(value, element) {
	return this.optional(element) || /^(?=.*[0-9])[- +()0-9]+$/.test(value);
	}, serviceaddon_validprice_err_msg);
	
		jQuery('.mp_update_service_addon').each(function(){
				jQuery(this).validate({
					rules: {
						u_service_title: {
													required: true,
							},
						service_maxqty: {			
													required: true
							}	
						
						},
					messages: {
								u_service_title: { required:serviceaddontitle_err_msg }, 
								service_maxqty: {required:serviceaddon_price_err_msg}
								
						}
				});
		});			
});		
	
/*** insert addons***/
/*** insert addons***/
 jQuery(document).on('click','#mp_create_service_addons',function(){
	if(jQuery('#mp_create_service_addon').valid()){
			///jQuery('.loader').show();
			 var ajax_url = header_object.plugin_path;
			 var article_title = jQuery('#mp-service-addons-title').val();
			/* var article_price = jQuery('#mp-service-addons-maxqty').val();*/
			 var article_img_service = jQuery('#bdscaduploadedimg').val();
			 var article_cubicfeets = jQuery('#mp-service-addons-cubicfeets').val();
			 var article_image_name = jQuery('.vehical_imageadd').data('image_name');
			 var article_max_qty = jQuery('#service_max_qty').val();
			
			   /*var per_flatfree = jQuery('#tax-vat-percentage').prop('checked');
			   if (per_flatfree == true) {
					var article_type = 'P';
				} else {
					var article_type = 'F';
				}*/
			
			  var addons_data = { 
							   article_title : article_title,
							  /* article_type : article_type,	*/						   
							   /*article_price : article_price,*/
							   article_cubicfeets : article_cubicfeets,
							   article_img_service : article_img_service,
							   article_image_name : article_image_name,
							   article_max_qty : article_max_qty,
							   action : "article_inst_data"
								 };
			 
			 jQuery.ajax({
				type : 'post',
				data : addons_data,
				url : ajax_url+"/assets/lib/service_ajax.php",
				success : function(res){
					jQuery('.mp-loa	ding-main').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					location.reload();
				}
			});  
	}
	 
 });
 /* Delete Addons Permanently */	
jQuery(document).on('click','.delete_addon',function(){
		var ajax_url = header_object.plugin_path;
		var addon_service_id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { addon_service_id:addon_service_id,
						 action:'delete_addons'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/service_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						location.reload();
						jQuery('.mp-locations-container').html(response);
						jQuery('.loader').hide();
						//jQuery('#location_detail_'+addon_service_id).fadeOut('slow');
						
					}
		});
});
 
 /*** update article status***/
 jQuery(document).on('change','.update_article_status',function(){
		jQuery('.loader').show();	
		var ajax_url = header_object.plugin_path;			
		var article_update_id = jQuery(this).data("id");
		if(jQuery(this).is(':checked')){
			var status = 'E';
		}else{
			var status =  'D';
		}
	  var addons_data = { 
					   article_update_id : article_update_id,						   
					   status : status,					  
					   action : "article_update_status"
					     };
	 jQuery.ajax({
        type : 'post',
        data : addons_data,
        url : ajax_url+"/assets/lib/service_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
			
        } 
    });   
 });
 /*Update article*/
 jQuery(document).on('click','.update_article_addon',function(){
		
			var article_update_id = jQuery(this).data("article_id");		
			if(!jQuery("#mp_update_service_addon_"+article_update_id).valid()){
					return false;
			}
			jQuery('.loader').show();	
			var ajax_url = header_object.plugin_path;	
		
			var article_title = jQuery('#mp-service-title'+article_update_id).val();
			/*var per_flatfree = jQuery('#calculation_type'+article_update_id).prop('checked');
			   if (per_flatfree == true) {
					var article_type = 'P';
				} else {
					var article_type = 'F';
				}*/
			
			/*var article_price = jQuery('#mp-service-addons-maxqty'+article_update_id).val();*/
			var article_cubicfeets = jQuery('#mp-service-addons-cubicfeets'+article_update_id).val();
			var article_image = jQuery('#bdscad'+article_update_id+'uploadedimg').val();
			var article_icon_name = jQuery('.vehical_image'+article_update_id).data('image_name');
			var article_max_qty = jQuery('#service_max_qty'+article_update_id).val();
			var article_data = { 
						   article_update_id : article_update_id,
						   article_title:article_title,
						   /*article_type:article_type,
						   article_price:article_price,*/
						   article_cubicfeets:article_cubicfeets,
						   article_image:article_image,
						   article_icon_name:article_icon_name,
						   article_max_qty:article_max_qty,
						   action : "addons_update"
							 };
		 
		 jQuery.ajax({
			type : 'post',
			data : article_data,
			url : ajax_url+"/assets/lib/service_ajax.php",
			success : function(res){
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
				location.reload();
			} 
		});  
 });
 /* Sort Service Position */
jQuery(document).ready(function(){		
			jQuery("#sortable-services").sortable({
				update : function(event,ui){
				var ajax_url = serviceObj.plugin_path;
				var position = jQuery(this).sortable('serialize');
				
				var postdata = {
				position : position,
				service_action :'sort_service_position'
				};
				jQuery('.loader').show();
				jQuery.ajax({
					url  : ajax_url+"/assets/lib/service_ajax.php",	
					type : 'POST',
					data : postdata,	
					success : function(response){
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					}
				});
				}
			});

	});	
	/********************** room_type Form Validations ****************************/
jQuery(document).ready(function(){
	var room_type_title = admin_validation_err_msg.room_type_title;	
	
	jQuery('#mp_create_room_type_frm').validate({
		rules: {
			room_type_title: {
						required: true
				}
			},
		messages: {
					room_type_title: { required:room_type_title }
			}
	});
});	
/* mp_create_room_type_frm Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var room_type_title = admin_validation_err_msg.room_type_title;	
	
		jQuery('.mp_create_room_type_frm').each(function(){
				jQuery(this).validate({
					rules: {
								room_type_title: {required: true,}
						},
					messages: {
								room_type_title: { required:room_type_title }
						}
				});
		});			
});	
/*** insert room_type***/
 jQuery(document).on('click','#mp_create_room_type',function(){
	if(jQuery('#mp_create_room_type_frm').valid()){
			jQuery('.loader').show();
			 var ajax_url = serviceObj.plugin_path;
			 var room_type_title = jQuery('#room_type_title').val();
			
			  var room_type_data = { 
							   room_type_title : room_type_title,
							   action : "room_type_data"
								 };
			 
			 jQuery.ajax({
				type : 'post',
				data : room_type_data,
				url : ajax_url+"/assets/lib/room_type_ajax.php",
				success : function(res){
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					location.reload();
				}
			});  
	}
	 
 });
/*Update room_type*/
 jQuery(document).on('click','.update_room_types',function(){
		
			var room_types_id = jQuery(this).data("update_room_types_id");		
			if(!jQuery("#mp_create_room_type_frm"+room_types_id).valid()){
					return false;
			}
			jQuery('.loader').show();	
			var ajax_url = serviceObj.plugin_path;	
		
			var room_types_title = jQuery('#room_types_title'+room_types_id).val();

			var article_cat_data = { 
						   room_types_id : room_types_id,
						   room_types_title:room_types_title,
						   action : "update_room_typess"
							 };		 
		 jQuery.ajax({
			type : 'post',
			data : article_cat_data,
			url : ajax_url+"/assets/lib/room_type_ajax.php",
			success : function(res){
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
				location.reload();
			} 
		});  
 });	
/* Delete room_type */	
jQuery(document).on('click','.delete_room_types',function(){
		var ajax_url = header_object.plugin_path;
		var room_types_id = jQuery(this).data('room_types_id');
		jQuery('.loader').show();
		var postdata = { room_types_id:room_types_id,
						 action:'delete_room_types'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/room_type_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						location.reload();
						jQuery('.mp-locations-container').html(response);
						jQuery('.loader').hide();
						//jQuery('#location_detail_'+addon_service_id).fadeOut('slow');
						
					}
		});
});	
 /*** update room_type status***/
 jQuery(document).on('change','.update_room_types_status',function(){
		jQuery('.loader').show();	
		var ajax_url = header_object.plugin_path;			
		var room_type_id = jQuery(this).data("room_t_id");
		if(jQuery(this).is(':checked')){
			var status = 'E';
		}else{
			var status =  'D';
		}
	  var room_type_data = { 
					   room_type_id : room_type_id,						   
					   status : status,					  
					   action : "room_type_update_status"
					     };
	 jQuery.ajax({
        type : 'post',
        data : room_type_data,
        url : ajax_url+"/assets/lib/room_type_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
			
        } 
    });   
 });
 /* validation of article_category data */
 jQuery(document).bind('ready ajaxComplete', function(){
	var article_category_title = admin_validation_err_msg.article_category_title;	
	
		jQuery('.mp_update_article_cat_').each(function(){
				jQuery(this).validate({
					rules: {
								article_category_title: {required: true,}
						},
					messages: {
								article_category_title: { required:article_category_title }
						}
				});
		});			
});	
jQuery(document).ready(function(){
	var article_category_title = admin_validation_err_msg.article_category_title;	
	
	/* var siteurl = header_object.site_url;	
	 */
		jQuery('#mp_update_article_cat_').validate({
			rules: {
				article_category_title: {required: true}
				},
			messages: {
						article_category_title: { required:article_category_title }	
			}
		});		
});
/*** insert article_category***/
 jQuery(document).on('click','#mp_create_article_category',function(){
	if(jQuery('#mp_update_article_cat_').valid()){
			jQuery('.loader').show();
			 var ajax_url = serviceObj.plugin_path;
			 var room_type_iid = jQuery('#room_type_iidd').val();
			
			 var category_title = jQuery('#article_category_title').val();
			
			  var room_type_data = { 
								room_type_iid : room_type_iid,
							    category_title : category_title,
							    action : "add_article_category"
								 };
			 
			 jQuery.ajax({
				type : 'post',
				data : room_type_data,
				url : ajax_url+"/assets/lib/category_article_ajax.php",
				success : function(res){
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					location.reload();
				}
			});  
	}
	 
 });
	
 /*Update article_category*/
 jQuery(document).on('click','.update_article_category',function(){
		
			var article_cat_id = jQuery(this).data("article_cat_id");		
			if(!jQuery("#mp_update_article_cat_"+article_cat_id).valid()){
					return false;
			}
			jQuery('.loader').show();	
			var ajax_url = serviceObj.plugin_path;	
		
			var article_cat_title = jQuery('#article_cat'+article_cat_id).val();
			
			var article_cat_data = { 
						   article_cat_id : article_cat_id,
						   article_cat_title:article_cat_title,
						   action : "update_article_cat"
							 };		 
		 jQuery.ajax({
			type : 'post',
			data : article_cat_data,
			url : ajax_url+"/assets/lib/category_article_ajax.php",
			success : function(res){
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
				location.reload();
			} 
		}); 
		
 });	
 /* Delete article_category Permanently */	
jQuery(document).on('click','.delete_category',function(){
		var ajax_url = header_object.plugin_path;
		var article_cat_id = jQuery(this).data('del_cat_id');
		jQuery('.loader').show();
		var postdata = { article_cat_id:article_cat_id,
						 action:'delete_category'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/category_article_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						location.reload();
						jQuery('.mp-locations-container').html(response);
						jQuery('.loader').hide();
						//jQuery('#location_detail_'+addon_service_id).fadeOut('slow');
						
					}
		});
});	
 /*** update article_category status***/
 jQuery(document).on('change','.update_article_cat_status',function(){
		jQuery('.loader').show();	
		var ajax_url = serviceObj.plugin_path;			
		var article_cat_id = jQuery(this).data("article_cat_id");
		if(jQuery(this).is(':checked')){
			var status = 'E';
		}else{
			var status =  'D';
		}
	  var room_type_data = { 
					   article_cat_id : article_cat_id,						   
					   status : status,					  
					   action : "article_cat_update_status"
					     };
	 jQuery.ajax({
        type : 'post',
        data : room_type_data,
        url : ajax_url+"/assets/lib/category_article_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();	
        } 
    });   
 });

jQuery(document).on('click','.update_s_addon',function(){
	jQuery('.loader').show();
	var ajax_url = serviceObj.plugin_path;	
	 var favorite = [];
            jQuery.each(jQuery("input[name='articlegroup']:checked"), function(){            
                favorite.push(jQuery(this).val());
            });
     var article_cat_id=jQuery('#ar_cat_id').val();
	 var selected_article=favorite.join(", ");
	 var article_cat_data = { 
					   article_cat_id : article_cat_id,						   
					   selected_article : selected_article,					  
					   action : "article_cat_up_us"
					     };
	 jQuery.ajax({
        type : 'post',
        data : article_cat_data,
        url : ajax_url+"/assets/lib/category_article_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();	
        } 
    }); 
 });
 

/*Code Start By AP_BI */

/* validation of distance data */
 
jQuery(document).ready(function(){
	var distance_msg = admin_validation_err_msg.distance_msg;	
	var distance_number_msg = admin_validation_err_msg.distance_number_msg;	
	var distance_price_msg = admin_validation_err_msg.distance_price_msg;	
	var distance_number_price_msg = admin_validation_err_msg.distance_number_price_msg;	
	/* var siteurl = header_object.site_url;	
	 */
		jQuery('#mp_insert_distance').validate({
			rules: {
				distance_di: {required: true, number : true},
				price_distance : {required: true, number : true}
				},
			messages: {
						distance_di: { required:distance_msg ,
						number : distance_number_msg }, 					
						price_distance: { required:distance_price_msg ,
						number : distance_number_price_msg}			
			}
		});		
});
 /* Insert Distance Data */						
 jQuery(document).on('click','#mp-add-new-distance',function(){
	if(jQuery('#mp_insert_distance').valid()){
			jQuery('.loader').show();
			 var ajax_url = header_object.plugin_path;
			 var distance = jQuery('#distance_di').val();
			 var moveto_distance_rule = jQuery('#moveto_distance_rule').val();
			 var price = jQuery('#price_distance').val();
			 var unit = jQuery('#unit_km').prop('checked');
			   if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
			  var distance_data = {	unit_val : unit_val,
							   		distance : distance,							   
							  		moveto_distance_rule : moveto_distance_rule,
							  		price : price,
							  	    action : "distance_insert_data"
								 };
			 jQuery.ajax({
				type : 'post',
				data : distance_data,
				url : ajax_url+"/assets/lib/distance_ajax.php",
				success : function(res){
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
				}
			});  
	}
	 
 });

 /* Update Distance Data*/

jQuery(document).on('click','#mp-update-distance',function(){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		var distance = jQuery('#distance_update'+id).val();
		var moveto_distance_rule = jQuery('#moveto_distance_rule_update'+id).val();
		var price = jQuery('#price_update'+id).val();
		var unit = jQuery('#unit_km').prop('checked');
		if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
		jQuery('.loader').show();
		var postdata = { id:id,
						 unit_val : unit_val,
						 distance:distance,
						 moveto_distance_rule:moveto_distance_rule,
						 price:price,
						 action:'update_distance'						 
						}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/distance_ajax.php",					
					type : 'POST',					
					data : postdata,										
					success  : function(res) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					}
		});
});  


/* validation of Floor */
 
jQuery(document).ready(function(){

	var floor_price_msg = admin_validation_err_msg.floor_price_msg;	
	var floor_number_price_msg = admin_validation_err_msg.floor_number_price_msg;	
	var floor_no_msg = admin_validation_err_msg.floor_no_msg;	
	var floor_no_valid_msg = admin_validation_err_msg.floor_no_valid_msg;	

		jQuery('#mp_insert_floor').validate({
			rules: {
				price: {required: true, number : true},
				
				floor_no : {required: true, number : true}
				},
			messages: {
						price: { required:floor_price_msg ,
						number : floor_number_price_msg },
						floor_no: { required:floor_no_msg , number : floor_no_valid_msg }	
			}
		});		
});
/* Insert Floor Data */

 jQuery(document).on('click','#mp-add-new-floor',function(){
	if(jQuery('#mp_insert_floor').valid()){
			jQuery('.loader').show();
			 var ajax_url = header_object.plugin_path;
			 var price = jQuery('#price').val();
			 var floor_no = jQuery('#floor_no').val();
			 var unit = jQuery('#unit_km').prop('checked');
			 if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
			 var floor_data = { price : price,
							    unit_val : unit_val,
							    floor_no : floor_no,
							    action : "floor_insert_data"
							   };
			 
			 jQuery.ajax({
				type : 'post',
				data : floor_data,
				url : ajax_url+"/assets/lib/distance_ajax.php",
				success : function(res){
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
				}
			});  
	}
	 
 });

 /*Update Floor Data*/

jQuery(document).on('click','#mp-update-floor',function(){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		var price = jQuery('#floor_price_update'+id).val();
		var floor_no = jQuery('#floor_no_update'+id).val();
		var unit = jQuery('#unit_km').prop('checked');
		if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
		jQuery('.loader').show();
		var postdata = { id:id,
						 unit_val : unit_val,
						 price:price,
						 floor_no:floor_no,
						 action:'update_floor'						 
						}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/distance_ajax.php",					
					type : 'POST',					
					data : postdata,										
					success  : function(res) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					}
		});
});  
/* validation of Elevator */
 
jQuery(document).ready(function(){

	var elevator_discount_msg = admin_validation_err_msg.elevator_discount_msg;	
	var elevator_number_price_msg = admin_validation_err_msg.elevator_number_price_msg;	

		jQuery('#mp_insert_elevator_data').validate({
			rules: {
				mp_elevator: {required: true, number : true}
				},
			messages: {
						mp_elevator: { required:elevator_discount_msg ,
						number : elevator_number_price_msg }		
			}
		});		
});
/* Insert Elevator Data*/

 jQuery(document).on('click','#mp_insert_elevator',function(){
	if(jQuery('#mp_insert_elevator_data').valid()){
			jQuery('.loader').show();
			 var ajax_url = header_object.plugin_path;
			 var elevator = jQuery('#mp-elevator').val();
			 var unit = jQuery('#unit_km').prop('checked');
			 if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
			 var elevator_data = {  elevator : elevator,
								    unit_val : unit_val,
								    action : "elevator_insert_data"
							     };
			 jQuery.ajax({
				type : 'post',
				data : elevator_data,
				url : ajax_url+"/assets/lib/distance_ajax.php",
				success : function(res){
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
				}
			});  
	}
	 
 });

/* Update Elevator Data*/

jQuery(document).on('click','#mp-update-elevator',function(){
	if(jQuery('#mp_insert_elevator_data').valid()){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		var discount = jQuery('#mp-elevator').val();
		var unit = jQuery('#unit_km').prop('checked');
		if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
		jQuery('.loader').show();
		var postdata = { id:id,
						 unit_val : unit_val,
						 discount:discount,
						 action:'update_elevator'						 
						}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/distance_ajax.php",					
					type : 'POST',					
					data : postdata,										
					success  : function(res) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					}
		});
	}
});  


/* validation of Packing/Unpacking  */
 
jQuery(document).ready(function(){
	var packing_msg = admin_validation_err_msg.packing_msg;	
	var packing_number_msg = admin_validation_err_msg.packing_number_msg;	
	var unpacking_price_msg = admin_validation_err_msg.unpacking_price_msg;	
	var unpacking_number_price_msg = admin_validation_err_msg.unpacking_number_price_msg;	
	
	/* var siteurl = header_object.site_url;	
	 */
		jQuery('#mp_insert_pac_unpac_data').validate({
			rules: {
				cost_packing: {required: true, number : true},
				cost_unpacking : {required: true, number : true}
				},
			messages: {
						cost_packing: { required:packing_msg ,
						number : packing_number_msg }, 					
						cost_unpacking: { required:unpacking_price_msg ,
						number : unpacking_number_price_msg}			
			}
		});		
});
/*Insert Packing/Unpacking Data*/

 jQuery(document).on('click','#mp-inset-pac_unpac',function(){
	if(jQuery('#mp_insert_pac_unpac_data').valid()){
			jQuery('.loader').show();
			 var ajax_url = header_object.plugin_path;
			 var cost_packing = jQuery('#cost_packing').val();
			 var cost_unpacking = jQuery('#cost_unpacking').val();
			 var unit = jQuery('#unit_km').prop('checked');
			 if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
			 var pac_unpac_data ={  cost_packing : cost_packing,
								    cost_unpacking : cost_unpacking,
								    unit_val : unit_val,
								    action : "pac_unpac_insert_data"
							  	 };
			 jQuery.ajax({
				type : 'post',
				data : pac_unpac_data,
				url : ajax_url+"/assets/lib/distance_ajax.php",
				success : function(res){
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
				}
			});  
	}
	 
 });

 /* validation of update Packing/Unpacking  */
jQuery(document).ready(function(){
	var packing_msg = admin_validation_err_msg.packing_msg;	
	var packing_number_msg = admin_validation_err_msg.packing_number_msg;	
	var unpacking_price_msg = admin_validation_err_msg.unpacking_price_msg;	
	var unpacking_number_price_msg = admin_validation_err_msg.unpacking_number_price_msg;	
	
	/* var siteurl = header_object.site_url;	
	 */
		jQuery('#mp_updatee_pac_unpac_data').validate({
			rules: {
				cost_packing: {required: true, number : true},
				cost_unpacking : {required: true, number : true}
				},
			messages: {
						cost_packing: { required:packing_msg ,
						number : packing_number_msg }, 					
						cost_unpacking: { required:unpacking_price_msg ,
						number : unpacking_number_price_msg}			
			}
		});		
});
 /*Update Packing/Unpacking Data*/

jQuery(document).on('click','#mp-update-pac_unpac',function(){
	if(jQuery('#mp_updatee_pac_unpac_data').valid()){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		var cost_packing = jQuery('#cost_packing').val();
		var cost_unpacking = jQuery('#cost_unpacking').val();
		var unit = jQuery('#unit_km').prop('checked');
		if (unit == true) {
					var unit_val = '1';
				} else {
					var unit_val = '2';
				}
		jQuery('.loader').show();
		var postdata = { id:id,
						 unit_val : unit_val,
						 cost_packing:cost_packing,
						 cost_unpacking:cost_unpacking,
						 action:'update_pac_unpac'						 
						}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/distance_ajax.php",					
					type : 'POST',					
					data : postdata,										
					success  : function(res) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					}
		});
		}
});  

/* Delete Distance  */	
jQuery(document).on('click','#mp-delete-distance',function(){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { id:id,
						 action:'delete_distance'						 
					   }
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/distance_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  

/* Delete Floor Data  */	
jQuery(document).on('click','#mp-delete-floor',function(){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { id:id,
						 action:'delete_floor'						 
					   }
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/distance_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
						location.reload();
					},
					error: function (xhr, ajaxOptions, thrownError) {
					}
		});
});  	

/*Code End By AP_BI */


 
	/* code end by bi */
	
	
	
	
	
 /* Get Selected Service Articles Info Click from Left Side */
jQuery(document).on('click','.mp_category_services',function(){
		var ajax_url = header_object.plugin_path;
		var service_id = jQuery(this).data('sid');
		var rso_service_id = jQuery('#addon_service_id').val();
		if(service_id!=rso_service_id){
			jQuery('.loader').show();
			
			var postdata = { service_id:service_id,
							 action:'get_articles_right'						 
			}
			jQuery.ajax({					
						url  : ajax_url+"/assets/lib/service_ajax.php",					
						type : 'POST',					
						data : postdata,					
						dataType : 'html',					
						success  : function(response) {
							jQuery('.loader').hide();
							jQuery('.mp-service-details').html(response);
						},
						error: function (xhr, ajaxOptions, thrownError) {
						}
			});
		}
});
 
 
 /* SAVE QTY RULE BUTTON */
jQuery(document).on('click', '.myloadedbtnsave_addons', function () {
	var ajax_url = header_object.plugin_path;
    var editid = jQuery(this).data('id');
		
    var id = jQuery(this).data('addon_service_id');
	
	if(!jQuery("#myedtform_addonunits"+editid).valid()){
			return false;
	}
	
    var qty = jQuery('.myloadedqty_addons' + editid).val();
    var rules = jQuery('.myloadedrules_addons' + editid).val();
    var price = jQuery('.myloadedprice_addons' + editid).val();
	jQuery('.loader').show();	
    var addon_updated_qty = {addon_service_id : id, addon_update_id : editid, unit : qty, rules : rules, rate : price,action : "addon_update_qty"}
	jQuery.ajax({
        type: 'post',
        data: addon_updated_qty,
        url: ajax_url + "/assets/lib/service_ajax.php",
        success: function (res) {
            jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
        }
    });
});

/* ADD NEW QTY RULE */
jQuery(document).on('click', '.mybtnaddnewqty_addon', function () {
	var ajax_url = header_object.plugin_path;
    var id = jQuery(this).data('id');
			
	if(!jQuery("#mynewaddedform_addonunits"+id).valid()){
			return false;
	}
	
	var qty = jQuery('.mynewqty_addons' + id).val();
	var rules = jQuery('.mynewrules_addons' + id).val();
    var price = jQuery('.mynewprice_addons' + id).val();	
	
	jQuery('.loader').show();	
	var addons_qty_data = {addon_service_id : id,unit : qty,rules : rules,rate : price,action : "add_new_qty"}
	jQuery.ajax({
        type : 'post',
        data : addons_qty_data,
        url : ajax_url+"/assets/lib/service_ajax.php",
        success : function(res){			
			var rulerefreshdata = {addon_id:id,action:'get_addon_pricing_rules'}
			jQuery.ajax({
				type : 'post',
				data : rulerefreshdata,
				url : ajax_url+"/assets/lib/service_ajax.php",
				success : function(res){
					jQuery('.myaddonspricebyqty'+id).html(res);
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();	
				}
			});			
        }
    });  
   
 });
 /* Delete Rule */
 jQuery(document).on('click', '.myloadedbtndelete_addons', function () {
	 var ajax_url = header_object.plugin_path;
	 var deleteid = jQuery(this).data('id');
	 
		jQuery('.loader').show();
		var postdata = { addon_service_id:deleteid,
						 action:'delete_addons_qty'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/service_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {						
						jQuery('.myaddon-qty_price_row'+deleteid).hide('slow');
						jQuery('.loader').hide();	
						jQuery('.mainheader_message_inner').show();
						moveto_hide_success_msg();
													
					}
		});
 });

 
jQuery(document).on('click','.verify_gc_account',function () {
	var redirect_link = jQuery(this).data('hreflink');
	window.open(redirect_link, '_self');
});

function ob_formatDate(date) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
}

jQuery(document).on('change','#mp_booking_datetime',function () {
	jQuery('.loader').show();
	var ajax_url = header_object.plugin_path;
	var selected_date = jQuery(this).val();
	
	var sel_date = ob_formatDate(selected_date);
	/* var provider_id = jQuery(this).data('selstaffid'); */
	var provider_id = jQuery('#edit-booking-details-view #mp_booking_provider').val();
	
	jQuery(this).attr('data-sel_date', sel_date);
	var getslots_data = {
		'seldate':sel_date,
		'selstaffid':provider_id,
		'action':'mp_get_provider_slots'
	};
	jQuery.ajax({
		type : 'POST',
		url  : ajax_url+"/assets/lib/mp_client_ajax.php",
		data : getslots_data,
		dataType : 'html',
		success  : function(res) {
			jQuery('#mp_booking_time').html(res);
			jQuery('#mp_booking_time').selectpicker('refresh');
			jQuery('.loader').hide();
		
		}
	});
});

/* Save GC Settings Start */

jQuery(document).on('click','#mp_save_gc_settings',function () {
	jQuery('.loader').show();
	var ajax_url = header_object.plugin_path;
	var appointup_gc_id = jQuery('.appointup_gc_id').val();
	var mp_gc_client_id = jQuery('#mp_gc_client_id').val();
	var mp_gc_client_secret = jQuery('#mp_gc_client_secret').val();
	var mp_gc_frontend_url = jQuery('#mp_gc_frontend_url').val();
	var mp_gc_admin_url = jQuery('#mp_gc_admin_url').val();
	if(jQuery('.gc_enable_disable').prop('checked')==true){
		var gc_enable_disable ='Y';
	} else{ 
		var gc_enable_disable='N';
	}
	if(jQuery('.appointup_gc_twowaysync').prop('checked')==true){
		var appointup_gc_twowaysync ='Y';
	} else{ 
		var appointup_gc_twowaysync='N';
	}
	var datastring = {
		'gc_enable_disable' : gc_enable_disable,
		'mp_gc_client_id' : mp_gc_client_id,
		'mp_gc_client_secret' : mp_gc_client_secret,
		'mp_gc_frontend_url' : mp_gc_frontend_url,
		'mp_gc_admin_url' : mp_gc_admin_url,
		'appointup_gc_twowaysync' : appointup_gc_twowaysync,
		'appointup_gc_id' : appointup_gc_id,
		'GC_settings' : 1
	};
	jQuery.ajax({
		type : 'POST',
		url  : ajax_url+"/assets/lib/setting_ajax.php",
		data : datastring,
		dataType : 'html',
		success  : function(res) {
			jQuery('.loader').hide();
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
		}
	});
});

/* Save GC Settings End */



jQuery(document).ready(function(){
	jQuery('#user-profile-booking-table').DataTable();
	jQuery('#user-all-bookings-details').DataTable();
	
});



/* Update Service Addon Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var additional_info_err_msg = admin_validation_err_msg.additional_info_err_msg;		
	var additional_info_unit_price_err_msg = admin_validation_err_msg.additional_info_unit_price_err_msg;		
	var additional_info_unit_price_digit_err_msg = admin_validation_err_msg.additional_info_unit_price_digit_err_msg;		
		jQuery('#mp_create_additional_info').validate({
					rules: {
						additional_info_title: {
								required: true
							},
						additional_price: {
								required: true,
								number : true
						}
						},
					messages: {
						additional_info_title: { required:additional_info_err_msg },
						additional_price: { required:additional_info_unit_price_err_msg , number:additional_info_unit_price_digit_err_msg }
					}
			});
});	

/* Create Additional Information */
jQuery(document).on('click','#create_additional_info',function(){
	var ajax_url = additional_infoObj.plugin_path;
		if(!jQuery('#mp_create_additional_info').valid()){
				return false;
		}
			var per_flatfree = jQuery('#tax-vatt-percentage').prop('checked');
			   if (per_flatfree == true) {
					var additional_type = 'P';
				} else {
					var additional_type = 'F';
				}
				
		var additional_price = jQuery('#mp-additional-price').val();
		var additional_info_title = jQuery('#additional_info_title').val();
		jQuery('.loader').show();
		var postdata = { additional_info_title:additional_info_title,
						 additional_info_status:'E',additional_type:additional_type,additional_price:additional_price,
						 additional_info_action:'create_additional_info'						 
		}
		jQuery.ajax({					
			url  : ajax_url+"/assets/lib/additional_info_ajax.php",					
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {				
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();					
				location.reload();				
			}
		});
}); 

/* Create Additional Information */
jQuery(document).on('click','.mp_update_additional_info',function(){		
		var addid = jQuery(this).data('addid');
		jQuery('#additional_info_utitle_err'+addid).hide();
		jQuery('#additional_info_price_err'+addid).hide();
		var ajax_url = additional_infoObj.plugin_path;
		var additional_info_utitle = jQuery('#additional_info_utitle'+addid).val();
		var additional_price = jQuery('#mp-additional-price'+addid).val();
		if(additional_info_utitle=='' &&  additional_price==''){
		jQuery('#additional_info_utitle_err'+addid).show();
		jQuery('#additional_info_price_err'+addid).show();
		}
		if(additional_info_utitle==''){
			jQuery('#additional_info_utitle_err'+addid).show();
				return false;
		}if(additional_price==''){
			jQuery('#additional_info_price_err'+addid).show();
				return false;
		}
		var per_flatfree = jQuery('#tax-vatt-percentage_update'+addid).prop('checked');
			   if (per_flatfree == true) {
					var additional_type = 'P';
				} else {
					var additional_type = 'F';
				}
				
		

		var additional_info_title = jQuery('#additional_info_title').val();
		
		jQuery('.loader').show();
		var postdata = { additional_info_title:additional_info_utitle,
						 additional_info_id:addid,additional_type:additional_type,additional_price:additional_price,
						 additional_info_action:'update_additional_info'						 
		}
		jQuery.ajax({					
			url  : ajax_url+"/assets/lib/additional_info_ajax.php",					
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {				
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();		
				location.reload();						
			}
		});
}); 

/* Change Status Additional Information */
jQuery(document).on('change','.update_addinfo_status',function(){		
		var addid = jQuery(this).data('id');
		
		var ajax_url = additional_infoObj.plugin_path;
		if(jQuery(this).is(':checked')){
			var additional_info_status = 'E';
		}else{
			var additional_info_status =  'D';
		}

		jQuery('.loader').show();
		var postdata = { additional_info_status:additional_info_status,
						 additional_info_id:addid,
						 additional_info_action:'update_additional_info_status'						 
		}
		jQuery.ajax({					
			url  : ajax_url+"/assets/lib/additional_info_ajax.php",					
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {				
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();				
			}
		});
});

/* Delete Additional Information */
jQuery(document).on('click','.delete_additional_info',function(){		
		var addid = jQuery(this).data('id');		
		var ajax_url = additional_infoObj.plugin_path;
		jQuery('.loader').show();
		var postdata = { additional_info_id:addid,
						 additional_info_action:'delete_additional_info'						 
		}
		jQuery.ajax({					
			url  : ajax_url+"/assets/lib/additional_info_ajax.php",					
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {				
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				jQuery('#service_detail_'+addid).hide('slow');
				moveto_hide_success_msg();				
			}
		});
});

/* Send Booking Quote */
/* Send Booking Quote */
jQuery(document).on('click','.pam_send_quote',function(){
	var booking_id = jQuery(this).data('id');
	jQuery('#pam_quote_booking_id').val(booking_id);
	var ajax_url = header_object.plugin_path;
		var postdata = { booking_id:booking_id,
						 action:'update_appointment_info_action'						 
		}
		jQuery.ajax({					
			url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {				
					jQuery('.update_appointment_res').html(response);
					
			}
		});
});


/* Validate Quote From */	
jQuery(document).bind('ready ajaxComplete', function(){
	var quoteprice_err_msg = admin_validation_err_msg.quoteprice_err_msg;	
	var quotepricevalid_err_msg = admin_validation_err_msg.quotepricevalid_err_msg;	
		
	jQuery('#pam_send_quote_form').validate({
		rules: {
				pam_quote_price: {
					required: true,
					number:true,
				}			
			},
		messages: {
				pam_quote_price: { required:quoteprice_err_msg,number:quotepricevalid_err_msg }
			}
	});		
});	
	
/* Submit Quote */	
jQuery(document).on('click','#pam_submit_quote',function(){
	jQuery('#pam_quote_price_err').hide();
	var ajax_url = header_object.plugin_path;
	var quote_price = jQuery(this).closest('form').find('input[name="pam_quote_price"]').val();
	var quote_description = jQuery(this).closest('form').find('#pam_quote_description').val();
	var booking_id = jQuery('#pam_quote_booking_id').val();
	var lead_date=jQuery('#mp_lead_date').val();
	var source=jQuery('#mp_source_mp').val();
	var via=jQuery('#mp_via_mp').val();
	var destination=jQuery('#mp_destination_mp').val();
	var loading_street_address=jQuery('#loading_street_address').val();
	var pam_loading_city=jQuery('#pam_loading_city').val();
	var pam_loading_state=jQuery('#pam_loading_state').val();
	var pam_unloading_street_address=jQuery('#pam_unloading_street_address').val();
	var pam_unloading_city=jQuery('#pam_unloading_city').val();
	var pam_unloading_state=jQuery('#pam_unloading_state').val();
	/* from */
	var pam_street_address_cab=jQuery('#pam_street_address_cab').val();
	var pam_city_cab=jQuery('#pam_city_cab').val();
	var pam_state_cab=jQuery('#pam_state_cab').val();
	var pam_member_cab=jQuery('#pam_member_cab').val();
	var pam_cab_date=jQuery('#to_cab_date').val();
	/* to */
	
	var to_pam_street_address_cab=jQuery('#to_pam_street_address_cab').val();
	var to_pam_city_cab=jQuery('#to_pam_city_cab').val();
	var to_pam_state_cab=jQuery('#to_pam_state_cab').val();
	
	var insurance_rate=jQuery('#insurance_rate').val();
	var cubic_feets_volume=jQuery('#cubic_feets_volume').val();
	

	/* var pam_first_name=jQuery('#pam_first_name').val();
	var pam_last_name=jQuery('#pam_last_name').val();
	var pam_email=jQuery('#pam_email').val();
	var pam_address=jQuery('#pam_address').val();
	var pam_zipcode=jQuery('#pam_zipcode').val();
	var pam_phone=jQuery('#pam_phone').val();
	var pam_notes=jQuery('#pam_notes').val();
	pam_first_name:pam_first_name,
	 pam_last_name:pam_last_name,
	 pam_email:pam_email,
	 pam_address:pam_address,
	 pam_zipcode:pam_zipcode,
	 pam_phone:pam_phone,
	 pam_notes:pam_notes,	*/
	
	
	if(jQuery('#pam_send_quote_form').valid()){	
		/*jQuery('.loader').show();*/
		var postdata = { booking_id:booking_id,
						 quote_price:quote_price,
						 quote_description:quote_description,
						 lead_date:lead_date,
						 source:source,
						 via:via,
						 destination:destination,
						 loading_street_address:loading_street_address,
						 pam_loading_city:pam_loading_city,
						 pam_loading_state:pam_loading_state,
						 pam_unloading_street_address:pam_unloading_street_address,
						 pam_unloading_city:pam_unloading_city,
						 pam_unloading_state:pam_unloading_state,
						 pam_street_address_cab:pam_street_address_cab,
						 pam_city_cab:pam_city_cab,
						 pam_state_cab:pam_state_cab,
						 pam_member_cab:pam_member_cab,
						 pam_cab_date:pam_cab_date,
						 to_pam_street_address_cab:to_pam_street_address_cab,
						 to_pam_city_cab:to_pam_city_cab,
						 to_pam_state_cab:to_pam_state_cab,
						 insurance_rate:insurance_rate,
						 cubic_feets_volume:cubic_feets_volume,
						 general_ajax_action:'pam_send_quote'				 
		}

		jQuery.ajax({					
			url  : ajax_url+"/assets/lib/admin_general_ajax.php",					
			type : 'POST',					
			data : postdata,					
			dataType : 'html',					
			success  : function(response) {	
				jQuery('.pam_quote_cancel').trigger('click');
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();					
				location.reload();					
			}
		});
	 } 
}); 
/* Get Destination Cities */	
 jQuery(document).on( "change",'#mp_source_mp', function() {	
	
	var ajaxurl = header_object.plugin_path;
	var source_city = jQuery(this).val();
	
	var city_type = jQuery(this).data('ct');	
	/* if(source_city==''){
		jQuery('#pam_sc_city_error').show();
	}else{
		jQuery('#pam_sc_city_error').hide();	
	} */
	
	if(source_city!=''){
		var dataString = { 
				'source_city':source_city,
				'city_type':city_type,				
				'action':'get_destination_city' 
			};
		jQuery('.loader').show();
		jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/admin_general_ajax.php",
			dataType : 'html',
			data:dataString,
			success:function(response){
				jQuery('.loader').hide();
				jQuery('#mp_destination_mp').html(response);
				jQuery('.selectpicker').selectpicker('refresh');
				jQuery('.destination_ct').hide();
			}
		});
	}
});	
/* jQuery(document).ready(function(){
      jQuery('.sendQuote').on('click', function(){ 
           jQuery("pam_send_quote").close();
      });
 }); */
/* jQuery(document).on('click','.sendQuote',function(){
	 jQuery("#pam_send_quote").fadeOut();
});
 */
 jQuery(document).ready(function() {
	jQuery('.ct-addition-btn').on('click', function() {
		var inputclass = jQuery(this).data('info');
		jQuery('.'+inputclass).val(parseInt(jQuery('.'+inputclass).val(), 10) + 1);
	});
	jQuery('.ct-subtraction-btn').on('click', function() {
		var inputclass = jQuery(this).data('info');
		if(parseInt(jQuery('.'+inputclass).val(), 10) - 1 <= 0){
			jQuery('.'+inputclass).val(0);
		}
		else {
			jQuery('.'+inputclass).val( parseInt(jQuery('.'+inputclass).val(), 10) - 1);
		}		
	});
});


/* movesize */
jQuery(document).bind('ready ajaxComplete', function(){
	var movesize_title = admin_validation_err_msg.movesize_title;	
	
	jQuery('#mp_create_movesize_frm').validate({
		rules: {
			movesize_title: {
						required: true
				}
			},
		messages: {
					movesize_title: { required:movesize_title }
			}
	});
});
/********************** move size Form Validations ****************************/

/* mp_create_movesize_frm Validation */
jQuery(document).bind('ready ajaxComplete', function(){
	var movesize_title = admin_validation_err_msg.movesize_title;	
	
		jQuery('.mp_create_movesize_frm').each(function(){
				jQuery(this).validate({
					rules: {
								movesize_title: {required: true,}
						},
					messages: {
								movesize_title: { required:movesize_title }
						}
				});
		});			
});	
/*** insert room_type***/
 jQuery(document).on('click','#mp_create_movesize',function(){
	if(jQuery('#mp_create_movesize_frm').valid()){
			jQuery('.loader').show();
			 var ajax_url = serviceObj.plugin_path;
			 var movesize_title = jQuery('#movesize_title').val();
			
			  var room_type_data = { 
							   movesize_title : movesize_title,
							   action : "move_size_data"
								 };
			 
			 jQuery.ajax({
				type : 'post',
				data : room_type_data,
				url : ajax_url+"/assets/lib/movesize_ajax.php",
				success : function(res){
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					location.reload();
				}
			});  
	}
	 
 });
/*Update room_type*/
 jQuery(document).on('click','.update_move_size',function(){
		
			var movesize_id = jQuery(this).data("update_move_sizes_id");		
			if(!jQuery("#mp_create_movesize_frm"+movesize_id).valid()){
					return false;
			}
			jQuery('.loader').show();	
			var ajax_url = serviceObj.plugin_path;	
		
			var movesize_title = jQuery('#movesize_title'+movesize_id).val();

			var article_cat_data = { 
						   movesize_id : movesize_id,
						   movesize_title:movesize_title,
						   action : "update_movesize"
							 };		 
		 jQuery.ajax({
			type : 'post',
			data : article_cat_data,
			url : ajax_url+"/assets/lib/movesize_ajax.php",
			success : function(res){
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
				location.reload();
			} 
		});  
 });	
/* Delete room_type */	
jQuery(document).on('click','.delete_movesize',function(){
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		jQuery('.loader').show();
		var postdata = { id:id,
						 action:'delete_movesize'						 
		}
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/movesize_ajax.php",					
					type : 'POST',					
					data : postdata,					
					dataType : 'html',					
					success  : function(response) {
						location.reload();
						jQuery('.mp-locations-container').html(response);
						jQuery('.loader').hide();
						//jQuery('#location_detail_'+addon_service_id).fadeOut('slow');
						
					}
		});
});	
 /*** update room_type status***/
 jQuery(document).on('change','.update_move_size_status',function(){
		jQuery('.loader').show();	
		var ajax_url = header_object.plugin_path;			
		var id = jQuery(this).data("room_t_id");
		if(jQuery(this).is(':checked')){
			var status = 'E';
		}else{
			var status =  'D';
		}
	  var room_type_data = { 
					   id : id,						   
					   status : status,					  
					   action : "movesize_update_status"
					     };
	 jQuery.ajax({
        type : 'post',
        data : room_type_data,
        url : ajax_url+"/assets/lib/movesize_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
			
        } 
    });   
 });
 
 
 
 /*** update room_type status***/
 jQuery(document).on('change','.update_move_size_status',function(){
		jQuery('.loader').show();	
		var ajax_url = header_object.plugin_path;			
		var id = jQuery(this).data("room_t_id");
		if(jQuery(this).is(':checked')){
			var status = 'E';
		}else{
			var status =  'D';
		}
	  var room_type_data = { 
					   id : id,						   
					   status : status,					  
					   action : "movesize_update_status"
					     };
	 jQuery.ajax({
        type : 'post',
        data : room_type_data,
        url : ajax_url+"/assets/lib/movesize_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();
			
        } 
    });   
 });

  /* Sort moveto Position */
jQuery(document).ready(function(){		
			jQuery("#sortable-movesize").sortable({
				update : function(event,ui){
				var ajax_url = serviceObj.plugin_path;
				var position = jQuery(this).sortable('serialize');
				
				var postdata = {
				position : position,
				service_action :'sort_movesize_position'
				};
				jQuery('.loader').show();
				jQuery.ajax({
					url  : ajax_url+"/assets/lib/movesize_ajax.php",	
					type : 'POST',
					data : postdata,	
					success : function(response){
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					moveto_hide_success_msg();
					
					}
				});
				}
			});

	});	

jQuery(document).ready(function(){
        jQuery('.distance_unit').change(function(){
        	var ajax_url = serviceObj.plugin_path;
            selected_value = jQuery("input[name='unit']:checked").val();

            var distance_unit_data = { 
		   	selected_value : selected_value,					  
		   	action : "update_distance_unit"
		     };
	     	jQuery('.loader').show();	
	 jQuery.ajax({
        type : 'post',
        data : distance_unit_data,
        url : ajax_url+"/assets/lib/distance_ajax.php",
        success : function(res){
			jQuery('.loader').hide();	
			/*jQuery('.mainheader_message_inner').show();
			moveto_hide_success_msg();*/
			location.reload();
        } 
    });   
        });
    });


jQuery(document).on('click','.image_click_li',function(){
var ajax_url = header_object.plugin_path;
var data_id = jQuery(this).attr("data-id");
var data_full_name = jQuery(this).attr("data-full_name");
var data_image_name = jQuery(this).attr("data-image_name");

jQuery(".vehical_image"+data_id).attr("src",ajax_url+"/assets/images/icons/article-icons/"+data_full_name);
jQuery(".vehical_image"+data_id).attr("data-image_name",data_full_name);
jQuery(".vehicle_name"+data_id).html(data_image_name);
});



/* Bhupendra Insert Cubic Feets */
 jQuery(document).on('click','#add_cfeets',function(){
 
 	var ajax_url = header_object.plugin_path;	
 	var per_cfeets = jQuery('#per_cfeets').val();
 	var per_cfeets_rate = jQuery('#per_cfeets_rate').val();
 	//var cubic_range_value = jQuery('#moveto_cubic_range_value').val();
 	if(per_cfeets == '')
 	{
 		jQuery(".erro_per_cfeets").text('Please Enter CubicFeets Range');
 	}
 	if(per_cfeets_rate == '')
 	{
 		jQuery(".erro_per_cfeets_rent").text('Please Enter CubicFeets Rent');
 	}
 	 
  
 	if(per_cfeets && per_cfeets_rate != '')
 	{
	 	jQuery('.loader').show();
	 	var cubicfeets_range_rate = { 
			   	per_cfeets : per_cfeets,
			   	per_cfeets_rate : per_cfeets_rate,
			  // 	cubic_range_value : cubic_range_value,				  
			   	action : "per_cubicfeets_range_price"
			     };
		jQuery.ajax({
	        type : 'post',
	        data : cubicfeets_range_rate,
	        url : ajax_url+"/assets/lib/cubicfeets_ajax.php",
	        success : function(response){
				jQuery('.loader').hide();
				jQuery('.mainheader_message_inner').show();
				location.reload();
	        } 
	    });   
	}
 });


 /* Update Cubic Feets Data*/

jQuery(document).on('click','#mp-update-cfeets',function(){
	jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		var per_cfeets = jQuery('#per_cfeets'+id).val();
		var per_cfeets_rate = jQuery('#per_cfeets_rate'+id).val();
	 	//var cubic_range_value = jQuery('#moveto_cubic_range_value'+id).val();
		var postdata = { id:id,
						 per_cfeets:per_cfeets,
						 per_cfeets_rate:per_cfeets_rate,
						// cubic_range_value : cubic_range_value,
						 action:'update_cubicfeets_range_price'						 
						};
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/cubicfeets_ajax.php",					
					type : 'POST',					
					data : postdata,										
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						location.reload();
					}
		});
}); 

/* Delete Cubic Feets Data  */	
jQuery(document).on('click','#mp-delete-cfeets',function(){
	jQuery('.loader').show();
		var ajax_url = header_object.plugin_path;
		var id = jQuery(this).data('id');
		var cubicfeets_id = { id:id,
						 action:'delete_cubicfeets_range'						 
					   };
		jQuery.ajax({					
					url  : ajax_url+"/assets/lib/cubicfeets_ajax.php",					
					type : 'POST',					
					data : cubicfeets_id,					
					success  : function(response) {
						jQuery('.loader').hide();
						jQuery('.mainheader_message_inner').show();
						location.reload();
					}
		});
});

jQuery(document).on('click','#mp-cubic-insert',function(){
	var ajax_url = header_object.plugin_path;
	var per_cubic_price = jQuery('#cubic_feets_range').val();
	var check = jQuery('#check').val();
	var identy_id = jQuery('#identify_id').val();
	jQuery('.loader').show();
	var cubicfeets_price = { price:per_cubic_price,
							 check:check,
							 identy_id:identy_id,
						 action:'cubicfeets_price'						 
					   };
	jQuery.ajax({					
				url  : ajax_url+"/assets/lib/cubicfeets_ajax.php",					
				type : 'POST',					
				data : cubicfeets_price,					
				success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					location.reload();
				}
		});
});

/* Moveto Min Rate */

jQuery(document).on('click','#mp-mover_price',function(){
	var ajax_url = header_object.plugin_path;
	var minrate = jQuery('#mp-mover_min_rate').val();
	var mover = {minrate:minrate,action:'mover_min_rate'};
	jQuery('.loader').show();
	jQuery.ajax({					
				url  : ajax_url+"/assets/lib/distance_ajax.php",					
				type : 'POST',					
				data : mover,					
				success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					location.reload();
				}
	});
});


jQuery(document).ready(function(){
        jQuery('.meter_cube').change(function(){
        	var ajax_url = serviceObj.plugin_path;
            selected_value = jQuery("input[name='cubic']:checked").val();
            var cubicvolume_set = { 
		   	selected_value : selected_value,					  
		   	action : "update_cubicvolume_set"
		     };
	     	jQuery('.loader').show();	
		 jQuery.ajax({
	        type : 'post',
	        data : cubicvolume_set,
	        url : ajax_url+"/assets/lib/distance_ajax.php",
	        success : function(res){
				jQuery('.loader').hide();	
				jQuery('.mainheader_message_inner').show();
				moveto_hide_success_msg();
				location.reload();
	        } 
	    });   
   });
});

jQuery(document).on('click','#mover_insurance_rate',function(){
	var ajax_url = header_object.plugin_path;
	var insurance_rate = jQuery('#mp-mover_insurance_rate').val();
	var mover = {insurance_rate:insurance_rate,action:'mover_insurance_rate'};
	jQuery('.loader').show();
	jQuery.ajax({					
				url  : ajax_url+"/assets/lib/distance_ajax.php",					
				type : 'POST',					
				data : mover,					
				success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					location.reload();
				}
	});
});


jQuery(document).on('click','.percentage_toggle',function(){
	var ajax_url = header_object.plugin_path;
	  if(jQuery(this).prop("checked") == true){
           var check_val = '1';
        }
        else if(jQuery(this).prop("checked") == false){
           var check_val = '0';
        }
	var permission = {check_val:check_val,action:'mover_insurance_permission'};
	jQuery('.loader').show();
	jQuery.ajax({					
				url  : ajax_url+"/assets/lib/distance_ajax.php",					
				type : 'POST',					
				data : permission,					
				success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					location.reload();
				}
	});
});


jQuery(document).on('click','.pricing_toggle',function(){
	var ajax_url = header_object.plugin_path;
	  if(jQuery(this).prop("checked") == true){
           var check_pricing = 'Y';
        }
        else if(jQuery(this).prop("checked") == false){
           var check_pricing = 'N';
        }
	var permission = {check_pricing:check_pricing,action:'move_pricing'};
	jQuery('.loader').show();
	jQuery.ajax({					
				url  : ajax_url+"/assets/lib/distance_ajax.php",					
				type : 'POST',					
				data : permission,					
				success  : function(response) {
					jQuery('.loader').hide();
					jQuery('.mainheader_message_inner').show();
					location.reload();
				}
	});
});



