<?php
class moveto_sms_template
{

    /* object properties */
    public $id;
	public $location_id;
    public $sms_subject;
    public $sms_template_name;
    public $sms_message;
    public $template_status;
    public $default_message;
    public $sms_parent_template;
    public $user_type;
	
	  
	
	/**
     * create moveto sms template table
     */ 
	function create_table() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_sms_templates';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name." (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `location_id` int(11) NOT NULL,
					  `sms_template_name` varchar(100) NOT NULL,
					  `sms_subject` varchar(100) NOT NULL,
					  `sms_message` text NOT NULL,
					  `default_message` text NOT NULL,
					  `sms_template_status` enum('e','d') NOT NULL,
					  `user_type` enum('AM','SP','C') NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	
	dbDelta($sql);     
			}
	} 
	
	/* Constructor Function to set the default values */
    function moveto_create_sms_template()
    {
        global $wpdb;
        
        $moveto_sms_templates = array(
            /* Admin/Manager sms template New Appointment Request Requires Approval */
            'New Quote Requests Requires Approval' => 'Hi {{admin_manager_name}}, <br /> You have a new Quote request as follows,<br />{{booking_detail}}<br /> Please proceed with confirm or reject action. <br /> Thank You, <br /> {{company_name}}',
           /* Admin/Manager sms template Appointment Reminder */
            'Admin Quote Requests Reminder' => 'Hi {{admin_manager_name}}, <br /><br /> This is a reminder notification for following Quote Requests<br />{{booking_detail}}  <br /> Thank You, <br /> {{company_name}}',
           /* Admin/Manager sms template Appointment Quote */
            'New Quote Requests' => 'Hi {{admin_manager_name}},<br /> You have successfully sent quote for following Quote Requests<br />Quote Estimate Price:{{quote_price}}<br /><br />Quote Detail:{{quote_detail}} <br/>  Thank You,<br /><br /> {{company_name}}',
		  
			
			/* customer sms template appointment Request */
			'Mover Request' => 'Hi {{customer_name}},<br /><br />You have successfully an Quote. Your Quote Requests details are noted below:<br/><br/>{{booking_detail}}<br/><br/>Please note that your mover is tentative and will be confirmed.<br/><br/>Regards,<br/>{{company_name}}',
			
			
			/* customer sms template appointment Qupte Sent */
            'Quote Request' => 'Hi {{customer_name}},<br /><br />Quote for you request is as follow: Order No. {{order_id}} Quote Details:<br />{{quote_price}}.<br/><br/>Regards,<br/>{{company_name}}');
			
           
			
                   $return = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_sms_templates");
        if (sizeof((array)$return) == 0) {           
        /* Inserting Email Template name in database */
		$sms_template_name_array=array('AA','RMA','QA','AC','QC','NAC');    
           
            $arrCounter = 0;
            foreach ($moveto_sms_templates as $option_key => $option_value) {
                if($arrCounter<=2){
					$usertype='AM';
				}elseif($arrCounter>=3 && $arrCounter<=5){
					$usertype='C';
				}						
				
                $return = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_sms_templates  SET sms_template_name ='" . $sms_template_name_array[$arrCounter] . "',sms_subject='" .$option_key. "',default_message='" . $option_value . "',user_type='".$usertype."'");
                $arrCounter++;
            }
            
            
        }
       
    }
    
	/**
	* Read All Email Templates
	* @return $return - db records objects
	*/
    function readAll()
    {
        global $wpdb;
        $return = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_sms_templates");
        return $return;
    }
    
	/**
	* Read One Email Template record
	* @return $return - db records objects
	*/
    function readOne()
    {
        global $wpdb;
        $return = $wpdb->get_results("SELECT sms_message,sms_subject,sms_template_status,default_message FROM  ".$wpdb->prefix."mp_sms_templates  WHERE  sms_template_name='" . $this->sms_template_name . "'");
        return $return;
    }
    
	
	/**
	* Update Email Template subject
	* @return $return - true on success,false on failure
	*/
	function update_template_subject_message()
    {
        global $wpdb;
        $return = $wpdb->query("UPDATE  ".$wpdb->prefix."mp_sms_templates  SET  sms_message  = '" . $this->sms_message . "' WHERE id  = '" . $this->id . "'");				
        if ($return) {
            return true;
        } else {
            return false;
        }
    }
    
	/**
	* Update template status
	* @return $return - true on success, false on failure
	*/
    function update_template_status()
    {
        global $wpdb;
			
        $return = $wpdb->query("UPDATE  ".$wpdb->prefix."mp_sms_templates  SET sms_template_status  = '".$this->template_status."' WHERE id  = '".$this->id."'");
        if ($return) {
            return true;
        } else {
            return false;
        }
    }
	
	/** 
	ReadAll Email Templates By User Type 
	**/
	function readall_by_usertype(){
		global $wpdb;
		global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_sms_templates where user_type='".$this->user_type."'");
        return $result;	
	}
	
	 function gettemplate_sms($usertype,$status,$template_name){
	  global $wpdb;
	  $return = $wpdb->get_results("SELECT 
	  sms_message,
	  sms_subject,
	  default_message,
	  sms_template_status FROM ".$wpdb->prefix."mp_sms_templates WHERE user_type='".$usertype."' and sms_template_name='".$template_name."'");
			return $return;
	 }
    
}
?>