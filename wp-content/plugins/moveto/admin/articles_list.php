<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	$article_category = new moveto_article_category();
	$article = new moveto_service();
	$general = new moveto_general();
	$mp_image_upload= new moveto_image_upload();
	$mp_currency_symbol = get_option('moveto_currency_symbol');
	$article->id = $_GET['siddd'];
	$article_category->id = $_GET['siddd'];
	$article_category->room_type_id = $_GET['cat_idd'];
	$room_types = $article_category->readone_roomm_type();
	$article_cat_name = $article_category->readone_roomm_type_art_cat_id();

?>
<div id="mp-services-panel" class="panel tab-content table-fixed">
<div id="mp-service-addon-panel" class="panel tab-content table-fixed">
<div class="panel-body">		
		<!-- ************************Article********************************** -->

		<div class="mp-service-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<ul class="breadcrumb">
                <li><a style="cursor:pointer"  href="?page=room_article_category&sid=<?php echo $_GET['cat_idd'];?>" class=""><?php echo ucfirst($room_types[0]->name); ?></a><li><a style="cursor:pointer"  href="?page=room_article_category&sid=<?php echo $_GET['siddd'];?>" class="" /></li><?php echo ucfirst($article_cat_name[0]->name); ?></a></li>
                <li><?php echo __("/ Articles","oct"); ?></li>
            </ul>
			<a href="?page=room_article_category&sid=<?php echo $_GET['cat_idd'];?>" class="btn btn-success"><i class="fa fa-angle-left icon-space "></i><?php echo __("Back To Article Category","mp");?></a>
			<div class="mp-service-top-header">
			
				<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
					<h2 class="m-0">Article</h2>
				</div>
				
			</div>
			<div id="hr"></div>
			<div class="tab-pane active" id="">
				<div class="tab-content mp-services-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-services" > <!-- sortable-services -->
						<input type="hidden" id="ar_cat_id" value="<?php echo $_GET['siddd'];?>" />
							<?php 
							
							$mp_article_category = $article_category->readAll_article_category_by_art();
							
							
							  $all_id=explode(',',$mp_article_category[0]->article_id);
							
							
							$mp_service_addons = $article->readAll_addonsss();
													
							if(sizeof((array)$mp_service_addons)>0){
							
							?>
							<?php
							foreach($mp_service_addons as $mp_addon){ 
								 $id_is=$mp_addon->id;
								 if($mp_addon->image != "" && $mp_addon->image != "/th_"){
								 	$image = site_url()."/wp-content/uploads".$mp_addon->image;
								 }elseif($mp_addon->predefinedimage != ""){
								 	$image = $plugin_url_for_ajax."/assets/images/icons/article-icons/".$mp_addon->predefinedimage;
								 }else{
								 	$image = $plugin_url_for_ajax."/assets/images/default.png";
								 }
							
							?>
							<div class="custom_article_checkbox">
								<input class = "get_article get my_vall" <?php if(in_array($id_is,$all_id)){echo "checked";}?> data-mydiv="<?php echo $mp_addon->id; ?>"  value="<?php echo $mp_addon->id; ?>" id="article_name<?php echo $mp_addon->id; ?>" name="articlegroup"  data-room_id="" data-id="" data-name="" data-price="" type="checkbox"  value="">
								
							
								<label for="article_name<?php echo $mp_addon->id; ?>" class="article_img">
									<img id="bdscad<?php echo $mp_addon->id; ?>addimage" src="<?php echo $image; ?>" class="mp-service-image" >
								</label>
								<div class="text-center article_text mt-10 custom_article_title"><?php echo $mp_addon->article_name; ?></div>
							</div>
							
     						<?php }}else{
								echo __("No Article Found.","mp");
							}if(sizeof((array)$mp_service_addons)>0){  ?>
								<div class="row article_save">
							<button  name="" class="btn btn-success mp-btn-width update_s_addon" type="button"><?php echo __("Save","mp");?></button>
						</div>
							<?php }?>
							</ul>
						</div>	
					</div>

				</div>
			</div>
			
		</div>
<!-- 		********************************************************** -->
	</div>	
</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var serviceObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>