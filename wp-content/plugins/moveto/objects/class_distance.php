<?php
class moveto_distance
{
    /* object properties */
    public $id;
		public $distance;
		public $rules;
		public $price;
		public $distance_id;
		public $type;
    public $discount;
    public $cost_packing;
    public $cost_unpacking;

		/**** Create Pricing Rules Table For Distance Menu****/ 

	function create_table() {
		global $wpdb;

		$table_name = $wpdb->prefix .'mp_pricing_rules';
		
		if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
		
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`distance` int(11) NOT NULL,
				`rules` enum('G','E') NOT NULL,
				`price` int(11) NOT NULL,
				`distance_id` int(11) NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		
		dbDelta($sql);     
				}
	}

	/**** Create Distance Table For Distance Menu****/
     
	function create_table_distance() {
		global $wpdb;

		$table_name = $wpdb->prefix .'mp_distance';

		if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
		
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`type` enum('K','M') NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		
		dbDelta($sql);     
				}
	}
	
  	 /**** Create Elevator Table For Distance Menu****/
   
  
	function create_table_elevator() {
		global $wpdb;

		$table_name = $wpdb->prefix .'mp_elevator';
		
		if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
		
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				`id` int(11) NOT NULL,
				`discount` double NOT NULL,
				`distance_id` int(11) NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		
		dbDelta($sql);     
				}
	}


	 /**** Create Packing/Unpacking Table For Distance Menu****/
    
	function create_table_pac_unpac() {
		global $wpdb;

		$table_name = $wpdb->prefix .'mp_packaging_unpackagin';
		
		if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
		
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`cost_packing` varchar(50) NOT NULL,
				`cost_unpacking` varchar(50) NOT NULL,
				`distance_id` int(11) NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		
		dbDelta($sql);     
				}
	}

 	/**** Create Floor Table For Distance Menu****/
    
	function create_table_floors() {
		global $wpdb;

		$table_name = $wpdb->prefix .'mp_floors';
		
		if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
		
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`price` int(11) NOT NULL,
				`distance_id` int(11) NOT NULL,
				`floor_no` int(11) NOT NULL,
				`status_pricing` enum('E','D') NOT NULL,
				
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		
		dbDelta($sql);     
				}
	}

	/*Insert Distance Data */

	function insert_distance(){
		global $wpdb;	
		$distance_insert = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (`distance`,`rules`,`price`,`distance_id`)values('".$this->distance."','".$this->moveto_distance_rule."','".$this->price."','".$this->unit."')");
		 return $distance_insert;
	}

	/*Read All Data For Update Distance*/

	function readAll(){
		global $wpdb;
		$read_distance = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_pricing_rules");	
		return $read_distance;
	}

	/*Update Distance Data*/

	function update_distance(){
		global $wpdb;
        $distance_update = $wpdb->query("UPDATE ".$wpdb->prefix."mp_pricing_rules SET distance='".$this->distance."',rules ='" . $this->moveto_distance_rule . "', price ='".$this->price . "',distance_id ='" . $this->unit . "' WHERE	id = " . $this->id);
	}

	/*Insert Floor Data */

	function insert_floor(){
		global $wpdb;	
		$floor_insert = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (`price`,`distance_id`,`floor_no`)values('".$this->price."','".$this->unit."','".$this->floor_no."')");
	     return $floor_insert;
	}

	/*Read All Data For Update Floor*/

	function readAll_floor(){
		global $wpdb;
		$read_floor = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_floors");	
		return $read_floor;
	}

	/*Update Floor Data*/

	function update_floor(){
		global $wpdb;
        $floor_update = $wpdb->query("UPDATE ".$wpdb->prefix."mp_floors SET price='".$this->price."',distance_id ='" . $this->unit . "',floor_no='".$this->floor_no."' WHERE	id = " . $this->id);
	}

	/*Insert Elevator Data */

	function insert_elevator(){
		global $wpdb;	
		$elevator_insert = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_elevator (`discount`,`distance_id`)values('".$this->elevator."','".$this->unit."')");
	    return $elevator_insert;
	}

	/*Read All Data For Update Elevator*/

	function readAll_elevator(){
		global $wpdb;
		
		$read_elevator = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_elevator");	
		return $read_elevator;
	}
																		
/*Read All Data For Packaging/Unpackaging*/

		function readAll_pack_unpack_data(){
			global $wpdb;
			
			$read_pack_unpack = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_packaging_unpackagin");	
			return $read_pack_unpack;
		}
	/*Update Elevator Data*/

	function update_elevator(){
		global $wpdb;
        $elevator_update = $wpdb->query("UPDATE ".$wpdb->prefix."mp_elevator SET discount='".$this->discount."',distance_id ='" . $this->unit . "' WHERE	id = " . $this->id);
	}

	/*Insert Packing/Unpacking Data */

	function insert_pac_unpac(){
		global $wpdb;	
		$pac_unpac_insert = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_packaging_unpackagin (`cost_packing`,`cost_unpacking`,`distance_id`)values('".$this->cost_packing."','".$this->cost_unpacking."','".$this->unit."')");
	     return $pac_unpac_insert;
	}

	/*Read All Data For Update Packing/Unpacking*/

	function readAll_pac_unpac(){
		global $wpdb;
		$read_pac_unpac = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_packaging_unpackagin");	
		return $read_pac_unpac;
	}

	/*Update Packing/Unpacking Data*/

	function update_pac_unpac(){
		global $wpdb;
        $pac_unpac_update = $wpdb->query("UPDATE ".$wpdb->prefix."mp_packaging_unpackagin SET cost_packing='".$this->cost_packing."',cost_unpacking='".$this->cost_unpacking."',distance_id ='" . $this->unit . "' WHERE	id = " . $this->id);
	}

	/*Delete Distance Data*/

	function delete_distance()
    {
        global $wpdb;
        $return = $wpdb->query("DELETE FROM  ".$wpdb->prefix."mp_pricing_rules  WHERE id =" . $this->id);
    }

    /*Delete Floor Data*/

    function delete_floor()
    {
        global $wpdb;
        $return = $wpdb->query("DELETE FROM  ".$wpdb->prefix."mp_floors  WHERE id =" . $this->id);
    }

    /*Get Additional Info FOr Booking Page*/
    function readAll_additional_info()
    {
		global $wpdb;
		$read_info = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_additional_info WHERE `status` = 'E'");	
		return $read_info;
	}

	function get_distance()
	{
		global $wpdb;
		$distance_info = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_pricing_rules ORDER BY `distance` DESC" );
		return $distance_info;
	}
	function update_distance_unit()
	{
		/*update_option('moveto_distance_unit',$this->distance_unit);*/
		$distance_unit_ = get_option('moveto_distance_unit');
		if(isset($distance_unit_)){
	    update_option('moveto_distance_unit', $this->distance_unit);
	    }else{
	    add_option('moveto_distance_unit', $this->distance_unit);
	    } 
		
	}
	/* Bhupendra */
	function update_cubic_volume()
	{
		$cubic = get_option('moveto_cubic_meter');
		if(isset($cubic)){
	    update_option('moveto_cubic_meter', $this->cubic_unit);
	    }else{
	    add_option('moveto_cubic_meter', $this->cubic_unit);
	    } 
		
	}

	function insurance_rate()
	{
		$insurance = get_option('moveto_insurance_rate');
		if(isset($insurance)){
	    update_option('moveto_insurance_rate', $this->insurance_rate);
	    }else{
	    add_option('moveto_insurance_rate', $this->insurance_rate);
	    } 
		
	}

	function insurance_permission()
	{
		$insurance_permission = get_option('moveto_insurance_permission');
		if(isset($insurance_permission)){
	    update_option('moveto_insurance_permission', $this->check_val);
	    }else{
	    add_option('moveto_insurance_permission', $this->check_val);
	    } 
		
	}
	
	
	
} 
?>