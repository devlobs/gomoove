var card, stripe;
var directionsDisplay;
var directionsService;
var strictBounds;
var total_distance = 0;
var distanceCTO = 0;
var distanceOTD = 0;
var finalDistance = 0;
var options;
var blur_call = true;
var autocompleteSource;
var autocompleteVia;
var autocompleteDestination;
jQuery(document).ready(function () {
    if (moveto_google_api_key != '') {
        directionsService = new google.maps.DirectionsService();
        var options = {
            componentRestrictions: {country: [moveto_google_map_country]}
        };
        if (moveto_google_map_country == "") {
            options = {
                componentRestrictions: {country: []}
            };
        }
        jQuery('#dvMap').show();
        var address = moveto_google_map_center_restrictions;
        var latititude = '';
        var longitude = '';
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'address': address}, function (results, status) {
            var location = results[0].geometry.location;
            latititude = location.lat();
            longitude = location.lng();
            strictBounds = results[0].geometry.bounds;
        });
        google.maps.event.addDomListener(window, 'load', function () {
            autocompleteSource = new google.maps.places.Autocomplete(document.getElementById('source_city'), options);
            autocompleteVia = new google.maps.places.Autocomplete(document.getElementById('via_city'), options);
            autocompleteDestination = new google.maps.places.Autocomplete(document.getElementById('destination_city'), options);

            directionsDisplay = new google.maps.DirectionsRenderer({'draggable': false});
            var mapCenter = new google.maps.LatLng(latititude, longitude);
            var mapOptions = {
                zoom: 5,
                clickableIcons: false,/*

		    	restriction: {
	            latLngBounds: strictBounds,
	            strictBounds: false,
	          },*/
                center: mapCenter,
                mapTypeControl: false,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                    mapTypeIds: ['roadmap', 'terrain']
                },
            };

            map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
            map.setOptions({
                draggable: true,
                zoomControl: true,
                scrollwheel: false,
                disableDoubleClickZoom: false,
                streetViewControl: false
            });
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('dvPanel'));
        });
    }
});

function GetRoute(sourceCity, viaCity, destinationCity, visibility) {
    jQuery('.loader').show();
    var source, destination, via;
    total_distance = 0;
    distanceCTO = 0;
    distanceOTD = 0;
    finalDistance = 0;
    var waypts = [];
    var ajaxurl = mpmain_obj.plugin_path;
    var service = new google.maps.DistanceMatrixService();
    if (moveto_company_origin_destination_distance_status == "E") {
        service.getDistanceMatrix({
            origins: [moveto_company_origin_address],
            destinations: [sourceCity],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function (response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS" && response.rows[0].elements[0].status != "NOT_FOUND") {
                if (moveto_distance_unit == 'Km') {
                    distanceCTO = Math.round(response.rows[0].elements[0].distance.value / 1000);
                } else {
                    distanceCTO = Math.round(response.rows[0].elements[0].distance.value / 1609.344);
                }
            } else {
                distanceCTO = 0;
            }
        });
        if (moveto_destination_company_distance == "E") {
            service.getDistanceMatrix({
                origins: [destinationCity],
                destinations: [moveto_company_destination_address],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS" && response.rows[0].elements[0].status != "NOT_FOUND") {
                    if (moveto_distance_unit == 'Km') {
                        distanceOTD = Math.round(response.rows[0].elements[0].distance.value / 1000);
                    } else {
                        distanceOTD = Math.round(response.rows[0].elements[0].distance.value / 1609.344);
                    }
                } else {
                    distanceOTD = 0;
                }

            });
        }
    }

    //*********DIRECTIONS AND ROUTE**********************//
    for (var i = 0; i < viaCity.length; i++) {
        waypts.push({
            location: viaCity[i],
            stopover: true
        });
    }
    var request = {
        origin: sourceCity,
        destination: destinationCity,
        waypoints: waypts,
        travelMode: google.maps.TravelMode.DRIVING,
        avoidHighways: false,
        avoidTolls: false,
    };
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);

            for (var i = 0; i < response.routes[0].legs.length; i++) {
                if (moveto_distance_unit == 'Km') {
                    total_distance += Math.round(response.routes[0].legs[i].distance.value / 1000);
                } else {
                    total_distance += Math.round(response.routes[0].legs[i].distance.value / 1609.344);
                }
            }


            if (total_distance != undefined && total_distance != 0) {
                jQuery('.error_invalid_cities').html("");
                jQuery(".loader").hide();
                jQuery('.range-slider__value').html(total_distance);
                jQuery('.range-slider__range').attr("value", jQuery.trim(total_distance));
                showSliderValue();
                jQuery('#range_counter').prop('disabled', true);
                jQuery('.loader').hide();
            } else if (jQuery.trim(total_distance) == '0') {
                distanceCTO = 0;
                distanceOTD = 0;
                jQuery('.error_invalid_cities').html("Please enter valid locations");
                jQuery(".loader").hide();
                jQuery('.range-slider__value').html(total_distance);
                jQuery('.range-slider__range').attr("value", jQuery.trim(total_distance));
                showSliderValue();
                jQuery('.loader').hide();
            }

            finalDistance = distanceCTO + total_distance + distanceOTD;
            var postdata = {finalDistance: finalDistance, action: 'set_distance_session'};
            jQuery.ajax({
                type: "POST",
                async: false,
                url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                dataType: 'html',
                data: postdata,
                success: function (data) {
                },
                error: function (data) {
                    console.log(data);
                }
            });
        } else {

            if (visibility == true) {
                distanceCTO = 0;
                distanceOTD = 0;
                if (jQuery.trim(viaCity) != '' && jQuery.trim(sourceCity) != '' && jQuery.trim(destinationCity) != '') {
                    jQuery('.error_invalid_cities').html("Please enter valid locations");
                    jQuery('.range-slider__value').html(total_distance);
                    jQuery('.range-slider__range').attr("value", jQuery.trim(total_distance));
                    showSliderValue();
                    directionsDisplay.setDirections(response);
                    jQuery('.loader').hide();
                } else {
                    total_distance = 0;
                    jQuery('.error_invalid_cities').html("");
                    jQuery('.range-slider__value').html(total_distance);
                    jQuery('.range-slider__range').attr("value", jQuery.trim(total_distance));
                    showSliderValue();
                    directionsDisplay.setDirections(response);
                    jQuery('.loader').hide();
                }
            } else {
                distanceCTO = 0;
                distanceOTD = 0;
                if (jQuery.trim(sourceCity) != '' && jQuery.trim(destinationCity) != '') {
                    jQuery('.error_invalid_cities').html("Please enter valid locations");
                    jQuery('.range-slider__value').html(total_distance);
                    jQuery('.range-slider__range').attr("value", jQuery.trim(total_distance));
                    showSliderValue();
                    directionsDisplay.setDirections(response);
                    jQuery('.loader').hide();

                } else {
                    total_distance = 0;
                    jQuery('.error_invalid_cities').html("");
                    jQuery('.range-slider__value').html(total_distance);
                    jQuery('.range-slider__range').attr("value", jQuery.trim(total_distance));
                    showSliderValue();
                    directionsDisplay.setDirections(response);
                    jQuery('.loader').hide();
                }
            }
            finalDistance = distanceCTO + total_distance + distanceOTD;
            var postdata = {finalDistance: finalDistance, action: 'set_distance_session'};
            jQuery.ajax({
                type: "POST",
                async: false,
                url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                dataType: 'html',
                data: postdata,
                success: function (data) {
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });

}

(function () {
    var $point_arr, $points, $progress, $trigger, active, max, tracker, val;

    $trigger = jQuery('.trigger').first();
    $points = jQuery('.progress-points').first();
    $point_arr = jQuery('.progress-point');
    $progress = jQuery('.progress').first();

    val = +$points.data('current') - 1;
    max = $point_arr.length - 1;
    tracker = active = 0;

    function activate(index) {
        if (index !== active) {
            active = index;
            var $_active = $point_arr.eq(active)

            $point_arr
                .removeClass('completed active')
                .slice(0, active).addClass('completed')

            $_active.addClass('active');

            return $progress.css('width', (index / max * 100) + "%");
        }
    };

    $points.on('click', 'li', function (event) {
        var index;
        _index = $point_arr.index(this);
        tracker = index === 0 ? 1 : index === val ? 0 : tracker;

        return activate(_index);
    });

    $trigger.on('click', function () {
        return activate(tracker++ % 2 === 0 ? 0 : val);
    });

    /* setTimeout((function() {
return activate(val);
}), 100);*/

}).call(this);


/* rangeSlider();*/
var rangeSlider = document.getElementById("range_counter");
var rangeBullet = document.getElementById("span_counter");

rangeSlider.addEventListener("input", showSliderValue, false);

function showSliderValue() {
    jQuery('.range-slider__value').html(rangeSlider.value);
    jQuery('.range-slider__range').attr("value", jQuery.trim(rangeSlider.value));
    rangeBullet.innerHTML = rangeSlider.value;
    var bulletPosition = (rangeSlider.value / rangeSlider.max);
    rangeBullet.style.left = (bulletPosition * 83.8) + "%";
}

/*  plus minus */
jQuery(document).ready(function () {

    var stripe_pubkey = mp_stripeObj.pubkey;
    if (mp_stripeObj.status_check == "E") {
        stripe = Stripe(stripe_pubkey);
        var elements = stripe.elements();
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
        card = elements.create('card', {style: style, hidePostalCode: true});
        card.mount('#card-element');
    }

    /* jQuery('ul.sideways').first().find("li.room_type_cat").trigger("click"); */
    jQuery("#first_cat").trigger("click");
    jQuery("#first_cat").addClass("active");

    jQuery("#date").change(function () {
        if (jQuery("#date").val().length > 0) {
            jQuery("#front-datepicker .fancy_label").addClass("focused_label");
        } else {
            jQuery("#front-datepicker .fancy_label").removeClass("focused_label");
        }
    });
});

/*Add via code by Anuj*/
jQuery(document).ready(function () {
    jQuery('#add_via').on("click", function () {
        jQuery('#via').toggle();
        if (jQuery("#btn_plus").is(":visible")) {
            jQuery("#btn_plus").css('display', 'none');
            jQuery("#btn_minus").css('display', 'inline-block');
        } else {
            jQuery("#btn_minus").css('display', 'none');
            jQuery("#btn_plus").css('display', 'inline-block');
        }

        var ajaxurl = mpmain_obj.plugin_path;
        var source_city = jQuery("#source_city").val();
        var via = [];
        var visibility = false;
        var destination_city = jQuery("#destination_city").val();
        if (jQuery("#via").is(":visible")) {
            via.push(jQuery("#via_city").val());
            visibility = true;
        }
        if (moveto_google_api_key != '') {
            res = GetRoute(source_city, via, destination_city, visibility);
        }

    });
});

/*code by Anuj ends*/

/* Front page Form Steps show/hide */

setTimeout(function () {
    jQuery('#source_city, #destination_city,#via_city').on('blur', function () {

        var res = '';
        var ajaxurl = mpmain_obj.plugin_path;
        var source_city = jQuery("#source_city").val();
        var via = [];
        var visibility = false;
        var destination_city = jQuery("#destination_city").val();
        if (jQuery("#via").is(":visible")) {
            via.push(jQuery("#via_city").val());
            visibility = true;
        }
        if (moveto_google_api_key != '') {
            res = GetRoute(source_city, via, destination_city, visibility);

        }
    });
}, 4000);
jQuery(".loader").show();
jQuery(window).load(function () {
    // Animate loader off screen

    jQuery(".loader").hide(10);
});

/* Front page Form Steps show/hide */
jQuery(document).ready(function () {

    jQuery('#btn-more-bookings').on("click", function () {
        jQuery('#mp_first_step').addClass('show-data');
        jQuery('#mp_first_step').removeClass('hide-data');
        jQuery('#mp_second_step').addClass('hide-data');
        jQuery('#mp_second_step').removeClass('show-data');
        jQuery('#mp_third_step').addClass('hide-data');
        jQuery('#mp_third_step').removeClass('show-data');
    });
    jQuery('.mp-cart-items-count').on("click", function () {
        jQuery('#mp_first_step').addClass('hide-data');
        jQuery('#mp_first_step').removeClass('show-data');
        jQuery('#mp_second_step').addClass('show-data');
        jQuery('#mp_second_step').removeClass('hide-data');
    });

});


/* scroll to top when on second step */
jQuery(document).ready(function () {
    jQuery('#first_step_submit,#first_step_progress, #second_step_progress, #second_step_submit, #forth_step_progress,#forth_step_submit, #third_step_progress').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 80
        }, 800, 'swing', function () {
        });
    });
});


jQuery(document).ready(function () {
    if (jQuery("#mp").width() >= 380 && jQuery("#mp").width() < 600) {
        jQuery(".mp-main-left").addClass("active-left-xs12");
        jQuery(".mp-main-right").addClass("active-right-xs12");
    }
    if (jQuery("#mp").width() >= 601 && jQuery("#mp").width() < 850) {
        jQuery(".mp-main-left").addClass("active-left-res100");
        jQuery(".mp-main-right").addClass("active-right-res57");
    }

    if (!jQuery('.mp_remove_left_sidebar_class').hasClass("no-sidebar-right")) {
        jQuery('.mp_remove_left_sidebar_class').addClass('mp-asr');
    }
    if (!jQuery('.mp_remove_right_sidebar_class').hasClass("no-cart-item-sidebar")) {
        jQuery('.mp_remove_right_sidebar_class').addClass('mp-cis');
    }
});


/* Booking summary delete extra service NS */
jQuery(document).ready(function () {
    jQuery(document).on("click", ".mp-delete-icon", function () {
        if (jQuery('.mp-es').hasClass('delete-toggle')) {
            jQuery(".mp-es").removeClass('delete-toggle');
        }
        jQuery(this).parent(".mp-es").addClass('delete-toggle');
    });
    jQuery(document).on("click", ".mp-delete-confirm", function () {
        jQuery(this).parent(".mp-es").slideUp();
    });

    /* Booking summary delete booking full list */
    jQuery(document).on("click", ".mp-delete-booking", function () {
        if (jQuery('.booking-list').hasClass('delete-list')) {
            jQuery(".booking-list").removeClass('delete-list');
        }
        jQuery(this).parent(".booking-list").addClass('delete-list');
    });
    jQuery(document).on("click", ".mp-delete-booking-box", function () {
        jQuery(this).parent(".booking-list").slideUp();
    });

    /* Remove delete booking button on ESC key */
    jQuery(document).on('keydown', function (e) {
        if (e.keyCode === 27) {
            jQuery(".booking-list").removeClass('delete-list');
            jQuery(".mp-es").removeClass('delete-toggle');
        }
    });


});
jQuery(document).ready(function () {
    jQuery('.mp-slots-count').tooltipster({
        animation: 'grow',
        delay: 10,
        side: 'top',
        theme: 'tooltipster-shadow',
        trigger: 'hover'
    });
});


/* custom dropdown show hide list */

jQuery(document).ready(function () {


    /* Location */
    jQuery(document).on("click", ".select-location", function () {
        jQuery(".service-selection").removeClass('clicked');
        jQuery(".service-dropdown").removeClass('bounceInUp');
        jQuery(".staff-selection").removeClass('clicked');
        jQuery(".staff-dropdown").removeClass('bounceInUp');

        jQuery(".cus-location").addClass('focus');
        jQuery(".location-selection").toggleClass('clicked');
        jQuery(".location-dropdown").toggleClass('bounceInUp');

    });
    jQuery(document).on("click", ".select-location-clone", function () {

        jQuery(".service-selection").removeClass('clicked');
        jQuery(".service-dropdown").removeClass('bounceInUp');
        jQuery(".staff-selection").removeClass('clicked');
        jQuery(".staff-dropdown").removeClass('bounceInUp');

        jQuery(".cus-location-clone").addClass('focus');
        jQuery(".location-selection-clone").toggleClass('clicked');
        jQuery(".location-dropdown-clone").toggleClass('bounceInUp');
    });

    jQuery(document).on("click", ".select_location", function () {
        jQuery('#bungalow_unloading_type').val(jQuery(this).find('.mp-value').text());
        jQuery('#selected_location').html(jQuery(this).html());
        jQuery(".location-selection").removeClass('clicked');
        jQuery(".location-dropdown").removeClass('bounceInUp');
    });
    jQuery(document).on("click", ".select_location-clone", function () {
        jQuery('#bungalow_loading_type').val(jQuery(this).find('.mp-value').text());
        jQuery('#selected_location-clone').html(jQuery(this).html());
        jQuery(".location-selection-clone").removeClass('clicked');
        jQuery(".location-dropdown-clone").removeClass('bounceInUp');
    });

    /* Car Type */
    jQuery(document).on("click", ".select-location-cartype_sig", function () {
        jQuery(".cus-location-cartype_sig").addClass('focus');
        jQuery(".location-selection-cartype_sig").addClass('clicked');
        jQuery(".location-dropdown-cartype_sig").addClass('bounceInUp');

    });

    jQuery(document).on("click", ".select_location-cartype_sig", function () {
        jQuery('#vehicle_service_car_type').val(jQuery(this).find('.mp-value').text());
        jQuery(".cus-location-cartype_sig").removeClass('focus');
        jQuery(".location-selection-cartype_sig").removeClass('clicked');
        jQuery(".location-dropdown-cartype_sig").removeClass('bounceInUp');
        jQuery('#selected_location-cartype').html(jQuery(this).html());
    });
    /* Service */
    jQuery(document).on("click", ".select-custom", function () {
        jQuery(".staff-selection").removeClass('clicked');
        jQuery(".staff-dropdown").removeClass('bounceInUp');
        jQuery(".location-selection").removeClass('clicked');
        jQuery(".location-dropdown").removeClass('bounceInUp');

        jQuery(".cus-select").addClass('focus');
        jQuery(".service-selection").toggleClass('clicked');
        jQuery(".service-dropdown").toggleClass('bounceInUp');
    });
    jQuery(document).on("click", ".select_custom", function () {
        jQuery(".service-selection").removeClass('clicked');
        jQuery(".service-dropdown").removeClass('bounceInUp');
    });

    /* Addon service counting */
    jQuery(function () {
        jQuery('#add').on('click', function () {
            var $qty = jQuery(this).closest('.mp-btn-group').find('.addon_qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal)) {
                $qty.val(currentVal + 1);
            }
        });
        jQuery('#minus').on('click', function () {
            var $qty = jQuery(this).closest('.mp-btn-group').find('.addon_qty');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                $qty.val(currentVal - 1);
            }
        });
    });
});
/* Calendar click date to show slots */
jQuery(document).ready(function () {

    jQuery(document).on('ready ajaxComplete', function () {
        var allowed_country_alpha_code = mpmain_obj.moveto_selected_flags;
        if (allowed_country_alpha_code != "") {
            var array_code = allowed_country_alpha_code.split(',');
            array_code_length = array_code.length;
            if (array_code_length == 1) {
                jQuery("#mp-front-phone").intlTelInput({
                    onlyCountries: array_code,
                    separateDialCode: true,
                    utilsScript: "js/utils.js"
                });
                jQuery('.iti-arrow').hide();
                jQuery('.country-list').hide();
            } else {
                jQuery("#mp-front-phone").intlTelInput({
                    onlyCountries: array_code,
                    separateDialCode: true,
                    utilsScript: "js/utils.js"
                });
            }
        } else {
            jQuery("#mp-front-phone").intlTelInput({
                separateDialCode: true,
                utilsScript: "js/utils.js"
            });
        }
    });
    /* payment methods */
    jQuery(document).on('click', '.payment_checkbox', function () {
        if (jQuery('#stripe-payments').is(':checked')) {
            jQuery('#stripe-payment-main').fadeIn("slow");
        } else {
            jQuery('#stripe-payment-main').fadeOut("slow");
        }

    });
});

/* see more instructions in service popup */
jQuery(document).ready(function () {
    jQuery(".show-more-toggler").click(function () {
        jQuery(".bullet-more").toggle("blind", {direction: "vertical"}, 500);
        jQuery(".show-more-toggler").toggleClass('rotate');
    });
});


/*********************************************************************************************/

/*********************************************************************************************/
jQuery(document).on('change', '.payment_class', function (event) {
    var p_val = jQuery(this).val();
    if (p_val == "stripe_payment") {
        jQuery('.card_payment_detail_form').fadeIn("slow");
    } else {
        jQuery('.card_payment_detail_form').fadeOut("slow");
    }
});
/* Get Location by Zip Code/Postal Code If Multisite is Enabled */
jQuery(document).on('keyup', '#mp_zip_code', function (event) {
    var ajaxurl = mpmain_obj.plugin_path;
    var location_err_msg = mpmain_obj.location_err_msg;
    var location_search_msg = mpmain_obj.location_search_msg;
    var Choose_service_msg = mpmain_obj.Choose_service;

    var zipcode = jQuery('#mp_zip_code').val();
    jQuery('#mp_selected_service').val(0);
    jQuery('#mp_selected_staff').val(0);
    jQuery('#mp_selected_location').val('X');
    jQuery('#mp_service_addons').html('');
    jQuery('#mp_service_addon_st').val('D');
    jQuery('#mp_selected_datetime').val('');
    jQuery('#mp_datetime_error').hide();
    jQuery('.mp-selected-date-view').addClass('mp-hide');

    if (zipcode != '') {
        jQuery(".loader").show();
        jQuery('#mp_location_success').hide();
        jQuery('#close_service_details').trigger('click');
        jQuery('#selected_custom .mp-value').html(Choose_service_msg);

        jQuery('#mp_location_error').html(location_search_msg);
        var postdata = {zipcode: zipcode, action: 'mp_get_location'};
        jQuery.ajax({
            type: "POST",
            async: false,
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: postdata,
            success: function (response) {
                jQuery(".loader").hide();
                if (jQuery.trim(response) == 'notfound') {
                    jQuery('#mp_location_success').hide();
                    jQuery('#mp_location_error').show();
                    jQuery('#mp_location_error').html(location_err_msg);
                } else {
                    jQuery('#mp_selected_location').val(0);
                    jQuery('#mp_location_success').show();
                    jQuery('#mp_location_error').hide();
                    /* Get Services By Found Location */
                    var location_id = 0;
                    var servicedata = {location_id: location_id, action: 'mp_get_location_services'};
                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                        dataType: 'html',
                        data: servicedata,
                        success: function (response) {
                            jQuery('#mp_services').html(response);
                        }
                    });
                }
            }
        });
    }
});

jQuery(document).on('click', '.select_location', function (event) {
    var ajaxurl = mpmain_obj.plugin_path;
    var location_err_msg = mpmain_obj.location_err_msg;
    var location_search_msg = mpmain_obj.location_search_msg;
    var Choose_service_msg = mpmain_obj.Choose_service;
    jQuery('#mp_location_error').hide();
    jQuery('#close_service_details').trigger('click');


    jQuery('#mp_selected_service').val(0);
    jQuery('#mp_selected_staff').val(0);
    jQuery('#mp_selected_location').val('X');
    jQuery('#mp_service_addon_st').val('D');
    jQuery('#mp_selected_datetime').val('');
    jQuery('#mp_datetime_error').hide();
    jQuery('.mp-selected-date-view').addClass('mp-hide');

    jQuery(".loader").show();

    /* Get Services By Found Location */
    var location_id = jQuery(this).attr('value');
    jQuery('#mp_selected_location').val(location_id);
    var servicedata = {location_id: location_id, action: 'mp_get_location_services'};
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: servicedata,
        success: function (response) {
            jQuery('#mp_services').html(response);
            jQuery(".loader").hide();
        }
    });
});


/* Hide Service Desciption On Click of Close */
jQuery(document).on("click", "#close_service_details", function () {
    jQuery(".service-details").removeClass('mp-show');
    jQuery(".service-details").addClass('mp-hide');

});

/* Get Service Detail On Select Of Service */
jQuery(document).on('click', '#mp_services .select_custom', function (event) {
    var ajaxurl = mpmain_obj.plugin_path;
    var sid = jQuery(this).data('sid');
    var multilmpion_status = mpmain_obj.multilocation_status;
    var zipwise_status = mpmain_obj.zipwise_status;
    var selected_location = jQuery('#mp_selected_location').val();
    jQuery('#mp_service_addon_st').val('D');
    jQuery('#mp_service_addons').html('');
    jQuery('#mp_selected_datetime').val('');
    jQuery('#mp_datetime_error').hide();
    jQuery('.mp-selected-date-view').addClass('mp-hide');

    jQuery('#mp_service_error').hide();
    if (multilmpion_status == 'E' && selected_location == 'X') {
        jQuery('#mp_location_error').show();
        jQuery(".common-selection-main").removeClass('clicked');
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp_location_error').offset().top - 80
        }, 800, 'swing', function () {
        });
        return false;
    }
    if (zipwise_status == 'E' && selected_location == 'X') {
        var Choose_zipcode_msg = mpmain_obj.Choose_zipcode;
        jQuery('#mp_location_success').hide();
        jQuery('#mp_location_error').show();
        jQuery('#mp_location_error').html(Choose_zipcode_msg);
        jQuery(".common-selection-main").removeClass('clicked');

        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp_location_error').offset().top - 80
        }, 800, 'swing', function () {
        });
        return false;
    }
    jQuery(".loader").show();
    jQuery('#selected_custom').html(jQuery(this).html());

    var servicedata = {sid: sid, action: 'mp_get_service_detail'};
    jQuery('#mp_selected_service').val(sid);
    /* Get Services By Found Location */
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: servicedata,
        success: function (response) {
            var service_details = jQuery.parseJSON(response);
            if (service_details.description != '') {
                jQuery('#mp_service_detail').html(service_details.description);
                jQuery(".common-selection-main").removeClass('clicked');
                jQuery(".custom-dropdown").slideUp();
                jQuery(".service-details").removeClass('mp-hide');
                jQuery(".service-details").addClass('mp-show');
                if (jQuery("#mp").width() >= 600 && jQuery("#mp").width() < 800) {
                    jQuery(".service-duration, .service-price").addClass("active-xs-12");
                }
            }
            if (service_details.addonsinfo != '') {
                jQuery('#mp_service_addon_st').val('E');
                jQuery('#mp_service_addons').html(service_details.addonsinfo);
                jQuery(".common-selection-main").removeClass('clicked');
                jQuery(".custom-dropdown").slideUp();
                jQuery("#mp_service_addons").removeClass('mp-hide');
                jQuery("#mp_service_addons").addClass('mp-show');
            }

        }
    });

    /* Get Provider By Service Provider */
    var servicestaffdata = {sid: sid, action: 'mp_get_service_providers'};
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: servicestaffdata,
        success: function (response) {
            jQuery(".loader").hide();
            jQuery('#mp_staff_info').html(response);
            if (jQuery("#mp").width() >= 600 && jQuery("#mp").width() < 800) {
                jQuery(".mp-staff-box").addClass("active-sm-6");
            }
        }
    });

});


/* Select Staff */
jQuery(document).on('click', '.mp-staff-box,#cus-select-staff .select_staff', function (event) {

    jQuery('#mp_service_error').hide();
    jQuery('#mp_staff_error').hide();
    jQuery('#mp_staff_error').addClass('mp-hide');

    jQuery('#mp_selected_datetime').val('');
    jQuery('#mp_datetime_error').hide();
    jQuery('.mp-selected-date-view').addClass('mp-hide');


    jQuery(".service-selection").removeClass('clicked');
    jQuery(".service-dropdown").removeClass('bounceInUp');
    jQuery(".location-selection").removeClass('clicked');
    jQuery(".location-dropdown").removeClass('bounceInUp');


    var selserviceid = jQuery('#mp_selected_service').val();
    if (selserviceid == 0) {
        var Choose_service_msg = mpmain_obj.Choose_service;
        jQuery('#mp_service_error').html(Choose_service_msg);
        jQuery('#mp_service_error').show();
        jQuery('#mp_service_error').removeClass('mp-hide');
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp_service_error').offset().top - 80
        }, 800, 'swing', function () {
        });
        return false;
    }

    var staffid = jQuery(this).data('staffid');
    jQuery('#mp_selected_staff').val(staffid);
    jQuery('#selected_custom_staff').html(jQuery(this).html());

});


/* Show Provider Time Slot*/
jQuery(document).on('click', '.mp-week,.by_default_today_selected', function () {
    if (jQuery(this).hasClass('inactive')) {
        return false;
    }

    var ajaxurl = mpmain_obj.plugin_path;

    var selstaffid = jQuery('#mp_selected_staff').val();
    if (selstaffid == 0) {
        jQuery('#mp_staff_error').show();
        jQuery('#mp_staff_error').removeClass('mp-hide');
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp_staff_error').offset().top - 80
        }, 800, 'swing', function () {
        });
        return false;
    } else {
        jQuery(".loader").show();
        var calrowid = jQuery(this).data('calrowid');
        var seldate = jQuery(this).data('seldate');
        var calenderdata = {selstaffid: selstaffid, seldate: seldate, action: 'mp_get_provider_slots'};

        jQuery('.mp-week').each(function () {
            jQuery(this).removeClass('active');

        });
        jQuery('.mp-show-time').each(function () {
            jQuery(this).removeClass('shown');
            jQuery(this).removeAttr('style');

        });
        jQuery(this).addClass('active');

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: calenderdata,
            success: function (response) {
                jQuery(".loader").hide();
                jQuery('.curr_selected_row' + calrowid).addClass('shown');
                jQuery('.curr_selected_row' + calrowid).css('display', 'block');
                jQuery('.curr_selected_row' + calrowid + ' .mp_day_slots').html(response);

            }
        });
    }
});

/* Select Time Slot*/
jQuery(document).on('click', '.mp_select_slot', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    var slotdate = jQuery(this).data('slot_db_date');
    var slottime = jQuery(this).data('slot_db_time');
    var displaydate = jQuery(this).data('displaydate');
    var displaytime = jQuery(this).data('displaytime');

    jQuery('#mp_datetime_error').hide();
    jQuery('.mp-selected-date-view').removeClass('mp-hide');
    jQuery('.time-slot').each(function () {
        jQuery(this).removeClass('mp-slot-selected');

    });
    jQuery(this).addClass('mp-slot-selected');
    jQuery('.mp-selected-date-view').removeClass('mp-hide');
    jQuery('#mp_selected_datetime').val(slotdate + ' ' + slottime);
    jQuery('.mp-date-selected').html(displaydate);
    jQuery('.mp-time-selected').html(displaytime);
    jQuery('.mp-show-time').hide();

});

/* Goto Today */
jQuery(document).on('click', '.today_btttn', function () {

    var calmonth = jQuery('.previous-date').data('curmonth');
    var calyear = jQuery('.previous-date').data('curyear');

    var selmonth = jQuery(this).data('smonth');
    var selyear = jQuery(this).data('syear');

    if (selmonth == calmonth && calyear == selyear) {
        jQuery('.by_default_today_selected').trigger('click');
    } else {
        jQuery(".loader").show();
        var ajaxurl = mpmain_obj.plugin_path;
        var calenderdata = {calmonth: calmonth, calyear: calyear, action: 'mp_cal_next_prev'};
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: calenderdata,
            success: function (response) {
                jQuery(".loader").hide();
                jQuery('.calendar-wrapper').html(response);
                jQuery('.by_default_today_selected').trigger('click');
            }
        });
    }
});

/* Get Calender Next Previous Month */
jQuery(document).on('click', '.mp_month_change', function () {

    jQuery('#mp_selected_datetime').val('');
    jQuery('#mp_datetime_error').hide();
    jQuery('.mp-selected-date-view').addClass('mp-hide');

    var ajaxurl = mpmain_obj.plugin_path;
    var calmonth = jQuery(this).data('calmonth');
    var calyear = jQuery(this).data('calyear');
    var calenderdata = {calmonth: calmonth, calyear: calyear, action: 'mp_cal_next_prev'};
    jQuery(".loader").show();

    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: calenderdata,
        success: function (response) {
            jQuery('.calendar-wrapper').html(response);
            jQuery(".loader").hide();
        }
    });
});


/********Code For Register booking complete and login and logout***************/


/* Validate Card Fields */
jQuery(document).ready(function () {
    jQuery('input.cc-number').payment('formatCardNumber');
    jQuery('input.cc-cvc').payment('formatCardCVC');
    jQuery('input.cc-exp-month').payment('restrictNumeric');
    jQuery('input.cc-exp-year').payment('restrictNumeric');

});


jQuery(document).on("click", '.mp-termcondition-area', function () {
    jQuery('.mp_terms_and_condition_error').hide();
});

jQuery(document).on("click", '#mp_log_out_user', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    var dataString = {'action': 'mp_logout_user'};
    jQuery(".loader").show();
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: dataString,
        success: function (response) {
            jQuery(".loader").hide();
            jQuery('.user-login-main').show();
            jQuery('.user-login-main').show();
            jQuery('.existing-user-success-login-message').hide();
            jQuery('#mp-new-user').trigger('click');

            jQuery(".mp-main-left label.custom").removeClass('focus');
            jQuery(".mp-main-left .custom-input").removeClass('focus');
            jQuery('#new_user_preferred_password').val('');
            jQuery('#new_user_preferred_username').val('');
            jQuery('#new_user_firstname').val('');
            jQuery('#new_user_lastname').val('');
            jQuery('#mp-front-phone').val('');
            jQuery('#new_user_street_address').val('');
            jQuery('#zipcode').val('');
            jQuery('#new_user_city').val('');
            jQuery('#new_user_state').val('');
            jQuery('#new_user_notes').val('');

            jQuery('#mp-male').prop('checked', true);

        }
    });
});

/* Display Country Code on click flag on phone*/
jQuery(window).load(function () {

    if (jQuery("#mp-front-phone").data('ccode') != '') {
        jQuery('.country').removeClass('active');
        jQuery('.country').each(function () {
            if ('+' + jQuery(this).data("dial-code") == jQuery("#mp-front-phone").data('ccode')) {
                jQuery(this).addClass('active');
                var get_phoneno = jQuery(this).val();
                jQuery('#mp-front-phone').intlTelInput("setNumber", '+' + jQuery(this).data("dial-code") + '' + get_phoneno);
            }
        });
    } else {
        var country_code = jQuery('.country.active').data("dial-code");
        if (country_code === undefined) {
            country_code = '1';
        }
        var get_phoneno = jQuery('#mp-front-phone').val();
        if (get_phoneno == '') {
            jQuery('#mp-front-phone').intlTelInput("setNumber", '+' + country_code);
        }
        jQuery("#mp-front-phone").attr('data-ccode', '+' + country_code);
    }
});
jQuery(document).on('click', '.country', function () {
    var country_code = jQuery(this).data("dial-code");
    var get_phoneno = jQuery('#mp-front-phone').val();
    jQuery('#mp-front-phone').intlTelInput("setNumber", '+' + country_code);
    jQuery("#mp-front-phone").attr('data-ccode', '+' + country_code);
});

/* On focus transform label */
jQuery(document).ready(function () {
    function checkForInput(element) {
        /* element is passed to the function ^ */
        if (jQuery(element).hasClass('mp-phone-input')) {
            var $label = jQuery('.mp-phone-label');
        } else {
            var $label = jQuery(element).siblings('label');
        }

        if (jQuery(element).val().length > 0) {
            $label.addClass('focus');
            jQuery(this).addClass("focus");
        } else {
            $label.removeClass('focus');
            jQuery(this).removeClass("focus");
        }
        /* user login then show the label at top */
        if (jQuery('.custom-input').val().length > 0) {
            /*jQuery(".mp-main-left label.custom").addClass('focus'); */
        }/*else{ */
        /*	jQuery('.label.custom').removeClass('focus');*/
        /*	} */
        /* user login then show the label at top */
        /* if (jQuery('#mp-front-phone').val().length > 0) {
			jQuery("label.mp-phone-label").addClass('focus');
		} */ else {
            jQuery('#mp-front-phone').removeClass('focus');
        }

    }

    /* The lines below are executed on page load */
    jQuery('.custom-input').each(function () {
        checkForInput(this);
        if (jQuery(this).val().length > 0) {
            jQuery(this).addClass('focus');
        } else {
            jQuery(this).removeClass('focus');
        }

    });

    /* The lines below (inside) are executed on change & keyup */
    jQuery('.custom-input').on('change keyup', function () {
        checkForInput(this);
        jQuery(this).addClass("focus");
    });
});

jQuery(document).ready(function () {
    jQuery('.sub_cat_bang input[type=radio]').is('checked', function () {
        jQuery('.appartment_no').addClass("with_bungalow");


    });
});


/*****************************************************************************************************************/
/************************************************* PAM CODE FRONT ************************************************/
/*****************************************************************************************************************/


jQuery(document).ready(function () {
    var pam_disabledates = mpmain_obj.pam_disabledates;
    var pamstart_date = mpmain_obj.pamstart_date;
    var pamoffdays = pam_disabledates.split('##');
    var pam_disabledatesarr = [];

    var pamintialdate = new Date(pamstart_date);
    pamintialdate.setDate(pamintialdate.getDate());

    for (var i = 0; i < pamoffdays.length; i++) {
        pam_disabledatesarr.push(pamoffdays[i]);
    }
    jQuery('#front-datepicker input, #from_cab #from_cab_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: pamintialdate,
        datesDisabled: pam_disabledatesarr,
    });
});

jQuery(document).ready(function () {
    jQuery('.selectpicker1, .selectpicker2, .selectpicker').selectpicker();
});


/* Get Destination Cities */
jQuery(document).on("change", '#pam_sc_city', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    var source_city = jQuery(this).val();


    var city_type = jQuery(this).data('ct');
    if (source_city == '') {
        jQuery('#pam_sc_city_error').show();
    } else {
        jQuery('#pam_sc_city_error').hide();
    }

    if (source_city != '') {
        var dataString = {
            'source_city': source_city,
            'city_type': city_type,
            'action': 'get_destination_city'
        };
        jQuery(".loader").show();
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: dataString,
            success: function (response) {
                jQuery(".loader").hide();
                jQuery('#pam_dc_city').html(response);
                jQuery('.selectpicker2').selectpicker('refresh');
                jQuery('.destination_ct').hide();
            }
        });
    }
});

jQuery(document).on("change", '#pam_dc_city', function () {

    if (jQuery(this).val() == 'Other') {
        jQuery('.destination_ct').show();
    } else {
        var destination_city = jQuery(this).val();
        jQuery('.destination_ct').hide();
    }
    if (destination_city == '') {
        jQuery('#pam_dc_city_error').show();
    } else {
        jQuery('#pam_dc_city_error').hide();
    }
});


jQuery(document).on('click', '#btn-second-step', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    if (jQuery('.mp-booking-step #first').hasClass('active')) {
        var selectcategory = jQuery('input[name="main_stuff_selection"]:checked').val();
        var source_city = jQuery('#pam_sc_city').val();
        var destination_city = jQuery('#pam_dc_city').val();
        var desctination_city_text = "";
        if (destination_city == 'Other') {
            desctination_city_text = jQuery('#destination_ct_input').val();
        } else {
            desctination_city_text = jQuery('#pam_dc_city').val();
        }
        var selected_date = jQuery('#pam_bookingdate').val();
        if (source_city == '' && destination_city == '') {
            jQuery('#pam_sc_city_error').show();
            jQuery('#pam_dc_city_error').show();
            return false;
        }
        if (source_city == '') {
            jQuery('#pam_sc_city_error').show();
            return false;
        }
        if (destination_city == '') {
            jQuery('#pam_dc_city_error').show();
            return false;
        }

        var current_stepdata = {
            'selectcategory': selectcategory,
            'source_city': source_city,
            'destination_city': destination_city,
            'desctination_city_text': desctination_city_text,
            'selected_date': selected_date,
            action: 'set_stepone_session'
        };
        var requested_step = 'second';
    }

    /* Set First Step Values In Session */
    jQuery(".loader").show();
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: current_stepdata,
        success: function (response) {
            jQuery(".loader").hide();
        }
    });

    /* Load Service Articles */
    var get_second_stepdata = {'selectcategory': selectcategory, action: 'load_service_articles'};
    jQuery(".loader").show();
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: get_second_stepdata,
        success: function (response) {
            jQuery(".loader").hide();
            jQuery('#mp_service_addons').html(response);

        }
    });


    if (selectcategory == '1') {
        jQuery('.pam_home_service_ls').show();
        jQuery('.pam_home_service_rs').show();
        jQuery('.pam_vehicle_service').hide();
        jQuery('.pam_office_service').hide();
        jQuery('.other_service_ls').hide();
        jQuery('.pam_pates_service_form').hide();
        jQuery('.pam_other_service').hide();
        jQuery('.pam_pets_service').hide();
        jQuery('.caps_form').hide();
    }
    if (selectcategory == '2') {
        jQuery('.pam_home_service_ls').hide();
        jQuery('.pam_home_service_rs').hide();
        jQuery('.pam_vehicle_service').hide();
        jQuery('.other_service_ls').hide();
        jQuery('.pam_office_service').show();
        jQuery('.pam_pates_service_form').hide();
        jQuery('.pam_other_service').hide();
        jQuery('.pam_pets_service').hide();
        jQuery('.caps_form').hide();

    }
    if (selectcategory == '3') {
        jQuery('.pam_home_service_ls').hide();
        jQuery('.pam_home_service_rs').hide();
        jQuery('.pam_vehicle_service').show();
        jQuery('.pam_office_service').hide();
        jQuery('.other_service_ls').hide();
        jQuery('.pam_pates_service_form').hide();
        jQuery('.pam_other_service').hide();
        jQuery('.pam_pets_service').hide();
        jQuery('.caps_form').hide();
    }
    if (selectcategory == '4') {
        jQuery('.pam_home_service_ls').hide();
        jQuery('.pam_home_service_rs').hide();
        jQuery('.pam_vehicle_service').hide();
        jQuery('.pam_pets_service').show();
        jQuery('.other_service_ls').hide();
        jQuery('.pam_office_service').hide();
        jQuery('#mp_service_addons').hide();
        jQuery('.pam_other_service').hide();
        jQuery('.caps_form').hide();
    }
    if (selectcategory == '5') {
        jQuery('.pam_home_service_ls').hide();
        jQuery('.pam_home_service_rs').hide();
        jQuery('.pam_vehicle_service').hide();
        jQuery('.pam_pets_service').hide();
        jQuery('.other_service_ls').hide();
        jQuery('.pam_office_service').hide();
        jQuery('#mp_service_addons').hide();
        jQuery('.pam_other_service').hide();
        jQuery('.caps_form').hide();
    }
    if (selectcategory == '6') {
        jQuery('.pam_home_service_ls').hide();
        jQuery('.pam_home_service_rs').hide();
        jQuery('.pam_vehicle_service').hide();
        jQuery('.pam_pets_service').hide();
        jQuery('.other_service_ls').show();
        jQuery('.pam_office_service').hide();
        jQuery('#mp_service_addons').hide();
        jQuery('.pam_other_service').show();
        jQuery('.caps_form').hide();
    }

    if (requested_step == 'second') {
        jQuery('#mp_first_step').hide();
        jQuery('#mp_third_step').hide();
        jQuery('#mp_fourth_step').hide();
        jQuery('#mp_second_step').show();
    }

});
/* pats select service statis options  */
jQuery(document).on('change', '.pets_service', function () {
    jQuery('.pets_service_gender').show();
    jQuery('.mp_service_addons').hide();
});
/* pats select gender options  */
jQuery(document).on('change', '.pets_service_gen', function () {
    jQuery('.mp_service_addons').hide();
    /* jQuery('.pam_pates_service_form').show(); */
});

/* Packer & Movers commercial Service Loading Options */
jQuery(document).on('change', '.commercial_main_stuff_selection', function () {
    jQuery('.pam_home_service_ls').hide();
    jQuery('.pam_home_service_rs').hide();

});
/* Packer & Movers other Service Options */
jQuery(document).on('change', '.other_services', function () {
    jQuery('.pam_home_service_ls').hide();
    jQuery('.pam_home_service_rs').hide();
    /*  jQuery('.other_service_ls').show(); */

});

/* Packer & Movers Home Service Loading Options */
jQuery(document).on('change', '.home_service_loading', function () {
    var home_loading_type = jQuery(this).val();
    if (home_loading_type == 'Appartment') {
        jQuery('.appartment_loading_section').show();
        jQuery('.bungalow_loading_section').hide();
    } else if (home_loading_type == 'Bungalow') {
        jQuery('.bungalow_loading_section').show();
        jQuery('.appartment_loading_section').hide();
    }
});
/* Packer & Movers Home Service unLoading Options */
jQuery(document).on('change', '.home_service_unloading', function () {
    var home_loading_type = jQuery(this).val();
    if (home_loading_type == 'Appartment') {
        jQuery('.appartment_unloading_section').show();
        jQuery('.bungalow_unloading_section').hide();
    } else if (home_loading_type == 'Bungalow') {
        jQuery('.bungalow_unloading_section').show();
        jQuery('.appartment_unloading_section').hide();
    }
});
/* Packer & Movers Vehicle Service Options */
jQuery(document).on('change', '.vehicle_service', function () {
    var vehicle_type = jQuery(this).val();
    if (vehicle_type == 'Car') {
        jQuery('.vehicle_service_car').show();
        jQuery('.vehicle_service_bike').show();
        jQuery('.append_vehicle').show();
        jQuery('.no_of_vehicle').val("");
    } else if (vehicle_type == 'Bike') {
        jQuery('.vehicle_service_bike').show();
        jQuery('.vehicle_service_car').hide();
        jQuery('.append_vehicle').hide();
        jQuery('.no_of_vehicle').val("");
    }
});
jQuery(document).on('change', '.pets_service', function () {

    var value = jQuery(this).prop('checked');
    var data_did = jQuery(this).data('div_id');

    if (value) {
        jQuery('#app' + data_did).show();

    } else {
        jQuery('#app' + data_did).hide();
    }

});
/* Show Addon Quantity */
jQuery(document).on('click', '.addon-checkbox', function () {
    var saddonid = jQuery(this).data('saddonid');
    jQuery('.mp-addon-count' + saddonid).toggle();
    var value = jQuery(this).prop('checked');
});
/* Addon Quantity Increment/Decrement */
jQuery(document).on('click', '.mp_addonqty', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    var addon_id = jQuery(this).data('addonid');
    var addon_qty_action = jQuery(this).data('qtyaction');
    var addon_maxqty = jQuery(this).data('addonmax');
    var currentqtyvalue = jQuery('#addonqty_' + addon_id).val();
    if (addon_qty_action == 'minus') {
        if (parseInt(currentqtyvalue) > 1) {
            jQuery('#addonqty_' + addon_id).val(parseInt(currentqtyvalue) - 1);
        }
    } else {
        if (parseInt(currentqtyvalue) < parseInt(addon_maxqty)) {
            jQuery('#addonqty_' + addon_id).val(parseInt(currentqtyvalue) + 1);
        }
    }
});
/* Hide Error Messages */
jQuery(document).on('click', '.custom-input', function () {
    jQuery('#bungalow_loading_floor_err').hide();
    jQuery('#appartment_loading_floor_err').hide();
    jQuery('#bungalow_unloading_floor_err').hide();
    jQuery('#appartment_unloading_floor_err').hide();
    jQuery('#bungalow_loading_type_err').hide();
    jQuery('#bungalow_unloading_type_err').hide();
    jQuery('#office_service_area_err').hide();
    jQuery('#vehicle_service_no_err').hide();
    jQuery('#vehicle_service_car_type_err').hide();
    jQuery('#pets_name_errs').hide();
    jQuery('#pets_breed_errs').hide();
    jQuery('#pets_age_errs').hide();
    jQuery('#pets_weight_errs').hide();
    jQuery('#other_name_errs').hide();
    jQuery('#other_floor_errs').hide();
    jQuery('#other_article_errs').hide();
    jQuery('#other_description_errs').hide();
});

/* Proceed TO Step 3rd */
jQuery(document).on('click', '#fourth', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    var additional_info_id = mpmain_obj.pam_enabled_additionalinfo;

    var service_info = {};
    var loading_info = {};
    var unloading_info = {};
    var cabs_info = {};
    var additional_info = {};
    var articles_info = {};
    var other_info = {};
    var vehicle_car_typess = [];
    var vehicle_no_info_vehicle = [];


    if (jQuery('.pam_home_service_ls').is(':visible')) {
        var home_loading_type = jQuery('input[name="home_service_loading"]:checked').val();
        var home_unloading_type = jQuery('input[name="home_service_unloading"]:checked').val();
        var home_service_err = '';

        if (home_loading_type == 'Bungalow') {
            var bungalow_loading_floor = jQuery('#bungalow_loading_floor').val();
            var bungalow_loading_type = jQuery('#bungalow_loading_type').val();
            service_info['service'] = 'Home';
            service_info['loading_floor_number'] = bungalow_loading_floor;
            service_info['loading_bunglow_type'] = bungalow_loading_type;

            if (bungalow_loading_floor == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_loading_floor_err').show();
            }
            if (bungalow_loading_type == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_loading_type_err').show();
            }
        } else {
            var appartment_loading_floor = jQuery('#appartment_loading_floor').val();
            service_info['service'] = 'Home';
            service_info['loading_floor_number'] = appartment_loading_floor;
            service_info['loading_bunglow_type'] = '';

            if (appartment_loading_floor == '') {
                home_service_err += 'Y';
                jQuery('#appartment_loading_floor_err').show();
            }
        }
        if (home_unloading_type == 'Bungalow') {
            var bungalow_unloading_floor = jQuery('#bungalow_unloading_floor').val();
            var bungalow_unloading_type = jQuery('#bungalow_unloading_type').val();
            service_info['unloading_floor_number'] = bungalow_unloading_floor;
            service_info['unloading_bunglow_type'] = bungalow_unloading_type;

            if (bungalow_unloading_floor == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_unloading_floor_err').show();
            }
            if (bungalow_unloading_type == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_unloading_type_err').show();
            }
        } else {
            var appartment_unloading_floor = jQuery('#appartment_unloading_floor').val();
            service_info['unloading_floor_number'] = appartment_unloading_floor;
            service_info['unloading_bunglow_type'] = '';

            if (appartment_unloading_floor == '') {
                home_service_err += 'Y';
                jQuery('#appartment_unloading_floor_err').show();
            }
        }
        if (home_service_err != '') {
            return false;
        }

    }
    /* Other Service Validate */
    if (jQuery('.pam_other_service').is(':visible')) {
        var other_service_err = '';
        var other_service_article = jQuery("input[name='other_service_article[]']").map(function () {
            return jQuery(this).val();
        }).get();

        var other_service_description = jQuery('#other_service_description').val();
        var other_service_floor_no = jQuery('#other_service_floor_no').val();

        articles_info['other_service_article'] = other_service_article;
        service_info['service'] = 'Other';
        service_info['other_service_description'] = other_service_description;
        service_info['other_service_floor_no'] = other_service_floor_no;

        if (other_service_floor_no == '') {
            other_service_err += 'Y';
            jQuery('#other_floor_errs').show();
        }
        if (other_service_article == '') {
            other_service_err += 'Y';
            jQuery('#other_article_errs').show();
        }
        if (other_service_description == '') {
            other_service_err += 'Y';
            jQuery('#other_description_errs').show();
        }
        if (other_service_err != '') {
            return false;
        }
    }


    /* Office Service Validate */
    if (jQuery('.pam_office_service').is(':visible')) {
        var office_service_err = '';
        var office_service_area = jQuery('#office_service_area').val();
        service_info['service'] = 'office';
        service_info['office_area'] = office_service_area;
        if (office_service_area == '' || isNaN(office_service_area)) {
            office_service_err += 'Y';
            jQuery('#office_service_area_err').show();
        }
        if (office_service_err != '') {
            return false;
        }
    }
    /* Vehicle Service Validate */
    if (jQuery('.pam_vehicle_service').is(':visible')) {
        jQuery('.vehicle_appdivs').each(function () {
            vehicle_car_typess.push(jQuery(this).find('.location-selection-cartype .data-list .mp-value').html());
            vehicle_no_info_vehicle.push(jQuery(this).find('.no_of_vehicle').val());

        });

        var vehicle_service_type = jQuery('input[name="vehicle_service"]:checked').val();
        var vehicle_service_err = '';
        var vehicle_service_quantity = jQuery('#vehicle_service_quantity').val();
        service_info['service'] = 'Vehicle';
        service_info['vehicle_qty'] = vehicle_service_quantity;

        if (vehicle_service_quantity == '' || isNaN(vehicle_service_quantity)) {
            vehicle_service_err += 'Y';
            jQuery('#vehicle_service_no_err').show();
        }

        if (vehicle_service_type == 'Car') {
            var vehicle_service_car_type = jQuery('#vehicle_service_car_type').val();
            /* service_info['vehicle_type'] = vehicle_service_car_type; */
            service_info['vehicle_type'] = vehicle_car_typess;
            service_info['no_of_vehicle'] = vehicle_no_info_vehicle;
            if (vehicle_service_car_type == '') {
                vehicle_service_err += 'Y';
                jQuery('#vehicle_service_car_type_err').show();
            }
        }

        if (vehicle_service_err != '') {
            return false;
        }
    }
    /* Pets Service Validate */
    if (jQuery('.pam_pets_service').is(':visible')) {

        var count_var = 0;

        jQuery.each(jQuery(".pets_service"), function () {
            var value = jQuery(this).prop('checked');
            if (value) {
                var pet_multiple_div = {};
                var pets_service_err = '';
                var data_did = jQuery(this).data('div_id');

                var pet_service_value = jQuery(this).val();
                var pet_service_error = "pet_service_" + pet_service_value.toLowerCase();
                var pets_service_gen = jQuery('.pets_service_gen').val();
                var pet_name = jQuery('#pet_name' + data_did).val();
                var pet_breed = jQuery('#pet_breed' + data_did).val();
                var pets_gender = jQuery('input[name="pets_gender_1' + data_did + '"]:checked').val();
                var pet_age = jQuery('#pet_age' + data_did).val();
                var pets_age_radio = jQuery('#age_value' + data_did).val();

                var pet_weight = jQuery('#pet_weight' + data_did).val();
                var pet_weight_gen = jQuery('#weight_value' + data_did).val();

                pet_multiple_div['pets_service_gen'] = pets_service_gen;
                pet_multiple_div['pets_service'] = pet_service_value;
                pet_multiple_div['pet_name'] = pet_name;
                pet_multiple_div['pet_breed'] = pet_breed;
                pet_multiple_div['pets_gender'] = pets_gender;
                pet_multiple_div['pet_age'] = pet_age;
                pet_multiple_div['pets_age_radio'] = pets_age_radio;
                pet_multiple_div['pet_weight'] = pet_weight;
                pet_multiple_div['pet_weight_gen'] = pet_weight_gen;
                pet_multiple_div['service'] = 'Pets';

                if (pet_name == '') {
                    pets_service_err += 'Y';
                    jQuery('#pets_name_errs' + pet_service_error).show();
                }
                if (pet_breed == '') {
                    pets_service_err += 'Y';
                    jQuery('#pets_breed_errs' + pet_service_error).show();
                }
                if (pet_age == '' && !(intRegex.test(pet_age))) {
                    /* if(pet_age==''){ */
                    pets_service_err += 'Y';
                    jQuery('#pets_age_errs' + pet_service_error).show();
                }
                if (pet_weight == '' && !(intRegex.test(pet_age))) {
                    pets_service_err += 'Y';
                    jQuery('#pets_weight_errs' + pet_service_error).show();
                }
                if (pets_service_err != '') {
                    return false;
                }
                count_var++;
                var service_count = "pet_info" + count_var;

                service_info[service_count] = pet_multiple_div;
            }

        });
        console.log(service_info);
    }
    jQuery(document).ready(function () {
        jQuery("#pet_namepet_service_dog").keyup(function () {
            jQuery("#pets_name_errspet_service_dog").hide();
        });
        jQuery("#pet_breedpet_service_dog").keyup(function () {
            jQuery("#pets_breed_errspet_service_dog").hide();
        });
        jQuery("#pet_agepet_service_dog").keyup(function () {
            jQuery("#pets_age_errspet_service_dog").hide();
        });
        jQuery("#pet_weightpet_service_dog").keyup(function () {
            jQuery("#pets_weight_errspet_service_dog").hide();
        });


        jQuery("#pet_namepet_service_cat").keyup(function () {
            jQuery("#pets_name_errspet_service_cat").hide();
        });
        jQuery("#pet_breedpet_service_cat").keyup(function () {
            jQuery("#pets_breed_errspet_service_cat").hide();
        });
        jQuery("#pet_agepet_service_cat").keyup(function () {
            jQuery("#pets_age_errspet_service_cat").hide();
        });
        jQuery("#pet_weightpet_service_cat").keyup(function () {
            jQuery("#pets_weight_errspet_service_cat").hide();
        });


        jQuery("#pet_namepet_service_birds").keyup(function () {
            jQuery("#pets_name_errspet_service_birds").hide();
        });
        jQuery("#pet_breedpet_service_birds").keyup(function () {
            jQuery("#pets_breed_errspet_service_birds").hide();
        });
        jQuery("#pet_agepet_service_birds").keyup(function () {
            jQuery("#pets_age_errspet_service_birds").hide();
        });
        jQuery("#pet_weightpet_service_birds").keyup(function () {
            jQuery("#pets_weight_errspet_service_birds").hide();
        });


        jQuery("#pet_namepet_service_other").keyup(function () {
            jQuery("#pets_name_errspet_service_other").hide();
        });
        jQuery("#pet_breedpet_service_other").keyup(function () {
            jQuery("#pets_breed_errspet_service_other").hide();
        });
        jQuery("#pet_agepet_service_other").keyup(function () {
            jQuery("#pets_age_errspet_service_other").hide();
        });
        jQuery("#pet_weightpet_service_other").keyup(function () {
            jQuery("#pets_weight_errspet_service_other").hide();
        });


    });


    /* Validate Loading/Unloading Address Section */
    jQuery('#pam_loading_unloading_address').validate({
        rules: {
            pam_loading_street_address: {required: true},
            pam_loading_city: {required: true},
            pam_loading_state: {required: true},

            pam_unloading_street_address: {required: true},
            pam_unloading_city: {required: true},
            pam_unloading_state: {required: true}

        },
        messages: {
            pam_loading_street_address: {required: mpmain_error_obj.pam_loading_street_address},
            pam_loading_city: {required: mpmain_error_obj.pam_loading_city},
            pam_loading_state: {required: mpmain_error_obj.pam_loading_state},

            pam_unloading_street_address: {required: mpmain_error_obj.pam_loading_street_address},
            pam_unloading_city: {required: mpmain_error_obj.pam_loading_city},
            pam_unloading_state: {required: mpmain_error_obj.pam_loading_state}

        }
    });
    if (!jQuery('#pam_loading_unloading_address').valid()) {
        return false;
    }
    loading_info['pam_loading_street_address'] = jQuery('#pam_loading_street_address').val();
    loading_info['pam_loading_city'] = jQuery('#pam_loading_city').val();
    loading_info['pam_loading_state'] = jQuery('#pam_loading_state').val();


    /* cabs Info by trupal added by pradeep*/
    if (jQuery('#cab_want').is(':checked')) {
        var cab_lable_err = ""
        var to_cab_lable_err = ""
        var pam_street_address_cab = cabs_info['pam_street_address_cab'] = jQuery('#pam_loading_street_address_cab').val();
        var pam_loading_city_cab = cabs_info['pam_city_cab'] = jQuery('#pam_loading_city_cab').val();
        var pam_loading_state_cab = cabs_info['pam_state_cab'] = jQuery('#pam_loading_state_cab').val();
        var pam_member_cab = cabs_info['pam_member_cab'] = jQuery('#pam_member_cab').val();

        var cab_date = cabs_info['cab_date'] = jQuery('#from_cab_date').val();
        var to_pam_street_address_cab = cabs_info['to_pam_street_address_cab'] = jQuery('#to_pam_loading_street_address_cab').val();
        var to_pam_loading_city_cab = cabs_info['to_pam_city_cab'] = jQuery('#to_pam_loading_city_cab').val();
        var to_pam_loading_state_cab = cabs_info['to_pam_state_cab'] = jQuery('#to_pam_loading_state_cab').val();

        if (pam_street_address_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_address_err').show();
        }
        if (pam_loading_city_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_city_err').show();
        }
        if (pam_loading_state_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_state_err').show();
        }
        if (pam_member_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_member_err').show();
        }
        if (to_pam_street_address_cab == '') {
            to_cab_lable_err += 'Y';
            jQuery('#to_cab_address_err').show();
        }
        if (to_pam_loading_city_cab == '') {
            to_cab_lable_err += 'Y';
            jQuery('#to_cab_city_err').show();
        }
        if (to_pam_loading_state_cab == '') {
            to_cab_lable_err += 'Y';
            jQuery('#to_cab_state_err').show();
        }
        if (cab_lable_err != '') {
            return false;
        }
        if (to_cab_lable_err != '') {
            return false;
        }
    }


    unloading_info['pam_unloading_street_address'] = jQuery('#pam_unloading_street_address').val();
    unloading_info['pam_unloading_city'] = jQuery('#pam_unloading_city').val();
    unloading_info['pam_unloading_state'] = jQuery('#pam_unloading_state').val();


    /* Fetch Additional Info */
    if (additional_info_id != '') {
        var additional_info_ids = additional_info_id.split(',');
        for (var ids = 0; ids < additional_info_ids.length; ids++) {
            var additional_info_id = additional_info_ids[ids];

            if (jQuery('#additional_info' + additional_info_id).is(':checked')) {
                additional_info[additional_info_id] = 'Y';
            } else {
                additional_info[additional_info_id] = 'N';
            }
        }
    }

    /* Fetch Service Articles Info */
    if (jQuery('#mp_service_addons').is(':visible')) {
        jQuery('.addon-checkbox').each(function () {
            var curraddonid = jQuery(this).data('saddonid');
            if (jQuery(this).is(':checked')) {
                articles_info[curraddonid] = jQuery('#addonqty_' + curraddonid).val();
            }
        });
    }

    var current_stepdata = {
        'service_info': service_info,
        'loading_info': loading_info,
        'cabs_info': cabs_info,
        'unloading_info': unloading_info,
        'articles_info': articles_info,
        'additional_info': additional_info,
        action: 'set_steptwo_session'
    }

    /* Set Second Step Values In Session */
    jQuery("input").blur(function () {
        jQuery('#compare_error_msg').hide();
    });
    if (loading_info['pam_loading_street_address'] == unloading_info['pam_unloading_street_address']) {
        jQuery('#compare_error_msg').show();
        return false;
    } else {
        jQuery(".loader").show();
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: current_stepdata,
            success: function (response) {
                jQuery(".loader").hide();
            }
        });
    }
    jQuery('#mp_fourth_step').show();
    jQuery('#mp_second_step').hide();


});

jQuery(document).on('click', '#btn-fourth-step', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    var additional_info_id = mpmain_obj.pam_enabled_additionalinfo;

    var service_info = {};
    var loading_info = {};
    var unloading_info = {};
    var cabs_info = {};
    var additional_info = {};
    var articles_info = {};
    var other_info = {};
    var vehicle_car_typess = [];
    var vehicle_no_info_vehicle = [];


    if (jQuery('.pam_home_service_ls').is(':visible')) {
        var home_loading_type = jQuery('input[name="home_service_loading"]:checked').val();
        var home_unloading_type = jQuery('input[name="home_service_unloading"]:checked').val();
        var home_service_err = '';

        if (home_loading_type == 'Bungalow') {
            var bungalow_loading_floor = jQuery('#bungalow_loading_floor').val();
            var bungalow_loading_type = jQuery('#bungalow_loading_type').val();
            service_info['service'] = 'Home';
            service_info['loading_floor_number'] = bungalow_loading_floor;
            service_info['loading_bunglow_type'] = bungalow_loading_type;

            if (bungalow_loading_floor == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_loading_floor_err').show();
            }
            if (bungalow_loading_type == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_loading_type_err').show();
            }
        } else {
            var appartment_loading_floor = jQuery('#appartment_loading_floor').val();
            service_info['service'] = 'Home';
            service_info['loading_floor_number'] = appartment_loading_floor;
            service_info['loading_bunglow_type'] = '';

            if (appartment_loading_floor == '') {
                home_service_err += 'Y';
                jQuery('#appartment_loading_floor_err').show();
            }
        }
        if (home_unloading_type == 'Bungalow') {
            var bungalow_unloading_floor = jQuery('#bungalow_unloading_floor').val();
            var bungalow_unloading_type = jQuery('#bungalow_unloading_type').val();
            service_info['unloading_floor_number'] = bungalow_unloading_floor;
            service_info['unloading_bunglow_type'] = bungalow_unloading_type;

            if (bungalow_unloading_floor == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_unloading_floor_err').show();
            }
            if (bungalow_unloading_type == '') {
                home_service_err += 'Y';
                jQuery('#bungalow_unloading_type_err').show();
            }
        } else {
            var appartment_unloading_floor = jQuery('#appartment_unloading_floor').val();
            service_info['unloading_floor_number'] = appartment_unloading_floor;
            service_info['unloading_bunglow_type'] = '';

            if (appartment_unloading_floor == '') {
                home_service_err += 'Y';
                jQuery('#appartment_unloading_floor_err').show();
            }
        }
        if (home_service_err != '') {
            return false;
        }

    }
    /* Other Service Validate */
    if (jQuery('.pam_other_service').is(':visible')) {
        var other_service_err = '';
        var other_service_article = jQuery("input[name='other_service_article[]']").map(function () {
            return jQuery(this).val();
        }).get();

        var other_service_description = jQuery('#other_service_description').val();
        var other_service_floor_no = jQuery('#other_service_floor_no').val();

        articles_info['other_service_article'] = other_service_article;
        service_info['service'] = 'Other';
        service_info['other_service_description'] = other_service_description;
        service_info['other_service_floor_no'] = other_service_floor_no;

        if (other_service_floor_no == '') {
            other_service_err += 'Y';
            jQuery('#other_floor_errs').show();
        }
        if (other_service_article == '') {
            other_service_err += 'Y';
            jQuery('#other_article_errs').show();
        }
        if (other_service_description == '') {
            other_service_err += 'Y';
            jQuery('#other_description_errs').show();
        }
        if (other_service_err != '') {
            return false;
        }
    }


    /* Office Service Validate */
    if (jQuery('.pam_office_service').is(':visible')) {
        var office_service_err = '';
        var office_service_area = jQuery('#office_service_area').val();
        service_info['service'] = 'office';
        service_info['office_area'] = office_service_area;
        if (office_service_area == '' || isNaN(office_service_area)) {
            office_service_err += 'Y';
            jQuery('#office_service_area_err').show();
        }
        if (office_service_err != '') {
            return false;
        }
    }
    /* Vehicle Service Validate */
    if (jQuery('.pam_vehicle_service').is(':visible')) {
        jQuery('.vehicle_appdivs').each(function () {
            vehicle_car_typess.push(jQuery(this).find('.location-selection-cartype .data-list .mp-value').html());
            vehicle_no_info_vehicle.push(jQuery(this).find('.no_of_vehicle').val());
            /* myarray.push($("#drop").val()); */
        });

        var vehicle_service_type = jQuery('input[name="vehicle_service"]:checked').val();
        var vehicle_service_err = '';
        var vehicle_service_quantity = jQuery('#vehicle_service_quantity').val();
        service_info['service'] = 'Vehicle';
        service_info['vehicle_qty'] = vehicle_service_quantity;

        if (vehicle_service_quantity == '' || isNaN(vehicle_service_quantity)) {
            vehicle_service_err += 'Y';
            jQuery('#vehicle_service_no_err').show();
        }

        if (vehicle_service_type == 'Car') {
            var vehicle_service_car_type = jQuery('#vehicle_service_car_type').val();

            service_info['vehicle_type'] = vehicle_car_typess;
            service_info['no_of_vehicle'] = vehicle_no_info_vehicle;
            if (vehicle_service_car_type == '') {
                vehicle_service_err += 'Y';
                jQuery('#vehicle_service_car_type_err').show();
            }
        }

        if (vehicle_service_err != '') {
            return false;
        }
    }
    /* Pets Service Validate */
    if (jQuery('.pam_pets_service').is(':visible')) {

        var count_var = 0;

        jQuery.each(jQuery(".pets_service"), function () {
            var value = jQuery(this).prop('checked');
            if (value) {
                var pet_multiple_div = {};
                var pets_service_err = '';
                var data_did = jQuery(this).data('div_id');

                var pet_service_value = jQuery(this).val();
                var pet_service_error = "pet_service_" + pet_service_value.toLowerCase();
                var pets_service_gen = jQuery('.pets_service_gen').val();
                var pet_name = jQuery('#pet_name' + data_did).val();
                var pet_breed = jQuery('#pet_breed' + data_did).val();
                var pets_gender = jQuery('input[name="pets_gender_1' + data_did + '"]:checked').val();
                var pet_age = jQuery('#pet_age' + data_did).val();
                var pets_age_radio = jQuery('#age_value' + data_did).val();

                var pet_weight = jQuery('#pet_weight' + data_did).val();
                var pet_weight_gen = jQuery('#weight_value' + data_did).val();

                pet_multiple_div['pets_service_gen'] = pets_service_gen;
                pet_multiple_div['pets_service'] = pet_service_value;
                pet_multiple_div['pet_name'] = pet_name;
                pet_multiple_div['pet_breed'] = pet_breed;
                pet_multiple_div['pets_gender'] = pets_gender;
                pet_multiple_div['pet_age'] = pet_age;
                pet_multiple_div['pets_age_radio'] = pets_age_radio;
                pet_multiple_div['pet_weight'] = pet_weight;
                pet_multiple_div['pet_weight_gen'] = pet_weight_gen;
                pet_multiple_div['service'] = 'Pets';


                if (pet_name == '') {
                    pets_service_err += 'Y';
                    jQuery('#pets_name_errs' + pet_service_error).show();
                }
                if (pet_breed == '') {
                    pets_service_err += 'Y';
                    jQuery('#pets_breed_errs' + pet_service_error).show();
                }
                if (pet_age == '' && !(intRegex.test(pet_age))) {
                    /* if(pet_age==''){ */
                    pets_service_err += 'Y';
                    jQuery('#pets_age_errs' + pet_service_error).show();
                }
                if (pet_weight == '' && !(intRegex.test(pet_age))) {
                    pets_service_err += 'Y';
                    jQuery('#pets_weight_errs' + pet_service_error).show();
                }
                if (pets_service_err != '') {
                    return false;
                }
                count_var++;
                var service_count = "pet_info" + count_var;

                service_info[service_count] = pet_multiple_div;

            }

        });
        console.log(service_info);
    }
    jQuery(document).ready(function () {
        jQuery("#pet_namepet_service_dog").keyup(function () {
            jQuery("#pets_name_errspet_service_dog").hide();
        });
        jQuery("#pet_breedpet_service_dog").keyup(function () {
            jQuery("#pets_breed_errspet_service_dog").hide();
        });
        jQuery("#pet_agepet_service_dog").keyup(function () {
            jQuery("#pets_age_errspet_service_dog").hide();
        });
        jQuery("#pet_weightpet_service_dog").keyup(function () {
            jQuery("#pets_weight_errspet_service_dog").hide();
        });


        jQuery("#pet_namepet_service_cat").keyup(function () {
            jQuery("#pets_name_errspet_service_cat").hide();
        });
        jQuery("#pet_breedpet_service_cat").keyup(function () {
            jQuery("#pets_breed_errspet_service_cat").hide();
        });
        jQuery("#pet_agepet_service_cat").keyup(function () {
            jQuery("#pets_age_errspet_service_cat").hide();
        });
        jQuery("#pet_weightpet_service_cat").keyup(function () {
            jQuery("#pets_weight_errspet_service_cat").hide();
        });


        jQuery("#pet_namepet_service_birds").keyup(function () {
            jQuery("#pets_name_errspet_service_birds").hide();
        });
        jQuery("#pet_breedpet_service_birds").keyup(function () {
            jQuery("#pets_breed_errspet_service_birds").hide();
        });
        jQuery("#pet_agepet_service_birds").keyup(function () {
            jQuery("#pets_age_errspet_service_birds").hide();
        });
        jQuery("#pet_weightpet_service_birds").keyup(function () {
            jQuery("#pets_weight_errspet_service_birds").hide();
        });


        jQuery("#pet_namepet_service_other").keyup(function () {
            jQuery("#pets_name_errspet_service_other").hide();
        });
        jQuery("#pet_breedpet_service_other").keyup(function () {
            jQuery("#pets_breed_errspet_service_other").hide();
        });
        jQuery("#pet_agepet_service_other").keyup(function () {
            jQuery("#pets_age_errspet_service_other").hide();
        });
        jQuery("#pet_weightpet_service_other").keyup(function () {
            jQuery("#pets_weight_errspet_service_other").hide();
        });


    });


    /* Validate Loading/Unloading Address Section */
    jQuery('#pam_loading_unloading_address').validate({
        rules: {
            pam_loading_street_address: {required: true},
            pam_loading_city: {required: true},
            pam_loading_state: {required: true},

            pam_unloading_street_address: {required: true},
            pam_unloading_city: {required: true},
            pam_unloading_state: {required: true}

        },
        messages: {
            pam_loading_street_address: {required: mpmain_error_obj.pam_loading_street_address},
            pam_loading_city: {required: mpmain_error_obj.pam_loading_city},
            pam_loading_state: {required: mpmain_error_obj.pam_loading_state},

            pam_unloading_street_address: {required: mpmain_error_obj.pam_loading_street_address},
            pam_unloading_city: {required: mpmain_error_obj.pam_loading_city},
            pam_unloading_state: {required: mpmain_error_obj.pam_loading_state}

        }
    });
    if (!jQuery('#pam_loading_unloading_address').valid()) {
        return false;
    }
    loading_info['pam_loading_street_address'] = jQuery('#pam_loading_street_address').val();
    loading_info['pam_loading_city'] = jQuery('#pam_loading_city').val();
    loading_info['pam_loading_state'] = jQuery('#pam_loading_state').val();


    /* cabs Info by trupal added by pradeep*/
    if (jQuery('#cab_want').is(':checked')) {
        var cab_lable_err = ""
        var to_cab_lable_err = ""
        var pam_street_address_cab = cabs_info['pam_street_address_cab'] = jQuery('#pam_loading_street_address_cab').val();
        var pam_loading_city_cab = cabs_info['pam_city_cab'] = jQuery('#pam_loading_city_cab').val();
        var pam_loading_state_cab = cabs_info['pam_state_cab'] = jQuery('#pam_loading_state_cab').val();
        var pam_member_cab = cabs_info['pam_member_cab'] = jQuery('#pam_member_cab').val();

        var cab_date = cabs_info['cab_date'] = jQuery('#from_cab_date').val();
        var to_pam_street_address_cab = cabs_info['to_pam_street_address_cab'] = jQuery('#to_pam_loading_street_address_cab').val();
        var to_pam_loading_city_cab = cabs_info['to_pam_city_cab'] = jQuery('#to_pam_loading_city_cab').val();
        var to_pam_loading_state_cab = cabs_info['to_pam_state_cab'] = jQuery('#to_pam_loading_state_cab').val();

        if (pam_street_address_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_address_err').show();
        }
        if (pam_loading_city_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_city_err').show();
        }
        if (pam_loading_state_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_state_err').show();
        }
        if (pam_member_cab == '') {
            cab_lable_err += 'Y';
            jQuery('#cab_member_err').show();
        }
        if (to_pam_street_address_cab == '') {
            to_cab_lable_err += 'Y';
            jQuery('#to_cab_address_err').show();
        }
        if (to_pam_loading_city_cab == '') {
            to_cab_lable_err += 'Y';
            jQuery('#to_cab_city_err').show();
        }
        if (to_pam_loading_state_cab == '') {
            to_cab_lable_err += 'Y';
            jQuery('#to_cab_state_err').show();
        }
        if (cab_lable_err != '') {
            return false;
        }
        if (to_cab_lable_err != '') {
            return false;
        }
    }


    unloading_info['pam_unloading_street_address'] = jQuery('#pam_unloading_street_address').val();
    unloading_info['pam_unloading_city'] = jQuery('#pam_unloading_city').val();
    unloading_info['pam_unloading_state'] = jQuery('#pam_unloading_state').val();
    /* unloading_info['pam_unloading_country'] = jQuery('#pam_unloading_country').val(); */


    /* Fetch Additional Info */
    if (additional_info_id != '') {
        var additional_info_ids = additional_info_id.split(',');
        for (var ids = 0; ids < additional_info_ids.length; ids++) {
            var additional_info_id = additional_info_ids[ids];

            if (jQuery('#additional_info' + additional_info_id).is(':checked')) {
                additional_info[additional_info_id] = 'Y';
            } else {
                additional_info[additional_info_id] = 'N';
            }
        }
    }

    /* Fetch Service Articles Info */
    if (jQuery('#mp_service_addons').is(':visible')) {
        jQuery('.addon-checkbox').each(function () {
            var curraddonid = jQuery(this).data('saddonid');
            if (jQuery(this).is(':checked')) {
                articles_info[curraddonid] = jQuery('#addonqty_' + curraddonid).val();
            }
        });
    }

    var current_stepdata = {
        'service_info': service_info,
        'loading_info': loading_info,
        'cabs_info': cabs_info,
        'unloading_info': unloading_info,
        'articles_info': articles_info,
        'additional_info': additional_info,
        action: 'set_steptwo_session'
    }

    /* Set Second Step Values In Session */
    jQuery("input").blur(function () {
        jQuery('#compare_error_msg').hide();
    });
    if (loading_info['pam_loading_street_address'] == unloading_info['pam_unloading_street_address']) {
        jQuery('#compare_error_msg').show();
        return false;
    } else {
        jQuery(".loader").show();
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: current_stepdata,
            success: function (response) {
                jQuery(".loader").hide();
            }
        });
    }
    jQuery('#mp_fourth_step').show();
    jQuery('#mp_second_step').hide();


});


jQuery(document).on("click", '#btn-checkout-step', function () {
    jQuery('.mp_terms_and_condition_error').hide();
    var errObj = mpmain_error_obj;
    var ajaxurl = mpmain_obj.plugin_path;
    var thankyou_url = mpmain_obj.thankyou_url;
    var mp_terms_and_condition_status = mpmain_obj.mp_terms_and_condition_status;
    var currstep = jQuery('.mp-booking-step').data('current');
    var terms_condition = jQuery("#mp-accept-conditions").prop("checked");

    if (mp_terms_and_condition_status == 'E' && terms_condition !== true) {
        jQuery('.mp_terms_and_condition_error').show();
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('.mp_terms_and_condition_error').offset().top - 80
        }, 800, 'swing', function () {
        });

        return false;
    }

    jQuery.validator.addMethod("pattern_phone", function (value, element) {
        return this.optional(element) || /^[0-9+]*$/.test(value);
    }, "Enter Only Numerics");

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    jQuery.validator.addMethod("pattern_email", function (value, element) {
        return this.optional(element) || re.test(value);
    }, "Enter Your Valid E-Mail");

    jQuery('#mp_newuser_form_validate').validate({
        rules: {
            'new_user_preferred_username': {
                required: true,
                pattern_email: true,
                remote: {
                    url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                    type: "POST",
                    async: false,
                    data: {
                        email: function () {
                            return jQuery("#new_user_preferred_username").val();
                        },
                        action: "check_existing_username"
                    }
                }
            },
            'new_user_preferred_email': {
                required: true,
                pattern_email: true
            },
            'new_user_preferred_password': {
                required: true,
                minlength: 8,
                maxlength: 30
            },
            'new_user_firstname': {
                required: true
            },
            'new_user_lastname': {
                required: true
            },
            'mp-phone': {
                required: true,
                minlength: 10,
                maxlength: 12,
            },
            'new_user_street_address': {
                required: true
            },
            'zipcode': {
                required: true,
                minlength: 5,
                maxlength: 7
            },
            'new_user_city': {
                required: true
            },
            'new_user_state': {
                required: true
            },
            'new_user_notes': {
                required: true
            },
        },
        messages: {
            'new_user_preferred_username': {
                required: errObj.Please_Enter_Email,
                pattern_email: errObj.Please_Enter_Valid_Email,
                remote: errObj.Email_already_exist
            },
            'new_user_preferred_email': {
                required: errObj.Please_Enter_Email,
                pattern_email: errObj.Please_Enter_Valid_Email
            },
            'new_user_preferred_password': {
                required: errObj.Please_Enter_Password,
                minlength: errObj.Please_enter_minimum_8_Characters,
                maxlength: errObj.Please_enter_maximum_30_Characters
            },
            'new_user_firstname': {
                required: errObj.Please_Enter_First_Name
            },
            'new_user_lastname': {
                required: errObj.Please_Enter_Last_Name
            },
            'mp-phone': {
                required: errObj.Please_Enter_Phone_Number,
                minlength: errObj.Please_enter_minimum_10_Characters,
                maxlength: errObj.Please_enter_maximum_14_Characters
            },
            'new_user_street_address': {
                required: errObj.Please_Enter_Address
            },
            'zipcode': {
                required: "Please Enter Zipcode",
                minlength: "Please Input 5 To 7 Characters",
                maxlength: "Please Input 5 To 7 Characters"
            },
            'new_user_city': {
                required: errObj.Please_Enter_City
            },
            'new_user_state': {
                required: errObj.Please_Enter_State
            },
            'new_user_notes': {
                required: errObj.Please_Enter_Notes
            },
        }
    });

    jQuery('.get_custom_field').each(function () {
        var name_field = jQuery(this).attr('name');
        var required_field = jQuery(this).data('required');
        var fieldlabel = jQuery(this).data('fieldlabel');
        if (required_field == "Y") {
            jQuery(this).rules("add", {
                required: true,
                messages: {required: errObj.Please_Enter + " " + fieldlabel + ""}
            });
        }
    });


    if (jQuery('#mp_newuser_form_validate').valid()) {
        jQuery(".loader").show();
        jQuery('.mp_terms_and_condition_error').hide();

        var pwd = jQuery('#new_user_preferred_password').val();
        var fname = jQuery('#new_user_firstname').val();
        var lname = jQuery('#new_user_lastname').val();
        var phone = jQuery('#mp-front-phone').val();
        var address = jQuery('#new_user_street_address').val();
        var city = jQuery('#new_user_city').val();
        var zipcode = jQuery('#zipcode').val();
        var state = jQuery('#new_user_state').val();
        var notes = jQuery('#new_user_notes').val();
        var user_type = jQuery('#pam_user_type').val();
        var check_gender = jQuery('.new_user_gender').prop('checked');
        var ccode = jQuery('#mp-front-phone').data('ccode');
        if (user_type == 'N') {
            var username = jQuery('#new_user_preferred_username').val();
        } else {
            var username = jQuery('#new_user_preferred_email').val();
        }

        if (user_type == 'G') {
            var mp_user_type = 'guest';
        } else if (user_type == 'E') {
            var mp_user_type = 'existing';
            if (jQuery('#mp_existing_login_btn').is(':visible')) {
                jQuery('#invalid_un_pwd').show();
                jQuery('html, body').stop().animate({
                    'scrollTop': jQuery('#invalid_un_pwd').offset().top - 80
                }, 800, 'swing', function () {
                });
                jQuery(".loader").hide();
                return false;
            }
        } else {
            var mp_user_type = 'new';
        }

        if (check_gender) {
            var gender = 'M';
        } else {
            var gender = 'F';
        }

        var dynamic_field_add = {};
        jQuery('.get_custom_field').each(function () {
            if (jQuery(this).data('fieldname') == "radio_group") {
                var radionames = jQuery(this).attr('name');
                dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery('input[name="' + radionames + '"]:checked').val();
            } else if (jQuery(this).data('fieldname') == "checkbox_group") {
                var checkboxnames = jQuery(this).attr('name');
                var checkvalue = '';
                jQuery('input[name="' + checkboxnames + '"]:checked').each(function () {
                    checkvalue += jQuery(this).val() + ',';
                });
                dynamic_field_add[jQuery(this).data('fieldlabel')] = checkvalue;
            } else {
                dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery(this).val();
            }
        });

        var dataString = {
            'username': username,
            'pwd': pwd,
            'fname': fname,
            'lname': lname,
            'phone': phone,
            'address': address,
            'zipcode': zipcode,
            'city': city,
            'state': state,
            'notes': notes,
            'mp_user_type': mp_user_type,
            'gender': gender,
            'dynamic_field_add': dynamic_field_add,
            'ccode': ccode,
            'action': 'mp_booking_complete'
        };

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: dataString,
            success: function (response) {
                jQuery(".loader").hide();
                if (thankyou_url != '') {
                    window.location.href = thankyou_url;

                } else {
                    jQuery('#mp_fourth_step').hide();
                    jQuery('#mp_third_step').show();
                }
            }
        });
    }
});
/* User Type Selection */
jQuery(document).on('click', '.new_and_existing_user_radio_btn', function () {
    var select_user = jQuery(this).val();
    jQuery('#pam_user_type').val(select_user);

    if (select_user == 'E') {
        jQuery('#guestexisting_user_email').show("blind", {direction: "vertical"}, 700);
        jQuery('.existing-user-login').show("blind", {direction: "vertical"}, 700);
        jQuery('.new-user-area').hide("blind", {direction: "vertical"}, 300);
        jQuery('.new-user-personal-detail-area').show("blind", {direction: "vertical"}, 300);
    } else if (select_user == 'G') {
        jQuery('#guestexisting_user_email').show("blind", {direction: "vertical"}, 700);
        jQuery('.existing-user-login').hide("blind", {direction: "vertical"}, 700);
        jQuery('.new-user-area').hide("blind", {direction: "vertical"}, 300);
        jQuery('.new-user-personal-detail-area').show("blind", {direction: "vertical"}, 300);
    } else {
        jQuery('.new-user-area').show("blind", {direction: "vertical"}, 700);
        jQuery('#guestexisting_user_email').hide("blind", {direction: "vertical"}, 300);
        jQuery('.existing-user-login').hide("blind", {direction: "vertical"}, 300);
        jQuery('.new-user-personal-detail-area').show("blind", {direction: "vertical"}, 700);
    }
});

/* Login Existing User */
jQuery(document).ready(function () {
    var errObj = mpmain_error_obj;
    jQuery('#existing-user').validate({
        rules: {
            'mp_existing_login_username_input': {required: true, email: true},
            'mp_existing_login_password_input': {required: true},
        },
        messages: {
            'mp_existing_login_username_input': {
                required: errObj.Please_Enter_Email,
                email: errObj.Please_Enter_Valid_Email
            },
            'mp_existing_login_password_input': {required: errObj.Please_Enter_Password},

        }
    });
});
jQuery(document).on('click', '#mp_existing_login_btn', function () {
    if (jQuery('#existing-user').valid()) {
        jQuery(".loader").show();
        var ajaxurl = mpmain_obj.plugin_path;
        var uname = jQuery('#mp_existing_login_username').val();
        var pwd = jQuery('#mp_existing_login_password').val();
        var dataString = {'uname': uname, 'pwd': pwd, 'action': 'get_existing_user_data'};
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: dataString,
            success: function (response) {
                jQuery(".loader").hide();
                if (jQuery.trim(response) != "Invalid Username or Password") {
                    var getdata = jQuery.parseJSON(response);
                    jQuery('.user-login-main').hide();
                    jQuery('.existing-user-login').hide();
                    jQuery('.existing-user-success-login-message').show();
                    jQuery('.new-user-personal-detail-area').show();
                    jQuery("#invalid_un_pwd").css("display", "none");
                    jQuery('.hide_new_user_login_details').hide();
                    jQuery('#logged_in_user_name').html(getdata.first_name + " " + getdata.last_name);

                    jQuery('#new_user_firstname').addClass("focus");
                    jQuery('#new_user_lastname').addClass("focus");
                    jQuery('#new_user_preferred_email').val(getdata.user_email);
                    jQuery('#mp-front-phone').addClass("focus");
                    jQuery('#new_user_street_address').addClass("focus");
                    jQuery('#zipcode').addClass("focus");
                    jQuery('#new_user_city').addClass("focus");
                    jQuery('#new_user_state').addClass("focus");
                    jQuery('#new_user_notes').addClass("focus");

                    jQuery('#new_user_preferred_password').val(getdata.password);
                    jQuery('#new_user_preferred_username').val(getdata.user_email);
                    jQuery('#new_user_firstname').val(getdata.first_name);
                    jQuery('#new_user_lastname').val(getdata.last_name);

                    jQuery('#mp-front-phone').intlTelInput("setNumber", getdata.phone);

                    jQuery('#mp-front-phone').attr('data-ccode', getdata.ccode);
                    jQuery('#new_user_street_address').val(getdata.address);
                    jQuery('#zipcode').val(getdata.user_zip);
                    jQuery('#new_user_city').val(getdata.city);
                    jQuery('#new_user_state').val(getdata.state);
                    jQuery('#new_user_notes').val(getdata.notes);

                    if (getdata.gender == 'M') {
                        jQuery('#mp-male').prop('checked', true);
                    } else {
                        jQuery('#mp-female').prop('checked', true);
                    }

                    jQuery('.error').each(function () {
                        jQuery(this).hide();
                    });
                } else {
                    jQuery("#invalid_un_pwd").css("display", "block");
                }
            }
        });
    }
});


/* other service validation focus */
jQuery(".flno_vali").focus(function () {
    jQuery("#other_floor_errs").hide();
});
jQuery(".art_err").focus(function () {
    jQuery("#other_article_errs").hide();
});
jQuery(".des_err").focus(function () {
    jQuery("#other_description_errs").hide();
});
jQuery(".des_err").focus(function () {
    jQuery("#other_description_errs").hide();
});

jQuery(document).ready(function () {
    var count_add = 0;
    var count_toc_icon_add = 0;

    var i = 2;
    jQuery('#add_plus').click(function () {
        count_add++;


        jQuery('#append_here').append('<div id="add_div' + count_add + '" class="mp-md-12 mp-lg-12 mp-sm-12 mp-xs-12"><div class="mp-form-row mp-md-3 mp-lg-3 mp-sm-11 mp-xs-11" style="margin-left: -8px;"><div class="pr margin_top_text"><input type="text" maxlength="5" name="other_service_article[]" class="custom-input other_service_article mt-13 art_err" id="other_service_article" value="" /><label class="custom">Article</label><i class="bottom-line"></i></div></div><div class="mp-form-row mp-md-1 mp-lg-1 mp-sm-1 mp-xs-1"><i class="fa fa-minus-circle add_plus fa-icon com-col" data-count_id=' + count_add + ' id="add_minus" style="margin-top: 65px;"></i></div></div>');
        i++;
    });
});
jQuery(document).ready(function () {
    var count_add = 0;
    var count_toc_icon_add = 0;

    var i = 2;
    jQuery('#add_vehicle').click(function () {
        count_add++;

        jQuery('#append_vehicle').append('<div class="vehicle_appdivs appand_sec" id="appdivs' + count_add + '"><div class="row mp-common-inputs fullwidth new-user-personal-detail-area car_sec" id="car_secc"><div class="mp-form-row mp-md-6 mp-lg-6 mp-sm-12 mp-xs-12 vehicle_service_car"><div class="mp-form-row p-7"><h3 class="block-title fs-18"><i class="vehicle-color-icon icons fs-18"></i> Car Type</h3><span id="vehicle_service_car_type_err" class="mp-error">Please choose car type</span><input type="hidden" value="" class="vehicle_service_car_type' + count_add + '" /><div id="cartype-select1" class="cus-location-cartype custom-input nmt cus-location-cartype_multi' + count_add + '"><div class="common-selection-main location-selection-cartype location-selection-cartype_multi' + count_add + '"><div class="selected-is select-location-cartype' + count_add + '" title="Choose Your Selection"><div style="padding: 18px 10px 10px 5px !important;" class="data-list selected_location-cartype' + count_add + '"><div class="mp-value">Please choose Car Type<input type="hidden" class="custom-input mt-13 input-new no_of_vehicle" name="vehicle_service_quantity[]" id="vehicle_service_quantity" value=""></div></div></div><ul class="common-data-dropdown location-dropdown-cartype location-dropdown-cartype_multi' + count_add + '"><li class="data-list select_location-cartype' + count_add + '" data-vals="XUV"><div class="mp-value" >SUV</div></li><li class="data-list select_location-cartype' + count_add + '" data-vals="Sedan"><div class="mp-value" >Sedan</div></li><li class="data-list select_location-cartype' + count_add + '" data-vals="Hatch back"><div class="mp-value" >Hatch back</div></li><li class="data-list select_location-cartype' + count_add + '" data-vals="MPV"><div class="mp-value" >MPV</div></li><li class="data-list select_location-cartype' + count_add + '" data-vals="Crossover"><div class="mp-value" >Crossover</div></li><li class="data-list select_location-cartype' + count_add + '" data-vals="Coupe"><div class="mp-value" >Coupe</div></li><li class="data-list select_location-cartype' + count_add + '" data-vals="Convertible"><div class="mp-value" >Convertible</div></li></ul></div></div><i class="bottom-line-car-type"></i></div></div><div class="mp-form-row mp-md-5 mp-lg-5 mp-sm-12 mp-xs-12 vehicle_service_bike"><div class="pr margin_top_text"><input type="text" class="custom-input mt-13 no_of_vehicle" name="vehicle_service_quantity[]" id="vehicle_service_quantity" value="" /><label class="custom">No. Of Vehicle</label><i class="bottom-line"></i></div><label id="vehicle_service_no_err" class="mp-relative mp-error">Please Enter Number Of Vehicle</label></div><div class="mp-form-row mp-md-1 mp-lg-1 mp-sm-12 mp-xs-12 vehicle_service_bike" style="line-height: 70px;!important"><i class="fa fa-minus-circle add_plus fa-icon com-col" data-count_id=' + count_add + ' id="append_vehicle_minus"></i></div></div></div>');
        i++;
        /* Car Type */
        jQuery(document).on("click", ".select-location-cartype" + count_add, function () {
            jQuery(".cus-location-cartype_multi" + count_add).addClass('focus');
            jQuery(".location-selection-cartype_multi" + count_add).addClass('clicked');
            jQuery(".location-dropdown-cartype" + count_add).addClass('bounceInUp');

        });

        jQuery(document).on("click", ".select_location-cartype" + count_add, function () {
            jQuery(".vehicle_service_car_type" + count_add).val(jQuery(this).find('.mp-value').text());
            jQuery(".cus-location-cartype_multi" + count_add).removeClass('focus');
            jQuery(".location-selection-cartype_multi" + count_add).removeClass('clicked');
            jQuery(".location-dropdown-cartype" + count_add).removeClass('bounceInUp');
            jQuery(".selected_location-cartype" + count_add).html(jQuery(this).html());
        });
    });


});
jQuery(document).on('click', '#add_minus', function () {
    var app_div_id = jQuery(this).data('count_id');
    jQuery('#add_div' + app_div_id).remove();

});
jQuery(document).on('click', '#append_vehicle_minus', function () {
    var app_div_id = jQuery(this).data('count_id');
    jQuery('#appdivs' + app_div_id).remove();

});

/* From add as above for cabs info*/
jQuery(function () {
    jQuery("#add_as_above").click(function () {
        if (jQuery(this).is(':checked')) {
            var pam_loading_street_address = jQuery("#pam_loading_street_address").val();
            var pam_loading_city = jQuery("#pam_loading_city").val();
            var pam_loading_state = jQuery("#pam_loading_state").val();
            jQuery("#pam_loading_street_address_cab").val(pam_loading_street_address);
            jQuery("#pam_loading_city_cab").val(pam_loading_city);
            jQuery("#pam_loading_state_cab").val(pam_loading_state);
            jQuery("#cab_address_err").hide();
            jQuery("#cab_city_err").hide();
            jQuery("#cab_state_err").hide();
        } else {
            var pam_loading_street_address = "";
            var pam_loading_city = "";
            var pam_loading_state = "";
            jQuery("#pam_loading_street_address_cab").val(pam_loading_street_address);
            jQuery("#pam_loading_city_cab").val(pam_loading_city);
            jQuery("#pam_loading_state_cab").val(pam_loading_state);
        }
    });
});
/* To add as above for cabs info*/
jQuery(function () {
    jQuery("#to_add_as_above").click(function () {
        if (jQuery(this).is(':checked')) {
            var pam_unloading_street_address = jQuery("#pam_unloading_street_address").val();
            var pam_unloading_city = jQuery("#pam_unloading_city").val();
            var pam_unloading_state = jQuery("#pam_unloading_state").val();
            jQuery("#to_pam_loading_street_address_cab").val(pam_unloading_street_address);
            jQuery("#to_pam_loading_city_cab").val(pam_unloading_city);
            jQuery("#to_pam_loading_state_cab").val(pam_unloading_state);
            jQuery("#to_cab_address_err").hide();
            jQuery("#to_cab_city_err").hide();
            jQuery("#to_cab_state_err").hide();
        } else {
            var pam_unloading_street_address = "";
            var pam_unloading_city = "";
            var pam_unloading_state = "";
            jQuery("#to_pam_loading_street_address_cab").val(pam_unloading_street_address);
            jQuery("#to_pam_loading_city_cab").val(pam_unloading_city);
            jQuery("#to_pam_loading_state_cab").val(pam_unloading_state);
        }
    });
});
jQuery(".ad_err").focus(function () {
    jQuery("#cab_address_err").hide();
});
jQuery(".city_err").focus(function () {
    jQuery("#cab_city_err").hide();
});
jQuery(".state_err").focus(function () {
    jQuery("#cab_state_err").hide();
});
jQuery(".to_ad_err").focus(function () {
    jQuery("#to_cab_address_err").hide();
});
jQuery(".to_city_err").focus(function () {
    jQuery("#to_cab_city_err").hide();
});
jQuery(".to_state_err").focus(function () {
    jQuery("#to_cab_state_err").hide();
});
jQuery(".mem_err").focus(function () {
    jQuery("#cab_member_err").hide();
});

/* check or uncheck want cabs info */
jQuery(function () {
    jQuery("#cab_want").click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery(".caps_form").show();
        } else {
            jQuery(".caps_form").hide();
        }
    });
});
/* Hide Show add button of vehicle type */
jQuery(document).on('click', '#vehicle_service_bike', function () {
    jQuery('#add_vehicle').hide();
});

jQuery(document).on('click', '#vehicle_service_car', function () {
    jQuery('#add_vehicle').show();
});

jQuery(document).ready(function () {
    jQuery('#btn-fourth-step').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 30
        }, 800, 'swing', function () {
        });
    });
});
jQuery(document).ready(function () {
    jQuery('#fourth').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 30
        }, 800, 'swing', function () {
        });
    });
});
jQuery(document).ready(function () {
    jQuery('#btn-second-step').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 30
        }, 800, 'swing', function () {
        });
    });
});
jQuery(document).ready(function () {
    jQuery('#second').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 30
        }, 800, 'swing', function () {
        });
    });
});
jQuery(document).ready(function () {
    jQuery('#btn-first-step').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 30
        }, 800, 'swing', function () {
        });
    });
});
jQuery(document).ready(function () {
    jQuery('#first').on('click', function () {
        jQuery('html, body').stop().animate({
            'scrollTop': jQuery('#mp-main').offset().top - 30
        }, 800, 'swing', function () {
        });
    });
});
jQuery('#third').on('click', function () {
    jQuery('html, body').stop().animate({
        'scrollTop': jQuery('#mp-main').offset().top - 30
    }, 800, 'swing', function () {
    });
});


jQuery(document).bind('ready ajaxComplete', function () {
    var ajaxurl = mpmain_obj.plugin_path;


    jQuery.validator.addMethod("pattern_phone", function (value, element) {
        return this.optional(element) || /^[0-9+]*$/.test(value);
    }, "Enter Only Numerics");

    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    jQuery.validator.addMethod("pattern_email", function (value, element) {
        return this.optional(element) || re.test(value);
    }, "Enter Your Valid E-Mail");

    jQuery('#validate_form').validate({
        rules: {

            'valid_source': {
                required: true
            },
            'valid_via': {
                required: true
            },
            'valid_destination': {
                required: true
            },
            'valid_first_name': {
                required: true
            },
            'valid_last_name': {
                required: true
            },
            'valid_email': {
                required: true,
                pattern_email: true
            },
            'valid_phone': {
                required: true,
                pattern_phone: true,
                minlength: 10,
                maxlength: 11
            },
            'valid_date': {
                required: true
            }
        },
        messages: {

            'valid_source': {
                required: mpmain_error_obj.Please_Enter_Source_City
            },
            'valid_via': {
                required: mpmain_error_obj.Please_Enter_Via_City
            },
            'valid_destination': {
                required: mpmain_error_obj.Please_Enter_Destination_City
            },
            'valid_first_name': {
                required: mpmain_error_obj.Please_Enter_Name
            },
            'valid_last_name': {
                required: mpmain_error_obj.Please_Enter_Name
            },
            'valid_email': {
                required: mpmain_error_obj.Please_Enter_Email,
                pattern_email: mpmain_error_obj.Please_Enter_Valid_Email
            },
            'valid_phone': {
                required: mpmain_error_obj.Please_Enter_Phone_Number,
                minlength: mpmain_error_obj.Please_enter_minimum_10_Characters,
                maxlength: mpmain_error_obj.Please_enter_maximum_11_Characters,
                pattern_phone: mpmain_error_obj.Please_Enter_Number
            },
            'valid_date': {
                required: mpmain_error_obj.Please_Select_Date
            },
        }
    });
});

jQuery(document).bind('ready ajaxComplete', function () {
    var ajaxurl = mpmain_obj.plugin_path;

    var loading_floor = mpmain_error_obj.Please_Select_Loading_Floor;
    var loading_street = mpmain_error_obj.Please_Enter_Loading_Street;
    var loading_city = mpmain_error_obj.Please_Enter_Loading_City;
    var loading_state = mpmain_error_obj.Please_Enter_Loading_State;

    var unloading_floor = mpmain_error_obj.Please_Select_UnLoading_Floor;
    var unloading_street = mpmain_error_obj.Please_Enter_UnLoading_Street;
    var unloading_city = mpmain_error_obj.Please_Enter_UnLoading_City;
    var unloading_state = mpmain_error_obj.Please_Enter_UnLoading_State;

    jQuery('#mp_third_step_validate').validate({
        rules: {
            flool_count: {
                required: true
            },
            street_address: {
                required: true
            },
            city_address: {
                required: true
            },
            state_address: {
                required: true
            },
            unflool_count: {
                required: true
            },
            unload_street_addr: {
                required: true
            },
            unload_city_addr: {
                required: true
            },
            unload_state_addr: {
                required: true
            }
        },
        messages: {
            flool_count: {required: loading_floor},
            street_address: {required: loading_street},
            city_address: {required: loading_city},
            state_address: {required: loading_state},
            unflool_count: {required: unloading_floor},
            unload_street_addr: {required: unloading_street},
            unload_city_addr: {required: unloading_city},
            unload_state_addr: {required: unloading_state}
        }
    });
});
jQuery(document).ready(function () {

    jQuery("#third_step_progress, #forth_step_progress").prop("disabled", true);
    jQuery("#third_step_progress, #forth_step_progress").css('cursor', 'unset');

});
jQuery(document).on('click', '#first_step_progress', function (event) {

    jQuery('#mp_second_step').slideUp(500);
    jQuery('#mp_first_step').slideDown(500);
    jQuery('#mp_third_step').slideUp(500);
    jQuery("#third_step_progress, #forth_step_progress").prop("disabled", true);
    jQuery("#third_step_progress, #forth_step_progress").css('cursor', 'unset');

    page = "first";


});

/*jQuery(document).on('click','.move_type',function(event){
var mttype = jQuery("input[name='MT_Radio']:checked").val();
if(mttype == 'House'){
	jQuery('.move_size_sh').show();
}else{
	jQuery('.move_size_sh').hide();
}

});*/

jQuery(document).on('click', '#first_step_submit', function (event) {
    jQuery("#second_step_progress").trigger("click");
});


jQuery(document).on('click', '#second_step_progress', function (event) {
    if (page == "first") {

        if (!jQuery('#validate_form').valid() || total_distance == 0) {
            jQuery("#first_step_progress").trigger("click");
            return false;
        }

        var moveto_cubic_meter = moveto_cubic_unit;
        if (moveto_cubic_meter == 'm_3') {
            var label_cubic_unit = 'Cubic Meter';
        } else {
            var label_cubic_unit = 'Cubic Feet';
        }
        var date = jQuery("#date").val();
        var bhk = jQuery("input[name='BHK_Radio']:checked").val();
        // if(bhk) // ! Removed to true check
        // {	jQuery("#first_step_progress").trigger("click");
        // 	jQuery('.error_msg_movesize').html("Please select move size");
        // 	return false;
        // }else{
        // 	jQuery('.error_msg_movesize').html("");
        // }
        if (!date) {
            jQuery("#first_step_progress").trigger("click");
            jQuery('.error_msg_date').html("Please select date");
            return false;
        } else {
            jQuery('.error_msg_date').html("");
        }
        if (jQuery('#validate_form').valid()) {
            var ajaxurl = mpmain_obj.plugin_path;
            var source_city = jQuery("#source_city").val();
            var via_city = jQuery("#via_city").val();
            var destination_city = jQuery("#destination_city").val();

            var name = jQuery("#name").val();
            var email = jQuery("#email").val();
            var phone = jQuery("#phone").val();

            var currency_symbol = jQuery('#dollar').val();
            jQuery(".loader").show();

            bhk = "";
            var servicedata = {
                source_city: source_city,
                destination_city: destination_city,
                via_city: via_city,
                bhk: bhk,
                name: name,
                email: email,
                phone: phone,
                date: date,
                action: 'first_step_data'
            };

            jQuery.ajax({
                type: "POST",
                url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                dataType: 'html',
                data: servicedata,
                success: function (response) {
                    page = "second";
                    var cart_data_send = {
                        action: 'cart_insert_data'
                    };


                    jQuery.ajax({
                        type: "POST",
                        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                        dataType: 'html',
                        data: cart_data_send,
                        success: function (resp) {
                            var res_split = resp.split("-");
                            jQuery('.lod_esti_title').html("Loading Estimation");
                            jQuery('.load_floor').html("Floor Price");
                            jQuery('.third_step_val_div').html(res_split[0]);
                            jQuery('.unlod_esti_title').html("Unloading Estimation");
                            jQuery('.unload_floor').html("Floor Price");
                            jQuery('.third_step_val_div_unload').html(res_split[1]);
                            jQuery('.elevator').html("Elevator Price");
                            jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
                            jQuery('.unelevator').html("Elevator Price");
                            jQuery('.unload_ele').html("-" + res_split[3]);
                            jQuery('.paird_step_val_div_packing').html(res_split[4]);
                            jQuery('.unpcking').html("Packaging Price");
                            jQuery('.thacking').html("Unpackaging Price");
                            jQuery('.third_step_val_div_unpacking').html(res_split[5]);
                            jQuery('.discount_label').html("Discount");
                            jQuery('.discounted_amount').html("-" + res_split[6]);
                            jQuery('.tax_label').html("GST/TAX");
                            jQuery('.taxed_amount').html(res_split[7]);
                            jQuery('.est_cost').html("Total");
                            jQuery('.total_cost').html(res_split[8]);
                            jQuery('.subtotal_label').html("Subtotal");
                            jQuery('.subtotal_amount').html(res_split[9]);
                            jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
                            jQuery('.cart_cubic_value').html(res_split[10]);
                            jQuery('.cart_cubic_volume').html(res_split[11]);
                            //jQuery('.insurance_rate').html(res_split[12]);
                            //jQuery('.insurance_rate').hide();
                            jQuery('.insurance_data').remove();


                            jQuery(".loader").hide();

                        }
                    });
                    jQuery("#third_step_progress").prop("disabled", false);
                    jQuery("#third_step_progress").css('cursor', 'pointer');
                    jQuery('#mp_first_step').slideUp(500);
                    jQuery('#mp_second_step').slideDown(500);
                }
            });
        }
    } else if (page == "third") {
        page = "second";
        jQuery('#mp_third_step').slideUp(500);
        jQuery('#mp_second_step').slideDown(500);
    }
});


/*jQuery(document).on('click','#second_step_progress',function(event){
if(page == "first"){
if(jQuery(".move_size_sh").is(":visible")){
bhk_vis = true;
var bhk = jQuery("input[name='BHK_Radio']:checked"). val();
} else{
bhk_vis = false;
var bhk = "";
}
var move_type = jQuery("input[name='MT_Radio']:checked").val();
var date = jQuery("#date").val();
if(!jQuery('#validate_form').valid() || total_distance == 0 || !date || (!bhk && bhk_vis == true) || !move_type){
jQuery("#first_step_progress").trigger("click");

if(!date){
jQuery('.error_msg_date').html("Please select date");
}else{
jQuery('.error_msg_date').html("");
}

if(!move_type){
jQuery('.error_msg_movetype').html("Please select move type");
jQuery('.error_msg_movetype').show();
}else{
jQuery('.error_msg_movetype').html("");
jQuery('.error_msg_movetype').hide();
}

if(!bhk && bhk_vis == true){
jQuery('.error_msg_movesize').html("Please select move size");
jQuery('.error_msg_movesize').show();
return false;
}else{
jQuery('.error_msg_movesize').html("");
jQuery('.error_msg_movesize').hide();
}
jQuery("#first_step_progress").trigger("click");
return false;
}else{
jQuery('.error_msg_movesize').html("");
jQuery('.error_msg_date').html("");
jQuery('.error_msg_movetype').html("");
jQuery('.error_msg_movetype').hide();
jQuery('.error_msg_movesize').hide();
}


if(jQuery('#validate_form').valid()){
var ajaxurl = mpmain_obj.plugin_path;
var source_city = jQuery("#source_city").val();
var via_city = jQuery("#via_city").val();
var destination_city = jQuery("#destination_city").val();

var name = jQuery("#name").val();
var email = jQuery("#email").val();
var phone = jQuery("#phone").val();

var currency_symbol=jQuery('#dollar').val();
jQuery(".loader").show();


var servicedata = {
source_city:source_city,
destination_city:destination_city,
via_city:via_city,
bhk:bhk,
move_type:move_type,
name:name,
email:email,
phone:phone,
date:date,
action:'first_step_data'};

jQuery.ajax({
type :"POST",
url :ajaxurl+"/assets/lib/mp_front_ajax.php",
dataType :'html',
data :servicedata,
success:function(response){
page = "second";
var responseSplit = response.split("&");
if(responseSplit[0] == 'different'){
jQuery('.cart_li').empty();
}


jQuery('#roomtype_data').html(responseSplit[1]);
jQuery("#first_cat").trigger("click");
jQuery("#first_cat").addClass("active");

var cart_data_send = {
action:'cart_insert_data'
};
jQuery.ajax({
type:"POST",
url : ajaxurl+"/assets/lib/mp_front_ajax.php",
dataType : 'html',
data:cart_data_send,
success:function(resp){
var res_split = resp.split("-");
jQuery('.lod_esti_title').html("Loading Estimation");
jQuery('.load_floor').html("Floor Price");
jQuery('.third_step_val_div').html(res_split[0]);
jQuery('.unlod_esti_title').html("Unloading Estimation");
jQuery('.unload_floor').html("Floor Price");
jQuery('.third_step_val_div_unload').html(res_split[1]);
jQuery('.elevator').html("Elevator Price");
jQuery('.third_step_val_div_elevators').html("-"+res_split[2]);
jQuery('.unelevator').html("Elevator Price");
jQuery('.unload_ele').html("-"+res_split[3]);
jQuery('.packing').html("Packaging Price");
jQuery('.third_step_val_div_packing').html(res_split[4]);
jQuery('.unpacking').html("Unpackaging Price");
jQuery('.third_step_val_div_unpacking').html(res_split[5]);
jQuery('.discount_label').html("Discount");
jQuery('.discounted_amount').html("-"+res_split[6]);
jQuery('.tax_label').html("GST/TAX");
jQuery('.taxed_amount').html(res_split[7]);
jQuery('.est_cost').html("Total");
jQuery('.total_cost').html(res_split[8]);
jQuery('.subtotal_label').html("Subtotal");
jQuery('.subtotal_amount').html(res_split[9]);

}
});
jQuery("#third_step_progress").prop("disabled" , false);
jQuery("#third_step_progress").css( 'cursor', 'pointer' );
jQuery(".loader").hide();
jQuery('#mp_first_step').slideUp(500);
jQuery('#mp_second_step').slideDown(500);
}
});
}
}else if(page == "third"){
page = "second";
jQuery('#mp_third_step').slideUp(500);
jQuery('#mp_second_step').slideDown(500);
}
}); */

/**GET ARTICALES AND ROOM TYPE DATA **/

jQuery(document).on('click', '.room_type_cat', function () {
    jQuery(document).ready(() => {
        jQuery("#amt_val").val('Select Insurance');
    });
    var ajaxurl = mpmain_obj.plugin_path;
    var id = jQuery(this).attr('data-id');
    var name = jQuery(this).attr('data-name');

    jQuery(".loader").show();
    var postdata = {
        id: id,
        name: name,
        action: 'get_article_type'
    };
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: postdata,
        success: function (response) {
            jQuery('.articles_tab').html(response);
            var check_li = true;

            jQuery(".cart_li .room_type").each(function () {
                var li_id = jQuery(this).attr("data-li_id");

                var ul_check_id = "articles_ul_" + li_id;
                if (jQuery("." + ul_check_id + " li").length == 0) {
                    jQuery('.cart_check_' + li_id).remove();
                }
                if (id == li_id) {
                    check_li = false;
                }
            });
            jQuery(".loader").hide();

        }

    });

});

/** MINUS BUTTON FUNCTION FOR STEP TWO **/

jQuery(document).on('click', '.minus', function (event) {
    jQuery(document).ready(() => {
        jQuery("#amt_val").val('Select Insurance');
    });
    var ajaxurl = mpmain_obj.plugin_path;
    var $input = jQuery(this).parent().find('input');
    var currency_symbol = jQuery('#dollar').val();
    var count = parseInt($input.val()) - 1;
    if (count > -1) {
        count = count < 1 ? 0 : count;
        $input.val(count);
        $input.change();

        var moveto_cubic_meter = moveto_cubic_unit;
        if (moveto_cubic_meter == 'm_3') {
            var label_cubic_unit = 'Cubic Meter';
        } else {
            var label_cubic_unit = 'Cubic Feet';
        }

        var get_article_room_id = jQuery(this).attr('data-room_id');
        var get_article_name = jQuery(this).attr('data-name');
        var get_article_id = jQuery(this).attr('data-id');
        var get_category_id = jQuery(this).attr('data-catid');
        var get_article_price = jQuery(this).attr('data-price');
        var get_article_caltype = jQuery(this).attr('data-caltype');
        var get_article_cubic = jQuery(this).attr('data-cubic');
        var cubic_count = '0';
        jQuery('.loader').show();
        jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop('checked', true);
        var check_li = true;
        jQuery(".articles_ul_" + get_article_room_id).find('li').each(function () {
            var li_id = jQuery(this).attr("data-li_id");
            if (get_article_id + "" + get_category_id == li_id) {
                check_li = false;
            }
        });

        if (count == 0) {
            jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop('checked', false);
            jQuery(".cart_check_" + get_article_id + get_article_room_id + get_category_id).html();

            jQuery(".cart_check_" + get_article_id + get_article_room_id + get_category_id).remove();
            var counts = 0;
            jQuery(".num_" + get_article_room_id + get_article_id + get_category_id).val(counts);

            var li_id = jQuery(this).attr("data-li_id");

            jQuery(".cart-value_" + get_article_id + get_article_room_id + get_category_id).html();
            jQuery(".title_" + get_article_id + get_article_room_id).html();

        }
        if (check_li) {

            var return_string = '<li class="cart-listing cart_check_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '" data-li_id="' + get_article_id + '">' +
                '<h3 class="cart-title">' + get_article_name + '</h3>' +
                '<h4 class="cart-value_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + count + '</h4>' +
                //'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+get_article_price+'</h4>'+
                '</li>';
            jQuery(".articles_ul_" + get_article_room_id).append(return_string);

        } else {

            var return_string = '<h4 class="cart-value_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + count + '</h4></li>';
            jQuery(".cart-value_" + get_article_id + get_article_room_id + get_category_id).html(return_string);
            var return_total = '<h4 class="cart-total_' + get_article_id + '' + get_article_room_id + '">' + currency_symbol + count * get_article_price + '</h4>' +
                jQuery(".cart-cost").html(return_total);
        }
        var cart_data_send = {
            get_article_room_id: get_article_room_id,
            get_article_id: get_article_id,
            get_article_name: get_article_name,
            count: count,
            cubic_count: cubic_count,
            get_article_cubic: get_article_cubic,
            get_article_price: get_article_price,
            get_article_cat_id: get_category_id,
            get_article_caltype: get_article_caltype,
            action: 'cart_insert_data'
        };


        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: cart_data_send,
            success: function (response) {
                jQuery('.loader').hide();
                var res_split = response.split("-");
                jQuery('.lod_esti_title').html("Loading Estimation");
                jQuery('.load_floor').html("Floor Price");
                jQuery('.third_step_val_div').html(res_split[0]);
                jQuery('.unlod_esti_title').html("Unloading Estimation");
                jQuery('.unload_floor').html("Floor Price");
                jQuery('.third_step_val_div_unload').html(res_split[1]);
                jQuery('.elevator').html("Elevator Price");
                jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
                jQuery('.unelevator').html("Elevator Price");
                jQuery('.unload_ele').html("-" + res_split[3]);
                jQuery('.packing').html("Packaging Price");
                jQuery('.third_step_val_div_packing').html(res_split[4]);
                jQuery('.unpacking').html("Unpackaging Price");
                jQuery('.third_step_val_div_unpacking').html(res_split[5]);
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + res_split[6]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(res_split[7]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(res_split[8]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(res_split[9]);
                jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
                jQuery('.cart_cubic_value').html(res_split[10]);
                jQuery('.cart_cubic_volume').html(res_split[11]);
                //jQuery('.insurance_rate').html(res_split[12]);
                //jQuery('.insurance_rate').hide();
                jQuery('.insurance_data').remove();

            }
        });

    }
    var article_div = jQuery(".articles_ul_" + get_article_room_id).text()
    if (article_div == '') {
        jQuery(".cart_check_" + get_article_room_id).remove();
    }

    var summaryData = jQuery(".cart_li").text();
    if (jQuery.trim(summaryData) == '') {
        jQuery(".r-cart-a").hide();
    }
});

/** PLUS BUTTON FUNCTION FOR STEP TWO **/

jQuery(document).on('click', '.plus', function (event) {
    jQuery(document).ready(() => {
        jQuery("#amt_val").val('Select Insurance');
    });
    var max_qty = parseInt(jQuery(this).attr("data-max_qty")) + 1;
    var ajaxurl = mpmain_obj.plugin_path;
    var $input = jQuery(this).parent().find('input');
    var currency_symbol = jQuery('#dollar').val();
    var moveto_cubic_meter = moveto_cubic_unit;
    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }

    var count = parseInt($input.val()) + 1;
    if (count == max_qty) {
        return false;
    }
    $input.val(count);
    $input.change();

    var get_article_room_id = jQuery(this).attr('data-room_id');
    var get_article_name = jQuery(this).attr('data-name');
    var get_article_id = jQuery(this).attr('data-id');
    var get_category_id = jQuery(this).attr('data-catid');
    var get_article_price = jQuery(this).attr('data-price');
    var get_article_cubic = jQuery(this).attr('data-cubic');
    var cubic_count = '1';

    var room_name = jQuery(this).attr('data-room');
    var get_article_caltype = jQuery(this).attr('data-caltype');
    jQuery('.loader').show();

    if (jQuery('.cart_check_' + get_article_room_id).length == 0) {
        var return_string = '<li class="cart-listing room_type cart_check_' + get_article_room_id + '" data-li_id="' + get_article_room_id + '">' +
            '<h3 class="cart-title cart_title">' + room_name + '</h3>' +
            '<ul class="articles_ul_' + get_article_room_id + '"></ul>' +
            '</li>';
        jQuery(".cart_li").append(return_string);
    }

    jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop('checked', true);
    var check_li = true;
    jQuery(".articles_ul_" + get_article_room_id).find('li').each(function () {
        var li_id = jQuery(this).attr("data-li_id");
        if (get_article_id + "" + get_category_id == li_id) {
            check_li = false;
        }
    });
    if (check_li) {

        var return_string = '<li class="cart-listing cart_check_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '" data-li_id="' + get_article_id + '' + get_category_id + '">' +
            '<h3 class="cart-title title_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + get_article_name + '</h3>' +
            '<h4 class="cart-value_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + count + '</h4>' +
            /*'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+get_article_price+'</h4>'+*/
            '</li>';
        jQuery(".articles_ul_" + get_article_room_id).append(return_string);

        jQuery('.r-cart-a').show();
    } else {

        var return_string = '<h4 class="cart-value_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + count + '</h4></li>';
        /*var return_total =	'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+'$'+count*get_article_price+'</h4>'+*/
        jQuery(".cart-value_" + get_article_id + get_article_room_id + get_category_id).html(return_string);

    }
    var cart_data_send = {
        get_article_room_id: get_article_room_id,
        get_article_id: get_article_id,
        get_article_name: get_article_name,
        count: count,
        cubic_count: cubic_count,
        get_article_cubic: get_article_cubic,
        get_article_price: get_article_price,
        get_article_cat_id: get_category_id,
        get_article_caltype: get_article_caltype,
        action: 'cart_insert_data'
    };

    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        dataType: 'html',
        data: cart_data_send,
        success: function (response) {
            jQuery('.loader').hide();
            var res_split = response.split("-");
            jQuery('.lod_esti_title').html("Loading Estimation");
            jQuery('.load_floor').html("Floor Price");
            jQuery('.third_step_val_div').html(res_split[0]);
            jQuery('.unlod_esti_title').html("Unloading Estimation");
            jQuery('.unload_floor').html("Floor Price");
            jQuery('.third_step_val_div_unload').html(res_split[1]);
            jQuery('.elevator').html("Elevator Price");
            jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
            jQuery('.unelevator').html("Elevator Price");
            jQuery('.unload_ele').html("-" + res_split[3]);
            jQuery('.packing').html("Packaging Price");
            jQuery('.third_step_val_div_packing').html(res_split[4]);
            jQuery('.unpacking').html("Unpackaging Price");
            jQuery('.third_step_val_div_unpacking').html(res_split[5]);
            jQuery('.discount_label').html("Discount");
            jQuery('.discounted_amount').html("-" + res_split[6]);
            jQuery('.tax_label').html("GST/TAX");
            jQuery('.taxed_amount').html(res_split[7]);
            jQuery('.est_cost').html("Total");
            jQuery('.total_cost').html(res_split[8]);
            jQuery('.subtotal_label').html("Subtotal");
            jQuery('.subtotal_amount').html(res_split[9]);
            jQuery('.cubic_feets').html(label_cubic_unit + " Rate");
            jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
            jQuery('.cart_cubic_value').html(res_split[10]);
            jQuery('.cart_cubic_volume').html(res_split[11]);
            //jQuery('.insurance_rate').html(res_split[12]);
            //jQuery('.insurance_rate').hide();
            jQuery('.insurance_data').remove();


        }
    });

});

/** GET ARTICALES DATA FOR STEP TWO **/

jQuery(document).on('click', '.get_article', function (event) {
    jQuery(document).ready(() => {
        jQuery("#amt_val").val('Select Insurance');
    });
    var get_article_room_id = jQuery(this).attr('data-room_id');
    var get_article_id = jQuery(this).attr('data-id');
    var get_category_id = jQuery(this).attr('data-catid');
    var get_article_name = jQuery(this).attr('data-name');
    var room_name = jQuery(this).attr('data-room');
    var get_article_cubic = jQuery(this).attr('data-cubic');

    var moveto_cubic_meter = moveto_cubic_unit;
    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }
    if (jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop("checked") == true) {
        if (jQuery('.cart_check_' + get_article_room_id).length == 0) {

            var return_string = '<li class="cart-listing room_type cart_check_' + get_article_room_id + '" data-li_id="' + get_article_room_id + '">' +
                '<h3 class="cart-title cart_title">' + room_name + '</h3>' +
                '<ul class="articles_ul_' + get_article_room_id + '"></ul>' +
                '</li>';
            jQuery(".cart_li").append(return_string);
        }
        var counts = 1;
        jQuery("#plus" + get_article_room_id + get_article_id + get_category_id).trigger("click");
        jQuery(".num_" + get_article_room_id + get_article_id + get_category_id).val(counts);
        jQuery('.r-cart-a').show();

    } else if (jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop("checked") == false) {
        var cubic_count = 0;
        var counts = 0;
        jQuery(".num_" + get_article_room_id + get_article_id + get_category_id).val(counts);


        var ajaxurl = mpmain_obj.plugin_path;
        currency_symbol = jQuery('#dollar').val();
        var count = 0;
        if (count > -1) {
            count = count < 1 ? 0 : count;

            var get_article_price = jQuery(this).attr('data-price');
            var get_article_caltype = jQuery(this).attr('data-caltype');

            jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop('checked', true);
            var check_li = true;
            jQuery(".articles_ul_" + get_article_room_id).find('li').each(function () {
                var li_id = jQuery(this).attr("data-li_id");
                if (get_article_id + "" + get_category_id == li_id) {
                    check_li = false;
                }
            });

            if (count == 0) {
                jQuery('.get_' + get_article_id + '' + get_category_id + '' + get_article_room_id).prop('checked', false);
                jQuery(".cart_check_" + get_article_id + get_article_room_id + get_category_id).html();

                jQuery(".cart_check_" + get_article_id + get_article_room_id + get_category_id).remove();
                var counts = 0;
                jQuery(".num_" + get_article_room_id + get_article_id + get_category_id).val(counts);

                var li_id = jQuery(this).attr("data-li_id");

                jQuery(".cart-value_" + get_article_id + get_article_room_id + get_category_id).html();
                jQuery(".title_" + get_article_id + get_article_room_id).html();

            }
            if (check_li) {

                var return_string = '<li class="cart-listing cart_check_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '" data-li_id="' + get_article_id + '">' +
                    '<h3 class="cart-title">' + get_article_name + '</h3>' +
                    '<h4 class="cart-value_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + count + '</h4>' +
                    /*'<h4 class="cart-total_'+get_article_id+''+get_article_room_id+'">'+get_article_price+'</h4>'+*/
                    '</li>';
                jQuery(".articles_ul_" + get_article_room_id).append(return_string);

            } else {

                var return_string = '<h4 class="cart-value_' + get_article_id + '' + get_article_room_id + '' + get_category_id + '">' + count + '</h4></li>';
                jQuery(".cart-value_" + get_article_id + get_article_room_id + get_category_id).html(return_string);
                var return_total = '<h4 class="cart-total_' + get_article_id + '' + get_article_room_id + '">' + currency_symbol + count * get_article_price + '</h4>' +
                    jQuery(".cart-cost").html(return_total);
            }
            var cart_data_send = {
                get_article_room_id: get_article_room_id,
                get_article_id: get_article_id,
                get_article_name: get_article_name,
                count: count,
                cubic_count: cubic_count,
                get_article_cubic: get_article_cubic,
                get_article_price: get_article_price,
                get_article_cat_id: get_category_id,
                get_article_caltype: get_article_caltype,
                action: 'cart_insert_data'
            };


            jQuery.ajax({
                type: "POST",
                url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                dataType: 'html',
                data: cart_data_send,

                success: function (response) {
                    var res_split = response.split("-");
                    jQuery('.lod_esti_title').html("Loading Estimation");
                    jQuery('.load_floor').html("Floor Price");
                    jQuery('.third_step_val_div').html(res_split[0]);
                    jQuery('.unlod_esti_title').html("Unloading Estimation");
                    jQuery('.unload_floor').html("Floor Price");
                    jQuery('.third_step_val_div_unload').html(res_split[1]);
                    jQuery('.elevator').html("Elevator Price");
                    jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
                    jQuery('.unelevator').html("Elevator Price");
                    jQuery('.unload_ele').html("-" + res_split[3]);
                    jQuery('.packing').html("Packaging Price");
                    jQuery('.third_step_val_div_packing').html(res_split[4]);
                    jQuery('.unpacking').html("Unpackaging Price");
                    jQuery('.third_step_val_div_unpacking').html(res_split[5]);
                    jQuery('.discount_label').html("Discount");
                    jQuery('.discounted_amount').html("-" + res_split[6]);
                    jQuery('.tax_label').html("GST/TAX");
                    jQuery('.taxed_amount').html(res_split[7]);
                    jQuery('.est_cost').html("Total");
                    jQuery('.total_cost').html(res_split[8]);
                    jQuery('.subtotal_label').html("Subtotal");
                    jQuery('.subtotal_amount').html(res_split[9]);
                    jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
                    jQuery('.cart_cubic_value').html(res_split[10]);
                    jQuery('.cart_cubic_volume').html(res_split[11]);
                    jQuery('.insurance_data').remove();
                    //jQuery('.insurance_rate').html(res_split[12]);
                    //jQuery('.insurance_rate').hide();
                    //jQuery('.insurance_data').remove();
                    /* jQuery('.amt_val').append('<div class="mp-xs-12 text-center cart-bg custom_insurance"> '
															+'<div class="mp-xs-5">'+
																'<label class="">Insurance Amount</label>'+
															'</div>'+
															'<div class="mp-xs-7">'+
																'<select name="amt_val" id="amt_val">'+
																	'<option>Select Insurance</option>'+
																    '<option value="50000">50000</option>'+
																    '<option value="100000">100000</option>'+
																    '<option value="150000">150000</option>'+
																    '<option value="200000">200000</option>'+
																    '<option value="other">Other</option>'+
																'</select>'+
															'</div>'

														); */

                }
            });

        }
        var article_div1 = jQuery(".articles_ul_" + get_article_room_id).text()
        if (article_div1 == '') {
            jQuery(".cart_check_" + get_article_room_id).remove();
        }
        var summaryData = jQuery(".cart_li").text();
        if (jQuery.trim(summaryData) == '') {

            jQuery(".r-cart-a").hide();
        }

    }
});

jQuery(document).on('click', '#second_step_submit', function (event) {
    jQuery('#third_step_progress').trigger('click');

});

jQuery(document).on('click', '#third_step_progress', function (event) {
    if (page == "second") {

        var check_cart_li = true;
        jQuery(".cart_li").find('li').each(function () {
            var li_id = jQuery(this).attr("data-li_id");
            var ul_check_id = "articles_ul_" + li_id;


            if (jQuery("." + ul_check_id + " li").length > 0 || jQuery(".cubic_list li").length > 0) {
                check_cart_li = false;
            }
        });
        if (check_cart_li) {
            jQuery("#second_step_progress").trigger("click");
            jQuery('.error_msg_livingroom').html("Please select atleast one article");
            return false;
        } else {
            jQuery('.error_msg_livingroom').hide();
            var selectcategory = jQuery('input[name="main_stuff_selection"]:checked').val();
            jQuery('#mp_second_step').slideUp(500);
            jQuery('#mp_third_step').slideDown(500);
        }
        var ajaxurl = mpmain_obj.plugin_path;
        var postdata = {action: 'get_step_three_data'};
        jQuery(".loader").show();
        jQuery.ajax({

            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: postdata,
            success: function (response) {
                page = "third";
                jQuery('#forth_step_progress').prop("disabled", false);
                jQuery("#forth_step_progress").css('cursor', 'pointer');
                jQuery(".loader").hide();
            }

        });
    }
});

/** GET LOADING FLOOR VALUE **/

jQuery(document).ready(function () {
    jQuery('.load_flool-count').click(function () {
        var ajaxurl = mpmain_obj.plugin_path;
        jQuery(".loader").show();
        var loading_floor_val = jQuery("input[name='flool_count']:checked").val();
        var loading_floor_number = jQuery("input[name='flool_count']:checked").data('floorno');

        var servicedata = {
            loading_floor_val: loading_floor_val,
            loading_floor_number: loading_floor_number,
            action: 'third_step_click_data'
        };
        jQuery('.lod_esti_title').html("Loading Estimation");
        jQuery('.load_floor').html("Floor Price");
        jQuery('.error_msg_load_floor').html("");

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: servicedata,
            success: function (response) {
                var resArr = response.split("-");
                jQuery('.third_step_val_div').html(resArr[3]);
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + resArr[0]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(resArr[1]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(resArr[2]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(resArr[4]);
                jQuery(".loader").hide();
            }

        });
    })
});

/** GET UNLOADING FLOOR VALUE **/

jQuery(document).ready(function () {
    jQuery('.unload_floor').click(function () {
        var ajaxurl = mpmain_obj.plugin_path;
        jQuery(".loader").show();
        var unloading_floor_val = jQuery("input[name='unflool_count']:checked").val();
        var unloading_floor_number = jQuery("input[name='unflool_count']:checked").data('floorno');
        var servicedata = {
            unloading_floor_val: unloading_floor_val,
            unloading_floor_number: unloading_floor_number,
            action: 'third_step_click_data_unload'
        };

        jQuery('.unlod_esti_title').html("Unloading Estimation");
        jQuery('.unload_floor').html("Floor Price");
        jQuery('.error_msg_unload_floor').html("");
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: servicedata,
            success: function (response) {
                var resArr = response.split("-");
                jQuery('.third_step_val_div_unload').html(resArr[3]);
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + resArr[0]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(resArr[1]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(resArr[2]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(resArr[4]);
                jQuery(".loader").hide();
            }
        });
    })
});


/** GET LOADING ELEVATOR VALUE **/

jQuery(document).ready(function () {
    jQuery('#load_elevator').click(function () {
        var ajaxurl = mpmain_obj.plugin_path;
        jQuery(".loader").show();
        var load_elevator_value = jQuery("input[name='elevators']:checked").val();

        if (jQuery(this).attr('checked')) {
            var elevatordata = {
                load_elevator_value: load_elevator_value,
                load_elevator_checked: "Yes",
                action: 'third_step_click_data_elevators'
            };
        } else {
            var elevatordata = {
                load_elevator_value: 0,
                load_elevator_checked: "No",
                action: 'third_step_click_data_elevators'
            };

        }

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: elevatordata,
            success: function (response) {
                var resArr = response.split("-");


                if (jQuery.trim(resArr[3]) == jQuery.trim(moveto_currency_symbol.symbol + 0) || jQuery.trim(resArr[3]) == jQuery.trim(0 + moveto_currency_symbol.symbol)) {
                    var html = '';
                    jQuery('.elevator').html(html);
                    jQuery('.third_step_val_div_elevators').html('');
                } else {
                    jQuery('.lod_esti_title').html("Loading Estimation");
                    jQuery('.elevator').html("Elevator Price");
                    jQuery('.third_step_val_div_elevators').html("-" + resArr[3]);
                }
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + resArr[0]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(resArr[1]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(resArr[2]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(resArr[4]);
                jQuery(".loader").hide();
            }
        });
    });
});
/** GET PACKAGING VALUE **/

jQuery(document).ready(function () {
    jQuery('#packaging').click(function () {

        var ajaxurl = mpmain_obj.plugin_path;
        var packaging_value = jQuery("input[name='packaging']:checked").val();
        jQuery(".loader").show();
        if (jQuery(this).attr('checked')) {
            var packagingData = {
                packaging_value: packaging_value,
                packaging_checked: "Yes",
                action: 'third_step_click_data_packaging'
            };
        } else {
            var packagingData = {
                packaging_value: 0,
                packaging_checked: "No",
                action: 'third_step_click_data_packaging'
            };
        }

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: packagingData,
            success: function (response) {

                var resArr = response.split("-");

                if (jQuery.trim(resArr[3]) == jQuery.trim(moveto_currency_symbol.symbol + 0) || jQuery.trim(resArr[3]) == jQuery.trim(0 + moveto_currency_symbol.symbol)) {
                    var html = '';
                    jQuery('.packing').html(html);
                    jQuery('.third_step_val_div_packing').html('');
                } else {
                    jQuery('.lod_esti_title').html("Loading Estimation");
                    jQuery('.packing').html("Packaging Price");
                    jQuery('.third_step_val_div_packing').html(resArr[3]);
                }
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + resArr[0]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(resArr[1]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(resArr[2]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(resArr[4]);
                jQuery(".loader").hide();
            }
        });
    });
});

/** GET UNPACKAGING VALUE **/

jQuery(document).ready(function () {
    jQuery('#unpackaging').click(function () {
        var ajaxurl = mpmain_obj.plugin_path;
        var unpackaging_value = jQuery("input[name='unpackaging']:checked").val();
        jQuery(".loader").show();
        if (jQuery(this).attr('checked')) {
            var unpackagingData = {
                unpackaging_value: unpackaging_value,
                unpackaging_checked: "Yes",
                action: 'third_step_click_data_unpackaging'
            };
        } else {
            var unpackagingData = {
                unpackaging_value: 0,
                unpackaging_checked: "No",
                action: 'third_step_click_data_unpackaging'
            };

        }

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: unpackagingData,
            success: function (response) {
                var resArr = response.split("-");
                if (jQuery.trim(resArr[3]) == jQuery.trim(moveto_currency_symbol.symbol + 0) || jQuery.trim(resArr[3]) == jQuery.trim(0 + moveto_currency_symbol.symbol)) {
                    var html = '';
                    jQuery('.unpacking').html(html);
                    jQuery('.third_step_val_div_unpacking').html('');

                } else {
                    jQuery('.unlod_esti_title').html("Unloading Estimation");
                    jQuery('.unpacking').html("Unpackaging Price");
                    jQuery('.third_step_val_div_unpacking').html(resArr[3]);

                }
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + resArr[0]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(resArr[1]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(resArr[2]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(resArr[4]);
                jQuery(".loader").hide();
            }
        });
    });
});

/** GET UNLOADING ELEVATOR VALUE **/

jQuery(document).ready(function () {
    jQuery('#unload_elevator').click(function () {
        var ajaxurl = mpmain_obj.plugin_path;
        var unload_elevator_value = jQuery("input[name='unloaad_ele']:checked").val();
        jQuery(".loader").show();
        if (jQuery(this).attr('checked')) {
            var elevatordata = {
                unload_elevator_value: unload_elevator_value,
                unload_elevator_checked: "Yes",
                action: 'third_step_click_data_unloadelevators'
            };
        } else {
            var elevatordata = {
                unload_elevator_value: 0,
                unload_elevator_checked: "No",

                action: 'third_step_click_data_unloadelevators'
            };
        }

        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: elevatordata,
            success: function (response) {
                var resArr = response.split("-");
                if (jQuery.trim(resArr[3]) == jQuery.trim(moveto_currency_symbol.symbol + 0) || jQuery.trim(resArr[3]) == jQuery.trim(0 + moveto_currency_symbol.symbol)) {
                    var html = '';
                    jQuery('.unelevator').html(html);
                    jQuery('.unload_ele').html('');
                } else {
                    jQuery('.unlod_esti_title').html("Unloading Estimation");
                    jQuery('.unelevator').html("Elevator Price");
                    jQuery('.unload_ele').html("-" + resArr[3]);
                }
                jQuery('.discount_label').html("Discount");
                jQuery('.discounted_amount').html("-" + resArr[0]);
                jQuery('.tax_label').html("GST/TAX");
                jQuery('.taxed_amount').html(resArr[1]);
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(resArr[2]);
                jQuery('.subtotal_label').html("Subtotal");
                jQuery('.subtotal_amount').html(resArr[4]);
                jQuery(".loader").hide();
            }
        });
    })


});


/** GET Additional Info VALUE **/

jQuery(document).ready(function () {
    jQuery('.additional_infoo').click(function () {
        jQuery(".loader").show();
        var ajaxurl = mpmain_obj.plugin_path;
        var id = jQuery(this).data('id');
        var caltype = jQuery(this).data('caltype');
        var additional_info_price = jQuery(this).val();
        if (jQuery("#additional_info_" + id).prop("checked") == true) {

            var Additionaldata = {

                additional_info_price: additional_info_price,
                caltype: caltype,
                additional_info_process: "INC",
                action: 'third_step_click_data_additional'
            };
        } else if (jQuery("#additional_info_" + id).prop("checked") == false) {

            var Additionaldata = {
                additional_info_price: additional_info_price,
                caltype: caltype,
                additional_info_process: "DEC",
                action: 'third_step_click_data_additional'
            };
        }
        jQuery.ajax({
            type: "POST",
            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
            dataType: 'html',
            data: Additionaldata,
            success: function (response) {
                jQuery('.est_cost').html("Total");
                jQuery('.total_cost').html(response);
                jQuery(".loader").hide();
            }
        });
    });


});


/** STEP THREE SUBMIT **/
jQuery(document).on('click', '#forth_step_progress', function (event) {
    if (page == "third") {
        var ajaxurl = mpmain_obj.plugin_path;
        var actual_link = mpmain_obj.actual_link;
        var thankupage_url = obj_thankspage_url.thankspage_url;
        var loading_floor_no = jQuery("input[name='flool_count']:checked").data('floorno');

        var unloading_floor_no = jQuery("input[name='unflool_count']:checked").data('floorno');
        if (loading_floor_no == undefined) {
            jQuery('#third_step_progress').trigger('click');
            jQuery('.error_msg_load_floor').html("Please select loading floor");
            return false;
        } else {
            jQuery('.error_msg_load_floor').html("");
        }
        if (unloading_floor_no == undefined) {
            jQuery('#third_step_progress').trigger('click');
            jQuery('.error_msg_unload_floor').html("Please select unloading floor");
            return false;
        } else {
            jQuery('.error_msg_unload_floor').html("");
        }
        if (!jQuery('#mp_third_step_validate').valid()) {
            jQuery('#third_step_progress').trigger('click');
            return false;
        }

        if (jQuery('#mp_third_step_validate').valid()) {
            jQuery(".loader").show();
            page = "third1";
            jQuery('#forth_step_progress').trigger('click');

            var loading_address_one = jQuery("input[name='street_address']").val();
            var loading_address_two = jQuery("input[name='city_address']").val();
            var loading_address_three = jQuery("input[name='state_address']").val();

            var favorite = [];
            jQuery.each(jQuery("input[name='additional_info']:checked"), function () {
                favorite.push(jQuery(this).data('id'));
            });

            /**For Unloading Point**/

            var unloading_address_one = jQuery("input[name='unload_street_addr']").val();
            var unloading_address_two = jQuery("input[name='unload_city_addr']").val();
            var unloading_address_three = jQuery("input[name='unload_state_addr']").val();


            /*array variable*/

            var loading_info = {};
            var unloading_info = {};
            var additional_info = {};


            loading_info['loading_address_one'] = loading_address_one;
            loading_info['loading_address_two'] = loading_address_two;
            loading_info['loading_address_three'] = loading_address_three;


            console.log(loading_info);

            unloading_info['unloading_address_one'] = unloading_address_one;
            unloading_info['unloading_address_two'] = unloading_address_two;
            unloading_info['unloading_address_three'] = unloading_address_three;

            additional_info['favorite'] = favorite;
            var payment_method = jQuery("input[name=payment_method]:checked").val();
            var current_stepdata = {
                'loading_info': loading_info,
                'unloading_info': unloading_info,
                'additional_info': additional_info,
                'payment_method': payment_method,
                action: 'booking_data_insert'
            }

            if (payment_method == "stripe_payment") {
                stripe.createToken(card).then(function (result) {
                    if (result.error) {
                        jQuery('.loader').hide();
                        jQuery("#card-errors").html(result.error.message);
                        jQuery("#card-errors").show();
                    } else {
                        jQuery("#card-errors").empty();
                        jQuery("#card-errors").hide();
                        current_stepdata["st_token"] = result.token.id;
                        jQuery.ajax({
                            type: "POST",
                            url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                            dataType: 'html',
                            data: current_stepdata,
                            success: function (response) {
                                jQuery('#second_step_progress').prop("disabled", true);
                                jQuery("#second_step_progress").css('cursor', 'unset');
                                jQuery('#third_step_progress').prop("disabled", true);
                                jQuery("#third_step_progress").css('cursor', 'unset');
                                jQuery(".loader").hide();
                                if (thankupage_url != '' && thankupage_url != null) {
                                    window.location.href = thankupage_url;
                                } else {
                                    jQuery('#mp_third_step').slideUp(500);
                                    jQuery('#mp_forth_step').slideDown(500);
                                }
                            }
                        });
                    }
                });
            } else if (payment_method == "paypal_payment") {
                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl + "/assets/lib/paypal_payment_process.php",
                    dataType: 'html',
                    data: current_stepdata,
                    success: function (response) {
                        var response_detail = jQuery.parseJSON(response);
                        if (response_detail.status == 'error') {
                            jQuery('.payment_error_msg').show();
                            jQuery('.payment_error_msg').text(response_detail.value);
                        } else {
                            window.location = response_detail.value;
                        }
                    }
                });
            } else if (payment_method == 'paytm_payment') {
                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl + "/assets/lib/paytm_payment_process.php",
                    data: current_stepdata,
                    success: function (response) {

                        var response_detail = jQuery.parseJSON(response);
                        jQuery('#mp_paytm_form').attr('action', response_detail.PAYTM_TXN_URL);
                        jQuery('#mp_paytm_form').append(response_detail.Extra_form_fields);
                        jQuery("#mp_paytm_form").submit();
                    }
                });
            } else if (payment_method == 'paystack_payment') {
                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl + "/assets/lib/paystack_payment_process.php",
                    data: current_stepdata,
                    success: function (response) {
                        var response_detail = jQuery.parseJSON(response);
                        console.log(response_detail);
                        if (response_detail.status == true) {
                            window.location = response_detail.data['authorization_url'];
                        } else {
                            jQuery('.payment_error_msg').show();
                            jQuery('.payment_error_msg').text(response_detail.message);
                        }
                    }
                });
            } else {

                jQuery.ajax({
                    type: "POST",
                    url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                    dataType: 'html',
                    data: current_stepdata,
                    success: function (response) {
                        jQuery('#first_step_progress').prop("disabled", true);
                        jQuery('#second_step_progress').prop("disabled", true);
                        jQuery('#third_step_progress').prop("disabled", true);
                        jQuery("#first_step_progress").css('cursor', 'unset');
                        jQuery("#second_step_progress").css('cursor', 'unset');
                        jQuery("#third_step_progress").css('cursor', 'unset');
                        jQuery(".loader").hide();
                        if (thankupage_url != '' && thankupage_url != null) {
                            window.location.href = thankupage_url;
                        } else {

                            jQuery('#mp_third_step').slideUp(500);
                            jQuery('#mp_forth_step').slideDown(500);
                        }
                    }
                });
            }
        }
    }
});

jQuery(document).on('click', '#forth_step_submit', function (event) {
    jQuery('#forth_step_progress').trigger('click');
});


jQuery(document).on('ready ajaxComplete', function () {
    var showChar = 20;
    var ellipsestext = "...";
    jQuery('.article_text').each(function () {
        var content = jQuery(this).html();
        if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var html = c + ellipsestext;
            jQuery(this).html(html);
        }
    });
});

jQuery(document).on('ready ajaxComplete', function () {
    var showChar = 5;
    var ellipsestext = "..";
    jQuery('.msize').each(function () {
        var content = jQuery(this).html();
        if (content.length > showChar) {
            var c = content.substr(0, showChar);
            var html = c + ellipsestext;
            jQuery(this).html(html);
        }
    });
});


/** Bhupendra Custom Cubic Feets **/
jQuery(document).ready(function () {
    jQuery(document).on('click', 'li.room_type_cat', function () {
        jQuery('li.room_type_cat').each(function () {
            if (jQuery(this).hasClass("active")) {
                if (jQuery(this).hasClass("cust_quick")) {
                    jQuery('.multi_quick').css("display", "block");
                }
            } else {
                jQuery('.multi_quick').css("display", "none");
            }
        });
    });
});

jQuery(document).one('click', '.cust_quick', function () {
    var active = jQuery(this).hasClass('active');
    if (active == true) {
        jQuery(".multi_quick").append('<form method="POST" name="cust">' +
            '<div class="exp"></div><br>' +
            '<button type="button" name="addmore" class="btn btn-success add_more">Add More<i class="fa fa-plus" aria-hidden="true" style="font-size:14px;margin-left:6px;"></i></button>' +
            '</form>');
    }
    jQuery(".exp").append('<div class="more"  id="row1">' +
        '<div class="fancy_input_wrap" style="width: 60%;">' +
        '<input type="text" name="title" class="fancy_input title" placeholder="">' +
        '<span class="highlight"></span><span class="bar"></span>' +
        '<label class="fancy_label" for="name">Title</label>' +
        '</div>' +
        '<div class="fancy_input_wrap" style="float:left;width:19%;margin: 5px 5px 3px 0px;">' +
        '<input type="text" name="height" class="fancy_input height" placeholder="" >' +
        '<span class="highlight"></span><span class="bar"></span>' +
        '<label class="fancy_label" for="name">Height</label>' +
        '</div>' +
        '<div class="fancy_input_wrap" style="float:left;width:19%;margin: 5px 5px 3px 0px;">' +
        '<input type="text" name="width" class="fancy_input width" placeholder="">' +
        '<span class="highlight"></span><span class="bar"></span>' +
        '<label class="fancy_label" for="name">Width</label>' +
        '</div>' +
        '<div class="fancy_input_wrap" style="float:left;width:19%;margin: 5px 5px 3px 0px;">' +
        '<input type="text" name="length" class="fancy_input length" placeholder="">' +
        '<span class="highlight"></span><span class="bar"></span>' +
        '<label class="fancy_label" for="name"> length</label>' +
        '</div>' +
        '<div><input type="text" name="total_cubicfeet" style="width: 50%;" class="total" value="" disabled>' +
        '<a href="#" class="remove_field"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px;margin:16px 0 0 12px;color:#FD3943;"></i></a>' +
        '</div>' +
        '</div>');
});
var i = 1;
jQuery(document).on('click', '.add_more', function () {
    jQuery(".r-cart-a").show();
    var moveto_cubic_meter = moveto_cubic_unit;
    var ajaxurl = mpmain_obj.plugin_path;
    var moreid = jQuery(".more").last().attr("id");
    var title = jQuery(".exp .title").last().val();
    var height = jQuery(".exp .height").last().val();
    var width = jQuery(".exp .width").last().val();
    var length = jQuery(".exp .length").last().val();
    var cubicfeet = height * width * length;
    var cubicfeets = jQuery(".exp .total").last().val(cubicfeet);
    var count = '1';

    var cubic_data = {
        cubicfeet_title: title,
        cubicfeet_count: cubicfeet,
        moreid: moreid,
        cubic_count: count,
        action: 'cart_insert_data'
    };
    var numericReg = /^[1-9][0-9]*$/;
    var title_reg = /^[a-zA-Z\s]+$/;
    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }

    jQuery('p.error_cubic').remove();
    jQuery('.error_cubic').remove();
    if (title == '' && height == '' && width == '' && length == '') {
        jQuery("<p class='error_cubic'>Please Enter Title, Height, Width & Length per " + label_cubic_unit + ".</p>").insertBefore(".add_more");
    }
    if (!title_reg.test(title)) {
        jQuery('.title').last().after('<p class="error_cubic">Invalid text.</p>');
    }
    if (!numericReg.test(height)) {
        jQuery('.height').last().after('<p class="error_cubic">Numeric characters only.</p>');
    }
    if (!numericReg.test(width)) {
        jQuery('.width').last().after('<p class="error_cubic">Numeric characters only.</p>');
    }
    if (!numericReg.test(length)) {
        jQuery('.length').last().after('<p class="error_cubic">Numeric characters only.</p>');
    }
    jQuery('.more').find('br').remove();

    if (jQuery(".more .error_cubic").length == 0) {

        if (title != '' && cubicfeets != '') {
            jQuery('.loader').show();
            if (jQuery("ul.cubic_list").length == 0 && title && cubicfeets) {
                var ul = jQuery('<ul class="cubic_list"><h3 class="cust_cubic cart-title cart_title">Custom</h3></ul>');
                jQuery('.cart_li').append(ul);
            }
            if (typeof title !== "undefined") {
                var div = jQuery('<li id="cust_cubic_data" class="cart-listing"><h3 class="cust_cubic cart-title">' + title + '</h3><h4>1</h4></li>');
                jQuery('.cubic_list').append(div);
            }

            jQuery.ajax({
                type: "POST",
                url: ajaxurl + "/assets/lib/mp_front_ajax.php",
                data: cubic_data,
                success: function (response) {
                    jQuery('.loader').hide();
                    var res_split = response.split("-");
                    jQuery('.lod_esti_title').html("Loading Estimation");
                    jQuery('.load_floor').html("Floor Price");
                    jQuery('.third_step_val_div').html(res_split[0]);
                    jQuery('.unlod_esti_title').html("Unloading Estimation");
                    jQuery('.unload_floor').html("Floor Price");
                    jQuery('.third_step_val_div_unload').html(res_split[1]);
                    jQuery('.elevator').html("Elevator Price");
                    jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
                    jQuery('.unelevator').html("Elevator Price");
                    jQuery('.unload_ele').html("-" + res_split[3]);
                    jQuery('.packing').html("Packaging Price");
                    jQuery('.third_step_val_div_packing').html(res_split[4]);
                    jQuery('.unpacking').html("Unpackaging Price");
                    jQuery('.third_step_val_div_unpacking').html(res_split[5]);
                    jQuery('.discount_label').html("Discount");
                    jQuery('.discounted_amount').html("-" + res_split[6]);
                    jQuery('.tax_label').html("GST/TAX");
                    jQuery('.taxed_amount').html(res_split[7]);
                    jQuery('.est_cost').html("Total");
                    jQuery('.total_cost').html(res_split[8]);
                    jQuery('.subtotal_label').html("Subtotal");
                    jQuery('.subtotal_amount').html(res_split[9]);
                    jQuery('.cubic_feets').html(label_cubic_unit + " Rate");
                    jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
                    jQuery('.cart_cubic_value').html(res_split[10]);
                    jQuery('.cart_cubic_volume').html(res_split[11]);
                    jQuery('.insurance_rate').html(res_split[12]);
                },
            });

            var maindiv = jQuery(".exp");
            i++;
            jQuery(maindiv).append('<div class="more" id="row' + i + '"> <div class="fancy_input_wrap" style="width: 60%;margin-top: 39px;" >' +
                '<input type="text" name="title" class="fancy_input title" placeholder="">' +
                '	<span class="highlight"></span><span class="bar"></span>' +
                '<label class="fancy_label" for="name">Title</label></div>' +
                '<div class="fancy_input_wrap" style="float:left;width:19%;margin: 5px 5px 3px 0px;">' +
                '<input type="text" name="height" class="fancy_input height" placeholder="" >' +
                '	<span class="highlight"></span><span class="bar"></span>' +
                '<label class="fancy_label" for="name">Height</label></div>' +

                '<div class="fancy_input_wrap" style="float:left;width:19%;margin: 5px 5px 3px 0px;">' +
                '<input type="text" name="width" class="fancy_input width" placeholder="">' +
                '	<span class="highlight"></span><span class="bar"></span>' +
                '<label class="fancy_label" for="name">Width</label></div>' +

                '<div class="fancy_input_wrap" style="float:left;width:19%;margin: 5px 5px 3px 0px;">' +
                '<input type="text" name="length" class="fancy_input length" placeholder="">' +
                '<span class="highlight"></span><span class="bar"></span>' +
                '<label class="fancy_label" for="name"> length</label></div>' +
                '<div class="float-left"><input type="text" name="total_cubicfeet" style="width: 50%;" class="total" value="" disabled>' +
                '<a href="#" class="remove_field"><i class="fa fa-times-circle" aria-hidden="true" style="font-size:24px;margin:16px 0 0 12px;color:#FD3943;"></i></a></div></div>');
        }

    }

});

jQuery(document).on("click", ".remove_field", function (e) {
    jQuery('.loader').show();
    var ajaxurl = mpmain_obj.plugin_path;
    var moveto_cubic_meter = moveto_cubic_unit;
    var removeid = jQuery(this).closest('.more').attr('id');
    var titleval = jQuery(this).closest('.more').find('input[name=title]').val();
    var count = '0';
    e.preventDefault();
    jQuery(this).parent().closest('.more').remove();
    var remove_data = {removeid: removeid, cubic_count: count, action: 'cart_insert_data'};

    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }
    jQuery('li#cust_cubic_data h3').each(function () {
        var remove_title = jQuery(this).text();
        if (remove_title == titleval) {
            jQuery(this).parent('#cust_cubic_data').remove();
        }
    });

    if (jQuery(".cubic_list li").length == 0) {
        jQuery('ul.cubic_list').remove();
    }

    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        data: remove_data,
        success: function (response) {
            jQuery('.loader').hide();
            var res_split = response.split("-");
            jQuery('.lod_esti_title').html("Loading Estimation");
            jQuery('.load_floor').html("Floor Price");
            jQuery('.third_step_val_div').html(res_split[0]);
            jQuery('.unlod_esti_title').html("Unloading Estimation");
            jQuery('.unload_floor').html("Floor Price");
            jQuery('.third_step_val_div_unload').html(res_split[1]);
            jQuery('.elevator').html("Elevator Price");
            jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
            jQuery('.unelevator').html("Elevator Price");
            jQuery('.unload_ele').html("-" + res_split[3]);
            jQuery('.packing').html("Packaging Price");
            jQuery('.third_step_val_div_packing').html(res_split[4]);
            jQuery('.unpacking').html("Unpackaging Price");
            jQuery('.third_step_val_div_unpacking').html(res_split[5]);
            jQuery('.discount_label').html("Discount");
            jQuery('.discounted_amount').html("-" + res_split[6]);
            jQuery('.tax_label').html("GST/TAX");
            jQuery('.taxed_amount').html(res_split[7]);
            jQuery('.est_cost').html("Total");
            jQuery('.total_cost').html(res_split[8]);
            jQuery('.subtotal_label').html("Subtotal");
            jQuery('.subtotal_amount').html(res_split[9]);
            jQuery('.cubic_feets').html(label_cubic_unit + " Rate");
            jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
            jQuery('.cart_cubic_value').html(res_split[10]);
            jQuery('.cart_cubic_volume').html(res_split[11]);
            jQuery('.insurance_rate').html(res_split[12]);
        }
    });

});


jQuery(document).on('click', '.insurance_check_it', function () {
    var ajaxurl = mpmain_obj.plugin_path;
    jQuery('.loader').show();
    var moveto_cubic_meter = moveto_cubic_unit;
    if (jQuery(this).prop("checked") == true) {
        /*jQuery('.insurance_check_it').attr('checked', true);*/
        jQuery('.insurance_check_it').prop('checked', true);
        var check_val = '1';
        jQuery('.insurance_sec').append('<div class="insurance_data"><div class="mp-xs-6">' +
            '<label class="insurance_label">Insurance</label>' +
            '</div><div class="mp-xs-6" style="padding-right: 5px;">' +
            '<h1 class="cart-cost insurance_rate insurance_cost"></h1></div></div>');
    } else {
        if (jQuery(this).prop("checked") == false) {
            /*jQuery('.insurance_check_it').attr('checked', false);*/
            jQuery('.insurance_check_it').prop('checked', false);

            var check_val = '0';
            jQuery('.insurance_data').remove();
        }
    }
    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }
    var postdata = {insurance_value: check_val, action: 'cart_insert_data'};
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        data: postdata,
        success: function (response) {
            jQuery('.loader').hide();
            var res_split = response.split("-");
            jQuery('.lod_esti_title').html("Loading Estimation");
            jQuery('.load_floor').html("Floor Price");
            jQuery('.third_step_val_div').html(res_split[0]);
            jQuery('.unlod_esti_title').html("Unloading Estimation");
            jQuery('.unload_floor').html("Floor Price");
            jQuery('.third_step_val_div_unload').html(res_split[1]);
            jQuery('.elevator').html("Elevator Price");
            jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
            jQuery('.unelevator').html("Elevator Price");
            jQuery('.unload_ele').html("-" + res_split[3]);
            jQuery('.packing').html("Packaging Price");
            jQuery('.third_step_val_div_packing').html(res_split[4]);
            jQuery('.unpacking').html("Unpackaging Price");
            jQuery('.third_step_val_div_unpacking').html(res_split[5]);
            jQuery('.discount_label').html("Discount");
            jQuery('.discounted_amount').html("-" + res_split[6]);
            jQuery('.tax_label').html("GST/TAX");
            jQuery('.taxed_amount').html(res_split[7]);
            jQuery('.est_cost').html("Total");
            jQuery('.total_cost').html(res_split[8]);
            jQuery('.subtotal_label').html("Subtotal");
            jQuery('.subtotal_amount').html(res_split[9]);
            jQuery('.cubic_feets').html(label_cubic_unit + " Rate");
            jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
            jQuery('.cart_cubic_value').html(res_split[10]);
            jQuery('.cart_cubic_volume').html(res_split[11]);
            jQuery('.insurance_rate').html(res_split[12]);
        }
    });
});

jQuery("#add_amt_Val").click(function () {
    var ajaxurl = mpmain_obj.plugin_path;
    jQuery('.loader').show();
    jQuery('.insurance_data').remove();
    var moveto_cubic_meter = moveto_cubic_unit;
    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }
    var amt = jQuery("#amt").val();


    var postdata = {amt: amt, action: 'cart_insert_data'};
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        data: postdata,
        success: function (response) {
            jQuery('.loader').hide();

            jQuery(".new_val").hide(1000, "linear", function () {
                jQuery('#amt').val('');
            });
            jQuery('.insurance_sec').append('<div class="insurance_data"><div class="mp-xs-5">' +
                '<label class="insurance_label">Insurance</label>' +
                '</div><div class="mp-xs-7" style="padding-right: 5px;">' +
                '<h1 class="cart-cost insurance_rate insurance_cost"></h1></div></div>');

            var res_split = response.split("-");
            jQuery('.lod_esti_title').html("Loading Estimation");
            jQuery('.load_floor').html("Floor Price");
            jQuery('.third_step_val_div').html(res_split[0]);
            jQuery('.unlod_esti_title').html("Unloading Estimation");
            jQuery('.unload_floor').html("Floor Price");
            jQuery('.third_step_val_div_unload').html(res_split[1]);
            jQuery('.elevator').html("Elevator Price");
            jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
            jQuery('.unelevator').html("Elevator Price");
            jQuery('.unload_ele').html("-" + res_split[3]);
            jQuery('.packing').html("Packaging Price");
            jQuery('.third_step_val_div_packing').html(res_split[4]);
            jQuery('.unpacking').html("Unpackaging Price");
            jQuery('.third_step_val_div_unpacking').html(res_split[5]);
            jQuery('.discount_label').html("Discount");
            jQuery('.discounted_amount').html("-" + res_split[6]);
            jQuery('.tax_label').html("GST/TAX");
            jQuery('.taxed_amount').html(res_split[7]);
            jQuery('.est_cost').html("Total");
            jQuery('.total_cost').html(res_split[8]);
            jQuery('.subtotal_label').html("Subtotal");
            jQuery('.subtotal_amount').html(res_split[9]);
            jQuery('.cubic_feets').html(label_cubic_unit + " Rate");
            jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
            jQuery('.cart_cubic_value').html(res_split[10]);
            jQuery('.cart_cubic_volume').html(res_split[11]);
            jQuery('.insurance_rate').html(res_split[12]);
        }
    });

});


jQuery("select#amt_val").change(function () {
    var ajaxurl = mpmain_obj.plugin_path;
    jQuery('.loader').show();
    jQuery('.insurance_data').remove();
    var moveto_cubic_meter = moveto_cubic_unit;
    if (moveto_cubic_meter == 'm_3') {
        var label_cubic_unit = 'Cubic Meter';
    } else {
        var label_cubic_unit = 'Cubic Feet';
    }
    var amt_val = jQuery("#amt_val").val();

    if (amt_val == 'other') {
        jQuery(".new_val").show("slow");
    } else {
        jQuery(".new_val").hide(1000, "linear", function () {
        });

    }

    /* var amt_val = jQuery("#amt_val").val();

		if(amt_val == '')
		{
			// jQuery(".new_val").show("slow");
				jQuery('.amt_val').hide();
			alert();
	    }
		 */

    /*  if(amt_val == '')
				{
			// jQuery(".new_val").show("slow");
				/* jQuery('.amt_val').hide();

			jQuery('#amt_val').show(); */
    /*  jQuery('.amt_val').hide();
			alert();
				}
			 */


    var postdata = {amt_val: amt_val, action: 'cart_insert_data'};
    jQuery.ajax({
        type: "POST",
        url: ajaxurl + "/assets/lib/mp_front_ajax.php",
        data: postdata,
        success: function (response) {
            jQuery('.loader').hide();

            //jQuery('#amt_val').val('Select Insurance');


            jQuery('.insurance_sec').append('<div class="insurance_data"><div class="mp-xs-5">' +
                '<label class="insurance_label">Insurance</label>' +
                '</div><div class="mp-xs-7" style="padding-right: 5px;">' +
                '<h1 class="cart-cost insurance_rate insurance_cost"></h1></div></div>');
            var res_split = response.split("-");
            jQuery('.lod_esti_title').html("Loading Estimation");
            jQuery('.load_floor').html("Floor Price");
            jQuery('.third_step_val_div').html(res_split[0]);
            jQuery('.unlod_esti_title').html("Unloading Estimation");
            jQuery('.unload_floor').html("Floor Price");
            jQuery('.third_step_val_div_unload').html(res_split[1]);
            jQuery('.elevator').html("Elevator Price");
            jQuery('.third_step_val_div_elevators').html("-" + res_split[2]);
            jQuery('.unelevator').html("Elevator Price");
            jQuery('.unload_ele').html("-" + res_split[3]);
            jQuery('.packing').html("Packaging Price");
            jQuery('.third_step_val_div_packing').html(res_split[4]);
            jQuery('.unpacking').html("Unpackaging Price");
            jQuery('.third_step_val_div_unpacking').html(res_split[5]);
            jQuery('.discount_label').html("Discount");
            jQuery('.discounted_amount').html("-" + res_split[6]);
            jQuery('.tax_label').html("GST/TAX");
            jQuery('.taxed_amount').html(res_split[7]);
            jQuery('.est_cost').html("Total");
            jQuery('.total_cost').html(res_split[8]);
            jQuery('.subtotal_label').html("Subtotal");
            jQuery('.subtotal_amount').html(res_split[9]);
            jQuery('.cubic_feets').html(label_cubic_unit + " Rate");
            jQuery('.cart_title_cubic').html("Total Volume(" + label_cubic_unit + ")");
            jQuery('.cart_cubic_value').html(res_split[10]);
            jQuery('.cart_cubic_volume').html(res_split[11]);
            jQuery('.insurance_rate').html(res_split[12]);
        }
    });

});
jQuery(document).ready(function () {
    jQuery(".plus_minus_text").hide();
    jQuery(".plus_minus_btn").keyup(function () {
        var id = jQuery(this).attr("data-id");
        var count_no = jQuery("#input_number_" + id).val();
        if (count_no > 0) {
            jQuery("#input_number_" + id).show();
        } else if (count_no == 0) {

            jQuery("#input_number_" + id).hide();
        }
    });
});