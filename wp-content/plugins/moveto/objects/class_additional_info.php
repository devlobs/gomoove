<?php
class moveto_additional_info{
 
   /* Object properties */
    public $id;
    public $additional_info;
    public $status;

	 /**
     * create moveto Additional Info table
     */ 
	function create_table() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_additional_info';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name." (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `additional_info` LONGTEXT  NOT NULL,
			  `status` enum('E','D') NOT NULL DEFAULT 'E',
			  `type` enum('P','F') NOT NULL,
			  `price` int(100) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	
	dbDelta($sql);     
			}
	} 
	
	/**
	* Read All Enable/Disable Additional Info
	* @return $return true - on sucess, false - on faliure
	*/
	function readAll(){
		global $wpdb;
		
		$return = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_additional_info ORDER BY id ASC");	
		
		return $return;
	}

	/**
	* Read All Enable Additional Info
	* @return $return true - on sucess, false - on faliure
	*/
	function readAll_enable(){
		global $wpdb;
		
		$return = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_additional_info where status='E' ORDER BY id ASC");	
		
		return $return;
	}
    
	/**
	* create Additional Info
	* @return $return true - on sucess, false - on faliure
	*/
	  function create(){
	 
		  global $wpdb;
		$return = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_additional_info (id,additional_info,status,type,price) values( '','".$this->additional_info."','".$this->status."','".$this->type."','".$this->price."')");
		
		if($return){
			return true;
		}else{
			return false;
		}
	 
	}
		
	
    /**
	* read Additional Info
	* @return $return true - on success, false - on failure
	*/
	function read(){
		global $wpdb;
		$return = $wpdb->get_results("SELECT
						id, additional_info
					FROM
						".$wpdb->prefix."mp_additional_info 
					ORDER BY
						additional_info");  
	 
		
			return $return;
	}
	
	
	/**
	* read one Additional Info
	*/
	function readOne(){
	 
		global $wpdb;
		$return = $wpdb->get_results("SELECT * FROM	".$wpdb->prefix."mp_additional_info WHERE status = 'E' AND id =".$this->id);
	 
		foreach($return as $row){
		$this->additional_info = $row->additional_info;
		$this->status = $row->status;
		}
	}
	
		
	/**
	* Update Additional Info
	* @return true - on success, false- on falure
	*/	
	function update(){
		 global $wpdb;
		 
		 $return = $wpdb->query("UPDATE
					 ".$wpdb->prefix."mp_additional_info 
				SET
					additional_info = '".$this->additional_info."',type='".$this->type."',price='".$this->price."'
				WHERE
					id =".$this->id);
	 
		if($return){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	* Update Status Additional Info
	* @return true - on success, false- on falure
	*/	
	function update_status(){
		 global $wpdb;
		 
		 $return = $wpdb->query("UPDATE
					 ".$wpdb->prefix."mp_additional_info 
				SET
					status = '".$this->status."'
				WHERE
					id =".$this->id);
	 
		if($return){
			return true;
		} else {
			return false;
		}
	}

	/**
	* Count all Additional Info
	* @return $num - number of records
	*/	
	public function countAll(){
		global $wpdb;
		$return = $wpdb->query("SELECT id FROM  ".$wpdb->prefix."mp_additional_info");
	 
		$num = sizeof((array)$return);
	 
		return $num;
	}

	/**
	*Delete Additional Info
	*@return true - sucess, false- faliure
	*/
	function delete(){
		 global $wpdb;
		 $return = $wpdb->query("DELETE FROM  ".$wpdb->prefix."mp_additional_info  WHERE id =".$this->id);
		
		$result=$return;
		if($result){
			return true;
		}else{
			return false;
		}
	}
	

}
?>