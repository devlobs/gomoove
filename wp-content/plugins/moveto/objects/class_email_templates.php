<?php
class moveto_email_template
{
    /* object properties */
    public $id;
	public $location_id;
    public $email_template_name;
    public $email_subject;
    public $email_message;
    public $template_status;
    public $default_message;
    public $method;
    public $email_parent_template;
    public $user_type;
	
	
  public function __construct() {	
		global $wpdb;				
		
		$company_header = '<table style="width: 100%;"><tr>';						
		if(get_option("moveto_company_logo")!='') {			
		$upload_dir_path= wp_upload_dir();			
		$image_path= $upload_dir_path['baseurl'].get_option("moveto_company_logo");			
		$company_header .= '<td style="background-color: #fff; padding-left: 50px; padding: 20px; text-align: left; padding-right: 20px;"><img style="float:left;clear:both;margin-bottom:20px;" src="'.$image_path.'" /></td>';		
		}					
		if (get_option("moveto_company_name")!='') {			 
		$company_header .= ' <td style="padding-left: 50px; text-align: right; padding-right: 20px;">'.get_option("moveto_company_name").'</td>';		
		} 				
				
		$company_header .= '</tr></table>';
		
		
		
		$this->email_parent_template =' 
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title></title>
	</head>

	<body style="background-color: #'.get_option("moveto_appearance_primary_color").'; padding: 20px; font-family: font-size: 14px; line-height: 1.43; font-family: &quot;Helvetica Neue&quot;, &quot;Segoe UI&quot;, Helvetica, Arial, sans-serif;">
		<div style="max-width: 600px; margin: 0px auto; background-color: #fff; box-shadow: 0px 20px 50px rgba(0,0,0,0.05);">
		'.$company_header.'
			<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);">
			###msg_content###
			</div>
		</div>	
	</body>
	</html>';
}

    
	
	/**
     * create moveto email template table
     */ 
	function create_table() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_email_templates';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name." (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `location_id` int(11) NOT NULL,
					  `email_template_name` varchar(100) NOT NULL,
					  `email_subject` varchar(200) NOT NULL,
					  `email_message` text NOT NULL,
					  `default_message` text NOT NULL,
					  `email_template_status` enum('e','d') NOT NULL,
					  `user_type` enum('AM','SP','C') NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	
	dbDelta($sql);     
			}
	} 
	
	/* Constructor Function to set the default values */
    function moveto_create_email_template()
    {
        global $wpdb;
        
        $moveto_email_templates = array(
           /* Admin email template New mover Request Requires Approval */
			'New Quote Request' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{admin_manager_name}}</h2><div style="color: #636363; font-size: 14px;">You have a new quote request as below.</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}<span></span>{{user_info}}</td></tr></table><div style="color: #636363; font-size: 14px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05); margin-bottom:50px;">Please proceed with sending quote from admin section.</div><br/>Thank You,<br />{{company_name}}</div>',		
			/* Admin email template quote Reminder */
            'Quote Request Reminder' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{admin_manager_name}}</h2><div style="color: #636363; font-size: 14px;">This is a reminder notification for following quote</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}<span></span>{{user_info}}</td></tr></table><div style="color:#636363; font-size: 14px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05);margin-bottom: 50px;">Please proceed with sending quote from admin section.</div><br/>Thank You,<br />{{company_name}}</div>',
           /* Admin/Manager email template mover Quote */
            'New Quote' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{admin_manager_name}}</h2><div style="color: #636363; font-size: 14px;">You have successfully sent quote for following mover ,</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Quote Details:</div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Estimate Price</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{quote_price}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Detail</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{quote_detail}}</p></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}<span></span>{{user_info}}</td></tr></table><br /><br />Thank You, <br /> {{company_name}}</div>',
           		
				
				
			/* Customer email template quote Request */
			
			'Quote Requests' => '<div style="border: 1px solid rgb(194, 194, 194); font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;padding: 60px 70px;border-radius: 5px;"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; padding-bottom:5px; font-size: 14px;border-bottom: 1px solid rgba(0,0,0,0.05);">
			You have successfully lead. Your quote details are noted below:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: 500; color: #050505; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: 500; color: #050505; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><label style="font-weight: 500; width:40%; float:left;color:rgb(153, 153, 153);">Source :</label><p style=" width:60%;float:left;margin:0;text-align:left;color: rgb(96, 96, 96);">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><label style="font-weight: 500; width:40%; float:left;color:rgb(153, 153, 153);">Destination :</label><p style=" width:60%;float:left;margin:0;text-align:left;color: rgb(96, 96, 96);">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><label style="font-weight: 500; width:40%; float:left;color:rgb(153, 153, 153);">Lead Date :</label><p style=" width:60%;float:left;margin:0;text-align:left;color: rgb(96, 96, 96);">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}<span></span>{{user_info}}</td></tr></table><div style="color: #636363; font-size: 14px; padding-top: 20px; border-top: 1px solid rgba(0,0,0,0.05); margin-bottom: 50px;">Please note that your quote is tentative and you we will send you quote.
			</div><br/>Thank You,<br />{{company_name}}</div>',
			
			/* Customer email template Quote Sent with price */
			
			'Mover Quote' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; font-size: 14px;">Quote for you request is as follow:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Quote Details:</div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Estimate Price</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{quote_price}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Detail</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{quote_detail}}</p></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source  :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}<span></span>{{user_info}}</td></tr></table><br/>Thank You, <br /> {{company_name}}</div>',
			
			/* customer email template quote Sent with price */
			
			'Quote Requests' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; font-size: 14px;">Quote for you request is as follow:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Quote Details:</div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Estimate Price</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{quote_price}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Quote Detail</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{quote_detail}}</p></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source  :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}<span></span>{{user_info}}</td></tr></table><br/>Thank You, <br /> {{company_name}}</div>',
			
			
			/* Customer email template Quote Sent without price */
			'Mover Quote without price' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; font-size: 14px;">Quote for you request is as follow:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="width:400px; padding:12px 5px;"></div><div style="width:400px; padding:12px 5px;"></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source  :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}{{insurance_rate}}<span></span>{{user_info}}</td></tr></table><br/>Thank You, <br /> {{company_name}}</div>',
				
			/* customer email template quote Sent without price */
			'Quote Requests without price' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h2 style="margin-top: 0px;">Hi {{client_name}}</h2><div style="color: #636363; font-size: 14px;">Quote for you request is as follow:</div><table style="margin-top: 30px; width: 100%;"><tr><td style="padding-right: 30px;"><div style="text-transform: uppercase; font-size: 11px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Lead No. {{order_id}} </div><div style="width:400px; padding:12px 5px;"></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;"><h4>Lead Details:</h4></div><div style="font-size: 13px; color: #111; font-weight: normal;"></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Source  :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{source_city}}</p></div><div style="width:400px; padding:12px 5px; float:none;"><strong style="font-weight: bold; width:60%; float:left;">Destination :</strong><p style="width:40%;text-align:right;float:left;margin:0;">{{destination_city}}</p></div><div style="width:400px; padding:12px 5px;"><strong style="font-weight: bold; width:60%; float:left;">Lead Date :</strong><p style=" width:40%;text-align:right;float:left;margin:0;">{{booking_date}}</p></div>{{service_info}}<span></span>{{loading_info}}<span></span>{{unloading_info}}<span></span>{{additional_info}}{{insurance_rate}}<span></span>{{user_info}}</td></tr></table><br/>Thank You, <br /> {{company_name}}</div>',
						
						
			/* customer email template New Account */
          /*   'Account created successfully,Follow account detail' => '<div style="padding: 60px 70px; border-top: 1px solid rgba(0,0,0,0.05);"><h1 style="margin-top: 0px;">Hi {{client_name}}</h1> <div style="color: #636363; font-size: 14px;"><p>Thank you for creating account on our website Once you click the button, just login to your account and you are set to go.</p></div><div style="text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin-bottom: 5px;">Your Login Credentials</div><br />{{account_detail}}<br /> .Please let us know if you facing any issue with your account. <br /><br /> Thank You, <br /> {{company_name}}</div>' */	
			);
		
        $return = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_email_templates");
        if (sizeof((array)$return) == 0) {           
			/* Inserting Email Template name in database */
			$email_template_name_array=array('AA','RMA','QA','AC','QC','ACW','QCW');
                     
            $arrCounter = 0;
            foreach ($moveto_email_templates as $option_key => $option_value) {
                if($arrCounter<=2){
					$usertype='AM';
				}elseif($arrCounter>=3 && $arrCounter<=5){
					$usertype='C';
				}						
						
                $return = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_email_templates  SET email_template_name ='" . $email_template_name_array[$arrCounter] . "',default_message='" . $option_value . "',email_subject='" .$option_key. "',user_type='".$usertype."'");
                $arrCounter++;
            }           
            
        }
       
    }
    
	/**
	* Read All Email Templates
	* @return $return - db records objects
	*/
    function readAll()
    {
        global $wpdb;
        $return = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_email_templates");
        return $return;
    }
    
	/**
	* Read One Email Template record
	* @return $return - db records objects
	*/
    function readOne()
    {
        global $wpdb;
        $return = $wpdb->get_results("SELECT email_subject,email_message,email_template_status,default_message FROM  ".$wpdb->prefix."mp_email_templates  WHERE  email_template_name='" . $this->email_template_name . "'");
        return $return;
    }
    
	
	/**
	* Update Email Template subject
	* @return $return - true on success,false on failure
	*/
	function update_template_subject_message()
    {
        global $wpdb;
        $return = $wpdb->query("UPDATE  ".$wpdb->prefix."mp_email_templates  SET email_subject  = '" . $this->email_subject . "', 
		email_message  = '" . $this->email_message . "' WHERE id  = '" . $this->id . "'");				
        if ($return) {
            return true;
        } else {
            return false;
        }
    }
    
	/**
	* Update template status
	* @return $return - true on success, false on failure
	*/
    function update_template_status()
    {
        global $wpdb;
        $return = $wpdb->query("UPDATE  ".$wpdb->prefix."mp_email_templates  SET email_template_status  = '".$this->template_status."' WHERE id  = '".$this->id."'");
        if ($return) {
            return true;
        } else {
            return false;
        }
    }
	
	/** 
	ReadAll Email Templates By User Type 
	**/
	function readall_by_usertype(){
		global $wpdb;
		global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_email_templates where user_type='".$this->user_type."'");
        return $result;	
	}
	
	/** 
	ReadAll Email Templates By User Type 
	**/
	function get_emailtemplate_by_sending_method(){
		global $wpdb;
		
        $result = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_email_templates where email_template_name='".$this->method."A' or email_template_name='".$this->method."S' or email_template_name='".$this->method."C'");
        return $result;	
	}
	
    
}
?>