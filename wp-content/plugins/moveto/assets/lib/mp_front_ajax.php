<?php  

	if(!session_id()) { @session_start(); }
	//print_r($_SESSION);
    $root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));	

	if (file_exists($root.'/wp-load.php')) {
        
		require_once($root.'/wp-load.php');	

	}		
	error_reporting(0);
	$plugin_url = plugins_url('',  dirname(__FILE__));
	$base =   dirname(dirname(dirname(__FILE__)));
	$uploaded_images_path = dirname(dirname(dirname(plugins_url( '/',dirname(__FILE__))))).'/uploads/';
	$currency_symbol = get_option('moveto_currency_symbol');

	include_once($base.'/objects/class_general.php');
	include_once($base.'/objects/class_service.php');
	include_once($base.'/objects/class_cities.php');
	include_once($base.'/objects/class_additional_info.php');
	include_once($base.'/objects/class_category.php');
	include_once($base.'/objects/class_provider.php');
	include_once($base.'/objects/class_front_moveto_first_step.php');
	include_once($base.'/objects/class_clients.php');
	include_once($base.'/objects/class_booking.php');
	include_once($base.'/objects/class_email_templates.php');
	include_once($base.'/objects/class_article_category.php');
	include_once($base.'/objects/class_room_type.php');
	include_once($base.'/objects/class_distance.php');

	/* declare classes */

	$mp_general = new moveto_general();	
	$mp_service = new moveto_service();
	$mp_city = new moveto_city();
	$moveto_additional_info = new moveto_additional_info();
	$first_step = new moveto_first_step();
	$mp_client_info = new moveto_clients();	
	$mp_booking = new moveto_booking();
	$email_template = new moveto_email_template();
	$article_category = new moveto_article_category();
	$room_type = new moveto_room_type();
	$obj_distance = new moveto_distance();
	$front_cubicfeets = new moveto_cubicfeets();
	

	function set_content_type() {			

		return 'text/html';		

	}

   if(get_option('moveto_tax') !=''){
		$tax = get_option('moveto_tax');
		}else{
			$tax = 0;
		}
    
/*Code By Ajay Prajapati For Distace Between Two Cities(26/3/19)*/
if(isset($_POST['action']) && $_POST['action']=='set_distance_session')
{
	$distance = $_POST['finalDistance'];
	$_SESSION['distances'] = round($distance);

	/* Bhupendra Comment */
 	/*$distanceArr = $obj_distance->get_distance();

 	foreach ($distanceArr as $value){
 		
 		if($distance > $value->distance){
 			$distance_price =  $value->price;
 			$_SESSION['distance_price'] = $distance_price;
 			exit;
 		}else{
 			$_SESSION['distance_price'] = $distanceArr[(sizeof($distanceArr) -1)]->price;
 		}

 	}*/
 	
}

if(isset($_POST['action']) && $_POST['action']=='mp_get_km_distance')
{
	$origintemp = array();
	$viatemp = array();
	$desttemp = array();
	$distance = 0;
	$distance1 = 0;
	$distance2 = 0;
	$origin = "";
	$via = "";
	$destination = "";
	$APIKey = get_option('moveto_google_api_key');
	if($APIKey != ""){
	if($_POST['source_city'] != "" && $_POST['destination_city'] != "" && $_POST['via'] == ""){
			$origintemp = explode(" ", $_POST['source_city']);
			$desttemp = explode(" ", $_POST['destination_city']);
			foreach ($origintemp as $value) {
				$origin .= $value; 
			}
			//$origin = $_POST['source_city']; 
			foreach ($desttemp as $value) {
				$destination .= $value; 
			}
			
			//$destination = $_POST['destination_city'];
			
			$api = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$origin."&destinations=".$destination."&key=".$APIKey."");
			$data = json_decode($api);
		if(get_option('moveto_distance_unit') == "miles"){
			if($data->rows[0]->elements[0]->status == 'OK'){
				$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1609.344 );
			}else{
				$distance = 0;
			}
		}else{
			if($data->rows[0]->elements[0]->status == 'OK'){
				$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
			}else{
				$distance = 0;
			}
		}
	}

	if($_POST['source_city'] != "" && $_POST['destination_city'] != "" && $_POST['via'] != ""){
			$origintemp = explode(" ", $_POST['source_city']);
			$viatemp = explode(" ", $_POST['via']);
			$desttemp = explode(" ", $_POST['destination_city']);
			foreach ($origintemp as $value) {
				$origin .= $value; 
			}
			foreach ($viatemp as $value) {
				$via .= $value; 
			}
			foreach ($desttemp as $value) {
				$destination .= $value; 
			}
			

			
			$api1 = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$origin."&destinations=".$via."&key=".$APIKey."");
			$data1 = json_decode($api1);
			
				if(get_option('moveto_distance_unit') == "miles"){
				if($data1->rows[0]->elements[0]->status == 'OK'){
				$distance1 = ($data1->rows[0]->elements[0]->distance->value / 1609.344 );
			}
			}else{
				if($data1->rows[0]->elements[0]->status == 'OK'){
				$distance1 = round((int)$data1->rows[0]->elements[0]->distance->value / 1000);
			}
			}
			
		
			$api2 = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$via."&destinations=".$destination."&key=".$APIKey."");
			$data2 = json_decode($api2);
			if(get_option('moveto_distance_unit') == "miles"){
				if($data2->rows[0]->elements[0]->status == 'OK'){
				$distance2 = ($data2->rows[0]->elements[0]->distance->value / 1609.344 );
			}
			}else{
				if($data2->rows[0]->elements[0]->status == 'OK'){
				$distance2 = round((int)$data2->rows[0]->elements[0]->distance->value / 1000);
			}
			}

			if(($data1->rows[0]->elements[0]->status != 'OK') || ($data2->rows[0]->elements[0]->status != 'OK')){
				$distance = 0;
			}else{
				$distance = $distance2 + $distance1;
			}
			
	}
	$_SESSION['distances'] = $distance;
	echo $_SESSION['distances'];
 	$distanceArr = $obj_distance->get_distance();

 	foreach ($distanceArr as $value){
 		
 		if($distance > $value->distance){
 			$distance_price =  $value->price;
 			$_SESSION['distance_price'] = $distance_price;
 			exit;
 		}else{
 			$_SESSION['distance_price'] = $distanceArr[(sizeof((array)$distanceArr) -1)]->price;
 		}

 	}
 	
	}else{
		echo "key missing";
	}


	
 	
}


/* Get Destination Cities */

if(isset($_POST['action'],$_POST['source_city']) && $_POST['action']=='get_destination_city' && $_POST['source_city']!='')

{

	

	$mp_city->source_city = addslashes(base64_decode($_POST['source_city']));

	$destination_cities = $mp_city->get_route_destination_cities();

	

	if(sizeof((array)$destination_cities)>0){ ?>

		<option value=""><?php echo __("Select Destination","mp");?></option><?php

		foreach($destination_cities as $destination_citie){ 

			if(base64_encode($destination_citie->source_city) == $_POST['source_city']){

				?><option value="<?php echo base64_encode($destination_citie->destination_city);?>"><?php echo $destination_citie->destination_city;?>
				<?php 

			}

			if(base64_encode($destination_citie->destination_city) == $_POST['source_city']){

				?><option value="<?php echo base64_encode($destination_citie->source_city);?>"><?php echo $destination_citie->source_city;?></option>
				<?php 

			}			

		}
		?>
		</option><option value="<?php echo "Other";?>"><?php echo "Other";?></option>
		<?php

	}else{ ?>

		<option value=""><?php echo __("No route found","mp");?></option><?php		
		
	}	

	

}



/* Set Step One Session */

if(isset($_POST['action']) && $_POST['action']=='set_stepone_session')

{

	if(isset($_SESSION['pam_stepone_session'])){

		$pam_stepone_session = unserialize($_SESSION['pam_stepone_session']);

	}else{

		$pam_stepone_session = array();

	}
	
	$pam_stepone_session['selectcategory'] = $_POST['selectcategory'];

	$pam_stepone_session['source_city'] = $_POST['source_city'];
	
	if($_POST['destination_city']=='Other'){
		
		$pam_stepone_session['destination_city'] = base64_encode($_POST['desctination_city_text']);
	}else{
	
		$pam_stepone_session['destination_city'] = $_POST['destination_city'];
	}
	$pam_stepone_session['selected_date'] = $_POST['selected_date'];

	$_SESSION['pam_stepone_session'] = serialize($pam_stepone_session);



}

/* Set Step Two Session */

if(isset($_POST['action']) && $_POST['action']=='set_steptwo_session')
{

	if(isset($_SESSION['set_steptwo_session'])){

		$set_steptwo_session = unserialize($_SESSION['set_steptwo_session']);

	}else{

		$set_steptwo_session = array();

	}

	$set_steptwo_session['service_info'] = serialize($_POST['service_info']);

	$set_steptwo_session['loading_info'] = serialize($_POST['loading_info']);
	
	$set_steptwo_session['cabs_info'] = serialize($_POST['cabs_info']);

	$set_steptwo_session['unloading_info'] = serialize($_POST['unloading_info']);

	$set_steptwo_session['additional_info'] = serialize($_POST['additional_info']);

	$set_steptwo_session['articles_info'] = serialize($_POST['articles_info']);

	$_SESSION['set_steptwo_session'] = serialize($set_steptwo_session);	

}



/* Fetch Select Service Addons */

if(isset($_POST['action'],$_POST['selectcategory']) && $_POST['action']=='load_service_articles' && $_POST['selectcategory']!='')

{

	

	$mp_service->service_id = $_POST['selectcategory'];

	$mp_service_addons = $mp_service->readAll_addons();							
	

	if(sizeof((array)$mp_service_addons)>0){ ?>

		<form  method="post" id="icon-info">

			<h3 class="block-title fs-22"><i class="icon-note icons fs-22 pr-7"></i> <?php echo __("Select Articles to be moved","mp");?></h3>

				<div class="pr mp-sm-12 mp-md-12 mp-sm-12 mp-xs-12 np">

					<div class="mp-extra-services-list mp-common-box">

						<ul class="addon-service-list fullwidth np">

					<?php foreach($mp_service_addons as $mp_addon){ ?>

							<li class="mp-sm-4 mp-md-3 mp-lg-2 mp-xs-6 mb-15">

								<input type="checkbox" name="addon-checkbox<?php echo $mp_addon->id;?>" data-saddonid="<?php echo $mp_addon->id;?>" data-saddonmaxqty="<?php echo $mp_addon->maxqty;?>" class="addon-checkbox" id="service_addon_<?php echo $mp_addon->id;?>" />

								<label class="mp-addon-ser border-c" data-addonid="<?php echo $mp_addon->id;?>" for="service_addon_<?php echo $mp_addon->id;?>">

									<span></span>

									<div class="addon-price fullwidth"><div class="addon-name fullwidth text-center"><?php echo $mp_addon->addon_service_name; ?></div></div>

									<div class="mp-addon-img">

										<?php if($mp_addon->image!=''){ ?>

										<img src="<?php echo $uploaded_images_path; ?><?php echo $mp_addon->image;?>" />

										<?php }else{ ?>

										<img src="<?php echo $plugin_url; ?>/images/addon.png" />

										<?php } ?>

									</div>

								</label>

								<div class="mp-addon-count mp-addon-count<?php echo $mp_addon->id;?> border-c add_minus_button">

								<div class="mp-btn-group">

								<?php if($mp_addon->multipleqty=='Y'){?><button data-addonmax="<?php echo $mp_addon->maxqty;?>" data-addonid="<?php echo $mp_addon->id;?>" data-qtyaction="minus" class="minus mp-btn-left mp-small-btn mp_addonqty" type="button">-</button><?php } ?>

								<?php if($mp_addon->multipleqty=='Y'){?><input type="text" value="1" id="addonqty_<?php echo $mp_addon->id;?>" class="mp-btn-text addon_qty" /><?php }else{ ?>

								<input type="text" value="1" id="addonqty_<?php echo $mp_addon->id;?>" style="display:none;" class="mp-btn-text addon_qty" />

								<?php } ?>

								<?php if($mp_addon->multipleqty=='Y'){?><button data-addonmax="<?php echo $mp_addon->maxqty;?>" data-addonid="<?php echo $mp_addon->id;?>" data-qtyaction="add" class="add mp-btn-right pull-right mp-small-btn mp_addonqty" type="button">+</button><?php } ?>

								</div>

								</div>	

							</li>			

					<?php } ?>

						</ul>

					</div>

				</div>											

		</form>

	<?php }		
	
}


/* Complete Checkkout & Save Booking */





















/* register , login and booking complete code here START */

if(isset($_POST['action']) && $_POST['action']=='check_existing_username'){

	$email =$_POST['email'];

	$exists = email_exists( $email );

	if (!email_exists($email)) {

		echo "true";

	} else {

		echo "false";

	}

}

if(isset($_POST['action']) && $_POST['action']=='get_existing_user_data'){

	$loginemail = $_POST['uname'];

	$loginpass = $_POST['pwd'];

	

	$user = get_user_by( 'email', $loginemail );

	if ( $user && wp_check_password( $loginpass, $user->data->user_pass, $user->ID) ){

		$user_id = $user->data->ID;

		$user_login = $user->data->user_login;

		wp_set_current_user( $user_id, $user_login );

		wp_set_auth_cookie( $user_id );

				

		$mp_booking->client_id = $user_id;

		$mp_booking->read_client_recent_checkout_info();

		

		if($mp_booking->user_info!=''){

			$userinfo = unserialize($mp_booking->user_info);			
			$first = ucfirst($userinfo['first_name']);
			$second = ucfirst($userinfo['last_name']);
			$get_data_1  = array("gender"=>$userinfo['user_gender'],"user_id"=>$user_id ,"user_email"=>$loginemail,"password"=>'',"first_name"=>$first,"last_name"=>$second,"phone"=>$userinfo['user_phone'],"address"=>$userinfo['user_address'],"city"=>$userinfo['user_city'],"state"=>$userinfo['user_state'],"notes"=>$userinfo['user_notes'],"ccode"=>$userinfo['user_ccode'],"user_zip"=>$userinfo['user_zipcode']);

			$get_data_2 = unserialize($userinfo['custom_fields_details']);			

		}else{

			$get_data_1 = array("gender"=>"M" ,"user_id"=>"","user_email"=>"","password"=>"","first_name"=>"","last_name"=>"","phone"=>"","address"=>"","city"=>"","state"=>"","notes"=>"","ccode"=>"","user_zip"=>"");

			$get_data_2 = array();

		}

			

		$get_data = array_merge($get_data_1,$get_data_2);		

		echo json_encode($get_data);die();

		

	}else{

	   echo "Invalid Username or Password";

	}

}

if(isset($_POST['action']) && $_POST['action']=='mp_logout_user'){

	wp_logout();

	unset($_SESSION['client_mp_name']);

	unset($_SESSION['client_mp_email']);

	unset($_SESSION['client_first_name']);

	unset($_SESSION['client_last_name']);

	unset($_SESSION['client_mp_ID']);

}

if(isset($_POST['action']) && $_POST['action']=='mp_booking_complete'){

	$preff_username = $_POST['username'];

	$preff_password = $_POST['pwd'];

	$first_name = ucwords($_POST['fname']);

	$last_name = ucwords($_POST['lname']);

	$user_phone = $_POST['phone'];

	$user_gender = $_POST['gender'];

	$user_address = $_POST['address'];

	$user_zipcode = $_POST['zipcode'];

	$user_city = $_POST['city'];

	$user_state = $_POST['state'];

	$user_notes = $_POST['notes'];

	$user_ccode = $_POST['ccode'];

	$username = $_POST['fname'].rand(0,999);

	

	if(isset($_POST['dynamic_field_add'])){

		$extra_details = $_POST['dynamic_field_add'];

	}else{

		$extra_details = array();

	}

	$serialize_extra_details = serialize($extra_details);

	

	

	

	$company_name = get_option('moveto_company_name');

	$company_address = get_option('moveto_company_address');

	$company_city = get_option('moveto_company_city');

	$company_state = get_option('moveto_company_state');

	$company_zip = get_option('moveto_company_zip');

	$company_country = get_option('moveto_company_country');

	$company_phone = get_option('moveto_company_country_code').get_option('moveto_company_phone');

	$company_email = get_option('moveto_company_email');

	$company_logo = $business_logo = site_url()."/wp-content/uploads/".get_option('moveto_company_logo');

	$admin_name = get_option('moveto_email_sender_name');		

	$sender_email_address = get_option('moveto_email_sender_address');		

	$headers = "From: $admin_name <$sender_email_address>" . "\r\n";

	

	

	

	

	

	if($_POST['mp_user_type'] == 'guest'){

		$user_id = 0;		

	}else if($_POST['mp_user_type'] == 'existing'){

		$user_id = get_current_user_id();

		if(!current_user_can('mp_client')){		

			$user = new WP_User($user_id);

			$user->add_cap('read');

			$user->add_cap('mp_client');

		}	

	}else{

		$mp_user_info = array(

						'user_login'    =>   $username,

						'user_email'    =>   $preff_username,

						'user_pass'     =>   $preff_password,

						'first_name'    =>   $first_name,

						'last_name'     =>   $last_name,

						'nickname'      =>  '',

						'role' => 'subscriber'

						);

		   

		$new_mp_user = wp_insert_user( $mp_user_info );

			

		$user = new WP_User($new_mp_user);

		$user->add_cap('read');

		$user->add_cap('mp_client'); 

		$user->add_role('mp_users');

		$user_id = $new_mp_user;

		$user_login = $preff_username;	

		wp_set_current_user( $user_id, $user_login );

		wp_set_auth_cookie( $user_id );

			

		$account_detail = '<div style="font-size: 13px; color: #111; font-weight: normal;">

			<div style="width:400px; padding:0px 5px;">

				<strong style="font-weight: bold; width:60%; float:left;">Email/Username :</strong>

				<p style=" width:40%;text-align:right; float:left;margin:0;">'.$preff_username.'</p>

				</div>

				<div style="width:400px; padding:0px 5px;">

					<strong style="font-weight: bold; width:60%; float:left;">Password :</strong>

					<p style=" width:40%;text-align:right;float:left;margin:0;">N/A</p>

				</div>

			</div>

		<div>

			<a href="'.site_url().'/wp-admin/" style="padding: 8px 20px; background-color: #4B72FA; color: #fff; font-weight: bolder; font-size: 16px; display: table; margin: 20px auto; text-decoration: none;">'.__('Sign In','mp').'</a>

		</div>';

			

		$mp_clientemail_templates = new moveto_email_template();

		$msg_template = $mp_clientemail_templates->email_parent_template;	

		$mp_clientemail_templates->email_template_name = "NAC";

		$template_detail = $mp_clientemail_templates->readOne();        

		if($template_detail[0]->email_message!=''){            

			$email_content = $template_detail[0]->email_message;        

		}else{            

			$email_content = $template_detail[0]->default_message;        

		}        

		$email_subject = $template_detail[0]->email_subject;

		$email_client_message = '';

		/* Sending email to client when New Appointment request Sent */ 		  

		if($template_detail[0]->email_template_status=='e'){

			$email_client_message = str_replace(array('{{client_name}}','{{account_detail}}','{{company_name}}'),array($first_name.''.$last_name,$account_detail,$company_name),$email_content);	

			$email_client_message = str_replace('###msg_content###',$email_client_message,$msg_template);		

			add_filter( 'wp_mail_content_type', 'set_content_type' );				

			$status = wp_mail($preff_username,$email_subject,$email_client_message,$headers);           

		}

	}

	

	$mp_booking->get_last_order_id();

	if(isset($mp_booking->last_order_id) && $mp_booking->last_order_id!=''){

		$next_order_id = $mp_booking->last_order_id + 1;	

	}else{

		$next_order_id = 1001;

	}

		

	/* Checkout Form Info */

	$user_info = serialize(array('email'=>$preff_username,'first_name' => $first_name, 'last_name' => $last_name, 'user_phone' => $user_phone, 'user_gender' => $user_gender, 'user_address' => $user_address,'user_zipcode'=>$user_zipcode, 'user_city' => $user_city, 'user_state' => $user_state, 'user_notes' => $user_notes, 'user_ccode' => $user_ccode,'custom_fields_details' => $serialize_extra_details));

	

	/* First Step Info */

	$first_step_info = unserialize($_SESSION['pam_stepone_session']);

	$service_id = $first_step_info['selectcategory'];

	$source_city = $first_step_info['source_city'];

	$destination_city = $first_step_info['destination_city'];

	$booking_date = $first_step_info['selected_date'];

		

	/* Second Step Info */

	$second_step_info = unserialize($_SESSION['set_steptwo_session']);	

	$service_info = $second_step_info['service_info'];

	$articles_info = $second_step_info['articles_info'];

	$loading_info = $second_step_info['loading_info'];
	
	$cabs_info = $second_step_info['cabs_info'];

	$unloading_info = $second_step_info['unloading_info'];

	$additional_info = $second_step_info['additional_info'];

	

	

	/* Add Booking Info Into Database */

	$mp_booking->order_id = $next_order_id;

	$mp_booking->client_id = $user_id;

	$mp_booking->service_id = $service_id;

	$mp_booking->source_city = $source_city;

	$mp_booking->destination_city = $destination_city;

	$mp_booking->booking_date = date_i18n('Y-m-d',strtotime($booking_date));

	$mp_booking->quote_price = '';

	$mp_booking->quote_detail = '';

	$mp_booking->service_info = $service_info;

	$mp_booking->articles_info = $articles_info;

	$mp_booking->loading_info = $loading_info;

	$mp_booking->cabs_info = $cabs_info;

	$mp_booking->unloading_info = $unloading_info;

	$mp_booking->additional_info = $additional_info;

	$mp_booking->user_info = $user_info;

	$mp_booking->booking_status = 'P';

	$mp_booking->reminder = '0';

	$mp_booking->notification = '0';

	$mp_booking->lastmodify = date_i18n('Y-m-d H:i:s');

	/* Add Booking */

	$mp_booking->add_booking();

	

	

	/* Getting Values For Email Template Tags */

	if($service_id==1){

		$service_title = __('Home','mp');	

	}elseif($service_id==2){

		$service_title = __('Office','mp');
	}elseif($service_id==3){

		$service_title = __('Vehicle','mp');
	}elseif($service_id==4){

		$service_title = __('Pets','mp');
	}elseif($service_id==6){

		$service_title = __('Other','mp');
	}else{
		
		$service_title = __('Commercial','mp');
	}

	  

	$bookingstatus = __('Pending','mp');

	

	/* Get User Info */

	if($user_info!=''){

		$userinfo = unserialize($user_info);		

		$pam_email = $userinfo['email'];

		$pam_gender = $userinfo['user_gender'];

		$pam_first_name = $userinfo['first_name'];

		$pam_last_name = $userinfo['last_name'];

		$pam_phone = $userinfo['user_phone'];

		$user_phone=$userinfo['user_phone'];

		$pam_address = $userinfo['user_address'];

		$pam_city = $userinfo['user_city'];

		$pam_state = $userinfo['user_state'];

		$pam_notes = $userinfo['user_notes'];

		$pam_custom_fields = unserialize($userinfo['custom_fields_details']);

	}else{

		$pam_email = '';		$pam_gender = '';		$pam_first_name = '';		$pam_last_name = '';		$pam_phone = '';		$pam_address = '';		$pam_city = '';		$pam_state = '';		$pam_notes = '';

		$pam_custom_fields = '';

	}



	/* Get Service Info */

	$service_info_detail = '';

	if($service_info!=''){

		$service_info_arr = unserialize($service_info);

		if($service_id==1){			

			$service_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Loading Information","mp").'</div>';

			if($service_info_arr['loading_bunglow_type']!=''){

			$service_info_detail .= '<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Home Type","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.__("Bungalow","mp").'</p></div><div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Bungalow Type","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['loading_bunglow_type'].'</p></div>';
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Bungalow No","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['loading_floor_number'].'</p></div>';

			}else{

			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Home Type","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'. __("Appartment","mp").'</p></div>';
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Floor Number","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['loading_floor_number'].'</p></div>';

			} 

			$service_info_detail .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Unloading Information","mp").'</div>';

			if($service_info_arr['unloading_bunglow_type']!=''){

			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Home Type","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.__("Bungalow","mp").'</p></div><div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Bungalow Type","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['unloading_bunglow_type'].'</p></div>';
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Bungalow No","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['unloading_floor_number'].'</p></div>';
			}else{

			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Home Type","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'. __("Appartment","mp").'</p></div>';

			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Floor Number","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['unloading_floor_number'].'</p></div>';
			} 

		}

		if($service_id==2){

			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Office Area","mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['office_area'].'</p></div>';

		}
		if($service_id==3){
				if(isset($service_info_arr['vehicle_type'])){ 
				$vehical_array = array();
				$vehical_no_array = array();
					foreach($service_info_arr['vehicle_type'] as $ser_type){
						$vehical_array[] = $ser_type;
					}
					foreach($service_info_arr['no_of_vehicle'] as $no_of_veh){
						$vehical_no_array[] = $no_of_veh;
					}
					$service_info .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("VEHICLE DETAILS","mp").'</div>';
					$service_info .='<div style="width:50%; padding:0px 5px;"><strong style="font-weight: bold;float:left;">'. __("Vehicle Type & Qty","mp").'</strong></div>';
					$service_info .='<div style="width:50%; padding:0px 5px;float:left;">';
					for($i=0;$i<sizeof((array)$vehical_array);$i++){
						$service_info .= '<p style=" width:100%;text-align:right;float:right;margin:0;">'.$vehical_array[$i].' - '.$vehical_no_array[$i].'</p>';
					}
					$service_info .='</div>';
				}else{
						$service_info = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("VEHICLE DETAILS","mp").'</div>';
						$service_info .= '<div style="width:400px; padding:0px 5px;"><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['vehicle_qty'].'</p></div>';
				}
			}	
		
		if($service_id==4){	
		foreach($service_info_arr as $key => $value){	
			$service_info_detail .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Pet Information","mp").'</div>';
	
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$value['pets_service'].'</strong></div>';
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Pet Name","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$value['pet_name'].'</p></div>';
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Pet Breed","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$value['pet_name'].'</p></div>';
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Pet Age","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$value['pet_age'].' '.$value['pets_age_radio'].'</p></div>';
			
			
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Pet Weight","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$value['pet_weight'].' '.$value['pet_weight_gen'].'</p></div>';			
			}
		}
		if($service_id==6){	
				
		/* foreach($service_info_arr as $key => $value){ */
			
			$service_info_detail .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Other Information","mp").'</div>';
	
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Other Service Description","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['other_service_description'].'</p></div>';
	
			$service_info_detail .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("other service floor no","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['other_service_floor_no'].'</p></div>';
					
			/* } */
		}			
	}

	

	/* Get Articles Info */

	$articles_info_detail = '';
	if($service_id==6){
			if($articles_info!=''){
				$articles_info_arr = unserialize($articles_info);
				
				$articles_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Service Articles","mp").'</div>';
				$i=1;
				foreach($articles_info_arr['other_service_article'] as $rer){
				
					$articles_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. __("Article","mp").$i.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$rer.'</p></div>';
					$i++;
					
				}	
				}
			}else{

	if($articles_info!=''){

		$articles_info_arr = unserialize($articles_info);
		if(sizeof((array)$articles_info_arr)>0){

			$articles_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Service Articles","mp").'</div>';

			foreach($articles_info_arr as $key => $value){

				$mp_service->addon_id = $key;

				$article_name = $mp_service->readOne_addon();

				$articles_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'. $article_name[0]->addon_service_name.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value.'</p></div>';

			}

		}	

	}
}

	/* Get Additional Info */

	$additional_info_detail = '';

	if($additional_info!=''){

		

		$additional_info_arr = unserialize($additional_info);

		if(sizeof((array)$additional_info_arr)>0){

			$additional_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Additional Information","mp").'</div>';

			foreach($additional_info_arr as $addkey => $addvalue){			

				if($addvalue=='Y'){

					$additional_status = __('Yes','mp');

				}else{

					$additional_status = __('No','mp');

				}

				$moveto_additional_info->id = $addkey;

				$moveto_additional_info->readOne();

				$additional_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$moveto_additional_info->additional_info.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$additional_status.'</p></div>';

			}

		}	

	}

	

	/* Get Loading Info */

	$loading_info_detail = '';

	if($loading_info!=''){										

		$loading_info_arr = unserialize($loading_info);				

			$loading_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Loading Address","mp").'</div>';

			$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["pam_loading_street_address"].'</p></div>';

			$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["pam_loading_city"].'</p></div>';

			$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["pam_loading_state"].'</p></div>';

			

	}

	
	/* Get Cabs Details */

	$cabs_info_detail = '';

	if($cabs_info!='N;'){										

		$cabs_info_arr = unserialize($cabs_info);				
			/* From */
			$cabs_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Cabs Info(From)","mp").'</div>';

			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["pam_street_address_cab"].'</p></div>';

			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["pam_city_cab"].'</p></div>';

			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["pam_state_cab"].'</p></div>';
			/* TO */
			$cabs_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Cabs Info(To)","mp").'</div>';

			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["to_pam_street_address_cab"].'</p></div>';

			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["to_pam_city_cab"].'</p></div>';

			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["to_pam_state_cab"].'</p></div>';
			
			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs Members',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["pam_member_cab"].'</p></div>';
			
			$cabs_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Cabs Date',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cabs_info_arr["cab_date"].'</p></div>';										

	}
	
	/* Get UnLoading Info */

	$unloading_info_detail = '';

	if($unloading_info!=''){										

		$unloading_info_arr = unserialize($unloading_info);				

		$unloading_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Unloading Address","mp").'</div>';

		$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["pam_unloading_street_address"].'</p></div>';

		$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["pam_unloading_city"].'</p></div>';

		$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["pam_unloading_state"].'</p></div>';								
	}

	

	/* Get Booking Client Info */

	$user_info_detail = '';

	$client_email = '';

	$client_phone = '';

	$client_name = '';

	if($user_info!=''){										

		$user_info_arr = unserialize($user_info);

		$client_email = $user_info_arr["email"];

		$client_phone = $user_info_arr["phone"];

		$client_phone = $phone;

		$client_name = $user_info_arr["name"];
								

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Name',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["name"].'</p></div>';

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Email',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["email"].'</p></div>';

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Phone',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["phone"].'</p></div>';

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Gender',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_gender.'</p></div>';

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["user_address"].'</p></div>';				

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["user_city"].'</p></div>';					

		$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["user_state"].'</p></div>';

	

		$pam_custom_fields = unserialize($user_info_arr['custom_fields_details']);

		if(sizeof((array)$pam_custom_fields)>0){

			foreach($pam_custom_fields as $fieldkey => $fieldvalue){

				$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$fieldkey.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$fieldvalue.'</p></div>';	

			}		
		
		}		
		$user_info_detail .=$cabs_info_detail;
	}	
	

	$search = array('{{client_name}}','{{admin_manager_name}}','{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}','{{order_id}}','{{service_title}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{articles_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_price}}','{{quote_detail}}','{{cabs_info}}');      
			

	$replace_with = array($client_name,$admin_name,$company_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo,$next_order_id,$service_title,base64_decode($source_city),base64_decode($destination_city),date_i18n(get_option('date_format'),strtotime($booking_date)),$bookingstatus,$service_info_detail,$articles_info_detail,$loading_info_detail,$unloading_info_detail,$additional_info_detail,$user_info_detail,'','',$cabs_info_detail); 



	/* Send email to Client when booking is complete */	

	if(get_option('moveto_client_email_notification_status')=='E' && $client_email!=''){	

		$mp_clientemail_templates = new moveto_email_template();

		$msg_template = $mp_clientemail_templates->email_parent_template;	

		$mp_clientemail_templates->email_template_name = "AC";

		$template_detail = $mp_clientemail_templates->readOne();        

		if($template_detail[0]->email_message!=''){            

			$email_content = $template_detail[0]->email_message;        

		}else{            

			$email_content = $template_detail[0]->default_message;        

		}        

		$email_subject = $template_detail[0]->email_subject;

		$email_client_message = '';

		/* Sending email to client when New Appointment request Sent */ 		  

		if($template_detail[0]->email_template_status=='e'){

			$email_client_message = str_replace($search,$replace_with,$email_content);	

			$email_client_message = str_replace('###msg_content###',$email_client_message,$msg_template);		

			add_filter( 'wp_mail_content_type', 'set_content_type' );				

			$status = wp_mail($client_email,$email_subject,$email_client_message,$headers);           

		}       

	}

	/* Send email to Admin when booking is complete */

	if(get_option('moveto_admin_email_notification_status')=='E'){

		$mp_adminemail_templates = new moveto_email_template();

		$msg_template = $mp_adminemail_templates->email_parent_template;

		$mp_adminemail_templates->email_template_name = "AA";  		

		$template_detail = $mp_adminemail_templates->readOne();        

		if($template_detail[0]->email_message!=''){            

			$email_content = $template_detail[0]->email_message;        

		}else{            

			$email_content = $template_detail[0]->default_message;

		}        

		$email_subject = $template_detail[0]->email_subject;

		$email_admin_message = '';				

		$email_admin_message = str_replace($search,$replace_with,$email_content);	

		$email_admin_message = str_replace('###msg_content###',$email_admin_message,$msg_template);

		add_filter( 'wp_mail_content_type', 'set_content_type' );		

		$status = wp_mail(get_option('moveto_email_sender_address'),$email_subject,$email_admin_message,$headers);	

	}

	/******************* Send Sms code START *********************/

	if(get_option("moveto_sms_reminder_status") == "E"){

		/** TWILIO **/

		if(get_option("moveto_sms_noti_twilio") == "E"){

			$obj_sms_template = new moveto_sms_template();

			$twillio_sender_number = get_option('moveto_twilio_number');

			$AccountSid = get_option('moveto_twilio_sid');

			$AuthToken =  get_option('moveto_twilio_auth_token'); 

			

			/* Send SMS To Client */

			if(get_option('moveto_twilio_client_sms_notification_status') == "E"){

				$twilliosms_client = new Client($AccountSid, $AuthToken);

				$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');

				

				if($template1[0]->sms_template_status == "e" && $user_phone!=''){

					if($template1[0]->sms_message == ""){

						$message = strip_tags($template1[0]->default_message);

					}else{

						$message = strip_tags($template1[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					$client_sms_body = str_replace($search,$replace_with,$message);

					$twilliosms_client->messages->create(

						$user_phone,

						array(

							'from' => $twillio_sender_number,

							'body' => $client_sms_body 

						)

					);

				}

			}

			

			/* Send SMS To Admin */

			if(get_option('moveto_twilio_admin_sms_notification_status') == "E"){		   

				$twilliosms_admin = new Client($AccountSid, $AuthToken);

				$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');

				if($template[0]->sms_template_status == "e" && get_option('moveto_twilio_admin_phone_no')!=''){

					if($template[0]->sms_message == ""){

						$message = strip_tags($template[0]->default_message);

					}else{

						$message = strip_tags($template[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					

					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					

					$admin_sms_body = str_replace($search,$replace_with,$message);	



					$twilliosms_staff->messages->create(

						get_option('moveto_twilio_ccode').get_option('moveto_twilio_admin_phone_no'),

						array(

							'from' => $twillio_sender_number,

							'body' => $admin_sms_body 

						)

					);

				}

			}

		}

				

		/** PLIVO **/

		if(get_option("moveto_sms_noti_plivo") == "E"){

			$obj_sms_template = new moveto_sms_template();

			$plivo_sender_number = get_option('moveto_plivo_number');	

			$auth_sid = get_option('moveto_plivo_sid');

			$auth_token = get_option('moveto_plivo_auth_token');

			

			/* Send SMS To Client */

			if(get_option('moveto_plivo_client_sms_notification_status') == "E"){

				$p_client = new Plivo\RestAPI($auth_sid, $auth_token, '', '');

				$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');

				if($template1[0]->sms_template_status == "e" && $user_phone!=''){

					if($template1[0]->sms_message == ""){

						$message = strip_tags($template1[0]->default_message);

					}else{

						$message = strip_tags($template1[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					$client_sms_body = str_replace($search,$replace_with,$message);

					

					$clientparams = array(

						'src' => $plivo_sender_number,

						'dst' => $user_phone,

						'text' => $client_sms_body,

						'method' => 'POST'

					);

					$response = $p_client->send_message($clientparams);

				}

			}

			/* Send SMS To Admin */

			if(get_option('moveto_plivo_admin_sms_notification_status') == "E"){		   

				$twilliosms_admin = new Client($AccountSid, $AuthToken);

				$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');

				if($template[0]->sms_template_status == "e" && get_option('moveto_plivo_admin_phone_no')!=''){

					if($template[0]->sms_message == ""){

						$message = strip_tags($template[0]->default_message);

					}else{

						$message = strip_tags($template[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					

					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					

					$admin_sms_body = str_replace($search,$replace_with,$message);	



					$adminparams = array(

						'src' => $plivo_sender_number,

						'dst' => get_option('moveto_plivo_ccode').get_option('moveto_plivo_admin_phone_no'),

						'text' => $admin_sms_body,

						'method' => 'POST'

						);

					$response = $p_admin->send_message($adminparams);

				}

			}

		}

		

		/** NEXMO **/

		if(get_option("moveto_sms_noti_nexmo") == "E"){

			$obj_sms_template = new moveto_sms_template();

			include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_nexmo.php');

			$nexmo_client = new moveto_nexmo();

			$nexmo_client->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');

			$nexmo_client->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');

			$nexmo_client->moveto_nexmo_form = get_option('moveto_nexmo_form');

			

			/* Send SMS To Client */

			if(get_option('moveto_nexmo_send_sms_client_status') == "E"){

				$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');

				if($template1[0]->sms_template_status == "e" && $user_phone!=''){

					if($template1[0]->sms_message == ""){

						$message = strip_tags($template1[0]->default_message);

					}else{

						$message = strip_tags($template1[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					$client_sms_body = str_replace($search,$replace_with,$message);

					$res = $nexmo_client->send_nexmo_sms($user_ccode.''.$user_phone,$client_sms_body);

				}

			}



			/* Send SMS To Admin */

			if(get_option('moveto_nexmo_send_sms_admin_status') == "E"){

				$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');

				if($template[0]->sms_template_status == "e" && get_option('moveto_nexmo_admin_phone_no')!=''){

					if($template[0]->sms_message == ""){

						$message = strip_tags($template[0]->default_message);

					}else{

						$message = strip_tags($template[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					

					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					

					$admin_sms_body = str_replace($search,$replace_with,$message);

					$nexmo_client->send_nexmo_sms(get_option('moveto_nexmo_ccode').get_option('moveto_nexmo_admin_phone_no'),$admin_sms_body);

				}

			}

		}

		

		/** TEXTLOCAL **/

		if(get_option("moveto_sms_noti_textlocal") == "E"){

			$obj_sms_template = new moveto_sms_template();

			$textlocal_apikey = get_option('moveto_textlocal_apikey');

			$textlocal_sender = get_option('moveto_textlocal_sender');

			

			/* Send SMS To Client */

			if(get_option('moveto_textlocal_client_sms_notification_status') == "E"){

				$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');

				if($template1[0]->sms_template_status == "e" && $user_phone!=''){

					if($template1[0]->sms_message == ""){

						$message = strip_tags($template1[0]->default_message);

					}else{

						$message = strip_tags($template1[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					$client_sms_body = str_replace($search,$replace_with,$message);

					

					$textlocal_numbers = $user_phone; 

					$textlocal_sender = urlencode($textlocal_sender);

					$client_sms_body = rawurlencode($client_sms_body);

					

					$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $client_sms_body);

					

					$ch = curl_init('https://api.textlocal.in/send/');

					curl_setopt($ch, CURLOPT_POST, true);

					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$result = curl_exec($ch);

					curl_close($ch);

				}

			}

			

			/* Send SMS To Admin */

			if(get_option('moveto_textlocal_admin_sms_notification_status') == "E"){

				$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');

				if($template[0]->sms_template_status == "e" && get_option('moveto_textlocal_admin_phone_no')!=''){

					if($template[0]->sms_message == ""){

						$message = strip_tags($template[0]->default_message);

					}else{

						$message = strip_tags($template[0]->sms_message);

					}

					$sender_name = get_option('moveto_email_sender_name');

					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');

					

					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

					

					$admin_sms_body = str_replace($search,$replace_with,$message);



					$textlocal_numbers = get_option('moveto_textlocal_ccode').get_option('moveto_textlocal_admin_phone_no');

					$textlocal_sender = urlencode($textlocal_sender);

					$admin_sms_body = rawurlencode($admin_sms_body);

					

					$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $admin_sms_body);

					

					$ch = curl_init('https://api.textlocal.in/send/');

					curl_setopt($ch, CURLOPT_POST, true);

					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$result = curl_exec($ch);

					curl_close($ch);

				}

			}

		}

	}

	/******************* Send Sms code END *********************/

}

												/** Code Start For New MoveTo BI***/


/*Create Session FOr Step Two Cart*/


if($_POST['action'] =='cart_insert_data'){

		if(isset($_POST['count'])){
		if($_POST['count'] == 0 && count((array)$_SESSION['move_cart']) != 0)
		{
			for ($i = 0; $i < (count((array)$_SESSION['move_cart'])); $i++)
			{
				if(trim($_SESSION['move_cart'][$i]['get_article_room_id']) == trim($_POST['get_article_room_id']) && trim($_SESSION['move_cart'][$i]['get_article_id']) == trim($_POST['get_article_id']) && trim($_SESSION['move_cart'][$i]['get_article_cat_id']) == trim($_POST['get_article_cat_id']))
				{
					if($_SESSION['move_cart'][$i]['get_article_id'].$_SESSION['move_cart'][$i]['get_article_room_id'].$_SESSION['move_cart'][$i]['get_article_cat_id'] == $_POST['get_article_id'].$_POST['get_article_room_id'].$_POST['get_article_cat_id'])
					{
						$remove_article_count = $_SESSION['move_cart'][$i]['count'];
					}
					$_SESSION['move_cart'][$i] = "";
					unset($_SESSION['total_amt']);
				}
			}
		}	elseif(count((array)$_SESSION['move_cart'])==0){
			$cart_details=array();
			$_SESSION['move_cart'] = array();
			$cart_details['get_article_room_id']=$_POST['get_article_room_id'];
			$cart_details['get_article_cat_id']=$_POST['get_article_cat_id'];
			$cart_details['get_article_id']=$_POST['get_article_id'];
			$cart_details['get_article_name']=$_POST['get_article_name'];
			$cart_details['count']=$_POST['count'];
			$cart_details['per_quibcfeets_volume']=$_POST['get_article_cubic'];
			/*$cart_details['get_article_price']=$_POST['get_article_price'];
			$cart_details['get_article_caltype']=$_POST['get_article_caltype'];*/
		/*	if($_POST['get_article_caltype'] == 'P'){
				$cart_details['tot_article_price']= ((($cart_details['get_article_price'] * $_SESSION['distance_price'])/ 100) * $cart_details['count']);
			}else{
				$cart_details['tot_article_price']= ($cart_details['get_article_price'] * $cart_details['count']);
			}*/
			

			array_push($_SESSION['move_cart'],$cart_details);
		} else {
			$check_cart_exist = true;
			$cart_array = $_SESSION['move_cart'];
 			for ($i = 0; $i < (count((array)$_SESSION['move_cart'])); $i++){
 				if($_SESSION['move_cart'][$i]){
				if(trim($_SESSION['move_cart'][$i]['get_article_room_id']) == trim($_POST['get_article_room_id']) && trim($_SESSION['move_cart'][$i]['get_article_id']) == trim($_POST['get_article_id']) && trim($_SESSION['move_cart'][$i]['get_article_cat_id']) == trim($_POST['get_article_cat_id'])){
					$_SESSION['move_cart'][$i]['count'] = $_POST['count'];
					/*if($_POST['get_article_caltype'] == 'P'){
					$_SESSION['move_cart'][$i]['tot_article_price'] = ((($_POST['get_article_price'] * $_SESSION['distance_price'])/ 100) * $_POST['count']);
					}else{
					$_SESSION['move_cart'][$i]['tot_article_price'] = ($_POST['get_article_price']* $_POST['count']);
					}*/
					$check_cart_exist = false;
				}
			}
			}
			if($check_cart_exist){
				$cart_details=array();
				$cart_details['get_article_room_id']=$_POST['get_article_room_id'];
				$cart_details['get_article_cat_id']=$_POST['get_article_cat_id'];
				$cart_details['get_article_id']=$_POST['get_article_id'];
				$cart_details['get_article_name']=$_POST['get_article_name'];
				$cart_details['count']=$_POST['count'];
				$cart_details['per_quibcfeets_volume']=$_POST['get_article_cubic'];

				
				/*$cart_details['get_article_price']=$_POST['get_article_price'];
				$cart_details['get_article_caltype']=$_POST['get_article_caltype'];
				if($_POST['get_article_caltype'] == 'P'){
				$cart_details['tot_article_price']= ((($cart_details['get_article_price'] * $_SESSION['distance_price'])/ 100) * $_POST['count']);
			}else{
				$cart_details['tot_article_price']= ($cart_details['get_article_price']* $cart_details['count']);
			}*/
				array_push($_SESSION['move_cart'],$cart_details);
			}
		}
	}else{
		for ($i = 0; $i < (count((array)$_SESSION['move_cart'])); $i++){
 				if($_SESSION['move_cart'][$i]){
				if(isset($_SESSION['move_cart'][$i]['get_article_room_id']) && isset($_SESSION['move_cart'][$i]['get_article_id']) && isset($_SESSION['move_cart'][$i]['get_article_cat_id'])){
					/*if($_SESSION['mp_cart'][$i]['get_article_caltype'] == 'P'){
					$_SESSION['mp_cart'][$i]['tot_article_price'] = ((($_SESSION['mp_cart'][$i]['get_article_price'] * $_SESSION['distance_price'])/ 100) * $_SESSION['mp_cart'][$i]['count']);
					}else{
					$_SESSION['mp_cart'][$i]['tot_article_price'] = ($_SESSION['mp_cart'][$i]['get_article_price']* $_SESSION['mp_cart'][$i]['count']);
					}*/
					
				}
			}
			}
	}
		/*$total_price = 0;
		if(count($_SESSION['move_cart'])!= 0){
			foreach ($_SESSION['move_cart'] as $value) {
				$total_price += (float)$value["tot_article_price"];
			}
			
		}*/
		/* Bhupendra */
		$distance_unit = get_option('moveto_distance_unit');
		$mover_min_rate = get_option('moveto_min_rate_service');
	 	$title = $_POST['cubicfeet_title'];
		$count = $_POST['cubicfeet_count'];
		$moreid = $_POST['moreid'];

		if(isset($_POST['cubic_count']))
		{
			if($_POST['cubic_count'] == 1)
			{
				if($_POST['cubicfeet_title'] && $_POST['cubicfeet_count'] != '')
				{
					$cubicarr['cubicfeet_title'] = $_POST['cubicfeet_title'];
					$cubicarr['cubicfeet_count'] = $_POST['cubicfeet_count'];
					$cubicarr['moreid'] = $_POST['moreid'];

					if(empty($_SESSION['cubicfeets']))
					{
						$_SESSION['cubicfeets'] = array($cubicarr);
					}
					else
					{
						if(!empty($_SESSION['cubicfeets']))
						{
							$match = 0;
							for ($i = 0; $i <= (count((array)$_SESSION['cubicfeets'])); $i++){
								if($_SESSION['cubicfeets'][$i]['cubicfeet_title'] == $_POST['cubicfeet_title'] && $_SESSION['cubicfeets'][$i]['moreid'] == $_POST['moreid'])
								{
									$match = 1;
								}
							}
						}
						if($match != 1)
						{
							array_push($_SESSION['cubicfeets'],$cubicarr);
						}
						else
						{
							/*for ($i = 0; $i <= (count($_SESSION['cubicfeets'])); $i++){
								if($_SESSION['cubicfeets'][$i]['cubicfeet_title'] == $_POST['cubicfeet_title'] && $_SESSION['cubicfeets'][$i]['moreid'] == $_POST['moreid'] && $_SESSION['cubicfeets'][$i]['cubicfeet_count'] != $_POST['cubicfeet_count'])
								{
							    	unset($_SESSION['cubicfeets'][$i]);
							    	array_push($_SESSION['cubicfeets'],$cubicarr);
								}
							}*/
							return false;
						}
					}
					
					$cubic_rate = $count;
				}

				if($_POST['get_article_cubic'] != '')
				{
					$cubic_rate = $_POST['get_article_cubic'];
				}
						$_SESSION['per_quibcfeets_volume'] += (float)$cubic_rate;
						//$total_price +=  $_SESSION['per_quibcfeets_rate'];
			}
			else
			{
				if($_POST['removeid'] != '')
				{
					for ($i = 0; $i <= (count((array)$_SESSION['cubicfeets'])); $i++){
						if ( $_SESSION['cubicfeets'][$i]['moreid'] == $_POST['removeid'] ) {
							$data = $front_cubicfeets->per_cubicfeets($_SESSION['cubicfeets'][$i]['cubicfeet_count']);
							$cubicrange_rate = $data[0]->cubicfeets_range_price;
							$cubic_rate = $_SESSION['cubicfeets'][$i]['cubicfeet_count'];
					    	unset($_SESSION['cubicfeets'][$i]);
						}
					}
				}
				else
				{
					if($_POST['get_article_cubic'] != '')
					{
						$cubic_rate = $_POST['get_article_cubic'];
						$data = $front_cubicfeets->per_cubicfeets($cubic_rate);
						$cubicrange_rate = $data[0]->cubicfeets_range_price;
					}
				}
							if($distance_unit == 'km')
						 	{
								$per_quibcfeets_rate = $_SESSION['distances'] * $cubicrange_rate;
						 	}
						 	else
						 	{
								$per_quibcfeets_rate = $_SESSION['distances'] * $cubicrange_rate;
						 	}
						 	if(!empty($remove_article_count))
							{
								//$per_quibcfeets_rate = $per_quibcfeets_rate * $remove_article_count;
								$cubic_rate = $cubic_rate * $remove_article_count;
							}
							/*$_SESSION['per_quibcfeets_rate'] -= (float)$per_quibcfeets_rate;*/
							
							$_SESSION['per_quibcfeets_volume'] -= (float)$cubic_rate;
							/*$total_price +=  $_SESSION['per_quibcfeets_rate'];*/
			}
		}
		 
		if(!empty($_SESSION['per_quibcfeets_volume']))
		{
			$volume = $_SESSION['per_quibcfeets_volume'];
			$total_cubic = $front_cubicfeets->per_cubicfeets($volume);
			$cubic_final_rate = $total_cubic[0]->cubicfeets_range_price;
			if($distance_unit == 'km')
		 	{
				$quibcfeets_rate = $_SESSION['distances'] * $cubic_final_rate;
		 	}
		 	else
		 	{
				$quibcfeets_rate = $_SESSION['distances'] * $cubic_final_rate;
		 	}
		 	$total_price += round($quibcfeets_rate);
		}

	  	
		/* Bhupendra */
		
		

		$si_unit = get_option('moveto_cubic_meter');
		if($si_unit)
		{
			$exp = explode("_",$si_unit);
			$num = $exp[1];
			$si_units = "<b>".$exp[0]."<sup>".$num."</sup></b>";
		}
		/* Bhupendra */
		
		/*if(isset($_POST['insurance_value']) && $_POST['insurance_value'] == '1')
		{
			if($insurance_permission == 1)
			{
				$insurance_final_rate = ($insurance_rate / 100) * $_SESSION['total_amt'];
				$_SESSION['insurance_rate'] = round($insurance_final_rate);
			}
		}
		else
		{
			if(isset($_POST['insurance_value']) && $_POST['insurance_value'] == '0')
			{
				//unset($_SESSION['insurance_rate']);
				$_SESSION['insurance_rate'] = 0;
			}
			else
			{
				if(!empty($_SESSION['insurance_rate']))
				{
					unset($_SESSION['insurance_rate']);
					$_SESSION['insurance_rate'] = 0;
					if($_SESSION['insurance_rate'] == 0)
					{
						echo $_SESSION['insurance_rate']."---";
						$cubic_rate = $_SESSION['per_quibcfeets_volume'];
						$data = $front_cubicfeets->per_cubicfeets($cubic_rate);
						$cubicrange_rate = $data[0]->cubicfeets_range_price;
						$ratesss = $_SESSION['distances'] * $cubicrange_rate;
						$insurance_final_rate = ($insurance_rate / 100) * $ratesss;
						$_SESSION['insurance_rate'] = round($insurance_final_rate);
					}
				}
			}
		}	*/
		

		$curr_symbol = get_option('moveto_currency_symbol');
		$_SESSION['total_amt']= $total_price;
		$ld_floor = $uld_floor = $ld_elev = $uld_elev = $pack = $unpack = $add =  0;
		if($_SESSION['loadinginfo']['loading_floor_val']){
		$ld_floor = (($_SESSION['loadinginfo']['loading_floor_val'] * $_SESSION['total_amt']) / 100);
		$_SESSION['loadinginfo']['loading_floor_amt'] = $ld_floor;
		}	
		if($_SESSION['unloadinginfo']['unloading_floor_val']){
		$uld_floor = (($_SESSION['unloadinginfo']['unloading_floor_val'] * $_SESSION['total_amt']) / 100);
		$_SESSION['unloadinginfo']['unloading_floor_amt'] = $uld_floor;
		}	
		if($_SESSION['loadinginfo']['load_elevator_value']){
		$ld_elev = (($_SESSION['loadinginfo']['load_elevator_value'] * $_SESSION['total_amt']) / 100);
		$_SESSION['loadinginfo']['load_elevator_amt'] = $ld_elev;
		}
		if($_SESSION['unloadinginfo']['unload_elevator_value']){
		$uld_elev = (($_SESSION['unloadinginfo']['unload_elevator_value'] * $_SESSION['total_amt']) / 100);
		$_SESSION['unloadinginfo']['unload_elevator_amt'] = $uld_elev;
		}
		if($_SESSION['loadinginfo']['packaging_value']){
		$pack = (($_SESSION['loadinginfo']['packaging_value'] * $_SESSION['total_amt']) / 100);
		$_SESSION['loadinginfo']['packaging_amt'] = $pack;
		}	
		if($_SESSION['unloadinginfo']['unpackaging_value']){
		$unpack = (($_SESSION['unloadinginfo']['unpackaging_value'] * $_SESSION['total_amt']) / 100);
		$_SESSION['unloadinginfo']['unpackaging_amt'] = $unpack;
		}	
		if($_SESSION['additional_info_price']){
		$add = $_SESSION['additional_info_price'];
		}
		$sub_total = ($_SESSION['distance_price'] + $_SESSION['total_amt'] + $ld_floor + $uld_floor ) - $ld_elev - $uld_elev + $pack + $unpack + $add;
		/*$final_amount_wodt = $_SESSION['total_amt'] + $_SESSION['distance_price'] + $ld_floor + $uld_floor - $ld_elev - $uld_elev;*/
		$_SESSION['sub_total'] = round($sub_total,2);
		/*if($mover_min_rate > $_SESSION['sub_total'])
		{
			$_SESSION['sub_total'] = $mover_min_rate;
		}*/
		$insurance_rate = get_option('moveto_insurance_rate');
		$insurance_permission = get_option('moveto_insurance_permission');
		$_SESSION['insurance_rate']=0;
		if($insurance_permission == 1)
			{
		/*if(isset($_POST['insurance_value']) && $_POST['insurance_value'] == '1')
		{
			$insurance_final_rate = ($insurance_rate / 100) * $_SESSION['total_amt'];
			$_SESSION['insurance_rate'] = round($insurance_final_rate);
			$_SESSION['session_checked'] = "on";

			
		}
		else if(isset($_POST['insurance_value']) && $_POST['insurance_value'] == '0')
		{	
			$_SESSION['insurance_rate'] = 0;
			$_SESSION['session_checked'] = "off";

		}
		else{
			if(trim($_SESSION['session_checked'])  == "on"){
				$insurance_final_rate = ($insurance_rate / 100) * $_SESSION['total_amt'];
				$_SESSION['insurance_rate'] = round($insurance_final_rate);
			}else{
				$_SESSION['insurance_rate']=0;
			}
						
		}*/
			$amt_val = '';
			$_SESSION['insurance_rate']=0;
			if(isset($_POST['amt_val']) )
			{
				$amt_val = $_POST['amt_val'];
				
			}
			
			if(isset($_POST['amt']) && $_POST['amt'] == '')
			{
				$amt_val = $_POST['amt'];
			}
			$_SESSION['selected_insurance'] = $amt_val;
			$insurance_final_rate = ($insurance_rate / 100) * $amt_val;
			$_SESSION['insurance_rate'] = round($insurance_final_rate);
			//echo $_SESSION['insurance_rate'];

		}
		
		
		

		$discount_val = get_option('moveto_discount');
		$discount_amt = 0;
		$discount_type = get_option('moveto_discount_type');
		if($discount_type == 'P'){
			$discount_amt = $sub_total * $discount_val / 100;
			$total_wDiscount = $sub_total - $discount_amt;
		}else{
			$discount_amt = $discount_val;
			$total_wDiscount = $sub_total - $discount_amt;
		}

		$discount_amt = round($discount_amt,2);
		$_SESSION['discount'] = $discount_amt;
			if(get_option('moveto_tax') !=''){
		$tax = get_option('moveto_tax');
		}else{
			$tax = 0;
		}
		$tax_amount = $total_wDiscount * $tax /100;
		$tax_amount = round($tax_amount,2);
		$_SESSION['tax'] = $tax_amount;
		$final_amount_wt = $total_wDiscount + $tax_amount + $_SESSION['insurance_rate']; 
		$_SESSION['final_amt']= round($final_amount_wt,2);
		if($mover_min_rate > $_SESSION['final_amt'])
		{
			$_SESSION['final_amt'] = $mover_min_rate;
		}



		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo $curr_symbol.$ld_floor."-".$curr_symbol.$uld_floor."-".$curr_symbol.$ld_elev."-".$curr_symbol.$uld_elev."-".$curr_symbol.$pack."-".$curr_symbol.$unpack."-".$curr_symbol.$discount_amt."-".$curr_symbol.$tax_amount."-".$curr_symbol.$_SESSION['final_amt']."-".$curr_symbol.$_SESSION['sub_total']."-".$curr_symbol.$_SESSION['per_quibcfeets_rate']."-".$_SESSION['per_quibcfeets_volume']." ".$si_units."-".$curr_symbol.$_SESSION['insurance_rate'];
		}else{
			echo $ld_floor.$curr_symbol."-".$uld_floor.$curr_symbol."-".$ld_elev.$curr_symbol."-".$uld_elev.$curr_symbol."-".$pack.$curr_symbol."-".$unpack.$curr_symbol."-".$discount_amt.$curr_symbol."-".$tax_amount.$curr_symbol."-".$_SESSION['final_amt'].$curr_symbol."-".$_SESSION['sub_total'].$curr_symbol."-".$_SESSION['per_quibcfeets_rate'].$curr_symbol."-".$_SESSION['per_quibcfeets_volume']." ".$si_units."-".$_SESSION['insurance_rate'].$curr_symbol;
		}
}

	
/**Get First Step Data Of Booking Page Store In Session ***/

if(isset($_POST['action']) && $_POST['action']=='first_step_data')
{

	$first_step_data['source_city'] = $_POST['source_city'];
	$first_step_data['destination_city'] = $_POST['destination_city'];
	$first_step_data['via_city'] = $_POST['via_city'];
	$first_step_data['bhk'] = $_POST['bhk'];
	$first_step_data['name'] = $_POST['name'];
	$first_step_data['email'] = $_POST['email'];
	$first_step_data['phone'] = $_POST['phone'];
	$first_step_data['date'] = $_POST['date'];
	$first_step_data['distance'] =$_SESSION['distances'];

	$first_step->booking_date=date_i18n('Y-m-d',strtotime($first_step_data['date']));
    $first_step->source_city=base64_encode($first_step_data['source_city']);
    $first_step->via_city=base64_encode($first_step_data['via_city']);
    $first_step->destination_city=base64_encode($first_step_data['destination_city']);
    $first_step->move_size=$first_step_data['bhk'];
    $user_detail =array();
	$user_detail['name']=$first_step_data['name'];
	$user_detail['email']=$first_step_data['email'];
	$user_detail['phone']=$first_step_data['phone'];
	$userData = serialize($user_detail);
 	$first_step->user_detail= $userData;
 	$first_step->first_step_data= $first_step_data;
 	$bookingInfoArr = array();
 	if($_SESSION['booking_id'] != ""){
 		$mp_booking->bookingId = $_SESSION['booking_id'];
		$bookingInfoArr = $mp_booking->get_booking();
		$bookingInfo = $bookingInfoArr[0];

		$orderID = $bookingInfo->order_id;
		$clientID = $bookingInfo->client_id;
		$first_step->orderID = $orderID;
		$first_step->clientID = $clientID;
		$first_step->update_first_step_data();
		$_SESSION['first_step_data'] = $first_step_data;
 	}else{
 	$_SESSION['additional_info_price'] = 0;
	$mp_booking->get_last_order_id();
	if(isset($mp_booking->last_order_id) && $mp_booking->last_order_id!=''){
	$next_order_id = $mp_booking->last_order_id + 1;	
	}else{
	$next_order_id = 1001;
	}
	$first_step->next_order_id= $next_order_id;
	$first_step->reg_date= date("Y-m-d H:i:s ");
	$_SESSION['first_step_data'] = $first_step_data;

	$result = $first_step->insert_first_step_data(); 
	echo $_SESSION['booking_id'] = $result;
 	}
 	
	//echo $_SESSION['first_step_data']['date'];
}

/**Get Step Two Of Booking Page Data ***/

if(isset($_POST['action']) && $_POST['action']=='get_article_type')
{
	$cartArr = array();
	foreach ($_SESSION['move_cart'] as $value) {
		$temp = $value['get_article_id']."".$value['get_article_cat_id']."".$value['get_article_room_id'];
		array_push($cartArr, $temp);
	}
	
	$room_type->id = $_POST['id'];
	$room_type->name = $_POST['name'];	
	$all_article_cat = $room_type->get_article_type();

	foreach($all_article_cat as $get_all_detail){
	?>
		<div class="tab-pane active" id="">	
			<div class="row">
			<h3 class="tab-tilte"><?php echo  $get_all_detail->name;?></h3>
			<?php 
			$split_article_id =  explode(",",$get_all_detail->article_id);
			
			foreach($split_article_id as $art_id){
					$room_type->id1 = trim($art_id);
					$all_article = $room_type->get_subarticle(); 
					if($all_article[0]->image !== '' && $all_article[0]->image !== '/th_' ){
						$image = "/uploads".$all_article[0]->image;
					}elseif($all_article[0]->predefinedimage !== NULL){
						$image = "/plugins/moveto/assets/images/icons/article-icons/".$all_article[0]->predefinedimage;
					}else{
						$image = "/plugins/moveto/assets/images/default.png";
					}
					if($_SESSION['move_cart']){
						$count = 0;
						$id_triplet = trim($art_id)."".trim($get_all_detail->id)."".trim($_POST['id']);
					 	 if(in_array($id_triplet, $cartArr) ){
					 	 	 foreach ($_SESSION['move_cart'] as $cart) {
					 	 	 if($cart['get_article_id'] == trim($art_id) && $cart['get_article_cat_id'] == trim($get_all_detail->id) && $cart[			'get_article_room_id'] == $_POST['id']){
  									$count = $cart['count']; 
  									}
  								}
					
								//$id_triplet = trim($art_id)."".trim($get_all_detail->id)."".trim($_POST['id']);
								?>
											<div class="mp-xs-6 mp-lg-3 mp-md-4 mp-sm-6 mtb-15">
                        <div class="custom_addon_checkbox">
                        <input class = "get_article get_<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>"  id="article_name<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>" data-room_id="<?php echo $room_type->id; ?>" data-catid = "<?php echo $get_all_detail->id;?>" data-room="<?php echo $room_type->name; ?>" data-id="<?php echo $room_type->id1;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" type="checkbox" name="article" value="" checked>
												<div class="article_img">
                        <label for="article_name<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>"  >
                            <img width="90" src='<?php  echo site_url()."/wp-content".$image;?>' />
                        </label>
                        <div class="text-center article_text"><?php echo  $all_article[0]->article_name;?></div>
												 <div class="number">
                            
                        <span class="common-set-btn minus plus_minus_btn" id="minus<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?> " data-room="<?php echo $room_type->name; ?>" data-room_id="<?php echo $room_type->id; ?>" data-catid="<?php echo $get_all_detail->id; ?>" data-max_qty="<?php echo $all_article[0]->max_qty; ?>" data-id="<?php echo $room_type->id1; ?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  value = "0" >
													<i class="lni-minus np common-pluse-min"></i>
                        </span>
                        <input type="text" class="input_number plus_minus_text set-number num_<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" value="<?php echo $count; ?>" readonly/>

                        <span class="common-set-btn plus plus_minus_btn" id="plus<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?> " data-room="<?php echo $room_type->name; ?>" data-room_id="<?php echo $room_type->id; ?>" data-catid="<?php echo $get_all_detail->id; ?>" data-max_qty="<?php echo $all_article[0]->max_qty; ?>" data-id="<?php echo $room_type->id1; ?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  value = "0">
                         <i class="lni-plus  np common-pluse-min"></i>
                        </span>
                        </div>
												</div>
                       
                        </div>
                      </div>					
							<?php
							}else{
							?>
											<div class="mp-xs-6 mp-lg-3 mp-md-4 mp-sm-6 mtb-15">
                        <div class="custom_addon_checkbox">
                        <input class = "get_article get_<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>"  id="article_name<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>" data-room_id="<?php echo $room_type->id; ?>" data-catid = "<?php echo $get_all_detail->id;?>" data-room="<?php echo $room_type->name; ?>" data-id="<?php echo $room_type->id1;?>" data-name="<?php echo  $all_article[0]->article_name;?>" data-cubic="<?php echo  $all_article[0]->cubicarea;?>" type="checkbox" name="article" value="">
												<div class="article_img">
                        <label  for="article_name<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>"  >
                            <img width="90" src='<?php  echo site_url()."/wp-content".$image;?>' />
                        </label>
                        <div class="text-center article_text"><?php echo  $all_article[0]->article_name;?></div>
												<div class="number">
                        <span class="common-set-btn minus plus_minus_btn" id="minus<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" data-room="<?php echo $room_type->name; ?>" data-room_id="<?php echo $room_type->id; ?>" data-catid="<?php echo $get_all_detail->id; ?>" data-max_qty="<?php echo $all_article[0]->max_qty; ?>" data-id="<?php echo $room_type->id1; ?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  value = "0" >
                       <i class="lni-minus np common-pluse-min"></i>
                        </span>
                        <input type="text" class="input_number plus_minus_text set-number num_<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" value="<?php echo $count; ?>" readonly/>

                        <span class="common-set-btn plus plus_minus_btn" id="plus<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" data-room="<?php echo $room_type->name; ?>" data-room_id="<?php echo $room_type->id; ?>" data-catid="<?php echo $get_all_detail->id; ?>" data-max_qty="<?php echo $all_article[0]->max_qty; ?>" data-id="<?php echo $room_type->id1; ?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  value = "0">
                        <i class="lni-plus  np common-pluse-min"></i>
                        </span>
                        </div>
                        </div>
												
                        </div>
                      </div>
								<?php               
									}
				
					}else{
					?>
										<div class="mp-xs-6 mp-lg-3 mp-md-4 mp-sm-6 mtb-15">
                        <div class="custom_addon_checkbox">
                        <input class = "get_article get_<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>"  id="article_name<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>" data-room_id="<?php echo $room_type->id; ?>" data-catid = "<?php echo $get_all_detail->id;?>" data-room="<?php echo $room_type->name; ?>" data-id="<?php echo $room_type->id1;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" type="checkbox" name="article" value="">
												<div class="article_img">
                        <label for="article_name<?php echo $room_type->id1."".$get_all_detail->id."".$_POST['id'];?>"  >
                            <img width="90" src='<?php  echo site_url()."/wp-content".$image;?>' />
                        </label>
                        <div class="text-center article_text"><?php echo  $all_article[0]->article_name;?></div>
												<div class="number">
                            
                        <span class="common-set-btn minus plus_minus_btn plus_minus_ <?php echo $room_type->id1; ?>" id="minus<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" data-room="<?php echo $room_type->name; ?>" data-room_id="<?php echo $room_type->id; ?>" data-catid="<?php echo $get_all_detail->id; ?>" data-max_qty="<?php echo $all_article[0]->max_qty; ?>" data-id="<?php echo $room_type->id1; ?>" data-cubic="<?php echo  $all_article[0]->cubicarea;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  value = "0" >
												<!-- <i class="fa fa-minus np common-pluse-min"></i> -->
												<i class="lni-minus np common-pluse-min"></i>
                        </span>
                        <input type="text" class="input_number plus_minus_text set-number num_<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" id="input_number_<?php echo $room_type->id1; ?>" data-id="<?php echo $room_type->id1; ?>" value="0" readonly/>

                        <span class="common-set-btn plus plus_minus_btn plus_minus_ <?php echo $room_type->id1; ?>" id="plus<?php echo $_POST['id'].$all_article[0]->id.$get_all_detail->id; ?>" data-room="<?php echo $room_type->name; ?>" data-room_id="<?php echo $room_type->id; ?>" data-catid="<?php echo $get_all_detail->id; ?>" data-max_qty="<?php echo $all_article[0]->max_qty; ?>" data-id="<?php echo $room_type->id1; ?>"  data-cubic="<?php echo  $all_article[0]->cubicarea;?>" data-name="<?php echo  $all_article[0]->article_name;?>"  value = "0">
                        <!--<i class="fa fa-plus np common-pluse-min"></i> -->
												 
												 <i class="lni-plus  np common-pluse-min"></i>
                        </span>
                        </div>
                        </div>
												
                        </div>
                      </div>
			<?php	}
			} 
			?>
			</div>
		</div>									
	<?php 
	}
}

/**Get Total Amount Of Data Value ***/

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data')
{
	//echo "------".$_SESSION['final_amt']."</br></br>";
	
	
	//if(isset($_SESSION['final_amt'])){
		/* $mover_min = get_option('moveto_min_rate_service');
		if($mover_min > $_SESSION['final_amt'])
		{
			$_SESSION['final_amt'] = $mover_min;
		
		} */
		
		if(isset($_SESSION['final_amt'])){
		
		
		
		
		$_SESSION['loadinginfo']['loading_floor_val'] = $_POST['loading_floor_val'];
		$_SESSION['loadinginfo']['loading_floor_amt'] = round((($_POST['loading_floor_val'] * $_SESSION['total_amt']) / 100),2);
		$_SESSION['loadinginfo']['loading_floor_number'] = $_POST['loading_floor_number'];

		$val = after_third_step_calc();
		
		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo get_option('moveto_currency_symbol').$_SESSION['discount']."-".get_option('moveto_currency_symbol').$_SESSION['tax']."-".get_option('moveto_currency_symbol').$val."-".get_option('moveto_currency_symbol').$_SESSION['loadinginfo']['loading_floor_amt']."-".get_option('moveto_currency_symbol').$_SESSION['sub_total'];
			echo $_SESSION['discount'].get_option('moveto_currency_symbol')."-".$_SESSION['tax'].get_option('moveto_currency_symbol')."-".$val.get_option('moveto_currency_symbol')."-".$_SESSION['loadinginfo']['loading_floor_amt'].get_option('moveto_currency_symbol')."-".$_SESSION['sub_total'].get_option('moveto_currency_symbol'); 
		}
		
		?>
	
		<?php
	}
}

/**Get Total Amount Of Data After Check Load Elevator Value ***/

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data_elevators')
{
		$_SESSION['loadinginfo']['load_elevator_value'] = $_POST['load_elevator_value'];
		$_SESSION['loadinginfo']['load_elevator_amt'] = round((($_POST['load_elevator_value'] * $_SESSION['total_amt']) / 100),2);
		$_SESSION['loadinginfo']['load_elevator_checked'] = $_POST['load_elevator_checked'];
		$val = after_third_step_calc();
		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo get_option('moveto_currency_symbol').$_SESSION['discount']."-".get_option('moveto_currency_symbol').$_SESSION['tax']."-".get_option('moveto_currency_symbol').$val."-".get_option('moveto_currency_symbol').$_SESSION['loadinginfo']['load_elevator_amt']."-".get_option('moveto_currency_symbol').$_SESSION['sub_total'];
		}else{
			echo $_SESSION['discount'].get_option('moveto_currency_symbol')."-".$_SESSION['tax'].get_option('moveto_currency_symbol')."-".$val.get_option('moveto_currency_symbol')."-".$_SESSION['loadinginfo']['load_elevator_amt'].get_option('moveto_currency_symbol')."-".$_SESSION['sub_total'].get_option('moveto_currency_symbol'); 
		}
		?>
		
		<?php
	
}

/**Get Total Amount Of Data After Check Unload Elevators Value ***/

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data_unloadelevators')
{
		$_SESSION['unloadinginfo']['unload_elevator_value'] = $_POST['unload_elevator_value'];	
		$_SESSION['unloadinginfo']['unload_elevator_amt'] = round((($_POST['unload_elevator_value'] * $_SESSION['total_amt']) / 100),2);
		$_SESSION['unloadinginfo']['unload_elevator_checked'] = $_POST['unload_elevator_checked'];
		$val = after_third_step_calc();
		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo get_option('moveto_currency_symbol').$_SESSION['discount']."-".get_option('moveto_currency_symbol').$_SESSION['tax']."-".get_option('moveto_currency_symbol').$val."-".get_option('moveto_currency_symbol').$_SESSION['unloadinginfo']['unload_elevator_amt']."-".get_option('moveto_currency_symbol').$_SESSION['sub_total'];
		}else{
			echo $_SESSION['discount'].get_option('moveto_currency_symbol')."-".$_SESSION['tax'].get_option('moveto_currency_symbol')."-".$val.get_option('moveto_currency_symbol')."-".$_SESSION['unloadinginfo']['unload_elevator_amt'].get_option('moveto_currency_symbol')."-".$_SESSION['sub_total'].get_option('moveto_currency_symbol'); 
		}

		?>
		
		<?php
	
}


/**Get Total Amount Of Data After Check Packaging Value ***/

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data_packaging')
{
	
		$_SESSION['loadinginfo']['packaging_value'] = $_POST['packaging_value'];	
		$_SESSION['loadinginfo']['packaging_amt'] = round((($_POST['packaging_value'] * $_SESSION['total_amt']) / 100),2);
		$_SESSION['loadinginfo']['packaging_checked'] = $_POST['packaging_checked'];
		$val = after_third_step_calc();
		
		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo get_option('moveto_currency_symbol').$_SESSION['discount']."-".get_option('moveto_currency_symbol').$_SESSION['tax']."-".get_option('moveto_currency_symbol').$val."-".get_option('moveto_currency_symbol').$_SESSION['loadinginfo']['packaging_amt']."-".get_option('moveto_currency_symbol').$_SESSION['sub_total'];
		}else{
			echo $_SESSION['discount'].get_option('moveto_currency_symbol')."-".$_SESSION['tax'].get_option('moveto_currency_symbol')."-".$val.get_option('moveto_currency_symbol')."-".$_SESSION['loadinginfo']['packaging_amt'].get_option('moveto_currency_symbol')."-".$_SESSION['sub_total'].get_option('moveto_currency_symbol'); 
		}
		?>
		
		<?php
	
}

/**Get Total Amount Of Data After Check Unload Elevators Value ***/

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data_unpackaging')
{
			
		$_SESSION['unloadinginfo']['unpackaging_value'] = $_POST['unpackaging_value'];	
		$_SESSION['unloadinginfo']['unpackaging_amt'] = round((($_POST['unpackaging_value'] * $_SESSION['total_amt']) / 100),2);
		$_SESSION['unloadinginfo']['unpackaging_checked'] = $_POST['unpackaging_checked'];
		$val = after_third_step_calc();
		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo get_option('moveto_currency_symbol').$_SESSION['discount']."-".get_option('moveto_currency_symbol').$_SESSION['tax']."-".get_option('moveto_currency_symbol').$val."-".get_option('moveto_currency_symbol').$_SESSION['unloadinginfo']['unpackaging_amt']."-".get_option('moveto_currency_symbol').$_SESSION['sub_total'];
		}else{
			echo $_SESSION['discount'].get_option('moveto_currency_symbol')."-".$_SESSION['tax'].get_option('moveto_currency_symbol')."-".$val.get_option('moveto_currency_symbol')."-".$_SESSION['unloadinginfo']['unpackaging_amt'].get_option('moveto_currency_symbol')."-".$_SESSION['sub_total'].get_option('moveto_currency_symbol'); 
		}
		?>
		
		<?php
	
}

/**Get Total Amount Of Data After Check Unload Floor Value ***/

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data_unload')
{
	
		$_SESSION['unloadinginfo']['unloading_floor_val'] = $_POST['unloading_floor_val'];	
		$_SESSION['unloadinginfo']['unloading_floor_amt'] = round((($_POST['unloading_floor_val'] * $_SESSION['total_amt']) / 100),2);
		$_SESSION['unloadinginfo']['unloading_floor_number'] = $_POST['unloading_floor_number'];
		$val = after_third_step_calc();
		if(get_option('moveto_currency_symbol_position') == 'B'){
			echo get_option('moveto_currency_symbol').$_SESSION['discount']."-".get_option('moveto_currency_symbol').$_SESSION['tax']."-".get_option('moveto_currency_symbol').$val."-".get_option('moveto_currency_symbol').$_SESSION['unloadinginfo']['unloading_floor_amt']."-".get_option('moveto_currency_symbol').$_SESSION['sub_total'];
		}else{
			echo $_SESSION['discount'].get_option('moveto_currency_symbol')."-".$_SESSION['tax'].get_option('moveto_currency_symbol')."-".$val.get_option('moveto_currency_symbol')."-".$_SESSION['unloadinginfo']['unloading_floor_amt'].get_option('moveto_currency_symbol')."-".$_SESSION['sub_total'].get_option('moveto_currency_symbol');
		}
		?>
		
		<?php
	
}

if(isset($_POST['action']) && $_POST['action']=='third_step_click_data_additional')
{	
	
	if($_POST['additional_info_process'] == "INC"){
		if($_POST['caltype'] == 'P'){
			$_SESSION['additional_info_price']	= round(($_SESSION['additional_info_price'] + (($_POST['additional_info_price'] * $_SESSION['total_amt']) / 100)),2);
		}else{
			$_SESSION['additional_info_price']	= round(($_SESSION['additional_info_price'] + $_POST['additional_info_price']),2);
		}
	}else{
		if($_POST['caltype'] == 'P'){
		$_SESSION['additional_info_price']	= round(($_SESSION['additional_info_price'] - (($_POST['additional_info_price'] * $_SESSION['total_amt']) / 100)),2);
		}else{
			$_SESSION['additional_info_price']	= round(($_SESSION['additional_info_price'] - $_POST['additional_info_price']),2);
		}
	}
		
		$val = after_third_step_calc();
		echo get_option('moveto_currency_symbol').$val;
		
	
}

function after_third_step_calc(){
	
	 
		
	 if(get_option('moveto_tax') !=''){
		$tax = get_option('moveto_tax');
		}else{
			$tax = 0;
		}
		$cur_sym = get_option('moveto_currency_symbol');
	$ld_floor = $uld_floor = $ld_elev = $uld_elev = $pack = $unpack = $add =  0;
	if(isset($_SESSION['loadinginfo']['loading_floor_amt'])){
		$ld_floor = $_SESSION['loadinginfo']['loading_floor_amt'];

	}
	if(isset($_SESSION['unloadinginfo']['unloading_floor_amt'])){
		$uld_floor = $_SESSION['unloadinginfo']['unloading_floor_amt'];

	}
	if(isset($_SESSION['loadinginfo']['load_elevator_amt'])){
		$ld_elev = $_SESSION['loadinginfo']['load_elevator_amt'];

	}
	if(isset($_SESSION['unloadinginfo']['unload_elevator_amt'])){
		$uld_elev = $_SESSION['unloadinginfo']['unload_elevator_amt'];

	}
	if(isset($_SESSION['loadinginfo']['packaging_amt'] )){
		$pack = $_SESSION['loadinginfo']['packaging_amt'] ;

	}
	if(isset($_SESSION['unloadinginfo']['unpackaging_amt'] )){
		$unpack = $_SESSION['unloadinginfo']['unpackaging_amt'] ;

	}
	if(isset($_SESSION['additional_info_price'])){
		$add = $_SESSION['additional_info_price'];

	}
	
	

	
	
	
	$discount_val = get_option('moveto_discount');
	$discount_amt = 0;
	//$_SESSION['total_amt']= $total_price;
	$discount_type = get_option('moveto_discount_type');
	$sub_total = ($_SESSION['distance_price'] + $_SESSION['total_amt']  + $ld_floor + $uld_floor ) - $ld_elev - $uld_elev + $pack + $unpack + $add;
	$_SESSION['sub_total'] = round($sub_total,2);
	
	
	/* $insurance_rate = get_option('moveto_insurance_rate');
		$insurance_permission = get_option('moveto_insurance_permission');
		$_SESSION['insurance_rate']=0;
		if($insurance_permission == 1)
		{ */
		/*if(isset($_POST['insurance_value']) && $_POST['insurance_value'] == '1')
		{
			$insurance_final_rate = ($insurance_rate / 100) * $_SESSION['total_amt'];
			$_SESSION['insurance_rate'] = round($insurance_final_rate);
			$_SESSION['session_checked'] = "on";

			
		}
		else if(isset($_POST['insurance_value']) && $_POST['insurance_value'] == '0')
		{	
			$_SESSION['insurance_rate'] = 0;
			$_SESSION['session_checked'] = "off";

		}
		else{
			if(trim($_SESSION['session_checked'])  == "on"){
				$insurance_final_rate = ($insurance_rate / 100) * $_SESSION['total_amt'];
				$_SESSION['insurance_rate'] = round($insurance_final_rate);
			}else{
				$_SESSION['insurance_rate']=0;
			}
						
		}*/
			/* $amt_val = '';
			$_SESSION['insurance_rate']=0;
			if(isset($_POST['amt_val']) && $_POST['amt_val'] != '')
			{
				$amt_val = $_POST['amt_val'];
			}
			if(isset($_POST['amt']) && $_POST['amt'] != '')
			{
				$amt_val = $_POST['amt'];
			}
			$insurance_final_rate = ($insurance_rate / 100) * $amt_val;
			$_SESSION['insurance_rate'] = round($insurance_final_rate);

		} */
		
		
		

	
	if($discount_type == 'P'){
		$discount_amt = $sub_total * $discount_val / 100;
		$total_wot = $sub_total - $discount_amt;
	}else{
		$discount_amt = $discount_val;
		$total_wot = $sub_total - $discount_amt;
	}
	$discount_amt = round($discount_amt,2);
	$_SESSION['discount'] = $discount_amt;
	$tax_amt =	$total_wot * $tax / 100;
	$tax_amt = round($tax_amt,2);
	$_SESSION['tax'] = $tax_amt;
	//echo $_SESSION['insurance_rate']; 
	$total_wt = $total_wot + $tax_amt  + $_SESSION['insurance_rate']; 
	$_SESSION['final_amt'] = round($total_wt,2);
	$mover_min = get_option('moveto_min_rate_service');
	//return round($total_wt,2);
		
		if($mover_min > $_SESSION['final_amt'])
		{
			$_SESSION['final_amt'] = $mover_min;
		 
		} 
  return $_SESSION['final_amt'];
}
/**Get Step Three Booking Page Data ***/

if(isset($_POST['action']) && $_POST['action']=='get_step_three_data')
{
	
	$get_floors_data = $obj_distance->readAll_floor(); 
	$get_elevators_data = $obj_distance->readAll_elevator();
	$get_addinfo_data = $obj_distance->readAll_additional_info(); 
	
	foreach($get_floors_data as $get_floors){
	?>
		<label for="lf-g" class="radio_boxed_wrapper">
			<input type="radio" id="floor<?php echo $get_floors->floor_no; ?>" class="" />
			<div class="custom_radio_box"><?php echo $get_floors->floor_no; ?></div>
		</label>									
	<?php 
	}
	foreach($get_elevators_data as $get_elevators){
	?>
		<div class="toggle-button-wrapp r" id="toggle_btn">
			<input type="checkbox" class="checkbox">
			<div class="knobs"></div>
			<div class="layer"></div>
		</div>								
	<?php 
	}
}

/* Booking Insert Data Code*/

if(isset($_POST['action']) && $_POST['action']=='booking_data_insert')
{
	$mp_booking->bookingId = $_SESSION['booking_id'];
	$bookingArr = $mp_booking->get_booking();
	$bookingData = $bookingArr[0];
	$client_name = "Move to Payment";
	if($bookingData!=''){
		$client_name = $bookingData->client_name;
	}
	
	$transaction_id = "";
	$payment_method = $_POST["payment_method"];
	if($payment_method == "stripe_payment"){
		require_once($base."/assets/stripe/stripe.php");
		try {
			$secret_key = get_option("moveto_stripe_secretKey");
			\Stripe\Stripe::setApiKey($secret_key);
			$objcharge = new \Stripe\Charge;
			$currency = get_option("moveto_currency");
			$token_id = sanitize_text_field($_POST["st_token"]);
			$stripe_amt = $_SESSION['final_amt'];
			
			$striperesponse = $objcharge::Create(array(
				"amount" => round($stripe_amt*100),
				"currency" => $currency,
				"source" => $token_id,
				"description" => $client_name
			));
			$transaction_id = $striperesponse->id;
		}
		catch (Exception $e) {
			$error = $e->getMessage();				
			echo "Message Is - ".sanitize_text_field($error);
			die();
		}
	}
	if($_POST['payment_method'] == 'paypal_payment'){
			$transaction_id = $_POST["paypal_transaction_id"];
	}
	if($_POST['payment_method'] == 'paytm_payment'){
			$transaction_id = $_POST["paytm_transaction_id"];
	}
	if($_POST['payment_method'] == 'paystack_payment'){
			$transaction_id = $_POST["paytm_transaction_id"];
	}
		
		$loading_info = array();
		$unloading_info = array();
		
	  $loading_info['loading_floor_amt'] = $_SESSION['loadinginfo']['loading_floor_amt'];
		$loading_info['loading_floor_number'] = $_SESSION['loadinginfo']['loading_floor_number'];
		$loading_info['load_elevator_amt'] = $_SESSION['loadinginfo']['load_elevator_amt'];
		$loading_info['load_elevator_checked'] = $_SESSION['loadinginfo']['load_elevator_checked'];
		$loading_info['packaging_amt'] = $_SESSION['loadinginfo']['packaging_amt'];
		$loading_info['packaging_checked'] = $_SESSION['loadinginfo']['packaging_checked'];
		$loading_info['loading_address_one'] = $_POST['loading_info']['loading_address_one'];
		$loading_info['loading_address_two'] = $_POST['loading_info']['loading_address_two'];
		$loading_info['loading_address_three'] = $_POST['loading_info']['loading_address_three'];
		
		$unloading_info['unloading_floor_amt'] = $_SESSION['unloadinginfo']['unloading_floor_amt'];
		$unloading_info['unloading_floor_number'] = $_SESSION['unloadinginfo']['unloading_floor_number'];
		$unloading_info['unload_elevator_amt'] = $_SESSION['unloadinginfo']['unload_elevator_amt'];
		$unloading_info['unload_elevator_checked'] = $_SESSION['unloadinginfo']['unload_elevator_checked'];
		$unloading_info['unpackaging_amt'] = $_SESSION['unloadinginfo']['unpackaging_amt'];
		$unloading_info['unpackaging_checked'] = $_SESSION['unloadinginfo']['unpackaging_checked'];
		$unloading_info['unloading_address_one'] = $_POST['unloading_info']['unloading_address_one'];
		$unloading_info['unloading_address_two'] = $_POST['unloading_info']['unloading_address_two'];
		$unloading_info['unloading_address_three'] = $_POST['unloading_info']['unloading_address_three'];
		
	

		$mp_booking->get_last_order_id();

		if(isset($mp_booking->last_order_id) && $mp_booking->last_order_id!=''){
			 $next_order_id = $mp_booking->last_order_id + 1;	
		}else{
			 $next_order_id = 1001;
		}

		
		
		$move_size = $_SESSION['first_step_data']['bhk'];

		$service_details = array();
		$service_details['article_details'] = $_SESSION['move_cart'];
		
		
		$service_info = $service_details['article_details'];
		$service_cubicfeets = $_SESSION['cubicfeets'];
		$cubic_feets_volume = $_SESSION['per_quibcfeets_volume'];

		$quote_price = $_SESSION['final_amt'];

		$insurance_rate = $_SESSION['insurance_rate'];

		
		$mp_booking->loading_data=serialize($loading_info);
		$mp_booking->unloading_data=serialize($unloading_info);
		$mp_booking->additional_info=serialize($_POST['additional_info']);
		
		$mp_booking->quote_price=$quote_price;
		
   	    $mp_booking->service_info_cubic = serialize($service_info_cubic);
   	    $mp_booking->service_info = serialize($service_info);
   	    $mp_booking->booking_id = $_SESSION['booking_id'];
				$mp_booking->payment_method = $payment_method;
				$mp_booking->transaction_id = $transaction_id;
				$mp_booking->insurance_rate = $insurance_rate;
				$mp_booking->service_cubicfeets = serialize($service_cubicfeets);
				$mp_booking->cubic_feets_volume = $cubic_feets_volume;

		$add_booking = $mp_booking->add_bookings();
		if($add_booking)
		{
				/* Get Booking Client Info */
				
 			$mp_booking->bookingId = $_SESSION['booking_id'];
 			$bookingArr = $mp_booking->get_booking();
			$bookingData = $bookingArr[0];
			$user_info_detail = '';
			$client_email = '';
			$client_phone = '';
			$client_name = '';
			$order_id = '';
			$source_city = '';
			$destination_city = '';
			$booking_date ='';
			$bookingstatus = '';
			$service_info = '';
			$loading_info = '';
			$unloading_info = '';
			$service_info = '';
			$additional_info = '';
			$moveto_currency_symbol = $currency_symbol;
			$quote_price = '';
			$quote_detail = '';
			$service_cubicfeets = '';
			$insurance_rate = '';
			$cubic_feets_volume = '';
			$selected_insurance = '';
			
			if($bookingData!=''){										
				
					$client_email = $bookingData->client_email;
					$client_phone = $bookingData->client_phone;
					$client_name = $bookingData->client_name;
					$order_id = $bookingData->order_id;
					$source_city = $bookingData->source_city;
					$destination_city = $bookingData->destination_city;
					$booking_date = $bookingData->booking_date;
					$booking_status = $bookingData->booking_status;
					$service_info = $bookingData->service_info;
					$loading_info = $bookingData->loading_info;
					$unloading_info = $bookingData->unloading_info;
					$service_info = $bookingData->service_info;
					$additional_info = $bookingData->additional_info;
					$moveto_currency_symbol = $currency_symbol;
					$quote_price = $bookingData->quote_price;
					$quote_detail = $bookingData->quote_detail;
					$insurance_rate = $bookingData->insurance_rate;
					$service_cubicfeets = $bookingData->cubic_feets_article;
					$service_cubicfeets_volume = $bookingData->cubic_feets_volume;
					$selected_insurance = $_SESSION['selected_insurance'];
			

					$user_info_detail = '';

					if($client_name!=''){										
					$user_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("User Details","mp").'</div>';
					$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Name',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$client_name.'</p></div>';

					$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Email',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$client_email.'</p></div>';

					$user_info_detail .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Phone',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$client_phone.'</p></div>';
							
					}							

					/* Get Loading Info */

					$loading_info_detail = '';

					if($loading_info!=''){										

						$loading_info_arr = unserialize($loading_info);	
						
							$loading_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Loading Details","mp").'</div>';

							$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Floor No',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_floor_number"].'</p></div>';

							$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_one"].'</p></div>';

							$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_two"].'</p></div>';

							$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_three"].'</p></div>';

							$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Elevator Available?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["load_elevator_checked"].'</p></div>';

							$loading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Packaging Required?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["packaging_checked"].'</p></div>';

					}

					/* Get UNLOADING Info */

					$unloading_info_detail = '';

					if($unloading_info!=''){										

						$unloading_info_arr = unserialize($unloading_info);				

							$unloading_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Unloading Details","mp").'</div>';

							$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Floor No',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_floor_number"].'</p></div>';

							$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_one"].'</p></div>';

							$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_two"].'</p></div>';

							$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_three"].'</p></div>';

							$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Elevator Available?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unload_elevator_checked"].'</p></div>';

							$unloading_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unpackaging Required?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unpackaging_checked"].'</p></div>';

					}

					/* Get Service Info */
					$service_info_data = '';

					if($service_info!=''){										
							$service_info_data .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Article Details","mp").'</div>';
						$service_info_arr = unserialize($service_info);				
						foreach($service_info_arr as $value){
						
							$service_info_data .= '<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$value['get_article_name'].'</strong></div>';

							$service_info_data .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Qty',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['count'].'</p></div>';

							$service_info_data .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Cubic Volume',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['per_quibcfeets_volume'].'</p></div>';
						}
					}


					/* Cubci Feets Rate */

					$service_cubic_feets = '';
					if($service_cubicfeets!=''){										
							$service_cubic_feets .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Custom Article","mp").'</div>';
							$service_cubicfeets = unserialize($service_cubicfeets);				
							foreach($service_cubicfeets as $value){	
								$service_cubic_feets .= '<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$value['cubicfeet_title'].'</strong></div>';

								$service_cubic_feets .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Qty',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">1</p></div>';

								$service_cubic_feets .='<div style="width:400px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Cubic Volume',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['cubicfeet_count'].'</p></div>';
							}
					}
					
					$service_info_detail = $service_info_data."</br>".$service_cubic_feets;
					
					/* Insurance Rate */
					
					$service_insurance_rate = '';
					$selected_insurance_rate = '';
					if($insurance_rate!=''){										
							$service_insurance_rate .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Insurance Rate","mp").'</div>';
							$service_insurance_rate .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$insurance_rate.'</strong></div>';
							$selected_insurance_rate .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Selected Insurance","mp").'</div>';
							$selected_insurance_rate .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Selected Insurance',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$selected_insurance.'</p></div>';
							
					}

					/* Total Cubic Volume */
					
					$service_cubic_total_volume = '';
					if($service_cubicfeets_volume!=''){										
							$service_cubic_total_volume .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Total Cubic Volume","mp").'</div>';
							$service_cubic_total_volume .= '<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$service_cubicfeets_volume.'</strong></div>';
					}

					$service_info_detail = $service_info_data."</br>".$service_cubic_feets;
					/* Get Additional Info */

					$additional_info_detail = '';

					if($additional_info!=''){
						$additional_info_arr = unserialize($additional_info);
						if(sizeof((array)$additional_info_arr['favorite'])>0){
							
							$additional_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Additional Information","mp").'</div>';
							
							$returndata = $moveto_additional_info->readAll();
							$additionalArr = array();
							foreach ($additional_info_arr['favorite'] as $value) {
								array_push($additionalArr, $value);
							}
							foreach($returndata as $additional){
								if(in_array($additional->id, $additionalArr)){
									$additional_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$additional->additional_info.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">Yes</p></div>';
								}else{
									$additional_info_detail .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$additional->additional_info.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">No</p></div>';
								}
							}
						}	
					}
						
						/*Start Of Quotetion Pricing Condition with email and Sms of Client and Amdin */
						
						$move_pricing_status = get_option('move_pricing_status');
						
						if($move_pricing_status == 'Y'){
							
							$search = array('{{client_name}}','{{admin_name}}','{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}','{{order_id}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_price}}','{{quote_detail}}','{{admin_manager_name}}','{{customer_name}}','{{insurance_rate}}','{{total_cubic_volume}}');      
				
							$replace_with = array($client_name,$admin_name,$company_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo,$order_id,base64_decode($source_city),base64_decode($destination_city),date_i18n(get_option('date_format'),strtotime($booking_date)),$bookingstatus,$service_info_detail,$loading_info_detail,$unloading_info_detail,$additional_info_detail,$user_info_detail,$moveto_currency_symbol.$quote_price,$quote_detail,$admin_name,$customer_name,$service_insurance_rate,$service_cubic_total_volume); 
						
						
								/* Send email to Client when lead is complete */	
								if(get_option('moveto_client_email_notification_status')=='E' && $client_email!=''){
						
									$mp_clientemail_templates = new moveto_email_template();
									$msg_template = $mp_clientemail_templates->email_parent_template;	
									$mp_clientemail_templates->email_template_name = "AC";
									$template_detail = $mp_clientemail_templates->readOne();        
									if($template_detail[0]->email_message!=''){            
										$email_content = $template_detail[0]->email_message;        
									}else{            
										$email_content = $template_detail[0]->default_message;        
									}        
									$email_subject = $template_detail[0]->email_subject;
									$email_client_message = '';
									
									/* Sending email to client when New quote request Sent */ 		  
									if($template_detail[0]->email_template_status=='e'){
									
										$email_client_message = str_replace($search,$replace_with,$email_content);	
										$email_client_message = str_replace('###msg_content###',$email_client_message,$msg_template);		
										add_filter( 'wp_mail_content_type', 'set_content_type' );	
										
										$status = wp_mail($client_email,$email_subject,$email_client_message,$headers);    
										
									}       
								}
						
								/* Send email to Admin when lead is complete */
								if(get_option('moveto_admin_email_notification_status')=='E'){
									$mp_adminemail_templates = new moveto_email_template();
									$msg_template = $mp_adminemail_templates->email_parent_template;
									$mp_adminemail_templates->email_template_name = "AA";  		
									$template_detail = $mp_adminemail_templates->readOne();        
									if($template_detail[0]->email_message!=''){            
										$email_content = $template_detail[0]->email_message;        
									}else{            
										$email_content = $template_detail[0]->default_message;
									}        
									$email_subject = $template_detail[0]->email_subject;
									$email_admin_message = '';				
									$email_admin_message = str_replace($search,$replace_with,$email_content);	
									$email_admin_message = str_replace('###msg_content###',$email_admin_message,$msg_template);
									add_filter( 'wp_mail_content_type', 'set_content_type' );		
									
									$status = wp_mail(get_option('moveto_email_sender_address'),$email_subject,$email_admin_message,$headers);
													
								}
								
								/******************* Send Sms code START *********************/
								if(get_option("moveto_sms_reminder_status") == "E"){
									/** TWILIO **/
									if(get_option("moveto_sms_noti_twilio") == "E"){
										$obj_sms_template = new moveto_sms_template();
										$twillio_sender_number = get_option('moveto_twilio_number');
										$AccountSid = get_option('moveto_twilio_sid');
										$AuthToken =  get_option('moveto_twilio_auth_token'); 
										
										/* Send SMS To Client */
										if(get_option('moveto_twilio_client_sms_notification_status') == "E"){
											$twilliosms_client = new Client($AccountSid, $AuthToken);
											$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
											
											if($template1[0]->sms_template_status == "e" && $client_phone!=''){
												if($template1[0]->sms_message == ""){
													$message = strip_tags($template1[0]->default_message);
												}else{
													$message = strip_tags($template1[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												$client_sms_body = str_replace($search,$replace_with,$message);
												$twilliosms_client->messages->create(
													$client_phone,
													array(
														'from' => $twillio_sender_number,
														'body' => $client_sms_body 
													)
												);
											}
										}
												
										/* Send SMS To Admin */
										if(get_option('moveto_twilio_admin_sms_notification_status') == "E"){		   
											$twilliosms_admin = new Client($AccountSid, $AuthToken);
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_twilio_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);	

												$twilliosms_staff->messages->create(
													get_option('moveto_twilio_ccode').get_option('moveto_twilio_admin_phone_no'),
													array(
														'from' => $twillio_sender_number,
														'body' => $admin_sms_body 
													)
												);
											}
										}
									}
													
									/** PLIVO **/
									if(get_option("moveto_sms_noti_plivo") == "E"){
										$obj_sms_template = new moveto_sms_template();
										$plivo_sender_number = get_option('moveto_plivo_number');	
										$auth_sid = get_option('moveto_plivo_sid');
										$auth_token = get_option('moveto_plivo_auth_token');
										
										/* Send SMS To Client */
										if(get_option('moveto_plivo_client_sms_notification_status') == "E"){
											$p_client = new Plivo\RestAPI($auth_sid, $auth_token, '', '');
											$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
											if($template1[0]->sms_template_status == "e" && $client_phone!=''){
												if($template1[0]->sms_message == ""){
													$message = strip_tags($template1[0]->default_message);
												}else{
													$message = strip_tags($template1[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$client_sms_body = str_replace($search,$replace_with,$message);
												
												$clientparams = array(
													'src' => $plivo_sender_number,
													'dst' => $client_phone,
													'text' => $client_sms_body,
													'method' => 'POST'
												);
												$response = $p_client->send_message($clientparams);
											}
										}
										/* Send SMS To Admin */
										if(get_option('moveto_plivo_admin_sms_notification_status') == "E"){		   
											$twilliosms_admin = new Client($AccountSid, $AuthToken);
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_plivo_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);	

												$adminparams = array(
													'src' => $plivo_sender_number,
													'dst' => get_option('moveto_plivo_ccode').get_option('moveto_plivo_admin_phone_no'),
													'text' => $admin_sms_body,
													'method' => 'POST'
													);
												$response = $p_admin->send_message($adminparams);
											}
										}
									}
											
									/** NEXMO **/
									if(get_option("moveto_sms_noti_nexmo") == "E"){
										$obj_sms_template = new moveto_sms_template();
										include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_nexmo.php');
										$nexmo_client = new moveto_nexmo();
										$nexmo_client->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');
										$nexmo_client->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');
										$nexmo_client->moveto_nexmo_form = get_option('moveto_nexmo_form');
										
										/* Send SMS To Client */
										if(get_option('moveto_nexmo_send_sms_client_status') == "E"){
											$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
											if($template1[0]->sms_template_status == "e" && $client_phone!=''){
												if($template1[0]->sms_message == ""){
													$message = strip_tags($template1[0]->default_message);
												}else{
													$message = strip_tags($template1[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$client_sms_body = str_replace($search,$replace_with,$message);
												$res = $nexmo_client->send_nexmo_sms($client_phone,$client_sms_body);
											}
										}

										/* Send SMS To Admin */
										if(get_option('moveto_nexmo_send_sms_admin_status') == "E"){
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_nexmo_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);
												$nexmo_client->send_nexmo_sms(get_option('moveto_nexmo_ccode').get_option('moveto_nexmo_admin_phone_no'),$admin_sms_body);
											}
										}
									}
											
									/** TEXTLOCAL **/
									if(get_option("moveto_sms_noti_textlocal") == "E"){
										$obj_sms_template = new moveto_sms_template();
										$textlocal_apikey = get_option('moveto_textlocal_apikey');
										$textlocal_sender = get_option('moveto_textlocal_sender');
										
										/* Send SMS To Client */
										
										if(get_option('moveto_textlocal_client_sms_notification_status') == "E"){
											$template = $obj_sms_template->gettemplate_sms("C",'e','AC');
											if($template[0]->sms_template_status == "e" && $client_phone!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$client_sms_body = str_replace($search,$replace_with,$message);

												$textlocal_numbers = $client_phone;
												$textlocal_sender =$textlocal_sender;
												$client_sms_body = $client_sms_body;
												
												$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $client_sms_body);
												
												$ch = curl_init('https://api.textlocal.in/send/');
												curl_setopt($ch, CURLOPT_POST, true);
												curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
												curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
												$result = curl_exec($ch);
												curl_close($ch);
											}
										}
										
										/* Send SMS To Admin */
										if(get_option('moveto_textlocal_admin_sms_notification_status') == "E"){
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_textlocal_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);

												$textlocal_numbers = get_option('moveto_textlocal_ccode').get_option('moveto_textlocal_admin_phone_no');
												$textlocal_sender = $textlocal_sender;
												$admin_sms_body = $admin_sms_body;
												
												$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $admin_sms_body);
												
												$ch = curl_init('https://api.textlocal.in/send/');
												curl_setopt($ch, CURLOPT_POST, true);
												curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
												curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
												$result = curl_exec($ch);
												curl_close($ch);
											}
										}
									}
								}
						}else{
							
							$search = array('{{client_name}}','{{admin_name}}','{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}','{{order_id}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_detail}}','{{admin_manager_name}}','{{insurance_rate}}','{{customer_name}}','{{total_cubic_volume}}');   
						
							$replace_with = array($client_name,$admin_name,$company_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo,$order_id,base64_decode($source_city),base64_decode($destination_city),date_i18n(get_option('date_format'),strtotime($booking_date)),$bookingstatus,$service_info_detail,$loading_info_detail,$unloading_info_detail,$additional_info_detail,$user_info_detail,$quote_detail,$admin_name,$selected_insurance_rate,$customer_name,$service_cubic_total_volume); 
						
							/* Send email to Client when lead is complete */	
							if(get_option('moveto_client_email_notification_status')=='E' && $client_email!=''){
					
								$mp_clientemail_templates = new moveto_email_template();
								$msg_template = $mp_clientemail_templates->email_parent_template;	
								$mp_clientemail_templates->email_template_name = "ACW";
								$template_detail = $mp_clientemail_templates->readOne();        
								if($template_detail[0]->email_message!=''){            
									$email_content = $template_detail[0]->email_message;        
								}else{            
									$email_content = $template_detail[0]->default_message;        
								}        
								$email_subject = $template_detail[0]->email_subject;
								$email_client_message = '';
								
								/* Sending email to client when New quote request Sent */ 		  
								if($template_detail[0]->email_template_status=='e'){
								
									$email_client_message = str_replace($search,$replace_with,$email_content);	
									$email_client_message = str_replace('###msg_content###',$email_client_message,$msg_template);		
									add_filter( 'wp_mail_content_type', 'set_content_type' );		
								
									$status = wp_mail($client_email,$email_subject,$email_client_message,$headers);    
									
								}       
							}
						
							/* Send email to Admin when lead is complete */
							if(get_option('moveto_admin_email_notification_status')=='E'){
								$mp_adminemail_templates = new moveto_email_template();
								$msg_template = $mp_adminemail_templates->email_parent_template;
								$mp_adminemail_templates->email_template_name = "AA";  		
								$template_detail = $mp_adminemail_templates->readOne();        
								if($template_detail[0]->email_message!=''){            
									$email_content = $template_detail[0]->email_message;        
								}else{            
									$email_content = $template_detail[0]->default_message;
								}        
								$email_subject = $template_detail[0]->email_subject;
								$email_admin_message = '';				
								$email_admin_message = str_replace($search,$replace_with,$email_content);	
								$email_admin_message = str_replace('###msg_content###',$email_admin_message,$msg_template);
								add_filter( 'wp_mail_content_type', 'set_content_type' );		
								
								$status = wp_mail(get_option('moveto_email_sender_address'),$email_subject,$email_admin_message,$headers);
												
							}
							
								/******************* Send Sms code START *********************/
								if(get_option("moveto_sms_reminder_status") == "E"){
									/** TWILIO **/
									if(get_option("moveto_sms_noti_twilio") == "E"){
										$obj_sms_template = new moveto_sms_template();
										$twillio_sender_number = get_option('moveto_twilio_number');
										$AccountSid = get_option('moveto_twilio_sid');
										$AuthToken =  get_option('moveto_twilio_auth_token'); 
										
										/* Send SMS To Client */
										if(get_option('moveto_twilio_client_sms_notification_status') == "E"){
											$twilliosms_client = new Client($AccountSid, $AuthToken);
											$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
											
											if($template1[0]->sms_template_status == "e" && $client_phone!=''){
												if($template1[0]->sms_message == ""){
													$message = strip_tags($template1[0]->default_message);
												}else{
													$message = strip_tags($template1[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												$client_sms_body = str_replace($search,$replace_with,$message);
												$twilliosms_client->messages->create(
													$client_phone,
													array(
														'from' => $twillio_sender_number,
														'body' => $client_sms_body 
													)
												);
											}
										}
												
										/* Send SMS To Admin */
										if(get_option('moveto_twilio_admin_sms_notification_status') == "E"){		   
											$twilliosms_admin = new Client($AccountSid, $AuthToken);
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_twilio_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);	

												$twilliosms_staff->messages->create(
													get_option('moveto_twilio_ccode').get_option('moveto_twilio_admin_phone_no'),
													array(
														'from' => $twillio_sender_number,
														'body' => $admin_sms_body 
													)
												);
											}
										}
									}
													
									/** PLIVO **/
									if(get_option("moveto_sms_noti_plivo") == "E"){
										$obj_sms_template = new moveto_sms_template();
										$plivo_sender_number = get_option('moveto_plivo_number');	
										$auth_sid = get_option('moveto_plivo_sid');
										$auth_token = get_option('moveto_plivo_auth_token');
										
										/* Send SMS To Client */
										if(get_option('moveto_plivo_client_sms_notification_status') == "E"){
											$p_client = new Plivo\RestAPI($auth_sid, $auth_token, '', '');
											$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
											if($template1[0]->sms_template_status == "e" && $client_phone!=''){
												if($template1[0]->sms_message == ""){
													$message = strip_tags($template1[0]->default_message);
												}else{
													$message = strip_tags($template1[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$client_sms_body = str_replace($search,$replace_with,$message);
												
												$clientparams = array(
													'src' => $plivo_sender_number,
													'dst' => $client_phone,
													'text' => $client_sms_body,
													'method' => 'POST'
												);
												$response = $p_client->send_message($clientparams);
											}
										}
										/* Send SMS To Admin */
										if(get_option('moveto_plivo_admin_sms_notification_status') == "E"){		   
											$twilliosms_admin = new Client($AccountSid, $AuthToken);
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_plivo_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);	

												$adminparams = array(
													'src' => $plivo_sender_number,
													'dst' => get_option('moveto_plivo_ccode').get_option('moveto_plivo_admin_phone_no'),
													'text' => $admin_sms_body,
													'method' => 'POST'
													);
												$response = $p_admin->send_message($adminparams);
											}
										}
									}
											
									/** NEXMO **/
									if(get_option("moveto_sms_noti_nexmo") == "E"){
										$obj_sms_template = new moveto_sms_template();
										include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_nexmo.php');
										$nexmo_client = new moveto_nexmo();
										$nexmo_client->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');
										$nexmo_client->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');
										$nexmo_client->moveto_nexmo_form = get_option('moveto_nexmo_form');
										
										/* Send SMS To Client */
										if(get_option('moveto_nexmo_send_sms_client_status') == "E"){
											$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
											if($template1[0]->sms_template_status == "e" && $client_phone!=''){
												if($template1[0]->sms_message == ""){
													$message = strip_tags($template1[0]->default_message);
												}else{
													$message = strip_tags($template1[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$client_sms_body = str_replace($search,$replace_with,$message);
												$res = $nexmo_client->send_nexmo_sms($client_phone,$client_sms_body);
											}
										}

										/* Send SMS To Admin */
										if(get_option('moveto_nexmo_send_sms_admin_status') == "E"){
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_nexmo_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);
												$nexmo_client->send_nexmo_sms(get_option('moveto_nexmo_ccode').get_option('moveto_nexmo_admin_phone_no'),$admin_sms_body);
											}
										}
									}
											
									/** TEXTLOCAL **/
									if(get_option("moveto_sms_noti_textlocal") == "E"){
										$obj_sms_template = new moveto_sms_template();
										$textlocal_apikey = get_option('moveto_textlocal_apikey');
										$textlocal_sender = get_option('moveto_textlocal_sender');
										
										/* Send SMS To Client */
										
										if(get_option('moveto_textlocal_client_sms_notification_status') == "E"){
											$template = $obj_sms_template->gettemplate_sms("C",'e','AC');
											if($template[0]->sms_template_status == "e" && $client_phone!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$client_sms_body = str_replace($search,$replace_with,$message);

												$textlocal_numbers = $client_phone;
												$textlocal_sender =$textlocal_sender;
												$client_sms_body = $client_sms_body;
												
												$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $client_sms_body);
												
												$ch = curl_init('https://api.textlocal.in/send/');
												curl_setopt($ch, CURLOPT_POST, true);
												curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
												curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
												$result = curl_exec($ch);
												curl_close($ch);
											}
										}
										
										/* Send SMS To Admin */
										if(get_option('moveto_textlocal_admin_sms_notification_status') == "E"){
											$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
											if($template[0]->sms_template_status == "e" && get_option('moveto_textlocal_admin_phone_no')!=''){
												if($template[0]->sms_message == ""){
													$message = strip_tags($template[0]->default_message);
												}else{
													$message = strip_tags($template[0]->sms_message);
												}
												$sender_name = get_option('moveto_email_sender_name');
												
												$admin_sms_body = str_replace($search,$replace_with,$message);

												echo $textlocal_numbers = get_option('moveto_textlocal_ccode').get_option('moveto_textlocal_admin_phone_no');
												$textlocal_sender = $textlocal_sender;
												$admin_sms_body = $admin_sms_body;
												
												$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $admin_sms_body);
												
												$ch = curl_init('https://api.textlocal.in/send/');
												curl_setopt($ch, CURLOPT_POST, true);
												curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
												curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
												$result = curl_exec($ch);
												curl_close($ch);
											}
										}
									}
								}
						}
						/*End Of Quotetion Pricing Condition with email and Sms of Client and Amdin */
			}
				echo "Booking done";
		}
}
?>