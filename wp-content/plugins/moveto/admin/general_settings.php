<?php
if ( ! defined( 'ABSPATH' ) ) exit;

// set page headers
$page_title = "Settings";
include_once "header.php";

$moveto_google_api_key = get_option('moveto_google_api_key');
if($moveto_google_api_key != ""){ 
?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $moveto_google_api_key; ?>"></script> 
<?php
}
// instantiate schedule object
$mp_settings = new moveto_settings();
$mp_email_templates = new moveto_email_template();
$mp_sms_templates = new moveto_sms_template();
$mp_settings->readAll();

$mp_floor = new moveto_distance();
$mp_floor->readAll_floor();

$plugin_relative_path = plugin_dir_path(dirname(dirname(dirname(dirname(__FILE__)))));
$plugin_url_for_ajax = plugins_url('',dirname(__FILE__));
$callbackurl_paystack = $plugin_url_for_ajax.'/assets/lib/paystack_callback.php';
$error = '';	
$img_error ='';

$upload_dir_path= wp_upload_dir();
$email_template_tags = array('{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{client_name}}','{{company_logo}}','{{order_id}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_price}}','{{quote_detail}}','{{admin_manager_name}}','{{customer_name}}','{{insurance_rate}}','{{total_cubic_volume}}');

$newaccountemail_template_tags = array('{{account_detail}}');    


$sms_template_tags = array('{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}','{{order_id}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_price}}','{{quote_detail}}','{{admin_manager_name}},{{customer_name}}','{{insurance_rate}}','{{total_cubic_volume}}'); 


/* $newaccountsms_template_tags = array('{{account_detail}}'); */

?>	
<div class="panel mp-panel-default" id="mp-settings">
	<div class="mp-settings mp-left-menu col-md-2 col-sm-3 col-xs-12 col-lg-2">
		<ul class="nav nav-tab nav-stacked">				
			<li class="active"><a href="#company-details" class="top-company-details" data-toggle="pill"><i class="fa fa-building-o fa-2x"></i><br /><?php echo __("Company","mp");?></a></li>
			<li><a href="#general-setting" class="top-general-setting" data-toggle="pill"><i class="fa fa-cog fa-2x"></i><br /><?php echo __("General","mp");?></a></li>
			<li><a href="#appearance-setting" class="top-appearance-setting" data-toggle="pill"><i class="fa fa-tachometer fa-2x"></i><br /><?php echo __("Appearance ","mp");?></a></li>
			<li><a href="#payment-setting" class="top-payment-setting" data-toggle="pill"><i class="fa fa-money fa-2x"></i><br /><?php echo __("Payment ","mp");?></a></li>
			<li><a href="#email-setting" class="top-email-setting" data-toggle="pill"><i class="fa fa-paper-plane fa-2x"></i><br /><?php echo __("Email Notification","mp");?></a></li>
			<li><a href="#email-template" class="top-email-template" data-toggle="pill"><i class="fa fa-envelope fa-2x"></i><br /><?php echo __("Email Templates","mp");?></a></li>
			<li><a href="#sms-reminder" class="top-sms-reminder" data-toggle="pill"><i class="fa fa-mobile fa-2x"></i><br /><?php echo __("SMS Notification","mp");?></a></li>
			<li><a href="#sms-template" class="top-sms-template" data-toggle="pill"><i class="fa fa-envelope fa-2x"></i><br /><?php echo __("SMS Templates","mp");?></a></li>
			<li><a href="#google-api" class="top-sms-template" data-toggle="pill"><i class="fa fa-key fa-2x"></i><br /><?php echo __("Google Map","mp");?></a></li>
			<!--<li><a href="#custom-form-fields" class="top-custom-formfield" data-toggle="pill"><i class="fa fa-align-left fa-2x"></i><br /><?php echo __("Custom Form Fields","mp");?></a></li>-->
		</ul>
	</div>
	<div class="mp-setting-details tab-content col-md-10 col-sm-9 col-lg-10 col-xs-12 np container-fluid">
		<div class="tab-content pr">
			<div class="company-details tab-pane active mp-toggle-abs" id="company-details">
				<form id="" method="post" type="" class="mp-company-details" >
					<div class="panel panel-default">
						<div class="panel-heading mp-top-right">
							<h1 class="panel-title"><?php echo __("Company Settings","mp");?> </h1>
						</div>
						<div class="panel-body">
							<table class="form-inline mp-common-table">
								<tbody>
									<tr>
										<td><label><?php echo __("Company Name","mp");?></label></td>
										<td>
											<div class="form-group">
												<input type="text" class="form-control" size="35" id="moveto_company_name" value="<?php echo $mp_settings->moveto_company_name; ?>" placeholder="<?php echo __("Your Company Name","mp");?>" />
											</div>	
											<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Company name is used for invoice purpose.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
										</td>
									</tr>
									<tr>
										<td><label><?php echo __("Company Email","mp");?></label></td>
										<td>
											<div class="form-group">
												<input type="text" class="form-control" size="35" id="moveto_company_email" value="<?php echo $mp_settings->moveto_company_email; ?>" placeholder="<?php echo __("Your Company email","mp");?>" />
											</div>	
										</td>
									</tr>
									<!--country code-->
									<tr>
										<td><label><?php echo __("Default Country Code","mp");?></label></td>
										<td>
											<div class="form-group">
												<input style="width: 30.5% !important;" type="text" class="form-control custom-flag-space" size="35" id="moveto_company_country_code" value="<?php echo $mp_settings->moveto_company_country_code; ?>"
												placeholder="<?php echo __("","mp");?>" />
											</div>	
										</td>
									</tr>
									
									<tr>
										<td><label><?php echo __("Company Phone","mp");?></label></td>
										<td>
											<div class="input-group">
												<span class="input-group-addon" style="width: 43px;height: 30px;"><span class="company_country_code_value"><?php echo $mp_settings->moveto_company_country_code; ?></span></span>
												<input type="hidden" class="default_company_country_flag" value="" />
												<input style="width: 75%;" type="text" class="form-control" size="35" id="moveto_company_phone" value="<?php echo $mp_settings->moveto_company_phone; ?>" placeholder="<?php echo __("Company Phone","mp");?>" />
											</div>	
										</td>
									</tr>
									
									<!-- country code -->
									
									<tr>
										<td><label><?php echo __("Company Address","mp");?></label></td>
									
										<td><div class="form-group">
											<div class="mp-col12"><textarea id="moveto_company_address" class="form-control" cols="44"><?php echo $mp_settings->moveto_company_address; ?></textarea></div>
											</div>
										</td>
									</tr>
									<tr>
										<td></td>
										<td><div class="form-group">
											<div class="mp-col6 mp-w-50">
												<input type="text" class="form-control" id="moveto_company_city" value="<?php echo $mp_settings->moveto_company_city; ?>" placeholder="<?php echo __("City","mp");?>" />
											</div>
											<div class="mp-col6 mp-w-50 float-right">
												<input type="text" class="form-control" id="moveto_company_state" value="<?php echo $mp_settings->moveto_company_state; ?>" placeholder="<?php echo __("State","mp");?>" />
											</div>
											</div>
										</td>
									</tr>
									<tr>
										<td></td>	
										<td><div class="form-group">	
											<div class="mp-col6 mp-w-50">
												<input type="text" class="form-control" id="moveto_company_zip" value="<?php echo $mp_settings->moveto_company_zip; ?>" placeholder="<?php echo __("Zip","mp");?>" />
											</div>	
											<div class="mp-col6 mp-w-50 float-right">
												<input type="text" class="form-control" id="moveto_company_country" value="<?php echo $mp_settings->moveto_company_country; ?>" placeholder="<?php echo __("Country","mp");?>" />
											</div>	
											</div>	
											
										</td>
									</tr>
									
									<tr>
										<td><label><?php echo __("Company Logo","mp");?></label></td>
										<td>
											<div class="form-group">
												<div class="mp-company-image-uploader">
													<img id="bdcslocimage" src="<?php if($mp_settings->moveto_company_logo==''){ echo $plugin_url_for_ajax.'/assets/images/company.png';}else{echo site_url()."/wp-content/uploads".$mp_settings->moveto_company_logo;	}?>" class="mp-company-image br-4" height="100" width="100">
													<label <?php if($mp_settings->moveto_company_logo==''){ echo "style='display:block'"; }else{ echo "style='display:none;'"; }?> for="mp-upload-imagebdcs" class="mp-company-img-icon-label">
														<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
														<i class="pull-left fa fa-plus-circle fa-2x  custom-imageplus-icon"></i>
													</label>
													<input data-us="bdcs" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdcs"  />
													
													<a id="mp-remove-company-imagebdcs" <?php if($mp_settings->moveto_company_logo!=''){ echo "style='display:block;'";}  ?> class="hide-div pull-left br-4 btn-danger mp-remove-company-img btn-xs mp_remove_image" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove company Image","mp");?>"></i></a>												
													<div style="display: none;" class="mp-popover" id="popover-mp-remove-company-imagebdcs">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<a href="javascript:void(0)" value="Delete" data-mediapath="<?php echo $mp_settings->moveto_company_logo;?>" data-imgfieldid="bdcsuploadedimg"
																		class="btn btn-danger btn-sm mp_delete_companyimage"><?php echo __("Yes","mp");?></a>
																		<a href="javascript:void(0)" id="popover-mp-remove-company-imagebdcs" class="btn btn-default btn-sm close_delete_popup" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div id="mp-image-upload-popupbdcs" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
										<div class="vertical-alignment-helper">
											<div class="modal-dialog modal-md vertical-align-center">
												<div class="modal-content" style="width:607px">
													<div class="modal-header">
														<div class="col-md-12 col-xs-12">
															<a data-us="bdcs" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdcs"><?php echo __("Crop & Save","mp");?></a>
															<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
														</div>	
													</div>
													<div class="modal-body">
														<img id="mp-preview-imgbdcs" />
													</div>
													<div class="modal-footer">
														<div class="col-md-12 np">
															<div class="col-md-4 col-xs-12">
																<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" style="width:100%;" class="form-control" id="bdcsfilesize" name="filesize" />
															</div>	
															<div class="col-md-4 col-xs-12">	
																<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" style="width:100%;" class="form-control" id="bdcsh" name="h" /> 
															</div>
															<div class="col-md-4 col-xs-12">	
																<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" style="width:100%;" class="form-control" id="bdcsw" name="w" />
															</div>
															<input type="hidden" id="bdcsx1" name="x1" />
															 <input type="hidden" id="bdcsy1" name="y1" />
															<input type="hidden" id="bdcsx2" name="x2" />
															<input type="hidden" id="bdcsy2" name="y2" />
															<input id="bdcsbdimagetype" type="hidden" name="bdimagetype"/>
															<input type="hidden" id="bdcsbdimagename" name="bdimagename" value="" />
														</div>
													</div>							
												</div>		
											</div>			
										</div>			
									</div>
									<input name="companyimage" id="bdcsuploadedimg" type="hidden" value="<?php echo $mp_settings->moveto_company_logo;?>" />



											
											<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Company logo is used for invoice purpose.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
										</td>
									</tr>
								</tbody>
								
								<tfoot>
									<tr>
										<td></td>
										<td>
											<a href="javascript:void(0)" id="mp_save_company_settings" name="" class="btn btn-success" type="submit"><?php echo __("Save Setting","mp");?></a>
											<button type="reset" class="btn btn-default ml-30"><?php echo __("Default Setting","mp");?></button>
								
										</td>
									</tr>
								</tfoot>
							</table>	
						</div>
					</div>
				</form>
			</div>
			<!-- file upload preview -->
				<div class="mp-company-logo-popup-view">
					<div id="mp-image-upload-popup" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
						<div class="vertical-alignment-helper">
							<div class="modal-dialog modal-md vertical-align-center">
								<div class="modal-content">
									<div class="modal-header">
										<div class="col-md-12 col-xs-12">
											<button type="submit" class="btn btn-success"><?php echo __("Crop & Save","mp");?></button>
											<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
										</div>	
									</div>
									<div class="modal-body">
										<img id="mp-preview-img" />
									</div>
									<div class="modal-footer">
										<div class="col-md-12 np">
											<div class="col-md-4 col-xs-12">
												<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="filesize" name="filesize" />
											</div>	
											<div class="col-md-4 col-xs-12">	
												<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="h" name="h" /> 
											</div>
											<div class="col-md-4 col-xs-12">	
												<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="w" name="w" />
											</div>
										</div>

									</div>							
								</div>		
							</div>			
						</div>			
					</div>
				</div>
							
			<div class="tab-pane mp-toggle-abs" id="general-setting">
				<form id="" method="post" type="" class="mp-general-setting" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("General Settings","mp");?></h1>
						</div>
						<div class="panel-body">
							<table class="form-inline mp-common-table" >
								<tbody>	   <tr>
										<td><label><?php echo __("Currency","mp");?></label></td>
										<td>
											<div class="form-group">
												<select id="moveto_currency" class="selectpicker form-control" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true"  >
													<option value=""><?php echo __("-- Select Currency --","mp");?></option>
												  <option value="ALL" <?php if($mp_settings->moveto_currency =='ALL' ){ echo ' selected '; }?>>Lek <?php echo "Albania Lek";?></option>
												  <option value="AED" <?php if($mp_settings->moveto_currency =='AED' ){ echo ' selected '; }?>>د.إ <?php echo "UAE Dirham";?></option>
												  <option value="AFN" <?php if($mp_settings->moveto_currency =='AFN' ){ echo ' selected '; }?>>؋ <?php echo "Afghanistan Afghani";?></option>
												  <option value="ARS" <?php if($mp_settings->moveto_currency =='ARS' ){ echo ' selected '; }?>>$ <?php echo "Argentina Peso";?></option>
												  <option value="ANG" <?php if($mp_settings->moveto_currency =='ANG' ){ echo ' selected '; }?>>NAƒ <?php echo "Neth Antilles Guilder";?></option>  
												  <option value="AWG" <?php if($mp_settings->moveto_currency =='AWG' ){ echo ' selected '; }?>>ƒ <?php echo "Aruba Guilder";?></option>
												  <option value="AUD" <?php if($mp_settings->moveto_currency =='AUD' ){ echo ' selected '; }?>>$ <?php echo "Australia Dollar";?></option>
												  <option value="AZN" <?php if($mp_settings->moveto_currency =='AZN' ){ echo ' selected '; }?>>ман <?php echo "Azerbaijan Manat";?></option>
												  <option value="BSD" <?php if($mp_settings->moveto_currency =='BSD' ){ echo ' selected '; }?>>$ <?php echo "Bahamas Dollar";?></option>
												  <option value="BBD" <?php if($mp_settings->moveto_currency =='BBD' ){ echo ' selected '; }?>>$ <?php echo "Barbados Dollar";?></option>
												  <option value="BYR" <?php if($mp_settings->moveto_currency =='BYR' ){ echo ' selected '; }?>>p <?php echo "Belarus Ruble";?></option>
												  <option value="BZD" <?php if($mp_settings->moveto_currency =='BZD' ){ echo ' selected '; }?>>BZ$ <?php echo "Belize Dollar";?></option>
												  <option value="BMD" <?php if($mp_settings->moveto_currency =='BMD' ){ echo ' selected '; }?>>$ <?php echo "Bermuda Dollar";?></option>					  
												  <option value="BOB" <?php if($mp_settings->moveto_currency =='BOB' ){ echo ' selected '; }?>>$b <?php echo "Bolivia	Boliviano";?></option>
												  <option value="BAM" <?php if($mp_settings->moveto_currency =='BAM' ){ echo ' selected '; }?>>KM <?php echo "Bosnia and Herzegovina Convertible Marka";?></option>
												  <option value="BWP" <?php if($mp_settings->moveto_currency =='BWP' ){ echo ' selected '; }?>>P <?php echo "Botswana Pula";?></option>
												  <option value="BGN" <?php if($mp_settings->moveto_currency =='BGN' ){ echo ' selected '; }?>>лв <?php echo "Bulgaria Lev";?></option>
												  <option value="BRL" <?php if($mp_settings->moveto_currency =='BRL' ){ echo ' selected '; }?>>R$ <?php echo "Brazil Real";?></option>
												  <option value="BND" <?php if($mp_settings->moveto_currency =='BND' ){ echo ' selected '; }?>>$ <?php echo "Brunei Darussalam Dollar";?></option>
												  
												  <option value="BDT" <?php if($mp_settings->moveto_currency =='BDT' ){ echo ' selected '; }?>>Tk <?php echo "Bangladesh Taka";?></option>
												  <option value="BIF" <?php if($mp_settings->moveto_currency =='BIF' ){ echo ' selected '; }?>>FBu <?php echo "Burundi Franc";?></option>
												  
												  <option value="CHF" <?php if($mp_settings->moveto_currency =='CHF' ){ echo ' selected '; }?>>CHF<?php echo "Swiss Franc";?></option>
												  
												  
												  <option value="KHR" <?php if($mp_settings->moveto_currency =='KHR' ){ echo ' selected '; }?>>៛  <?php echo "Cambodia Riel";?></option>
												  <option value="KMF" <?php if($mp_settings->moveto_currency =='KMF' ){ echo ' selected '; }?>>KMF <?php echo "Comoros Franc";?></option>
												  
												  <option value="CAD" <?php if($mp_settings->moveto_currency =='CAD' ){ echo ' selected '; }?>>$ <?php echo "Canada Dollar";?></option>
												  <option value="KYD" <?php if($mp_settings->moveto_currency =='KYD' ){ echo ' selected '; }?>>$ <?php echo "Cayman Dollar";?></option>
												  
												  <option value="CLP" <?php if($mp_settings->moveto_currency =='CLP' ){ echo ' selected '; }?>>$ <?php echo "Chile Peso";?></option>
												  <option value="CYN" <?php if($mp_settings->moveto_currency =='CYN' ){ echo ' selected '; }?>>¥ <?php echo "China Yuan Renminbi";?></option>
												  
												  <option value="CVE" <?php if($mp_settings->moveto_currency =='CVE' ){ echo ' selected '; }?>>Esc <?php echo "Cape Verde Escudo";?></option>
												  
												  <option value="COP" <?php if($mp_settings->moveto_currency =='COP' ){ echo ' selected '; }?>>$ <?php echo "Colombia Peso";?></option>
												  <option value="CRC" <?php if($mp_settings->moveto_currency =='CRC' ){ echo ' selected '; }?>>₡ <?php echo "Costa Rica Colon";?></option>
												  <option value="HRK" <?php if($mp_settings->moveto_currency =='HRK' ){ echo ' selected '; }?>>kn <?php echo "Croatia	Kuna";?></option>
												  <option value="CUP" <?php if($mp_settings->moveto_currency =='CUP' ){ echo ' selected '; }?>>₱ <?php echo "Cuba Peso";?></option>
												  <option value="CZK" <?php if($mp_settings->moveto_currency =='CZK' ){ echo ' selected '; }?>>Kč <?php echo "Czech Republic Koruna";?></option>
												 <option value="DKK" <?php if($mp_settings->moveto_currency =='DKK' ){ echo ' selected '; }?>>kr <?php echo "Denmark	Krone";?></option>
												 <option value="DOP" <?php if($mp_settings->moveto_currency =='DOP' ){ echo ' selected '; }?>>RD$ <?php echo "Dominican Republic Peso";?></option>
												 <option value="DJF" <?php if($mp_settings->moveto_currency =='DJF' ){ echo ' selected '; }?>>Fdj <?php echo "Djibouti Franc";?></option>
												 <option value="DZD" <?php if($mp_settings->moveto_currency =='DZD' ){ echo ' selected '; }?>>دج <?php echo "Algerian Dinar";?></option>
												 <option value="XCD" <?php if($mp_settings->moveto_currency =='XCD' ){ echo ' selected '; }?>>$  <?php echo "East Caribbean Dollar";?></option>
												 <option value="EGP" <?php if($mp_settings->moveto_currency =='EGP' ){ echo ' selected '; }?>>£ <?php echo "Egypt Pound";?></option>
												 <option value="ETB" <?php if($mp_settings->moveto_currency =='ETB' ){ echo ' selected '; }?>>Br <?php echo "Ethiopian Birr";?></option>
												 <option value="SVC" <?php if($mp_settings->moveto_currency =='SVC' ){ echo ' selected '; }?>>$  <?php echo "El Salvador Colon";?></option>
												 <option value="EEK" <?php if($mp_settings->moveto_currency =='EEK' ){ echo ' selected '; }?>>kr <?php echo "Estonia Kroon";?></option>
												 <option value="EUR" <?php if($mp_settings->moveto_currency =='EUR' ){ echo ' selected '; }?>>€  <?php echo "Euro Member Euro";?></option>
												 <option value="FKP" <?php if($mp_settings->moveto_currency =='FKP' ){ echo ' selected '; }?>>£ <?php echo "Falkland Islands Pound";?></option>
												 <option value="FJD" <?php if($mp_settings->moveto_currency =='FJD' ){ echo ' selected '; }?>>$  <?php echo "Fiji Dollar";?></option>
												 <option value="GHC" <?php if($mp_settings->moveto_currency =='GHC' ){ echo ' selected '; }?>>¢ <?php echo "Ghana Cedis";?></option>
												 <option value="GIP" <?php if($mp_settings->moveto_currency =='GIP' ){ echo ' selected '; }?>>£ <?php echo "Gibraltar Pound";?></option>
												 <option value="GMD" <?php if($mp_settings->moveto_currency =='GMD' ){ echo ' selected '; }?>>D <?php echo "Gambian Dalasi";?></option>
												 <option value="GNF" <?php if($mp_settings->moveto_currency =='GNF' ){ echo ' selected '; }?>>FG <?php echo "Guinea Franc";?></option>
												 <option value="GTQ" <?php if($mp_settings->moveto_currency =='GTQ' ){ echo ' selected '; }?>>Q <?php echo "Guatemala Quetzal";?></option>
												 <option value="GGP" <?php if($mp_settings->moveto_currency =='GGP' ){ echo ' selected '; }?>>£ <?php echo "Guernsey Pound";?></option>
												 <option value="GYD" <?php if($mp_settings->moveto_currency =='GYD' ){ echo ' selected '; }?>>$ <?php echo "Guyana Dollar";?></option>
											  <option value="HNL" <?php if($mp_settings->moveto_currency =='HNL' ){ echo ' selected '; }?>>L <?php echo "Honduras Lempira";?></option>
											  <option value="HKD" <?php if($mp_settings->moveto_currency =='HKD' ){ echo ' selected '; }?>>$ <?php echo "Hong Kong Dollar";?></option>
											  
											  <option value="HRK" <?php if($mp_settings->moveto_currency =='HRK' ){ echo ' selected '; }?>>kn <?php echo "Croatian Kuna";?></option>
											  <option value="HTG" <?php if($mp_settings->moveto_currency =='HTG' ){ echo ' selected '; }?>>G <?php echo "Haitian Gourde";?></option>
											  <option value="HUF" <?php if($mp_settings->moveto_currency =='HUF' ){ echo ' selected '; }?>>Ft <?php echo "Hungary	Forint";?></option>
											  <option value="ISK" <?php if($mp_settings->moveto_currency =='ISK' ){ echo ' selected '; }?>>kr <?php echo "Iceland	Krona";?></option>
											  <option value="INR" <?php if($mp_settings->moveto_currency =='INR' ){ echo ' selected '; }?>>Rs <?php echo "India Rupee";?></option>
											  <option value="IDR" <?php if($mp_settings->moveto_currency =='IDR' ){ echo ' selected '; }?>>Rp <?php echo "Indonesia Rupiah";?></option>
											  <option value="IRR" <?php if($mp_settings->moveto_currency =='IRR' ){ echo ' selected '; }?>>﷼ <?php echo "Iran Rial";?></option>
											  <option value="IMP" <?php if($mp_settings->moveto_currency =='IMP' ){ echo ' selected '; }?>>£ <?php echo "Isle of Man Pound";?></option>
											  <option value="ILS" <?php if($mp_settings->moveto_currency =='ILS' ){ echo ' selected '; }?>>₪ <?php echo "Israel Shekel";?></option>
											  <option value="JMD" <?php if($mp_settings->moveto_currency =='JMD' ){ echo ' selected '; }?>>J$ <?php echo "Jamaica Dollar";?></option>
											  <option value="JPY" <?php if($mp_settings->moveto_currency =='JPY' ){ echo ' selected '; }?>>¥ <?php echo "Japan Yen";?></option>
											  <option value="JEP" <?php if($mp_settings->moveto_currency =='JEP' ){ echo ' selected '; }?>>£ <?php echo "Jersey Pound";?></option>
											  <option value="KZT" <?php if($mp_settings->moveto_currency =='KZT' ){ echo ' selected '; }?>>лв <?php echo "Kazakhstan Tenge";?></option>
											  <option value="KPW" <?php if($mp_settings->moveto_currency =='KPW' ){ echo ' selected '; }?>>₩ <?php echo "Korea(North) Won";?></option>
											  <option value="KRW" <?php if($mp_settings->moveto_currency =='KRW' ){ echo ' selected '; }?>>₩ <?php echo "Korea(South) Won";?></option>
											  <option value="KGS" <?php if($mp_settings->moveto_currency =='KGS' ){ echo ' selected '; }?>>лв <?php echo "Kyrgyzstan Som";?></option>
											  <option value="KES" <?php if($mp_settings->moveto_currency =='KES' ){ echo ' selected '; }?>>KSh <?php echo "Kenyan Shilling";?></option>
												<option value="LAK" <?php if($mp_settings->moveto_currency =='LAK' ){ echo ' selected '; }?>>₭ <?php echo "Laos	Kip";?></option>
												<option value="LVL" <?php if($mp_settings->moveto_currency =='LVL' ){ echo ' selected '; }?>>Ls <?php echo "Latvia Lat";?></option>
												<option value="LBP" <?php if($mp_settings->moveto_currency =='LBP' ){ echo ' selected '; }?>>£ <?php echo "Lebanon Pound";?></option>
												<option value="LRD" <?php if($mp_settings->moveto_currency =='LRD' ){ echo ' selected '; }?>>$ <?php echo "Liberia Dollar";?></option>
												<option value="LTL" <?php if($mp_settings->moveto_currency =='LTL' ){ echo ' selected '; }?>>Lt <?php echo "Lithuania Litas";?></option>
												<option value="MKD" <?php if($mp_settings->moveto_currency =='MKD' ){ echo ' selected '; }?>>ден <?php echo "Macedonia Denar";?>	</option>
												<option value="MYR" <?php if($mp_settings->moveto_currency =='MYR' ){ echo ' selected '; }?>>RM <?php echo "Malaysia Ringgit";?></option>
												<option value="MUR" <?php if($mp_settings->moveto_currency =='MUR' ){ echo ' selected '; }?>>₨ <?php echo "Mauritius Rupee";?></option>
												<option value="MXN" <?php if($mp_settings->moveto_currency =='MXN' ){ echo ' selected '; }?>>$ <?php echo "Mexico Peso";?></option>
												<option value="MNT" <?php if($mp_settings->moveto_currency =='MNT' ){ echo ' selected '; }?>>₮ <?php echo "Mongolia Tughrik";?></option>
												<option value="MZN" <?php if($mp_settings->moveto_currency =='MZN' ){ echo ' selected '; }?>>MT <?php echo "Mozambique Metical";?></option>
												<option value="MAD" <?php if($mp_settings->moveto_currency =='MAD' ){ echo ' selected '; }?>>د.م. <?php echo "Moroccan Dirham";?></option>
												<option value="MDL" <?php if($mp_settings->moveto_currency =='MDL' ){ echo ' selected '; }?>>MDL <?php echo "Moldovan Leu";?></option>
												<option value="MOP" <?php if($mp_settings->moveto_currency =='MOP' ){ echo ' selected '; }?>>$ <?php echo "Macau Pataca";?></option>
												<option value="MRO" <?php if($mp_settings->moveto_currency =='MRO' ){ echo ' selected '; }?>>UM <?php echo "Mauritania Ougulya";?></option>
												<option value="MVR" <?php if($mp_settings->moveto_currency =='MVR' ){ echo ' selected '; }?>>Rf <?php echo "Maldives Rufiyaa";?></option>
												<option value="PGK" <?php if($mp_settings->moveto_currency =='PGK' ){ echo ' selected '; }?>>K <?php echo "Papua New Guinea Kina";?></option>
												<option value="NAD" <?php if($mp_settings->moveto_currency =='NAD' ){ echo ' selected '; }?>>$ <?php echo "Namibia Dollar";?></option>
												<option value="NPR" <?php if($mp_settings->moveto_currency =='NPR' ){ echo ' selected '; }?>>₨ <?php echo "Nepal Rupee";?></option>
												<option value="ANG" <?php if($mp_settings->moveto_currency =='ANG' ){ echo ' selected '; }?>>ƒ <?php echo "Netherlands Antilles Guilder";?></option>
												<option value="NZD" <?php if($mp_settings->moveto_currency =='NZD' ){ echo ' selected '; }?>>$ <?php echo "New Zealand Dollar";?></option>
												<option value="NIO" <?php if($mp_settings->moveto_currency =='NIO' ){ echo ' selected '; }?>>C$ <?php echo "Nicaragua Cordoba";?></option>
												<option value="NGN" <?php if($mp_settings->moveto_currency =='NGN' ){ echo ' selected '; }?>>₦ <?php echo "Nigeria Naira";?></option>
												<option value="NOK" <?php if($mp_settings->moveto_currency =='NOK' ){ echo ' selected '; }?>>kr <?php echo "Norway Krone";?></option>
												<option value="OMR" <?php if($mp_settings->moveto_currency =='OMR' ){ echo ' selected '; }?>>﷼ <?php echo "Oman Rial";?></option>
												<option value="MWK" <?php if($mp_settings->moveto_currency =='MWK' ){ echo ' selected '; }?>>MK <?php echo "Malawi Kwacha";?></option>
											<option value="PKR" <?php if($mp_settings->moveto_currency =='PKR' ){ echo ' selected '; }?>>₨ <?php echo "Pakistan Rupee";?></option>
											<option value="PAB" <?php if($mp_settings->moveto_currency =='PAB' ){ echo ' selected '; }?>>B/ <?php echo "Panama Balboa";?></option>
											<option value="PYG" <?php if($mp_settings->moveto_currency =='PYG' ){ echo ' selected '; }?>>Gs <?php echo "Paraguay Guarani";?></option>
											<option value="PEN" <?php if($mp_settings->moveto_currency =='PEN' ){ echo ' selected '; }?>>S/ <?php echo "Peru Nuevo Sol";?></option>
											<option value="PHP" <?php if($mp_settings->moveto_currency =='PHP' ){ echo ' selected '; }?>>₱ <?php echo "Philippines Peso";?></option>
											<option value="PLN" <?php if($mp_settings->moveto_currency =='PLN' ){ echo ' selected '; }?>>zł <?php echo "Poland Zloty";?></option>
											<option value="QAR" <?php if($mp_settings->moveto_currency =='QAR' ){ echo ' selected '; }?>>﷼ <?php echo "Qatar Riyal";?></option>
											<option value="RON" <?php if($mp_settings->moveto_currency =='RON' ){ echo ' selected '; }?>>lei <?php echo "Romania New Leu";?></option>
											<option value="RUB" <?php if($mp_settings->moveto_currency =='RUB' ){ echo ' selected '; }?>>руб <?php echo "Russia Ruble";?></option>
											<option value="SHP" <?php if($mp_settings->moveto_currency =='SHP' ){ echo ' selected '; }?>>£ <?php echo "Saint Helena Pound";?></option>
											<option value="SAR" <?php if($mp_settings->moveto_currency =='SAR' ){ echo ' selected '; }?>>﷼ <?php echo "Saudi Arabia	Riyal";?></option>
											<option value="RSD" <?php if($mp_settings->moveto_currency =='RSD' ){ echo ' selected '; }?>>Дин <?php echo "Serbia Dinar";?></option>
											<option value="SCR" <?php if($mp_settings->moveto_currency =='SCR' ){ echo ' selected '; }?>>₨ <?php echo "Seychelles Rupee";?></option>
											<option value="SGD" <?php if($mp_settings->moveto_currency =='SGD' ){ echo ' selected '; }?>>$ <?php echo "Singapore	Dollar";?></option>
											<option value="SBD" <?php if($mp_settings->moveto_currency =='SBD' ){ echo ' selected '; }?>>$ <?php echo "Solomon Islands Dollar";?></option>
											<option value="SOS" <?php if($mp_settings->moveto_currency =='SOS' ){ echo ' selected '; }?>>S <?php echo "Somalia Shilling";?></option>
											<option value="SLL" <?php if($mp_settings->moveto_currency =='SLL' ){ echo ' selected '; }?>>Le <?php echo "Sierra Leone Leone";?></option>
											<option value="STD" <?php if($mp_settings->moveto_currency =='STD' ){ echo ' selected '; }?>>Db <?php echo "Sao Tome Dobra";?></option>
											<option value="SZL" <?php if($mp_settings->moveto_currency =='SZL' ){ echo ' selected '; }?>>SZL <?php echo "Swaziland Lilageni";?></option>
											<option value="ZAR" <?php if($mp_settings->moveto_currency =='ZAR' ){ echo ' selected '; }?>>R <?php echo "South Africa Rand";?></option>
											<option value="LKR" <?php if($mp_settings->moveto_currency =='LKR' ){ echo ' selected '; }?>>₨ <?php echo "Sri Lanka Rupee";?></option>
											<option value="SEK" <?php if($mp_settings->moveto_currency =='SEK' ){ echo ' selected '; }?>>kr <?php echo "Sweden Krona";?></option>
											<option value="CHF" <?php if($mp_settings->moveto_currency =='CHF' ){ echo ' selected '; }?>>CHF <?php echo "Switzerland Franc";?> </option>
											<option value="SRD" <?php if($mp_settings->moveto_currency =='SRD' ){ echo ' selected '; }?>>$ <?php echo "Suriname Dollar";?></option>
											<option value="SYP" <?php if($mp_settings->moveto_currency =='SYP' ){ echo ' selected '; }?>>£ <?php echo "Syria	Pound";?></option>
											<option value="TWD" <?php if($mp_settings->moveto_currency =='TWD' ){ echo ' selected '; }?>>NT <?php echo "Taiwan New Dollar";?></option>
											<option value="THB" <?php if($mp_settings->moveto_currency =='THB' ){ echo ' selected '; }?>>฿ <?php echo "Thailand Baht";?></option>
											<option value="TOP" <?php if($mp_settings->moveto_currency =='TOP' ){ echo ' selected '; }?>>T$ <?php echo "Tonga Pa'ang";?></option>
											<option value="TZS" <?php if($mp_settings->moveto_currency =='TZS' ){ echo ' selected '; }?>>x <?php echo "Tanzanian Shilling";?></option>
											<option value="TTD" <?php if($mp_settings->moveto_currency =='TTD' ){ echo ' selected '; }?>>TTD <?php echo "Trinidad and Tobago Dollar";?></option>
											<option value="TRY" <?php if($mp_settings->moveto_currency =='TRY' ){ echo ' selected '; }?>>₤ <?php echo "Turkey Lira";?></option>
											<option value="TVD" <?php if($mp_settings->moveto_currency =='TVD' ){ echo ' selected '; }?>>$ <?php echo "Tuvalu Dollar";?></option>
											<option value="UAH" <?php if($mp_settings->moveto_currency =='UAH' ){ echo ' selected '; }?>>₴ <?php echo "Ukraine Hryvna";?></option>
											<option value="UGX" <?php if($mp_settings->moveto_currency =='UGX' ){ echo ' selected '; }?>>USh <?php echo "Ugandan Shilling";?></option>
											<option value="GBP" <?php if($mp_settings->moveto_currency =='GBP' ){ echo ' selected '; }?>>£ <?php echo "United Kingdom Pound";?></option>
											<option value="USD" <?php if($mp_settings->moveto_currency =='USD' ){ echo ' selected '; }?>>$ <?php echo "United States	Dollar";?></option>
											<option value="UYU" <?php if($mp_settings->moveto_currency =='UYU' ){ echo ' selected '; }?>>$U <?php echo "Uruguay Peso";?></option>
											<option value="UZS" <?php if($mp_settings->moveto_currency =='UZS' ){ echo ' selected '; }?>>лв <?php echo "Uzbekistan Som";?></option>
											<option value="VEF" <?php if($mp_settings->moveto_currency =='VEF' ){ echo ' selected '; }?>>Bs <?php echo "Venezuela Bolivar Fuerte";?></option>
											<option value="VND" <?php if($mp_settings->moveto_currency =='VND' ){ echo ' selected '; }?>>₫ <?php echo "Viet Nam Dong";?></option>
											<option value="VUV" <?php if($mp_settings->moveto_currency =='VUV' ){ echo ' selected '; }?>>Vt <?php echo "Vanuatu Vatu";?></option>
											<option value="XAF" <?php if($mp_settings->moveto_currency =='XAF' ){ echo ' selected '; }?>>BEAC <?php echo "CFA Franc (BEAC)";?></option>
											<option value="XOF" <?php if($mp_settings->moveto_currency =='XOF' ){ echo ' selected '; }?>>BCEAO <?php echo "CFA Franc (BCEAO)";?></option>
											<option value="XPF" <?php if($mp_settings->moveto_currency =='XPF' ){ echo ' selected '; }?>>F <?php echo "Pacific Franc";?></option>
											<option value="YER" <?php if($mp_settings->moveto_currency =='YER' ){ echo ' selected '; }?>>﷼ <?php echo "Yemen	Rial";?></option>
											<option value="WST" <?php if($mp_settings->moveto_currency =='WST' ){ echo ' selected '; }?>>WS$ <?php echo "Samoa Tala";?></option>
											<option value="ZAR" <?php if($mp_settings->moveto_currency =='ZAR' ){ echo ' selected '; }?>>R <?php echo "South African Rand";?></option>
											<option value="ZWD" <?php if($mp_settings->moveto_currency =='ZWD' ){ echo ' selected '; }?>>Z$ <?php echo "Zimbabwe Dollar";?></option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td><label><?php echo __("Currency symbol position","mp");?></label></td>
										<td>
											<div class="form-group">
												<select id="moveto_currency_symbol_position" class="selectpicker" data-size="10"  style="display: none;">
													<option value="B"  <?php if($mp_settings->moveto_currency_symbol_position!='P') { echo " selected "; }?>  ><?php echo __("Before","mp");?>&nbsp;&nbsp;(e.g.&nbsp;<?php echo $mp_settings->moveto_currency_symbol;?>100)</option>
													<option value="A" <?php if($mp_settings->moveto_currency_symbol_position=='P') { echo " selected "; }?> ><?php echo __("After","mp");?>&nbsp;&nbsp;(e.g.&nbsp;100<?php echo $mp_settings->moveto_currency_symbol;?>)</option>
												</select>
											</div>	
										</td>
									</tr>	
									<tr>
										<td><label><?php echo __("Maximum Distance","mp");?></label></td>
										<td>
											<div class="form-group">
												<input id="moveto_max_distance" type="text" class="form-control" name="" value="<?php echo $mp_settings->moveto_max_distance; ?>" placeholder="eg: 1000" />
											</div>	
										</td>
									</tr>	
									<tr>
										<td><label><?php echo __("GST/Tax (%)","mp");?></label></td>
										<td>
											<div class="form-group">
												<input id="moveto_tax" type="text" class="form-control" name="" value="<?php echo $mp_settings->moveto_tax; ?>" placeholder="" />
											</div>	
										</td>
									</tr>	
									<tr>
										<td><label><?php echo __("Discount Type","mp");?></label></td>
										<td>
											<label>
												<input class="m-0 discount_type" name="disc_type" id="unit_km" type="radio" value="P" <?php  if(get_option('moveto_discount_type') == 'P'){echo "checked";} ?>/>
												<span>Percentage</span>
													</label><span>&nbsp&nbsp</span>
												<label>
												<input class="m-0 discount_type" name="disc_type" id="unit" type="radio" value="F" <?php  if(get_option('moveto_discount_type') == 'F'){echo "checked";} ?>/>
												<span>Flat</span>
										</label>
									</td>
									</tr>	
									<tr>
										<td><label><?php echo __("Discount","mp");?></label></td>
										<td>
											<div class="form-group">
												<input id="moveto_discount" type="text" class="form-control" name="" value="<?php echo $mp_settings->moveto_discount; ?>" placeholder="" />
											</div>	
										</td>
									</tr>	
									<tr>
										<td><label><?php echo __("'Thankyou Page' Url","mp");?></label></td>
										<td>
											<div class="form-group">
												<input id="moveto_thankyou_page" type="text" class="form-control" size="50" name="" value="<?php echo $mp_settings->moveto_thankyou_page; ?>" placeholder="Custom Thankyou page url" />
												<i><?php echo __("Default url is :","mp");?> <?php echo site_url();?>/mp-thankyou/</i>
											</div>	
										</td>
									</tr>
									<tr>
										<td><label><?php echo __("Thankyou Page message","mp");?></label></td>
										<td>
											<div class="form-group">
												
												<textarea id="moveto_thankyou_page_message"  class="form-control" cols="44"><?php echo $mp_settings->moveto_thankyou_page_message; ?></textarea>
											</div>	
										</td>
									</tr>
												
								<tr>
                                    <td><label><?php echo __("Cancellation Policy","mp");?></label></td>
                                    <td>
                                        <div class="form-group">
                                            <label class="toggle-large" for="moveto_cancelation_policy_status">
												<input type="checkbox" class="mp-toggle-sh" name="moveto_cancelation_policy_status" id="moveto_cancelation_policy_status" <?php if($mp_settings->moveto_cancelation_policy_status=='E') { echo ' checked  '; }?>  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
											</label>
											<div class="<?php if($mp_settings->moveto_cancelation_policy_status=='D') { echo "hide-div";} ?> collapse_moveto_cancelation_policy_status">
												<div class="mp-custom-radio">
                                                    <ul class="mp-radio-list np mb-15">
                                                        <li class="w100">
                                                            <label><?php echo __("Cancellation Policy Header","mp");?></label>
                                                            <input type="text" class="w100 form-control" id="moveto_cancelation_policy_header" name="moveto_cancelation_policy_header" value="<?php echo ($mp_settings->moveto_cancelation_policy_header);?>" />
                                                        </li>
                                                    </ul>
                                                </div>
                                                <label><?php echo __("Cancellation Policy Textarea","mp");?></label>
                                               <textarea class="form-control w100" id="moveto_cancelation_policy_text" name="moveto_cancelation_policy_text" row="4" cols="40"><?php echo ($mp_settings->moveto_cancelation_policy_text);?></textarea>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
							
							<tr>
										<td><label><?php echo __("Estimate Quote","mp");?></label></td>
										<td>
										
										
											<div class="form-group">
												<label class="toggle-large" for="move_pricing_status">
													<input <?php if($mp_settings->move_pricing_status=='Y'){ echo 'checked="checked"';} ?> type="checkbox" id="move_pricing_status" class="mp-toggle-sh"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
												</label>
											</div>
										</td>
							</tr>
								
							
							
									
								
								
								
								
								
								
								
								<tr>
                                    <td><label><?php echo __("Terms & Conditions","mp");?></label></td>
                                    <td>
                                        <div class="form-group">
                                        	<label class="toggle-large" for="moveto_allow_terms_and_conditions">
												<input type="checkbox" class="mp-toggle-sh" name="moveto_allow_terms_and_conditions" id="moveto_allow_terms_and_conditions" <?php if($mp_settings->moveto_allow_terms_and_conditions=='E') { echo ' checked  '; }?>  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
											</label>
											
                                            <div class="<?php if($mp_settings->moveto_allow_terms_and_conditions=='D') { echo "hide-div";}?> collapse_moveto_allow_terms_and_conditions">
                                                <div class="mp-custom-radio">
                                                    <ul class="mp-radio-list">
                                                        <li>
                                                            <label><?php echo __("Terms & Condition Link","mp");?></label>
                                                            <input type="text" class="form-control" size="50" id="moveto_allow_terms_and_conditions_url" name="moveto_allow_terms_and_conditions_url" value="<?php echo urldecode($mp_settings->moveto_allow_terms_and_conditions_url);?>" />
														</li>
                                                    </ul>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label><?php echo __("Privacy Policy","mp");?></label></td>
                                    <td>
                                        <div class="form-group">
                                        	<label class="toggle-large" for="moveto_allow_privacy_policy">
												<input type="checkbox" class="mp-toggle-sh" name="moveto_allow_privacy_policy" id="moveto_allow_privacy_policy" <?php if($mp_settings->moveto_allow_privacy_policy=='E') { echo ' checked  '; }?>  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
											</label>
											
											<div class="<?php if($mp_settings->moveto_allow_privacy_policy=='D') { echo "hide-div";}?> collapse_moveto_allow_privacy_policy">
												<div class="mp-custom-radio">
                                                    <ul class="mp-radio-list">
                                                        <li class="mp-privacy-policy-li-width">
                                                            <?php echo __("Privacy Policy Link","mp");?>
                                                            <input type="text" class="form-control" size="50" id="moveto_allow_privacy_policy_url" name="moveto_allow_privacy_policy_url" value="<?php echo urldecode($mp_settings->moveto_allow_privacy_policy_url);?>" />
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                         </div>
                                    </td>
                                </tr>
															
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td>
											<a id="mp_save_general_settings" name="" class="btn btn-success"><?php echo __("Save Setting","mp");?></a>
											<button type="reset" class="btn btn-default ml-30"><?php echo __("Default Setting","mp");?></button>
								
										</td>
									</tr>
								</tfoot>
							</table>
							
						</div>
					</div>
				</form>	
			</div>
			
			<div class="tab-pane mp-toggle-abs" id="appearance-setting">
				<form id="" method="post" type="" class="mp-appearance-settings" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("Appearance Settings","mp");?></h1>
						</div>
						<div class="panel-body">
							<table class="form-inline mp-common-table" >
								<tbody>
									<tr>
										<td><label> <?php echo __("Color Scheme","mp");?></label></td>
										<td>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">
												<label><?php echo __("Primary Color","mp");?></label>
												<input type="text" id="moveto_primary_color" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_primary_color;?>" />
											</div>	
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">
												<label><?php echo __("Secondary color","mp");?></label>
												<input type="text" id="moveto_secondary_color" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_secondary_color;?>" />
											</div>	
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">
												<label><?php echo __("Text color","mp");?></label>
												<input type="text" id="moveto_text_color" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_text_color;?>" />
											</div>	
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">
												<label><?php echo __("Text color on Bg","mp");?></label>
												<input type="text" id="moveto_bg_text_color" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_bg_text_color;?>" />
											</div>	
										</td>
									</tr>
									<tr>
										<td><label> <?php echo __("Admin Area Color Scheme","mp");?></label></td>
										<td>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">	
												<label><?php echo __("Admin Primary Color","mp");?></label>
												<input type="text" id="moveto_admin_color_primary" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_admin_color_primary;?>" />
											</div>	
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">	
												<label><?php echo __("Admin Secondary color","mp");?></label>
												<input type="text" id="moveto_admin_color_secondary" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_admin_color_secondary;?>" />
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">	
												<label><?php echo __("Admin Text color","mp");?></label>
												<input type="text" id="moveto_admin_color_text" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_admin_color_text;?>" />
											</div>
											<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 npl">
												<label><?php echo __("Admin Text color on Bg","mp");?></label>
												<input type="text" id="moveto_admin_color_bg_text" class="form-control demo" data-control="saturation" value="<?php echo $mp_settings->moveto_admin_color_bg_text;?>" />
											</div>
										</td>
									</tr>
									
									<!--<tr>
										<td><label><?php echo __("Guest user checkout","mp");?></label></td>
										<td>
											<div class="form-group">
												<label for="moveto_guest_user_checkout">
													<input <?php if($mp_settings->moveto_guest_user_checkout=='E') { echo ' checked="checked" '; }?> type="checkbox" id="moveto_guest_user_checkout" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
												</label>
											</div>
											<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("With this feature you can allow a visitor to book appointment without registration.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
										</td>
									</tr>-->
									<tr>
										<td><label><?php echo __("Postal code","mp");?></label></td>
										<td>
											<div class="form-group">
												<label for="moveto_postalcode">
													<input <?php if($mp_settings->moveto_postalcode=='E') { echo ' checked="checked" '; }?> type="checkbox" id="moveto_postalcode" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
												</label>
											</div>
										</td>
									</tr>
									<!--<tr>
										<td><label><?php echo __("Cabs","mp");?></label></td>
										<td>
											<div class="form-group">
												<label for="moveto_cabs">
													<input <?php if($mp_settings->moveto_cabs=='E') { echo ' checked="checked" '; }?> type="checkbox" id="moveto_cabs" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
												</label>
											</div>
										</td>
									</tr>-->
									<?php 
									$arrs = explode(",",$mp_settings->moveto_country_flags);
									$country_code_alpha_array = array
  (
	array("af","Afghanistan (&#8235;افغانستان&#8236;&lrm;)","+93"),array("al","Albania (Shqipëri)","+355  "),array("dz","Algeria (&#8235;الجزائر&#8236;&lrm;)","+213"),array("as","American Samoa","+1684 "),array("ad","Andorra","+376"),array("ao","Angola","+244"),array("ai","Anguilla","+1264"),array("ag","Antigua and Barbuda","+1268"),array("ar","Argentina","+54"),array("am","Armenia (Հայաստան)","+374"), array("aw","Aruba","+297"), array("au","Australia","+61"),array("at","Austria (Österreich)","+43"),array("az","Azerbaijan (Azərbaycan)","+994"),array("bs","Bahamas","+1242"),array("bh","Bahrain (&#8235;البحرين&#8236;&lrm;)","+973"),array("ct","Bangladesh (বাংলাদেশ)","+880"),array("bb","Barbados","+1246"), array("by","Belarus (Беларусь)","+375"),array("be","Belgium (België)","+32"),array("bz","Belize","+501"),array("bj","Benin (Bénin)","+229"),array("bm","Bermuda","+1441"),array("bt","Bhutan (འབྲུག)  ","+975"),array("bo","Bolivia","+591"),array("ba","Bosnia and Herzegovina (Босна и Херцеговина)","+387"),array("bw","Botswana","+267"),array("br","Brazil (Brasil)","+55"),array("io","British Indian Ocean Territory","+246"),array("vg","British Virgin Islands","+1284"),array("bn","Brunei","+673"),array("bg","Bulgaria (България)","+359"),array("bf","Burkina Faso","+226"),array("bi","Burundi (Uburundi)","+257"),array("kh","Cambodia (កម្ពុជា)","+855 "), array("cm","Cameroon (Cameroun)","+237"),array("ca","Canada","+1"),array("cv","Cape Verde (Kabu Verdi)","+238 "),array("bq","Caribbean Netherlands","+599 "), array("ky","Cayman Islands","+1345"), array("cf","Central African Republic (République centrafricaine)","+236"),array("td","Chad (Tchad)","+23"),array("cl","Chile","+56"),array("cn","China (中国)","+86"),array("cx","Christmas Island","+61"),array("cc","Cocos (Keeling) Islands","+61"),array("co","Colombia","+57"),array("km","Comoros (&#8235;جزر القمر&#8236;&lrm;)","+269"),array("cd","Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)","+243"),array("cg","Congo (Republic) (Congo-Brazzaville)","+242"),array("ck","Cook Islands","+682"),array("cr","Costa Rica","+506"),array("ci","Côte d’Ivoire","+225"),array("hr","Croatia (Hrvatska)","+385"),array("cu","Cuba","+53"),array("cw","Curaçao","+599"),array("cy","Cyprus (Κύπρος)","+357"),array("cz","Czech Republic (Česká republika)","+420"),array("dk","Denmark (Danmark)","+45"),array("dj","Djibouti","+253"),array("dm","Dominica","+1767"),array("do","Dominican Republic (República Dominicana)","+1"),array("ec","Ecuador","+593"),array("eg","Egypt (&#8235;مصر&#8236;&lrm;)","+20 "),array("sv","El Salvador","+503"),array("gq","Equatorial Guinea (Guinea Ecuatorial)","+240"),array("er","Eritrea","+291"),array("ee","Estonia (Eesti)","+372"),array("et","Ethiopia","+251"),array("fk","Falkland Islands (Islas Malvinas)","+500"),array("fo","Faroe Islands (Føroyar)","+298"),array("fj","Fiji","+679"),array("fi","Finland (Suomi)","+358"),array("fr","France","+33"),array("gf","French Guiana (Guyane française)","+594"),array("pf","French Polynesia (Polynésie française)","+689"),array("ga","Gabon","+241"), array("gm","Gambia","+220"),array("ge","Georgia (საქართველო)","+995"),array("de","Germany (Deutschland)","+49"),array("gh","Ghana (Gaana)","+233"),array("gi","Gibraltar","+350"),array("gr","Greece (Ελλάδα)","+30"),array("gl","Greenland (Kalaallit Nunaat)","+299"),array("gd","Grenada","+1473"), array("gp","Guadeloupe","+590"),array("gu","Guam","+1671"),array("gt","Guatemala","+502"),array("gg","Guernsey","+44"),array("gn","Guinea (Guinée)","+224"),array("gw","Guinea-Bissau (Guiné Bissau)","+245"),array("gy","Guyana","+592"),array("ht","Haiti","+509"),array("hn","Honduras","+504"),array("hk","Hong Kong (香港)","+852"),array("hu","Hungary (Magyarország)","+36"),array("is","Iceland (Ísland)","+354"),array("in","India (भारत)","+91"),array("id","Indonesia","+62"),array("ir","Iran (&#8235;ایران&#8236;&lrm;)","+98"),array("iq","Iraq (&#8235;العراق&#8236;&lrm;)","+964"),array("ie","Ireland","+353"),array("im","Isle of Man","+44"),array("il","Israel (&#8235;ישראל&#8236;&lrm;)","+972"),array("it","Italy (Italia)","+39"),array("jm","Jamaica","+1876"),array("jp","Japan (日本)","+81"),array("je","Jersey","+44"),array("jo","Jordan (&#8235;الأردن&#8236;&lrm;)","+962"),array("kz","Kazakhstan (Казахстан)","+7"),array("ke","Kenya","+254"),array("ki","Kiribati","+686"),array("kw","Kuwait (&#8235;الكويت&#8236;&lrm;)","+965"),array("kg","Kyrgyzstan (Кыргызстан)","+996"),array("la","Laos (ລາວ)","+856"),array("lv","Latvia (Latvija)","+371"),array("lb","Lebanon (&#8235;لبنان&#8236;&lrm;)","+961"),array("ls","Lesotho","+266"),array("lr","Liberia","+231"),array("ly","Libya (&#8235;ليبيا&#8236;&lrm;)","+218"),array("li","Liechtenstein","+423"),array("lt","Lithuania (Lietuva)","+370"),array("lu","Luxembourg","+352"),array("mo","Macau (澳門)","+853"),array("mk","Macedonia (FYROM) (Македонија)","+389"),array("mg","Madagascar (Madagasikara)","+261"),array("mw","Malawi","+265"),array("my","Malaysia","+60"),array("mv","Maldives","+960"),array("ml","Mali","+223"), array("mt","Malta","+356"),array("mh","Marshall Islands","+692"),array("mq","Martinique","+596"),array("mr","Mauritania (&#8235;موريتانيا&#8236;&lrm;)","+222"),array("mu","Mauritius (Moris)","+230"),array("yt","Mayotte","+262"),array("mx","Mexico (México)","+52"),array("fm","Micronesia","+691"),array("md","Moldova (Republica Moldova)","+373"),array("mc","Monaco","+377"),array("mn","Mongolia (Монгол)","+976"),array("me","Montenegro (Crna Gora)","+382"),array("ms","Montserrat","+1664"),array("ma","Morocco (&#8235;المغرب&#8236;&lrm;)","+212"),array("mz","Mozambique (Moçambique)","+258"),array("mm","Myanmar (Burma) (မြန်မာ)","+95"),array("na","Namibia (Namibië)","+264"),array("nr","Nauru","+674"),array("np","Nepal (नेपाल)","+977"),array("nl","Netherlands (Nederland)","+31"),array("nc","New Caledonia (Nouvelle-Calédonie)","+687"),array("nz","New Zealand","+64"),array("ni","Nicaragua","+505"),array("ne","Niger (Nijar)","+227"),array("ng","Nigeria","+234"),array("nu","Niue","+683"),array("nf","Norfolk Island","+672"),array("kp","North Korea (조선 민주주의 인민 공화국)","+850"),array("mp","Northern Mariana Islands","+1670"),array("no","Norway (Norge)","+47"),array("om","Oman (&#8235;عُمان&#8236;&lrm;)","+968"),array("pk","Pakistan (&#8235;پاکستان&#8236;&lrm;)","+92"),array("pw","Palau","+680"),array("ps","Palestine (&#8235;فلسطين&#8236;&lrm;)","+970"),array("pa","Panama (Panamá)","+507"),array("pg","Papua New Guinea","+675"),array("py","Paraguay","+595"),array("pe","Peru (Perú)","+51"),array("ph","Philippines","+63"),array("pl","Poland (Polska)","+48"),array("pt","Portugal","+351"),array("pr","Puerto Rico","+1"),array("qa","Qatar (&#8235;قطر&#8236;&lrm;)","+974"),array("re","Réunion (La Réunion)","+262"),array("ro","Romania (România)","+40"),array("ru","Russia (Россия)","+7"),array("rw","Rwanda","+250"),array("bl","Saint Barthélemy (Saint-Barthélemy)","+590"),array("sh","Saint Helena","+290"), array("kn","Saint Kitts and Nevis","+1869"),array("lc","Saint Lucia","+1758"), array("mf","Saint Martin (Saint-Martin (partie française))","+590"),array("pm","Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)","+508"), array("vc","Saint Vincent and the Grenadines","+1784"),array("ws","Samoa","+685"),array("sm","San Marino","+378"),array("st","São Tomé and Príncipe (São Tomé e Príncipe)","+239"),array("sa","Saudi Arabia (&#8235;المملكة العربية السعودية&#8236;&lrm;)","+966"),array("sn","Senegal (Sénégal)","+221"),array("rs","Serbia (Србија)","+381"),array("sc","Seychelles","+248"),array("sl","Sierra Leone","+232"),array("sg","Singapore","+65"),array("sx","Sint Maarten","+1721"),array("sk","Slovakia (Slovensko)","+421"),array("si","Slovenia (Slovenija)","+386"),array("sb","Solomon Islands","+677"),array("so","Somalia (Soomaaliya)","+252"),array("za","South Africa","+27"),array("kr","South Korea (대한민국)","+82"),array("ss","South Sudan (&#8235;جنوب السودان&#8236;&lrm;)","+211"),array("es","Spain (España)","+34"),array("lk","Sri Lanka (ශ්&zwj;රී ලංකාව)","+94"),array("sd","Sudan (&#8235;السودان&#8236;&lrm;)","+249"),array("sr","Suriname","+597"),array("sj","Svalbard and Jan Mayen","+47"),array("sz","Swaziland","+268"),array("se","Sweden (Sverige)","+46"),array("ch","Switzerland (Schweiz)","+41"),array("sy","Syria (&#8235;سوريا&#8236;&lrm;)","+963"),array("tw","Taiwan (台灣)","+886"),array("tj","Tajikistan","+992"),array("tz","Tanzania","+255"),array("th","Thailand (ไทย)","+66"),array("tl","Timor-Leste","+670"),array("tg","Togo","+228"),array("tk","Tokelau","+690"),array("to","Tonga","+676"),array("tt","Trinidad and Tobago","+1868"),array("tn","Tunisia (&#8235;تونس&#8236;&lrm;)","+216"),array("tr","Turkey (Türkiye)","+90"),array("tm","Turkmenistan","+993"),array("tc","Turks and Caicos Islands","+1649"),array("tv","Tuvalu","+688"),array("vi","U.S. Virgin Islands","+1340"),array("ug","Uganda","+256"),array("ua","Ukraine (Україна)","+380"),array("ae","United Arab Emirates (&#8235;الإمارات العربية المتحدة&#8236;&lrm;)","+971"),array("gb","United Kingdom","+44"),array("us","United States","+1"),array("uy","Uruguay","+598"),array("uz","Uzbekistan (Oʻzbekiston)","+998"),array("vu","Vanuatu","+678"),array("va","Vatican City (Città del Vaticano)","+39"),array("ve","Venezuela","+58"),array("vn","Vietnam (Việt Nam)","+84"),array("wf","Wallis and Futuna","+681"),array("eh","Western Sahara (&#8235;الصحراء الغربية&#8236;&lrm;)","+212"),array("ye","Yemen (&#8235;اليمن&#8236;&lrm;)","+967"),array("zm","Zambia","+260"),array("zw","Zimbabwe","+263"),array("ax","Åland Islands","+358"));
									?>
									
									<tr>
										<td><label><?php echo __("Country code","mp");?></label></td>
										<td>
											 <select id="selected_country_code_display" name="selected_country_code_display[]" multiple class="selectpicker" data-size="10" data-live-search="true" data-live-search-placeholder="search">
									
								 <?php  
										for($i=0;$i<count((array)$country_code_alpha_array);$i++){
											?>
											<option <?php  if(in_array($country_code_alpha_array[$i][0],$arrs)){ echo "selected"; }?> data-subtext="<?php echo $country_code_alpha_array[$i][1]; ?> - <?php  echo $country_code_alpha_array[$i][2]?>" value="<?php echo $country_code_alpha_array[$i][0];?>"><?php echo $country_code_alpha_array[$i][0]?></option>
											<?php  
										}
									?>
								</select>
										</td>
									</tr>
									<tr>
									   <td><label><?php echo __("Frontend Custom CSS","mp");?></label></td>
									   <td>       
										<div class="form-group">
										 <label for="moveto_frontend_custom_css">
										  <textarea id="moveto_frontend_custom_css" class="form-control" cols="80" rows="6"><?php echo $mp_settings->moveto_frontend_custom_css; ?></textarea>
										 </label>
										</div>
										 <a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("This custom css will apply on frontend","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
												
									   </td>
									</tr>									
								 </tbody>
								<tfoot>
									<tr>
										<td></td>
										<td>
											<a href="javascript:void(0)" id="mp_save_appearance_settings" name="" class="btn btn-success" type="submit"><?php echo __("Save Setting","mp");?></a>
											<button type="reset" class="btn btn-default ml-30"><?php echo __("Default Setting","mp");?></button>
								
										</td>
									</tr>
								</tfoot>
							</table>
							
						</div>
					</div>
				</form>
			</div>			
			<div class="tab-pane mp-toggle-abs" id="payment-setting">
				<form id="" method="post" type="" class="mp-payment-settings" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("Payment Gateways","mp");?></h1>
						</div>
						<div class="panel-body">
							<div id="accordion" class="panel-group">
								<div class="panel panel-default mp-all-payments-main">
									<div class="panel-heading">
										<h4 class="panel-title">
											<span><?php echo __("All Payment Gateways","mp");?></span>
											<div class="mp-enable-disable-right pull-right">
												<label class="toggle-large" for="moveto_payment_gateways_status">
													<input type="checkbox" <?php if($mp_settings->moveto_payment_gateways_status=='E'){ echo 'checked="checked"';} ?> class="mp-toggle-sh" id="moveto_payment_gateways_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
												</label>
											</div>
											
										</h4>
									</div>
									
									
									<div id="collapseOne" class="<?php if($mp_settings->moveto_payment_gateways_status=='D'){ echo 'hide-div';} ?>  panel-collapse  collapse_moveto_payment_gateways_status">
										<div class="panel-body">
										
										<div class="alert alert-danger" style="display: none;">
											<a href="#" class="close" data-dismiss="alert">&times;</a>
											<strong><?php echo __("Warning!","mp");?></strong><?php echo __("Currency you have selected ( currency option ) is not supported by Stipe.","mp");?> 
										</div>
											<div id="accordion" class="panel-group">
												<div class="panel panel-default mp-payment-methods">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Pay locally","mp");?></span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for="moveto_locally_payment_status">
																	<input type="checkbox" <?php if($mp_settings->moveto_locally_payment_status=='E'){ echo 'checked="checked"';} ?> class="mp-toggle-sh" id="moveto_locally_payment_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
															
														</h4>
													</div>
												</div>
												<div class="panel panel-default mp-payment-methods">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Stripe Payment Form","mp");?>
															<img class="mp-img-payments mp-stripe" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/stripe.jpg" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for=		"moveto_payment_method_Stripe">
																	<input <?php if($mp_settings->moveto_payment_method_Stripe=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_payment_method_Stripe" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																	
																	
																</label>
															</div>
														</h4>
													</div>
													<div id="collapseOne" class="<?php if($mp_settings->moveto_payment_method_Stripe=='D'){ echo 'hide-div';} ?> panel-collapse collapse_moveto_payment_method_Stripe">
														<div class="panel-body">
															<table class="form-inline mp-common-table">
																<tbody>
																	<tr>
																		<td><label><?php echo __("Secret Key*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="text" class="form-control" id="moveto_stripe_secretKey" size="50" value="<?php echo $mp_settings->moveto_stripe_secretKey ;?>" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Publishable Key*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_stripe_publishableKey" value="<?php echo $mp_settings->moveto_stripe_publishableKey;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<!--Paypal Express Checkout-start-->
												<div class="panel panel-default mp-payment-methods">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Paypal Express Checkout","mp");?>
															<img class="mp-img-payments mp-paypal" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/paypal.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for=		"moveto_payment_method_Paypal">
																	<input <?php if($mp_settings->moveto_payment_method_Paypal=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_payment_method_Paypal" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
													<div id="collapseOne" class="<?php if($mp_settings->moveto_payment_method_Paypal=='D'){ echo 'hide-div';} ?> panel-collapse collapse_moveto_payment_method_Paypal">
														<div class="panel-body">
															<table class="form-inline mp-common-table">
																<tbody>
																	<tr>
																		<td><label><?php echo __("API Username*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="text" class="form-control" id="moveto_paypal_api_username" size="50" value="<?php echo $mp_settings->moveto_paypal_api_username ;?>" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("API Password*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paypal_api_password" value="<?php echo $mp_settings->moveto_paypal_api_password;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Signature*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paypal_api_signature" value="<?php echo $mp_settings->moveto_paypal_api_signature;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Paypal guest payment","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<label class="toggle-large" for="moveto_paypal_guest_checkout">
																					<input <?php if($mp_settings->moveto_paypal_guest_checkout=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_paypal_guest_checkout" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																				
																				</label>
																			</div>	
																			<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Let user pay through credit card without having Paypal account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Test Mode","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<label class="toggle-large" for="moveto_paypal_testing_mode">
																					<input <?php if($mp_settings->moveto_paypal_testing_mode=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_paypal_testing_mode" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																				
																				</label>
																			</div>	
																			<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("You can enable Paypal test mode for sandbox account testing.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<!--Paypal Express Checkout-end-->
													<!--Paytm-start-->
												<div class="panel panel-default mp-payment-methods">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Paytm Payment Form","mp");?>
															<img class="mp-img-payments mp-paytm" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/paytm.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for=		"moveto_payment_method_Paytm">
																	<input <?php if($mp_settings->moveto_payment_method_Paytm=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_payment_method_Paytm" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
													<div id="collapseOne" class="<?php if($mp_settings->moveto_payment_method_Paytm=='D'){ echo 'hide-div';} ?> panel-collapse collapse_moveto_payment_method_Paytm">
														<div class="panel-body">
															<table class="form-inline mp-common-table">
																<tbody>
																	<tr>
																		<td><label><?php echo __("Merchant Key*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="text" class="form-control" id="moveto_paytm_merchantkey" size="50" value="<?php echo $mp_settings->moveto_paytm_merchantkey ;?>" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Merchant Id*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paytm_merchantid" value="<?php echo $mp_settings->moveto_paytm_merchantid;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Merchant Website URL*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paytm_website" value="<?php echo $mp_settings->moveto_paytm_website;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Merchant Channel Id*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paytm_channelid" value="<?php echo $mp_settings->moveto_paytm_channelid;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Merchant Industry Type*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paytm_industryid" value="<?php echo $mp_settings->moveto_paytm_industryid;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Test Mode","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<label class="toggle-large" for="moveto_paytm_testing_mode">
																					<input <?php if($mp_settings->moveto_paytm_testing_mode=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_paytm_testing_mode" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																				
																				</label>
																			</div>	
																			<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("You can enable Paytm test mode for sandbox account testing.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													</div>
													<!--Paytm-start end-->
												<div class="panel panel-default mp-payment-methods">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Paystack Payment Form","mp");?>
															<img class="mp-img-payments mp-paystack" style="height: 21px;" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/paystack-logo.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for="moveto_payment_method_Paystack">
																	<input <?php if($mp_settings->moveto_payment_method_Paystack=='E'){echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_payment_method_Paystack" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
													<div id="collapseOne" class="<?php if($mp_settings->moveto_payment_method_Paystack=='D'){ echo 'hide-div';} ?> panel-collapse collapse_moveto_payment_method_Paystack">
														<div class="panel-body">
															<table class="form-inline mp-common-table">
																<tbody>
																	<tr>
																		<td><label><?php echo __("Public Key*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paystack_public_key" size="50" value="<?php echo $mp_settings->moveto_paystack_public_key ;?>" />
																			</div>
																				<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("This Payment Gateway is Only Use with NGN Currency.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>																			
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("Secret Key*","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="password" class="form-control" id="moveto_paystack_secret_key" value="<?php echo $mp_settings->moveto_paystack_secret_key;?>" size="50" />
																			</div>	
																		</td>
																	</tr>
																	<tr>
																		<td><label><?php echo __("CallBack URL*","mp");?></label></td>
																		<td>
																			<div class="form-group">  
																				<input type="text" class="form-control" value="<?php echo  $callbackurl_paystack;?>" size="50"/>
																			</div>	
																			<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Put This URL into the paystack setting account under the API Keys & Webhooks in Test Callback URL Field.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>		
																		</td>
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
												</div>
												<div style="display: none;" id="error_payment_method">
													<span class="error_payment_method_msg" style="color: #ff0000;font-size: 11px;font-weight: 700;"></span> 
												</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								<a id="mp_save_payment_settings" name="" class="btn btn-success" type="submit"><?php echo __("Save Setting","mp");?></a>
								
							</div>
						</div>
					</div>
			<div class="tab-pane mp-toggle-abs" id="email-setting">
				<form id="" method="post" type="" class="mp-email-settings" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("Email Settings","mp");?></h1>
						</div>
						<div class="panel-body">
							
						<div class="panel-body">
							<table class="form-inline mp-common-table" >
								<tbody>
									<tr>
										<td><label><?php echo __("Admin Email Notifications","mp");?></label></td>
										<td>
											<div class="form-group">
												<label class="toggle-large" for="moveto_admin_email_notification_status">
													<input <?php if($mp_settings->moveto_admin_email_notification_status=='E'){ echo 'checked="checked"';} ?> type="checkbox" id="moveto_admin_email_notification_status" class="mp-toggle-sh"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
												</label>
											</div>
										</td>
									</tr>

									<!--<tr>
										<td><label><?php echo __("Manager Email Notifications","mp");?></label></td>
										<td>
											<div class="form-group">
												<label class="toggle-large" for="moveto_manager_email_notification_status">
													<input  <?php if($mp_settings->moveto_manager_email_notification_status=='E'){ echo 'checked="checked"';} ?> type="checkbox" id="moveto_manager_email_notification_status" class="mp-toggle-sh"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
												</label>
											</div>
										</td>
									</tr>-->
									
									
									<tr>
										<td><label><?php echo __("Client Email Notifications","mp");?></label></td>
										<td>
											<div class="form-group">
												<label class="toggle-large" for="moveto_client_email_notification_status">
													<input <?php if($mp_settings->moveto_client_email_notification_status=='E'){ echo 'checked="checked"';} ?> type="checkbox" id="moveto_client_email_notification_status" class="mp-toggle-sh"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
												</label>
											</div>
										</td>
									</tr>
									<tr>
										<td><label><?php echo __("Sender Name","mp");?></label></td>
										<td>
											<div class="form-group">
												<input type="text" value="<?php echo $mp_settings->moveto_email_sender_name;?>" class="form-control w-300" id="moveto_email_sender_name" />
											</div>
										</td>
									</tr>
									<tr>
										<td><label><?php echo __("Sender Email Address (moveto Admin Email)","mp");?></label></td>
										<td>
											<div class="form-group">
												<input type="email" class="form-control w-300" id="moveto_email_sender_address" value="<?php echo $mp_settings->moveto_email_sender_address;?>" placeholder="admin@example.com" />
											</div>
										</td>
									</tr>
									<tr><td class="np"><hr /></td><td class="np"><hr /></td></tr>
									<td><label><?php echo __("Quote Requests Reminder Buffer","mp");?></label></td>
										<td>
											<div class="form-group">
												<select id="moveto_email_reminder_buffer" class="selectpicker" data-size="5" data-width="auto" >
													<option value=""><?php echo __("Set Email & SMS Reminder Buffer","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='60'){ echo 'selected';} ?> value="60"><?php echo __("1 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='120'){ echo 'selected';} ?> value="120"><?php echo __("2 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='180'){ echo 'selected';} ?> value="180"><?php echo __("3 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='240'){ echo 'selected';} ?> value="240"><?php echo __("4 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='300'){ echo 'selected';} ?> value="300"><?php echo __("5 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='360'){ echo 'selected';} ?> value="360"><?php echo __("6 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='420'){ echo 'selected';} ?> value="420"><?php echo __("7 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='480'){ echo 'selected';} ?> value="480"><?php echo __("8 Hrs","mp");?></option>
													<option <?php if($mp_settings->moveto_email_reminder_buffer=='1440'){ echo 'selected';} ?> value="1440"><?php echo __("1 Day","mp");?></option>
												</select>
											</div>	
										</td>
									</tr>
									
									
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td>
											<a href="javascript:void(0)" id="mp_save_email_settings" name="" class="btn btn-success" type="submit"><?php echo __("Save Setting","mp");?></a>
										</td>
									</tr>
								</tfoot>
							</table>
							
						</div>
							
						</div>
					</div>
				</form>
			</div>
			<div class="tab-pane mp-toggle-abs" id="email-template">
				<div class="panel panel-default wf-100">
					<div class="panel-heading">
						<h1 class="panel-title"><?php echo __("Email Template Settings","mp");?></h1>
					</div>
					<!-- Client email templates -->
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a data-toggle="tab" href="#client-email-template"><?php echo __("Client Email Templates","mp");?></a></li>
						<li><a data-toggle="tab" href="#admin-manager-email-template"><?php echo __("Admin/Manager Email Template","mp");?></a></li>
						
					</ul>
					<div class="tab-content">
						<div id="client-email-template" class="tab-pane fade in active">
							<h3><?php echo __("Client Email Templates","mp");?></h3>
								<div id="accordion" class="panel-group">
									<?php $mp_email_templates->user_type='C';
									$C_templates = $mp_email_templates->readall_by_usertype();
									foreach($C_templates as $C_template){ ?>
									
									<div class="panel panel-default mp-email-panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="mp-col11">
													<div class="mp-yes-no-email-right pull-left">
														<label for="email_template_status<?php echo $C_template->id;?>">
															<input <?php if($C_template->email_template_status=='e'){echo "checked='checked'";} ?> class="mp_update_emailstatus" type="checkbox" id="email_template_status<?php echo $C_template->id;?>" data-eid="<?php echo $C_template->id;?>" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
															
														</label>
													</div>	
													<span id="email_subject_label<?php echo $C_template->id;?>" class="mp-template-name"><?php echo $C_template->email_subject;?></span>
												</div>	
												<div class="pull-right mp-col1">
													<div class="pull-right">
														<div class="mp-show-hide pull-right">
															<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox" id="<?php echo $C_template->id;?>" >
															<label class="mp-show-hide-label" for="<?php echo $C_template->id;?>"></label>
														</div>
													</div>
												</div>
											</h4>
										</div>
										<div class="panel-collapse collapse emailtemplatedetail emaildetail_<?php echo $C_template->id;?>">
											<div class="panel-body">
												<div class="mp-email-temp-collapse-div col-md-12 col-lg-12 col-xs-12 np">
													<form id="" method="post" type="" class="slide-toggle" >
														<div class="col-md-8 col-sm-8 col-xs-12 form-group">
															<label><?php echo __("Email Subject","mp");?></label>
															<input type="text" class="form-control" name="email_subject<?php echo $C_template->id;?>" value="<?php echo $C_template->email_subject;?>" />
															<label><?php echo __("Email Content","mp");?></label>
															<?php
															if($C_template->email_message!=''){
																
															$content=stripslashes_deep($C_template->email_message);
															}else{
															$content=stripslashes_deep($C_template->default_message);
															}
															$editorName=  'email_message'.$C_template->id;
															$editorId ='email_editor'.$C_template->id;
															wp_editor($content,$editorId, array('textarea_name'=>$editorName,'media_buttons'=>true, 'teeny'=>false, 'tinymce' => false,'editor_class'=>'ak_wp_editor','wpautop' => true,'tabindex' => '','tabfocus_elements' => ':prev,:next','dfw' => false,'quicktags' => true)); ?>
															
															<a data-eid="<?php echo $C_template->id;?>" class="btn btn-success mp-btn-width pull-left cb ml-15 mt-20 mp_save_emailtemplate" type="submit"><?php echo __("Save Template","mp");?></a>
															
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12">
															<div class="mp-email-content-tags">
																<b><?php echo __("Tags","mp");?> </b><br />
																<?php 
																if($C_template->email_template_name=='NAC'){
																	echo "<a data-eid='".$C_template->id."' class='tags' data-value='{{account_detail}}'>{{account_detail}}</a><br/>";
																	echo "<a data-eid='".$C_template->id."' class='tags' data-value='{{client_name}}'>{{client_name}}</a><br/>";
																}else{		
																	foreach($email_template_tags as $tags){
																			
																			echo "<a data-eid='".$C_template->id."' class='tags' data-value='".$tags."'>".$tags."</a><br/>";
																	}
																} ?>
																<br />
															</div>
														</div>
														
													</form>	
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>								
						</div>
						<div id="admin-manager-email-template" class="tab-pane fade">
							<h3><?php echo __("Admin/Manager Provider Email Template","mp");?></h3>
							<div id="accordion" class="panel-group">
									<?php $mp_email_templates->user_type='AM';
									$AM_templates = $mp_email_templates->readall_by_usertype();
									foreach($AM_templates as $AM_template){ ?>
									
									<div class="panel panel-default mp-email-panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="mp-col11">
													<div class="mp-yes-no-email-right pull-left">
														<label for="email_template_status<?php echo $AM_template->id;?>">
															<input <?php if($AM_template->email_template_status=='e'){echo "checked='checked'";} ?> class="mp_update_emailstatus" type="checkbox" id="email_template_status<?php echo $AM_template->id;?>" data-eid="<?php echo $AM_template->id;?>" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
														</label>
													</div>
													
													<span id="email_subject_label<?php echo $AM_template->id;?>" class="mp-template-name"><?php echo $AM_template->email_subject;?></span>
														
												</div>	
												<div class="pull-right mp-col1">
													<div class="pull-right">
														<div class="mp-show-hide pull-right">
															<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox" id="<?php echo $AM_template->id;?>" ><!--Added Serivce Id-->
															<label class="mp-show-hide-label" for="<?php echo $AM_template->id;?>"></label>
														</div>
													</div>
												</div>
											</h4>
										</div>
										<div class="panel-collapse collapse emailtemplatedetail emaildetail_<?php echo $AM_template->id;?>">
											<div class="panel-body">
												<div class="mp-email-temp-collapse-div col-md-12 col-lg-12 col-xs-12 np">
													<form id="" method="post" type="" class="slide-toggle" >
														<div class="col-md-8 col-sm-8 col-xs-12 form-group">
															<label><?php echo __("Email Subject","mp");?></label>
															<input type="text" class="form-control" name="email_subject<?php echo $AM_template->id;?>" value="<?php echo $AM_template->email_subject;?>" />
															<label><?php echo __("Email Content","mp");?></label>
															<?php
															if($AM_template->email_message!=''){
															$content=stripslashes_deep($AM_template->email_message);
															}else{
															$content=stripslashes_deep($AM_template->default_message);
															}
															$editorName=  'email_message'.$AM_template->id;
															$editorId ='email_editor'.$AM_template->id;

															wp_editor($content,$editorId, array('textarea_name'=>$editorName,'media_buttons'=>true, 'teeny'=>false, 'tinymce' => false,'editor_class'=>'ak_wp_editor','wpautop' => true,'tabindex' => '','tabfocus_elements' => ':prev,:next','dfw' => false,'quicktags' => true)); ?>
															
															<a data-eid="<?php echo $AM_template->id;?>" class="btn btn-success mp-btn-width pull-left cb ml-15 mt-20 mp_save_emailtemplate" type="submit"><?php echo __("Save Template","mp");?></a>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12">
															<div class="mp-email-content-tags">
																<b><?php echo __("Tags","mp");?> </b><br />
																<?php foreach($email_template_tags as $tags){
																		
																		echo "<a data-eid='".$AM_template->id."' class='tags' data-value='".$tags."'>".$tags."</a><br/>";
																	} ?>
																<br />
															</div>
														</div>
														
													</form>	
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>	
						</div>
						
						
					</div>
				</div>
			</div>
				<!-- ---------------------------------------------- -->
<div class="tab-pane mp-toggle-abs" id="google-api">
				<form id="" method="post" type="" class="mp-email-settings" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("Google Map","mp");?></h1>
						</div>
						<div class="panel-body">
							
						<div class="panel-body">
							<table class="form-inline mp-common-table" style="width:100%;" >
								<tbody>
									
									<tr>
										<td><label><?php echo __("API Key","mp");?></label></td>
										<td>
											<div class="form-group">
												<input type="text" value="<?php echo $mp_settings->moveto_google_api_key;?>" class="form-control w-300" id="moveto_google_api_key" />
											</div>
										</td>
									</tr>
									<tr>
										<td><label><?php echo __("Active Country","mp");?></label></td>
										<td>
											 <select id="google_map_country_code" name="google_map_country_code[]"  class="selectpicker w-300" data-size="10" data-live-search="true" data-live-search-placeholder="search">
											 	<option value="All Countries">All Countries</option>
									
								 <?php  
								 $gmcc = explode(",",$mp_settings->moveto_google_map_country);
										for($i=0;$i<count((array)$country_code_alpha_array);$i++){
											?>
											<option <?php  if(in_array($country_code_alpha_array[$i][0],$gmcc)){ echo "selected"; }?> data-subtext="<?php echo $country_code_alpha_array[$i][1]; ?>" value="<?php echo $country_code_alpha_array[$i][0];?>"><?php echo $country_code_alpha_array[$i][0]?></option>
											<?php  
										}
									?>
								</select>
										</td>
									</tr>
									<tr>
										<td colspan= "2" class="panel-group">
											<div class="panel panel-default mp-all-payments-main">
												<div class="panel-heading">
													<h4 class="panel-title ">
														<span><?php echo __("Company-Origin Distance","mp");?></span>
														<div class="mp-enable-disable-right pull-right">
															<label class="toggle-large" for="moveto_company_origin_destination_distance_status">
																<input type="checkbox" <?php if($mp_settings->moveto_company_origin_destination_distance_status=='E'){ echo 'checked="checked"';} ?> class="mp-toggle-sh" id="moveto_company_origin_destination_distance_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
															</label>
														</div>
														
													</h4>
												</div>
											</div>
										</td>
									</tr>
										<tr><td colspan= "2">
										<div id="collapseOne" class="<?php if($mp_settings->moveto_company_origin_destination_distance_status=='D'){ echo 'hide-div';} ?>  panel-collapse  collapse_moveto_company_origin_destination_distance_status">
										<div class="panel-body">
											<div id="accordion" class="panel-group">
												<div class="panel-body">
															<table class="form-inline mp-common-table">
																<tbody>
																	<tr>
																		<td><label><?php echo __("Company Origin Address","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="text" class="form-control" id="moveto_company_origin_address" size="50" value="<?php echo $mp_settings->moveto_company_origin_address ;?>" />
																			</div>	
																		</td>
																	</tr>
																</tbody>
															</table>
															<div style="padding-left:17%; display: none;" id="error_coa">
													<span style=" color: #ff0000;font-size: 11px;font-weight: 700;">Please enter Company Origin Address</span>
												</div>
														</div>

												<!--Paypal Express Checkout-start-->
												<div class="panel panel-default mp-payment-methods">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Destination-Company Distance","mp");?>
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for=		"moveto_destination_company_distance">
																	<input <?php if($mp_settings->moveto_destination_company_distance=='E'){ echo 'checked="checked"';} ?> type="checkbox" class="mp-toggle-sh" id="moveto_destination_company_distance" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
													<div id="collapseOne" class="<?php if($mp_settings->moveto_destination_company_distance=='D'){ echo 'hide-div';} ?> panel-collapse collapse_moveto_destination_company_distance">
														<div class="panel-body">
															<table class="form-inline mp-common-table">
																<tbody>
																	<tr>
																		<td><label><?php echo __("Company Destination Address","mp");?></label></td>
																		<td>
																			<div class="form-group">
																				<input type="text" class="form-control" id="moveto_company_destination_address" size="50" value="<?php echo $mp_settings->moveto_company_destination_address ;?>" />
																			</div>	
																		</td>
																	</tr>
																</tbody>
															</table>
															<div style="padding-left:21%; display: none;" id="error_dca">
													<span style=" color: #ff0000;font-size: 11px;font-weight: 700;">Please enter Company Origin Address</span>
												</div>
														</div>
													</div>
												</div>
												<!--Paypal Express Checkout-end-->
												</div>
												
											</div>
										</div>
									</div></td></tr>
									
									
								</tbody>
								<tfoot>
									<tr>
										
										<td>
											<a href="javascript:void(0)" id="mp_save_api_key" name="" class="btn btn-success" type="submit"><?php echo __("Save Setting","mp");?></a>
										</td>
									</tr>
								</tfoot>
							</table>
							
						</div>
							
						</div>
					</div>
				</form>
			</div>
<!-- ---------------------------------------------- -->
			<!--twilio --> 
			<div class="tab-pane mp-toggle-abs" id="sms-reminder">
				<form id="" method="post" type="" class="mp-sms-reminder" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("SMS Reminder","mp");?></h1>
						</div>
						<div class="panel-body np">
							<div id="accordion" class="panel-group mp-all-sms-main">
								<div class="panel panel-default mp-sms-gateway nb">
									<div class="panel-heading">
										<h4 class="panel-title">
											<span><?php echo __("SMS Service","mp");?></span>
											<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="It will send sms to service provider and client for Quote Requests Lead"><i class="fa fa-info-circle fa-lg ml-30"></i></a>
											<div class="mp-enable-disable-right pull-right">
												<label class="toggle-large" for="moveto_sms_reminder_status">
													<input <?php if($mp_settings->moveto_sms_reminder_status=='E'){echo "checked='checked'";} ?> type="checkbox" class="mp-toggle-sh" name="moveto_sms_reminder_status" id="moveto_sms_reminder_status"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
												</label>
											</div>
											
										</h4>
									</div>
									
									<div id="collapseOne" class="panel-collapse collapse collapse_moveto_sms_reminder_status hide-div" <?php if($mp_settings->moveto_sms_reminder_status=='E'){ echo 'style="display: block;"'; } ?>>
										<div class="panel-body">
											<div id="accordion" class="panel-group">
												<div class="panel panel-default mp-sms-gateway nb">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Twilio SMS Gateway","mp");?><img class="mp-sms-gateway-img" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/twilio-logo.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for="moveto_sms_noti_twilio">
																	<input <?php if($mp_settings->moveto_sms_noti_twilio=='E'){echo "checked='checked'";} ?> type="checkbox" class="mp-toggle-sh" id="moveto_sms_noti_twilio" name="moveto_sms_noti_twilio"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
										
												<div id="collapseOne" class="panel-collapse collapse_moveto_sms_noti_twilio <?php if($mp_settings->moveto_sms_noti_twilio=='D'){ echo 'hide-div';} ?>">
													<div class="panel-body padding-15">
													<table class="form-inline table mp-common-table table-hover table-bordered table-striped">
														<tr><th colspan="3"><?php echo __("Twilio Account Settings","mp");?></th></tr>
														<tbody>
															<tr>
																<td><label><?php echo __("Account SID","mp");?></label></td>
																<td colspan="2">
																	<div class="form-group mp-lgf">
																		<input type="text" class="form-control" name="moveto_twilio_sid" id="moveto_twilio_sid" size="70" value="<?php echo $mp_settings->moveto_twilio_sid;?>"/>
																	</div>	
																	<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Twilio Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																</td>
															</tr>
															<tr>
																<td><label><?php echo __("Auth Token","mp");?></label></td>
																<td colspan="2">
																	<div class="form-group mp-lgf">
																		<input type="password" class="form-control" name="moveto_twilio_auth_token"
																		id="moveto_twilio_auth_token" size="70" value="<?php echo $mp_settings->moveto_twilio_auth_token;?>" />
																	</div>	
																	<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Twilio Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																</td>
															</tr>
															<tr>
																<td><label><?php echo __("Twilio Sender Number","mp");?></label></td>
																<td colspan="2">
																	<div class="form-group mp-lgf">
																		<input type="text" class="form-control" name="moveto_twilio_number" id="moveto_twilio_number" size="70" value="<?php echo $mp_settings->moveto_twilio_number;?>" />
																	</div>	
																	<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Must be a valid number associated with your Twilio account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																</td>
															</tr>
															<tr>
																<td id="hr"></td><td id="hr"></td><td id="hr"></td>
															</tr>
														</tbody>
														
														<tbody>
														
														<th colspan="3"><?php echo __("Twilio SMS Settings","mp");?></th>
															
															<tr>
																<td><label><?php echo __("Send SMS to Client","mp");?></label></td>
																<td colspan="2">
																	<div class="form-group">
																		<label class="toggle-large" for="moveto_twilio_client_sms_notification_status">
																			<input <?php if($mp_settings->moveto_twilio_client_sms_notification_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_twilio_client_sms_notification_status" id="moveto_twilio_client_sms_notification_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																		</label>
																	</div>	
																	<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to client for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																</td>
															</tr>
															<tr>
																<td><label><?php echo __("Send SMS to Admin","mp");?></label></td>
																<td colspan="2">
																	<div class="form-group">
																		<label class="toggle-large" for="moveto_twilio_admin_sms_notification_status">
																			<input <?php if($mp_settings->moveto_twilio_admin_sms_notification_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_twilio_admin_sms_notification_status" id="moveto_twilio_admin_sms_notification_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																		
																		</label>
																	</div>	
																	<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to Admin for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																</td>
															</tr>
															<tr>
																<td><label><?php echo __("Admin Phone Number","mp");?></label></td>
																<td colspan="2">
																	<div class="input-group moveto_twillio_cd">
																		 <!--<span class="input-group-addon"><span class="">+1</span></span>-->
																		 <input type="text" class="form-control" name="moveto_twilio_admin_phone_no" id="moveto_twilio_admin_phone_no" value="<?php echo $mp_settings->moveto_twilio_admin_phone_no;?>" />
																		 <input type="hidden" id="moveto_twilio_ccode_alph" value="<?php echo $mp_settings->moveto_twilio_ccode_alph;?>" />
																		 <input type="hidden" id="moveto_twilio_ccode" value="<?php echo $mp_settings->moveto_twilio_ccode;?>" />
																		 
																	</div>	
																</td>
															</tr>
															<tr>
																<td id="hr"></td><td id="hr"></td><td id="hr"></td>
															</tr>
														</tbody>
													</table>
												</div>	
												</div>	
												</div>
												
												<!-- Plivo Settings -->
												<div class="panel panel-default mp-sms-gateway">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Plivo SMS Gateway","mp");?><img class="mp-sms-gateway-img" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/plivo-logo.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for="moveto_sms_noti_plivo">
																	<input <?php if($mp_settings->moveto_sms_noti_plivo=='E'){echo "checked='checked'";} ?> type="checkbox" class="mp-toggle-sh" id="moveto_sms_noti_plivo" name="moveto_sms_noti_plivo"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
										
												<div id="collapseOne" class="panel-collapse collapse_moveto_sms_noti_plivo <?php if($mp_settings->moveto_sms_noti_plivo=='D'){ echo 'hide-div';} ?> ">
													<div class="panel-body padding-15">
														<table class="form-inline table mp-common-table table-hover table-bordered table-striped">
															<tr><th colspan="3"><?php echo __("Plivo Account Settings","mp");?></th></tr>
															<tbody>
																<tr>
																	<td><label><?php echo __("Account SID","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="text" class="form-control" name="moveto_plivo_sid" id="moveto_plivo_sid" size="70" value="<?php echo $mp_settings->moveto_plivo_sid;?>"/>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Plivo Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Auth Token","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="password" class="form-control" name="moveto_plivo_auth_token"
																			id="moveto_plivo_auth_token" size="70" value="<?php echo $mp_settings->moveto_plivo_auth_token; ?>" />
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Plivo Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Plivo Sender Number","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="text" class="form-control" name="moveto_plivo_number" id="moveto_plivo_number" size="70" value="<?php echo $mp_settings->moveto_plivo_number;?>" />
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Must be a valid number associated with your Plivo account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td id="hr"></td><td id="hr"></td><td id="hr"></td>
																</tr>
															</tbody>
															
															<tbody>
															
															<th colspan="3"><?php echo __("Plivo SMS Settings","mp");?></th>
																
																<tr>
																	<td><label><?php echo __("Send SMS to Client","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group">
																			<label class="toggle-large" for="moveto_plivo_client_sms_notification_status">
																				<input <?php if($mp_settings->moveto_plivo_client_sms_notification_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_plivo_client_sms_notification_status" id="moveto_plivo_client_sms_notification_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																			</label>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to client for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Send SMS to Admin","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group">
																			<label class="toggle-large" for="moveto_plivo_admin_sms_notification_status">
																				<input <?php if($mp_settings->moveto_plivo_admin_sms_notification_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_plivo_admin_sms_notification_status" id="moveto_plivo_admin_sms_notification_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																			</label>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to Admin for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Admin Phone Number","mp");?></label></td>
																	<td colspan="2">
																		<div class="input-group moveto_plivo_cd">
																			<!--<span class="input-group-addon"><span class="">+1</span></span>-->
																			<input type="text" class="form-control" name="moveto_plivo_admin_phone_no" id="moveto_plivo_admin_phone_no" value="<?php echo $mp_settings->moveto_plivo_admin_phone_no;?>" />
																									
																			<input type="hidden" id="moveto_plivo_ccode_alph" value="<?php echo $mp_settings->moveto_plivo_ccode_alph;?>" />
																			<input type="hidden" id="moveto_plivo_ccode" value="<?php echo $mp_settings->moveto_plivo_ccode;?>" />
																			
																		</div>	
																	</td>
																</tr>
																<tr>
																	<td id="hr"></td><td id="hr"></td><td id="hr"></td>
																</tr>
															</tbody>
														</table>
													</div>	
												</div>	
												</div>
											<!-- NEXMO  -->
												<div class="panel panel-default mp-sms-gateway">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Nexmo SMS Gateway","mp");?><img class="mp-sms-gateway-img" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/nexmo_logo.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for="moveto_sms_noti_nexmo">
																	<input <?php if($mp_settings->moveto_sms_noti_nexmo=='E'){echo "checked='checked'";} ?> type="checkbox" class="mp-toggle-sh" id="moveto_sms_noti_nexmo" name="moveto_sms_noti_nexmo"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
										
												<div id="collapseOne" class="panel-collapse collapse_moveto_sms_noti_nexmo <?php if($mp_settings->moveto_sms_noti_nexmo=='D'){ echo 'hide-div';} ?> ">
													<div class="panel-body padding-15">
														<table class="form-inline table mp-common-table table-hover table-bordered table-striped">
															<tr><th colspan="3"><?php echo __("Nexmo Account Settings","mp");?></th></tr>
															<tbody>
																<tr>
																	<td><label><?php echo __("Nexmo API Key","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="text" class="form-control" name="moveto_nexmo_apikey" id="moveto_nexmo_apikey" size="70" value="<?php echo $mp_settings->moveto_nexmo_apikey;?>"/>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Nexmo Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Nexmo API Secret","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="password" class="form-control" name="moveto_nexmo_api_secret"
																			id="moveto_nexmo_api_secret" size="70" value="<?php echo $mp_settings->moveto_nexmo_api_secret; ?>" />
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Nexmo Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Nexmo From","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="text" class="form-control" name="moveto_nexmo_form" id="moveto_nexmo_form" size="70" value="<?php echo $mp_settings->moveto_nexmo_form;?>" />
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Must be a valid number associated with your Nexmo account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td id="hr"></td><td id="hr"></td><td id="hr"></td>
																</tr>
															</tbody>
															
															<tbody>
															
															
																
																<tr>
																	<td><label><?php echo __("Nexmo Send Sms To Client Status","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group">
																			<label class="toggle-large" for="moveto_nexmo_send_sms_client_status">
																				<input <?php if($mp_settings->moveto_nexmo_send_sms_client_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_nexmo_send_sms_client_status" id="moveto_nexmo_send_sms_client_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																			</label>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to client for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Nexmo Send Sms To admin Status","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group">
																			<label class="toggle-large" for="moveto_nexmo_send_sms_admin_status">
																				<input <?php if($mp_settings->moveto_nexmo_send_sms_admin_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_nexmo_send_sms_admin_status" id="moveto_nexmo_send_sms_admin_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																			</label>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to Admin for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Nexmo Admin Phone Number","mp");?></label></td>
																	<td colspan="2">
																		<div class="input-group moveto_nexmo_cd">
																			<!--<span class="input-group-addon"><span class="">+1</span></span>-->
																			<input type="text" class="form-control" name="moveto_nexmo_admin_phone_no" id="moveto_nexmo_admin_phone_no" value="<?php echo $mp_settings->moveto_nexmo_admin_phone_no;?>" />
																			
																			<input type="hidden" id="moveto_nexmo_ccode_alph" value="<?php echo $mp_settings->moveto_nexmo_ccode_alph;?>" />
																			<input type="hidden" id="moveto_nexmo_ccode" value="<?php echo $mp_settings->moveto_nexmo_ccode;?>" />
																		</div>	
																	</td>
																</tr>
																<tr>
																	<td id="hr"></td><td id="hr"></td><td id="hr"></td>
																</tr>
															</tbody>
														</table>
													</div>	
												</div>	
												</div>
											<!-- nexmo end -->	
											<!-- Textlocal Settings -->
												<div class="panel panel-default mp-sms-gateway">
													<div class="panel-heading">
														<h4 class="panel-title">
															<span><?php echo __("Textlocal SMS Gateway","mp");?><img class="mp-sms-gateway-img" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/textlocal-logo.png" />
															</span>
															<div class="mp-enable-disable-right pull-right">
																<label class="toggle-large" for="moveto_sms_noti_textlocal">
																	<input <?php if($mp_settings->moveto_sms_noti_textlocal=='E'){echo "checked='checked'";} ?> type="checkbox" class="mp-toggle-sh" id="moveto_sms_noti_textlocal" name="moveto_sms_noti_textlocal"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																</label>
															</div>
														</h4>
													</div>
										
												<div id="collapseOne" class="panel-collapse collapse_moveto_sms_noti_textlocal <?php if($mp_settings->moveto_sms_noti_textlocal=='D'){ echo 'hide-div';} ?> ">
													<div class="panel-body padding-15">
														<table class="form-inline table mp-common-table table-hover table-bordered table-striped">
															<tr><th colspan="3"><?php echo __("Textlocal Account Settings","mp");?></th></tr>
															<tbody>
																<tr>
																	<td><label><?php echo __("Account API Key","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="text" class="form-control" name="moveto_textlocal_apikey" id="moveto_textlocal_apikey" size="70" value="<?php echo $mp_settings->moveto_textlocal_apikey;?>"/>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Textlocal Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Sender Name","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group mp-lgf">
																			<input type="text" class="form-control" name="moveto_textlocal_sender"
																			id="moveto_textlocal_sender" size="70" value="<?php echo $mp_settings->moveto_textlocal_sender; ?>" />
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Available from within your Textlocal Account.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td id="hr"></td><td id="hr"></td><td id="hr"></td>
																</tr>
															</tbody>
															
															<tbody>
															
															<th colspan="3"><?php echo __("Textlocal SMS Settings","mp");?></th>
																
																<tr>
																	<td><label><?php echo __("Send SMS To Client","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group">
																			<label class="toggle-large" for="moveto_textlocal_client_sms_notification_status">
																				<input <?php if($mp_settings->moveto_textlocal_client_sms_notification_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_textlocal_client_sms_notification_status" id="moveto_textlocal_client_sms_notification_status"  data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																			</label>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to Client for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Send SMS To Admin","mp");?></label></td>
																	<td colspan="2">
																		<div class="form-group">
																			<label class="toggle-large" for="moveto_textlocal_admin_sms_notification_status">
																				<input <?php if($mp_settings->moveto_textlocal_admin_sms_notification_status=='E'){echo "checked='checked'";} ?> type="checkbox" name="moveto_textlocal_admin_sms_notification_status" id="moveto_textlocal_admin_sms_notification_status" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" />
																			</label>
																		</div>	
																		<a class="mp-tooltip-link" href="#" data-toggle="tooltip" title="<?php echo __("Enable or Disable, Send SMS to admin for Quote Requests lead info.","mp");?>"><i class="fa fa-info-circle fa-lg"></i></a>
																	</td>
																</tr>
																<tr>
																	<td><label><?php echo __("Admin Phone Number","mp");?></label></td>
																	<td colspan="2">
																		<div class="input-group moveto_textlocal_cd">
																			<!--<span class="input-group-addon"><span class="">+1</span></span>-->
																			<input type="text" class="form-control" name="moveto_textlocal_admin_phone_no" id="moveto_textlocal_admin_phone_no" value="<?php echo $mp_settings->moveto_textlocal_admin_phone_no;?>" />
																			<input type="hidden" id="moveto_textlocal_ccode_alph" value="<?php echo $mp_settings->moveto_textlocal_ccode_alph;?>" />
																			<input type="hidden" id="moveto_textlocal_ccode" value="<?php echo $mp_settings->moveto_textlocal_ccode;?>" />
																		</div>	
																	</td>
																</tr>
																<tr>
																	<td id="hr"></td><td id="hr"></td><td id="hr"></td>
																</tr>
															</tbody>
														</table>
													</div>	
												</div>	
												</div>		
												<!--Textlocal End -->
											</div>
										</div>
									</div>
								</div>
									
								<a id="mp_update_smssettings" name="mp_update_smssettings" class="btn btn-success mt-10 ml-15" href="javascript:void(0)"><?php echo __("Save Setting","mp");?></a>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="tab-pane mp-toggle-abs" id="sms-template">
				<div class="panel panel-default wf-100">
					<div class="panel-heading">
						<h1 class="panel-title"><?php echo __("SMS Template Settings","mp");?></h1>
					</div>
					<!-- Client sms templates -->
					<ul class="nav nav-tabs nav-justified">
						<li class="active"><a data-toggle="tab" href="#client-sms-template"><?php echo __("Client SMS Templates","mp");?></a></li>
						<!--<li><a data-toggle="tab" href="#service-provider-sms-template"><?php //echo __("Service Provider SMS Template","mp");?></a></li>-->
						<li><a data-toggle="tab" href="#admin-manager-sms-template"><?php echo __("Admin/Manager SMS Template","mp");?></a></li>
						
					</ul>
					<div class="tab-content">
						<div id="client-sms-template" class="tab-pane fade in active">
							<h3><?php echo __("Client SMS Templates","mp");?></h3>
								<div id="accordion" class="panel-group">
									<?php $mp_sms_templates->user_type='C';
									$AM_templates = $mp_sms_templates->readall_by_usertype();
									foreach($AM_templates as $AM_template){ ?>
									
									<div class="panel panel-default mp-sms-panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="mp-col11">
													<div class="mp-yes-no-sms-right pull-left">
														<label for="sms_template_status<?php echo $AM_template->id;?>">
															<input <?php if($AM_template->sms_template_status=='e'){echo "checked='checked'";} ?> class="mp_update_smsstatus" type="checkbox" id="sms_template_status<?php echo $AM_template->id;?>" data-eid="<?php echo $AM_template->id;?>" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
														</label>
													</div>
													<span id="sms_subject_label<?php echo $AM_template->id;?>" class="mp-template-name"><?php echo $AM_template->sms_subject;?></span>
														
												</div>	
												<div class="pull-right mp-col1">
													<div class="pull-right">
														<div class="mp-show-hide pull-right">
															<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox" id="sms<?php echo $AM_template->id;?>" data-id="<?php echo $AM_template->id;?>">
															<label class="mp-show-hide-label" for="sms<?php echo $AM_template->id;?>"></label>
														</div>
													</div>
												</div>
											</h4>
										</div>
										<div class="panel-collapse collapse smstemplatedetail smsdetail_<?php echo $AM_template->id;?>">
											<div class="panel-body">
												<div class="mp-sms-temp-collapse-div col-md-12 col-lg-12 col-xs-12 np">
													<form id="" method="post" type="" class="slide-toggle" >
														<div class="col-md-8 col-sm-8 col-xs-12 form-group">
															<label><?php echo __("SMS Content","mp");?></label>
															<?php
															if($AM_template->sms_message!=''){
															$content=stripslashes_deep($AM_template->sms_message);
															}else{
															$content=stripslashes_deep($AM_template->default_message);
															}
															$editorName=  'sms_message'.$AM_template->id;
															$editorId ='sms_editor'.$AM_template->id;
															wp_editor($content,$editorId, array('textarea_name'=>$editorName,'media_buttons'=>true, 'teeny'=>false, 'tinymce' => false,'editor_class'=>'ak_wp_editor','wpautop' => true,'tabindex' => '','tabfocus_elements' => ':prev,:next','dfw' => false,'quicktags' => true)); ?>
															
															<a data-eid="<?php echo $AM_template->id;?>" class="btn btn-success mp-btn-width pull-left cb ml-15 mt-20 mp_save_smstemplate" type="submit"><?php echo __("Save Template","mp");?></a>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12">
															<div class="mp-sms-content-tags">
																<b><?php echo __("Tags","mp");?> </b><br />
																<?php foreach($sms_template_tags as $tags){
																		
																		echo "<a data-eid='".$AM_template->id."' class='tags' data-value='".$tags."'>".$tags."</a><br/>";
																	} ?>
																<br />
															</div>
														</div>
														
													</form>	
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>								
						</div>
						<div id="service-provider-sms-template" class="tab-pane fade">
							<h3><?php echo __("Service Provider SMS Template","mp");?></h3>
							<div id="accordion" class="panel-group">
									<?php $mp_sms_templates->user_type='SP';
									$AM_templates = $mp_sms_templates->readall_by_usertype();
									foreach($AM_templates as $AM_template){ ?>
									
									<div class="panel panel-default mp-sms-panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="mp-col11">
													<div class="mp-yes-no-sms-right pull-left">
														<label for="sms_template_status<?php echo $AM_template->id;?>">
															<input <?php if($AM_template->sms_template_status=='e'){echo "checked='checked'";} ?> class="mp-toggle-input mp_update_smsstatus" type="checkbox" id="sms_template_status<?php echo $AM_template->id;?>" data-eid="<?php echo $AM_template->id;?>" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
														</label>
													</div>
													
													<span id="sms_subject_label<?php echo $AM_template->id;?>" class="mp-template-name"><?php echo $AM_template->sms_subject;?></span>
														
												</div>	
												<div class="pull-right mp-col1">
													<div class="pull-right">
														<div class="mp-show-hide pull-right">
															<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox" id="sms<?php echo $AM_template->id;?>"  data-id="<?php echo $AM_template->id;?>"><!--Added Serivce Id-->
															<label class="mp-show-hide-label" for="sms<?php echo $AM_template->id;?>"></label>
														</div>
													</div>
												</div>
											</h4>
										</div>
										<div class="panel-collapse collapse smstemplatedetail smsdetail_<?php echo $AM_template->id;?>">
											<div class="panel-body">
												<div class="mp-sms-temp-collapse-div col-md-12 col-lg-12 col-xs-12 np">
													<form id="" method="post" type="" class="slide-toggle" >
														<div class="col-md-8 col-sm-8 col-xs-12 form-group">
															<label><?php echo __("SMS Content","mp");?></label>
															<?php
															if($AM_template->sms_message!=''){
															$content=stripslashes_deep($AM_template->sms_message);
															}else{
															$content=stripslashes_deep($AM_template->default_message);
															}
															$editorName=  'sms_message'.$AM_template->id;
															$editorId ='sms_editor'.$AM_template->id;
															wp_editor($content,$editorId, array('textarea_name'=>$editorName,'media_buttons'=>true, 'teeny'=>false, 'tinymce' => false,'editor_class'=>'ak_wp_editor','wpautop' => true,'tabindex' => '','tabfocus_elements' => ':prev,:next','dfw' => false,'quicktags' => true)); ?>
															
															<a data-eid="<?php echo $AM_template->id;?>" class="btn btn-success mp-btn-width pull-left cb ml-15 mt-20 mp_save_smstemplate" type="submit"><?php echo __("Save Template","mp");?></a>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12">
															<div class="mp-sms-content-tags">
																<b><?php echo __("Tags","mp");?> </b><br />
																<?php foreach($sms_template_tags as $tags){
																		
																		echo "<a data-eid='".$AM_template->id."' class='tags' data-value='".$tags."'>".$tags."</a><br/>";
																	} ?>
																<br />
															</div>
														</div>
														
													</form>	
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>	
						</div>
						
						<div id="admin-manager-sms-template" class="tab-pane fade">
							<h3><?php echo __("Admin/Manager Provider SMS Template","mp");?></h3>
							<div id="accordion" class="panel-group">
									<?php $mp_sms_templates->user_type='AM';
									$AM_templates = $mp_sms_templates->readall_by_usertype();
									foreach($AM_templates as $AM_template){ ?>
									
									<div class="panel panel-default mp-sms-panel">
										<div class="panel-heading">
											<h4 class="panel-title">
												<div class="mp-col11">
													<div class="mp-yes-no-sms-right pull-left">
														<label for="sms_template_status<?php echo $AM_template->id;?>">
															<input <?php if($AM_template->sms_template_status=='e'){echo "checked='checked'";} ?> class="mp-toggle-input mp_update_smsstatus" type="checkbox" id="sms_template_status<?php echo $AM_template->id;?>" data-eid="<?php echo $AM_template->id;?>" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
														</label>
													</div>
													<span id="sms_subject_label<?php echo $AM_template->id;?>" class="mp-template-name"><?php echo $AM_template->sms_subject;?></span>
														
												</div>	
												<div class="pull-right mp-col1">
													<div class="pull-right">
														<div class="mp-show-hide pull-right">
															<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox" id="sms<?php echo $AM_template->id;?>" data-id="<?php echo $AM_template->id;?>"><!--Added Serivce Id-->
															<label class="mp-show-hide-label" for="sms<?php echo $AM_template->id;?>"></label>
														</div>
													</div>
												</div>
											</h4>
										</div>
										<div class="panel-collapse collapse smstemplatedetail smsdetail_<?php echo $AM_template->id;?>">
											<div class="panel-body">
												<div class="mp-sms-temp-collapse-div col-md-12 col-lg-12 col-xs-12 np">
													<form id="" method="post" type="" class="slide-toggle" >
														<div class="col-md-8 col-sm-8 col-xs-12 form-group">
															<label><?php echo __("SMS Content","mp");?></label>
															<?php
															if($AM_template->sms_message!=''){
															$content=stripslashes_deep($AM_template->sms_message);
															}else{
															$content=stripslashes_deep($AM_template->default_message);
															}
															$editorName=  'sms_message'.$AM_template->id;
															$editorId ='sms_editor'.$AM_template->id;

															wp_editor($content,$editorId, array('textarea_name'=>$editorName,'media_buttons'=>true, 'teeny'=>false, 'tinymce' => false,'editor_class'=>'ak_wp_editor','wpautop' => true,'tabindex' => '','tabfocus_elements' => ':prev,:next','dfw' => false,'quicktags' => true)); ?>
															
															<a data-eid="<?php echo $AM_template->id;?>" class="btn btn-success mp-btn-width pull-left cb ml-15 mt-20 mp_save_smstemplate" type="submit"><?php echo __("Save Template","mp");?></a>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-12">
															<div class="mp-sms-content-tags">
																<b><?php echo __("Tags","mp");?> </b><br />
																<?php foreach($sms_template_tags as $tags){
																		
																		echo "<a data-eid='".$AM_template->id."' class='tags' data-value='".$tags."'>".$tags."</a><br/>";
																	} ?>
																<br />
															</div>
														</div>
														
													</form>	
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>	
						  </div>
					</div>
				</div>
			</div>
			
			<div class="tab-pane mp-toggle-abs" id="labels">
				<form id="" method="post" type="" class="mp-labels-settings" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("Labels","mp");?></h1>
						</div>
						<div class="panel-body">
							<div class="table-responsive"> 
								<table class="form-inline mp-common-table table table-hover table-bordered table-striped">
									<tbody>
										<tr><th colspan="3"><?php echo __("Appointkart Frontend First Step Labels","mp");?></th></tr>
										<tr>
											<th><?php echo __("Original Label","mp");?></th>
											<th><?php echo __("Custom Label","mp");?></th>
										</tr>
										<tr>
											<td><?php echo __("Choose Service","mp");?></td>
											<td><div class="form-group">
												<input class="form-control" type="text" name="" value="<?php echo __("Choose Date,Time and Provider","mp");?>" />
												</div>
											</td>
										</tr>
										<tr>
											<td><?php echo __("Choose Service","mp");?></td>
											<td><div class="form-group">
												<input class="form-control" type="text" name="" value="<?php echo __("Choose Date,Time and Provider","mp");?>" />
												</div>
											</td>
										</tr>
										<tr><th colspan="3"><?php echo __("Appointkart Frontend First Step Labels","mp");?></th></tr>
										<tr>
											<td><?php echo __("Your Quote Requests","mp");?></td>
											<td><div class="form-group">
												<input class="form-control" type="text" name="" value="<?php echo __("Your Quote Requests","mp");?>" />
												</div>
											</td>
										</tr>
										<tr>
											<td><?php echo __("Total","mp");?></td>
											<td><div class="form-group">
												<input class="form-control" type="text" name="" value="<?php echo __("Total","mp");?>" />
												</div>
											</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											
											<td colspan="3">
												<button id="" name="" class="btn btn-success" type="submit"><?php echo __("Save Setting","mp");?></a>
												<button type="reset" class="btn btn-default ml-30"><?php echo __("Reset","mp");?></button>
									
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="tab-pane mp-toggle-abs" id="custom-form-fields">
				<div id="" class="mp-custom-form-fields" >
					<div class="panel panel-default">
						<div class="panel-heading">
							<h1 class="panel-title"><?php echo __("Custom Form Fields","mp");?></h1>
						</div>
						<div class="panel-body">
							<!--  <form action="">
								<textarea name="form-builder-template" id="form-builder-template" cols="30" rows="10"></textarea>
							  </form> -->
							  
								<div class="build-wrap"></div>
								<div class="render-wrap"></div>
								<!--<button id="edit-form">Edit Form</button>-->
								

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(get_option('moveto_payment_method_Paypal')=='E' || get_option('moveto_payment_method_Stripe')=='E' || get_option('moveto_payment_method_Authorizenet')=='E'|| get_option('moveto_payment_method_2Checkout')=='E') {
		$any_payment_method_enable = 'E';
} else {
		$any_payment_method_enable = 'D';
}
if(get_option('default_company_country_flag') != ''){
	$default_flag = get_option('default_company_country_flag');
}else{
	$default_flag = "us";
}
?>	
<script>
var general_setting_pd_ed={"payment_gateway_status":"<?php echo $any_payment_method_enable; ?>"};
var general_settings_ajax_path={"ajax_path":"<?php echo $plugin_url_for_ajax; ?>"};
var general_settings_default_flag={"default_flag":"<?php echo $default_flag; ?>"};
var input = document.getElementById('moveto_company_origin_address');
options = {
		  componentRestrictions: {country: []}
		 };
google.maps.event.addDomListener(window, 'load', function () {
		    new google.maps.places.Autocomplete(document.getElementById('moveto_company_origin_address'));
		    new google.maps.places.Autocomplete(document.getElementById('moveto_company_destination_address'));
		});
</script>