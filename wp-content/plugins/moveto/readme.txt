﻿=== Moveto - A premium appointment lead tool ===
Contributors: Skymoonlabs Tags: appointment lead,Personal

TrainerRequires at least: 4.0

Tested up to: 4.9.5

License: GPLv3License 

URI: http://www.gnu.org/licenses/gpl-3.0.html 

An appointment lead tool, easy to use and with lots of features 

== Description ==
Moveto is very easy to use appointment lead tool for almost all type of services like  Home,Office,Vehicle,Pets,Commercial,etc.


== Installation ==
1. Upload plugin files to the "/wp-content/plugins/" directory.
2. Activate the plugin through the "Plugins" menu in WordPress.
3. Place "do_action( 'moveto' );" in your templates Or use [moveto] as short code in your posts or pages.= Configuration =After easy installation of this plugin you can add articles and routes, set availability. There are lots of cool features to configure in settings tab of admin panel.


== Changelog === 

1.0 =
* Initial release

= 1.1 =
* Icons CSS Improvements

= 2.0 =
* Added Distance Calculation through Google API
* Dynamic Pricing Added
* Article Packages Added
* All New User Interface

= 2.1 =
*Fixed issue on plugin activation

= 3.0 =
*Added Stripe,Paypal and Paytm Payment Gateway
*Added Tax Calculation
*Added Google Place Autocomplete
*Added Distance Calculation in Miles
*Improved Cart Design
*Client Manageable Google API Key
*First Step disable issue on going to second step solved
*Manageable Maximum Distance from Admin Side
*Some Design Improvements

= 3.1 =
*Integrated Google Maps
*Option for Google Placeautocomplete to limit it to country
*Manegeable Company to Source and Destination to Company Distance For Cost Calculation.
*Added Discount with Percentage and Flat rate.
*New Cart Design
*Some Design Improvements

= 4.0 =
*Added Calculation based on article volume
*Two types of volume- Cubic Feets and Cubic Metres
*Added Insurance Option Fully Manageable from admin panel.

= 4.1 =
*Design Improvements
*Added Paystack payment gateway
*Added Hide/Show price from summary
*Calculation issue fixed

