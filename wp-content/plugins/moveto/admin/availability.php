<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Location */
	$schedule_offdays = new moveto_schedule_offdays(); ?>
	
<div id="mp-staff-panel" class="panel tab-content">
	<div class="panel-body">
		<div class="mp-staff-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<!-- right side common menu for staff -->

			<div class="mp-staff-top-header">					
			</div>			
			<ul class="nav nav-tabs nav-justified mp-staff-right-menu">				
				<li><a href="#member-offdays" class="active" data-toggle="tab"><?php echo __("Off Days","mp");?></a></li>
			</ul>
			
			
			<div class="tab-pane active" id="demo-andrew"><!-- first staff nmember -->			
				<div class="container-fluid tab-content mp-staff-right-details">
					<div class="tab-pane active member-offdays" id="member-offdays">
						<div class="panel panel-default">
							<div class="panel-body">
							<input type="hidden" value="0" id="staff_offdays_id" />
							<?php
							/* Get Offdays Information */
							$schedule_offdays->provider_id = 0;
							$all_off_days = $schedule_offdays->read_all_offs_by_provider();
						  
							if(sizeof((array)$all_off_days)!=0) {
							  foreach($all_off_days as $trun){
								$arr_all_off_day [] = $trun->off_date;
							  }
							}
							
							
							$year_arr = array(date('Y'),date('Y')+1);
							$month_num=date('n');

							if(isset($_GET['y']) && in_array($_GET['y'],$year_arr)) {
							 $year = $_GET['y'];
							} else {
							 $year=date('Y');
							}

							$nextYear = date('Y')+1;
							$date=date('d');
							
							$month=array(__('January','mp'),__('February','mp'),__('March','mp'),__('April','mp'),__('May','mp'),__('June','mp'),__('July','mp'),__('August','mp'),__('September','mp'),__('October','mp'),__('November','mp'),__('December','mp'));


							echo '<table class="offdaystable">';
							echo '<th colspan=4 align=center><div style="margin-top:10px;"><span style="float:right;">'.date('Y').'</span></div></th>';

							for ($reihe=1; $reihe<=4; $reihe++) {
								echo '<tr>';
								for ($spalte=1; $spalte<=3; $spalte++) {
									$this_month=($reihe-1)*3+$spalte;
									$erster=date('w',mktime(0,0,0,$this_month,1,$year));
									$insgesamt=date('t',mktime(0,0,0,$this_month,1,$year));
									if($erster==0) $erster=7;
									echo '<td class="col-md-4 col-sm-4 col-lg-4 col-xs-12">';
									echo '<table align="center" class="table table-bordered table-striped monthtable">';?>
									<th colspan="7" align="center"><?php echo $month[$this_month-1];?>
									
									
									<div class="pull-right">
										<div class="mp-custom-checkbox">
											<ul class="mp-checkbox-list">
												<li>
													<input type="checkbox" class="fullmonthoff" id="<?php echo $year.'-'.$this_month;?>" <?php  $schedule_offdays->off_year_month=$year.'-'.$this_month;	if($schedule_offdays->check_full_month_off()==true) { echo " checked "; }  ?> />
													<label for="<?php echo $year.'-'.$this_month;?>"><?php echo __("Full Month","mp");?><span class="ml5r0"></span></label>
												</li>
											</ul>
										</div>
									</div>
									
									
									</th>
									<?php 
									echo '<tr><td><b>M</b></td><td><b>T</b></td>';
									echo '<td><b>W</b></td><td><b>T</b></td>';
									echo '<td><b>F</b></td><td class="sat"><b>S</b></td>';
									echo '<td class="sun"><b>S</b></td></tr>';
									echo '<tr class="dateline selmonth_'.$year.'-'.$this_month.'"><br>';
									$i=1;
									while ($i<$erster) {
										echo '<td> </td>';
										$i++;
									}
									$i=1;
									while ($i<=$insgesamt) {
										$rest=($i+$erster-1)%7;
										
										$cal_cur_date =  $year."-".sprintf('%02d', $this_month)."-".sprintf('%02d', $i);
										 
								
										
										if (($i==$date) && ($this_month==$month_num)) {
											
											if(isset($arr_all_off_day)  && in_array($cal_cur_date, $arr_all_off_day)) { 
											  echo '<td  id="'.$year.'-'.$this_month.'-'.$i.'"  class="selectedDate RR"  align=center>';
											} else {
											  echo '<td  id="'.$year.'-'.$this_month.'-'.$i.'"  class="date_single RR"  align=center>';
											}
										
										} else {
											if(isset($arr_all_off_day)  &&  in_array($cal_cur_date, $arr_all_off_day)) { 
											  echo '<td  id="'.$year.'-'.$this_month.'-'.$i.'"  class="selectedDate RR"  align=center>';
											} else {
											   echo '<td  id="'.$year.'-'.$this_month.'-'.$i.'" class="date_single RR"  align=center>';
											}
										}
										
										
										
										if (($i==$date) && ($this_month==$month_num)) {
											echo '<span style="color:#3d3d3d;">'.$i.'</span>';
										}	else if ($rest==6) {
											echo '<span   style="color:#0000cc;">'.$i.'</span>';
										} else if ($rest==0) {
											echo '<span  style="color:#cc0000;">'.$i.'</span>';
										} else {
											echo $i;
										}
										echo "</td>\n";
										if ($rest==0) echo "</tr>\n<tr class='dateline selmonth_".$year."-".$this_month."'>\n";
										$i++;
									}
									echo '</tr>';
									echo '</table>';
									echo '</td>';
								}
								echo '</tr>';
							}

							echo '</table>';
							?>
							</div>
						</div>
					</div>
				
				</div><!-- end first -->
			</div>			
		</div>
		
	</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var staffObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>