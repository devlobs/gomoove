<?php
session_start();
$_SESSION["cart_details"] = "";

if($_POST["action"] === "add_to_cart")
{
	$article_id = $_POST["article_id"];
	$cat_id = $_POST["cat_id"];
	$room_id = $_POST["room_id"];
	$qty = $_POST["qty"];

	$article_array = array(
		"room_id"=>$room_id,
		"cat_id"=>$cat_id,
		"article_id"=>$article_id,
		"qty"=>$qty
	);
	if($qty > 0)
	{
		$_SESSION["cart_details"]["article_".$room_id] = $article_array;
	}
	else
	{
		_SESSION["cart_details"] = "";
	}
	
}

?>