<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Location */
	$location = new moveto_location();
	$category = new moveto_category();
	$staff = new moveto_staff();
	$service = new moveto_service();
	$general = new moveto_general();
	$mp_image_upload= new moveto_image_upload();
	$mp_currency_symbol = get_option('moveto_currency_symbol');
	
	if(isset($_POST['mp_create_service'])){
		$service->color_tag = $_POST['color_tag'];
		$service->service_title = filter_var($_POST['service_title'], FILTER_SANITIZE_STRING);
		$service->service_description = filter_var($_POST['service_description'], FILTER_SANITIZE_STRING);
		$service->image = $_POST['service_image'];
		$service->service_category = $_POST['service_category'];
		$service->duration = ($_POST['service_duration_hrs']*60) + $_POST['service_duration_mins'];
		$service->amount = $_POST['service_price'];
		$service->offered_price = $_POST['offered_price'];
		$service->location_id = $_SESSION['mp_location'];
		$serice_id = $servicecreate = $service->create();
	}
	/* Get All Services */
	$service->location_id = $_SESSION['mp_location'];
	
	$service->id = $_GET['sid'];
	//echo $_GET['sid'];exit;
	$mp_service_addons = $service->readAll_addons();
	
	
	$service->id = $_GET['sid'];
	$service->readOne();
	$service_title = $service->service_title;
	
	
	 if(!empty($_POST['service_title']))
	{
		/*$service->addons_id = $_GET['sid'];
		$service->addons_location_id = $_SESSION['mp_location']; */
		/* $insert_addons = $service->insert_addons(); */
	}
	if(!empty($_POST['u_service_title']))
	{
		/*$service->addons_id = $_GET['sid'];
		$service->addons_location_id = $_SESSION['mp_location']; */
		$update_addons = $service->addon_update();
	}
?>
<input type="hidden" name="addon_service_id" id="addon_service_id" value="<?php echo $_GET['sid'];?>"></input>
<div id="mp-service-addon-panel" class="panel tab-content table-fixed">
	<div class="panel-body">
		<div class="mp-service-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<ul class="breadcrumb">
                <li><a style="cursor:pointer"  href="?page=services_submenu" class=""><?php echo $service_title; ?></a></li>
                <li><?php echo __("Addons","mp"); ?></li>
            </ul>
			<div class="mp-service-top-header">
				<a href="?page=services_submenu" class="btn btn-success"><i class="fa fa-angle-left icon-space "></i><?php echo __("Back To Services","mp");?></a>
				<div class="pull-right">
					<table>
						<tbody>
							<tr>
								<td>
									<button id="mp-add-new-service-addons" class="btn btn-success" value="add new service"><i class="fa fa-plus icon-space "></i><?php echo __("Create Addon Service","mp");?></button>
								</td>
							</tr>							
						</tbody>
					</table>
					</form>
				</div>
			</div>
			<div id="hr"></div>
			<div class="tab-pane active" id="">
				<div class="tab-content mp-services-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-services" > <!-- sortable-services -->
							<?php foreach($mp_service_addons as $mp_addon){ 
								$service->id = $mp_addon->id;	?>
							<li id="service_detail_<?php echo $mp_addon->id; ?>" class="panel panel-default mp-services-panel" >
							
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="col-lg-6 col-sm-7 col-xs-12 np">
											<div class="pull-left">
												<i class="fa fa-th-list"></i>
											</div>	
											<span class="mp-service-title-name f-letter-capitalize"><?php echo $mp_addon->addon_service_name; ?></span>
											
										</div>
										<div class="col-lg-6 col-sm-5 col-xs-12 np">
											<div class="col-lg-2 col-sm-2 col-xs-4 np">
												<span class="mp-service-price-main"><span><?php echo $mp_currency_symbol;?></span><?php echo $mp_addon->base_price; ?></span>
											</div>	
											<div class="col-lg-2 col-sm-2 col-xs-4 np">
												<label for="sevice-endis-<?php echo $mp_addon->id; ?>">
													<input data-id="<?php echo $mp_addon->id; ?>" type="checkbox" class="update_service_addon_status" id="sevice-endis-<?php echo $mp_addon->id; ?>" <?php if($mp_addon->status=='E'){echo 'checked'; } ?> data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" >
												</label>
											</div>
											<div class="pull-right">
												<div class="col-lg-2 col-sm-2 col-xs-4 np">
												<a data-poid="mp-popover-delete-service<?php echo $mp_addon->id; ?>" id="mp-delete-service<?php echo $mp_addon->id; ?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this Addon?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete Addon","mp");?>"></i></a>
													<div class="mp-popover" id="mp-popover-delete-service<?php echo $mp_addon->id; ?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<?php if($service->total_service_bookings()>0){?>
																		<span class="mp-popover-title"><?php echo __("Unable to delete service,having lead","mp");?></span>
																		<?php }else{?>		
																		<button data-id="<?php echo $mp_addon->id; ?>" value="Delete" class="btn btn-danger btn-sm mr-10 delete_addon" type="submit"><?php echo __("Yes","mp");?></button>
																		<button data-poid="mp-popover-addon<?php echo $mp_addon->id; ?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button><?php } ?>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $mp_addon->id; ?>" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="<?php echo $mp_addon->id; ?>"></label>
												</div>
											</div>
										</div>
										
									</h4>
								</div>
								<div id="" class="service_detail panel-collapse collapse detail-id_<?php echo $mp_addon->id; ?>">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-5 col-md-5 col-lg-5 col-xs-12">
											<form data-sid="<?php echo $mp_addon->id; ?>" id="mp_update_service_addon_<?php echo $mp_addon->id; ?>" method="post" type="" class="slide-toggle mp_update_service_addon" >
												<table class="mp-create-service-table">
													<tbody>
														<tr>
															<td><label for="mp-service-title<?php echo $mp_addon->id; ?>"><?php echo __("Article Title","mp");?></label></td>
															<td><input type="text" name="u_service_title" class="form-control" id="mp-service-title<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->addon_service_name; ?>" /></td>
														</tr>
														
														<tr>
															<td><label for="mp-service-desc"><?php echo __("Article Image","mp");?></label></td>
															<td>
																<div class="mp-service-image-uploader">
																	<img id="bdscad<?php echo $mp_addon->id; ?>addimage" src="<?php if($mp_addon->image==''){ echo $plugin_url_for_ajax.'/assets/images/addon.png';}else{
																	echo site_url()."/wp-content/uploads".$mp_addon->image;
																	}?>" class="mp-service-image br-100" height="100" width="100">
																	
																	<label <?php if($mp_addon->image==''){ echo "style='display:block'"; }else{ echo "style='display:none'"; } ?> for="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>" class="mp-service-img-icon-label show_image_icon_add<?php echo $mp_addon->id; ?>">
																		<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
																		<i class="pull-left fa fa-plus-circle fa-2x"></i>
																	</label>
																	<input data-us="bdscad<?php echo $mp_addon->id; ?>" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>"  />
																	<a  id="mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" <?php if($mp_addon->image==''){ echo "style='display:none;'";}else{ echo "style='display:block'"; }  ?> class="pull-left br-100 btn-danger mp-remove-service-img btn-xs mp_remove_image" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove Service Image","mp");?>"></i></a>
																	
																	
																	
																	<div id="popover-mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" style="display: none;">
																		<div class="arrow"></div>
																		<table class="form-horizontal" cellspacing="0">
																			<tbody>
																				<tr>
																					<td>
																						<a href="javascript:void(0)" value="Delete" data-mediaid="<?php echo $mp_addon->id; ?>" data-mediasection='service' data-mediapath="<?php echo $mp_addon->image;?>" data-imgfieldid="bdscad<?php echo $mp_addon->id;?>uploadedimg" class="btn btn-danger btn-sm mp_delete_image"><?php echo __("Yes","mp");?></a>
																						<a href="javascript:void(0)" id="popover-mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" class="btn btn-default btn-sm close_delete_popup" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>	
											<div id="mp-image-upload-popupbdscad<?php echo $mp_addon->id; ?>" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
												<div class="vertical-alignment-helper">
													<div class="modal-dialog modal-md vertical-align-center">
														<div class="modal-content">
															<div class="modal-header">
																<div class="col-md-12 col-xs-12">
																	<a data-us="bdscad<?php echo $mp_addon->id; ?>" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>" ><?php echo __("Crop & Save","mp");?></a>
																	<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
																</div>	
															</div>
															<div class="modal-body">
																<img id="mp-preview-imgbdscad<?php echo $mp_addon->id; ?>" />
															</div>
															<div class="modal-footer">
																<div class="col-md-12 np">
																	<div class="col-md-4 col-xs-12">
																		<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>filesize" name="filesize" />
																	</div>	
																	<div class="col-md-4 col-xs-12">	
																		<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>h" name="h" /> 
																	</div>
																	<div class="col-md-4 col-xs-12">	
																		<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>w" name="w" />
																	</div>
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>x1" name="x1" />
																	 <input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>y1" name="y1" />
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>x2" name="x2" />
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>y2" name="y2" />
																	<input id="bdscad<?php echo $mp_addon->id; ?>bdimagetype" type="hidden" name="bdimagetype"/>
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>bdimagename" name="bdimagename" value="" />
																	</div>
															</div>							
														</div>		
													</div>			
												</div>			
											</div>
											</td>
										<input name="image" id="bdscad<?php echo $mp_addon->id;?>uploadedimg" type="hidden" value="<?php echo $mp_addon->image;?>" />
														</tr>
														
														<tr>
															<td><label for="mp-service-price<?php echo $mp_addon->id; ?>"><?php echo __("Price","mp");?></label></td>
															<td>
																<div class="input-group">
																	<span class="input-group-addon"><?php echo $mp_currency_symbol;?></span>
																	<input name="u_service_price" id="mp-service-price<?php echo $mp_addon->id; ?>" type="text" class="form-control" placeholder="<?php /*echo __("Cancel","mp");*/?>US Dollar" value="<?php echo $mp_addon->base_price; ?>">
																</div>	
																<label id="mp-service-price<?php echo $mp_addon->id; ?>-error" class="error" for="mp-service-price<?php echo $mp_addon->id;?>" style="display:none"></label>
															</td>
														</tr>
														<tr>
														
															<td><label for="mp-service-maxqty"><?php echo __("Max Qty","mp");?></label></td>
															<td><input type="text" name="service_maxqty" class="form-control maxqty<?php echo $mp_addon->id; ?>" id="mp-service-addons-maxqty<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->maxqty; ?>" /></td>
														</tr>
													<tr>
														<td><label><?php echo __("Multiple Qty","mp");?></label></td>
													<td>
														<div class="form-group">
														<label for="service_addons_multiple_qty">
															<input type="checkbox" class="addon_multipleqty<?php echo $mp_addon->id;?>" <?php if ($mp_addon->multipleqty == 'Y') { echo"checked"; } ?> id="service_addons_multiple_qty" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" value="<?php echo $mp_addon->multipleqty; ?>"/>
													</label>
													</div>
												</td>
												</tr>
												<tr>
												<td></td>
													<td>
														<button data-service_id="<?php echo $mp_addon->id; ?>" name="" class="btn btn-success mp-btn-width update_service_addon" type="button"><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
													
													</tbody>
												</table>
											</form>
											
										</div>
										<div class="mp-service-collapse-div col-sm-7 col-md-7 col-lg-7 col-xs-12 mt-20">
										<h6 class="mp-right-header"><?php echo __("Service Addons price rules","mp");?></h6>
										<ul>
										<li>
										<label class="col-xs-12 col-sm-2" for="service_addons_price"><?php echo __("Price","mp");?></label>
											<div class="col-xs-4 col-sm-2 npl">
                                                <input class="form-control" placeholder="1" value="1" id="" type="text" readonly="readonly" /></div>
                                            <div class="col-xs-4 col-sm-2 npl" >
                                                <select class="form-control Addons_price_rules_select" id="">
                                                    <option selected="" readonly value="=">= </option>
                                                </select>
                                            </div>
											<div class="col-xs-4 col-sm-2 npl">
                                                <input class="pull-left form-control" readonly value="<?php echo $mp_addon->base_price; ?>" placeholder="<?php echo __("Price","mp");?> type="text" />
                                            </div>
										</li>
										</ul>
										<ul class="myaddonspricebyqty<?php echo $mp_addon->id; ?>">
										<?php
										$service->addon_service_id = $mp_addon->id;
                                        $idss = $_GET['sid'];
                                        $result = $service->readall_qty_addon();
										
										$count_addon = count((array)$result);
										if($count_addon > 0){
                                        foreach($result as $r) {
											
                                            ?>
										<li class="form-group myaddon-qty_price_row<?php echo $r->id; ?>">
										<form class="service_addon_pricing" id="myedtform_addonunits<?php echo $r->id; ?>">
										<label class="col-xs-12 col-sm-2" for="service_addons_price"><?php echo __("Qty","mp");?></label>
											<div class="col-xs-4 col-sm-2 npl">
                                                <input id="myedtqty_addon<?php echo $r->id; ?>" name="txtedtqtyaddons" class="form-control myloadedqty_addons<?php echo $r->id; ?>" placeholder="1" value="<?php echo $r->unit; ?>" type="text" />
											</div>
                                            <div class="col-xs-4 col-sm-2 npl">
                                                <select class="form-control Addons_price_rules_select myloadedrules_addons<?php echo $r->id; ?>">
                                                    <option <?php if ($r->rules == 'E'){ ?>selected<?php } ?>
                                                                    value="E">=
                                                    </option>
                                                    <option <?php if ($r->rules == 'G'){ ?>selected<?php } ?>
                                                                    value="G"> &gt; </option>
                                                </select>
                                            </div>
											<div class="col-xs-4 col-sm-2 npl">
                                                <input name="myedtpriceaddon" id="myedtprice_addon<?php echo $r->id; ?>" class="pull-left form-control myloadedprice_addons<?php echo $r->id; ?>" value="<?php echo $r->rate; ?>"  placeholder="<?php echo __("Price","mp");?>" type="text" />
                                            </div>
											<div class="col-xs-12 col-sm-3  pull-left np" >
                                                <a data-id="<?php echo $r->id; ?>" class="btn btn-success btn-circle mr-15 pull-left myloadedbtnsave_addons update-addon-rule" data-addon_service_id="<?php echo $mp_addon->id;?>"><i class="fa fa-save" title="<?php echo __("Update","mp");?>"></i></a>
											
                                                <a href="javascript:void(0);" data-id="<?php echo $r->id; ?>" class="btn btn-danger btn-circle pull-left delete-addon-rule myloadedbtndelete_addons">
												<i class="fa fa-trash"></i>
												</a>
											</div>
											</form>
										</li>
										<?php
                                        }}
                                        ?>
										<li class="form-group">
										<form class="add_addon_pricing" id="mynewaddedform_addonunits<?php echo $mp_addon->id; ?>">
										<label class="col-xs-12 col-sm-2" for="service_addons_price"><?php echo __("Qty","mp");?></label>
											<div class="col-xs-4 col-sm-2 npl">
                                                <input name="mynewssqtyaddon" id="mynewaddedqty_addon<?php echo $mp_addon->id; ?>" class="form-control mynewqty_addons<?php echo $mp_addon->id; ?>" type="text" />
											</div>
                                            <div class="col-xs-4 col-sm-2 npl">
                                                <select class="form-control Addons_price_rules_select mynewrules_addons<?php echo $mp_addon->id; ?>">
                                                        <option selected value="E">=</option>
                                                        <option value="G"> &gt; </option>
                                                </select>
                                            </div>
											<div class="col-xs-4 col-sm-2 npl">
                                                <input name="mynewsspriceaddon" id="mynewaddedprice_addon<?php echo $mp_addon->id; ?>" class="pull-left form-control mynewprice_addons<?php echo $mp_addon->id; ?>"  placeholder="<?php echo __("Price","mp");?>" type="text" />
                                            </div>
											<div class="col-xs-12 col-sm-3 pull-left npl" >
                                                <a href="javascript:void(0);" data-id="<?php echo $mp_addon->id; ?>" class="btn btn-success btn-circle pull-left mybtnaddnewqty_addon add-addon-price-rule form-group new-manage-price-list"><?php echo __("Add New","mp");?></a>
											</div>
											</form>
										</li>
										
										</ul>
										</div>
									</div>
								</div>
							</li>
							<?php } ?>
							<!-- add new service pop up -->
							<li>
							<div class="panel panel-default mp-services-panel mp-add-new-service-addons">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="mp-col6">
											<span class="mp-service-title-name"><?php echo __("Add New Addon Service","mp");?></span>		
										</div>
										<div class="pull-right mp-col6">					
											<div class="pull-right">
													<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addservice" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="addservice"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="" class="service_detail panel-collapse collapse in detail-id_addservice">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form id="mp_create_service_addon" method="post" type="" class="slide-toggle" >
												<table class="mp-create-service-table">
													<tbody>
														<tr>
															<td><label for="mp-service-title"><?php echo __("Article Title","mp");?></label></td>
															<td><input type="text" name="service_title" class="form-control" id="mp-service-addons-title" /></td>
														</tr>
														
														<tr>
															<td><label for="mp-service-desc"><?php echo __("Service Image","mp");?></label></td>
															<td>
																<div class="mp-service-image-uploader">
																	<img id="bdscadlocimage" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/service.png" class="mp-service-image br-100" height="100" width="100">
																	<label for="mp-upload-imagebdscad" class="mp-service-img-icon-label">
																		<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
																		<i class="pull-left fa fa-plus-circle fa-2x"></i>
																	</label>
																	<input data-us="bdscad" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdscad"  />
																	
																	<a style="display: none;" id="mp-remove-service-imagebdscad" class="pull-left br-100 btn-danger mp-remove-service-img btn-xs" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove service Image","mp");?>"></i></a>
																	<div id="popover-mp-remove-service-imagebdscad" style="display: none;">
																		<div class="arrow"></div>
																		<table class="form-horizontal" cellspacing="0">
																			<tbody>
																				<tr>
																					<td>
																						<a href="javascript:void(0)" id="" value="Delete" class="btn btn-danger btn-sm" type="submit"><?php echo __("Yes","mp");?></a>
																						<a href="javascript:void(0)" id="mp-close-popover-service-imagebdscad" class="btn btn-default btn-sm" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div><!-- end pop up -->
																</div>
										<div id="mp-image-upload-popupbdscad" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
											<div class="vertical-alignment-helper">
												<div class="modal-dialog modal-md vertical-align-center">
													<div class="modal-content">
														<div class="modal-header">
															<div class="col-md-12 col-xs-12">
																<a data-us="bdscad" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdscad"><?php echo __("Crop & Save","mp");?></a>
																<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
															</div>	
														</div>
														<div class="modal-body">
															<img id="mp-preview-imgbdscad" />
														</div>
														<div class="modal-footer">
															<div class="col-md-12 np">
																<div class="col-md-4 col-xs-12">
																	<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="bdscadfilesize" name="filesize" />
																</div>	
																<div class="col-md-4 col-xs-12">	
																	<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="bdscadh" name="h" /> 
																</div>
																<div class="col-md-4 col-xs-12">	
																	<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="bdscadw" name="w" />
																</div>
																<input type="hidden" id="bdscadx1" name="x1" />
																 <input type="hidden" id="bdscady1" name="y1" />
																<input type="hidden" id="bdscadx2" name="x2" />
																<input type="hidden" id="bdscady2" name="y2" />
																<input id="bdscadbdimagetype" type="hidden" name="bdimagetype"/>
																<input type="hidden" id="bdscadbdimagename" name="bdimagename" value="" />
															</div>
														</div>							
													</div>		
												</div>			
											</div>			
										</div>
										<input name="service_image" id="bdscaduploadedimg" type="hidden" value="" />						
															</td>
														</tr>
														<tr>
															<td><label for="mp-service-price"><?php echo __("Price","mp");?></label></td>
															<td>
																<div class="input-group">
																	<span class="input-group-addon"><?php echo $mp_currency_symbol;?></span>
																	<input type="text" name="service_addons_price" class="form-control" id="service_addons_price" placeholder="<?php echo __("US Dollar","mp");?>">
																</div>	
																<label id="service_addons_price-error" class="error" for="service_addons_price" style="display:none;"></label>
															</td>
														</tr>
														<tr>
															<td><label for="mp-service-maxqty"><?php echo __("Max Qty","mp");?></label></td>
															<td><input type="text" name="service_maxqty" class="form-control maxqty" id="mp-service-addons-maxqty" /></td>
														</tr>
													<tr>
														<td><label><?php echo __("Multiple Qty","mp");?></label></td>
													<td>
														<div class="form-group">
														<label for="service_addons_multiple_qty">
															<input type="checkbox" class="addon_multipleqty" id="service_addons_multiple_qty" data-toggle="toggle" data-size="small" data-on="<?php echo __("On","mp");?>" data-off="<?php echo __("Off","mp");?>" data-onstyle="success" data-offstyle="default" />
													</label>
													</div>
												</td>
												</tr>
													</tbody>
												</table>
											
										</div>
										<table class="col-sm-7 col-md-7 col-lg-7 col-xs-12 mt-20 mb-20">
											<tbody>
												<tr>
													<td></td>
													<td>
														<button id="mp_create_service_addons" name="mp_create_service_addons" class="btn btn-success mp-btn-width col-md-offset-4" type="button" ><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
											</tbody>
										</table>
										
										
										</form>									
									</div>
								</div>
							</div>
							</li>
							
							</ul>
						</div>	
					</div>
				</div>
			</div>
			
		</div>
			
	</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var serviceObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>