<?php
class moveto_size
{    
    /* object properties */
    public $id;
	public $name;
	public $status;
	public $room_type_id;
	
    
	/**
     * create room type table
     */ 
	function create_table_move_size() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_movesize';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(50) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			 `position` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	dbDelta($sql);     
			}
	}
	/* Real All room type */
	function readAll_movesize()
	{
		global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_movesize'." ORDER BY position ASC";
		$stmt = $wpdb->get_results($queryString); 
		return $stmt;
	}
	
	
	/* Add New room_type */
	function insert_movesize()
	{
		global $wpdb;	
		  $ins_mp_movesize = $wpdb->query("INSERT INTO ".$wpdb->prefix .'mp_movesize'." (name)values('".$this->name."')");
		 return $ins_mp_movesize;
	}
	/* update room_type */
	function movesize_update()
    {
        global $wpdb;
		$update_mp_movesize = $wpdb->query("UPDATE ".$wpdb->prefix."mp_movesize SET name='".$this->name."' WHERE	id = " . $this->id); 
		return $update_mp_movesize;		
    }
	/* delete room_type */
	function movesize_delete()
    {
        global $wpdb;
		$delete_article_category   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_movesize  WHERE id =" . $this->id);
        return $delete_article_category;
    }
	/* Sort movesize Position */
	function sort_movesize_position(){
		global $wpdb;
			 $stmt = $wpdb->query("UPDATE ".$wpdb->prefix."mp_movesize set position='".$this->position."' where id='".$this->id."';");
			 
			return $result;	
	}	



}
?>