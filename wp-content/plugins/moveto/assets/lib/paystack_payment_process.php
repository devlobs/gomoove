<?php
$curl = curl_init();


if(!session_id()) { @session_start(); }
	$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));	
if (file_exists($root.'/wp-load.php')) {
	require_once($root.'/wp-load.php');	
}	

if($_SERVER['REQUEST_METHOD'] === 'POST'){
			$_SESSION['paystack_post_session'] = json_encode($_POST);
}
		

$plugin_url = plugins_url('',  dirname(__FILE__));
$base =   dirname(dirname(dirname(__FILE__)));

$moveto_paystack_public_key = urlencode(get_option('moveto_paystack_public_key'));
$moveto_paystack_secret_key = urlencode(get_option('moveto_paystack_secret_key'));

$email = $_SESSION['first_step_data']['email'];
$amount = $_SESSION['final_amt'] * 100;


// url to go to after payment
	$callback_url = $base.'/assets/paystack_callback.php';  

	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => json_encode([
	'amount'=>$amount,
	'email'=>$email,
	'callback_url' => $callback_url
	]),
	CURLOPT_HTTPHEADER => [
	"authorization: Bearer ".$moveto_paystack_secret_key."", //replace this with your own test key
	"content-type: application/json",
	"cache-control: no-cache"

	],
	));

	$response = curl_exec($curl);
	print_r($response);
	$err = curl_error($curl);

	if($err){
		
		// there was an error contacting the Paystack API
		die('Curl returned error: ' . $err);
	
	}

	$tranx = json_decode($response, true);
	if(!$tranx['status']){
		
		// there was an error from the API
		print_r('API returned error: ' . $tranx['message']);
		
	}

	// comment out this line if you want to redirect the user to the payment page
	// redirect to page so User can pay
	// uncomment this line to allow the user redirect to the payment page
	//return  $tranx['data']['authorization_url'];
	
	/* header('Location: ' . $tranx['data']['authorization_url']); */

	return $tranx;
?>