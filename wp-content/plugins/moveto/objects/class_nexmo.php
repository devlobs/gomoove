<?php 
  class moveto_nexmo{  
	 var $moveto_nexmo_apikey; 	 
	 var $moveto_nexmo_api_secret; 
	 var $moveto_nexmo_form; 
     
	 public function send_nexmo_sms($phone,$moveto_nexmo_text) {
		 $nexmo_api_key=$this->moveto_nexmo_apikey;
		 $moveto_nexmo_api_secret=$this->moveto_nexmo_api_secret;
		 $moveto_nexmo_form=$this->moveto_nexmo_form;
		 $queryinfo = array('api_key' => $nexmo_api_key, 'api_secret' => $moveto_nexmo_api_secret, 'to' => $phone, 'from' => $moveto_nexmo_form, 'text' => $moveto_nexmo_text);
		$url = 'https://rest.nexmo.com/sms/json?' . http_build_query($queryinfo);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		return $response;
	 } 
  }
?>