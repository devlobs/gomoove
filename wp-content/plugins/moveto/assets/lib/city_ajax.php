<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	$city = new moveto_city();
	
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	
	$mp_currency_symbol = get_option('moveto_currency_symbol');

	
	
/* Update City Detail */		
if(isset($_POST['city_action'],$_POST['city_id']) && $_POST['city_action']=='update_city' && $_POST['city_id']!=''){
		$city->id= $_POST['city_id'];		
		$city->source_city= filter_var($_POST['source_city'], FILTER_SANITIZE_STRING);
		$city->destination_city= filter_var($_POST['destination_city'], FILTER_SANITIZE_STRING);
		$city->price= filter_var($_POST['price'], FILTER_SANITIZE_STRING);		
		$city->update(); 
}
	
/* Delete City Permanently */		
if(isset($_POST['city_action'],$_POST['city_id']) && $_POST['city_action']=='delete_city' && $_POST['city_id']!=''){
		$city->id= $_POST['city_id'];
		$city->delete(); 
}	

/* Check City Route */		
if(isset($_POST['action'],$_POST['source_city'],$_POST['destination_city']) && $_POST['action']=='check_route' && $_POST['source_city']!='' && $_POST['destination_city']!=''){
		$city->source_city= trim($_POST['source_city']);
		$city->destination_city= trim($_POST['destination_city']);
		echo $city->check_route(); 
}	
?>			

