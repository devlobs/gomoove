<?php
class moveto_cubicfeets
{
   	/**** Create Distance Table For Distance Menu****/
     
	function create_table_cubicfeets() {
		global $wpdb;

		$table_name = $wpdb->prefix .'mp_cubicfeets';

		if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
		
		$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`cubicfeets_range` int(11) NOT NULL,
				`cubicfeets_range_price` int(11) NOT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
		
		dbDelta($sql);     
				}
	}

	/*Insert Quickbid Data */

	function insert_cubicfeets(){
		global $wpdb;	
		$cubic_insert = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_cubicfeets (`cubicfeets_range`,`cubicfeets_range_price`)values('".$this->per_cfeets."','".$this->per_cfeets_rate."')");
		 return $cubic_insert;
	}

	/*Read All Data For Update Distance*/
	function readAll(){
		global $wpdb;
		$read_quickbid = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cubicfeets");	
		return $read_quickbid;
	}

	function update_cubicfeets(){
		global $wpdb;
		$quickbid_update = $wpdb->query("UPDATE ".$wpdb->prefix."mp_cubicfeets SET cubicfeets_range='".$this->u_per_cubic_range."',cubicfeets_range_price ='" . $this->u_per_cubic_rate . "' WHERE id = " . $this->id);
		return $quickbid_update;	
	}

	function delete_cubicfeets(){
		global $wpdb;
        $return = $wpdb->query("DELETE FROM  ".$wpdb->prefix."mp_cubicfeets WHERE id =" . $this->id);
	}

	function per_cubicfeets($rate){
		global $wpdb;
        $cubic_feets = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cubicfeets WHERE cubicfeets_range < $rate ORDER BY `cubicfeets_range` desc" );
		return $cubic_feets;
	}

	function cubicfeets_range(){
		global $wpdb;
        $cubic_feets_range = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cubicfeets ORDER BY `cubicfeets_range` desc" );
		return $cubic_feets_range;
	}

	/*function cubic_price_insert(){
		global $wpdb;	
		$cubic_insert = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_cubicfeets (`per_cubicfeets_price`,`identity_cubicfeets`)values('".$this->price."','".$this->identyfy."')");
		 return $cubic_insert;
	}*/

	/*function cubic_price_update(){
		global $wpdb;
		$quickbid_update = $wpdb->query("UPDATE ".$wpdb->prefix."mp_cubicfeets SET per_cubicfeets_price='".$this->price."' WHERE id = " . $this->identy_id);
		return $quickbid_update;	
	}*/

	/*function cubic_price_view(){
		global $wpdb;	
		$read_cubic_price = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cubicfeets WHERE identity_cubicfeets = 1");
		 return $read_cubic_price;
	}*/
} 
?>