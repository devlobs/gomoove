<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Article */
	$service = new moveto_service();
	$general = new moveto_general();
	$mp_image_upload= new moveto_image_upload();
	$mp_currency_symbol = get_option('moveto_currency_symbol');
	$uploads = wp_upload_dir();
	$default_vehicle_image = array();

	if ($dir = opendir($uploads['basedir']."/../plugins/moveto/assets/images/icons/article-icons/")) {
	while (false !== ($file = readdir($dir))) {
	if ($file != "." && $file != "..") {
	$default_vehicle_image[] = $file; 
	}
	}
	closedir($dir);
	}

	$cubic_meter = get_option('moveto_cubic_meter');
	if($cubic_meter == 'ft_3' )
	{
		$cubic_unit = 'Cubic Feet'; 
	}
	else
	{
		$cubic_unit = 'Cubic Meter'; 
	}

?>
<div id="mp-services-panel" class="panel tab-content table-fixed">
	
		
	<div id="mp-service-addon-panel" class="panel tab-content table-fixed">
	<div class="panel-body">
		<div class="mp-service-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<div class="mp-service-top-header">
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					<h2 class="m-0">Article</h2>
				</div>
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					<button id="mp-add-new-service-addons" class="btn btn-success pull-right" value="add new service"><i class="fa fa-plus icon-space "></i><?php echo __("Add Article","mp");?></button>
				</div>
			</div>
			<div id="hr"></div>
			<div class="tab-pane active" id="">
				<div class="tab-content mp-services-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-services" > <!-- sortable-services -->
							<?php 
							$mp_service_addons = $service->readAll_addons();							
							if(sizeof((array)$mp_service_addons)>0){
							foreach($mp_service_addons as $mp_addon){  ?>
							<li id="service_detail_<?php echo $mp_addon->id; ?>" class="panel panel-default mp-services-panel" >							
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="col-lg-9 col-sm-10 col-xs-12 np">
											<div class="pull-left">
												<i class="fa fa-th-list"></i>
											</div>	
											<span class="mp-service-title-name f-letter-capitalize"><?php echo $mp_addon->article_name; ?></span>
											
										</div>
										<div class="col-lg-3 col-sm-2 col-xs-12 np">	
												<div class="mp-col6 np">
												</div>
												<div class="mp-col4 np">
													<label for="sevice-endis-<?php echo $mp_addon->id; ?>">
														<input data-id="<?php echo $mp_addon->id; ?>" type="checkbox" class="update_article_status" id="sevice-endis-<?php echo $mp_addon->id; ?>" <?php if($mp_addon->status=='E'){echo 'checked'; } ?> data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" >
													</label>
												</div>								
											<div class="pull-right">
												
												<div class="mp-col1 np">
												<a data-poid="mp-popover-delete-service<?php echo $mp_addon->id; ?>" id="mp-delete-service<?php echo $mp_addon->id; ?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this Article?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete Addon","mp");?>"></i></a>
													<div class="mp-popover" id="mp-popover-delete-service<?php echo $mp_addon->id; ?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<?php /* if($service->total_service_bookings()>0){ */if(0>0){?>
																		<span class="mp-popover-title"><?php echo __("Unable to delete article,having lead","mp");?></span>
																		<?php }else{?>		
																		<button data-id="<?php echo $mp_addon->id; ?>" value="Delete" class="btn btn-danger btn-sm mr-10 delete_addon" type="submit"><?php echo __("Yes","mp");?></button>
																		<button data-poid="mp-popover-addon<?php echo $mp_addon->id; ?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button><?php } ?>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $mp_addon->id; ?>" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="<?php echo $mp_addon->id; ?>"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="" class="service_detail panel-collapse collapse detail-id_<?php echo $mp_addon->id; ?>">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form data-sid="<?php echo $mp_addon->id; ?>" id="mp_update_service_addon_<?php echo $mp_addon->id; ?>" method="post" type="" class="slide-toggle mp_update_service_addon" >
												<table class="mp-create-service-table">
													<tbody>
														<input type="hidden" name="addon_service_id" id="addon_service_id" value="1" />
														<tr>
															<td><label for="mp-service-title<?php echo $mp_addon->id; ?>"><?php echo __("Name","mp");?></label></td>
															<td><input type="text" name="u_service_title" class="form-control" id="mp-service-title<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->article_name; ?>" /></td>
														</tr>
														
														<!-- <tr>
														<td><label><?php echo __("Calculation Type","mp");?></label></td>
														<td>
														  <label>
															<input class="m-0" name="group1" id="calculation_type<?php echo $mp_addon->id; ?>" type="radio" value="percentage" <?php if($mp_addon->type=='P'){ echo 'checked';} ?> />
															<span>Percentage</span>
														  </label>
													   
														  <label>
															<input class="m-0" name="group1"  type="radio" value="flat" <?php if($mp_addon->type=='F'){ echo 'checked';} ?>  />
															<span>Flat</span>
														  </label>
														</td>
														</tr>
														<tr>
														
															<td><label for="mp-service-maxqty"><?php echo __("Price per unit","mp");?></label></td>
															<td><input type="text" name="service_maxqty" class="form-control maxqty<?php echo $mp_addon->id; ?>" id="mp-service-addons-maxqty<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->price; ?>" /></td>
														</tr> -->
														<tr>
														
															<td><label for="mp-service-cubf"><?php echo $cubic_unit; ?></label></td>
															<td><input type="text" name="service_cubic_feets" class="form-control cubicfeets<?php echo $mp_addon->id; ?>" id="mp-service-addons-cubicfeets<?php echo $mp_addon->id; ?>" value="<?php echo $mp_addon->cubicarea; ?>" /></td>
														</tr>
													
												<tr>
															<td><label for="mp-service-desc"><?php echo __("Image","mp");?></label></td>
															<td>
																<div class="mp-service-image-uploader">
																	<img id="bdscad<?php echo $mp_addon->id; ?>addimage" src="<?php if($mp_addon->image==''){ echo $plugin_url_for_ajax.'/assets/images/addon.png';}else{
																	echo site_url()."/wp-content/uploads".$mp_addon->image;
																	}?>" class="mp-service-image br-100" height="100" width="100">
																	
																	<label <?php if($mp_addon->image==''){ echo "style='display:block'"; }else{ echo "style='display:none'"; } ?> for="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>" class="mp-service-img-icon-label show_image_icon_add<?php echo $mp_addon->id; ?>">
																		<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
																		<i class="pull-left fa fa-plus-circle fa-2x"></i>
																	</label>
																	<input data-us="bdscad<?php echo $mp_addon->id; ?>" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>"  />
																	<a  id="mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" <?php if($mp_addon->image==''){ echo "style='display:none;'";}else{ echo "style='display:block'"; }  ?> class="pull-left br-100 btn-danger mp-remove-service-img btn-xs mp_remove_image" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove Service Image","mp");?>"></i></a>
																	
																	
																	
																	<div id="popover-mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" style="display: none;">
																		<div class="arrow"></div>
																		<table class="form-horizontal" cellspacing="0">
																			<tbody>
																				<tr>
																					<td>
																						<a href="javascript:void(0)" value="Delete" data-mediaid="<?php echo $mp_addon->id; ?>" data-mediasection='service' data-mediapath="<?php echo $mp_addon->image;?>" data-imgfieldid="bdscad<?php echo $mp_addon->id;?>uploadedimg" class="btn btn-danger btn-sm mp_delete_image"><?php echo __("Yes","mp");?></a>
																						<a href="javascript:void(0)" id="popover-mp-remove-service-imagebdscad<?php echo $mp_addon->id; ?>" class="btn btn-default btn-sm close_delete_popup" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div>
																</div>	
											<div id="mp-image-upload-popupbdscad<?php echo $mp_addon->id; ?>" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
												<div class="vertical-alignment-helper">
													<div class="modal-dialog modal-md vertical-align-center">
														<div class="modal-content">
															<div class="modal-header">
																<div class="col-md-12 col-xs-12">
																	<a data-us="bdscad<?php echo $mp_addon->id; ?>" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdscad<?php echo $mp_addon->id; ?>" ><?php echo __("Crop & Save","mp");?></a>
																	<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
																</div>	
															</div>
															<div class="modal-body">
																<img id="mp-preview-imgbdscad<?php echo $mp_addon->id; ?>" />
															</div>
															<div class="modal-footer">
																<div class="col-md-12 np">
																	<div class="col-md-4 col-xs-12">
																		<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>filesize" name="filesize" />
																	</div>	
																	<div class="col-md-4 col-xs-12">	
																		<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>h" name="h" /> 
																	</div>
																	<div class="col-md-4 col-xs-12">	
																		<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="bdscad<?php echo $mp_addon->id; ?>w" name="w" />
																	</div>
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>x1" name="x1" />
																	 <input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>y1" name="y1" />
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>x2" name="x2" />
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>y2" name="y2" />
																	<input id="bdscad<?php echo $mp_addon->id; ?>bdimagetype" type="hidden" name="bdimagetype"/>
																	<input type="hidden" id="bdscad<?php echo $mp_addon->id; ?>bdimagename" name="bdimagename" value="" />
																	</div>
															</div>							
														</div>		
													</div>			
												</div>			
											</div>
											</td>
										<input name="image" id="bdscad<?php echo $mp_addon->id;?>uploadedimg" type="hidden" value="<?php echo $mp_addon->image;?>" />

										<td>
											
										</td>
										<td>
															<div class="btn-group">
<?php 
$image_name_expload = explode(".",$mp_addon->predefinedimage);
$image_name_with_dash = $image_name_expload[0];
$full_image_name = $mp_addon->predefinedimage;
?>
<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
<img class="vehical_image<?php echo sanitize_text_field($mp_addon->id); ?>" data-image_name="<?php echo sanitize_text_field($mp_addon->predefinedimage); ?>" src="<?php echo esc_url($plugin_url_for_ajax."/assets/images/icons/article-icons/".$full_image_name); ?>" height="40px">
<span class="vehicle_name<?php echo sanitize_text_field($mp_addon->id); ?>"><?php echo $image_name_with_dash; ?></span>
<span class="glyphicon glyphicon-chevron-down"></span>
</button>

<ul class="dropdown-menu dropdown_ul">
<?php 
foreach($default_vehicle_image as $dvir){
$image_name_expload = explode(".",$dvir);
$image_name_with_dash = $image_name_expload[0];
$full_image_name = $dvir;
?>
<li>
<a href="javascript:void(0);" class="image_click_li" data-id="<?php echo sanitize_text_field($mp_addon->id); ?>" data-image_name="<?php echo sanitize_text_field($image_name_with_dash); ?>" data-full_name="<?php echo sanitize_text_field($full_image_name); ?>"><img src="<?php echo esc_url($plugin_url_for_ajax."/assets/images/icons/article-icons/".$full_image_name); ?>" height="40px" /><?php echo sanitize_text_field($image_name_with_dash); ?></a>
</li>
<?php 
}
?>
</ul>
</div>
															</td>
										</tr>
												<tr>
													<td><label for="mp-m-title"><?php echo __("Max Quantity","mp");?></label></td>
													<td><input type="text" name="service_max_qty<?php echo $mp_addon->id; ?>" class="form-control" id="service_max_qty<?php echo $mp_addon->id; ?>" value="<?php      echo $mp_addon->max_qty;  ?>" /></td>
												</tr>
												<tr>
													<td></td>
													<td>
														<button data-article_id="<?php echo $mp_addon->id; ?>" name="" class="btn btn-success mp-btn-width update_article_addon" type="button"><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
												</tbody>
												</table>
											</form>
											
										</div>											
									</div>
								</div>
							</li>
							<?php }
							}else{
								echo __("No Article Found.","mp");
							} ?>							
							<!-- add new service pop up -->
							<li>
							<div class="panel panel-default mp-services-panel mp-add-new-service-addons">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="mp-col6">
											<span class="mp-service-title-name"><?php echo __("Add New Article","mp");?></span>		
										</div>
										<div class="pull-right mp-col6">					
											<div class="pull-right">
													<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addservice" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="addservice"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="" class="service_detail panel-collapse collapse in detail-id_addservice">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form id="mp_create_service_addon" method="post" type="" class="slide-toggle" >
												<table class="mp-create-service-table">
													<tbody>
														<input type="hidden" name="addon_service_id" id="addon_service_id" value="1" />
														<tr>
															<td><label for="mp-service-title"><?php echo __("Name","mp");?></label></td>
															<td><input type="text" name="service_title" class="form-control" id="mp-service-addons-title" /></td>
														</tr>
														<!-- <tr>
															<td><label><?php echo __("Calculation Type","mp");?></label></td>
															<td>
																<label>
															<input class="m-0" name="group1"  id="tax-vat-percentage" type="radio" value="percentage" checked />
															<span>Percentage</span>
														  </label>
													   
														  <label>
															<input class="m-0" name="group1" type="radio" value="flat" />
															<span>Flat</span>
														  </label>
															</td>
														</tr>
														
														<tr>
															<td><label for="mp-service-maxqty"><?php echo __("Price per unit","mp");?></label></td>
															<td><input type="text" name="service_maxqty" class="form-control maxqty" id="mp-service-addons-maxqty" /></td>
														</tr> -->
														<tr>
														
															<td><label for="mp-service-cubf"><?php echo $cubic_unit; ?></label></td>
															<td><input type="text" name="service_cubic_feets" class="form-control service_cubic_feets" id="mp-service-addons-cubicfeets" value="" /></td>
														</tr>
														<tr>
															<td><label for="mp-service-desc"><?php echo __("Image","mp");?></label></td>
															<td>
																<div class="mp-service-image-uploader">
																	<img id="bdscadlocimage" src="<?php echo $plugin_url_for_ajax; ?>/assets/images/service.png" class="mp-service-image br-100" height="100" width="100">
																	<label for="mp-upload-imagebdscad" class="mp-service-img-icon-label">
																		<i class="mp-camera-icon-common br-100 fa fa-camera"></i>
																		<i class="pull-left fa fa-plus-circle fa-2x"></i>
																	</label>
																	<input data-us="bdscad" class="hide mp-upload-images" type="file" name="" id="mp-upload-imagebdscad"  />
																	
																	<a style="display: none;" id="mp-remove-service-imagebdscad" class="pull-left br-100 btn-danger mp-remove-service-img btn-xs" rel="popover" data-placement='bottom' title="<?php echo __("Remove Image?","mp");?>"> <i class="fa fa-trash" title="<?php echo __("Remove Article Image","mp");?>"></i></a>
																	<div id="popover-mp-remove-service-imagebdscad" style="display: none;">
																		<div class="arrow"></div>
																		<table class="form-horizontal" cellspacing="0">
																			<tbody>
																				<tr>
																					<td>
																						<a href="javascript:void(0)" id="" value="Delete" class="btn btn-danger btn-sm" type="submit"><?php echo __("Yes","mp");?></a>
																						<a href="javascript:void(0)" id="mp-close-popover-service-imagebdscad" class="btn btn-default btn-sm" href="javascript:void(0)"><?php echo __("Cancel","mp");?></a>
																					</td>
																				</tr>
																			</tbody>
																		</table>
																	</div><!-- end pop up -->
																</div>
																<div id="mp-image-upload-popupbdscad" class="mp-image-upload-popup modal fade" tabindex="-1" role="dialog">
																	<div class="vertical-alignment-helper">
																		<div class="modal-dialog modal-md vertical-align-center">
																			<div class="modal-content">
																				<div class="modal-header">
																					<div class="col-md-12 col-xs-12">
																						<a data-us="bdscad" class="btn btn-success mp_upload_img" data-imageinputid="mp-upload-imagebdscad"><?php echo __("Crop & Save","mp");?></a>
																						<button type="button" class="btn btn-default hidemodal" data-dismiss="modal" aria-hidden="true"><?php echo __("Cancel","mp");?></button>
																					</div>	
																				</div>
																				<div class="modal-body">
																					<img id="mp-preview-imgbdscad" />
																				</div>
																				<div class="modal-footer">
																					<div class="col-md-12 np">
																						<div class="col-md-4 col-xs-12">
																							<label class="pull-left"><?php echo __("File size","mp");?></label> <input type="text" class="form-control" id="bdscadfilesize" name="filesize" />
																						</div>	
																						<div class="col-md-4 col-xs-12">	
																							<label class="pull-left"><?php echo __("H","mp");?></label> <input type="text" class="form-control" id="bdscadh" name="h" /> 
																						</div>
																						<div class="col-md-4 col-xs-12">	
																							<label class="pull-left"><?php echo __("W","mp");?></label> <input type="text" class="form-control" id="bdscadw" name="w" />
																						</div>
																						<input type="hidden" id="bdscadx1" name="x1" />
																						<input type="hidden" id="bdscady1" name="y1" />
																						<input type="hidden" id="bdscadx2" name="x2" />
																						<input type="hidden" id="bdscady2" name="y2" />
																						<input id="bdscadbdimagetype" type="hidden" name="bdimagetype"/>
																						<input type="hidden" id="bdscadbdimagename" name="bdimagename" value="" />
																					</div>
																				</div>							
																			</div>		
																		</div>			
																	</div>			
																</div>
																	<input name="service_image" id="bdscaduploadedimg" type="hidden" value="" />						
															</td>
															<td>
															<div class="btn-group">
															<?php 
															$default_vehicle_first_image = $default_vehicle_image[0];
															$first_image_expload_array = explode(".",$default_vehicle_first_image);
															$first_image_name_with_dash = $first_image_expload_array[0];/*
															$first_image_expload_array = explode("-",$first_image_name_with_dash);
															$first_image_name = $first_image_expload_array[1];*/
															?>
															<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															<img class="vehical_imageadd" data-image_name="<?php echo sanitize_text_field($default_vehicle_image[0]); ?>" src="<?php echo esc_url($plugin_url_for_ajax."/assets/images/icons/article-icons/".$default_vehicle_first_image); ?>" height="40px" />
															<span class="vehicle_nameadd"><?php echo sanitize_text_field(ucwords($first_image_name_with_dash)); ?></span>
															<span class="glyphicon glyphicon-chevron-down"></span>
															</button>

															<ul class="dropdown-menu dropdown_ul" style="height: 235px; overflow-y: scroll;">
															<?php 
															foreach($default_vehicle_image as $dvi){
															$image_name_expload = explode(".",$dvi);
															$image_name_with_dash = $image_name_expload[0];/*
															$image_name_expload = explode("-",$image_name_with_dash);
															$image_name = $image_name_expload[1];*/
															$full_image_name = $dvi;
															?>
															<li>
															<a href="javascript:void(0);" class="image_click_li" data-id="add" data-image_name="<?php echo sanitize_text_field(ucwords($image_name_with_dash)); ?>" data-full_name="<?php echo sanitize_text_field($full_image_name); ?>" title="Select this card"><img src="<?php echo esc_url($plugin_url_for_ajax."/assets/images/icons/article-icons/".$full_image_name); ?>" height="40px" /><?php echo sanitize_text_field(ucwords($image_name_with_dash)); ?></a>
															</li>
															<?php 
															}
															?>
															</ul>
															</div>
															</td>
														</tr>
													<tr>
														<td><label for="mp-service-title"><?php echo __("Max Quantity","mp");?></label></td>
														<td><input type="text" name="service_max_qty" class="form-control" id="service_max_qty" /></td>
													</tr>
										
												<tr>
													<td></td>
													<td>
														<button id="mp_create_service_addons" name="mp_create_service_addons" class="btn btn-success mp-btn-width col-md-offset-4" type="button" ><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
										
										</tbody>
												</table>
											
										</div>
										
										</form>									
									</div>
								</div>
							</div>
							</li>
							
							</ul>
						</div>	
					</div>
				</div>
			</div>
			
		</div>
			
	</div>
</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var serviceObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>