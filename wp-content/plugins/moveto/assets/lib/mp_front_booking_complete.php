<?php  
	if(!session_id()) { @session_start(); }
	
	include_once(dirname(dirname(dirname(__FILE__))).'/objects/plivo.php');
	require_once dirname(dirname(dirname(__FILE__))).'/assets/Twilio/autoload.php'; 
	use Twilio\Rest\Client;
	
		
    $root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));	
	if (file_exists($root.'/wp-load.php')) {
		require_once($root.'/wp-load.php');	
	}
	
	$plugin_url = plugins_url('',  dirname(__FILE__));
	$base =   dirname(dirname(dirname(__FILE__)));

	/* include_once($base.'/objects/class_location.php');
	include_once($base.'/objects/class_service.php');
	include_once($base.'/objects/class_provider.php');
	include_once($base.'/objects/class_clients.php'); */
	include_once($base.'/objects/class_booking.php');
	include_once($base.'/objects/class_email_templates.php');
	
	/* declare classes *//* 
	$mp_location = new moveto_location();
	$mp_service = new moveto_service();
	$mp_staff = new moveto_staff();
	$mp_client_info = new moveto_clients();	 */
	$mp_booking = new moveto_booking();
	$mp_email_templates = new moveto_email_template();

/* booking complete code here START */
if(isset($_SESSION['mp_detail']) && $_SESSION['mp_detail']!=''){
	$preff_username = $_SESSION['mp_detail']['preff_username'];
	$preff_password = $_SESSION['mp_detail']['preff_password'];
	$first_name = $_SESSION['mp_detail']['first_name'];
	$last_name = $_SESSION['mp_detail']['last_name'];
	$user_phone = $_SESSION['mp_detail']['user_phone'];
	$user_gender = $_SESSION['mp_detail']['user_gender'];
	$user_address = $_SESSION['mp_detail']['user_address'];
	$user_city = $_SESSION['mp_detail']['user_city'];
	$user_state = $_SESSION['mp_detail']['user_state'];
	$user_notes = $_SESSION['mp_detail']['user_notes'];
	$user_ccode = $_SESSION['mp_detail']['user_ccode'];
	
	if(isset($_SESSION['mp_detail']['serialize_extra_details'])){
		$extra_details = $_SESSION['mp_detail']['serialize_extra_details'];
	}else{
		$extra_details = array();
	}

	$serialize_extra_details = serialize($extra_details);
	
	if($_SESSION['mp_detail']['payment_method'] == 'stripe'){
		$mp_payments->transaction_id = $_SESSION['mp_detail']['stripe_trans_id'];
	}else if($_SESSION['mp_detail']['payment_method'] == 'paypal'){
		$mp_payments->transaction_id = $_SESSION['mp_detail']['paypal_transaction_id'];
	} else if($_SESSION['mp_detail']['payment_method'] == 'payumoney'){
		$mp_payments->transaction_id = $_SESSION['payu_transaction_id'];
	} else if($_SESSION['mp_detail']['payment_method'] == 'paytm'){
		$mp_payments->transaction_id = $_POST['transaction_id'];
	}else if($_SESSION['mp_detail']['payment_method'] == 'paystack'){
		$mp_payments->transaction_id = $_SESSION['transaction_id'];
	}
	else{
		$mp_payments->transaction_id = '';
	}
	
	
	if($_SESSION['mp_detail']['mp_user_type'] == 'guest'){
		/* existing user code here */
		$user_id = 0;
	}else if($_SESSION['mp_detail']['mp_user_type'] == 'existing'){
		/* existing user code here */
		$user_id = get_current_user_id();
		
		update_user_meta( $user_id, 'mp_client_phone', $user_phone );
		update_user_meta( $user_id, 'mp_client_gender', $user_gender );
		update_user_meta( $user_id, 'mp_client_address', $user_address );
		update_user_meta( $user_id, 'mp_client_city', $user_city );
		update_user_meta( $user_id, 'mp_client_state', $user_state );
		update_user_meta( $user_id, 'mp_client_notes', $user_notes );
		update_user_meta( $user_id, 'mp_client_extra_details', $serialize_extra_details );
		update_user_meta( $user_id, 'mp_client_ccode', $user_ccode );
	}else{
		/* New user code here */
		
		$username = $_SESSION['mp_detail']['username'];
		
		/* insert data in user table */
		$mp_user_info = array(
						'user_login'    =>   $username,
						'user_email'    =>   $preff_username,
						'user_pass'     =>   $preff_password,
						'first_name'    =>   $first_name,
						'last_name'     =>   $last_name,
						'nickname'      =>  '',
						'role' => 'subscriber'
						);
		   
		$new_mp_user = wp_insert_user( $mp_user_info );
			
		$user = new WP_User($new_mp_user);
		$user->add_cap('read');
		$user->add_cap('mp_client'); 
		$user->add_role('mp_users');
		$user_id = $new_mp_user;
		$user_login = $preff_username;
		$booking_locations = array();
		foreach($_SESSION['mp_cart_item'] as $cart_itemloc){
			$citemloc = unserialize($cart_itemloc);
			$booking_locations[] = $citemloc['selected_location'];
		}
		
		
		add_user_meta( $new_mp_user, 'mp_client_locations', '#'.implode('#',$booking_locations).'#');
		add_user_meta( $new_mp_user, 'mp_client_phone', $user_phone );
		add_user_meta( $new_mp_user, 'mp_client_gender', $user_gender );
		add_user_meta( $new_mp_user, 'mp_client_address', $user_address );
		add_user_meta( $new_mp_user, 'mp_client_city', $user_city );
		add_user_meta( $new_mp_user, 'mp_client_state', $user_state );
		add_user_meta( $new_mp_user, 'mp_client_notes', $user_notes );
		add_user_meta( $new_mp_user, 'mp_client_ccode', $user_ccode );
		add_user_meta( $new_mp_user, 'mp_client_extra_details', $serialize_extra_details );
		
		/* Set cookie of user after booking */
		wp_set_current_user( $user_id, $user_login );
		wp_set_auth_cookie( $user_id );
	}
	$mp_booking->get_last_order_id();
	$next_order_id = $mp_booking->last_order_id + 1;
	
	/* Adding Appointments Into Google Calendar START */
	if (!function_exists('moveto_addevent_googlecalender_provider')) {
		function moveto_addevent_googlecalender_provider($provider_id,$provider_gc_id,$gc_token,$summary,$location,$description,$event_color,$date,$start,$end,$GcclientID,$GcclientSecret,$GcEDvalue,$providerTZ){
			require_once dirname(dirname(dirname(__FILE__)))."/assets/GoogleCalendar/google-api-php-client/src/Google_Client.php";
			require_once dirname(dirname(dirname(__FILE__)))."/assets/GoogleCalendar/google-api-php-client/src/contrib/Google_CalendarService.php";

			$clientP = new Google_Client();
			$clientP->setApplicationName("OctaBook Google Calender");
			$clientP->setClientId($GcclientID);
			$clientP->setClientSecret($GcclientSecret);   
			$clientP->setRedirectUri(get_option('mp_gc_frontend_url'));
			$clientP->setDeveloperKey($GcclientID);
			$clientP->setScopes( 'https://www.googleapis.com/auth/calendar' );
			$clientP->setAccessType('offline');

			$calP = new Google_CalendarService($clientP);

			$clientP->setAccessToken($gc_token);
			$accesstoken = json_decode($gc_token);  

			if ($gc_token) {
				if ($clientP->isAccessTokenExpired()) {
					$clientP->refreshToken($accesstoken->refresh_token);
				}
			}
			if ($clientP->getAccessToken()){
				$startTP = new Google_EventDateTime();
				$endTP = new Google_EventDateTime();
				$eventP = new Google_Event();
				$calendarId = $provider_gc_id;
				$startTP->setTimeZone($providerTZ);
				$startTP->setDateTime($date."T".$start);
				$endTP->setTimeZone($providerTZ);
				$endTP->setDateTime($date."T".$end);
				$eventP->setSummary($summary);
				$eventP->setColorId($event_color);
				$eventP->setLocation($location);
				$eventP->setDescription($description);
				$eventP->setStart($startTP);
				$eventP->setEnd($endTP);

				$insert = $calP->events->insert($provider_gc_id,$eventP);
			}

			if(isset($insert)){
				return $insert;
			}else{
				return '';
			}  
		}
	}
	
	$GcclientID = get_option('mp_gc_client_id');
	$GcclientSecret = get_option('mp_gc_client_secret');
	$GcEDvalue = get_option('mp_gc_status');
		
	$service = new moveto_service();
	$ordercounter = 0;
	foreach($_SESSION['mp_cart_item'] as $cart_item){
		$citem = unserialize($cart_item);
		$service->id=$citem['selected_service'];
		$serviceInfo = $service->readOne();
		$service_title = $service->service_title;
		
		$gc_token = get_option('mp_gc_token');
		$summary = $service_title."-".$first_name." ".$last_name;
		$description = 'Service='.$service_title.', Name='.$first_name." ".$last_name.', Email='.$preff_username.', Phone='.$user_ccode.''.$user_phone;
		$event_color = '9';
		
		$date = date_i18n('Y-m-d', $citem['selected_datetime']);
		$start = date_i18n('H:i:s',$citem['selected_datetime']);
		$event_endtime = date_i18n('H:i:s',$citem['selected_enddatetime']);
		$end = $event_endtime;
		
		if(get_option('timezone_string') != ''){
			$providerTZ = get_option('timezone_string');
		}else{
			$gmt_offset = get_option('gmt_offset');
			$hr_minute = explode('.', $gmt_offset);
			if (isset($hr_minute[1])) {
				if ($hr_minute[1] == '5') {
					$gmt_offset = $hr_minute[0].'.30';
				}else{
					$gmt_offset = $hr_minute[0].'.45';
				}
			}else{
				$gmt_offset = $hr_minute[0];
			}
			$seconds = $gmt_offset * 60 * 60;
			$get_tz = timezone_name_from_abbr('', $seconds, 1);
			if($get_tz === false){ $get_tz = timezone_name_from_abbr('', $seconds, 0); }
			$providerTZ = $get_tz;
		}
		$provider_gc_id = get_option('mp_gc_id');
		$provider_id = '';
		if($gc_token != '' && $GcEDvalue == 'Y' && $GcclientID!='' && $GcclientSecret!=''){
			$event_Status = moveto_addevent_googlecalender_provider($provider_id,$provider_gc_id,$gc_token,$summary,$location,$description,$event_color,$date,$start,$end,$GcclientID,$GcclientSecret,$GcEDvalue,$providerTZ);
			 $gc_event_id = $event_Status['id'];
		}else{
			 $gc_event_id = '';
		}
		
		$citem = unserialize($cart_item);
		$mp_booking->location_id = $citem['selected_location'];
		$mp_booking->order_id = $next_order_id;
		$mp_booking->client_id = $user_id;
		$mp_booking->service_id = $citem['selected_service'];
		$mp_booking->provider_id = $citem['selected_staff'];
		$mp_booking->booking_price = $citem['total_price'];
		$mp_booking->booking_datetime = date_i18n('Y-m-d H:i:s', $citem['selected_datetime']);
		$mp_booking->booking_endtime = date_i18n('Y-m-d H:i:s', $citem['selected_enddatetime']);
		if(get_option('moveto_appointment_auto_confirm')=='E'){
			$mp_booking->booking_status = 'C';
		}else{
			$mp_booking->booking_status = 'P';	
		}
		$mp_booking->reject_reason = '';
		$mp_booking->cancel_reason = '';
		$mp_booking->confirm_note = '';
		$mp_booking->reschedule_note = '';
		$mp_booking->reminder = '0';
		$mp_booking->notification = '0';
		$mp_booking->gc_event_id = $gc_event_id;
		$mp_booking->lastmodify = date_i18n('Y-m-d H:i:s');
		$mp_booking->add_bookings();
		
		/* add addons */
		foreach($citem['each_addon_price'] as $arrkey => $aitem){
			$mp_booking->order_id = $next_order_id;
			$mp_booking->service_id = $citem['selected_service'];
			$mp_booking->addons_service_id = $aitem['addonid'];
			$mp_booking->associate_service_id = $citem['service_addons'][$arrkey]['maxqty'];
			$mp_booking->addons_amount = $aitem['addon_price'];
			$mp_booking->insert_booking_addons();
		}
		
		
		/* Add Order Client Info & Payment Info Once */
		if($ordercounter==0){
			$personal_info_arr = array('ccode'=>$user_ccode,'dob'=>'','zip'=>'','skype'=>'','age'=>'','phone1'=>$user_phone, 'gender'=>$user_gender, 'first_name'=>$first_name, 'last_name'=>$last_name, 'address'=>$user_address, 'city'=>$user_city, 'state'=>$user_state, 'notes'=>$user_notes);
	
			$personal_info_merged_arr = array_merge($personal_info_arr,unserialize($extra_details));
			
			$personal_info_serialize_arr = serialize($personal_info_merged_arr);
			
			$mp_client_info->order_id = $next_order_id;
			$mp_client_info->clientName = $first_name." ".$last_name;
			$mp_client_info->client_email = $preff_username;
			$mp_client_info->client_phone = $user_ccode.''.$user_phone;
			$mp_client_info->client_personal_info = $personal_info_serialize_arr;
			$mp_client_info->add_client_info();
			
			$mp_payments->payment_method = $_SESSION['mp_detail']['payment_method'];
			$mp_payments->location_id = $citem['selected_location'];;
			$mp_payments->client_id = $user_id;
			$mp_payments->order_id = $next_order_id;
			$mp_payments->amount = $_SESSION['mp_sub_total'];
			$mp_payments->discount = $_SESSION['mp_detail']['discount'];
			$mp_payments->taxes = $_SESSION['mp_taxvat'];
			$mp_payments->partial = $_SESSION['mp_partialdeposit'];
			$mp_payments->net_total = $_SESSION['mp_nettotal'];
			$mp_payments->add_payments();
		}			
		$ordercounter++;
		
	}
	/*  Adding Appointments Into Google Calendar END  */
		
	
	
	
	unset($_SESSION['mp_cart_item']);
	unset($_SESSION['mp_partialdeposit']);
	unset($_SESSION['mp_partialdeposit_remaining']);
	unset($_SESSION['mp_nettotal']);
	unset($_SESSION['mp_taxvat']);
	unset($_SESSION['mp_sub_total']);
	unset($_SESSION['mp_coupon_id']);
	unset($_SESSION['mp_coupon_code']);
	unset($_SESSION['mp_coupon_discount']);
		
	/* Send Email To Custom, Staff, Admin */
	function set_content_type() {			
		return 'text/html';		
	}
			
	$mp_booking->order_id=$next_order_id;        
	$client_bookings= $mp_booking->get_all_bookings_by_order_id();		
	$sender_name = get_option('moveto_email_sender_name');		
	$sender_email_address = get_option('moveto_email_sender_address');		
	$headers = "From: $sender_name <$sender_email_address>" . "\r\n";
	
	$client_name = $first_name." ".$last_name;
	$company_name = get_option('moveto_company_name');
	$company_address = get_option('moveto_company_address');
	$company_city = get_option('moveto_company_city');
	$company_state = get_option('moveto_company_state');
	$company_zip = get_option('moveto_company_zip');
	$company_country = get_option('moveto_company_country');
	$company_phone = get_option('moveto_company_country_code').get_option('moveto_company_phone');
	$company_email = get_option('moveto_company_email');
	$company_logo = $business_logo = site_url()."/wp-content/uploads/".get_option('moveto_company_logo');
	
	
	
	
	/* main loop for content and mail start here */           
	$booking_counter = 1; 
	$booking_counter_txt = '';
	$booking_details = '';
	$booking_details_sms = '';
	
	foreach($client_bookings as $single_booking){
			
		$mp_service->id = $single_booking->service_id;                    
		$mp_staff->id = $single_booking->provider_id;                    
		$mp_service->readOne();                    
		$staffinfo = $mp_staff->readOne();   
		$location_details = '';
		if($single_booking->location_id!=0 || $single_booking->location_id!=''){
			$mp_location->id = $single_booking->location_id;
			$locationinfo = $mp_location->readOne();
			if(sizeof((array)$locationinfo)>0){
				$location_details .= "<br/><span><strong>".__('Location','mp')."</strong>: ".stripslashes_deep($locationinfo[0]->location_title)."</span><br/><br/><span><strong>".__('Location Address','mp')."</strong>: ".stripslashes_deep($locationinfo[0]->address)."</span><br/><br/>";
			}
		}
		
		$addons_detail = '';
		$addon_titles = '';
		$addon_prices = '';
		$addon_qty = '';
		$mp_booking->order_id =  $single_booking->order_id;
		$serviceaddons_info = $mp_booking->select_addonsby_orderidand_serviceid();	
		$totalserviceaddons = sizeof((array)$serviceaddons_info);
		if($totalserviceaddons>0){
			$addoncounter = 1;
			foreach($serviceaddons_info as $serviceaddon_info){				
				$mp_service->addon_id = $serviceaddon_info->addons_service_id;
				$addon_info = $mp_service->readOne_addon();
				if($addoncounter==$totalserviceaddons){
					$addon_titles .= stripslashes_deep($addon_info[0]->addon_service_name); 
					$addon_prices .= $serviceaddon_info->addons_service_rat; 
					$addon_qty .= $serviceaddon_info->associate_service_d; 
				}else{
					$addon_titles .= stripslashes_deep($addon_info[0]->addon_service_name).','; 
					$addon_prices .= $serviceaddon_info->addons_service_rat.',';
					$addon_qty .= $serviceaddon_info->associate_service_d.',';
				}				
				$addoncounter++;
			}			
			$addons_detail .="<br/><span><strong>".__('Addon Tittle(s)','mp')."</strong>: ".$addon_titles."</span><br/><br/><span><strong>".__('Addon Price(s)','mp')."</strong>: ".$addon_prices."</span><br/><br/><span><strong>".__('Addon Quantity(s)','mp')."</strong>: ".$addon_qty."</span><br/><br/>";			
		}
		



		
		$datetime = explode(' ',$single_booking->booking_datetime);        
		$booking_id=base64_encode($next_order_id);        
		$encoded_cinfo_sp=base64_encode($booking_id."-confirm");        
		$appoint_confirm_link_sp =plugins_url('',dirname(__FILE__))."/lib/booking_crc_email.php?".$encoded_cinfo_sp;                        
		$encoded_rinfo_sp=base64_encode($booking_id."-reject");        
		$appoint_reject_link_sp =plugins_url('',dirname(__FILE__))."/lib/booking_crc_email.php?".$encoded_rinfo_sp; 

		/*Client Cancel Link */
		$encoded_cinfo_client=base64_encode($booking_id."-clientcancel");        

		$appoint_cancel_link_client =plugins_url('',dirname(__FILE__))."/lib/booking_crc_email.php?".$encoded_cinfo_client;
		
		if(isset($_SESSION['booking_type']) || get_option('moveto_auto_confirm_appointment')=='Y' ){
		$confirm_link_sp='';
		}else{
		$confirm_link_sp="<a style='text-decoration: none;color: #FFF;background-color: #348eda;	border: solid #348eda;border-width: 10px 30px; line-height: 1;	font-weight: bold;margin-right: 10px;text-align: center;cursor: pointer;display: inline-block; border-radius: 10px;'  id='email-btn-primary' class='email-btn-primary' href='".$appoint_confirm_link_sp."-".base64_encode($single_booking->provider_id."+".$single_booking->id)."' >".__('Confirm','mp')."</a>";     
		}		
		$reject_link_sp ="<a style='text-decoration: none;color: #FFF;background-color: red;border: solid red;border-width: 10px 30px;line-height: 1;font-weight: bold;margin-right: 10px;text-align: center;cursor: pointer;display: inline-block;border-radius: 10px;'  id='email-btn-secondary' class='email-btn-secondary' href='".$appoint_reject_link_sp."-".base64_encode($single_booking->provider_id."+".$single_booking->id)."' >".__('Reject','mp')."</a>";

		$cancel_link_client ="<a style='text-decoration: none;color: #FFF;background-color: red;border: solid red;border-width: 10px 30px;line-height: 1;font-weight: bold;margin-right: 10px;text-align: center;cursor: pointer;display: inline-block;border-radius: 10px;'  id='email-btn-secondary' class='email-btn-secondary' href='".$appoint_cancel_link_client."-".base64_encode($single_booking->client_id."+".$single_booking->id)."' >".__('Cancel','mp')."</a>";
		
		$booking_details .= $location_details;
		
		$extra_details = unserialize($serialize_extra_details);
		$user_extra_info = get_user_meta($user_id,'mp_client_extra_details');
								 if($user_extra_info != '') { 
									foreach($user_extra_info as $extra_details){
										$unser_date = unserialize($extra_details);
										$sec_unser_data = unserialize($unser_date);
										foreach($sec_unser_data as $key=>$val){
										$booking_details .=	 "<div class='col-xs-12 np'><b> ".$key."</b> - ".$val."</div><br/>";
										}
									}
								}
								
		$booking_details .= "<span><strong>".__('For','mp')."</strong>: ".stripslashes_deep($mp_service->service_title)."</span><br/><br/>
								<span><strong>".__('With','mp')."</strong>: ".ucwords(stripslashes_deep($staffinfo[0]['staff_name']))."</span><br/><br/>
								<span><strong>".__('On','mp')."</strong>: ".date_i18n(get_option('date_format'),strtotime($datetime[0]))."</span><br/><br/>
								<span><strong>".__('At','mp')."</strong>: ".date_i18n(get_option('time_format'),strtotime($datetime[1]))."</span><br/><br/>
								<span>".$cancel_link_client."</span><br/>".$addons_detail."</span><br/>";

		$booking_details_sms .= ' With :'.ucwords(stripslashes_deep($staffinfo[0]['staff_name'])).' On : '.date_i18n(get_option('date_format'),strtotime($datetime[0])).' At : '.date_i18n(get_option('time_format'),strtotime($datetime[1])).' For: '.$mp_service->service_title.', ';
		
		if(sizeof((array)$client_bookings) > 1) {
			$booking_counter_txt = "#".$booking_counter."<br/>";
		}
		
		$arr_providers_booking_details[$staffinfo[0]['id']][] = $location_details."<br/>".$booking_counter_txt."
		<span><strong>".__('For','mp')."</strong> :".stripslashes_deep($mp_service->service_title)."</span><br/><br/>
								<span><strong>".__('With','mp')."</strong> :".ucwords(stripslashes_deep($staffinfo[0]['staff_name']))."</span><br/><br/>
								<span><strong>".__('On','mp')."</strong> :".date_i18n(get_option('date_format'),strtotime($datetime[0]))."</span><br/><br/>
								<span><strong>".__('At','mp')."</strong> :".date_i18n(get_option('time_format'),strtotime($datetime[1]))."</span><br/><br/>".$addons_detail."<span>".$confirm_link_sp."</span>	<span>".$reject_link_sp."</span><br/>";
		
		
		$booking_counter++;
	}
	
	/* Mail content loop end here */

	/* Send Email To Client */
		if(get_option('moveto_client_email_notification_status')=='E'){	
		$mp_clientemail_templates = new moveto_email_template();
		$msg_template = $mp_clientemail_templates->email_parent_template;	
		if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
			$mp_clientemail_templates->email_template_name = "CC"; 
		}else{
			$mp_clientemail_templates->email_template_name = "AC";
		}
		$template_detail = $mp_clientemail_templates->readOne();        
		if($template_detail[0]->email_message!=''){            
			$email_content = $template_detail[0]->email_message;        
		}else{            
			$email_content = $template_detail[0]->default_message;        
		}        
		$email_subject = $template_detail[0]->email_subject;
		$email_client_message = '';
		/* Sending email to client when New Appointment request Sent */ 		  
		if($template_detail[0]->email_template_status=='e'){			
			$search = array('{{customer_name}}','{{booking_details}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');      
			$replace_with = array($client_name,$booking_details,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
			
			$email_client_message = str_replace($search,$replace_with,$email_content);	
			$email_client_message = str_replace('###msg_content###',$email_client_message,$msg_template);		
			add_filter( 'wp_mail_content_type', 'set_content_type' );
			$status = wp_mail($preff_username,$email_subject,$email_client_message,$headers);           
		}       
	}
	/* Send email to admin/service provider when booking is complete */
	$client_full_detail='<br/>';
	$sms_client_full_detail=' ';
	if(ucwords($client_name)!=''){ 
		$client_full_detail .= "<span><strong>".__('Client Name','mp')."</strong>: ".ucwords($client_name)."</span><br/><br/>";
		$sms_client_full_detail .= __('Client Name','mp').": ".ucwords($client_name)." ";
	}if($preff_username!=''){ 
		$client_full_detail .= "<span><strong>".__('Client Email','mp')."</strong>: ".$preff_username."</span><br/><br/>";
		$sms_client_full_detail .= __('Client Email','mp').": ".$preff_username." ";
	}	
	if($user_gender!=''){
		if($user_gender == "M"){
			$gender_display = "Male";
		}else{
			$gender_display = "Female";
		}
		$client_full_detail .="<span><strong>".__('Gender','mp')."</strong>: ".$gender_display."</span><br/><br/>";
		$sms_client_full_detail .= __('Gender','mp').": ".$gender_display." ";
	}
	if($user_phone!=''){
		$client_full_detail .="<span><strong>".__('Client Phone','mp')."</strong>: ".$user_phone."</span><br/><br/>";
		$sms_client_full_detail .= __('Client Phone','mp').": ".$user_phone." ";
	}
	if($user_address!=''){
		$client_full_detail .="<span><strong>".__('Address','mp')."</strong>: ".$user_address."</span><br/><br/>";
		$sms_client_full_detail .= __('Address','mp').": ".$user_address." ";
	}
	if($user_city!=''){
		$client_full_detail .="<span><strong>".__('City','mp')."</strong>: ".$user_city."</span><br/><br/>";
		$sms_client_full_detail .= __('City','mp').": ".$user_city." ";
	}
	if($user_state!=''){
		$client_full_detail .="<span><strong>".__('State','mp')."</strong>: ".$user_state."</span><br/><br/>";
		$sms_client_full_detail .= __('State','mp').": ".$user_state." ";
	}
	if($user_notes!=''){
		$client_full_detail .="<span><strong>".__('Notes','mp')."</strong>: ".$user_notes."</span><br/><br/>";
		$sms_client_full_detail .= __('Notes','mp').": ".$user_notes." ";
	}
	$client_detail= $client_full_detail;
	$sms_client_detail= $sms_client_full_detail;
	
	/* Send Email To Staff */
	if(get_option('moveto_service_provider_email_notification_status')=='E'){	
		$mp_staffemail_templates = new moveto_email_template();	
		$msg_template = $mp_staffemail_templates->email_parent_template;
		if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
			$mp_staffemail_templates->email_template_name = "CS"; 
		}else{
			$mp_staffemail_templates->email_template_name = "AS";  
		}
		$template_detail = $mp_staffemail_templates->readOne();        
		if($template_detail[0]->email_message!=''){            
			$email_content = $template_detail[0]->email_message;        
		}else{            
			$email_content = $template_detail[0]->default_message;
		}        
		$email_subject = $template_detail[0]->email_subject;   
		$email_staff_message = '';
		
		if($template_detail[0]->email_template_status=='e'){
			foreach($arr_providers_booking_details as $provider_id => $bookingstrarr){
				$mp_staff->id = $provider_id;
				$staffinfo = $mp_staff->readOne();
				$strtoprint = "";
				foreach ($bookingstrarr as $bookingsss) {
					$strtoprint .= $bookingsss;
				}

				$search = array('{{admin_manager_name}}{{service_provider_name}}','{{booking_details}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
				
				$replace_with = array($staffinfo[0]['staff_name'],$strtoprint,$client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

				$email_staff_message = str_replace($search,$replace_with,$email_content);
				$email_staff_message = str_replace('###msg_content###',$email_staff_message,$msg_template);
				add_filter( 'wp_mail_content_type', 'set_content_type' );
				$status = wp_mail($staffinfo[0]['email'],$email_subject,$email_staff_message,$headers);
			}
		}
	}
	
	/* Send Email To Admin */	
	$arr_admin_booking_detail='';        
	foreach($arr_providers_booking_details as $provider_id => $bookingstrarr){
			for($i=0;$i<sizeof((array)$bookingstrarr);$i++){                
				$arr_admin_booking_detail .=$bookingstrarr[$i];               
			}
	}	
	$arr_admin_bookingfulldetail=$arr_admin_booking_detail; 		
	if(get_option('moveto_admin_email_notification_status')=='E'){
		$mp_adminemail_templates = new moveto_email_template();
		$msg_template = $mp_adminemail_templates->email_parent_template;
		
		if(isset($_SESSION['booking_type']) ||get_option('moveto_appointment_auto_confirm')=='E'){
			$mp_adminemail_templates->email_template_name = "CA"; 
		}else{
			$mp_adminemail_templates->email_template_name = "AA";  
		}
		$template_detail = $mp_adminemail_templates->readOne();        
		if($template_detail[0]->email_message!=''){            
			$email_content = $template_detail[0]->email_message;        
		}else{            
			$email_content = $template_detail[0]->default_message;
		}        
		$email_subject = $template_detail[0]->email_subject;
		$email_admin_message = '';
		$admin_sender_name = get_option('moveto_email_sender_name');

	
		$search = array('{{admin_manager_name}}','{{booking_details}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{service_provider_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
		
		$replace_with = array($admin_sender_name,$company_name,$arr_admin_bookingfulldetail,$client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
		$email_admin_message = str_replace($search,$replace_with,$email_content);
		$email_admin_message = str_replace('###msg_content###',$email_admin_message,$msg_template);
		add_filter( 'wp_mail_content_type', 'set_content_type' );		
		$status = wp_mail(get_option('moveto_email_sender_address'),$email_subject,$email_admin_message,$headers); 	
	}
	
	/******************* Send Sms code START *********************/
	if(get_option("moveto_sms_reminder_status") == "E"){
		/** TWILIO **/
		if(get_option("moveto_sms_noti_twilio") == "E"){
			$obj_sms_template = new moveto_sms_template();
			$twillio_sender_number = get_option('moveto_twilio_number');
			$AccountSid = get_option('moveto_twilio_sid');
			$AuthToken =  get_option('moveto_twilio_auth_token'); 
			
			/* Send SMS To Client */
			if(get_option('moveto_twilio_client_sms_notification_status') == "E"){
				$twilliosms_client = new Client($AccountSid, $AuthToken);
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','CC');
				}else{
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
				}
				
				if($template1[0]->sms_template_status == "e" && $user_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					$client_sms_body = str_replace($search,$replace_with,$message);
					$twilliosms_client->messages->create(
						$user_ccode.''.$user_phone,
						array(
							'from' => $twillio_sender_number,
							'body' => $client_sms_body 
						)
					);
				}
			}
			/* Send SMS To Staff */
			if(get_option('moveto_twilio_service_provider_sms_notification_status') == "E"){
				$twilliosms_staff = new Client($AccountSid, $AuthToken);
				foreach($arr_providers_booking_details as $provider_id => $bookingstrarr){
					$mp_staff->id = $provider_id;
					$staffinfo = $mp_staff->readOne();
					if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
						$template = $obj_sms_template->gettemplate_sms("SP",'e','CS');
					}else{
						$template = $obj_sms_template->gettemplate_sms("SP",'e','AS');
					}
					
					if($template[0]->sms_template_status == "e" && $staffinfo[0]['phone']!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$sender_name = get_option('moveto_email_sender_name');
						$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
						$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

						$staff_sms_body = str_replace($search,$replace_with,$message);						
						
						$twilliosms_staff->messages->create(
							$staffinfo[0]['phone'],
							array(
								'from' => $twillio_sender_number,
								'body' => $staff_sms_body 
							)
						);
					}
				}
			}
			/* Send SMS To Admin */
			if(get_option('moveto_twilio_admin_sms_notification_status') == "E"){		   
				$twilliosms_admin = new Client($AccountSid, $AuthToken);
				
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template = $obj_sms_template->gettemplate_sms("AM",'e','CA');
				}else{
					$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
				}
				if($template[0]->sms_template_status == "e" && get_option('moveto_twilio_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					
					$admin_sms_body = str_replace($search,$replace_with,$message);	

					$twilliosms_staff->messages->create(
						get_option('moveto_twilio_ccode').get_option('moveto_twilio_admin_phone_no'),
						array(
							'from' => $twillio_sender_number,
							'body' => $admin_sms_body 
						)
					);
				}
			}
		}
				
		/** PLIVO **/
		if(get_option("moveto_sms_noti_plivo") == "E"){
			$obj_sms_template = new moveto_sms_template();
			$plivo_sender_number = get_option('moveto_plivo_number');	
			$auth_sid = get_option('moveto_plivo_sid');
			$auth_token = get_option('moveto_plivo_auth_token');
			
			/* Send SMS To Client */
			if(get_option('moveto_plivo_client_sms_notification_status') == "E"){
				$p_client = new Plivo\RestAPI($auth_sid, $auth_token, '', '');
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','CC');
				}else{
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
				}
				if($template1[0]->sms_template_status == "e" && $user_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					$client_sms_body = str_replace($search,$replace_with,$message);
					
					$clientparams = array(
						'src' => $plivo_sender_number,
						'dst' => $user_ccode.''.$user_phone,
						'text' => $client_sms_body,
						'method' => 'POST'
					);
					$response = $p_client->send_message($clientparams);
				}
			}
			/* Send SMS To Staff */
			if(get_option('moveto_plivo_service_provider_sms_notification_status') == "E"){
				$p_staff = new Plivo\RestAPI($auth_id, $auth_token, '', '');
				foreach($arr_providers_booking_details as $provider_id => $bookingstrarr){
					$mp_staff->id = $provider_id;
					$staffinfo = $mp_staff->readOne();
					if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
						$template = $obj_sms_template->gettemplate_sms("SP",'e','CS');
					}else{
						$template = $obj_sms_template->gettemplate_sms("SP",'e','AS');
					}
					
					if($template[0]->sms_template_status == "e" && $staffinfo[0]['phone']!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$sender_name = get_option('moveto_email_sender_name');
						$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
						$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

						$staff_sms_body = str_replace($search,$replace_with,$message);						
						
						$staffparams = array(
						'src' => $plivo_sender_number,
						'dst' => $staffinfo[0]['phone'],
						'text' => $staff_sms_body,
						'method' => 'POST'
						);
						$response = $p_staff->send_message($staffparams);
					}
				}
			}
			/* Send SMS To Admin */
			if(get_option('moveto_plivo_admin_sms_notification_status') == "E"){		   
				$twilliosms_admin = new Client($AccountSid, $AuthToken);
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template = $obj_sms_template->gettemplate_sms("AM",'e','CA');
				}else{
					$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
				}
				if($template[0]->sms_template_status == "e" && get_option('moveto_plivo_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					
					$admin_sms_body = str_replace($search,$replace_with,$message);	

					$adminparams = array(
						'src' => $plivo_sender_number,
						'dst' => get_option('moveto_plivo_ccode').get_option('moveto_plivo_admin_phone_no'),
						'text' => $admin_sms_body,
						'method' => 'POST'
						);
					$response = $p_admin->send_message($adminparams);
				}
			}
		}
		
		/** NEXMO **/
		if(get_option("moveto_sms_noti_nexmo") == "E"){
			$obj_sms_template = new moveto_sms_template();
			include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_nexmo.php');
			$nexmo_client = new moveto_nexmo();
			$nexmo_client->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');
			$nexmo_client->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');
			$nexmo_client->moveto_nexmo_form = get_option('moveto_nexmo_form');
			
			/* Send SMS To Client */
			if(get_option('moveto_nexmo_send_sms_client_status') == "E"){
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','CC');
				}else{
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
				}
				if($template1[0]->sms_template_status == "e" && $user_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					$client_sms_body = str_replace($search,$replace_with,$message);
					$res = $nexmo_client->send_nexmo_sms($user_ccode.''.$user_phone,$client_sms_body);
				}
			}
			/* Send SMS To Staff */
			if(get_option('moveto_nexmo_send_sms_sp_status') == "E"){
				foreach($arr_providers_booking_details as $provider_id => $bookingstrarr){
					$mp_staff->id = $provider_id;
					$staffinfo = $mp_staff->readOne();
					if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
						$template = $obj_sms_template->gettemplate_sms("SP",'e','CS');
					}else{
						$template = $obj_sms_template->gettemplate_sms("SP",'e','AS');
					}
					
					if($template[0]->sms_template_status == "e" && $staffinfo[0]['phone']!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$sender_name = get_option('moveto_email_sender_name');
						$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
						$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

						$staff_sms_body = str_replace($search,$replace_with,$message);
						$nexmo_client->send_nexmo_sms($staffinfo[0]['phone'],$staff_sms_body);
					}
				}
			}
			/* Send SMS To Admin */
			if(get_option('moveto_nexmo_send_sms_admin_status') == "E"){
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template = $obj_sms_template->gettemplate_sms("AM",'e','CA');
				}else{
					$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
				}
				if($template[0]->sms_template_status == "e" && get_option('moveto_nexmo_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					
					$admin_sms_body = str_replace($search,$replace_with,$message);
					$nexmo_client->send_nexmo_sms(get_option('moveto_nexmo_ccode').get_option('moveto_nexmo_admin_phone_no'),$admin_sms_body);
				}
			}
		}
		
		/** TEXTLOCAL **/
		if(get_option("moveto_sms_noti_textlocal") == "E"){
			$obj_sms_template = new moveto_sms_template();
			$textlocal_apikey = get_option('moveto_textlocal_apikey');
			$textlocal_sender = get_option('moveto_textlocal_sender');
			
			/* Send SMS To Client */
			if(get_option('moveto_textlocal_client_sms_notification_status') == "E"){
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','CC');
				}else{
					$template1 = $obj_sms_template->gettemplate_sms("C",'e','AC');
				}
				if($template1[0]->sms_template_status == "e" && $user_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{customer_name}}','{{booking_details}}','{{booking_detail}}','{{company_name}}','{{service_provider_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					$replace_with = array($client_name,$booking_details_sms,$booking_details_sms,$company_name,'',$sender_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					$client_sms_body = str_replace($search,$replace_with,$message);
					
					$textlocal_numbers = $user_ccode.''.$user_phone;
					$textlocal_sender = urlencode($textlocal_sender);
					$client_sms_body = rawurlencode($client_sms_body);
					
					$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $client_sms_body);
					
					$ch = curl_init('https://api.textlocal.in/send/');
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
			/* Send SMS To Staff */
			if(get_option('moveto_textlocal_service_provider_sms_notification_status') == "E"){
				foreach($arr_providers_booking_details as $provider_id => $bookingstrarr){
					$mp_staff->id = $provider_id;
					$staffinfo = $mp_staff->readOne();
					if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
						$template = $obj_sms_template->gettemplate_sms("SP",'e','CS');
					}else{
						$template = $obj_sms_template->gettemplate_sms("SP",'e','AS');
					}
					
					if($template[0]->sms_template_status == "e" && $staffinfo[0]['phone']!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$sender_name = get_option('moveto_email_sender_name');
						$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
						$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);

						$staff_sms_body = str_replace($search,$replace_with,$message);
						
						$textlocal_numbers = $staffinfo[0]['phone'];
						$textlocal_sender = urlencode($textlocal_sender);
						$staff_sms_body = rawurlencode($staff_sms_body);
						
						$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $staff_sms_body);
						
						$ch = curl_init('https://api.textlocal.in/send/');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);
					}
				}
			}
			/* Send SMS To Admin */
			if(get_option('moveto_textlocal_admin_sms_notification_status') == "E"){
				if(isset($_SESSION['booking_type']) || get_option('moveto_appointment_auto_confirm')=='E'){
					$template = $obj_sms_template->gettemplate_sms("AM",'e','CA');
				}else{
					$template = $obj_sms_template->gettemplate_sms("AM",'e','AA');
				}
				if($template[0]->sms_template_status == "e" && get_option('moveto_textlocal_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$search = array('{{admin_manager_name}}','{{service_provider_name}}','{{booking_details}}','{{booking_detail}}','{{appoinment_client_detail}}','{{company_name}}','{{customer_name}}','{{admin_manager_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}');
					
					$replace_with = array($sender_name,$staffinfo[0]['staff_name'],$booking_details_sms,$booking_details_sms,$sms_client_detail,$company_name,'','',$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo);
					
					$admin_sms_body = str_replace($search,$replace_with,$message);

					$textlocal_numbers = get_option('moveto_textlocal_ccode').get_option('moveto_textlocal_admin_phone_no');
					$textlocal_sender = urlencode($textlocal_sender);
					$admin_sms_body = rawurlencode($admin_sms_body);
					
					$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $admin_sms_body);
					
					$ch = curl_init('https://api.textlocal.in/send/');
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
		}
	}
	/******************* Send Sms code END *********************/
	
	
	if($_SESSION['mp_detail']['payment_method'] == 'paypal'){
		$thankyou_url = get_option('moveto_thankyou_page');
		if($thankyou_url!=''){
			?>
			<script>
				window.location.href = '<?php echo $thankyou_url; ?>';
			</script>
			<?php
		}else{
			?>
			<script>
				window.location.href = '<?php echo site_url().'/mp-thankyou/'; ?>';
			</script>
			<?php
		}
	}
	if($_SESSION['mp_detail']['payment_method'] == 'payumoney'){
		$thankyou_url = get_option('moveto_thankyou_page');
		if($thankyou_url!=''){
			?>
			<script>
				window.location.href = '<?php echo $thankyou_url; ?>';
			</script>
			<?php
		}else{
			?>
			<script>
				window.location.href = '<?php echo site_url().'/mp-thankyou/'; ?>';
			</script>
			<?php
		}
	}
	if($_SESSION['mp_detail']['payment_method'] == 'paytm_payment'){
		$thankyou_url = get_option('moveto_thankyou_page');
		if($thankyou_url!=''){
			echo $thankyou_url;
		}else{
			echo site_url().'/mp-thankyou/';
		}
	}
}
/* booking complete code here END */