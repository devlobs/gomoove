<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gomoove' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']p<gOp*mv;Js_}`+^t3|>Ar3%^HoB?r1P/794R5jBCZ/77*[9/%[sb<Rws5p<<i]' );
define( 'SECURE_AUTH_KEY',  '2A:k3X+O:-c]nfmUN.iwOn:]@Coo5]D);tLjBBWnB:Vy6F-ab&{GQr,fbA*v;XGq' );
define( 'LOGGED_IN_KEY',    'dc*l.|*dn5(we?Y]t(oUU>h^woodG4@,MA@3Y7]YVx^ k]NWe>>;!fb4iO8a33=0' );
define( 'NONCE_KEY',        'cGR&OHzNP]`V%/}9<k&<3m]mrI-.yY4S]Hl4[_>&#f8o-lp@zoWTv(yO_z%~!!qf' );
define( 'AUTH_SALT',        '}8+5Vtl$[MqCY}Qh4fz.CsT`[^:Yb !8&9%$so3v75QV7dUxG3luJP2<%0VEjjT8' );
define( 'SECURE_AUTH_SALT', 'y/kP*WS^[&UAKT3h-HJ#<(S]JTdk3FI!>pv%|!2N9UnFR:5}uj;m&QiS#&:BYDEj' );
define( 'LOGGED_IN_SALT',   '(%}E}sJOCPkx07d9Ko*F7]KF>wCI(87P^~m1ksxPe+e~32*t3Sz_RZeAvS#-Busg' );
define( 'NONCE_SALT',       '({V!0f%_R/8ITB^<]lJcLK~l>#x`uqz4IjBDJ} 2b(%_Zaw&v12}Nd^/`WsoxU[y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
