<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	$emailtemplates = new moveto_email_template();
	$smstemplates = new moveto_sms_template();
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	$moveto_currencysymbol_array =  array('ALL'=>'Lek', 'AFN'=>'؋', 'ARS'=>'$', 'AWG'=>'ƒ', 'AUD'=>'$', 'AZN'=>'ман', 'AED'=>'د.إ', 'ANG'=>'NAƒ',	'BSD'=>'$', 'BBD'=>'$', 'BYR'=>'p.', 'BZD'=>'BZ$', 'BMD'=>'$', 'BOB'=>'$b', 'BAM'=>'KM', 'BWP'=>'P', 'BGN'=>'лв', 'BRL'=>'R$', 'BND'=>'$', 'BDT'=>'Tk', 'BIF'=>'FBu',	 'KHR'=>'៛', 'CAD'=>'$', 'KYD'=>'$', 'CLP'=>'$', 'CNY'=>'¥', 'CYN'=>'¥', 'COP'=>'$', 'CRC'=>'₡', 'HRK'=>'kn', 'CUP'=>'₱', 'CZK'=>'Kč', 'CVE'=>'Esc', 'CHF'=>'CHF',	 'DKK'=>'kr', 'DOP'=>'RD$', 'DJF'=>'Fdj', 'DZD'=>'دج',	 'XCD'=>'$', 'EGP'=>'£', 'SVC'=>'$', 'EEK'=>'kr', 'EUR'=>'€', 'ETB'=>'Br', 'FKP'=>'£',	 'FJD'=>'$', 'GHC'=>'¢', 'GIP'=>'£', 'GTQ'=>'Q', 'GGP'=>'£', 'GYD'=>'$', 'GMD'=>'D', 'GNF'=>'FG', 'HNL'=>'L', 'HKD'=>'$', 'HUF'=>'Ft', 'HRK'=>'kn', 'HTG'=>'G',	 'ISK'=>'kr', 'INR'=>'Rs.', 'IDR'=>'Rp', 'IRR'=>'﷼', 'IMP'=>'£', 'ILS'=>'₪',	 'JMD'=>'J$', 'JPY'=>'¥', 'JEP'=>'£',	 'KZT'=>'лв', 'KPW'=>'₩', 'KRW'=>'₩', 'KGS'=>'лв', 'KES'=>'KSh', 'KMF'=>'KMF',	 'LAK'=>'₭', 'LVL'=>'Ls', 'LBP'=>'£', 'LRD'=>'$', 'LTL'=>'Lt',	 'MKD'=>'ден', 'MYR'=>'RM', 'MUR'=>'₨', 'MXN'=>'$', 'MNT'=>'₮', 'MZN'=>'MT', 'MDL'=>'MDL', 'MOP'=>'$', 'MRO'=>'UM', 'MVR'=>'Rf', 'MWK'=>'MK', 'MAD'=>'د.م.',	 'NAD'=>'$', 'NPR'=>'₨', 'ANG'=>'ƒ', 'NZD'=>'$', 'NIO'=>'C$', 'NGN'=>'₦', 'NOK'=>'kr', 'OMR'=>'﷼', 'PKR'=>'₨', 'PAB'=>'B/.', 'PYG'=>'Gs', 'PEN'=>'S/.', 'PHP'=>'₱', 'PLN'=>'zł', 'PGK'=>'K',	 'QAR'=>'﷼',	 'RON'=>'lei', 'RUB'=>'руб',	 'SHP'=>'£', 'SAR'=>'﷼', 'RSD'=>'Дин.', 'SCR'=>'₨', 'SGD'=>'$', 'SBD'=>'$', 'SOS'=>'S', 'ZAR'=>'R', 'LKR'=>'₨', 'SEK'=>'kr', 'CHF'=>'CHF', 'SRD'=>'$', 'SYP'=>'£', 'SLL'=>'Le', 'STD'=>'Db', 'TWD'=>'NT', 'THB'=>'฿', 'TTD'=>'TTD', 'TRY'=>'₤', 'TVD'=>'$', 'TOP'=>'T$', 'TZS'=>'x',	 'UAH'=>'₴', 'GBP'=>'£', 'USD'=>'$', 'UYU'=>'$U', 'UZS'=>'лв', 'UGX'=>'USh', 'VEF'=>'Bs', 'VND'=>'₫', 'VUV'=>'Vt',	 'WST'=>'WS$',	 'XAF'=>'BEAC', 'XOF'=>'BCEAO', 'XPF'=>'F',	 'YER'=>'﷼', 'ZWD'=>'Z$', 'ZAR'=>'R');
			
/* Update API Key */
if(isset($_POST['setting_action']) && $_POST['setting_action']=='update_api_key'){
		
		
		update_option('moveto_google_api_key',filter_var($_POST['moveto_api_key'], FILTER_SANITIZE_STRING));
		update_option('moveto_google_map_country',$_POST['moveto_country_flags']);
		update_option('moveto_google_map_center_restrictions',$_POST['country']);
		update_option('moveto_company_origin_destination_distance_status',$_POST['moveto_company_origin_destination_distance_status']);
		update_option('moveto_destination_company_distance',$_POST['moveto_destination_company_distance']);
		update_option('moveto_company_origin_address',$_POST['moveto_company_origin_address']);
		update_option('moveto_company_destination_address',$_POST['moveto_company_destination_address']);
			
}
/* Update Settings */
if(isset($_POST['setting_action']) && $_POST['setting_action']=='update_settings'){
	foreach($_POST as $option_name => $option_value){
		if($option_name=='moveto_allow_terms_and_conditions_url' || $option_name=='moveto_allow_privacy_policy_url'){
			$option_value = urlencode($option_value);
		}
		update_option($option_name,filter_var($option_value, FILTER_SANITIZE_STRING));
			
		if($option_name=='moveto_currency'){
			if(isset($moveto_currencysymbol_array[$option_value])){			
			$currencysymbol = $moveto_currencysymbol_array[$option_value];
			}else{
			$currencysymbol ='$';
			}
			update_option('moveto_currency_symbol',$currencysymbol);
		}
	}
}
/* Update Settings */
if(isset($_POST['setting_action']) && $_POST['setting_action']=='delete_company_image'){
		update_option('moveto_company_logo','');
		unlink($root.'/wp-content/uploads'.$_POST['mediapath']);
}
/* Update Email Templates */
if(isset($_POST['setting_action'],$_POST['template_id']) && $_POST['setting_action']=='update_emailtemplate' && $_POST['template_id']!=''){
		$emailtemplates->id = $_POST['template_id'];
		$emailtemplates->email_subject = $_POST['email_subject'];
		$emailtemplates->email_message = $_POST['email_message'];
		$emailtemplates->update_template_subject_message();

}
/* Update Email Template Status */
if(isset($_POST['setting_action'],$_POST['template_id']) && $_POST['setting_action']=='update_emailtemplate_status' && $_POST['template_id']!=''){
		$emailtemplates->id = $_POST['template_id'];
		$emailtemplates->template_status = $_POST['email_status'];
		$emailtemplates->update_template_status();

}
/* Update SMS Templates */
if(isset($_POST['setting_action'],$_POST['template_id']) && $_POST['setting_action']=='update_smstemplate' && $_POST['template_id']!=''){
		$smstemplates->id = $_POST['template_id'];
		$smstemplates->sms_message = $_POST['sms_message'];
		$smstemplates->update_template_subject_message();

}
/* Update SMS Template Status */
if(isset($_POST['setting_action'],$_POST['template_id']) && $_POST['setting_action']=='update_smstemplate_status' && $_POST['template_id']!=''){
		$smstemplates->id = $_POST['template_id'];
		$smstemplates->template_status = $_POST['sms_status'];
		$smstemplates->update_template_status();

}