<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Location */
	$room_type = new moveto_room_type();
	$general = new moveto_general();
	$mp_image_upload= new moveto_image_upload();
	$mp_currency_symbol = get_option('moveto_currency_symbol');

?>
<div id="mp-services-panel" class="panel tab-content table-fixed">
<div id="mp-service-addon-panel" class="panel tab-content table-fixed">
<div class="panel-body">
				
<!-- 		*************************1 BHK-Room Type**************************** -->

<div class="mp-service-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<ul class="breadcrumb">
                <li><?php echo __("Room Type","mp"); ?></li>
            </ul>
			<div class="mp-service-top-header">
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					
				</div>
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					<button id="mp-add-new-service-addons" class="btn btn-success pull-right" value="add new service"><i class="fa fa-plus icon-space "></i><?php echo __("Add Room Type","mp");?></button>
				</div>
			</div>
			<div id="hr"></div>
			<div class="tab-pane active" id="">
				<div class="tab-content mp-services-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-services" > <!-- sortable-services -->
							<?php 
							
							$mp_room_type = $room_type->readAll_room_type();							
							if(sizeof((array)$mp_room_type)>0){
							foreach($mp_room_type as $mp_room_types){  ?>
							<li id="service_detail_<?php echo $mp_room_types->id; ?>" class="panel panel-default mp-services-panel" >							
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="col-lg-9 col-sm-10 col-xs-12">
											
											<span class="mp-service-title-name f-letter-capitalize"><?php echo $mp_room_types->name; ?></span>
											
											<div class="pull-right">
												<a href="?page=room_article_category&sid=<?php echo $mp_room_types->id; ?>" class="btn btn-success mp-btn-width mp-btn-room"><?php echo __("Article Cat","oct");?></a>
											</div>

										</div>
										<div class="col-lg-3 col-sm-2 col-xs-12 np">	
												<div class="mp-col6 np">
												</div>
												<div class="mp-col4 np">
													<label for="sevice-endis-<?php echo $mp_room_types->id; ?>">
														<input data-room_t_id="<?php echo $mp_room_types->id; ?>" type="checkbox" class="update_room_types_status" id="sevice-endis-<?php echo $mp_room_types->id; ?>" <?php if($mp_room_types->status=='E'){echo 'checked'; } ?> data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" >
													</label>
												</div>								
											<div class="pull-right">
												
												<div class="mp-col1 np">
												<a data-poid="mp-popover-delete-service<?php echo $mp_room_types->id; ?>" id="mp-delete-service<?php echo $mp_room_types->id; ?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete This Room Type?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete Addon","mp");?>"></i></a>
													<div class="mp-popover" id="mp-popover-delete-service<?php echo $mp_room_types->id; ?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<?php /* if($service->total_service_bookings()>0){ */if(0>0){?>
																		<span class="mp-popover-title"><?php echo __("Unable to delete article,having lead","mp");?></span>
																		<?php }else{?>		
																		<button data-room_types_id="<?php echo $mp_room_types->id; ?>" value="Delete" class="btn btn-danger btn-sm mr-10 delete_room_types" type="submit"><?php echo __("Yes","mp");?></button>
																		<button data-poid="mp-popover-addon<?php echo $mp_room_types->id; ?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button><?php } ?>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $mp_room_types->id; ?>" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="<?php echo $mp_room_types->id; ?>"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="package-detail" class="package-detail service_detail panel-collapse collapse detail-id_<?php echo $mp_room_types->id; ?>">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form data-sid="<?php echo $mp_room_types->id; ?>" id="mp_create_room_type_frm<?php echo $mp_room_types->id; ?>" method="post" type="" class="slide-toggle mp_create_room_type_frm"  >
												<table class="mp-create-service-table">
													<tbody>
														<input type="hidden" name="addon_service_id" id="addon_service_id" value="1" />
														<tr>
															<td><label for="mp-service-title<?php echo $mp_room_types->id; ?>"><?php echo __("Name","mp");?></label></td>
															<td><input type="text" name="room_type_title" class="form-control" id="room_types_title<?php echo $mp_room_types->id; ?>" value="<?php echo $mp_room_types->name; ?>" /></td>
														</tr>	
												<tr>
													<td></td>
													<td>
														<button data-update_room_types_id="<?php echo $mp_room_types->id; ?>" name="" class="btn btn-success mp-btn-width update_room_types" type="button"><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>	</tbody>
												</table>
											</form>
											
										</div>											
									</div>
								</div>
							</li>
							<?php }
							}else{
								echo __("No Room Type Found.","mp");
							} ?>
							<!-- add new service pop up -->
							<li>
							<div class="panel panel-default mp-services-panel mp-add-new-service-addons">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="mp-col6">
											<span class="mp-service-title-name"><?php echo __("Add New Room-Type","mp");?></span>		
										</div>
										<div class="pull-right mp-col6">					
											<div class="pull-right">
													<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addservice" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="addservice"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="package-detail" class="package-detail service_detail panel-collapse collapse in detail-id_addservice">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form id="mp_create_room_type_frm" method="post" type="" class="slide-toggle" >
												<table class="mp-create-service-table">
													<tbody>
														<input type="hidden" name="addon_service_id" id="addon_service_id" value="1" />
														<tr>
															<td><label for="mp-service-title"><?php echo __("Name","mp");?></label></td>
															<td><input type="text" name="room_type_title" class="form-control" id="room_type_title" /></td>
														</tr>
														
												<tr>
													<td></td>
													<td>
														<button id="mp_create_room_type" name="mp_create_room_type" class="btn btn-success mp-btn-width col-md-offset-4" type="button" ><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
										
										</tbody>
												</table>
											
										</div>
										
										</form>									
									</div>
								</div>
							</div>
							</li>
							
							</ul>
						</div>	
					</div>

				</div>
			</div>
			
		</div>
		<!-- **************************************************** -->
	</div>
</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var serviceObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>