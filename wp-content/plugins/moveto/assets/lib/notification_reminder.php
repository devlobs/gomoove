<?php 
/* if access from public url */
if( $_SERVER['REMOTE_ADDR'] != $_SERVER['SERVER_ADDR'] ){
    die('access is not permitted');
}

	include_once(dirname(dirname(dirname(__FILE__))).'/objects/plivo.php');
	require_once dirname(dirname(dirname(__FILE__))).'/assets/Twilio/autoload.php'; 
	use Twilio\Rest\Client;

	$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));		
	if (file_exists($root.'/wp-load.php')) {		
		require_once($root.'/wp-load.php');		
	}
	/* error_reporting(E_ALL); ini_set('display_errors', 1); */
	$AccountSid = get_option('moveto_twilio_sid');
	$AuthToken =  get_option('moveto_twilio_auth_token');
	
  function set_content_type() {
	return 'text/html';
  }
  
  $movetobj = new moveto_booking(); 
  $mp_service = new moveto_service();
  $email_template = new moveto_email_template();
  $msg_template = $email_template->email_parent_template;	
  $obj_sms_template = new moveto_sms_template();
  $moveto_additional_info = new moveto_additional_info();
  $bookings = $movetobj->read_all_upcoming_bookings_reminder_notification();
  $moveto_additional_info = new moveto_additional_info();
  $roomtype_obj=new moveto_room_type();
  $moveto_article_category_obj=new moveto_article_category();
  
  $currtime = date_i18n('Y-m-d H:i:s');   	
  $company_name = get_option('moveto_company_name');
  $company_address = get_option('moveto_company_address');
  $company_city = get_option('moveto_company_city');
  $company_state = get_option('moveto_company_state');
  $company_zip = get_option('moveto_company_zip');
  $company_country = get_option('moveto_company_country');
  $company_phone = get_option('moveto_company_country_code').get_option('moveto_company_phone');
  $company_email = get_option('moveto_company_email');
  $company_logo = $business_logo = site_url()."/wp-content/uploads/".get_option('moveto_company_logo');
  $admin_name = get_option('moveto_email_sender_name');		
  $sender_email_address = get_option('moveto_email_sender_address');		
  $headers = "From: $admin_name <$sender_email_address>" . "\r\n";
  
  foreach($bookings as $single_booking){
	  $service_start_time = date_i18n('Y-m-d H:i:s',strtotime($single_booking->booking_date));
	  $date1=strtotime($service_start_time);		
	  $date2=strtotime($currtime);		
	  $diff  = abs($date1 - $date2);		
	  $rem_minutes   = round($diff / 60);		
	  $reminder_buffer_time = get_option('moveto_email_reminder_buffer'); 
	  
	 if($reminder_buffer_time >= $rem_minutes){
				  
		$movetobj->reminder = 1;
		$movetobj->id=$single_booking->id;
		$movetobj->update_booking_reminder_buffer_status();
				
		$source_city = $single_booking->source_city;
		$destination_city = $single_booking->destination_city;
		$booking_date = $single_booking->booking_date;
		$order_id = $single_booking->order_id;
		$quote_price = $single_booking->quote_price;
		$quote_detail = $single_booking->quote_detail;
			
		if($single_booking->service_id==1){
			$service_title = __('Home','mp');	
		}elseif($single_booking->service_id==2){
			$service_title = __('Office','mp');
		}elseif($single_booking->service_id==3){
			$service_title = __('Vehicle','mp');
		}elseif($single_booking->service_id==4){
			$service_title = __('Pets','mp');
		}elseif($single_booking->service_id==6){
			$service_title = __('Other','mp');
		}else{
			$service_title = __('Commercial','mp');
		}
		  
		if($single_booking->booking_status=='D'){
			$bookingstatus = __('Completed','mp');
		}else{
			$bookingstatus = __('Pending','mp');
		}
		/* Get User Info */
		$user_info = '';
		$client_email = '';
		$client_phone = '';
		$client_name = '';
		if($single_booking->user_info!=''){										
			$user_info_arr = unserialize($single_booking->user_info);	
			$client_email = $user_info_arr["email"];
			$client_phone = $user_info_arr["user_phone"];
			$client_name = $user_info_arr["first_name"].' '.$user_info_arr["last_name"];
			
			$user_gender = __('Male','mp');
			if($user_info_arr["user_gender"]=='F'){
				$user_gender = __('Female','mp');
			}									
			$user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Name',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["name"].'</p></div>';
			$user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Email',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["email"].'</p></div>';
			$user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Phone',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["phone"].'</p></div>';
			/* $user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Gender',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_gender.'</p></div>';
			$user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["user_address"].'</p></div>';				
			$user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["user_city"].'</p></div>';					
			$user_info .='<div style="width:300px; padding:13px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$user_info_arr["user_state"].'</p></div>'; */

		}
		
		
		
		/* Get Service Info */
		$service_info = '';
		if($single_booking->service_info!=''){
						$service_info .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Article Details","mp").'</div>';
						$service_info_arr = unserialize($single_booking->service_info);
						
						foreach($service_info_arr as $value){	
						
						$service_info .= '<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$value['get_article_name'].'</strong></div>';

						$service_info .= '<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__("Article Name","mp").'</strong><p style="width:40%;text-align:right;float:left;margin:0;">'.$value['get_article_name'].'*'.$value['count'].'</p></div>';
						}
		}
		/* Get Additional Info */
		$additional_info = '';
		if($single_booking->additional_info!=''){
			
			$additional_info_arr = unserialize($single_booking->additional_info);
						if(sizeof((array)$additional_info_arr['favorite'])>0){

					$additional_info .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Additional Information","mp").'</div>';

						$returndata = $moveto_additional_info->readAll();

						$additionalArr = array();
						foreach ($additional_info_arr['favorite'] as $value) {
						array_push($additionalArr, $value);
						}
							foreach($returndata as $additional){

								if(in_array($additional->id, $additionalArr)){
									$additional_info .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$additional->additional_info.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">Yes</p></div>';
									
								}else
								{
									$additional_info .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$additional->additional_info.'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">No</p></div>';
								}
							}
					}
						
		}
		
		/* Get Loading Info */
		$loading_info = '';
		if($single_booking->loading_info!=''){										
			$loading_info_arr = unserialize($single_booking->loading_info);				
			$loading_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Loading Details","mp").'</div>';

					$loading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Floor No',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_floor_number"].'</p></div>';

					$loading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_one"].'</p></div>';

					$loading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_two"].'</p></div>';

					$loading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_three"].'</p></div>';

					$loading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Elevator Available?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["load_elevator_checked"].'</p></div>';

					$loading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Packaging Required?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["packaging_checked"].'</p></div>';										
		}
		/* Get UnLoading Info */
		$unloading_info = '';
		if($single_booking->unloading_info!=''){										
			$unloading_info_arr = unserialize($single_booking->unloading_info);				
			$unloading_info_detail .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Unloading Details","mp").'</div>';

					$unloading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Floor No',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_floor_number"].'</p></div>';

					$unloading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_one"].'</p></div>';

					$unloading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_two"].'</p></div>';

					$unloading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_three"].'</p></div>';

					$unloading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Elevator Available?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unload_elevator_checked"].'</p></div>';

					$unloading_info_detail .='<div style="width:300px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unpackaging Required?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unpackaging_checked"].'</p></div>';
		}
			
		
		$search = array('{{client_name}}','{{admin_name}}','{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}','{{order_id}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{articles_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_price}}','{{quote_detail}}');      
					
		$replace_with = array($client_name,$admin_name,$company_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo,$order_id,base64_decode($source_city),base64_decode($destination_city),date_i18n(get_option('date_format'),strtotime($booking_date)),$bookingstatus,$service_info,$articles_info,$loading_info,$unloading_info,$additional_info,$user_info,$quote_price,$quote_detail); 
		
				
		
		/******************* Send Email Notification *********************/
		
		/* Send email to Admin when booking is complete */
		if(get_option('moveto_admin_email_notification_status')=='E'){
			$mp_adminemail_templates = new moveto_email_template();
			$msg_template = $mp_adminemail_templates->email_parent_template;
			$mp_adminemail_templates->email_template_name = "RMA";  		
			$template_detail = $mp_adminemail_templates->readOne();        
			if($template_detail[0]->email_message!=''){            
				$email_content = $template_detail[0]->email_message;        
			}else{            
				$email_content = $template_detail[0]->default_message;
			}        
			$email_subject = $template_detail[0]->email_subject;
			$email_admin_message = '';				
			$email_admin_message = str_replace($search,$replace_with,$email_content);	
			$email_admin_message = str_replace('###msg_content###',$email_admin_message,$msg_template);
			add_filter( 'wp_mail_content_type', 'set_content_type' );		
			$status = wp_mail(get_option('moveto_email_sender_address'),$email_subject,$email_admin_message,$headers);	
		}
		/* Send Email Notification End Here */
		
		
		
		/******************* Send SMS Notification *********************/
		if(get_option("moveto_sms_reminder_status") == "E"){		
			/* Send SMS To Admin */
			if(get_option('moveto_plivo_admin_sms_notification_status') == "E"){					
					$p_admin = new Plivo\RestAPI($auth_id, $auth_token, '', '');					
					$template = $obj_sms_template->gettemplate_sms("AM",'e','RMA');					
					if($template[0]->sms_template_status == "e" && get_option('moveto_plivo_admin_phone_no')!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}						
						$admin_sms_body = str_replace($search,$replace_with,$message);
						$adminparams = array(
						'src' => $plivo_sender_number,
						'dst' => get_option('moveto_plivo_ccode').get_option('moveto_plivo_admin_phone_no'),
						'text' => $admin_sms_body,
						'method' => 'POST'
						);
						$response = $p_admin->send_message($adminparams);
					} 
				}			
			/* Plivo SMS Sending End Here */
			
			
			/*******************  SMS sending code via Twilio  **************/
			   /* Send SMS To Admin */
			   if(get_option('moveto_twilio_admin_sms_notification_status') == "E"){		   
				$twilliosms_admin = new Client($AccountSid, $AuthToken);
				$template = $obj_sms_template->gettemplate_sms("AM",'e','RMA');					
					if($template[0]->sms_template_status == "e" && get_option('moveto_twilio_admin_phone_no')!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$admin_sms_body = str_replace($search,$replace_with,$message);
						$twilliosms_admin->messages->create(
							get_option('moveto_twilio_ccode').get_option('moveto_twilio_admin_phone_no'),
							array(
								'from' => $twillio_sender_number,
								'body' => $admin_sms_body 
							)
						);
						
					}		
			   }				
			
			/* Twilio SMS Sending End Here */
			
			/*******************  SMS sending code via Nexmo  **************/
			if(get_option('moveto_sms_noti_nexmo')=="E"){
			  include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_nexmo.php');
			  $nexmo_client = new moveto_nexmo();
			  $nexmo_client->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');
			  $nexmo_client->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');
			  $nexmo_client->moveto_nexmo_form = get_option('moveto_nexmo_form');
			 /* Send SMS To Admin */
			  if(get_option('moveto_nexmo_send_sms_admin_status') == "E"){
				$template = $obj_sms_template->gettemplate_sms("AM",'e','RMA');					
					if($template[0]->sms_template_status == "e" && get_option('moveto_nexmo_admin_phone_no')!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$admin_sms_body = str_replace($search,$replace_with,$message);
						$nexmo_client->send_nexmo_sms(get_option('moveto_nexmo_ccode').get_option('moveto_nexmo_admin_phone_no'),$admin_sms_body);
					}
			  }
			  
			}
			/* Nexmo SMS Sending End Here */
			
			/*******************  SMS sending code via TEXTLOCAL  **************/
			if(get_option('moveto_sms_noti_textlocal')=="E"){
			  $textlocal_apikey = get_option('moveto_textlocal_apikey');
			  $textlocal_sender = get_option('moveto_textlocal_sender');
			  /* Send SMS To Admin */
			  if(get_option('moveto_textlocal_admin_sms_notification_status') == "E"){
					$template = $obj_sms_template->gettemplate_sms("AM",'e','RMA');					
					if($template[0]->sms_template_status == "e" && get_option('moveto_textlocal_admin_phone_no')!=''){
						if($template[0]->sms_message == ""){
							$message = strip_tags($template[0]->default_message);
						}else{
							$message = strip_tags($template[0]->sms_message);
						}
						$admin_sms_body = str_replace($search,$replace_with,$message);
						
						$textlocal_numbers = get_option('moveto_textlocal_ccode').get_option('moveto_textlocal_admin_phone_no');
						$textlocal_sender = urlencode($textlocal_sender);
						$admin_sms_body = rawurlencode($admin_sms_body);
						
						$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $admin_sms_body);
						
						$ch = curl_init('https://api.textlocal.in/send/');
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						$result = curl_exec($ch);
						curl_close($ch);
					}
			  }
			  
			}/* TEXTLOCAL SMS Sending End Here */
		}	
	 }						
}
?>