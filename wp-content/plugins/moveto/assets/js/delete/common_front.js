
/* Custom nice scroll bar */
jQuery(document).ready(function () {
	
	jQuery("#main").niceScroll({cursorcolor:"#5BC0DE",cursoropacitymax:0.7,touchbehavior:true});
	//jQuery(".bdp-wrapper-inner-class").niceScroll();
	
	// jQuery("#calendar").niceScroll({cursorcolor:"#5BC0DE",cursoropacitymax:0.7,touchbehavior:true});
	
});

jQuery(document).bind('ready ajaxComplete',function(){
		var default_flag = general_settings_default_flag.default_flag;
	  /* jQuery("#mp-phone").intlTelInput(); */
	 jQuery("#mp-phone").intlTelInput({'initialCountry':default_flag});
	
});

/***/
jQuery(document).on('keyup keydown blur','.add_show_error_class',function(event){
 jQuery('.ct-loading-main').hide();
    var id = jQuery(this).attr('id');
    var Number = /(?:\(?\+\d{2}\)?\s*)?\d+(?:[ -]*\d+)*$/;
    if(jQuery(this).hasClass('error')){
        jQuery( this ).removeClass('error');
        jQuery( "#"+id ).parent().removeClass('error');
        jQuery( this ).addClass('show-error');

        jQuery( "#"+id ).parent().addClass('show-error');
        /* if(jQuery('#ct-user-phone').val() != ''){
            if(!jQuery('#ct-user-phone').val().match(Number)){
                jQuery( '.intl-tel-input' ).parent().addClass('show-error');
            }
        } */
    }else{
        jQuery( this ).removeClass('error');
        jQuery( "#"+id ).parent().removeClass('error');
        jQuery( this ).removeClass('show-error');
        jQuery( "#"+id ).parent().removeClass('show-error');
       /*  if(jQuery('#ct-user-phone').val() != ''){
            if(jQuery('#ct-user-phone').val().match(Number)){
                jQuery( '.intl-tel-input' ).parent().removeClass('show-error');
            }
        } */
  
    }
});
/**/


/* front details user date of birth picker */
jQuery(document).on('ready ajaxComplete', function(){
	jQuery( "#datepicker" ).datepicker({ maxDate: 0,
		changeMonth: true,
		changeYear: true});
 });

/*
  $( function() {
    // run the currently selected effect
    function runEffect() {
      // get effect type from
      var selectedEffect = $( "#effectTypes" ).val();
 
      // Most effect types need no options passed by default
      var options = {};
      // some effects have required parameters
      if ( selectedEffect === "scale" ) {
        options = { percent: 50 };
      } else if ( selectedEffect === "transfer" ) {
        options = { to: "#button", className: "ui-effects-transfer" };
      } else if ( selectedEffect === "size" ) {
        options = { to: { width: 200, height: 60 } };
      }
 
      // Run the effect
      $( "#effect" ).effect( selectedEffect, options, 500, callback );
    };
 
    // Callback function to bring a hidden box back
    function callback() {
      setTimeout(function() {
        $( "#effect" ).removeAttr( "style" ).hide().fadeIn();
      }, 1000 );
    };
 
    // Set effect from select menu value
    $( "#button" ).on( "click", function() {
      runEffect();
      return false;
    });
  } );
*/ 
 

jQuery(document).on('ready ajaxComplete', function(){
	  jQuery("#user_phone").intlTelInput({
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: false,
      // dropdownContainer: "body",
      // excludeCountries: ["us"],
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // initialCountry: "auto",
      // nationalMode: false,
      // numberType: "MOBILE",
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "js/utils.js"
	});
});
 /* user new and existing radio show hide fields */
jQuery(document).on('click', '#bdp-existing-user', function(){
	jQuery('.existing-user-login').show( "blind", {direction: "vertical"}, 1000 );
	jQuery('.new-user-area').hide();
	
});
jQuery(document).on('click', '#bdp-new-user', function(){			
	jQuery('.new-user-area').show( "blind", {direction: "vertical"}, 1000 );
	jQuery('.existing-user-login').hide();
	
}); 
jQuery(document).on('click', '#bdp-guest-user', function(){			
	jQuery('.new-user-area').show( "blind", {direction: "vertical"}, 1000 );
	jQuery('.existing-user-login').hide();
	
}); 
/* payment methods */
jQuery(document).on('click','.payment_checkbox',function() {
	if(jQuery('#stripe-payments').is(':checked') || jQuery('#2checkout-payments').is(':checked') || jQuery('#authorize-payments').is(':checked') ) {
		jQuery('#stripe-payment-main').fadeIn("slow"); } else {
			
		 jQuery('#stripe-payment-main').fadeOut("slow");
	}
	// if(jQuery('#ak_payment_authorizenet').is(':checked')) { jQuery('#authorizenet_fields').fadeIn("slow"); } else {
		 // jQuery('#authorizenet_fields').fadeOut("slow");
	 // }

});

jQuery(document).on('click', '#show-pass', function(){
	if (jQuery(".user-password").attr("type")=="password") {
		jQuery(".user-password").attr("type", "text");
	}
	else{
		jQuery(".user-password").attr("type", "password");
	}
});
/* slide effect on click next button to next step  */


/* 1 show hide locaion details */

jQuery(document).on('click','.bdp_view_map', function() {
	var locid = jQuery(this).data('view_map_id');
	jQuery('.bdp-location-map').each(function(){
		if(locid==jQuery(this).data('view_map_id')){ return; }
		jQuery(this).hide( "blind", {direction: "vertical"}, 500 );	
	});	
	jQuery('#'+jQuery(this).data('view_map_id')).toggle( "blind", {direction: "vertical"}, 1000 );
});




/* On page load show 1. locations*/
jQuery(document).ready(function () {
	if(obj_multi_locations.option_value == "E"){
	/* Location */
	jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	var labels_head = nav_labels;	
	/* top progress bar */
	jQuery('.bdp-locations').addClass('is-active');
	jQuery("#bdp_previous_btn").css("display","none");
	var getdata =  {key:'new',value:'alsonew',action:'get_locations_view'}
	jQuery('.bdp-loader').css("display","block");
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('#bdp_wrapper_inner').html(response);
			jQuery(".content-header").html("1. "+labels_head.location_msg);
			jQuery('.mp-loading-main').hide();
		}
	}); 
	jQuery('.bdp-loader').css("display","none");
	}else{
		/* Service */
		jQuery('.mp-loading-main').show();
	
		ajaxurl = obj_locations.ajaxurl;
		var labels_head = nav_labels;
		location_id = "0";
		/* location_id = jQuery(this).data('lid'); */
		jQuery('.bdp_locations').each(function(){
			jQuery(this).removeClass('selected');		
		});	
		jQuery('#lid'+jQuery(this).data('lid')).addClass('selected');
		
		jQuery("#bdp_previous_btn").css("display","inline-block");
		/* top progress bar */
		jQuery('.bdp-locations').removeClass('is-active');
		jQuery('.bdp-locations').addClass('is-complete');
		jQuery('.bdp-services').addClass('is-active');
		
		
		var getdata =  {id:location_id,action:'get_services_view'}
		jQuery('.bdp-loader').css("display","block");

		jQuery.ajax({
			url  : ajaxurl+"/assets/lib/front_ajax.php",
			type : 'POST',
			data : getdata, 
			dataType : 'html',
			success  : function(response) {
				jQuery('#bdp_wrapper_inner').html(response);
				jQuery(".content-header").html("1. "+labels_head.service_msg);
				jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" }, 500);
				jQuery('.mp-loading-main').hide();
			}
		}); 
		jQuery('.bdp-loader').css("display","none");
	}
	
	
});




/* 2. show services list on select location*/
jQuery(document).on('click','.bdp_location_select', function() {
	jQuery('.mp-loading-main').show();
	
	ajaxurl = obj_locations.ajaxurl;
	var labels_head = nav_labels;
	location_id = jQuery(this).data('lid');
	jQuery('.bdp_locations').each(function(){
		jQuery(this).removeClass('selected');		
	});	
	jQuery('#lid'+jQuery(this).data('lid')).addClass('selected');
	
	jQuery("#bdp_previous_btn").css("display","inline-block");
	/* top progress bar */
	jQuery('.bdp-locations').removeClass('is-active');
	jQuery('.bdp-locations').addClass('is-complete');
	jQuery('.bdp-services').addClass('is-active');
	
	
	var getdata =  {id:location_id,action:'get_services_view'}
	jQuery('.bdp-loader').css("display","block");

	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('#bdp_wrapper_inner').html(response);
			jQuery(".content-header").html("2. "+labels_head.service_msg);
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" }, 500);
			jQuery('.mp-loading-main').hide();
		}
	}); 
	jQuery('.bdp-loader').css("display","none");
});


/*************************/
/* 2 show hide service details */

jQuery(document).on('click','.bdp_service_detail', function() {
	
	var serid = jQuery(this).data('view_service_id');
	
	
	jQuery('.bdp_service_detail').each(function(){
		
		crr_id = jQuery(this).data('view_service_id');
		
		if(serid==crr_id){ 
		jQuery('#link_'+serid).toggleClass('ser-checked'); jQuery('#detail_'+serid).toggle( "blind", {direction: "vertical"}, 1000 ); return;  }
		
		jQuery(this).removeClass('ser-checked'); 
		
		jQuery('#detail_'+crr_id).hide( "blind", {direction: "vertical"}, 500 );	

		
	});	
	
	
	
}); 

jQuery(document).on('click','.close_service_detail', function() {
	jQuery('.bdp-service-details').each(function(){
		jQuery(this).hide( "blind", {direction: "vertical"}, 500 );	
	});	
}); 

/* 2. show providers list on select service */

jQuery(document).on('click','.bdp_service_select', function() {
	jQuery('.mp-loading-main').show();
	jQuery('.bdp_services').each(function(){
		jQuery(this).removeClass('selected');	
	});
	
	ajaxurl = obj_locations.ajaxurl;
	var labels_head = nav_labels;
	service_id=jQuery(this).data('serid');
	jQuery('#serid'+service_id).addClass('selected');

	jQuery('#'+jQuery(this).data('serid')).addClass('selected');
	
	/* top progress bar */
	jQuery('.bdp-services').removeClass('is-active');
	jQuery('.bdp-services').addClass('is-complete');
	jQuery('.bdp-provider').addClass('is-active');
	var getdata =  {id:service_id,action:'get_providers_view'}
		jQuery('.bdp-loader').css("display","block");
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('#bdp_wrapper_inner').html(response);
			jQuery(".content-header").html("3. "+labels_head.provider_msg);
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" }, 500);
			jQuery('.mp-loading-main').hide();
		}
		
	}); 
		jQuery('.bdp-loader').css("display","none");
});

/*************************/
/* 3 show hide providers details */

jQuery(document).on('click','.bdp_provider_detail', function() {
	jQuery(this).toggleClass('prov-checked');
	var provid = jQuery(this).data('view_provider_id');
	jQuery('.bdp-provider-details').each(function(){
		if(provid==jQuery(this).data('view_provider_id')){return; }
		jQuery(this).hide( "blind", {direction: "vertical"}, 500 );	
		jQuery(this).removeClass('prov-checked');
	});	
	
	jQuery('#'+jQuery(this).data('view_provider_id')).toggle( "blind", {direction: "vertical"}, 1000 );
}); 

jQuery(document).on('click','.close_provider_detail', function() {
	jQuery('.bdp-provider-details').each(function(){
		jQuery(this).hide( "blind", {direction: "vertical"}, 500 );	
	});	
}); 

/* 3. show calendar on select provider */
jQuery(document).on('click','.bdp_provider_select', function() {
	jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	provider_id = jQuery(this).data('proid');	
	
	jQuery('.bdp_providers').each(function(){
		//if(!jQuery(this).hasClass('selected'))
		jQuery(this).removeClass('selected');		
	});
	
	jQuery('#proid'+jQuery(this).data('proid')).addClass('selected');

	/* top progress bar */
	jQuery('.bdp-provider').removeClass('is-active');
	jQuery('.bdp-provider').addClass('is-complete');
	jQuery('.bdp-date-time').addClass('is-active');	
	var getdata =  {id:provider_id,action:'get_month_slot_view'}
	jQuery('.bdp-loader').css("display","block");
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('#bdp_wrapper_inner').html(response);
			jQuery(".content-header").html("4. Select Date & Time");
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" }, 500);
			jQuery('.mp-loading-main').hide();
		}
	}); 
		jQuery('.bdp-loader').css("display","none");
});


/* 4. show details on select date time */
/* jQuery(document).on('click','.time-slot', function() { */
jQuery(document).on('click','.time_slotss', function() {
	jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	jQuery('.bdp_datetime_slots').each(function(){
		jQuery(this).removeClass('selected');		
	});	
	jQuery('#'+jQuery(this).data('proid')).addClass('selected');
	// jQuery("#bdp_next_btn").css("display","none");
	/* top progress bar */
	jQuery('.bdp-date-time').removeClass('is-active');
	jQuery('.bdp-date-time').addClass('is-complete');
	jQuery('.bdp-details').addClass('is-active');	
	var slot_db_date_time = jQuery(this).data('slot_db_date_time');
	
	var getdata =  {slot_db_date_time:slot_db_date_time, action:'get_personal_details_view'}
	/* var getdata =  {slot_db_date_time:slot_db_date_time, action:'get_cart_view'} */
	jQuery('.bdp-loader').css("display","block");
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			var cp = jQuery.trim(response);
			var cp_details = cp.split('###########');
			jQuery('#bdp_wrapper_inner').html(cp_details[0]);
			jQuery('.cart_section').html(cp_details[1]);
			jQuery(".content-header").html("5. Personal Details & Payment");
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" }, 500);
			jQuery('#add_more_services').show();
			jQuery('#total_item').html(jQuery('.hdn_total').val());
			jQuery('.mp-loading-main').hide();
		}
	}); 
		jQuery('.bdp-loader').css("display","none");
});

jQuery(document).on("click",'.country',function(){
	var num_code=jQuery(this).data("dial-code");
	jQuery("#mp-phone").val('+'+num_code);
});


/* 5. show details checkout */
jQuery(document).on('click','.complete-checkout', function() {
	var check_user_type = jQuery('.check_user_type:checked').val();
	if(check_user_type == "New User"){
		var user_login_status = "N";
	}else if(check_user_type == "Guest User"){
		var user_login_status = "GU";
	}else{
		var user_login_status = "Y";
	}
	
	var thankupage_url = obj_thankspage_url.thankspage_url;
	
	jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	var siteurl = ob_siteurl.siteurl;
	var val_error_msg = error_labels;
	var booking_page_url = url_frontpage.site_url_frontpage;
	jQuery('#details-payment-list').addClass('selected');
			
	/*selected date time */
	var selected_date_time_from_cal = jQuery('#selected_date_time_from_cal').val();
	
	/* top progress bar */
	jQuery('.bdp-details').removeClass('is-active');
	jQuery('.bdp-details').addClass('is-complete');
	jQuery('.bdp-complete').addClass('is-active');
	// var getdata =  {action:'get_complete_view'}
	jQuery('.bdp-loader').css("display","block");
	/* var user_login_status = jQuery('#mp-user-login-status').val(); */
	
	/* For New User */
	
	if(user_login_status == "N"){
		
		jQuery('#frmbooking').validate({
			rules:{
				'mp_preff_username':{required:true,remote: {
								url  : ajaxurl+"/assets/lib/front_ajax.php",
								type: "POST",
								async: false,
								data: {
								username: function(){ return jQuery("#preff-username").val(); },
								action:"check_username"
								}
							}},
				'bdp-payment-options':{required:true},
				'mp_preff_password':{required:true},
				'mp_first_name':{required:true},
				'mp_last_name':{required:true},
				'mp_email':{
							required:true,
							email:true,
							remote: {
								url  : ajaxurl+"/assets/lib/front_ajax.php",
								type: "POST",
								async: false,
								data: {
								email: function(){ return jQuery("#mp-email").val(); },
								action:"check_useremail"
								}
							}
							},
			},
			messages:{
				'mp_preff_username':{
					required : val_error_msg.username_err_msg,
					remote:val_error_msg.username_already_exist_err_msg
					},
				'bdp-payment-options':{required:"select payment"},
				'mp_preff_password':{required : val_error_msg.password_err_msg},
				'mp_first_name':{required: val_error_msg.firstname_err_msg},
				'mp_last_name':{required: val_error_msg.lastname_err_msg},
				'mp_email':{
					required: val_error_msg.email_err_msg ,
					email:val_error_msg.email_err_msg ,
					remote:val_error_msg.email_already_err_msg},
			}
		});
		
		jQuery('.get_custom_field').each(function(){
			var name_field = jQuery(this).attr('name');
			var required_field = jQuery(this).data('required');
			var fieldlabel = jQuery(this).data('fieldlabel')
			if(required_field == "Y"){
				jQuery(this).rules("add",{ required : true, messages : { required : "Please Enter "+fieldlabel+""}});
			}
		});
		/* Below Code for new users */
		
		var dynamic_field_add = {};
		var preff_username = jQuery("#preff-username").val();
		var preff_password = jQuery("#preff-password").val();
		var first_name = jQuery("#first-name").val();
		var last_name = jQuery("#last-name").val();
		var user_email = jQuery("#mp-email").val();
		var notes = jQuery("#bdp_notes").val();
		var code = jQuery('li.active').attr("data-dial-code");
		var phone_pre = jQuery("#mp-phone").val();
		var phone = "+"+code+phone_pre;
		/* var phone = jQuery("#mp-phone").val(); */
		
		var payment_value = jQuery(".payment_value:checked").val();
		jQuery('.get_custom_field').each(function(){
			if(jQuery(this).data('fieldname') == "radio_group"){
				dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery('.get_custom_field:checked').val();
			}else{
			dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery(this).val();
			}
		});
		dynamic_field_add['notes'] = jQuery("#bdp_notes").val();
		
		var dataString = {preff_username:preff_username,preff_password:preff_password,first_name:first_name,last_name:last_name,user_email:user_email,notes:notes,dynamic_field_add:dynamic_field_add,selected_date_time_from_cal:selected_date_time_from_cal,phone:phone,action:"add_mp_new_user"}
	
		if(jQuery('#frmbooking').valid()){
			if(payment_value == "Pay At Venue"){
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/front_ajax.php",
					data : dataString,
					success:function(response){
						jQuery('.mp-loading-main').hide();
						if(jQuery.trim(response) == 'Booking success'){
							if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
							  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
							}
						}
					}
				});
			} else if(payment_value == "authorize"){
			var cc_card_num = jQuery('#card-number').val();
			var cc_exp_month = jQuery('#card-expiry').val();
			var cc_exp_year = jQuery('#card_exp_year').val();
			var cc_card_code = jQuery('#cvc-code').val();
			var first_name = jQuery("#first-name").val();
			var last_name = jQuery("#last-name").val();
			
			var user_email = jQuery("#mp-email").val();
			var phone = jQuery("#mp-phone").val();
				 var dataString = {
				  cc_card_num:cc_card_num,
				  cc_exp_month:cc_exp_month,
				  cc_exp_year:cc_exp_year,
				  cc_card_code:cc_card_code,
				  user_email:user_email,
				  last_name:last_name,
				  first_name:first_name,
				  phone:phone,
				 };
						jQuery('.mp-loading-main').show();
						jQuery.ajax({
							type:"POST",
							url  : ajaxurl+"/assets/lib/authorizenet_payment_process.php",
							data:dataString,
							success:function(response){
								var response_detail = jQuery.parseJSON(response);
								console.log(response_detail);
								if(response_detail.success==false){
									clicked=false; 
									} else {
										var transaction_id = response_detail.transaction_id;
										dataString['transaction_id'] = transaction_id;
										dataString['payment_method'] = 'authorize_payments'; 
										dataString['preff_username'] = preff_username; 
										dataString['preff_password'] = preff_password; 
										dataString['first_name'] = first_name; 
										dataString['last_name'] = last_name; 
										dataString['user_email'] = user_email; 
										dataString['dynamic_field_add'] = dynamic_field_add;
										dataString['selected_date_time_from_cal'] = selected_date_time_from_cal;
										dataString['phone'] = phone;
										dataString['action'] = 'add_mp_new_user'; 
										jQuery.ajax({
											type : "POST",
											url  : ajaxurl+"/assets/lib/front_ajax.php",
											data : dataString,
											success:function(response){
												if(jQuery.trim(response) == 'Booking success'){
													if(thankupage_url != '' && thankupage_url != null){
													 window.location.href = thankupage_url; 
													}else{
													 window.location.href = ajaxurl+'/frontend/mp_thankyou.php'; 
													}
												}
											}
										});					
								}
							}
						})

					} else if(payment_value == "2checkout") {
						
						var seller_id = twocheckout_Obj.sellerId;
						var publishable_Key = twocheckout_Obj.publishKey;
						/*  Called when token created successfully. */
						jQuery('.mp-loading-main').show();
						function successCallback(data) {
							/* Set the token as the value for the token input */
							var twmpoken = data.response.token.token;
							dataString['twmpoken'] = twmpoken;
							dataString['payment_method'] = "2checkout";
							dataString['action'] = 'add_mp_new_user';
							  jQuery.ajax({
									type : "POST",
									url  : ajaxurl+"/assets/lib/front_ajax.php",
									data : dataString,
									success:function(response){
										jQuery('.mp-loading-main').hide();
										if(jQuery.trim(response) == 'Booking success'){
											if(thankupage_url != '' && thankupage_url != null){
												window.location.href = thankupage_url;
											}else{
											  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
											}
										}
									}
								});	
						};

						/*  Called when token creation fails. */
						function errorCallback(data) {
							
							if (data.errorCode === 200) {
								clicked=false; 
								tokenRequest();
							} else {
								clicked=false; 
							}
						};
							function tokenRequest() {
							/* Setup token request arguments */
							
							var args = {
								sellerId: seller_id,
								publishableKey: publishable_Key,
								ccNo: jQuery('#card-number').val(),
								cvv: jQuery('#cvc-code').val(),
								expMonth: jQuery('#card-expiry').val(),
								expYear: jQuery('#card_exp_year').val()
							};
							/* Make the token request */
							TCO.loadPubKey('sandbox',function(){
								TCO.requestToken(successCallback, errorCallback, args);
							});
						};

						tokenRequest();
					} else if(payment_value == "Paypal"){
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/mp_pp_payment_process.php",
					data : dataString,
					success:function(response){
						jQuery('.mp-loading-main').hide();
						jQuery('#redirect_pp_url').html(response);
					}
				});
			}else if(payment_value == "stripe"){
				 var stripe_pubkey = stripeObj.p_key;
				 Stripe.setPublishableKey(stripe_pubkey);
				 
				 var cc_card_num = jQuery('#card-number').val();
				 var cc_exp_month = jQuery('#card-expiry').val();
				 var cc_exp_year = jQuery('#card_exp_year').val();
				 var cc_card_code = jQuery('#cvc-code').val();

				 dataString = {
				  cc_card_num:cc_card_num,
				  cc_exp_month:cc_exp_month,
				  cc_exp_year:cc_exp_year,
				  cc_card_code:cc_card_code
				 };

				 var stripeResponseHandler = function(status, response)
				 {
				  if (response.error) 
				  {
				   if(cc_card_num == ""){
					jQuery('#card-number').css('border','1px solid #ff0000');
				   }  
				   if(cc_exp_month == ""){
					jQuery('#card-expiry').css('border','1px solid #ff0000');
				   }  
				   if(cc_exp_year == ""){
					jQuery('#card_exp_year').css('border','1px solid #ff0000');
				   }  
				   if(cc_card_code == ""){
					jQuery('#cvc-code').css('border','1px solid #ff0000');
				   }
				  }
				  else 
				  {
				   var token = response.id;
				   function waitForElement()
				   {
					if(typeof token !== "undefined" && token != '')
					{
					 var st_token = token;
					 dataString['st_token'] = st_token;
					 dataString['preff_username'] = preff_username;
					 dataString['preff_password'] = preff_password;
					 dataString['first_name']     = first_name;
					 dataString['last_name']      = last_name;
					 dataString['user_email']     = user_email;
					 dataString['notes']          = notes;
				     dataString['dynamic_field_add'] = dynamic_field_add;
		             dataString['selected_date_time_from_cal'] = selected_date_time_from_cal;
					 dataString['phone'] = phone;
					 dataString['payment_method'] = "stripe";
					 dataString['action'] = "add_mp_new_user";
					 jQuery.ajax({
					  type:"POST",
					  url:ajaxurl+"/assets/lib/front_ajax.php",
					  data:dataString,
					  success:function(response){
						jQuery('.mp-loading-main').hide();					
						if(jQuery.trim(response) == 'Booking success'){
							if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
							  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
							}
						}
					  }
					 });
					} 
					else
					{
					 setTimeout(function(){ waitForElement(); },2000);
					}
				   }
				  waitForElement();
				  }
				 };
					Stripe.card.createToken({
						number: jQuery('#card-number').val(),
						cvc: jQuery('#cvc-code').val(),
						exp_month: jQuery('#card-expiry').val(),
						exp_year: jQuery('#card_exp_year').val()
					}, stripeResponseHandler);
			} else {
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/front_ajax.php",
					data : dataString,
					success:function(response){
						jQuery('.mp-loading-main').hide();
						if(jQuery.trim(response) == 'Booking success'){
							if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
							  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
							}
						}
					}
				});
			}
		}else{
			jQuery('.mp-loading-main').hide();
		}
	}else if(user_login_status == "GU"){
		
		jQuery('#frmbooking').validate({
			rules:{
				'mp_preff_username':{required:true,remote: {
								url  : ajaxurl+"/assets/lib/front_ajax.php",
								type: "POST",
								async: false,
								data: {
								username: function(){ return jQuery("#preff-username").val(); },
								action:"check_username"
								}
							}},
				'mp_preff_password':{required:true},
				'mp_first_name':{required:true},
				'mp_last_name':{required:true},
				'mp_email':{
							required:true,
							email:true,
							async: false,
							remote: {
								url  : ajaxurl+"/assets/lib/front_ajax.php",
								type: "POST",
								data: {
								email: function(){ return jQuery("#mp-email").val(); },
								action:"check_useremail"
								}
							}
							},
			},
			messages:{
				'mp_preff_username':{
					required : val_error_msg.username_err_msg,
					remote:val_error_msg.username_already_exist_err_msg
					},
				'mp_preff_password':{required : val_error_msg.password_err_msg},
				'mp_first_name':{required: val_error_msg.firstname_err_msg},
				'mp_last_name':{required: val_error_msg.lastname_err_msg},
				'mp_email':{
					required: val_error_msg.email_err_msg ,
					email:val_error_msg.email_err_msg ,
					remote:val_error_msg.email_already_err_msg},
			}
		});
		
		jQuery('.get_custom_field').each(function(){
			var name_field = jQuery(this).attr('name');
			var required_field = jQuery(this).data('required');
			var fieldlabel = jQuery(this).data('fieldlabel')
			if(required_field == "Y"){
				jQuery(this).rules("add",{ required : true, messages : { required : "Please Enter "+fieldlabel+""}});
			}
		});
		/* Below Code for new users */
		
		var dynamic_field_add = {};
		var preff_username = jQuery("#preff-username").val();
		var preff_password = jQuery("#preff-password").val();
		var first_name = jQuery("#first-name").val();
		var last_name = jQuery("#last-name").val();
		var user_email = jQuery("#mp-email").val();
		var notes = jQuery("#bdp_notes").val();
		var phone = jQuery("#mp-phone").val();
		
		var payment_value = jQuery(".payment_value:checked").val();
		jQuery('.get_custom_field').each(function(){
			if(jQuery(this).data('fieldname') == "radio_group"){
				dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery('.get_custom_field:checked').val();
			}else{
			dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery(this).val();
			}
		});
		dynamic_field_add['notes'] = jQuery("#bdp_notes").val();
		var user_status = "guest_user";
		
		var dataString = {preff_username:preff_username,preff_password:preff_password,first_name:first_name,last_name:last_name,user_email:user_email,notes:notes,dynamic_field_add:dynamic_field_add,selected_date_time_from_cal:selected_date_time_from_cal,phone:phone,user_status:user_status,action:"add_mp_guest_user"}
		if(jQuery('#frmbooking').valid()){
			if(payment_value == "Pay At Venue"){
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/front_ajax.php",
					data : dataString,
					success:function(response){
						jQuery('.mp-loading-main').hide();
						if(jQuery.trim(response) == 'Booking success'){
							
							if(thankupage_url != '' && thankupage_url != null){
								
								window.location.href = thankupage_url;
							}else{
								
							  /* window.location.href = booking_page_url; */
							  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
							}
						}
					}
				});
			} else if(payment_value == "authorize"){
			var cc_card_num = jQuery('#card-number').val();
			var cc_exp_month = jQuery('#card-expiry').val();
			var cc_exp_year = jQuery('#card_exp_year').val();
			var cc_card_code = jQuery('#cvc-code').val();
			var first_name = jQuery("#first-name").val();
			var user_email = jQuery("#mp-email").val();
			var phone = jQuery("#mp-phone").val();
				 var dataString = {
				  cc_card_num:cc_card_num,
				  cc_exp_month:cc_exp_month,
				  cc_exp_year:cc_exp_year,
				  cc_card_code:cc_card_code,
				  user_email:user_email,
				  first_name:first_name,
				  phone:phone,
				 };
						jQuery('.mp-loading-main').show();
						jQuery.ajax({
							type:"POST",
							url  : ajaxurl+"/assets/lib/authorizenet_payment_process.php",
							data:dataString,
							success:function(response){
								var response_detail = jQuery.parseJSON(response);
								console.log(response_detail);
								if(response_detail.success==false){
									clicked=false; 
									} else {
										var transaction_id = response_detail.transaction_id;
										dataString['transaction_id'] = transaction_id;
										dataString['payment_method'] = 'authorize_payments'; 
										dataString['action'] = 'add_mp_guest_user'; 
										jQuery.ajax({
											type : "POST",
											url  : ajaxurl+"/assets/lib/front_ajax.php",
											data : dataString,
											success:function(response){
												if(jQuery.trim(response) == 'Booking success'){
													if(thankupage_url != '' && thankupage_url != null){
														window.location.href = thankupage_url;
													}else{
													  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
													}
												}
											}
										});					
								}
							}
						})

					} else if(payment_value == "2checkout") {
			
						var seller_id = twocheckout_Obj.sellerId;
						var publishable_Key = twocheckout_Obj.publishKey;
						/*  Called when token created successfully. */
						jQuery('.mp-loading-main').show();
						function successCallback(data) {
							
							/* Set the token as the value for the token input */
							
							var twmpoken = data.response.token.token;
							dataString['twmpoken'] = twmpoken;
							dataString['payment_method'] = "2checkout";
							dataString['action'] = 'add_mp_guest_user';
							  jQuery.ajax({
									type : "POST",
									url  : ajaxurl+"/assets/lib/front_ajax.php",
									data : dataString,
									success:function(response){
										if(jQuery.trim(response) == 'Booking success'){
											if(thankupage_url != '' && thankupage_url != null){
												window.location.href = thankupage_url;
											}else{
											  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
											}
										}
									}
								});	
						};

						/*  Called when token creation fails. */
						function errorCallback(data) {
							
							if (data.errorCode === 200) {
								clicked=false; 
								/* jQuery('.ct-loading-main-complete_booking').hide(); */
								tokenRequest();
							} else {
								clicked=false; 
								/*  jQuery('.ct-loading-main-complete_booking').hide();
								 jQuery('#ct-card-payment-error').show();
								 jQuery('#ct-card-payment-error').text(response.error.message); */
							}
						};

						function tokenRequest() {
							
							/* Setup token request arguments */
							var args = {
								sellerId: seller_id,
								publishableKey: publishable_Key,
								ccNo: jQuery('#card-number').val(),
								cvv: jQuery('#cvc-code').val(),
								expMonth: jQuery('#card-expiry').val(),
								expYear: jQuery('#card_exp_year').val()
							};
							/* Make the token request */
							TCO.loadPubKey('sandbox',function(){
								TCO.requestToken(successCallback, errorCallback, args);
							});
						};
						
						tokenRequest();
					} else if(payment_value == "Paypal"){
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/mp_pp_payment_process.php",
					data : dataString,
					success:function(response){
						jQuery('.mp-loading-main').hide();
						jQuery('#redirect_pp_url').html(response);
					}
				});
			}else if(payment_value == "stripe"){
				 var stripe_pubkey = stripeObj.p_key;
				 Stripe.setPublishableKey(stripe_pubkey);
				 
				 var cc_card_num = jQuery('#card-number').val();
				 var cc_exp_month = jQuery('#card-expiry').val();
				 var cc_exp_year = jQuery('#card_exp_year').val();
				 var cc_card_code = jQuery('#cvc-code').val();

				 dataString = {
				  cc_card_num:cc_card_num,
				  cc_exp_month:cc_exp_month,
				  cc_exp_year:cc_exp_year,
				  cc_card_code:cc_card_code
				 };

				 var stripeResponseHandler = function(status, response)
				 {
				  if (response.error) 
				  {
				   if(cc_card_num == ""){
					jQuery('#card-number').css('border','1px solid #ff0000');
				   }  
				   if(cc_exp_month == ""){
					jQuery('#card-expiry').css('border','1px solid #ff0000');
				   }  
				   if(cc_exp_year == ""){
					jQuery('#card_exp_year').css('border','1px solid #ff0000');
				   }  
				   if(cc_card_code == ""){
					jQuery('#cvc-code').css('border','1px solid #ff0000');
				   }
				  }
				  else 
				  {
				   var token = response.id;
				   function waitForElement()
				   {
					if(typeof token !== "undefined" && token != '')
					{
					 var st_token = token;
					 dataString['st_token'] = st_token;
					 dataString['preff_username'] = preff_username;
					 dataString['preff_password'] = preff_password;
					 dataString['first_name']     = first_name;
					 dataString['last_name']      = last_name;
					 dataString['user_email']     = user_email;
					 dataString['notes']          = notes;
				     dataString['dynamic_field_add'] = dynamic_field_add;
		             dataString['selected_date_time_from_cal'] = selected_date_time_from_cal;
					 dataString['phone'] = phone;
					 dataString['payment_method'] = "stripe";
					 dataString['action'] = "add_mp_guest_user";
					 dataString['user_status'] = user_status;
					 jQuery.ajax({
					  type:"POST",
					  url:ajaxurl+"/assets/lib/front_ajax.php",
					  data:dataString,
					  success:function(response){
						jQuery('.mp-loading-main').hide();					
						if(jQuery.trim(response) == 'Booking success'){
							   
							  if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
							 /*  window.location.href = booking_page_url; */
							  window.location.href = ajaxurl+'/frontend/mp_thankyou.php'; 
							}
						}
					  }
					 });
					} 
					else
					{
					 setTimeout(function(){ waitForElement(); },2000);
					}
				   }
				  waitForElement();
				  }
				 };
					Stripe.card.createToken({
						number: jQuery('#card-number').val(),
						cvc: jQuery('#cvc-code').val(),
						exp_month: jQuery('#card-expiry').val(),
						exp_year: jQuery('#card_exp_year').val()
					}, stripeResponseHandler);
			} else {
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/front_ajax.php",
					data : dataString,
					success:function(response){
						jQuery('.mp-loading-main').hide();
						if(jQuery.trim(response) == 'Booking success'){
							 
							 if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
								window.location.href = ajaxurl+'/frontend/mp_thankyou.php'; 
							  /* window.location.href = booking_page_url; */
							}
						}
					}
				});
			}
		}else{
			jQuery('.mp-loading-main').hide();
		}
		
	}else{
		jQuery('#frmbooking').validate({
			rules:{
				
				'mp_first_name':{required:true},
				'mp_last_name':{required:true},
				'mp_email':{
							required:true,
							
							},
			},
			messages:{
				'mp_first_name':{required: val_error_msg.firstname_err_msg},
				'mp_last_name':{required: "Please Enter Lastname"},
				'mp_email':{required: "Please Enter Email",/* remote:"Email Already Exists" */},
			}
		});
		
		jQuery('.get_custom_field').each(function(){
			var name_field = jQuery(this).attr('name');
			var required_field = jQuery(this).data('required');
			var fieldlabel = jQuery(this).data('fieldlabel')
			if(required_field == "Y"){
				jQuery(this).rules("add",{ required : true, messages : { required : "Please Enter "+fieldlabel+""}});
			}
		});
		/* Below Code for existing users */
		
		var dynamic_field_add = {};
		var first_name = jQuery("#first-name").val();
		var last_name = jQuery("#last-name").val();
		var user_email = jQuery("#mp-email").val();
		var notes = jQuery("#bdp_notes").val();
		var phone = jQuery("#mp-phone").val();
		var payment_value = jQuery(".payment_value:checked").val();
		
		jQuery('.get_custom_field').each(function(){
			if(jQuery(this).data('fieldname') == "radio_group"){
				dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery('.get_custom_field:checked').val();
			}else{
			dynamic_field_add[jQuery(this).data('fieldlabel')] = jQuery(this).val();
			}
		});
		dynamic_field_add['notes'] = jQuery("#bdp_notes").val();
		
		var dataString = {first_name:first_name,last_name:last_name,user_email:user_email,notes:notes,dynamic_field_add:dynamic_field_add,selected_date_time_from_cal:selected_date_time_from_cal,phone:phone,action:"add_mp_exist_user"}
		if(jQuery('#frmbooking').valid()){
		if(payment_value == "Pay At Venue"){
			jQuery.ajax({
				type : "POST",
				url  : ajaxurl+"/assets/lib/front_ajax.php",
				data : dataString,
				success:function(response){
					jQuery('.mp-loading-main').hide();					
					if(jQuery.trim(response) == 'Booking success'){
						if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
						  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
							}
					}
				}
			});
		} else if(payment_value == "authorize"){
			var cc_card_num = jQuery('#card-number').val();
			var cc_exp_month = jQuery('#card-expiry').val();
			var cc_exp_year = jQuery('#card_exp_year').val();
			var cc_card_code = jQuery('#cvc-code').val();
			var first_name = jQuery("#first-name").val();
			var user_email = jQuery("#mp-email").val();
			var phone = jQuery("#mp-phone").val();
				 var dataString = {
				  cc_card_num:cc_card_num,
				  cc_exp_month:cc_exp_month,
				  cc_exp_year:cc_exp_year,
				  cc_card_code:cc_card_code,
				  user_email:user_email,
				  first_name:first_name,
				  phone:phone,
				 };
						jQuery('.mp-loading-main').show();
						jQuery.ajax({
							type:"POST",
							url  : ajaxurl+"/assets/lib/authorizenet_payment_process.php",
							data:dataString,
							success:function(response){
								var response_detail = jQuery.parseJSON(response);
								console.log(response_detail);
								if(response_detail.success==false){
									clicked=false; 
									
									} else {
										var transaction_id = response_detail.transaction_id;
										dataString['transaction_id'] = transaction_id;
										dataString['payment_method'] = 'authorize_payments'; 
										dataString['action'] = 'add_mp_exist_user'; 
										jQuery.ajax({
											type : "POST",
											url  : ajaxurl+"/assets/lib/front_ajax.php",
											data : dataString,
											success:function(response){
												if(jQuery.trim(response) == 'Booking success'){
													if(thankupage_url != '' && thankupage_url != null){
														window.location.href = thankupage_url;
													}else{
													  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
													}
												}
											}
										});					
								}
							}
						})

					} else if(payment_value == "2checkout") {
			
						var seller_id = twocheckout_Obj.sellerId;
						var publishable_Key = twocheckout_Obj.publishKey;
						/*  Called when token created successfully. */
						jQuery('.mp-loading-main').show();
						function successCallback(data) {
							
							/* Set the token as the value for the token input */
							
							var twmpoken = data.response.token.token;
							dataString['twmpoken'] = twmpoken;
							dataString['payment_method'] = "2checkout";
							
							  jQuery.ajax({
									type : "POST",
									url  : ajaxurl+"/assets/lib/front_ajax.php",
									data : dataString,
									success:function(response){
										if(jQuery.trim(response) == 'Booking success'){
											if(thankupage_url != '' && thankupage_url != null){
												window.location.href = thankupage_url;
											}else{
											  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
											}
										}
									}
								});	
						};

						/*  Called when token creation fails. */
						function errorCallback(data) {
							
							if (data.errorCode === 200) {
								clicked=false; 
								tokenRequest();
							} else {
								clicked=false; 
							}
						};

						function tokenRequest() {
							
							/* Setup token request arguments */
							var args = {
								sellerId: seller_id,
								publishableKey: publishable_Key,
								ccNo: jQuery('#card-number').val(),
								cvv: jQuery('#cvc-code').val(),
								expMonth: jQuery('#card-expiry').val(),
								expYear: jQuery('#card_exp_year').val()
							};
							/* Make the token request */
							TCO.loadPubKey('sandbox',function(){
								TCO.requestToken(successCallback, errorCallback, args);
							});
						};
						
						tokenRequest();
					} else if(payment_value == "Paypal"){
				jQuery.ajax({
					type : "POST",
					url  : ajaxurl+"/assets/lib/mp_pp_payment_process.php",
					data : dataString,
					success:function(response){
						jQuery('#redirect_pp_url').html(response);
						jQuery('.mp-loading-main').hide();
						/*if(jQuery.trim(response) == 'Booking success'){
							  window.location.href = siteurl+'/vk-thankyou/';
						}*/
					}
				});
		}else if(payment_value == "stripe"){
				 var stripe_pubkey = stripeObj.p_key;
				 Stripe.setPublishableKey(stripe_pubkey);
				 
				 var cc_card_num = jQuery('#card-number').val();
				 var cc_exp_month = jQuery('#card-expiry').val();
				 var cc_exp_year = jQuery('#card_exp_year').val();
				 var cc_card_code = jQuery('#cvc-code').val();

				 dataString = {
				  cc_card_num:cc_card_num,
				  cc_exp_month:cc_exp_month,
				  cc_exp_year:cc_exp_year,
				  cc_card_code:cc_card_code
				 };

				 var stripeResponseHandler = function(status, response)
				 {
				  if (response.error) 
				  {
				   if(cc_card_num == ""){
					jQuery('#card-number').css('border','1px solid #ff0000');
				   }  
				   if(cc_exp_month == ""){
					jQuery('#card-expiry').css('border','1px solid #ff0000');
				   }  
				   if(cc_exp_year == ""){
					jQuery('#card_exp_year').css('border','1px solid #ff0000');
				   }  
				   if(cc_card_code == ""){
					jQuery('#cvc-code').css('border','1px solid #ff0000');
				   }
				  }
				  else 
				  {
				   var token = response.id;
				   function waitForElement()
				   {
					if(typeof token !== "undefined" && token != '')
					{
					 var st_token = token;
					 dataString['st_token'] = st_token;
					 dataString['preff_username'] = preff_username;
					 dataString['preff_password'] = preff_password;
					 dataString['first_name']     = first_name;
					 dataString['last_name']      = last_name;
					 dataString['user_email']     = user_email;
					 dataString['notes']          = notes;
				     dataString['dynamic_field_add'] = dynamic_field_add;
		             dataString['selected_date_time_from_cal'] = selected_date_time_from_cal;
					 dataString['phone'] = phone;
					 dataString['payment_method'] = "stripe";
					 dataString['action'] = "add_mp_exist_user";
					 jQuery.ajax({
					  type:"POST",
					  url:ajaxurl+"/assets/lib/front_ajax.php",
					  data:dataString,
					  success:function(response){
						jQuery('.mp-loading-main').hide();					
						if(jQuery.trim(response) == 'Booking success'){
							if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
							}else{
							  window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
							}
						}
					  }
					 });
					} 
					else
					{
					 setTimeout(function(){ waitForElement(); },2000);
					}
				   }
				  waitForElement();
				  }
				 };
					Stripe.card.createToken({
						number: jQuery('#card-number').val(),
						cvc: jQuery('#cvc-code').val(),
						exp_month: jQuery('#card-expiry').val(),
						exp_year: jQuery('#card_exp_year').val()
					}, stripeResponseHandler);
			} else {
			jQuery.ajax({
				type : "POST",
				url  : ajaxurl+"/assets/lib/front_ajax.php",
				data : dataString,
				success:function(response){
					jQuery('.mp-loading-main').hide();
					if(jQuery.trim(response) == 'Booking success'){
						if(thankupage_url != '' && thankupage_url != null){
								window.location.href = thankupage_url;
						}else{
						window.location.href = ajaxurl+'/frontend/mp_thankyou.php';
						}
					}
				}
			});
		}
		}else{
			jQuery('.mp-loading-main').hide();
		}
		
	}
		jQuery('.bdp-loader').css("display","none");
		jQuery('.add_show_error_class').each(function(){
			jQuery(this).trigger('keyup');
		});
});




/* click next button show next data */
jQuery(document).on('click','#bdp_next_btn', function() {
	jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	var labels_head = nav_labels;
	var action = jQuery('#bdp_next').val();
	var getdata =  {action:action}
	
	/* Get Service View */
	if(action=="get_services_view"){
		var arr=[];
		jQuery('.bdp_locations').each(function(){
			var loc_id=jQuery(this).data('lcid');
			var test=jQuery(this).hasClass("selected");
			if(test==true){
		
				arr.push(loc_id);
			}
		});		
	
		if(arr.length>0){
			var gsv=1;
		}else{
			jQuery('.mp-loading-main').hide();
			return false;
		}
	}
	/*Provider View */
	if(action=="get_providers_view"){
		var arr1=[];
		jQuery('.bdp_services').each(function(){
			var sid=jQuery(this).data('sid');
			var test=jQuery(this).hasClass("selected");
			if(test==true){
				
				arr1.push(sid);
			}
		});		
		
		if(arr1.length>0){
			var gpv=1;
		}else{
			jQuery('.mp-loading-main').hide();
			return false;
		}
	}
	/* Month SLot View */
	if(action=="get_month_slot_view"){
			var arr1=[];
		jQuery('.bdp_providers').each(function(){
			var pid=jQuery(this).data('proid');
			var test=jQuery(this).hasClass("selected");
			if(test==true){
				
				arr1.push(pid);
			}
		});		
		
		if(arr1.length>0){
			var gmsv=1;
		}else{
			jQuery('.mp-loading-main').hide();
			return false;
		}
			
	}
	/* User Detail View  */
	if(action=="get_personal_details_view"){
			var gpdv=1;
	}	
	/* Get Complete View */
	if(action=="get_complete_view"){
			var gcv=1;
	}	
		jQuery('.bdp-loader').css("display","block");
if(gsv==1 || gpv==1 || gmsv == 1 || gpdv==1 || gcv==1){		
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('.mp-loading-main').hide();
			jQuery('#bdp_wrapper_inner').html(response);
			

			if(action == 'get_services_view'){
				/* top progress bar */
				jQuery('.bdp-locations').removeClass('is-active');
				jQuery('.bdp-locations').addClass('is-complete');
				jQuery('.bdp-services').addClass('is-active');
				jQuery(".content-header").html("2. "+labels_head.service_msg);	
				jQuery("#bdp_previous_btn").css("display","inline-block");	
				/* jQuery("#bdp_next_btn").css("display","block"); */
			}
			if(action == 'get_providers_view'){
				/* top progress bar */
				jQuery('.bdp-services').removeClass('is-active');
				jQuery('.bdp-services').addClass('is-complete');
				jQuery('.bdp-provider').addClass('is-active');
				jQuery(".content-header").html("3. "+labels_head.provider_msg);
				/* jQuery("#bdp_next_btn").css("display","block");	 */			
			}
			if(action == 'get_month_slot_view'){
				/* top progress bar */
				jQuery('.bdp-provider').removeClass('is-active');
				jQuery('.bdp-provider').addClass('is-complete');
				jQuery('.bdp-date-time').addClass('is-active');	
				jQuery(".content-header").html("4. "+labels_head.select_date_time_msg);	
				/* jQuery("#bdp_next_btn").css("display","block"); */				
			}
			if(action == 'get_personal_details_view'){
				/* top progress bar */
				jQuery('.bdp-date-time').removeClass('is-active');
				jQuery('.bdp-date-time').addClass('is-complete');
				jQuery('.bdp-details').addClass('is-active');	
				jQuery(".content-header").html("5. "+labels_head.personal_details_msg);	
				jQuery("#bdp_next_btn").css("display","none");
				var cps = jQuery.trim(response);
				var cps_details = cps.split('###########');
				jQuery('#bdp_wrapper_inner').html(cps_details[0]);
				jQuery('.cart_section').html(cps_details[1]);
			}
			
			if(action == 'get_complete_view'){
				/* top progress bar */
				jQuery('.bdp-details').removeClass('is-active');
				jQuery('.bdp-details').addClass('is-complete');
				jQuery('.bdp-complete').addClass('is-active');
				jQuery(".content-header").html("6. Booking Completed");	
			}
			
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" } ,500);	
		}
		
	});	
		}else{
			jQuery('.mp-loading-main').hide();
		};
    	jQuery('.bdp-loader').css("display","none");
 });
 
 
 /* click next button show next data */
jQuery(document).on('click','.mp-checkout-btn', function() {
	jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	var labels_head = nav_labels;
	var slot_db_date_time = "";
	var action = "get_personal_details_view";
	var getdata =  {slot_db_date_time:slot_db_date_time,action:action}
	
	/* Get Service View */
		
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('.mp-loading-main').hide();
			var cps = jQuery.trim(response);
			var cps_details = cps.split('###########');
			jQuery('#bdp_wrapper_inner').html(cps_details[0]);
			jQuery('.cart_section').html(cps_details[1]);
			/* jQuery('#bdp_wrapper_inner').html(response); */
			jQuery("#bdp_previous_btn").css("display","inline-block");
			jQuery('.bdp-locations').removeClass('is-active');
			jQuery('.bdp-locations').addClass('is-complete');
			jQuery('.bdp-services').addClass('is-complete');
			jQuery('.bdp-provider').addClass('is-complete');
			jQuery('.bdp-date-time').addClass('is-complete');
			jQuery('.bdp-details').addClass('is-active');	
			jQuery(".content-header").html("5. "+labels_head.personal_details_msg);	
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" } ,500);	
		}
		
	});	
    	jQuery('.bdp-loader').css("display","none");
 });
 
 
 /* click back button show previous data */
 jQuery(document).on('click','#bdp_previous_btn', function() {
	 jQuery('.mp-loading-main').show();
	ajaxurl = obj_locations.ajaxurl;
	var labels_head = nav_labels;
	var action = jQuery('#bdp_previous').val();
	var getdata =  {action:action}
	jQuery('.bdp-loader').css("display","block");

	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('#bdp_wrapper_inner').html(response);
			jQuery('.mp-loading-main').hide();
			if(action == 'get_locations_view'){
				/* top progress bar */
				jQuery('.bdp-locations').removeClass('is-complete');
				jQuery('.bdp-locations').addClass('is-active');
				jQuery('.bdp-services').removeClass('is-active');
				jQuery(".content-header").html("1. "+labels_head.location_msg);	
				jQuery("#bdp_previous_btn").css("display","none");
				
			}
			if(action == 'get_services_view'){
				/* top progress bar */
				jQuery('.bdp-services').removeClass('is-complete');
				jQuery('.bdp-services').addClass('is-active');
				jQuery('.bdp-provider').removeClass('is-active');
				jQuery(".content-header").html("2. "+labels_head.service_msg);
						
			}
			if(action == 'get_providers_view'){
				/* top progress bar */
				jQuery('.bdp-provider').removeClass('is-complete');
				jQuery('.bdp-provider').addClass('is-active');
				jQuery('.bdp-date-time').removeClass('is-active');
				jQuery(".content-header").html("3. "+labels_head.provider_msg);	
			}
			if(action == 'get_month_slot_view'){
				/* top progress bar */
				jQuery('.bdp-date-time').removeClass('is-complete');
				jQuery('.bdp-date-time').addClass('is-active');
				jQuery('.bdp-details').removeClass('is-active');
				jQuery(".content-header").html("4. "+labels_head.select_date_time_msg);
				jQuery("#bdp_next_btn").css("display","inline-block");				
			}
			if(action == 'get_personal_details_view'){
				/* top progress bar */
				jQuery('.bdp-details').removeClass('is-complete');
				jQuery('.bdp-details').addClass('is-active');
				jQuery(".content-header").html("5. "+labels_head.personal_details_msg);
			}
			
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "left" }, 500);
		}
	});		jQuery('.bdp-loader').css("display","none");
 });
 
 
 
 /* html calendar */
 
  /* html calendar */
 jQuery(document).on('click','.month_change', function() {
	 ajaxurl = obj_locations.ajaxurl;
	 var next_year = jQuery(this).data('next_month_year');
	 var next_month = jQuery(this).data('next_month');
	 

	var getdata =  {year:next_year,month:next_month,action:'get_calendar'}	
	 
	 jQuery.ajax({
			url  : ajaxurl+"/assets/lib/front_ajax.php",
			type : 'POST',
			data : getdata, 
			dataType : 'html',
			success  : function(response) {
			  jQuery('.calendar-wrapper').html(response);
			}
		});
	 
 });
 
 
jQuery(document).on('click','.hide_previous_dates', function() {
	 jQuery('.time_slot_box').hide();
 });

 jQuery(document).on('click','.selected_date', function() {
	ajaxurl = obj_locations.ajaxurl;
	jQuery('.bdp-week').each(function(){
		jQuery(this).removeClass('active');		
		jQuery('.bdp-show-time').hide();
		jQuery('.bdp-show-time').removeClass('shown');
	});	
	
	jQuery('.content_width').removeClass('col-lg-12');
	jQuery('.content_width').removeClass('col-md-12');
	jQuery('.content_width').removeClass('col-sm-12');
	
	jQuery('.content_width').addClass('col-lg-8');
	jQuery('.content_width').addClass('col-md-7');
	jQuery('.content_width').addClass('col-sm-6');		
	 
	jQuery(this).addClass('active');
	jQuery('.bdp-show-time').show( "blind", {direction: "vertical"}, 500 );
	jQuery('.bdp-show-time').addClass('shown');
	
	var id = jQuery(this).data('id');
	var selected_dates = jQuery(this).data('selected_dates');
	var s_date = jQuery(this).data('s_date');
    var cur_dates = jQuery(this).data('cur_dates');
    var c_date = jQuery(this).data('c_date');
	
	var getdata =  {id:id,selected_dates:selected_dates,action:'get_slots'};
	
	jQuery('.bdp-show-time').html('');
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('.time_slot_box').hide();
            jQuery('.display_selected_date_slots_box'+id).html(response);
            jQuery('.display_selected_date_slots_box'+id).show();
		}
	});
});


/* For user login in frontend */
jQuery(document).on('click','.user_front_login',function(){
	var username = jQuery('#username').val();
	var pass = jQuery('#password').val();
	var dataString = {username:username,pass:pass,action:"user_front_login"}
	jQuery('#frmlogin').validate({
			rules:{
				'existing_user':{required:true},
				'user_password':{required:true},
			},
			messages:{
				'existing_user':{required : "Please Enter Email"},
				'user_password':{required : "Please Enter Password"},
				
			}
		});
	if(jQuery('#frmlogin').valid()){
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			if(jQuery.trim(response) != "Invalid Username or Password"){
			var getdata = jQuery.parseJSON(response);
			jQuery("#first-name").val(getdata.first_name);
			jQuery("#last-name").val(getdata.last_name);
			jQuery("#mp-email").val(getdata.user_email);
			jQuery("#bdp_notes").val(getdata.notes);
			jQuery(".user_front_logout").attr("data-user_id",getdata.user_id);
			jQuery(".bdp-user-login-radio").hide();
			jQuery(".existing-user-login").hide();
			jQuery(".user_header").hide();
			jQuery(".existing-user-loggedin").show();
			jQuery("#mp-user-login-status").val('Y');
			jQuery(".loggedin_name").html(getdata.first_name);
			jQuery("#invalid_un_pwd").css("display","none");
			}else{
				jQuery("#invalid_un_pwd").css("display","block");
			}
		}
	});
	}
});

jQuery(document).on('click','.user_front_logout',function(){
	var user_id = jQuery(this).data('user_id');
	var dataString = {user_id:user_id,action:"user_front_logout"}
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			jQuery(".bdp-user-login-radio").show();
			jQuery(".existing-user-login").show();
			jQuery(".user_header").show();
			jQuery(".existing-user-loggedin").hide();
			jQuery("#first-name").val("");
			jQuery("#last-name").val("");
			jQuery("#mp-email").val("");
		}
	});
});

/* jQuery(document).on('click','#cart_view',function(){
	var action = "get_cart_view";
	var dataString = {action:"get_cart_view"}
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			jQuery('#bdp_wrapper_inner').html(response);
			if(action == 'get_cart_view'){
				
				jQuery('.bdp-locations').addClass('is-complete');
				jQuery('.bdp-locations').removeClass('is-active');
				jQuery('.bdp-services').addClass('is-complete');
				jQuery('.bdp-provider').addClass('is-complete');
				jQuery('.bdp-date-time').addClass('is-complete');
				jQuery('.bdp-cart').addClass('is-active');	
				jQuery('#add_more_services').show();
				jQuery(".content-header").html("5. Your Cart Details");	
				// jQuery("#bdp_next_btn").css("display","none");
			}
		}
	});
}); */

jQuery(document).on('click','#add_more_services',function(){
	var action = "get_services_view";
	var dataString = {action:"get_services_view"}
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			jQuery('#bdp_wrapper_inner').html(response);
			if(action == 'get_services_view'){
				/* top progress bar */
				jQuery('#add_more_services').hide();
				jQuery('#add_more_services').css('display','none');
				jQuery('.bdp-locations').removeClass('is-active');
				jQuery('.bdp-locations').addClass('is-complete');
				jQuery('.bdp-services').addClass('is-active');
				jQuery('.bdp-provider').removeClass('is-complete');
				jQuery('.bdp-date-time').removeClass('is-complete');
				jQuery('.bdp-services').removeClass('is-complete');
				jQuery('.bdp-cart').removeClass('is-active');	
				jQuery(".content-header").html("2. Select Your Service");	
				jQuery("#bdp_previous_btn").css("display","inline-block");	
				
			}
		}
	});
});

jQuery(document).on('click','.delete_cart_item',function(){
	jQuery('.mp-loading-main').show();
	var cart_id = jQuery(this).data('id');
	var currency = ob_currency.mp_currency;
	// var net_total = jQuery('.net_total').data('grandtotal');
	var net_total = jQuery('.sub_total').data('amount');
	dataString = { cart_id:cart_id,net_total:net_total,action:"delete_cart_item" }
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			jQuery('.mp-loading-main').hide();
			jQuery('.cart_li'+cart_id).fadeOut();
			var get_det = jQuery.trim(response);
			var res = get_det.split('###');
			jQuery('#total_item').html(res[0]);
			jQuery('.sub_total').html(res[1]);
			if(res[1] != 0){
				jQuery('.net_total').html(res[4]);
			}else{
				jQuery('.mp_tax_amount').html("0");
				jQuery('.net_total').html("0");
			}
			jQuery('.mp_tax_amount').html(res[3]);
			jQuery('.cp_discount').html(res[2]);
			jQuery('.partial_amount_ontotal').html(res[5]);
			jQuery('.partial_amount_remaining').html(res[6]);
			
		}
	});
});

jQuery(document).bind('ready ajaxComplete',function(){
	jQuery('#coupon_apply_form').validate({
	rules: {
	'coupon_apply_val':{required:true}
	},
	messages: {
	'coupon_apply_val':{required : "Please enter Coupon"},
	},
	}); 
});
jQuery(document).on('click','#apply_coupon',function(){
	var coupon_value = jQuery('#mp-coupon').val();
	var currency = ob_currency.mp_currency;
	/* var net_total = jQuery('.net_total').data('grandtotal'); */
	var net_total = jQuery('.sub_total').data('amount');
	dataString = { coupon_value:coupon_value,net_total:net_total,action:"check_couponcode" }
	/* if(coupon_value != "" && coupon_value != null){ */
	
	if(jQuery('#coupon_apply_form').valid()){
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			if(jQuery.trim(response) != 'NO'){
			var cp = jQuery.trim(response);
			var cp_details = cp.split('###');
			jQuery('.net_total').html(cp_details[0]);
			jQuery('.coupon_display').css('display','block');
			jQuery('.cp_discount').html(cp_details[2]);
			jQuery('.partial_amount_ontotal').html(cp_details[4]);
			jQuery('.partial_amount_remaining').html(cp_details[5]);
			//jQuery('.cp_discount').html('$'+cp_details[1]);
			/* jQuery('.coupon_message').css('display','none');
			jQuery('.coupon_message_invalid').css('display','none'); */
			
			}else{
				/* jQuery('#mp_coupon_error').css('display','none'); */
				jQuery('#mp_coupon_error').css('display','block');
			}
		}
	});
	}
	/* } */
});
function month_change(next_year,next_month,today_date) {
	var ajaxurl = obj_locations.ajaxurl;
	var getdata =  {year:next_year,month:next_month,action:'get_calendar'}
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		type : 'POST',
		data : getdata,
		dataType : 'html',
		success  : function(response) {
		  jQuery('.calendar-wrapper').html(response);
		  jQuery('.selected_datess'+today_date).trigger('click');
		}
	});
}
jQuery(document).on("click",".today_btttn",function() {
	var today_date = jQuery(this).data('cur_dates');
	var next_year = jQuery(this).data('next_month_year');
	var next_month = jQuery(this).data('next_month');
	var check_date = '-'+next_month+'-'+next_year;
	var dates = jQuery('.selected_date').data('selected_dates');
	if (dates.indexOf(check_date) >= 0){
		jQuery('.selected_datess'+today_date).trigger('click');
	}else{
		month_change(next_year,next_month,today_date);
	}
});

/* More Service */
jQuery(document).on('click','#more_services', function() {
	/* Get Service View */
	var action = "get_services_view";
	var dataString = {action:"get_services_view"}
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_ajax.php",
		data:dataString,
		success:function(response){
			jQuery('#bdp_wrapper_inner').html(response);
			if(action == 'get_services_view'){
				/* top progress bar */
				jQuery('#add_more_services').hide();
				jQuery('#add_more_services').css('display','none');
				jQuery('.bdp-locations').removeClass('is-active');
				jQuery('.bdp-locations').addClass('is-complete');
				jQuery('.bdp-services').addClass('is-active');
				jQuery('.bdp-provider').removeClass('is-complete');
				jQuery('.bdp-date-time').removeClass('is-complete');
				jQuery('.bdp-services').removeClass('is-complete');
				jQuery('.bdp-cart').removeClass('is-active');	
				jQuery(".content-header").html("2. Select Your Service");	
				jQuery("#bdp_previous_btn").css("display","inline-block");	
				jQuery(".bdp-details").removeClass('is-active');
				jQuery(".bdp-provider").removeClass('is-active');
				jQuery(".bdp-date-time").removeClass('is-active');
				
			}
		}
	});
	
});
