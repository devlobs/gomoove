<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	$additional_info = new moveto_additional_info();
	
	$alladditional_info = $additional_info->readAll();
?>
<div id="mp-services-panel" class="panel tab-content table-fixed">
	<div class="panel-body table-cell col-md-9 col-sm-9 col-xs-12 col-lg-9">
		<div class="mp-service-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<!-- right side common menu for service -->
			<div class="mp-service-top-header">
				<span class="mp-service-service-name pull-left" id="mp-category-title"></span>
				
				<div class="pull-right">
					<table>
						<tbody>
							<tr>
								<td>
									<button id="mp-add-new-service" class="btn btn-success" value="add new service"><i class="fa fa-plus icon-space "></i><?php echo __("Add New Info","mp");?></button>
								</td>													
							</tr>
						</tbody>
					</table>
					
			</div>
			</div>
			<div id="hr"></div>
			<div class="tab-pane active" id=""><!-- services list -->
				<div class="tab-content mp-services-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-services" > <!-- sortable-services -->
							
							<?php if(sizeof((array)$alladditional_info)>0) {
									foreach($alladditional_info as $additional_info){ ?>
							
							<li id="service_detail_<?php echo $additional_info->id;?>" class="panel panel-default mp-services-panel" >
							
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="col-lg-6 col-sm-7 col-xs-12 np">
											<div class="pull-left">
												<i class="fa fa-th-list"></i>
											</div>	
											<span class="mp-service-title-name f-letter-capitalize"><?php echo $additional_info->additional_info;?></span>
											
										</div>
										<div class="col-lg-6 col-sm-5 col-xs-12 np">
											<div class="col-lg-2 col-sm-2 col-xs-4 np">
												<label for="sevice-endis-<?php echo $additional_info->id;?>">
									<input <?php if($additional_info->status=='E'){ echo "checked='checked'"; } ?> data-id="<?php echo $additional_info->id;?>" type="checkbox" class="update_addinfo_status" id="sevice-endis-<?php echo $additional_info->id;?>" data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" >
												</label>
											</div>
											<div class="pull-right">
												<div class="col-lg-2 col-sm-2 col-xs-4 np">
												<a data-poid="mp-popover-delete-service<?php echo $additional_info->id;?>" id="mp-delete-service<?php echo $additional_info->id;?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this Additional info?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete Addon","mp");?>"></i></a>
													<div class="mp-popover" id="mp-popover-delete-service<?php echo $additional_info->id;?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>										
																		<input  data-id="<?php echo $additional_info->id;?>" value="<?php echo __("Yes","mp");?>" class="btn btn-danger btn-sm mr-10 delete_additional_info" type="button" />
																		<button data-poid="mp-popover-addon<?php echo $additional_info->id;?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $additional_info->id;?>" ><label class="mp-show-hide-label" for="<?php echo $additional_info->id;?>"></label>
												</div>
											</div>
										</div>
									</h4>
								</div>
								<div id="" class="service_detail panel-collapse collapse detail-id_<?php echo $additional_info->id;?>">
									<div class="panel-body">
										<form id="mp_update_service<?php echo $additional_info->id;?>" method="post" class="slide-toggle mp_update_service" >
											<div class="mp-service-collapse-div col-sm-7 col-md-7 col-lg-7 col-xs-12">										
												<table class="mp-create-service-table">
													<tbody>
														<tr>
															<td><label for="additional_info_utitle<?php echo $additional_info->id;?>"><?php echo __("Info Title","mp");?></label></td>
															<td><input type="text" value="<?php echo $additional_info->additional_info;?>" name="additional_info_utitle" class="form-control" id="additional_info_utitle<?php echo $additional_info->id;?>" />
															<label style="display:none;" id="additional_info_utitle_err<?php echo $additional_info->id;?>" class="error" for="" ><?php echo __("Please Enter Additional Info Title.","mp");?></label>
															</td>
														</tr>

														<tr>
															<td>
																<label><?php echo __("Calculation Type","mp");?></label>
															</td>
															<td>
															  <label>
																<input class="m-0" name="group1" id='tax-vatt-percentage_update<?php echo $additional_info->id;?>'  <?php if($additional_info->type=='P'){ echo 'checked'; }?> type="radio" value="percentage" checked />
																<span>Percentage</span>
															  </label>
														   
															  <label>
																<input class="m-0" name="group1"  <?php if($additional_info->type=='F'){ echo 'checked'; }?> type="radio" value="flat" />
																<span>Flat</span>
															  </label>
															</td>
														</tr>
														<tr>
														
															<td>
																<label for="mp-service-maxqty"><?php echo __("Price per unit","mp");?></label>
															</td>

															<td>
																<input type="text" name="service_maxqty" class="form-control maxqty<?php echo $additional_info->id; ?>"   id="mp-additional-price<?php echo $additional_info->id;?>"   value="<?php echo $additional_info->price;?>"" />
																<label style="display:none;" id="additional_info_price_err<?php echo $additional_info->id;?>" class="error" for="" ><?php echo __('Please Enter Price In Digits Only','mp');?></label>
															</td>
														</tr>

													</tbody>
												</table>
										</div>
										<table class="col-sm-7 col-md-7 col-lg-7 col-xs-12 mt-20 mb-20">
											<tbody>
												<tr>
													<td></td>
													<td>
														<input type="button" data-addid="<?php echo $additional_info->id;?>" id="mp_update_additional_info<?php echo $additional_info->id;?>" name="mp_update_additional_info" class="btn btn-success mp-btn-width col-md-offset-4 mp_update_additional_info" value="<?php echo __("Update","mp");?>" />
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
											</tbody>
										</table>
										</form>									
									</div>
								</div>
							</li>
							<?php }
							}else{ ?>
								<h4><?php echo __("No Additional Information Found.","mp");?></h4>
							<?php } ?>
							<!-- add new service pop up -->
							<li>
							<div class="panel panel-default mp-services-panel mp-add-new-service">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="mp-col6">
											<span class="mp-service-title-name"><?php echo __("Add New Info","mp");?></span>		
										</div>										
										<div class="pull-right col-lg-6 col-sm-5 col-xs-12 np">
											<div class="pull-right">											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addservice" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="addservice"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="" class="service_detail panel-collapse collapse in detail-id_addservice">
									<div class="panel-body">
										<form id="mp_create_additional_info" method="post" class="slide-toggle" >
											<div class="mp-service-collapse-div col-sm-7 col-md-7 col-lg-7 col-xs-12">
												
													<table class="mp-create-service-table">
														<tbody>
															
															<tr>
																<td><label for="mp-service-title"><?php echo __("Additional Info Title","mp");?></label></td>
																<td><input type="text" name="additional_info_title" class="form-control" id="additional_info_title" /></td>
															</tr>
															<tr>
															<td>
																<label><?php echo __("Calculation Type","mp");?></label>
															</td>
															<td>
															  <label>
																<input class="m-0" name="group1" id='tax-vatt-percentage' type="radio" value="percentage" checked />
																<span>Percentage</span>
															  </label>
														   
															  <label>
																<input class="m-0" name="group1" type="radio" value="flat" />
																<span>Flat</span>
															  </label>
															</td>
														</tr>
														<tr>
														
															<td>
																<label for="mp-service-maxqty"><?php echo __("Price per unit","mp");?></label>
															</td>

															<td>
																<input type="text" name="additional_price" class="form-control" id="mp-additional-price" value="" />
															</td>
														</tr>
														</tbody>
													</table>
												
											</div>
											<table class="col-sm-7 col-md-7 col-lg-7 col-xs-12 mt-20 mb-20">
												<tbody>
													<tr>
														<td></td>
														<td>
															<input type="button" id="create_additional_info" name="create_additional_info" class="btn btn-success mp-btn-width col-md-offset-4" value="<?php echo __("Save","mp");?>"/>
															<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
														</td>
													</tr>
												</tbody>
											</table>									
										</form>									
									</div>
								</div>
							</div>
							</li>
							
							</ul>
						</div>	
					</div>
				</div>
			</div>
			
		</div>
			
	</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var additional_infoObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>