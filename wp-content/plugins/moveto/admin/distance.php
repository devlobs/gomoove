<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	$read_distance = new Moveto_distance();
	$read_floor = new Moveto_distance();
	$read_elevator = new Moveto_distance();
	$read_packing_unpacking = new Moveto_distance();
	$read_cubicfeets = new moveto_cubicfeets();

?>	
<div id="mp-distance-panel" class="panel tab-content table-fixed">	
	<div id="mp-distance-addon-panel" class="panel tab-content table-fixed">
		<div class="panel-body">
			<div class="mp-details-panel tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12 pdr-0">
				<div id="hr"></div>
					<div id="" class="service_detail panel-collapse collapse in detail-id_addservice">
						<div class="panel-body">
							<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
									<form id="mp_insert_distance" method="post" action="" class="slide-toggle" >
											<label class="distance-title">Distance Unit</label>
												<table class="mp-create-distance-table">
													<tbody>
													<tr>
															<!-- <td>
																<label for=""><?php echo __("Distance Unit","mp");?></label>
															</td> -->
															<td>
																<label>
																	<input class="m-0 distance_unit" name="unit" id="unit_km" type="radio" value="km" <?php  if(get_option('moveto_distance_unit') == 'km'){echo "checked";} ?>/>
																		<span>Km</span>
																			</label><span>&nbsp&nbsp</span>
																		<label>
																		<input class="m-0 distance_unit" name="unit" id="unit" type="radio" value="miles" <?php  if(get_option('moveto_distance_unit') == 'miles'){echo "checked";} ?>/>
																		<span>Miles</span>
																</label>
															</td>
														</tr>
														<tr>
															<!-- <td>
															 <label class="distance-title"><?php echo __("Pricing","mp");?></label>
															
															</td> -->
															<td></td>
														</tr>
														<!-- <tr>
															<td>
																<label>Distance</label>
															</td>
															<td>
																<label>Rule</label>
															</td>
														</tr> -->
														<?php
														$all_records = $read_distance->readAll();
														foreach ($all_records as $print )   
														{
														?>
														<!-- <tr>
															<td>
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0 mt-20">
																	<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
																		<input type="text" name="distance_update" id="distance_update<?php echo $print->id;?>" class="form-control" value="<?php echo $print->distance;?>">	
																	</div>
																</div>
															</td>
															<td>
															<div style="padding: 0;" class="col-sm-12 col-md-12 col-lg-12 col-xs-12 ta-c mt-20">
																	<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3 pd-0" >
																		
																		<select id="moveto_distance_rule_update<?php echo $print->id;?>" class="selectpicker" data-size="10"  style="display: none;" value="<?php echo $print->rules;?>">
																			  <option <?php if ($print->rules == 'E'){ ?>selected<?php } ?>
                                                                    value="E">=
                                                    </option>
                                                    <option <?php if ($print->rules == 'G'){ ?>selected<?php } ?>
                                                                    value="G">></option>
																		</select>
																	</div>

																	<div class="col-sm-4 col-md-4 col-lg-6 col-xs-5">
																		<input type="text" name="price_update" id="price_update<?php echo $print->id;?>" class="form-control" id="" value="<?php echo $print->price;?>">	
																	</div>

																	<div class="row col-sm-2 col-md-2 col-lg-2 col-xs-2 ml-5">
																		<a class="pull-right btn-circle btn-success btn-sm" id="mp-update-distance" data-id="<?php echo $print->id;?>" title="Update"><i class="fa fa-edit"></i></a>
																	</div>

																	<div class="row col-sm-2 col-md-2 col-lg-2 col-xs-2 ml-5">
																			<a id="mp-delete-distance" class="pull-right btn-circle btn-danger btn-sm" data-id="<?php echo $print->id;?>"><i class="fa fa-trash padd_2" title="<?php echo __("Delete","mp");?>"></i></a>
																	</div>
																</div>
															</td>
														</tr> -->
														<?php 
														}
															?>	 
														<!-- <tr class="dis-inline">
															<td></td>
															<td></td>
														</tr>
														<tr>	
															<td>
																<div style="padding: 0;" class="col-sm-10 col-md-10 col-lg-12 col-xs-9">
																	<div style="padding: 0;" class="col-sm-12 col-md-12 col-lg-10 col-xs-12">
																		<input type="text" name="distance_di" id="distance_di" class="form-control">	
																	</div>
																</div>
															</td>
															<td>
															<div style="padding: 0;" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
																	<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3 pd-0">
																		<select id="moveto_distance_rule" class="selectpicker" data-size="10"  style="display: none;">
																			<option value="G">></option>
																			<option value="E">=</option>
																		</select>
																	</div>
																	<div class="col-sm-4 col-md-4 col-lg-6 col-xs-5">
																		<input type="text" name="price_distance" id="price_distance" class="form-control" id="" value="">	
																	</div>
																	<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 pdr-0">
																		<a class="btn btn-success" id="mp-add-new-distance"  ><i class="fa fa-plus icon-space "></i><?php echo __("Add New","mp");?></a>
																	</div>
																</div>
															</td>
														</tr> -->
													</tbody>
												</table>
											</form>
										</div>
 
										<!-- *******************Bhupendra****************** -->

										<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
											<form id="mp_insert_elevator_data" method="post" action="" type="" class="slide-toggle" >
												<table class="mp-create-distance-table">
													<tbody>
														<label class="distance-title">Base Price</label>
														<tr>
															<td class="col-sm-3 col-xs-12">
																<label for="mp-service-title"><?php echo __("Rate","mp");?></label>
															</td>
															<td class="col-sm-4 col-xs-12">
																<input type="text" name="" class="form-control" id="mp-mover_min_rate" value="<?php echo get_option('moveto_min_rate_service') ?>" />
															</td>
															<td>
																<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 pdr-0">
																		<a class="btn btn-success" id="mp-mover_price" ><?php echo __("Update","mp");?></a>
																</div> 
															</td>
														</tr>
													</tbody>
												</table>
											</form>
											</div>

										<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
											<form id="mp_insert_elevator_data" method="post" action="" type="" class="slide-toggle" >
												<table class="mp-create-distance-table">
													<tbody>
														<label class="distance-title">Insurance</label>
														<tr>
															<td class="col-sm-3 col-xs-2">
																<label for="mp-service-title"><?php echo __("Percentage (%)","mp");?></label>
															</td>
															<td class="col-sm-4 col-xs-12">
																<input type="text" name="" class="form-control" id="mp-mover_insurance_rate" value="<?php echo get_option('moveto_insurance_rate') ?>" />
															</td>
															<td>
																<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 pdr-0">
																		<a class="btn btn-success" id="mover_insurance_rate" ><?php echo __("Update","mp");?></a>
																</div> 
															</td>
														</tr>
														<tr>
															<td class="col-sm-3 col-xs-12">
																<label for="mp-service-title" class="mt-10"><?php echo __("Insurance","mp");?></label>
															</td>
															<td>
															<?php $check = get_option('moveto_insurance_permission'); 
															?>

															<label class="switch">
															  <input type="checkbox" <?php if($check == 1)
																{echo 'checked';}else{echo '';} ?> class="percentage_toggle">
															  <span class="slider round"></span>
															</label>
															</td>
														</tr>
													</tbody>
												</table>
											</form>
										</div>
										
										
										
										
										<!-- <div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
											<form id="mp_cubicfeets_range" method="post" action="" class="slide-toggle" >
											<label class="distance-title">CubicFeets</label>
												<table class="mp-create-distance-table">
													<tbody>
														<tr>
															<td>
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mt-20">
																 <label for=""><?php echo __("SI Unit","mp");?></label>
																 &nbsp&nbsp
																	<input class="m-0 meter_cube" name="cubic" type="radio" value="ft_3" <?php  if(get_option('moveto_cubic_meter') == 'ft_3'){echo "checked";} ?>/>
																		<span>Cubic Feet</span>
																			<span>&nbsp&nbsp</span>
																		<input class="m-0 meter_cube" name="cubic" type="radio" value="m_3" <?php  if(get_option('moveto_cubic_meter') == 'm_3'){echo "checked";} ?>/>
																		<span>Cubic Meter</span>
																</div>
															</td>
														</tr>
														 <tr>
														 	<td style="">
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0">
																	<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
																	<?php  if(get_option('moveto_cubic_meter') == 'ft_3')
																			{ ?>
																			<label>Per CubicFeets Range</label>
																		<?php	}else{ ?>
																			<label>Per CubicMeter Range</label>
																	<?php	}
																	?>
																	</div>

																</div>
															</td>
															<td style="">
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0">
																	<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
																		<?php  if(get_option('moveto_cubic_meter') == 'ft_3')
																			{ ?>
																			<label>Per CubicFeets Rate</label>
																		<?php	}else{ ?>
																			<label>Per CubicMeter Rate</label>
																		<?php	}
																		?>
																	</div>

																</div>
															</td>
														 </tr>
														<?php
															$all_cubicfeets = $read_cubicfeets->readAll();
															foreach ($all_cubicfeets as $record )   
															{
														 ?>
														 <tr>
														 	<td class="greater_after">
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mb-10 mt-10">
																	<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
																		<input type="text" name="per_cfeets" id="per_cfeets<?php echo $record->id; ?>" class="form-control"  value="<?php echo $record->cubicfeets_range; ?>">
																	</div>

																</div>
															</td>
															<td>
																
																<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 ta-c mtb-10">
																	<div class="row col-sm-12 col-md-12 col-lg-9 col-xs-9 ">
																			<input type="text" name="per_cfeets_rate" id="per_cfeets_rate<?php echo $record->id; ?>" class="form-control"  value="<?php echo $record->cubicfeets_range_price; ?>">	
																	</div>
																	<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3 ml-5">
																		<a class="row pull-right btn-circle btn-success btn-sm" id="mp-update-cfeets" data-id="<?php echo $record->id;?>" title="Update"><i class="fa fa-edit"></i></a>
																	</div>
																	<div class="row col-sm-2 col-md-2 col-lg-2 col-xs-3 ml-5">
																			<a id="mp-delete-cfeets" class="pull-right btn-circle btn-danger btn-sm" data-id="<?php echo $record->id;?>"><i class="fa fa-trash padd_2" title="<?php echo __("Delete","mp");?>"></i></a>
																	</div>
																</div>
															</td>
														 </tr>
														 <?php
															}
														 ?>
														 <tr>	
															<td style="">
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mt-20">
																	<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
																		<input type="text" name="per_cfeets" id="per_cfeets" class="form-control per_cfeets" >
																		<span class="erro_per_cfeets"></span>
																	</div>
																</div>
															</td>
															
															<td>
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mt-20">
																	<div class="row col-sm-12 col-md-12 col-lg-9 col-xs-9 ">
																			<input type="text" name="per_cfeets_rate" id="per_cfeets_rate" class="form-control">
																		<span class="erro_per_cfeets_rent"></span>	
																	</div>
																</div>
																<div class="col-sm-2 col-md-2 col-lg-2 col-xs-2 pdr-0">
																	<a class="btn btn-success" id="add_cfeets"  ><i class="fa fa-plus icon-space "></i><?php echo __("Add New","mp");?></a>
																</div>
															</td>
														 </tr>
													</tbody>
												</table>
											</form>
										</div> -->

										<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
												<form id="mp_cubicfeets_range" method="post" action="" class="slide-toggle" >
												<label class="distance-title">CubicFeets</label>
												<table class="mp-create-distance-table">
												<tbody>
												<tr>
												<td>
												<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mt-20 mb-10">
												<label for=""><?php echo __("SI Unit","mp");?></label>
												&nbsp&nbsp
												<input class="m-0 meter_cube" name="cubic" type="radio" value="ft_3" <?php if(get_option('moveto_cubic_meter') == 'ft_3'){echo "checked";} ?>/>
												<span>Cubic Feet</span>
												<span>&nbsp&nbsp</span>
												<input class="m-0 meter_cube" name="cubic" type="radio" value="m_3" <?php if(get_option('moveto_cubic_meter') == 'm_3'){echo "checked";} ?>/>
												<span>Cubic Meter</span>
												</div>
												</td>
												</tr>
												<tr>
												<td style="">
												<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0">
												<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
												<?php if(get_option('moveto_cubic_meter') == 'ft_3')
												{ ?>
												<label>Per CubicFeets Range</label>
												<?php	}else{ ?>
												<label>Per CubicMeter Range</label>
												<?php	}
												?>
												</div>

												</div>
												</td>
												<td style="">
												<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0">
												<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
												<?php if(get_option('moveto_cubic_meter') == 'ft_3')
												{ ?>
												<label>Per CubicFeets Rate</label>
												<?php	}else{ ?>
												<label>Per CubicMeter Rate</label>
												<?php	}
												?>
												</div>

												</div>
												</td>
												</tr>
												<?php
												$all_cubicfeets = $read_cubicfeets->readAll();
												foreach ($all_cubicfeets as $record ) 
												{
												?>
												<tr>
												<td class="greater_after">
												<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mb-10 mt-10">
												<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
												<input type="text" name="per_cfeets" id="per_cfeets<?php echo $record->id; ?>" class="form-control" value="<?php echo $record->cubicfeets_range; ?>">
												</div>

												</div>
												</td>
												<td>

												<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 ta-c mtb-10 responsive-Per-CubicFeets-Rate">
												<div class="row col-sm-12 col-md-12 col-lg-9 col-xs-9 ">
												<input type="text" name="per_cfeets_rate" id="per_cfeets_rate<?php echo $record->id; ?>" class="form-control" value="<?php echo $record->cubicfeets_range_price; ?>">	
												</div>
												<div class="col-sm-2 col-md-2 col-lg-2 col-xs-3 ml-5">
												<a class="row pull-right btn-circle btn-success btn-sm" id="mp-update-cfeets" data-id="<?php echo $record->id;?>" title="Update"><i class="fa fa-edit"></i></a>
												</div>
												<div class="row col-sm-2 col-md-2 col-lg-2 col-xs-3 ml-5 responsive-trash-edit-btn">
												<a id="mp-delete-cfeets" class="pull-right btn-circle btn-danger btn-sm" data-id="<?php echo $record->id;?>"><i class="fa fa-trash padd_2" title="<?php echo __("Delete","mp");?>"></i></a>
												</div>
												</div>
												</td>
												</tr>
												<?php
												}
												?>
												<tr>	
												<td style="">
												<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mt-10 mb-10">
												<div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
												<input type="text" name="per_cfeets" id="per_cfeets" class="form-control per_cfeets" >
												<span class="erro_per_cfeets"></span>
												</div>
												</div>
												</td>

												<td>
												<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c mt-10 mb-10">
												<div class="row col-sm-12 col-md-12 col-lg-9 col-xs-9 float-left">
												<input type="text" name="per_cfeets_rate" id="per_cfeets_rate" class="form-control">
												<span class="erro_per_cfeets_rent"></span>	
												</div>

												<div class="col-sm-2 col-md-2 col-lg-3 col-xs-3 pdr-0 float-left">
												<a class="btn btn-success ml-10" id="add_cfeets" ><i class="fa fa-plus icon-space "></i><?php echo __("Add New","mp");?></a>
												</div>
												</div>
												</td>
												</tr>
												</tbody>
												</table>
												</form>
												</div>
 
										

										<!-- *****************Floors********************** -->
										
											<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
											<form id="mp_insert_floor" method="post" action="" class="slide-toggle" >
												<table class="mp-create-distance-table" width="100%">
													<tbody>
														<tr>
															<td><label class="distance-title">Floors</label></td>
														</tr>
														<tr>
															<td><label>&nbsp Floor no</label></td>
															<td><label>&nbsp Price (%age)</label></td>
														</tr>
															
														<?php
																 $i = 1;
																$all_records = $read_floor->readAll_floor();
																foreach ($all_records as $print )   
																{
																?>
														<tr>
															<td style="">
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0 mt-20">
																	<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
																		
																		
																				<input type="text" name="floor_no_update" id="floor_no_update<?php echo $print->id;?>" class="form-control" id="" value="<?php echo $print->floor_no;?>">
																		
																	</div>

																</div>
															</td>
																
															<td>
															<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 ta-c mt-20">
																
																	<div class="col-sm-4 col-md-4 col-lg-6 col-xs-5">
																		<input type="text" name="floor_price_update" id="floor_price_update<?php echo $print->id;?>" class="form-control" id="" value="<?php echo $print->price;?>">	
																	</div>

																	<div class="row col-sm-2 col-md-2 col-lg-2 col-xs-3 ml-5">
																		<label></label>
																		<a class="btn-sm pull-left btn-circle btn-success"  id="mp-update-floor" data-id="<?php echo $print->id;?>" title="Update"><i class="fa fa-edit"></i></a>
																	</div>
																	<div class="row col-sm-2 col-md-2 col-lg-2 col-xs-3 ml-5">
																		<label></label>
																			<a id="mp-delete-floor" title="Delete" class="pull-left btn-circle btn-danger btn-sm" data-id="<?php echo $print->id;?>"><i class="fa fa-trash padd_2"></i></a>
																	</div>
																	</div>
																</div>
															</td>
															<?php  $i++; } ?>
														</tr>
														<tr class="dis-inline">
															<td></td>
															<td></td>
														</tr>
														<tr>	
															<td style="">
																<div class="col-sm-10 col-md-10 col-lg-12 col-xs-12 ta-c pd-0">
																	<div class="col-sm-12 col-md-12 col-lg-10 col-xs-9 pd-0">
																		
																			<input type="text" name="floor_no" id="floor_no" class="form-control"  value="">	
																		
																	</div>

																</div>
															</td>
															<td>
															<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12">
																	<div class="col-sm-4 col-md-4 col-lg-6 col-xs-5">
																		<input type="text" name="price" id="price" class="form-control" id="" value="">	
																	</div>
																	<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 pdr-0">
																		<a class="btn btn-success" id="mp-add-new-floor"  ><i class="fa fa-plus icon-space "></i><?php echo __("Add New","mp");?></a>
																	</div>
															</div>
															</td>
														</tr>
														
													</tbody>
												</table>
											</form>
										</div>
									<!-- **************************************** -->

									<!-- *****************Loading/unloading********************** -->

											<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
											<form id="mp_insert_elevator_data" method="post" action="" type="" class="slide-toggle" >
												<table class="mp-create-distance-table">
													<tbody>
														<label class="distance-title">Loading/unloading with elevator</label>
														<?php
														$all_records = $read_elevator->readAll_elevator();
															foreach ($all_records as $prints )   
																{}
																?>
														<tr>
															<td class="col-sm-3 col-xs-12">
																<label for="mp-service-title"><?php echo __("Discount %age","mp");?></label>
															</td>
															<td class="col-sm-4 col-xs-12">
																<input type="text" name="mp_elevator" class="form-control" id="mp-elevator" value="<?php echo $prints->discount;?>" />
															</td>
																	<?php if($prints->id != ''){ ?>
																		<td>
																		<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 pdr-0">
																				<a class="btn btn-success" id="mp-update-elevator" name="mp-update-elevator" data-id="<?php echo $prints->id;?>"><?php echo __("Update","mp");?></a>
																		</div> 
																	</td>
																	<?php  } 
																	  else{ ?>			
																		<td>
																				<div class="col-sm-4 col-md-4 col-lg-4 col-xs-4 pdr-0">
																					<a class="btn btn-success" id="mp_insert_elevator" name="mp_insert_elevator" ><?php echo __("Save","mp");?></a>
																				</div>
																	  </td>
														  		<?php  } ?>
														</tr>
													
													</tbody>
												</table>
											</form>
											</div>

										<!-- **************************************** -->

										<!-- *****************Packing/unpacking********************** -->
										
											<!-- <div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
												<div class="col-sm-12 col-md-12 col-lg-9">
												 <?php $all_records = $read_packing_unpacking->readAll_pac_unpac();
													$ex= count((array)$all_records); ?>
													<form <?php if($ex==1){ echo "id='mp_updatee_pac_unpac_data'";}else{ ?>id="mp_insert_pac_unpac_data" <?php } ?> method="post" action="" type="" class="slide-toggle" >
														<table class="mp-create-distance-table">
															<tbody>
															
																<label class="distance-title">Packing/unpacking</label>
																	
																<tr class="dis-inline">
																	<td></td>
																	<td></td>
																</tr>
																	<?php
																		$all_records = $read_packing_unpacking->readAll_pac_unpac();
																		foreach ($all_records as $pac_unpac )   
																{}
																?>
																<tr>
																	<td><label for="mp-service-title"><?php echo __("Cost for Packing (%age)","mp");?></label></td>
																	
																	<td><input type="text" name="cost_packing" id="cost_packing" class="form-control" value="<?php echo $pac_unpac->cost_packing;?>" /></td>
																</tr>
																<tr class="dis-inline">
																	<td></td>
																	<td></td>
																</tr>
																<tr>
																	<td><label for="mp-service-title"><?php echo __("Cost for Unpacking (%age)","mp");?></label></td>
																	
																	<td><input type="text"  name="cost_unpacking" id="cost_unpacking"  class="form-control"  value="<?php echo $pac_unpac->cost_unpacking;?>"/></td>

																</tr>

															</tbody>
														</table>
													</form>

												</div>
												<?php if($pac_unpac == 1){ ?>
															<div class="col-lg-6 text-right">
																	<a class="btn btn-success" id="mp-update-pac_unpac" name="mp-update-pac_unpac" data-id="<?php echo $pac_unpac->id;?>"><?php echo __("Update","mp");?></a>
														</div>
													<?php  } 
													else{ ?>			
																<div class="col-lg-6 text-right">
																	<a class="btn btn-success" id="mp-inset-pac_unpac" name="mp-insert-pac_unpac" data-id="<?php echo $pac_unpac->id;?>"><?php echo __("Save","mp");?></a>
															</div>
														  		<?php  } ?>
											</div> -->

											<div class="col-sm-12 col-md-12 col-lg-9 col-xs-12 mt-30 pdr-0 distance-border">
													<div class="col-sm-12 col-md-12 col-lg-9">
													<?php $all_records = $read_packing_unpacking->readAll_pac_unpac();
													$ex= count((array)$all_records); ?>
													<form <?php if($ex==1){ echo "id='mp_updatee_pac_unpac_data'";}else{ ?>id="mp_insert_pac_unpac_data" <?php } ?> method="post" action="" type="" class="slide-toggle" >
													<table class="mp-create-distance-table">
													<tbody>

													<label class="distance-title">Packing/unpacking</label>

													<tr class="dis-inline">
													<td></td>
													<td></td>
													</tr>
													<?php
													$all_records = $read_packing_unpacking->readAll_pac_unpac();
													foreach ($all_records as $pac_unpac ) 
													{}
													?>
													<tr>
													<td><label for="mp-service-title"><?php echo __("Cost for Packing (%age)","mp");?></label></td>

													<td><input type="text" name="cost_packing" id="cost_packing" class="form-control" value="<?php echo $pac_unpac->cost_packing;?>" /></td>
													</tr>
													<tr class="dis-inline">
													<td></td>
													<td></td>
													</tr>
													<tr>
													<td><label for="mp-service-title"><?php echo __("Cost for Unpacking (%age)","mp");?></label></td>

													<td><input type="text" name="cost_unpacking" id="cost_unpacking" class="form-control" value="<?php echo $pac_unpac->cost_unpacking;?>"/></td>
													<td> 
													<?php if($pac_unpac->id != ''){ ?>
													<div class="col-lg-6 text-right">
													<a class="btn btn-success" id="mp-update-pac_unpac" name="mp-update-pac_unpac" data-id="<?php echo $pac_unpac->id;?>"><?php echo __("Update","mp");?></a>
													</div>
													<?php } 
													else{ ?>	
													<div class="col-lg-6 text-right">
													<a class="btn btn-success" id="mp-inset-pac_unpac" name="mp-insert-pac_unpac" data-id="<?php echo $pac_unpac->id;?>"><?php echo __("Save","mp");?></a>
													</div>
													<?php } ?>
													</td>

													</tr>

													</tbody>
													</table>
													</form>

													</div>

													</div>
<!-- **************************************** -->
																
									</div>
								</div>
			
		</div>
			
	</div>
</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var serviceObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>