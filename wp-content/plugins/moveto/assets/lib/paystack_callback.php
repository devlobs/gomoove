<?php

$curl = curl_init();


if(!session_id()) { @session_start(); }
	$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));	
if (file_exists($root.'/wp-load.php')) {
	require_once($root.'/wp-load.php');	
}	
$_SESSION['transaction_id'] = '';

$plugin_url = plugins_url('',  dirname(__FILE__));
$base =   dirname(dirname(dirname(__FILE__)));

$moveto_paystack_public_key = urlencode(get_option('moveto_paystack_public_key'));
$moveto_paystack_secret_key = urlencode(get_option('moveto_paystack_secret_key'));

$reference = isset($_GET['reference']) ? $_GET['reference'] : '';
if(!$reference){
  die('No reference supplied');
}

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_HTTPHEADER => [
    "accept: application/json",
   "authorization: Bearer ".$moveto_paystack_secret_key."",
    "cache-control: no-cache"
  ],
));

$response = curl_exec($curl);
$err = curl_error($curl);

if($err){
    // there was an error contacting the Paystack API
  die('Curl returned error: ' . $err);
}

$tranx = json_decode($response);

if(!$tranx->status){
  // there was an error from the API
  die('API returned error: ' . $tranx->message);
}

if('success' == $tranx->status){
  $_SESSION['transaction_id'] = $tranx->data->reference;
  $transaction_id = $_SESSION['transaction_id'];
	$thankyou_url = get_option('moveto_thankyou_page');
	$thankyou_page_url = "";
	if($thankyou_url != '' && $thankyou_url != null){
		$thankyou_page_url = $thankyou_url;
	}else{
		$thankyou_page_url = "../../frontend/mp_thankyou.php";
	}  ?>
	<script src="../js/jquery-2.1.4.min.js" type="text/javascript"></script><script>
		   var dataString = <?php   echo $_SESSION['paystack_post_session']; ?>;
			function checkout_at_booking_complete(trans_id){
				dataString['payment_method'] = "paystack_payment";
				dataString['paytm_transaction_id'] = trans_id;
				
				jQuery.ajax({
					type:'POST',
					url:'mp_front_ajax.php',
					data: dataString,
					success:function(response){
							window.location.href='<?php echo $thankyou_page_url;?>';
					}
				});
			}
			checkout_at_booking_complete('<?php echo $transaction_id; ?>');
			</script>
			<?php 
}	
