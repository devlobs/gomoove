<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_move_size.php');

	$movesize = new moveto_size();
	$general = new moveto_general();
	$staff = new moveto_staff();
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	
	$mp_currency_symbol = get_option('moveto_currency_symbol');
/* insert room type */	
if(!empty($_POST['action']) && $_POST['action']=='move_size_data'){
	$movesize->name = $_POST['movesize_title'];
	$movesize->insert_movesize();  
}
/* update room type */	
if(!empty($_POST['action']) && $_POST['action']=='update_movesize'){
	$movesize->id = $_POST['movesize_id'];
	$movesize->name = $_POST['movesize_title'];
	$movesize->movesize_update();  
}

/* delete room type */	
if(!empty($_POST['action']) && $_POST['action']=='delete_movesize'){
	$movesize->id = $_POST['id']; 
	$movesize->movesize_delete();
}
/* update room type status */
if(isset($_POST['action']) && $_POST['action'] == "movesize_update_status"){
	$movesize_id = $_POST['id'];
	$status = $_POST['status'];	
	global $wpdb;
    $query = "UPDATE ".$wpdb->prefix."mp_movesize SET status ='$status' WHERE	id =$movesize_id";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}
/* sorting */
if(isset($_POST['position'],$_POST['service_action']) && $_POST['position']!='' && $_POST['service_action']=='sort_movesize_position'){
		parse_str($_POST['position'], $output);
		$order_counter=0;
		foreach ($output as $order_no){
			foreach($order_no as $order_value){			 
			  $movesize->position = $order_counter;
			  $movesize->id = $order_value;
			  $movesize->sort_movesize_position();
			$order_counter++;
			}
		}
}
 ?>			