<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));

	 /* Intialization of Class Object */
	$mp_bookings = new moveto_booking();
	$todayAnalyatics = $mp_bookings->get_today_booking_and_earning();
	$weekstartdate = date_i18n('Y-m-d',strtotime('monday this week'));
	$weekenddatedate = date_i18n('Y-m-d',strtotime('sunday this week'));
	$weekAnalyatics = $mp_bookings->get_week_booking_and_earning($weekstartdate,$weekenddatedate);
	$yearAnalyatics = $mp_bookings->get_year_booking_and_earning();	
	$upcommingbookings = $mp_bookings->today_upcomming_appointments();
	$latestactivitybookings = $mp_bookings->get_booking_by_latest_activity();
	$compaltedquickactionbookings = $mp_bookings->get_compalted_quickaction_bookings();	
	$mp_currency_symbol = get_option('moveto_currency_symbol');
?>
<div id="pam_send_quote" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo __("Send Quote","mp");?></h4>
            </div>
            <form id="pam_send_quote_form" method="post">
				<input type="hidden" class="form-control" id="pam_quote_booking_id" value="" />
				<div class="modal-body">
					<div class="form-group">
						<label for="pam_quote_price"><?php echo __("Quote Price","mp");?></label>
						<input type="text" class="form-control" name="pam_quote_price" id="pam_quote_price" placeholder="<?php echo $quote_price;?>" />
						<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
					</div>
					<div class="form-group">
						<label for="pam_quote_description"><?php echo __("Quote Description","mp");?></label>
						<textarea id="pam_quote_description" class="form-control" rows="3" cols="5"></textarea>
					</div>	
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pam_quote_cancel" data-dismiss="modal"><?php echo __("Cancel","mp");?></button>
					<button type="button" id="pam_submit_quote" class="btn btn-primary"><?php echo __("Send Now","mp");?></button>
				</div>
			</form>
        </div>
    </div>
</div>	
<div class="panel mp-panel-default" id="mp-dashboard">
	<div class="panel-body">
		<ul class="nav nav-tab mp-top-menus-stats">
			<li class="active col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-10">
				<a href="#mp-today-stats" data-toggle="pill">
					<div class="mp-title-amount-stats">
						<div class="mp-icon-booking-stats pull-left">
							<div class="mp-dash-icon today costom-color-icone">
							<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
							</div>
							<div class="mp-dash-details of-h">
								<span class="mp-stats-title"><?php echo __('Leads',"mp"); ?></span>
								<span class="mp-stats-counting"><?php echo $todayAnalyatics[0]->bookings;?></span>
							</div>
						</div>
						<h4 class="mp-dash-header pull-right"><?php echo __('Today',"mp"); ?></h4>
					</div>					
				</a>
			</li>
			<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-10">
				<a href="#mp-this-week-stats" data-toggle="pill">
					<div class="mp-title-amount-stats">
						<div class="mp-icon-booking-stats pull-left">
							<div class="mp-dash-icon this-week">
							<i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
							</div>
							<div class="mp-dash-details of-h">
								<span class="mp-stats-title"><?php echo __('Leads',"mp"); ?></span>
								<span class="mp-stats-counting"><?php echo $weekAnalyatics[0]->bookings;?></span>
							</div>
						</div>
						<h4 class="mp-dash-header pull-right"><?php echo __('This Week',"mp"); ?></h4>
					</div>						
				</a>
			</li>
			<li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-10">
				<a href="#mp-this-year-stats" data-toggle="pill">
					<div class="mp-title-amount-stats">
						<div class="mp-icon-booking-stats pull-left">
							<div class="mp-dash-icon this-year">
							<i class="fa fa-calendar-plus-o icons"></i>
								<!--<i class="icon-diamond"></i>-->
							</div>
							<div class="mp-dash-details of-h">
								<span class="mp-stats-title"><?php echo __('Leads',"mp"); ?></span>
								<span class="mp-stats-counting"><?php echo $yearAnalyatics[0]->bookings;?></span>
							</div>
						</div>
						<h4 class="mp-dash-header pull-right"><?php echo __('This Year',"mp"); ?></h4>
						
					</div>					
				</a>
			</li>
		</ul>
<div class="panel-body">	
	<!-- new Todays comming appointments -->
	
	<!-- booking activity -->
	<div class="mp-bookings-activity col-md-12 col-lg-12 rnp  mb-20">
		<div class="panel panel-default h-450 br-2 b-shadow mt-20">
			<div class="panel-heading  bg-info"><?php echo __('Latest Leads Activity',"mp"); ?></div>
			<div class="col-md-12 col-sm-12 col-xs-12 np ofh top-label">
				<label class="col-md-2 col-lg-2 col-sm-6 col-xs-12 np ofh ta-c"><?php echo __("Move Size","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Lead Date","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Client Name","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Source","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Destination","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 ofh ta-c np"><span class="pr-20"><?php echo __("Lead Status","mp");?></span></label>
			</div>
			<hr id="hr" />
			<div class="panel-body lead-body">
				<?php if(sizeof((array)$latestactivitybookings)==0){ ?>
				<div class="mp-no-today-booking-message no-lead-body">
					<i class="fa fa-clock-o fa-3x"></i>
					<h3><?php echo __('No latest lead',"mp"); ?> </h3>		
				</div>
				<?php }else{ ?>
				<div class="mp-latest-activity">
					<div class="mp-bookings-activity-list">
				<?php 	$move_size = "";
						foreach($latestactivitybookings as $latestactivity){
								if($latestactivity->move_size){
									$move_size=$latestactivity->move_size;
								}else{
									$move_size = "N/A";
								}
								
								if($latestactivity->user_info!=''){
									$userinfo = unserialize($latestactivity->user_info);
								
									$clientname = ucwords($userinfo['name']);
								}else{
									$clientname = 'N/A';
								}
									
								$source_city = base64_decode($latestactivity->source_city);
								$destination_city = base64_decode($latestactivity->destination_city);
						?>
						<div class="mp-today-list mp-activity-list col-md-12" data-bookingid = '<?php echo $latestactivity->id;?>' data-toggle="modal" data-target="#booking-details">
							
							
							<div class="col-md-12 col-sm-12 col-xs-12 np ofh">
								<span class="col-md-2 col-lg-2 col-sm-6 col-xs-12 ta-c np ofh"><?php echo __(stripslashes_deep($move_size),"mp");?> </span>
								<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 ta-c np ofh"><i class=""></i><?php echo date_i18n(get_option('date_format'),strtotime($latestactivity->booking_date)); ?>							
								</span>
								<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 ta-c np ofh"><i class=""></i><?php echo $clientname;?></span>
								<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 ta-c np ofh"><i class="icon-space"></i><?php echo $source_city;?></span>
								<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 ta-c ofh np"><i class="icon-space"></i><?php echo $destination_city;?></span>
								<div class="col-md-2 col-lg-2 col-sm-6 col-xs-12 mobile-align ta-c np">
									<?php if($latestactivity->booking_status=='P' || $latestactivity->booking_status==''){
										echo '<span class="mp-label btn-info br-2">'.__('Pending','mp').'</span>';
									}else{
										echo '<span class="mp-label btn-success br-2">'.__('Completed','mp').'</span>';
									} ?>
								
								
								</div>
											
							</div>							
						</div>						
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- booking activity -->
	<div class="mp-bookings-activity col-md-12 col-lg-12 rnp mt-10">
		<div class="panel panel-default h-450 br-2 b-shadow">
			<div class="panel-heading  bg-primary"><?php echo __('Completed Leads Quick Actions',"mp"); ?></div>
			<div class="col-md-12 col-sm-12 col-xs-12 np ofh top-label">	 
				<label class="col-md-2 col-lg-2 col-sm-6 col-xs-12 np ofh ta-c"><?php echo __("Move Size","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Lead Date","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Client Name","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><?php echo __("Source","mp");?></label>
				<label class="col-md-1 col-sm-6 col-lg-1 col-xs-12 np ofh ta-c"><?php echo __("Destination","mp");?></label>
				<label class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ofh ta-c"><span class="pr-20"><?php echo __("Quote","mp");?></span></label>
				<label class="col-md-1 col-sm-6 col-lg-1 col-xs-12 ofh ta-c np"><span class="pr-20"><?php echo __("Status","mp");?></span></label>
				
			</div>
			<hr id="hr" />
			<div class="panel-body lead-body">
				<?php if(sizeof((array)$compaltedquickactionbookings)==0){ ?>
				<div class="mp-no-today-booking-message no-lead-body">
					<i class="fa fa-clock-o fa-3x"></i>
					<h3><?php echo __('No Past Lead For Quick Action',"mp"); ?> </h3>		
				</div>
				<?php }else{ ?>
				<div class="mp-latest-activity pl-10 pr-10">
					<div class="mp-bookings-activity-list">
					
						<?php
						$moves = "";
						 foreach($compaltedquickactionbookings as $pastquickactionbooking){
								
								if($pastquickactionbooking->service_id==1){
									$service_title = __('Home','mp');	
								}elseif($pastquickactionbooking->service_id==2){
									$service_title = __('Office','mp');
								}elseif($pastquickactionbooking->service_id==3){
									$service_title = __('Vehicle','mp');
								}elseif($pastquickactionbooking->service_id==4){
									$service_title = __('Pets','mp');
								}elseif($pastquickactionbooking->service_id==5){
									$service_title = __('Commercial','mp');
								}else{
									$service_title = __('Other','mp');
								}
								if($pastquickactionbooking->move_size){
									$moves = $pastquickactionbooking->move_size;
								}else{
									$moves = "N/A";
								}
								if($pastquickactionbooking->user_info!=''){
									$userinfo = unserialize($pastquickactionbooking->user_info);
									if($userinfo['first_name']){
										$clientname = ucwords($userinfo['first_name'].' '.$userinfo['last_name']);
									}elseif($userinfo['name']){
										$clientname = ucwords($userinfo['name']);
									}
								}else								{
									$clientname = 'N/A';
								}
								$source_city = base64_decode($pastquickactionbooking->source_city);
								$destination_city = base64_decode($pastquickactionbooking->destination_city);
								
						?>
						
						<div class="mp-today-list-actions">
										<div class="mp-past-booking-button col-md-2 col-lg-2 col-sm-6 col-xs-12 mobile-align ta-r np">
											<div class="btn-group btn-group-xs m-50 db">			   
											   <button type="button" data-id="mp-delete-past-booking<?php echo $pastquickactionbooking->id;?>" class="mr-5 btn btn-danger mp_delete_past_booking" rel="popover" data-placement='left' title="Delete Lead?"><?php echo __('Delete','mp');?></button>
											   
											   <div id="popover-mp-delete-past-booking<?php echo $pastquickactionbooking->id;?>" style="display: none;">
													<div class="arrow"></div>
													<table class="form-horizontal" cellspacing="0">
														<tbody>												
															<tr>
																<td>
																	<button id="mp_booking_delete" data-booking_id="<?php echo $pastquickactionbooking->id;?>" value="Delete" class="btn btn-danger btn-sm" type="submit"><?php echo __('Yes','mp');?></button>
																	<button class="btn btn-default btn-sm mp_close_delete_booking_popover" href="javascript:void(0)"><?php echo __('Cancel','mp');?></button>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											   
											   
										   </div>
										</div>	
							<div class="mp-today-list mp-activity-list col-md-12" data-bookingid = '<?php echo $pastquickactionbooking->id;?>' data-toggle="modal" data-target="#booking-details">
								
									<div class="col-md-12 col-sm-12 col-xs-12 np ofh">
										<span class="col-md-2 col-lg-2 col-sm-6 col-xs-12 np ta-c ofh"><?php echo __(stripslashes_deep($moves),"mp");?> </span>
										<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ta-c ofh"><i class=""></i><?php echo date_i18n(get_option('date_format'),strtotime($pastquickactionbooking->booking_date)); ?>							
										</span>
										<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ta-c ofh"><i class=""></i><?php echo $clientname;?></span>
										<span class="col-md-2 col-sm-6 col-lg-2 col-xs-12 np ta-c ofh"><i class="icon-space"></i><?php echo $source_city;?></span>
										<span class="col-md-3 col-sm-6 col-lg-1 col-xs-12 ofh ta-c np"><i class="icon-space"></i><?php echo $destination_city;?></span>
										<span class="col-md-3 col-sm-6 col-lg-2 col-xs-12 ofh ta-c np"><i class="icon-space"></i><?php echo $mp_currency_symbol."".$pastquickactionbooking->quote_price;?></span>
									</div>							
								<!--<div class="col-md-12 col-sm-12 col-xs-12 np mt-5 mb-5 ofh"><span class="mp-service-text"><?php echo __(stripslashes_deep($service_title),"mp");?></span></div>
									
								<div class="mt-5 mb-5 col-md-12 col-sm-12 np">
									<span class="col-md-3 col-sm-3 col-lg-3 col-xs-12 np ofh"><?php echo __('On','mp');?> <?php echo date_i18n('l',strtotime($pastquickactionbooking->booking_date)); ?></span>
									<span class="col-md-6 col-sm-5 col-lg-5 col-xs-12 np ofh"><i class="icon-calendar icon-space"></i><?php echo date_i18n(get_option('date_format'),strtotime($pastquickactionbooking->booking_date)); ?>									
									</span>
									<span class="col-md-3 col-sm-4 col-lg-4 col-xs-12 np ofh"><i class="icon-user icon-space"></i><?php echo $clientname;?></span>
								</div> -->
								
							</div>
						</div>	
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	
	
</div>
</div>

<?php 
	include('footer.php');	
?>