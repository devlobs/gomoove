<?php 
if ( ! defined( 'ABSPATH' ) ) exit;
include_once "header.php";
global $wpdb;
$page_title = "Clients";

$plugin_url_for_ajax = plugins_url('',  dirname(__FILE__));
$user_ID = get_current_user_id();
$order_client_info = array();

$mp_bookings = new moveto_booking(); 
$clients = new moveto_clients();


$all_clients_info = $clients->get_registered_clients();
/** Code For Guest User **/
$all_guesuser_info = $clients->get_all_guest_users_orders();
?>

<div id="mp-customers-listing" class="panel tab-content">
	<div class="panel panel-default">
		<div class="tab-content">
			<div id="registered-customers-listing" class="tab-pane fade in active">
				<h3><?php echo __("Registered Customers","mp");?></h3>  <div class=""></div>
					<div id="accordion" class="panel-group">
						<table id="registered-client-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th><?php echo __("Client Name","mp");?></th>
										<th><?php echo __("Email","mp");?></th>
										<th><?php echo __("Phone","mp");?></th>
										<th class="thd-w200"><?php echo __("Leads","mp");?></th>
									</tr>
								</thead>
								<tbody id="mp_registered_list" >
									<?php 
										
										foreach($all_clients_info as $client_info){
											
												$mp_bookings->client_id = $client_info->ID;
												$all_booking = $mp_bookings->get_client_all_bookings_by_client_id();
												
												if(sizeof((array)$all_booking)>0){
													$allboking = sizeof((array)$all_booking)-1;
													$clientlastoid = $all_booking[$allboking]->order_id;
													$mp_bookings->order_id = $clientlastoid;
													$order_client_info = $mp_bookings->get_all_bookings_by_order_id();
												}
												
												if($order_client_info[0]->user_info!=''){
													$userinfo = unserialize($order_client_info[0]->user_info);
													$customer_name = ucwords($userinfo['name']);
													$customer_phone = $userinfo['phone'];
													$customer_email = $userinfo['email'];
												}else{
													$customer_name = '';
													$customer_phone = '';
													$customer_email = '';
												}
												$customer_name_book=$client_info->display_name;
												$customer_name_email=$client_info->user_email;
											?>
												<tr id="client_detail<?php echo $client_info->ID; ?>">
													<td><?php echo __(stripslashes_deep($customer_name_book),"mp");?></td>
													<td><?php echo __($customer_name_email,"mp");?></td>
													<td><?php echo __($customer_phone,"mp");?></td>
																
													
													
												<td class="mp-bookings-td">
													<a class="btn btn-primary mp_show_bookings " data-method="registered" data-client_id='<?php echo $client_info->ID; ?>' href="#registered-details" data-toggle="modal"><i class="icon-calendar icons icon-space"></i> <?php echo __("Leads","mp");?><span class="badge"><?php echo sizeof((array)$all_booking); ?></span></a>
																										
													<a data-poid="popover-delete-reg-client<?php echo $client_info->ID;?>" class="col-sm-offset-1 btn btn-danger mp-delete-popover " rel="popover" data-placement='bottom' title="<?php echo __("Delete this Client?","mp");?>"> <i class="fa fa-trash icon-space " title="<?php echo __("Delete Client","mp");?>"></i><?php echo __("Delete","mp");?></a>
													<div id="popover-delete-reg-client<?php echo $client_info->ID;?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<button data-method="registered" data-client_id="<?php echo $client_info->ID;?>" value="Delete" class="btn btn-danger btn-sm mp_delete_client" type="submit"><?php echo __("Yes","mp");?></button>
																		<button class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>											
													</td>
												</tr>
										   <?php  } ?>
								</tbody>
							</table>
						
						<div id="registered-details" class="modal fade booking-details-modal">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title"><?php echo __("Registered Customers Leads","mp");?></h4>
									</div>
									<div class="modal-body">
										<div class="table-responsive"> 
										<table id="registered-client-booking-details"  class="display responsive table table-striped table-bordered" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th style="width: 9px !important;"><?php echo __("Id","mp");?></th>
														<th style="width: 9px !important;"><?php echo __("Move Size","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Source","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Destination","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Lead Date","mp");?></th>
														<th style="width: 67px !important;"><?php echo __("Service Info","mp");?></th>											
														
														<th style="width: 70px !important;"><?php echo __("Additional Info","mp");?></th>
														<th style="width: 44px !important;"><?php echo __("Loading Details","mp");?></th>
														<th style="width: 39px !important;"><?php echo __("Unloading Details","mp");?></th>								
														<th style="width: 257px !important;"><?php echo __("Lead Client Info","mp");?></th>
														
														
														<th style="width: 257px !important;"><?php echo __("Quote Price","mp");?></th>
														
													</tr>
												</thead>
												<tbody id="mp_client_bookingsregistered"></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			</div>
			<div id="guest-customers-listing" class="tab-pane fade">
				<h3><?php echo __("Guest Customers","mp");?></h3>
				<div id="accordion" class="panel-group">

					<table id="guest-client-table" class="display responsive table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								
								<th><?php echo __("Client Name","mp");?></th>
								<th><?php echo __("Email","mp");?></th>
								<th><?php echo __("Phone","mp");?></th>
								<th class="thd-w200"><?php echo __("Leads","mp");?></th>
								
							</tr>
						</thead>
						<tbody id="mp_guest_list">
							
							<?php foreach($all_guesuser_info as $client_info){						
									$mp_bookings->order_id = $client_info->order_id;				
									$all_booking=$mp_bookings->get_all_bookings_by_order_id();
									if($all_booking[0]->user_info!=''){
										$userinfo = unserialize($all_booking[0]->user_info);
										$customer_name = $userinfo['first_name'].' '.$userinfo['last_name'];
										$customer_phone = $userinfo['user_phone'];
										$customer_email = $userinfo['email'];
									}else{
										$customer_name = '';
										$customer_phone = '';
										$customer_email = '';
									}
									
								?>
									<tr id="client_detail<?php echo $client_info->order_id; ?>">
										<td><?php echo __(stripslashes_deep($customer_name),"mp");?></td>
										<td><?php echo __($customer_email,"mp");?></td>
										<td><?php echo __($customer_phone,"mp");?></td>
																
										<td class="mp-bookings-td"> 
										<a class="btn btn-primary mp_show_bookings" data-method="guest" data-client_id='<?php echo $client_info->order_id; ?>' href="#guest-details" data-toggle="modal"><i class="icon-calendar icons icon-space"></i><?php echo __("Leads","mp");?><span class="badge"><?php echo sizeof((array)$all_booking); ?></span></a>
																										
										<a data-poid="popover-delete-guest-client<?php echo $client_info->order_id; ?>" class="col-sm-offset-1 btn btn-danger mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this Client?","mp");?>"> <i class="fa fa-trash icon-space " title="<?php echo __("Delete Client","mp");?>"></i><?php echo __("Delete","mp");?></a>
										
										<div id="popover-delete-guest-client<?php echo $client_info->order_id; ?>" style="display: none;">
											<div class="arrow"></div>
											<table class="form-horizontal" cellspacing="0">
												<tbody>
													<tr>
														<td>
															<button data-method="guest" data-client_id="<?php echo $client_info->order_id;?>" value="Delete" class="btn btn-danger btn-sm mp_delete_client" type="submit"><?php echo __("Yes","mp");?></button>
															<button class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>									
									</tr>
									   <?php  } ?>
							</tbody>
					</table>
						
					<div id="guest-details" class="modal fade booking-details-modal">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title"><?php echo __("Guest Customers Leads","mp");?></h4>
								</div>
								<div class="modal-body">
									<div class="table-responsive">
										<div class="table-responsive"> 
										<table id="guest-client-booking-details" class="display responsive table table-striped table-bordered" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th style="width: 9px !important;"><?php echo __("Id","mp");?></th>
													<th style="width: 48px !important;"><?php echo __("Source","mp");?></th>
													<th style="width: 48px !important;"><?php echo __("Destination","mp");?></th>
													<th style="width: 48px !important;"><?php echo __("Lead Date","mp");?></th>
													<th style="width: 67px !important;"><?php echo __("Service Info","mp");?></th>											
													<th style="width: 44px !important;"><?php echo __("Articles Info","mp");?></th>
													<th style="width: 70px !important;"><?php echo __("Additional Info","mp");?></th>
													<th style="width: 44px !important;"><?php echo __("Loading Details","mp");?></th>
													<th style="width: 39px !important;"><?php echo __("Unloading Details","mp");?></th>								
													<th style="width: 257px !important;"><?php echo __("Leads Client Info","mp");?></th>
													<th style="width: 257px !important;"><?php echo __("Notes","mp");?></th>
													<th style="width: 257px !important;"><?php echo __("Quote Price","mp");?></th>
													<th style="width: 257px !important;"><?php echo __("Custom Fields Info","mp");?></th>
												</tr>
											</thead>
											<tbody id="mp_client_bookingsguest">
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<?php 
	include_once "footer.php";
?>
 <script type="text/javascript">
   var ob_client_listing = {"plugin_path":"<?php echo $plugin_url_for_ajax;?>",   "message_deleteclient":"<?php echo __("Booking(s) for this client will be deleted as well, Do you want to delete it?")?>",   "message_recdelete":"<?php echo __("Record deleted!")?>"   };
</script>