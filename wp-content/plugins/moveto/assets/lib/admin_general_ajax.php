<?php 

error_reporting('E_ALL');
ini_set('display_error', 1);
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
if (file_exists($root.'/wp-load.php')) {
	require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	$mp_service = new moveto_service();
	$moveto_additional_info = new moveto_additional_info();
	$general = new moveto_general();
	$clients = new moveto_clients();
	$order_client_info = new moveto_order();
	$mp_city = new moveto_city();
	$mp_booking = new moveto_booking();
	$email_template = new moveto_email_template();
	$roomtype_obj=new moveto_room_type();
	$moveto_article_category_obj=new moveto_article_category();
	$msg_template = $email_template->email_parent_template;	
	include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_sms_templates.php');
	$obj_sms_template = new moveto_sms_template();
	
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	$quote_price = "";
	
	/* Email Content Type Header */ 
	function set_content_type() {			
		return 'text/html';		
	}
	


/** Get Registerd User All Bookings **/
if(isset($_POST['general_ajax_action'],$_POST['method']) && $_POST['general_ajax_action']=='get_client_bookings' && $_POST['method']=='registered'){
	
	$mp_booking->client_id=$_POST['listing_client_id'];								
	$bookings = $mp_booking->get_client_all_bookings_by_client_id();

	foreach($bookings as $booking){
		if($booking->booking_status=='D'){
			$booking_status = __('Completed','mp');
		}else{
			$booking_status = __('Pending','mp');
		}	
		
		$booking_date = date_i18n('Y-m-d',strtotime($booking->booking_date));
		$source_city = base64_decode($booking->source_city);
		$via_city = base64_decode($booking->via_city);
		$destination_city = base64_decode($booking->destination_city);
		
		  
		if($booking->booking_status=='D'){
			$bookingstatus = __('Completed','mp');
		}else{
			$bookingstatus = __('Pending','mp');
		}
		/* Get User Info */
		if($booking->user_info!=''){
			$userinfo = unserialize($booking->user_info);
			$pam_name = $userinfo['name'];
			$pam_email = $userinfo['email'];
			$pam_phone = $userinfo['phone'];
		}else{
			$pam_email = ''; $pam_name = ''; $pam_phone = '';
		}

		/* Get Service Info */
		$service_info = '';
		if($booking->service_info!=''){
			$home_type_info = $booking->move_size;

			$service_info_arr = unserialize($booking->service_info);

			foreach ($service_info_arr as $key => $value) {
				if(count((array)$value) > 0){
					 if(isset($value['get_article_room_id'])) {
					 $room_id=$value['get_article_room_id'];
					 $moveto_article_category_obj->id=$room_id;
				}
				if(isset($value['get_article_id'])) {
					 $article_id=$value['get_article_id'];
				}
				if(isset($value['get_article_price'])) {
					 $article_price=$value['get_article_price'];
				}
				
					$room_type_name_array= $moveto_article_category_obj->readOne_room_type();
					if(!empty($room_type_name_array))
					{
						echo $room_type_name= $room_type_name_array[0]->name;
						$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Room Type',"mp").'</h5><span class="span-scroll span_indent">'.$room_type_name.'</span></li>';
					}
			
				if(isset($value['get_article_name'])) {
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Name',"mp").'</h5><span class="span-scroll span_indent">'.$value['get_article_name'].'</li>';
					}
					if(isset($value['count'])) {
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Qty',"mp").'</h5><span class="span-scroll span_indent">'.$value['count'].'</li>';
					}
					/*if(isset($value['get_article_price'])) {
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Base Price',"mp").'</h5><span class="span-scroll span_indent">'.$value['get_article_price'].'</li>';
					
					}*/
					/*if(isset($value['tot_article_price'])) {
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Total Price',"mp").'</h5><span class="span-scroll span_indent">'.$value['tot_article_price'].'</li>';
					}*/
					
					}
				}
		}
										
		/* Get Articles Info */
		/* $articles_info_arr = unserialize($booking->articles_info); */
		
		$articles_info = '';
	/* 	if($booking->service_id==6){
		if($booking->articles_info!=''){
			$articles_info_arr = unserialize($booking->articles_info);
			
			
			$i=1;
			foreach($articles_info_arr['other_service_article'] as $rer){
			
				$articles_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Article","mp").$i.'</h5><span class="span-scroll span_indent">'.$rer.'</span></li>';
				$i++;
			}	
			}
		}else{ */
/* 		if($booking->articles_info!=''){
			$articles_info_arr = unserialize($booking->articles_info);
			if(sizeof((array)$articles_info_arr) > 0){

				
				foreach($articles_info_arr as $key => $value){
					$mp_service->addon_id = $key;
					$article_name = $mp_service->readOne_addon();
					if(sizeof((array)$article_name) > 0){
					$articles_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. $article_name[0]->addon_service_name.'</h5><span class="span-scroll span_indent">'.$value.'</span></li>';
				}
			}
			}
			} */
		/* } */
			/* Get Additional Info */

			$additional_info = '';
			if($booking->additional_info!=''){
				
				$additional_info_arr = unserialize($booking->additional_info);
				$i=1;
				if(sizeof((array)$additional_info_arr['favorite'])>0){

						$returndata = $moveto_additional_info->readAll();

						$additionalArr = array();
						foreach ($additional_info_arr['favorite'] as $value) {
						array_push($additionalArr, $value);
						}
							foreach($returndata as $additional){

								if(in_array($additional->id, $additionalArr)){
									$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$additional->additional_info.'</h5><span class="span-scroll span_indent">Yes</span></li>';
								}else
								{

									$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$additional->additional_info.'</h5><span class="span-scroll span_indent">No</span></li>';
								}

							}

					}	
					
			}
		
		/* Get Loading Info */

										$loading_info = '';
										if($booking->loading_info!=''){										
											$loading_info_arr = unserialize($booking->loading_info);	
												
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Loading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_number"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price Of Loading Floor',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_checked"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator Price',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Packaging  ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["packaging_checked"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Packaging Price ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["packaging_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Street Address',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_one"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_two"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_three"].'</span></li>';
																				
										}
										/* Get UnLoading Info */
										$unloading_info = '';
										if($booking->unloading_info!=''){										
											$unloading_info_arr = unserialize($booking->unloading_info);	
																				
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unloading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_number"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price of Unloading Floor ',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_checked"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator Price',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Unpackaging ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_checked"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Unpackaging Price',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Street Address',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_one"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_two"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_three"].'</span></li>';
											
										}
		/* Get Booking Client Info */
		$user_info = '';
		if($booking->user_info!=''){										
			$user_info_arr = unserialize($booking->user_info);	
			$name = ucwords($user_info_arr['name']);																			
			$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Name',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["name"].'</span></li>';
			$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Email',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["email"].'</span></li>';
			$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Phone',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["phone"].'</span></li>';
		}							
		
		
		?>
		<tr>
			<td><?php echo $booking->order_id; ?></td>
			<td><?php echo $booking->move_size; ?></td>
			<td><?php echo $source_city; ?></td>
			
			<td><?php echo $destination_city; ?></td>
			<td><?php echo date_i18n(get_option('date_format'),strtotime($booking_date)); ?></td>
			<td><?php echo $service_info; ?></td>
			
			<td><?php echo $additional_info; ?></td>
			<td><?php echo $loading_info; ?></td>
			<td><?php echo $unloading_info; ?></td>
			<td><?php echo $user_info; ?></td>
			<?php if($booking->quote_price!=""){
					?>
					<td><?php echo get_option('moveto_currency_symbol').$booking->quote_price; ?></td>
					<?php
				}else{?>
				<td><?php echo "-"; ?></td>
				<?php } ?>
			
		</tr>
<?php }

}
/** Delete Registered Client & releated Info **/
if (isset($_POST['general_ajax_action'],$_POST['delete_id']) && $_POST['delete_id'] != '' && $_POST['general_ajax_action']=='delete_registered_client') {
		
		$user_info = get_userdata($_POST['delete_id']);
		
		if(isset($user_info->caps,$user_info->roles) && isset($user_info->caps['mp_client']) && in_array('administrator',$user_info->roles)){
			$user = new WP_User($_POST['delete_id']);
			$user->remove_cap( 'mp_client' );
		}else{
			 wp_delete_user($_POST['delete_id']); 
		}
		       
		$mp_booking->client_id = $_POST['delete_id'];
		$mp_booking->delete_users_booking_by_user_id();   
		
}


/** Get Guest Client Bookings **/
if(isset($_POST['general_ajax_action'],$_POST['method']) && $_POST['general_ajax_action']=='get_client_bookings' && $_POST['method']=='guest'){
			$mp_booking->order_id = $_POST['listing_client_id'];
			$mp_booking->readOne_by_booking_order_id();
								
								
			if($mp_booking->service_id==1){
						$service_title = __('Home','mp');
					}elseif($mp_booking->service_id==2){
						$service_title = __('Office','mp');
					}elseif($mp_booking->service_id==3){
						$service_title = __('Vehicle','mp');
					}elseif($mp_booking->service_id==4){
						$service_title = __('Pets','mp');
					}elseif($mp_booking->service_id==6){
						$service_title = __('Other','mp');
					}else{
						$service_title = __('Commercial','mp');
					}
					  
					if($mp_booking->booking_status=='D'){
						$booking_status = __('Completed','mp');
					}else{
						$booking_status = __('Pending','mp');
					}	
					
					$booking_date = date_i18n('Y-m-d',strtotime($mp_booking->booking_date));
					$source_city = base64_decode($mp_booking->source_city);
					$destination_city = base64_decode($mp_booking->destination_city);
													
					if($mp_booking->service_id==1){
						$service_title = __('Home','mp');
					}elseif($mp_booking->service_id==2){
						$service_title = __('Office','mp');
					}elseif($mp_booking->service_id==3){
						$service_title = __('Vehicle','mp');
					}elseif($mp_booking->service_id==4){
						$service_title = __('Pets','mp');
					}elseif($mp_booking->service_id==6){
						$service_title = __('Other','mp');
					}else{
						$service_title = __('Commercial','mp');
					}								
					/* 
					if($mp_booking->service_id==1){
						$service_title = __('Home','mp');	
					}elseif($mp_booking->service_id==2){
						$service_title = __('Office','mp');
					}else{
						$service_title = __('Vehicle','mp');
					} */
					  
					if($mp_booking->booking_status=='D'){
						$bookingstatus = __('Completed','mp');
					}else{
						$bookingstatus = __('Pending','mp');
					}
					/* Get User Info */
					if($mp_booking->user_info!=''){
						$userinfo = unserialize($mp_booking->user_info);
						$pam_email = $userinfo['email'];
						$pam_name = $userinfo['name'];
						$pam_phone = $userinfo['phone'];
						$pam_custom_fields = unserialize($userinfo['custom_fields_details']);
					}else{
						$pam_email = '';	$pam_name = '';		$pam_phone = '';
						$pam_custom_fields = array();
					}

					/* Get Service Info */
					$service_info = '';
					if($mp_booking->service_info!=''){
						$service_info_arr = unserialize($mp_booking->service_info);
						if($mp_booking->service_id==1){			
							$service_info = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Loading Information","mp").'</h5>';
							if($service_info_arr['loading_bunglow_type']!=''){
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__("Home Type","mp").'</h5><span class="span-scroll span_indent">'.__("Bungalow","mp").'</span></li><li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__("Bungalow Type","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['loading_bunglow_type'].'</span></li>';
							$service_info .= '<li><h5 style="" class="mp-customer-details-hr">'. __("Bungalow No","mp").'</h5><span style="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
							}else{
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Home Type","mp").'</h5><span class="span-scroll span_indent">'. __("Appartment","mp").'</span></li>';
							
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Floor Number","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
							} 
							
							
							
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Unloading Info","mp").'</h5>';
							if($service_info_arr['unloading_bunglow_type']!=''){
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__("Home Type","mp").'</h5><span class="span-scroll span_indent">'.__("Bungalow","mp").'</span></li><li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Bungalow Type","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['unloading_bunglow_type'].'</span></li>';
							$service_info .= '<li><h5 style="" class="mp-customer-details-hr">'. __("Bungalow No","mp").'</h5><span style="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
							}else{
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Home Type","mp").'</h5><span class="span-scroll span_indent">'. __("Appartment","mp").'</span></li>';
							
							$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Floor Number","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['unloading_floor_number'].'</span></li>';
							} 
							
							
						}
										
					}
					
					/* Get Articles Info */
					$articles_info = '';
					if($mp_booking->service_id==6){
						if($mp_booking->articles_info!=''){
							$articles_info_arr = unserialize($mp_booking->articles_info);
							
							$articles_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Service Articles","mp").'</div>';
							$i=1;
							foreach($articles_info_arr['other_service_article'] as $rer){
							
								$articles_info_detail .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Article","mp").$i.'</h5><span class="span-scroll span_indent">'.$rer.'</span></li>';
								/* <b>Article'.$i.'</b>:'.$rer.'<br>'; */
								$i++;
								
							}	
							}
						}else{
							if($mp_booking->articles_info!=''){
								$articles_info_arr = unserialize($mp_booking->articles_info);
								
								foreach($articles_info_arr as $key => $value){
									$mp_service->addon_id = $key;
									$article_name = $mp_service->readOne_addon();
									$articles_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. $article_name[0]->addon_service_name.'</h5><span class="span-scroll span_indent">'.$value.'</span></li>';
								}
							}
						}
					/* Get Additional Info */
					$additional_info = '';
					if($mp_booking->additional_info!=''){
						
						$additional_info_arr = unserialize($mp_booking->additional_info);
						
						foreach($additional_info_arr as $addkey => $addvalue){			
							if($addvalue=='Y'){
								$additional_status = __('Yes','mp');
							}else{
								$additional_status = __('No','mp');
							}
							$moveto_additional_info->id = $addkey;
							$moveto_additional_info->readOne();
							$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$moveto_additional_info->additional_info.'</h5><span class="span-scroll span_indent">'.$additional_status.'</span></li>';
						}
					}
					
					
					/* Get Loading Info */
						$loading_info = '';
						if($mp_booking->loading_info!=''){										
							$loading_info_arr = unserialize($mp_booking->loading_info);				
							$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator"].'</span></li>';
							$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Loading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_no"].'</span></li>';
							$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price Of Loading Floor',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_val"].'</span></li>';
						}
						/* Get UnLoading Info */
						$unloading_info = '';
						if($mp_booking->unloading_info!=''){										
							$unloading_info_arr = unserialize($mp_booking->unloading_info);				
							$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator"].'</span></li>';
							$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unloading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_no"].'</span></li>';
							$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price of Unloading Floor ',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_val"].'</span></li>';

						}

					/* Get Booking Client Info */
					$user_info = '';
					if($mp_booking->user_info!=''){										
						$user_info_arr = unserialize($mp_booking->user_info);			
						$firstn = ucwords($user_info_arr['name']);	
													
						$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Name',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["name"].'</span></li>';
						$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Email',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["email"].'</span></li>';
						$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Phone',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["phone"].'</span></li>';
						/*$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Address',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["user_address"].'</span></li>';								
						$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["user_city"].'</span></li>';								
						$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["user_state"].'</span></li>';*/										
					}
									
					$custom_info = '';
					if(sizeof((array)$pam_custom_fields)>0){
						foreach($pam_custom_fields as $fieldkey => $fieldvalue){
							$custom_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$fieldkey.'</h5><span class="span-scroll span_indent">'.$fieldvalue.'</span></li>';
						}	
					}
					?>
					<tr>
						<td><?php echo $mp_booking->order_id; ?></td>
						<td><?php echo $source_city; ?></td>
						<td><?php echo $destination_city; ?></td>
						<td><?php echo date_i18n(get_option('date_format'),strtotime($booking_date)); ?></td>
						<td><?php echo $service_info; ?></td>
						<td><?php echo $articles_info; ?></td>
						<td><?php echo $additional_info; ?></td>
						<td><?php echo $loading_info; ?></td>
						<td><?php echo $unloading_info; ?></td>
						<td><?php echo $user_info; ?></td>
						
						<?php if($mp_booking->quote_price!=""){
												?>
												<td><?php echo get_option('moveto_currency_symbol').$mp_booking->quote_price; ?></td>
												<?php
											}else{?>
											<td><?php echo "-"; ?></td>
											<?php } ?>
						<td><?php echo $custom_info; ?></td>
					</tr>
	<?php 
}

/** Delete Guest User Info & Releated Data like-Bookings,payments.order client info **/
if (isset($_POST['general_ajax_action'],$_POST['delete_id']) && $_POST['delete_id'] != '' && $_POST['general_ajax_action']=='delete_guest_client') {
		$mp_booking->order_id =$_POST['delete_id'];   
		$mp_booking->delete_users_booking_by_order_id();
}

/** Get Export Filtered Bookings Detail **/
if(isset($_POST['general_ajax_action']) && $_POST['general_ajax_action']=='filtered_bookings'){
		
	$all_bookings = $mp_booking->readAll($_POST['booking_start'],$_POST['booking_end'],$_POST['booking_service'],'Export');
	foreach($all_bookings as $single_booking){ 
		if($single_booking->service_id==1){
			$service_title = __('Home','mp');
			}elseif($single_booking->service_id==2){
				$service_title = __('Office','mp');
			}elseif($single_booking->service_id==3){
				$service_title = __('Vehicle','mp');
			}elseif($single_booking->service_id==4){
				$service_title = __('Pets','mp');
			}elseif($single_booking->service_id==6){
				$service_title = __('Other','mp');
			}else{
				$service_title = __('Commercial','mp');
			}
			  
			if($single_booking->booking_status=='D'){
				$booking_status = __('Completed','mp');
			}else{
				$booking_status = __('Pending','mp');
			}	
			
			$booking_date = date_i18n('Y-m-d',strtotime($single_booking->booking_date));
			$source_city = base64_decode($single_booking->source_city);
			$destination_city = base64_decode($single_booking->destination_city);
											
			if($single_booking->service_id==1){
				$service_title = __('Home','mp');
			}elseif($single_booking->service_id==2){
				$service_title = __('Office','mp');
			}elseif($single_booking->service_id==3){
				$service_title = __('Vehicle','mp');
			}elseif($single_booking->service_id==4){
				$service_title = __('Pets','mp');
			}else{
				$service_title = __('Commercial','mp');
			}									
			
			/* if($single_booking->service_id==1){
				$service_title = __('Home','mp');	
			}elseif($single_booking->service_id==2){
				$service_title = __('Office','mp');
			}else{
				$service_title = __('Vehicle','mp');
			} */
			  
			if($single_booking->booking_status=='D'){
				$bookingstatus = __('Completed','mp');
			}else{
				$bookingstatus = __('Pending','mp');
			}
			/* Get User Info */
			if($single_booking->user_info!=''){
				$userinfo = unserialize($single_booking->user_info);
				$pam_email = $userinfo['email'];
				$pam_gender = $userinfo['user_gender'];
				$pam_first_name = $userinfo['first_name'];
				$pam_last_name = $userinfo['last_name'];
				$pam_phone = $userinfo['user_phone'];
				
				$pam_city = $userinfo['user_city'];
				$pam_state = $userinfo['user_state'];
				$pam_custom_fields = unserialize($userinfo['custom_fields_details']);
			}else{
				$pam_email = '';		$pam_gender = '';		$pam_first_name = '';		$pam_last_name = '';		$pam_phone = '';		$pam_address = '';		$pam_city = '';		$pam_state = '';
				$pam_custom_fields = array();
			}

			/* Get Service Info */
			$service_info = '';
			if($single_booking->service_info!=''){
				$service_info_arr = unserialize($single_booking->service_info);
				/*if($single_booking->service_id==1){			
					$service_info = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Loading Information","mp").'</h5>';
					if($service_info_arr['loading_bunglow_type']!=''){
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__("Home Type","mp").'</h5><span class="span-scroll span_indent">'.__("Bungalow","mp").'</span></li><li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__("Bungalow Type","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['loading_bunglow_type'].'</span></li>';
					$service_info .= '<li><h5 style="" class="mp-customer-details-hr">'. __("Bungalow No","mp").'</h5><span style="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
					}else{
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Home Type","mp").'</h5><span class="span-scroll span_indent">'. __("Appartment","mp").'</span></li>';
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Floor Number","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
					} 
				
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Unloading Information","mp").'</h5>';
					if($service_info_arr['unloading_bunglow_type']!=''){
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__("Home Type","mp").'</h5><span class="span-scroll span_indent">'.__("Bungalow","mp").'</span></li><li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Bungalow Type","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['unloading_bunglow_type'].'</span></li>';
					$service_info .= '<li><h5 style="" class="mp-customer-details-hr">'. __("Bungalow No","mp").'</h5><span style="span-scroll span_indent">'.$service_info_arr['unloading_floor_number'].'</span></li>';
					}else{
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Home Type","mp").'</h5><span class="span-scroll span_indent">'. __("Appartment","mp").'</span></li>';
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Floor Number","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['unloading_floor_number'].'</span></li>';
					} 
					
					
				}
				if($single_booking->service_id==2){
					$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Office Area","mp").'</h5><span class="span-scroll span_indent">'.$service_info_arr['office_area'].' sq.ft.</span></li>';
				}
				if($single_booking->service_id==3){
					if(isset($service_info_arr['vehicle_type'])){ 
					$vehical_array = array();
					$vehical_no_array = array();
						foreach($service_info_arr['vehicle_type'] as $ser_type){
							$vehical_array[] = $ser_type;
						}
						foreach($service_info_arr['no_of_vehicle'] as $no_of_veh){
							$vehical_no_array[] = $no_of_veh;
						}
						for($i=0;$i<sizeof($vehical_array);$i++){
							$service_info .= '<li><label>'. __("Car Type","mp").'</label><span class="span-scroll span_indent">'.$vehical_array[$i].' - '.$vehical_no_array[$i].'</span></li>';
						}
					}else{
							$service_info .= '<li><label>'. __("Vehicle","mp").'</label><span class="span-scroll span_indent">'. __("Bike","mp").'</span></li>';
								
							$service_info .= '<li><label>'. __("Vehicle Quantity","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['vehicle_qty'].'</span></li>';
					}
				}
				if($single_booking->service_id==4){	
					foreach($service_info_arr as $key => $value){
						if(isset($value['pets_age_radio'])){
								$agetype = $value['pets_age_radio'];
							}else{
								$agetype = "";
							}
							if(isset($value['pet_weight_gen'])){
								$wttype = $value['pet_weight_gen'];
							}else{
								$wttype = "";
							}
						$service_info .= '<h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$value['pets_service'].'</h5>';			
						$service_info .= '<li><label style="font-weight: 600;">'.__("Pet Name","mp").'</label><span class="span-scroll span_indent">'.$value['pet_name'].'</span></li>
						<li><label style="font-weight: 600;">'.__("Pet Breed","mp").'</label><span class="span-scroll span_indent">'.$value['pet_breed'].'</span></li>';
						
						$service_info .= '<li><label style="font-weight: 600;">'. __("Pet Age","mp").'</label><span style="span-scroll span_indent">' .$value['pet_age'].''.$agetype.' </span></li><li><label style="font-weight: 600;">'.__("Pet Weight","mp").'</label><span class="span-scroll span_indent">'.$value['pet_weight'].''.$wttype.' </span></li>';			
					}
				}
				if($single_booking->service_id==6){

				$service_info = '<li><label style="font-weight: 600;">'. __("service","mp").'</label><span style="span-scroll span_indent">' .$service_info_arr['service'].'</span></li><li><label style="font-weight: 600;">'.__("other service Description","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['other_service_description'].'</span></li><li><label style="font-weight: 600;">'.__("other service floor no","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['other_service_floor_no'].'</span></li>';

				}				*/
			}
			
			/* Get Articles Info */
			$articles_info = '';
			if($single_booking->service_id==6){
				if($single_booking->articles_info!=''){
					$articles_info_arr = unserialize($single_booking->articles_info);
					
					$articles_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Service Articles","mp").'</div>';
					$i=1;
					foreach($articles_info_arr['other_service_article'] as $rer){
					
						$articles_info_detail .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Article","mp").$i.'</h5><span class="span-scroll span_indent">'.$rer.'</span></li>';
						/* <b>Article'.$i.'</b>:'.$rer.'<br>'; */
						$i++;
						
					}	
					}
				}else{
				if($single_booking->articles_info!=''){
					$articles_info_arr = unserialize($single_booking->articles_info);
					
					foreach($articles_info_arr as $key => $value){
						$mp_service->addon_id = $key;
						$article_name = $mp_service->readOne_addon();
						$articles_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. $article_name[0]->addon_service_name.'</h5><span class="span-scroll span_indent">'.$value.'</span></li>';
						}
					}
			}
						
			/* Get Additional Info */
			$additional_info = '';
			if($single_booking->additional_info!=''){
				
				$additional_info_arr = unserialize($single_booking->additional_info);
				
				foreach($additional_info_arr as $addkey => $addvalue){			
					if($addvalue=='Y'){
						$additional_status = __('Yes','mp');
					}else{
						$additional_status = __('No','mp');
					}
					$moveto_additional_info->id = $addkey;
					$moveto_additional_info->readOne();
					$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$moveto_additional_info->additional_info.'</h5><span class="span-scroll span_indent">'.$additional_status.'</span></li>';
				}
			}
			
			/* Get Loading Info */
			$loading_info = '';
			if($single_booking->loading_info!=''){										
				$loading_info_arr = unserialize($single_booking->loading_info);				
				$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator"].'</span></li>';
				$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Loading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_no"].'</span></li>';
				$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price Of Loading Floor',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_val"].'</span></li>';
					
			}
			/* Get UnLoading Info */
			$unloading_info = '';
			if($single_booking->unloading_info!=''){										
				$unloading_info_arr = unserialize($single_booking->unloading_info);				
				$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator"].'</span></li>';
				$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unloading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_no"].'</span></li>';
				$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price of Unloading Floor ',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_val"].'</span></li>';
				/* $unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Country',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["pam_unloading_country"].'</span></li>';										 */
			}
			
			/* Get Booking Client Info */
			$user_info = '';
			
			if($single_booking->user_info!=''){										
				$user_info_arr = unserialize($single_booking->user_info);		
				$firstn = ucwords($user_info_arr['name']);								
				$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Name',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["name"].'</span></li>';
				$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Email',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["email"].'</span></li>';
				$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Phone',"mp").'</h5><span class="span-scroll span_indent">'.$user_info_arr["phone"].'</span></li>';
												
			}
			
			
			/* $custom_info = '';
			if(sizeof($pam_custom_fields)>0){
				foreach($pam_custom_fields as $fieldkey => $fieldvalue){
					$custom_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$fieldkey.'</h5><span class="span-scroll span_indent">'.$fieldvalue.'</span></li>';
					
				}
				
			}
			 */
			?>
			<tr>
				<td><?php echo $single_booking->order_id; ?></td>
				<td><?php echo $service_title; ?></td>
				<td><?php echo $source_city; ?></td>
				<td><?php echo $destination_city; ?></td>
				<td><?php echo date_i18n(get_option('date_format'),strtotime($booking_date)); ?></td>
				<td><?php echo $service_info; ?></td>
				<td><?php echo $articles_info; ?></td>
				<td><?php echo $additional_info; ?></td>
				<td><?php echo $loading_info; ?></td>
				<td><?php echo $unloading_info; ?></td>
				<td><?php echo $user_info; ?></td>
			
				<td><?php echo $custom_info; ?></td>
			</tr>
	<?php } 
}	

/* Get Calender Upcomming Appointments */
/*if(isset($_GET['general_ajax_action']) && $_GET['general_ajax_action']=='get_upcoming_appointments'){
	
		$mp_booking->location_id = $_SESSION['mp_location'];
		
		$start_date= '';
		$end_date = '';
		$order_id = '';
		if(isset($_SESSION['mp_booking_filtersd'],$_SESSION['mp_booking_filtered']) && $_SESSION['mp_booking_filtersd']!='' && $_SESSION['mp_booking_filtered']!=''){
			$start_date = $_SESSION['mp_booking_filtersd'];
			$end_date = $_SESSION['mp_booking_filtered'];
		}
		if(isset($_SESSION['mp_booking_filterservice']) && $_SESSION['mp_booking_filterservice']!=''){
			$service_id = $_SESSION['mp_booking_filterservice'];
		}
		
		if(($start_date!='' && $end_date!='') || $service_id!='' || $provider_id!=''){
			$all_upcoming_appointments = $mp_booking->readAll($start_date,$end_date,$order_id);	
		}else{
			$all_upcoming_appointments = $mp_booking->read_all_upcoming_bookings();		
		}
		$appointment_array_for_cal = array();
		foreach( $all_upcoming_appointments as $app) {
			
		$appointment_id  = $app->id;		*/
		
	/*	if($app->order_id==1){
			$service_title ='<span class="glyphicon glyphicon-home"></span>'.' '.__('Home','mp');
			$color_tag = '#008000';
		}elseif($app->service_id==2){
			$service_title = '<span class="fa fa-university"></span>'.' '.__('Office','mp');
			$color_tag = '#335bff';
		}elseif($app->service_id==3){
			$service_title = '<span class="fa fa-university"></span>'.' '.__('Vehicle','mp');
			$color_tag = '#335bff';
		}elseif($app->service_id==4){
			$service_title = '<span class="fa fa-university"></span>'.' '.__('Pets','mp');
			$color_tag = '#335bff';
		}elseif($app->service_id==6){
			$service_title = '<span class="fa fa-university"></span>'.' '.__('Other','mp');
			$color_tag = '#335bff';
		}else{
			$service_title = '<span class="fa fa-car"></span>'.' '.__('Commercial','mp');
			$color_tag = '#ff3377';
		}*/
		  
		/*if($app->booking_status=='D'){
			$booking_status = __('Completed','mp');
		}else{
			$booking_status = __('Pending','mp');
		}		
		$service_start_time = date_i18n('Y-m-d',strtotime($app->booking_date)).' 00:00:00';
		$service_end_time = date_i18n('Y-m-d',strtotime($app->booking_date)).' 00:30:00';
		
		/* Get User Info */
	/*	if($app->user_info!=''){
			$userinfo = unserialize($app->user_info);
			$customer_name = $userinfo['name'];
			$customer_phone = $userinfo['phone'];
			$customer_email = $userinfo['email'];
		}else{
			$customer_name = '';
			$customer_phone = '';
			$customer_email = '';
		}

		$appointment_array_for_cal[]= array(
						"id"=>"$appointment_id",
						"color_tag"=>"$color_tag",
						"title"=>"$service_title",
						"start"=>"$service_start_time",
						"end"=>"$service_end_time",
						"event_status"=>"$booking_status",
						"client_name"=>"$customer_name",
						"client_phone"=>"$customer_phone",
						"client_email"=>"$customer_email"
						
						);
   }   
   
 $json_encoded_string_for_cal  =  json_encode($appointment_array_for_cal);

 
echo $json_encoded_string_for_cal;die();
}*/

/* Get Calender Upcomming Appointments */
if(isset($_GET['general_ajax_action']) && $_GET['general_ajax_action']=='get_upcoming_appointments'){
	
		$mp_booking->location_id = $_SESSION['mp_location'];
		
		$start_date= '';
		$end_date = '';
		$service_id = '';
		$provider_id = ''; 
		if(isset($_SESSION['mp_booking_filtersd'],$_SESSION['mp_booking_filtered']) && $_SESSION['mp_booking_filtersd']!='' && $_SESSION['mp_booking_filtered']!=''){
			$start_date = $_SESSION['mp_booking_filtersd'];
			$end_date = $_SESSION['mp_booking_filtered'];
		}
		if(isset($_SESSION['mp_booking_filterservice']) && $_SESSION['mp_booking_filterservice']!=''){
			$service_id = $_SESSION['mp_booking_filterservice'];
		}
		
		if(($start_date!='' && $end_date!='') || $service_id!='' || $provider_id!=''){
			$all_upcoming_appointments = $mp_booking->readAll($start_date,$end_date,$service_id,$provider_id);	
		}else{
			$all_upcoming_appointments = $mp_booking->read_all_upcoming_bookings();		
		}
		$appointment_array_for_cal = array();
		foreach( $all_upcoming_appointments as $app) {
			
		$appointment_id  = $app->id;		
		
		
		$service_title =$app->move_size;
		
		$articles_info_arr = unserialize($all_upcoming_appointments[0]->service_info);
		if($app->booking_status=='D'){
			$booking_status = __('Completed','mp');
		}else{
			$booking_status = __('Pending','mp');
		}		
		$service_start_time = date_i18n('Y-m-d',strtotime($app->booking_date)).' 00:00:00';
		$service_end_time = date_i18n('Y-m-d',strtotime($app->booking_date)).' 00:30:00';
		
		/* Get User Info */
		if($app->user_info!=''){
			$userinfo = unserialize($app->user_info);
			$customer_name = $userinfo['name'];
			$customer_phone = $userinfo['phone'];
			$customer_email = $userinfo['email'];
		}else{
			$customer_name = '';
			$customer_phone = '';
			$customer_email = '';
		}

		
		
		$appointment_array_for_cal[]= array(
						"id"=>"$appointment_id",
						"color_tag"=>"$color_tag",
						"title"=>"$service_title",
						"start"=>"$service_start_time",
						"end"=>"$service_end_time",
						"event_status"=>"$booking_status",
						"client_name"=>"$customer_name",
						"client_phone"=>"$customer_phone",
						"client_email"=>"$customer_email"
						
						);
   }   
   
 $json_encoded_string_for_cal  =  json_encode($appointment_array_for_cal);

 
echo $json_encoded_string_for_cal;die();
}

/** Get Single Booking Detail **/
if(isset($_POST['general_ajax_action'],$_POST['appointment_id']) && $_POST['general_ajax_action']=='get_appointment_detail' && $_POST['appointment_id']!=''){
	
	$mp_booking->id = $_POST['appointment_id']; 
  $mp_booking->readOne_by_booking_id();
	$mp_booking->order_id = $mp_booking->order_id; 

	if($mp_booking->booking_status=='D'){
		$bookingstatus = __('Completed','mp');
	}else{
		$bookingstatus = __('Pending','mp');
	}
	if($mp_booking->quote_price!=''){
		$quote_price = get_option('moveto_currency_symbol').$mp_booking->quote_price;
	}else{
		$quote_price = "";
	}
	/* Get User Info */
	if($mp_booking->user_info!=''){

		$userinfo = unserialize($mp_booking->user_info);
		$pam_email = $userinfo['email'];
		$pam_name = ucfirst($userinfo['name']);
		$pam_phone = $userinfo['phone'];
		
	}else{
		$pam_email = ''; $pam_name = ''; $pam_phone = '';
		
	}
	$payment_method = $mp_booking->payment_method;
/* Get Service Info */

	$service_info = '';
	$service_info = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Moving Service Information","mp").'</h5>';
										if($mp_booking->service_info!='' || $mp_booking->cubic_feets_article!=''){
											$movesize_info = $mp_booking->move_size;
											$service_info .='<li><label>'. __("Move Size","mp").'</label><span class="mp_service  span_indent">'.$movesize_info.'</span></li><br>';
											
											$service_info_arr = unserialize($mp_booking->service_info);
									
											/* $article_name = $service_info_arr['get_article_name']; */


											foreach ($service_info_arr as $key => $value) {
												if(count((array)$value) > 0){
													if(isset($value['get_article_room_id'])) {
													 $room_id=$value['get_article_room_id'];
													 }
													 if(isset($value['get_article_id'])) {
													 $article_id=$value['get_article_id'];
													 }
													 if(isset($value['get_article_price'])) {
													 $article_price=$value['get_article_price'];
													 }
											 		$moveto_article_category_obj->id=$room_id;
													$room_type_name_array= $moveto_article_category_obj->readOne_room_type();
													if(!empty($room_type_name_array))
													{
													 $room_type_name= $room_type_name_array[0]->name; 
													
													 $service_info .='<li><label>'. __("Room Type","mp").'</label><span class="mp_service  span_indent">'.$room_type_name.'</span></li>'; 
													}
													if(isset($value['get_article_name'])) {
													$service_info .='<li><label>'. __("Article Name","mp").'</label><span class="mp_service  span_indent">'.$value['get_article_name'].'</span></li>';
													}
													if(isset($value['count'])) {
													$service_info .='<li><label>'. __("Article Qty","mp").'</label><span class="mp_service  span_indent">'.$value['count'].'</span></li>';
													}
													/*if(isset($value['get_article_price'])) {
													$service_info .='<li><label>'. __("Article Base Price","mp").'</label><span class="mp_service  span_indent">'.$value['get_article_price'].'</span></li>';
													}*/
													/*if(isset($value['tot_article_price'])) {
													$service_info .='<li><label>'. __("Article Total Price","mp").'</label><span class="mp_service  span_indent">'.$value['tot_article_price'].'</span></li>';
													}*/
													$service_info .='<br>';
												
 
													
				}
				
			}
			$service_info_cubic = unserialize($mp_booking->cubic_feets_article);
			if(!empty($service_info_cubic)){
			foreach ($service_info_cubic as $key => $value) {
												if(count((array)$value) > 0){
													if(isset($value['cubicfeet_title'])) {
													$service_info .='<li><label>'. __("Article Name","mp").'</label><span class="mp_service  span_indent">'.$value['cubicfeet_title'].'</span></li>';
													$service_info .='<li><label>'. __("Quanty","mp").'</label><span class="mp_service  span_indent">1</span></li>';
													$service_info .='<br>';
													}
				}
				
			}
			}

				}else{
					$service_info = '<h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Moving Service Information","mp").'</h5><li>No Information Available</li>';
				}

		/*$service_info_cubic = unserialize($mp_booking->cubic_feets_article);
		foreach ($service_info_arr as $key => $value) {

		}*/

	
	
	/* Get Additional Info  Admin PopUp Open On View Details Of Booking*/
	$additional_info = '';
	
	if($mp_booking->additional_info!='')
	{
	
		$additional_info_arr = unserialize($mp_booking->additional_info);
		if(sizeof((array)$additional_info_arr['favorite'])>0){
			$additional_info = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Additional Information","mp").'</h5>';
					$returndata = $moveto_additional_info->readAll();

					$additionalArr = array();
					foreach ($additional_info_arr['favorite'] as $value) {
					array_push($additionalArr, $value);
					}
						foreach($returndata as $additional){

							if(in_array($additional->id, $additionalArr)){
							$additional_info .='<li><label>'.$additional->additional_info.'</label><span class="span-scroll span_indent">Yes</span></li>';
							
							}else
							{

								$additional_info .='<li><label>'.$additional->additional_info.'</label><span class="span-scroll span_indent">No</span></li>';
							
							}

						}

				}	
			
	}
	
	/* Get Loading Info */
	$loading_info = '';
	if($mp_booking->loading_info!=''){										
		$loading_info_arr = unserialize($mp_booking->loading_info);		
		$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Loading Details","mp").'</h5>';
		
		$loading_info .='<li><label>'.__('Loading Floor no',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_number"].'</span></li>';
		$loading_info .='<li><label>'.__('Price Of Loading Floor',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_amt"].'</span></li>';
		$loading_info .='<li><label>'.__('Load Elevator ?',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_checked"].'</span></li>';
		$loading_info .='<li><label>'.__('Load Elevator Price',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_amt"].'</span></li>';
		$loading_info .='<li><label>'.__('Load Packaging  ?',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["packaging_checked"].'</span></li>';
		$loading_info .='<li><label>'.__('Load Packaging Price ?',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["packaging_amt"].'</span></li>';
		$loading_info .='<li><label>'.__('Street Address',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_one"].'</span></li>';
		$loading_info .='<li><label>'.__('City',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_two"].'</span></li>';
		$loading_info .='<li><label>'.__('State',"mp").'</label><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_three"].'</span></li>';
		
	}

	/* Get UnLoading Info */
	$unloading_info = '';
	if($mp_booking->unloading_info!=''){										
		$unloading_info_arr = unserialize($mp_booking->unloading_info);				
		$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Unloading Details","mp").'</h5>';
	$unloading_info .='<li><label>'.__('Unloading Floor no',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_number"].'</span></li>';
											
	$unloading_info .='<li><label>'.__('Price of Unloading Floor ',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_amt"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('Unload Elevator ?',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_checked"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('Unload Elevator Price',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_amt"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('Unload Unpackaging ?',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_checked"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('Unload Unpackaging Price',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_amt"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('Street Address',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_one"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('City',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_two"].'</span></li>';
	
	$unloading_info .='<li><label>'.__('State',"mp").'</label><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_three"].'</span></li>';
		
	}
	
	
	/* $custom_info = '';
	if(sizeof($pam_custom_fields)>0){
		foreach($pam_custom_fields as $fieldkey => $fieldvalue){
			$custom_info .='<li><label>'.$fieldkey.'</label><span class="client_notes span-scroll span_indent">'.$fieldvalue.'</span></li>';
		}		
	} */

	$appointment_detail = array();     
	$appointment_detail['id'] = $mp_booking->id;
/* 	$appointment_detail['service_title'] = $service_title; */
	$appointment_detail['source_city'] = base64_decode($mp_booking->source_city);   
	$appointment_detail['via_city'] = base64_decode($mp_booking->via_city);   
	$appointment_detail['destination_city'] = base64_decode($mp_booking->destination_city); 
	$appointment_detail['booking_date'] = date_i18n(get_option('date_format'),strtotime($mp_booking->booking_date));
   
	$appointment_detail['booking_status']=$bookingstatus;

	
	$appointment_detail['service_info']=$service_info;
/* 	$appointment_detail['articles_info']=$articles_info; */
	$appointment_detail['additional_info']=$additional_info;
	$appointment_detail['loading_info']=$loading_info;
	$appointment_detail['unloading_info']=$unloading_info;
  
	$appointment_detail['client_name']=$pam_name;
	$appointment_detail['client_phone']= $pam_phone;
	$appointment_detail['client_email']= $pam_email;
	$appointment_detail['quote_price']=$quote_price;
	$appointment_detail['payment_method']=ucwords(str_replace("_", " ", strtolower($payment_method)));
	
    echo json_encode($appointment_detail);die();
}

/* Filter Appointments On Appointup Page */
if(isset($_POST['general_ajax_action']) && $_POST['general_ajax_action']=='filter_appointments'){
	if($_POST['startdate']!=''){$_SESSION['mp_booking_filtersd'] = date("Y-m-d",strtotime($_POST['startdate']));}else{ if(isset($_SESSION['mp_booking_filtersd'])){ unset($_SESSION['mp_booking_filtersd']); }}
	if($_POST['enddate']!=''){$_SESSION['mp_booking_filtered'] = date("Y-m-d",strtotime($_POST['enddate']));}else{ if(isset($_SESSION['mp_booking_filtered'])){ unset($_SESSION['mp_booking_filtered']); }}	
}
/* Delete Appointment,Order Payment,Order Client Info */
if(isset($_POST['general_ajax_action'],$_POST['booking_id']) && $_POST['general_ajax_action']=='delete_appointment' && $_POST['booking_id']!=''){
	$mp_booking->id = $_POST['booking_id'];
	$mp_booking->delete_booking();
	
}

/** Get Service Chart Analytics Info **/
if(isset($_POST['general_ajax_action']) && $_POST['general_ajax_action']=='view_chart_analytics'){	
	global $current_user;
	$current_user = wp_get_current_user();
	$info = get_userdata( $current_user->ID );
	/* Coupon Detail */
	$coupons->location_id = $_SESSION['mp_location'];
	$couponsinfo = $coupons->readAll();
	if(isset($info->caps['administrator'])){
	/* Service Detail */
	$service->location_id= $_SESSION['mp_location'];
	$servicesInfo =  $service->readAll();
	/* Staff Detail */
	$staff->location_id =$_SESSION['mp_location'];
	$staffsinfo = $staff->readAll_with_disables();   
	
	}else{
	/* Service Detail */
	$service->provider_id=  $current_user->ID;
	$servicesInfo =  $service->readall_services_of_provider();
	/* Staff Detail */
	$staff->id =  $current_user->ID;
	$staffsinfo = $staff->readOne();   
	}
	
	$chart_data_array = array();
	if(isset($_POST['method']) && $_POST['method']=='service'){
		foreach($servicesInfo as $serviceInfo){
			$mp_booking->service_id = $serviceInfo->id;
			$totalbookings = $mp_booking->readall_bookings_by_service_id();
			if($totalbookings>0){
				$chart_data_array[]=array(
						"value"=>$totalbookings,
						"color"=>"$serviceInfo->color_tag",
						"label"=>"$serviceInfo->service_title"
				);
			}
		}
	}
	if(isset($_POST['method']) && $_POST['method']=='provider'){
		foreach($staffsinfo as $staff_info){
		$mp_booking->provider_id = $staff_info['id'];
		$totalbookings = $mp_booking->readall_bookings_by_provider_id();
			if($totalbookings>0){
				$chart_data_array[]=array(
						"value"=>$totalbookings,
						"color"=>"#".mt_rand(100000, 999999),
						"label"=>ucfirst($staff_info['staff_name'])
					);
			}
		}
	}
	if(isset($_POST['method']) && $_POST['method']=='coupon'){
		foreach($couponsinfo as $couponinfo){
			if($couponinfo->coupon_used>0){
				$chart_data_array[]=array(
					"value"=>"$couponinfo->coupon_used",
					"color"=>"#".mt_rand(100000, 999999),
					"label"=>"$couponinfo->coupon_code"
				);
			}	
		}	
	}
	$json_chart_data =  json_encode($chart_data_array);	
		echo $json_chart_data;die();
}
/** Get Notification Count **/
if(isset($_POST['general_ajax_action']) && $_POST['general_ajax_action']=='get_notification_count'){
		if(isset($_SESSION['mp_location'])){
			if($mp_booking->get_notifications_count()>0){
				echo '<span id="mp-notification-top" class="get_notification_rem">'.$mp_booking->get_notifications_count().'</span>';
			}
		}
}	

/** Get Notification Bookings **/
if(isset($_POST['general_ajax_action']) && $_POST['general_ajax_action']=='get_notification_bookings'){
		$notificationbookings = $mp_booking->get_notifications_bookings();
		function time_elapsed_string($datetime, $full = false) {
			$now = new DateTime;
			$ago = new DateTime($datetime);
			$diff = $now->diff($ago);

			$diff->w = floor($diff->d / 7);
			$diff->d -= $diff->w * 7;

			$string = array(
				'y' => 'year',
				'm' => 'month',
				'w' => 'week',
				'd' => 'day',
				'h' => 'hour',
				'i' => 'minute',
				's' => 'second',
			);
			foreach ($string as $k => &$v) {
				if ($diff->$k) {
					$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
				} else {
					unset($string[$k]);
				}
			}

			if (!$full) $string = array_slice($string, 0, 1);
			return $string ? implode(', ', $string) . ' ago' : 'just now';
		}
		if(sizeof((array)$notificationbookings)>0){			
			foreach($notificationbookings as $notificationbooking){								
					
					if($notificationbooking->service_id==1){
						$service_title = __('Home','mp');	
					}elseif($notificationbooking->service_id==2){
						$service_title = __('Office','mp');
					}elseif($notificationbooking->service_id==3){
						$service_title = __('Vehicle','mp');
					}elseif($notificationbooking->service_id==4){
						$service_title = __('Pets','mp');
					}elseif($notificationbooking->service_id==6){
						$service_title = __('Other','mp');
					}else{
						$service_title = __('Commercial','mp');
					}
					
					if($notificationbooking->user_info!=''){
						$userinfo = unserialize($notificationbooking->user_info);
						$clientname = $userinfo['name'];
					}else{
						$clientname = '';
					}
					
					
					 ?>					
					<li class="mp-today-list" id="mp_notification<?php echo $notificationbooking->id;?>" data-bookingid = '<?php echo $notificationbooking->id;?>' data-toggle="modal" data-target="#booking-details">
								<div class="list-inner">
						<span class="booking-text">
								<?php if($notificationbooking->booking_status=='P' || $notificationbooking->booking_status==''){
									echo '<span class="mp-label btn-info br-2">'.__('Pending','mp').'</span>';
								}else{
									echo '<span class="mp-label btn-success br-2">'.__('Completed','mp').'</span>';
								} ?><span class="mp-noti-text">  <?php echo $clientname;?> <?php echo __('for a','mp');?> 
						<?php echo __(stripslashes_deep($service_title),"mp");?> <?php echo __('on','mp');?> <?php echo date_i18n('d-M-Y',strtotime($notificationbooking->booking_date)); ?> </span></span>
						<span class="booking-time">
						<?php echo time_elapsed_string($notificationbooking->lastmodify); //echo $timeago;?> </span><a data-booking_id="<?php echo $notificationbooking->id;?>" class="pull-right mp-mark-read mp_unread_notification" href="javascript:void(0);"><?php echo __("mark as read","mp");?></a> 
						</div>
					</li>					
				<?php									
			}			
		}else{ ?>
			<div class="list-inner mp-no-notification">
				<i class="fa fa-clock-o fa-3x"></i>
				<div class="booking-text">
					<h4><?php echo __('No notification found','mp');?></h4>
				</div>
			
			</div>
			<?php 		
		}
}	
/** Mark Notification As Readed **/
if(isset($_POST['general_ajax_action'],$_POST['booking_id']) && $_POST['general_ajax_action']=='remove_notifications_bookings' && $_POST['booking_id']!=''){
		$mp_booking->id = $_POST['booking_id'];
		$mp_booking->remove_notifications_bookings();
}
/** Download Client Invoice--From Client Dashboard **/
if(isset($_GET['general_ajax_action'],$_GET['order_id'],$_GET['client_id'],$_GET['key']) && $_GET['general_ajax_action']=='client_download_invoice' && $_GET['order_id']!='' && $_GET['client_id']!='' && $_GET['key']!=''){
	
	$keystring =  substr($_GET['key'], 1, -1);
	$decodedkey = base64_decode($keystring);
	$validatekey = $decodedkey-1247;
	if($validatekey!=$_GET['order_id']){
		echo __("Invalid key supplied.","mp"); die();
	}
	
	include(dirname(dirname(dirname(__FILE__))).'/assets/pdf/tfpdf/tfpdf.php');
	
	$mp_booking->order_id = $_GET['order_id'];
	$mp_booking->client_id = $_GET['client_id'];
	$clientorderbookings = $mp_booking->get_client_bookings_by_order_id();
	
		
					
	$invoice_number = strtoupper(date_i18n('M',strtotime($clientorderbookings[0]->lastmodify))).'-'.$_GET['client_id'];
	$invoice_date = date_i18n(get_option('moveto_datepicker_format'),strtotime($clientorderbookings[0]->lastmodify));	
	
	/*Client info*/
	$order_info->order_id = $_GET['order_id'];
	$order_info->readOne_by_order_id();
	$client_personal_info=unserialize($order_info->client_personal_info);

	/*Payment Info */
	$payments->order_id = $_GET['order_id'];
	$payments->read_one_by_order_id();	
	$payments->payment_method;
	if($payments->payment_method == 'paypal') { $pay_type = __('Paypal','mp'); }
	elseif($payments->payment_method == 'pay_locally') { $pay_type = __('Pay Locally','mp'); }
	elseif($payments->payment_method == 'Free') {  $pay_type = __('Free','mp');}
	elseif($payments->payment_method == 'stripe'){ $pay_type = __('Stripe','mp'); }
	elseif($payments->payment_method =='authorizenet'){$pay_type = __('Authorize .Net','mp');}
	else{$pay_type = '-';} 
	$net_amount = $general->mp_price_format_for_pdf($payments->net_total);
	$discount = $general->mp_price_format_for_pdf($payments->discount);
	$taxes = $general->mp_price_format_for_pdf($payments->taxes);
	$partial = $general->mp_price_format_for_pdf($payments->partial);
	
	
	$backgroundimage=$plugin_url_for_ajax."/assets/images/client_inv.jpg";
	if(get_option('moveto_company_logo')==''){
		$companylogo=$plugin_url_for_ajax."/assets/images/company.png";
	}else{
		$companylogo=site_url()."/wp-content/uploads".get_option('moveto_company_logo');	
	}
	
	$pdf = new tFPDF();
	$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
	$pdf->SetFont('DejaVu','',14);
	$pdf->SetMargins(0,0);
	$pdf->SetTopMargin(0);
	$pdf->SetAutoPageBreak(true,0);
	$pdf->AddPage();
	$pdf->SetFillColor(242,242,242);
    $pdf->SetTextColor(102,103,102);
    $pdf->SetDrawColor(128,255,0);
    $pdf->SetLineWidth(0);
   
	$pdf->Cell(210,297,'',0,1,'C',true);
	$pdf->Image($backgroundimage,0,0,210);
	
	$pdf->Image($companylogo,20,15,20); 
	$pdf->SetFont('DejaVu','',9);
	$pdf->Text(130,12,get_option('moveto_company_name'));
	$pdf->Text(130,17,get_option('moveto_company_address'));
	$pdf->Text(130,22,get_option('moveto_company_city').",".get_option('moveto_company_state'));
	$pdf->Text(130,27,get_option('moveto_company_country'));
	$pdf->Text(130,30,get_option('moveto_company_phone'));
	$pdf->Text(130,33,get_option('moveto_company_email'));
	
	$pdf->SetFont('DejaVu','',15);
	$pdf->Text(21,56,__("INVOICE TO:","mp"));
	
	$pdf->SetFont('DejaVu','',12);
	$pdf->Text(21,63,ucwords($order_info->client_name));
	
	$pdf->SetFont('DejaVu','',8);
	if(isset($client_personal_info['address'])){
	$pdf->Text(21,68,$client_personal_info['address']);
	}
	if(isset($client_personal_info['city'],$client_personal_info['state'])){
	$pdf->Text(21,72,$client_personal_info['city'].",".$client_personal_info['state']);
	}
	if(isset($client_personal_info['country'])){
	$pdf->Text(21,76,$client_personal_info['country']);	
	}
	$pdf->Text(30,82,$order_info->client_phone);
	$pdf->Text(30,88,$order_info->client_email);
	
	$pdf->SetFont('DejaVu','',28);
	$pdf->SetTextColor(0,0,0);
	$pdf->Text(95,62,__("INVOICE #","mp").strtoupper(date_i18n('M',strtotime($clientorderbookings[0]->lastmodify)))."-".sprintf("%04d",$_GET['order_id']));


	$pdf->SetFont('DejaVu','',7);
	$pdf->SetTextColor(102,103,102);
	$pdf->Text(109 - $pdf->GetStringWidth(__("Invoice Date","mp"))/2,77,__("Invoice Date","mp"));
	$pdf->Text(182 - $pdf->GetStringWidth(__("Payment Method","mp"))/2,77,__("Payment Method","mp"));
	
	$pdf->SetFont('DejaVu','',10);
   
	$pdf->Text(109 - $pdf->GetStringWidth(date_i18n(get_option('moveto_datepicker_format'),strtotime($clientorderbookings[0]->lastmodify)))/2,82,date_i18n(get_option('moveto_datepicker_format'),strtotime($clientorderbookings[0]->lastmodify)));
	$pdf->Text(181 - $pdf->GetStringWidth(strtoupper($pay_type))/2,82,strtoupper($pay_type));
	$pdf->SetFont('DejaVu','',13.5);
	$pdf->Text(20,107,__("Description","mp"));
	$pdf->Text(60,107,__("Duration","mp"));
	$pdf->Text(90,107,__("Provider","mp"));
	$pdf->Text(120,107,__("Date","mp"));
	$pdf->Text(150,107,__("Time","mp"));
	$pdf->Text(180,107,__("Price","mp"));
	
	$pdf->SetFont('DejaVu','',8);
	
	$addondetails_startpoint = 120;
	foreach($clientorderbookings as $clientorderbooking){								
		$service->id= $clientorderbooking->service_id;
		$service->readone();	
		$servicedurationstrinng = '';
		if(floor($service->duration/60)!=0){ $servicedurationstrinng .= floor($service->duration/60); $servicedurationstrinng .= __(" Hrs","mp"); } 
		if($service->duration%60 !=0){  $servicedurationstrinng .= $service->duration%60; $servicedurationstrinng .= __(" Mins","mp"); }
		$staff->id=$clientorderbooking->provider_id;
		$staff_info = $staff->readOne();   
	  
	$pdf->Text(20,$addondetails_startpoint,$service->service_title);
	 $pdf->Text(60,$addondetails_startpoint,$servicedurationstrinng);
	$pdf->Text(90,$addondetails_startpoint,ucfirst($staff_info[0]['staff_name']));
	$pdf->Text(120,$addondetails_startpoint,date_i18n('M d,Y',strtotime($clientorderbooking->booking_datetime)));
	$pdf->Text(150,$addondetails_startpoint,date_i18n(get_option('time_format'),strtotime($clientorderbooking->booking_datetime)).'-'.date_i18n(get_option('time_format'),strtotime($clientorderbooking->booking_endtime)));
		
	/* $pdf->Text(180,$addondetails_startpoint,iconv("UTF-8", "windows-1252",get_option('moveto_currency_symbol')).$clientorderbooking->booking_price); */
	$pdf->Text(180,$addondetails_startpoint,iconv("UTF-8", "windows-1252",$general->mp_price_format_for_pdf($clientorderbooking->booking_price)));
	$addondetails_startpoint=$addondetails_startpoint+5;
	}
	
	
	$pdf->SetFont('DejaVu','',8);
	$pdf->Text(145,170,__("Total","mp"));
	$pdf->Text(145,175,__("Tax","mp"));
	$pdf->Text(145,180,__("Discount","mp"));	   
	   
	$pdf->SetFont('DejaVu','',8);
	/* $pdf->Text(180,170,iconv("UTF-8", "windows-1252",get_option('moveto_currency_symbol')).$payments->amount);
	$pdf->Text(180,175,iconv("UTF-8", "windows-1252",get_option('moveto_currency_symbol')).$payments->taxes);
	$pdf->Text(180,180,iconv("UTF-8", "windows-1252",get_option('moveto_currency_symbol')).$payments->discount); */
	$pdf->Text(180,170,iconv("UTF-8", "windows-1252",$general->mp_price_format_for_pdf($payments->amount)));
	$pdf->Text(180,175,iconv("UTF-8", "windows-1252",$general->mp_price_format_for_pdf($payments->taxes)));
	$pdf->Text(180,180,iconv("UTF-8", "windows-1252",$general->mp_price_format_for_pdf($payments->discount)));
	/* $pdf->Text(170,185,$payment_net_amount); */
	
	$pdf->SetFont('DejaVu','',15);
	$pdf->SetTextColor(0,0,0);
	$pdf->Text(140,193,__("TOTAL:","mp"));
	$pdf->Text(175,193,iconv("UTF-8", "windows-1252",$general->mp_price_format_for_pdf($payments->net_total)));

	$pdf->SetFont('DejaVu','',12);
	$pdf->SetTextColor(102,103,102);
	
	$pdf->SetFont('DejaVu','',14);
	$pdf->Text(23,195,__("THANK YOU FOR YOUR BUSINESS!","mp"));

	$pdf->SetFont('DejaVu','',14);
	$pdf->Text(145,210,__("For ","mp").get_option('moveto_company_name'));
	
	$pdf->SetFont('DejaVu','',8);
	$pdf->Text(153,225,__("Company Director","mp"));

	$pdf->Output("#".$invoice_number.".pdf","D");
	
}





/* moveto Publish/Hide/Delete Review */
if(isset($_POST['general_ajax_action'],$_POST['method'],$_POST['review_id']) && $_POST['general_ajax_action']=='publish_hide_delete_review' && $_POST['method']!='' && $_POST['review_id']!=''){
	if($_POST['method']=='delete'){		
		$reviews->id = $_POST['review_id'];
		$reviews->delete();
	}else{
		$reviews->status = $_POST['method'];
		$reviews->id = $_POST['review_id'];
		$reviews->update_review_status();	
	}	
}

if(isset($_POST['general_ajax_action']) && $_POST['general_ajax_action']=='save_custom_form'){
   update_option('moveto_custom_form',$_POST['formdata']);
}
/* Client Dashboard Login */
if(isset($_POST['general_ajax_action'],$_POST['username'],$_POST['password']) && $_POST['general_ajax_action']=='client_dashboard_login'){
   $creds                  = array();
	$creds['user_login']    = $_POST['username'];
	$creds['user_password'] = $_POST['password'];
	$creds['remember']      = true;
	$user                   = wp_signon($creds, false);
	if (is_wp_error($user)) {
		echo __("Invalid Username or Password.", "ak");
	} else {
		echo '1';die();
	}
}

if(isset($_POST['general_ajax_action'],$_POST['user_type']) && $_POST['general_ajax_action']=='refresh_register_client_datatable' && $_POST['user_type']=='registered'){ 

	$all_clients_info = $clients->get_registered_clients();
?>
<h3><?php echo __("Registered Customers","mp");?></h3>  <div class=""></div>
					<div id="accordion" class="panel-group">
						<table id="registered-client-table" class="display responsive nowrap table table-striped table-bordered" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th><?php echo __("Client Name","mp");?></th>
										<th><?php echo __("Email","mp");?></th>
										<th><?php echo __("Phone","mp");?></th>
										<th class="thd-w200"><?php echo __("Bookings","mp");?></th>
									</tr>
								</thead>
								<tbody id="mp_registered_list" >
									<?php 
										
										foreach($all_clients_info as $client_info){
												$mp_booking->client_id = $client_info->ID;
												$all_booking = $mp_booking->get_client_all_bookings_by_client_id();
												
												if(sizeof((array)$all_booking)>0){
													$allboking = sizeof((array)$all_booking)-1;
													$clientlastoid = $all_booking[$allboking]->order_id;
													$mp_booking->order_id = $clientlastoid;
													$order_client_info = $mp_booking->get_all_bookings_by_order_id();
												}
												if($order_client_info[0]->user_info!=''){
													$userinfo = unserialize($order_client_info[0]->user_info);
													$customer_name = $userinfo['name'];
													$customer_phone = $userinfo['user_phone'];
													$customer_email = $userinfo['email'];
												}else{
													$customer_name = '';
													$customer_phone = '';
													$customer_email = '';
												}
											?>
												<tr id="client_detail<?php echo $client_info->ID; ?>">
													<td><?php echo __(stripslashes_deep($customer_name),"mp");?></td>
													<td><?php echo __($customer_email,"mp");?></td>
													<td><?php echo __($customer_phone,"mp");?></td>
																
													
													
												<td class="mp-bookings-td">
													<a class="btn btn-primary mp_show_bookings " data-method="registered" data-client_id='<?php echo $client_info->ID; ?>' href="#registered-details" data-toggle="modal"><i class="icon-calendar icons icon-space"></i> <?php echo __("Leads","mp");?><span class="badge"><?php echo sizeof((array)$all_booking); ?></span></a>
																										
													<a data-poid="popover-delete-reg-client<?php echo $client_info->ID;?>" class="col-sm-offset-1 btn btn-danger mp-delete-popover " rel="popover" data-placement='bottom' title="<?php echo __("Delete this Client?","mp");?>"> <i class="fa fa-trash icon-space " title="<?php echo __("Delete Client","mp");?>"></i><?php echo __("Delete","mp");?></a>
													<div id="popover-delete-reg-client<?php echo $client_info->ID;?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<button data-method="registered" data-client_id="<?php echo $client_info->ID;?>" value="Delete" class="btn btn-danger btn-sm mp_delete_client" type="submit"><?php echo __("Yes","mp");?></button>
																		<button class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>											
													</td>
												</tr>
										   <?php  } ?>
								</tbody>
							</table>

						<div id="registered-details" class="modal fade booking-details-modal">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title"><?php echo __("Registered Customers Leads","mp");?></h4>
									</div>
									<div class="modal-body">
										<div class="table-responsive"> 
										<table id="registered-client-booking-details"  class="display responsive table table-striped table-bordered" cellspacing="0" width="100%">
												<thead>
													<tr>
														<th style="width: 9px !important;"><?php echo __("Order Id","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Source","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Via","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Destination","mp");?></th>
														<th style="width: 48px !important;"><?php echo __("Lead Date","mp");?></th>
														<th style="width: 67px !important;"><?php echo __("Service Info","mp");?></th>											
														<th style="width: 44px !important;"><?php echo __("Articles Info","mp");?></th>
														<th style="width: 70px !important;"><?php echo __("Additional Info","mp");?></th>
														<th style="width: 44px !important;"><?php echo __("Loading Details","mp");?></th>
														<th style="width: 39px !important;"><?php echo __("Unloading Details","mp");?></th>								
														<th style="width: 257px !important;"><?php echo __("Lead Client Info","mp");?></th>
													
													</tr>
												</thead>
												<tbody id="mp_client_bookingsregistered"></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
<?php 
}

if(isset($_POST['general_ajax_action'],$_POST['user_type']) && $_POST['general_ajax_action']=='refresh_register_client_datatable' && $_POST['user_type']=='guest'){
	

	$all_guesuser_info = $clients->get_all_guest_users_orders();
?>
<h3><?php echo __("Guest Customers","mp");?></h3>
				<div id="accordion" class="panel-group">

					<table id="guest-client-table" class="display responsive table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								
								<th><?php echo __("Client Name","mp");?></th>
								<th><?php echo __("Email","mp");?></th>
								<th><?php echo __("Phone","mp");?></th>
								<th class="thd-w200"><?php echo __("Bookings","mp");?></th>
								
							</tr>
						</thead>
						<tbody id="mp_guest_list">
							
							<?php foreach($all_guesuser_info as $client_info){						
									$mp_booking->order_id = $client_info->order_id;				
									$all_booking=$mp_booking->get_all_bookings_by_order_id();
									if($all_booking[0]->user_info!=''){
										$userinfo = unserialize($all_booking[0]->user_info);
										$customer_name = $userinfo['name'];
										$customer_phone = $userinfo['phone'];
										$customer_email = $userinfo['email'];
									}else{
										$customer_name = '';
										$customer_phone = '';
										$customer_email = '';
									}
									
								?>
									<tr id="client_detail<?php echo $client_info->order_id; ?>">
										<td><?php echo __(stripslashes_deep($customer_name),"mp");?></td>
										<td><?php echo __($customer_email,"mp");?></td>
										<td><?php echo __($customer_phone,"mp");?></td>
																
										<td class="mp-bookings-td"> 
										<a class="btn btn-primary mp_show_bookings" data-method="guest" data-client_id='<?php echo $client_info->order_id; ?>' href="#guest-details" data-toggle="modal"><i class="icon-calendar icons icon-space"></i><?php echo __("Leads","mp");?><span class="badge"><?php echo sizeof((array)$all_booking); ?></span></a>
																										
										<a data-poid="popover-delete-guest-client<?php echo $client_info->order_id; ?>" class="col-sm-offset-1 btn btn-danger mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete this Client?","mp");?>"> <i class="fa fa-trash icon-space " title="<?php echo __("Delete Client","mp");?>"></i><?php echo __("Delete","mp");?></a>
										
										<div id="popover-delete-guest-client<?php echo $client_info->order_id; ?>" style="display: none;">
											<div class="arrow"></div>
											<table class="form-horizontal" cellspacing="0">
												<tbody>
													<tr>
														<td>
															<button data-method="guest" data-client_id="<?php echo $client_info->order_id;?>" value="Delete" class="btn btn-danger btn-sm mp_delete_client" type="submit"><?php echo __("Yes","mp");?></button>
															<button class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</td>									
									</tr>
									   <?php  } ?>
							</tbody>
					</table>
						
					<div id="guest-details" class="modal fade booking-details-modal">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title"><?php echo __("Guest Customers Leads","mp");?></h4>
								</div>
								<div class="modal-body">
									<div class="table-responsive">
										<div class="table-responsive"> 
										<table id="guest-client-booking-details" class="display responsive table table-striped table-bordered" cellspacing="0" width="100%">
											<thead>
												<tr>
													<th style="width: 9px !important;"><?php echo __("Order Id","mp");?></th>
													<th style="width: 48px !important;"><?php echo __("Source","mp");?></th>
													<th style="width: 48px !important;"><?php echo __("Destination","mp");?></th>
													<th style="width: 48px !important;"><?php echo __("Lead Date","mp");?></th>
													<th style="width: 67px !important;"><?php echo __("Service Info","mp");?></th>											
													<th style="width: 44px !important;"><?php echo __("Articles Info","mp");?></th>
													<th style="width: 70px !important;"><?php echo __("Additional Info","mp");?></th>
													<th style="width: 44px !important;"><?php echo __("Loading Details","mp");?></th>
													<th style="width: 39px !important;"><?php echo __("Unloading Details","mp");?></th>								
													<th style="width: 257px !important;"><?php echo __("Lead Client Info","mp");?></th>
													
													<th style="width: 257px !important;"><?php echo __("Custom Fields Info","mp");?></th>
												</tr>
											</thead>
											<tbody id="mp_client_bookingsguest">
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> <?php
}
/* send quate */

/* PAM Send Quote */	
if(isset($_POST['general_ajax_action'],$_POST['booking_id'],$_POST['quote_price']) && $_POST['general_ajax_action']=='pam_send_quote' && $_POST['booking_id']!=''  && $_POST['quote_price']!='' ){
	
	$mp_booking->id = $_POST['booking_id']; 
	$mp_booking->quote_price = $_POST['quote_price']; 
	$mp_booking->quote_detail = $_POST['quote_description']; 
    $mp_booking->send_quote();
	
	$mp_booking->id = $_POST['booking_id']; 
	$mp_booking->readOne_by_booking_id();
	
		/* Get Loading Info */
	$loading_info = '';
	if($mp_booking->loading_info!=''){										
		$loading_info_arr = unserialize($mp_booking->loading_info);					 
	}
	
	/* Get UnLoading Info */
	$unloading_info = '';
	if($mp_booking->unloading_info!=''){										
		$unloading_info_arr = unserialize($mp_booking->unloading_info);				
	}
	$loading_info_arr['pam_loading_street_address'] = $_POST['loaiding_address_one'];
	$loading_info_arr['pam_loading_city'] = $_POST['loaiding_address_two'];
	$loading_info_arr['pam_loading_state'] = $_POST['pam_loading_three'];
	
	$unloading_info_arr['pam_unloading_street_address'] = $_POST['unloaiding_address_one'];
	$unloading_info_arr['pam_unloading_city'] = $_POST['unloaiding_address_two'];
	$unloading_info_arr['pam_unloading_state'] = $_POST['unloaiding_address_three'];
	
	
	/* unloading information */
	$unloading_info_arr['pam_unloading_street_address'] = $_POST['unloaiding_address_one'];
	$unloading_info_arr['pam_unloading_city'] = $_POST['unloaiding_address_two'];
	$unloading_info_arr['pam_unloading_state'] = $_POST['unloaiding_address_three'];
	
	 	$userinfo = unserialize($mp_booking->user_info);
		$user_in['email']= $_POST['email'];
		$user_in['name']= $_POST['name'];
		$user_in['phone']= $_POST['phone'];
	
	$mp_booking->id = $_POST['booking_id']; 

	
	$ser_load=serialize($loading_info_arr);
	$ser_unload=serialize($unloading_info_arr);

	
	$mp_booking->booking_date=$_POST['lead_date'];
	$mp_booking->source_city=base64_encode($_POST['source']);
	$mp_booking->via_city=base64_encode($_POST['via']);
	$mp_booking->destination_city=base64_encode($_POST['destination']);
	$mp_booking->loading_info=$ser_load;
	$mp_booking->unloading_info=$ser_unload;
	$mp_booking->user_info=$user_innfo;
	$mp_booking->booking_status = 'D'; 
	/* Update Booking information */
	$mp_booking->update_booking_status();
	
	/* Getting Booking Detail For Template Tags */
	$mp_booking->id = $_POST['booking_id']; 
    $mp_booking->readOne_by_booking_id();
	$source_city = $mp_booking->source_city;
	$destination_city = $mp_booking->destination_city;
	$booking_date = $mp_booking->booking_date;
	$order_id = $mp_booking->order_id;
	$quote_price =$mp_booking->quote_price;
	$quote_detail = $mp_booking->quote_detail;
	$cubic_feets_volume = $mp_booking->cubic_feets_volume;
	$insurance_rate = $mp_booking->insurance_rate;

 
	if($mp_booking->booking_status=='D'){
		$bookingstatus = __('Completed','mp');
	}else{
		$bookingstatus = __('Pending','mp');
	}
	
	$user_info = '';
	$client_email = '';
	$client_phone = '';
	$client_name = '';
		
	if($mp_booking->user_info!=''){										
		$userinfo = unserialize($mp_booking->user_info);
	    $client_email = $userinfo['email'];
		$client_name = ucfirst($userinfo['name']);
		$client_phone = $userinfo['phone'];
		$customer_name=ucwords($userinfo['name']);
		
		$user_info .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("User Information","mp").'</div>';
		$user_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Name',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$client_name.'</p></div>';
		$user_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Email',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$client_email.'</p></div>';
		$user_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Phone',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$client_phone.'</p></div>';
		
	}
	
	
	
	/* Get Service Info */
	
	$service_info = '';
	if($mp_booking->service_info!=''){
		$home_type_info = $mp_booking->move_size;

		$service_info_arr = unserialize($mp_booking->service_info);
		
		$article_name = $service_info_arr['get_article_name'];
		
		$service_info .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Services Details","mp").'</div><br></br>';
		$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Move Size',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$home_type_info.'</p></div><br></br>';

		$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Room Type',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$room_type_name.'</p></div>';
		foreach ($service_info_arr as $key => $value) {
			if(count((array)$value) > 0){
				
				if(isset($value['get_article_room_id'])) {
					 $room_id=$value['get_article_room_id'];
				}
				if(isset($value['get_article_id'])) {
					 $article_id=$value['get_article_id'];
				}
				if(isset($value['get_article_price'])) {
					 $article_price=$value['get_article_price'];
				}
				 
				 $moveto_article_category_obj->id=$room_id;
				$room_type_name_array= $moveto_article_category_obj->readOne_room_type();
	
				$room_type_name= $room_type_name_array[0]->name;
				
				
				
				if(isset($value['get_article_name'])) {
				$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Name',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['get_article_name'].'</p></div>';
				}
				if(isset($value['count'])) {
				$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Qty',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['count'].'</p></div>';
				}
				if(isset($value['per_quibcfeets_volume'])) {
				$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Cubic Volume',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['per_quibcfeets_volume'].'</p></div>';
				}	
				/*if(isset($value['get_article_price'])) {
				$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Base Price',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['get_article_price'].'</p></div>';	
				}
				if(isset($value['tot_article_price'])) {
				$service_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Total Price',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['tot_article_price'].'</p></div>';
				}*/
			}
		}
	}
	/* Custom Cubic Article */
	$custom_cubic_article = '';
	if($mp_booking->cubic_feets_article!=''){
		$service_info_cubic_article = unserialize($mp_booking->cubic_feets_article);
		if(is_array($service_info_cubic_article) || is_object($service_info_cubic_article))
			{				
			$service_info_cubic .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Custom Article","mp").'</div><br></br>';				
		foreach ($service_info_cubic_article as $key => $value) {
			if(count((array)$value) > 0){
				if(isset($value['cubicfeet_title'])) {
				$service_info_cubic .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Name',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['cubicfeet_title'].'</p></div>';
				$service_info_cubic .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Qty',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">1</p></div>';
				}
				if(isset($value['per_quibcfeets_volume'])) {
				$service_info_cubic .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Article Cubic Volume',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$value['per_quibcfeets_volume'].'</p></div>';
				}	
			}
		}
		}
	}
	
	$service_info_all = $service_info."</br>".$service_info_cubic;

	/* Insurance Rate */
	$service_insurance_rate = '';
	if($insurance_rate!=''){										
		$service_insurance_rate ='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Insurance Rate',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$insurance_rate.'</p></div>';
	}

	/* Total Cubic Volume */
	$service_cubic_total_volume = '';

	if($cubic_feets_volume!=''){										
		$service_cubic_total_volume ='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Total Cubic Volume',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$cubic_feets_volume.'</p></div>';
	}

	
	/* Get Additional Info */
	$additional_info = '';
	if($mp_booking->additional_info!=''){
		
		$additional_info_arr = unserialize($mp_booking->additional_info);
		$additional_info = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Additional Information","mp").'</div>';
		
		if(sizeof((array)$additional_info_arr['favorite'])>0){

				$additional_info_detail .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Additional Information","mp").'</div>';

					$returndata = $moveto_additional_info->readAll();

					$additionalArr = array();
					foreach ($additional_info_arr['favorite'] as $value) {
					array_push($additionalArr, $value);
					}
						foreach($returndata as $additional){

							if(in_array($additional->id, $additionalArr)){
								$additional_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$additional->additional_info.'</strong><p style=" width:40%;text-align:right;float:right;margin:0;">Yes</p></div>';
								
							}else
							{
								$additional_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.$additional->additional_info.'</strong><p style=" width:40%;text-align:right;float:right;margin:0;">No</p></div>';
								
							}

						}

				}
	}
	
	/* Get Loading Info */
	$loading_info = '';
		
	if($mp_booking->loading_info!=''){										
		$loading_info_arr = unserialize($mp_booking->loading_info);				
		$loading_info .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Loading Details","mp").'</div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Loading Floor no',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_floor_number"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Price Of Loading Floor',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_floor_amt"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Load Elevator ?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["load_elevator_checked"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Load Elevator Price',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["load_elevator_amt"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Load Packaging  ?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["packaging_checked"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Load Packaging Price',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["packaging_amt"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Street Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_one"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_floor_val"].'</p></div>';
		$loading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$loading_info_arr["loading_address_three"].'</p></div>';
		
		
	}
	/* Get UnLoading Info */
	$unloading_info = '';
	if($mp_booking->unloading_info!=''){										
		$unloading_info_arr = unserialize($mp_booking->unloading_info);				
		$unloading_info .='<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Unloading Details","mp").'</div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unloading Floor no',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_floor_number"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Price of Unloading Floor ',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_floor_amt"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unload Elevator ?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unload_elevator_checked"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unload Elevator Price',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unload_elevator_amt"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unload Unpackaging ?',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unpackaging_checked"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Unload Unpackaging Price',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unpackaging_amt"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('Street Address',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_one"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('City',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_two"].'</p></div>';
		$unloading_info .='<div style="width:400px; padding:0px 5px;"><strong style="font-weight: bold; width:60%; float:left;">'.__('State',"mp").'</strong><p style=" width:40%;text-align:right;float:left;margin:0;">'.$unloading_info_arr["unloading_address_three"].'</p></div>';
		
		
	}
	
	$company_name = get_option('moveto_company_name');
	$company_address = get_option('moveto_company_address');
	$company_city = get_option('moveto_company_city');
	$company_state = get_option('moveto_company_state');
	$company_zip = get_option('moveto_company_zip');
	$company_country = get_option('moveto_company_country');
	$company_phone = get_option('moveto_company_country_code').get_option('moveto_company_phone');
	$company_email = get_option('moveto_company_email');
	$company_logo = $business_logo = site_url()."/wp-content/uploads/".get_option('moveto_company_logo');
	$admin_name = get_option('moveto_email_sender_name');		
	$sender_email_address = get_option('moveto_email_sender_address');		
	$moveto_currency_symbol = get_option('moveto_currency_symbol');		
	$headers = "From: $admin_name <$sender_email_address>" . "\r\n";
	

	
	$search = array('{{client_name}}','{{admin_name}}','{{company_name}}','{{company_address}}','{{company_city}}','{{company_state}}','{{company_zip}}','{{company_country}}','{{company_phone}}','{{company_email}}','{{company_logo}}','{{order_id}}','{{source_city}}','{{destination_city}}','{{booking_date}}','{{bookingstatus}}','{{service_info}}','{{loading_info}}','{{unloading_info}}','{{additional_info}}','{{user_info}}','{{quote_price}}','{{quote_detail}}','{{admin_manager_name}}','{{customer_name}}','{{insurance_rate}}','{{total_cubic_volume}}');      
	$replace_with = array($client_name,$admin_name,$company_name,$company_address,$company_city,$company_state,$company_zip,$company_country,$company_phone,$company_email,$company_logo,$order_id,base64_decode($source_city),base64_decode($destination_city),date_i18n(get_option('date_format'),strtotime($booking_date)),$bookingstatus,$service_info_all,$loading_info,$unloading_info,$additional_info,$user_info,$moveto_currency_symbol.$quote_price,$quote_detail,$admin_name,$customer_name,$service_insurance_rate,$service_cubic_total_volume); 
	/* echo get_option('moveto_admin_email_notification_status');
	echo get_option('moveto_client_email_notification_status'); */
	/* Send email to Client when lead is complete */	
	if(get_option('moveto_client_email_notification_status')=='E' && $client_email!=''){	
		$mp_clientemail_templates = new moveto_email_template();
		$msg_template = $mp_clientemail_templates->email_parent_template;	
		$mp_clientemail_templates->email_template_name = "QC";
		$template_detail = $mp_clientemail_templates->readOne();        
		if($template_detail[0]->email_message!=''){  			
			$email_content = $template_detail[0]->email_message;        
		}else{ 			
			$email_content = $template_detail[0]->default_message;        
		}        
		 $email_subject = $template_detail[0]->email_subject;
		$email_client_message = '';
		/* Sending email to client when New quote request Sent */ 		  
		if($template_detail[0]->email_template_status=='e'){
			$email_client_message = str_replace($search,$replace_with,$email_content);	
			$email_client_message = str_replace('###msg_content###',$email_client_message,$msg_template);		
			add_filter( 'wp_mail_content_type', 'set_content_type' );				
			$status = wp_mail($client_email,$email_subject,$email_client_message,$headers);  		
		}       
	}
	/* Send email to Admin when lead is complete */
	if(get_option('moveto_admin_email_notification_status')=='E'){
		$mp_adminemail_templates = new moveto_email_template();
		$msg_template = $mp_adminemail_templates->email_parent_template;
		$mp_adminemail_templates->email_template_name = "QA";  		
		$template_detail = $mp_adminemail_templates->readOne();        
		if($template_detail[0]->email_message!=''){            
			$email_content = $template_detail[0]->email_message;        
		}else{            
			$email_content = $template_detail[0]->default_message;
		}        
		$email_subject = $template_detail[0]->email_subject;
		$email_admin_message = '';				
		$email_admin_message = str_replace($search,$replace_with,$email_content);	
		$email_admin_message = str_replace('###msg_content###',$email_admin_message,$msg_template);
		add_filter( 'wp_mail_content_type', 'set_content_type' );		
		$status = wp_mail(get_option('moveto_email_sender_address'),$email_subject,$email_admin_message,$headers);	
		
	}
	
	
		/******************* Send Sms code START *********************/
	if(get_option("moveto_sms_reminder_status") == "E"){
		/** TWILIO **/
		if(get_option("moveto_sms_noti_twilio") == "E"){
			$obj_sms_template = new moveto_sms_template();
			$twillio_sender_number = get_option('moveto_twilio_number');
			$AccountSid = get_option('moveto_twilio_sid');
			$AuthToken =  get_option('moveto_twilio_auth_token'); 
			
			/* Send SMS To Client */
			if(get_option('moveto_twilio_client_sms_notification_status') == "E"){
				$twilliosms_client = new Client($AccountSid, $AuthToken);
				$template1 = $obj_sms_template->gettemplate_sms("C",'e','QC');
				
				if($template1[0]->sms_template_status == "e" && $client_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					$client_sms_body = str_replace($search,$replace_with,$message);
					$twilliosms_client->messages->create(
						$client_phone,
						array(
							'from' => $twillio_sender_number,
							'body' => $client_sms_body 
						)
					);
				}
			}
			
			/* Send SMS To Admin */
			if(get_option('moveto_twilio_admin_sms_notification_status') == "E"){		   
				$twilliosms_admin = new Client($AccountSid, $AuthToken);
				$template = $obj_sms_template->gettemplate_sms("AM",'e','QA');
				if($template[0]->sms_template_status == "e" && get_option('moveto_twilio_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$admin_sms_body = str_replace($search,$replace_with,$message);	

					$twilliosms_staff->messages->create(
						get_option('moveto_twilio_ccode').get_option('moveto_twilio_admin_phone_no'),
						array(
							'from' => $twillio_sender_number,
							'body' => $admin_sms_body 
						)
					);
				}
			}
		}
				
		/** PLIVO **/
		if(get_option("moveto_sms_noti_plivo") == "E"){
			$obj_sms_template = new moveto_sms_template();
			$plivo_sender_number = get_option('moveto_plivo_number');	
			$auth_sid = get_option('moveto_plivo_sid');
			$auth_token = get_option('moveto_plivo_auth_token');
			
			/* Send SMS To Client */
			if(get_option('moveto_plivo_client_sms_notification_status') == "E"){
				$p_client = new Plivo\RestAPI($auth_sid, $auth_token, '', '');
				$template1 = $obj_sms_template->gettemplate_sms("C",'e','QC');
				if($template1[0]->sms_template_status == "e" && $client_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$client_sms_body = str_replace($search,$replace_with,$message);
					
					$clientparams = array(
						'src' => $plivo_sender_number,
						'dst' => $client_phone,
						'text' => $client_sms_body,
						'method' => 'POST'
					);
					$response = $p_client->send_message($clientparams);
				}
			}
			/* Send SMS To Admin */
			if(get_option('moveto_plivo_admin_sms_notification_status') == "E"){		   
				$twilliosms_admin = new Client($AccountSid, $AuthToken);
				$template = $obj_sms_template->gettemplate_sms("AM",'e','QA');
				if($template[0]->sms_template_status == "e" && get_option('moveto_plivo_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$admin_sms_body = str_replace($search,$replace_with,$message);	

					$adminparams = array(
						'src' => $plivo_sender_number,
						'dst' => get_option('moveto_plivo_ccode').get_option('moveto_plivo_admin_phone_no'),
						'text' => $admin_sms_body,
						'method' => 'POST'
						);
					$response = $p_admin->send_message($adminparams);
				}
			}
		}
		
		/** NEXMO **/
		if(get_option("moveto_sms_noti_nexmo") == "E"){
			$obj_sms_template = new moveto_sms_template();
			include_once(dirname(dirname(dirname(__FILE__))).'/objects/class_nexmo.php');
			$nexmo_client = new moveto_nexmo();
			$nexmo_client->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');
			$nexmo_client->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');
			$nexmo_client->moveto_nexmo_form = get_option('moveto_nexmo_form');
			
			/* Send SMS To Client */
			if(get_option('moveto_nexmo_send_sms_client_status') == "E"){
				$template1 = $obj_sms_template->gettemplate_sms("C",'e','QC');
				if($template1[0]->sms_template_status == "e" && $client_phone!=''){
					if($template1[0]->sms_message == ""){
						$message = strip_tags($template1[0]->default_message);
					}else{
						$message = strip_tags($template1[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$client_sms_body = str_replace($search,$replace_with,$message);
					$res = $nexmo_client->send_nexmo_sms($client_phone,$client_sms_body);
				}
			}

			/* Send SMS To Admin */
			if(get_option('moveto_nexmo_send_sms_admin_status') == "E"){
				$template = $obj_sms_template->gettemplate_sms("AM",'e','QA');
				if($template[0]->sms_template_status == "e" && get_option('moveto_nexmo_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$admin_sms_body = str_replace($search,$replace_with,$message);
					$nexmo_client->send_nexmo_sms(get_option('moveto_nexmo_ccode').get_option('moveto_nexmo_admin_phone_no'),$admin_sms_body);
				}
			}
		}
		
		/** TEXTLOCAL **/
		if(get_option("moveto_sms_noti_textlocal") == "E"){
			$obj_sms_template = new moveto_sms_template();
			$textlocal_apikey = get_option('moveto_textlocal_apikey');
			$textlocal_sender = get_option('moveto_textlocal_sender');
			
			/* Send SMS To Client */
			
			if(get_option('moveto_textlocal_client_sms_notification_status') == "E"){
				$template = $obj_sms_template->gettemplate_sms("C",'e','QC');
				if($template[0]->sms_template_status == "e" && $client_phone!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$client_sms_body = str_replace($search,$replace_with,$message);

					$textlocal_numbers = $client_phone;
					$textlocal_sender =$textlocal_sender;
					$client_sms_body = $client_sms_body;
					
					$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $client_sms_body);
					
					$ch = curl_init('https://api.textlocal.in/send/');
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
			
			/* Send SMS To Admin */
			if(get_option('moveto_textlocal_admin_sms_notification_status') == "E"){
				$template = $obj_sms_template->gettemplate_sms("AM",'e','QA');
				if($template[0]->sms_template_status == "e" && get_option('moveto_textlocal_admin_phone_no')!=''){
					if($template[0]->sms_message == ""){
						$message = strip_tags($template[0]->default_message);
					}else{
						$message = strip_tags($template[0]->sms_message);
					}
					$sender_name = get_option('moveto_email_sender_name');
					
					$admin_sms_body = str_replace($search,$replace_with,$message);

					echo $textlocal_numbers = get_option('moveto_textlocal_ccode').get_option('moveto_textlocal_admin_phone_no');
					$textlocal_sender = $textlocal_sender;
					$admin_sms_body = $admin_sms_body;
					
					$data = array('apikey' => $textlocal_apikey, 'numbers' => $textlocal_numbers, "sender" => $textlocal_sender, "message" => $admin_sms_body);
					
					$ch = curl_init('https://api.textlocal.in/send/');
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$result = curl_exec($ch);
					curl_close($ch);
				}
			}
		}
	}
	
}

	/* update appointment */
if(isset($_POST['action']) && $_POST['action']=='update_appointment_info_action')
{
	
	$booking_id=$_POST['booking_id'];
	$mp_booking->id = $_POST['booking_id'];
    $mp_booking->readOne_by_booking_id();
   /* $insurance_rate = $mp_booking->insurance_rate;
    $cubic_feets_volume = $mp_booking->cubic_feets_volume;*/

    
	
	if($mp_booking->service_id==1){
		$service_title = __('Home','mp');
	$service_id=1;		
	}elseif($mp_booking->service_id==2){
		$service_title = __('Office','mp');
		$service_id=2;	
	}elseif($mp_booking->service_id==3){
		$service_title = __('Vehicle','mp');
		$service_id=3;	
	}elseif($mp_booking->service_id==4){
		$service_title = __('Pets','mp');
		$service_id=4;	
	}elseif($mp_booking->service_id==6){
		$service_title = __('Other','mp');
		$service_id=6;	
	}
    
	if($mp_booking->service_id==1){
		$home_type = __('Appartment','mp');		
	}elseif($mp_booking->service_id==2){
		$home_type = __('Bungalow','mp');	
	}	
	if($mp_booking->booking_status=='D'){
		$bookingstatus = __('Completed','mp');
	}else{
		$bookingstatus = __('Pending','mp');
	}
	/* Get User Info */
	if($mp_booking->user_info!=''){
		$userinfo = unserialize($mp_booking->user_info);
		$pam_email = $userinfo['email'];
		$pam_name = $userinfo['name'];
		$pam_phone = $userinfo['phone'];
		
	}else{
		$pam_email = ''; $pam_name = '';	$pam_phone = '';
		$pam_custom_fields = array();
	}

	/* Get Service Info */
	$service_info = '';
	if($mp_booking->service_info!=''){
		$service_info_arr = unserialize($mp_booking->service_info);
		/*if($mp_booking->service_id==1){			
			$service_info = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Loading Information","mp").'</h5>';
			if($service_info_arr['loading_bunglow_type']!=''){
			$service_info .= '<li><label>'.__("Home Type","mp").'</label><span class="span-scroll span_indent">'.__("Bungalow","mp").'</span></li><li><label>'.__("Bungalow Type","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['loading_bunglow_type'].'</span></li>';
			
			$service_info .= '<li><label>'. __("Bungalow No","mp").'</label><span style="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
			
			}else{
			$service_info .= '<li><label>'. __("Home Type","mp").'</label><span class="span-scroll span_indent">'. __("Appartment","mp").'</span></li>';
			$service_info .= '<li><label>'. __("Floor Number","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['loading_floor_number'].'</span></li>';
			} 
						
			$service_info .= '<br><li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Unloading Information","mp").'</h5>';
			if($service_info_arr['unloading_bunglow_type']!=''){
			$service_info .= '<li><label>'.__("Home Type","mp").'</label><span class="span-scroll span_indent">'.__("Bungalow","mp").'</span></li><li><label>'. __("Bungalow Type","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['unloading_bunglow_type'].'</span></li>';
			
			$service_info .= '<li><label style="" class="mp-customer-details-hr">'. __("Bungalow No","mp").'</label><span style="span-scroll span_indent">'.$service_info_arr['unloading_floor_number'].'</span></li>';
			}else{
			$service_info .= '<li><label>'. __("Home Type","mp").'</label><span class="span-scroll span_indent">'. __("Appartment","mp").'</span></li>';
			$service_info .= '<li><label>'. __("Floor Number","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['unloading_floor_number'].'</span></li>';
			} 
			
			
		}*/
		/*if($mp_booking->service_id==2){
			$service_info .= '<li><label>'. __("Office Area","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['office_area'].' sq-ft</span></li>';
		}*/
	/*	if($mp_booking->service_id==3){
		if(isset($service_info_arr['vehicle_type'])){ 
		$vehical_array = array();
		$vehical_no_array = array();
			foreach($service_info_arr['vehicle_type'] as $ser_type){
				$vehical_array[] = $ser_type;
			}
			foreach($service_info_arr['no_of_vehicle'] as $no_of_veh){
				$vehical_no_array[] = $no_of_veh;
			}
			for($i=0;$i<sizeof($vehical_array);$i++){
				
				$service_info .= '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("VEHICLE DETAILS","mp").'</div>';
				$service_info .= '<div style="width:400px; padding:0px 5px;"><p style=" width:40%;text-align:right;float:left;margin:0;">'.$vehical_array[$i].' - '.$vehical_no_array[$i].'</p></div>';
			}
		}else{
				$service_info = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("VEHICLE DETAILS","mp").'</div>';
				$service_info .= '<div style="width:400px; padding:0px 5px;"><p style=" width:40%;text-align:right;float:left;margin:0;">'.$service_info_arr['vehicle_qty'].'</p></div>';
		}
	}*/
		
	/*	if($mp_booking->service_id==4){	
							foreach($service_info_arr as $key => $value){
										if(isset($value['pets_age_radio'])){
											$agetype = $value['pets_age_radio'];
										}else{
											$agetype = "";
										}
										if(isset($value['pet_weight_gen'])){
											$wttype = $value['pet_weight_gen'];
										}else{
											$wttype = "";
										}										
								$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Pet Information","mp").'</h5>';	
								$service_info .= '<h6 class="mp-customer-details-hr" style="font-weight: 600;">'.$value['pets_service'].'</h6>';		
								$service_info .= '<li><label>'.__("Pet Name","mp").'</label><span class="span-scroll span_indent">'.$value['pet_name'].'</span></li>
								<li><label>'.__("Pet Breed","mp").'</label><span class="span-scroll span_indent">'.$value['pet_breed'].'</span></li>';
								
								$service_info .= '<li><label>'. __("Pet Age","mp").'</label><span style="span-scroll span_indent">'.$value['pet_age'].''.$agetype.' </span></li><li><label>'.__("Pet Weight","mp").'</label><span class="span-scroll span_indent">'.$value['pet_weight_gen'].''.$wttype.' </span></li>';		
							}
						}*/
		/*if($mp_booking->service_id==6){				
				$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Other Information","mp").'</h5>';	
					
				$service_info .= '<li><label>'.__("service","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['service'].'</span></li>
				<li><label>'.__("other service Description","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['other_service_description'].'</span></li>
				<li><label>'.__("other service floor no","mp").'</label><span class="span-scroll span_indent">'.$service_info_arr['other_service_floor_no'].'</span></li>';
			
		}*/
	}
	
	/* Get Articles Info */
	$articles_info = '';
	if($mp_booking->service_id==6){
		if($mp_booking->articles_info!=''){
			$articles_info_arr = unserialize($mp_booking->articles_info);
			
			$articles_info_detail = '<div style="width:100%;float:left;text-transform: uppercase; font-size: 12px; letter-spacing: 1px; font-weight: bold; color: #B8B8B8; margin:10px 0;">'. __("Service Articles","mp").'</div>';
			$i=1;
			foreach($articles_info_arr['other_service_article'] as $rer){
			
				$articles_info_detail .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Article","mp").$i.'</h5><span class="span-scroll span_indent">'.$rer.'</span></li>';
				/* <b>Article'.$i.'</b>:'.$rer.'<br>'; */
				$i++;
				
			}	
			}
		}else{
	if($mp_booking->articles_info!=''){
		$articles_info_arr = unserialize($mp_booking->articles_info);
		if(sizeof((array)$articles_info_arr) > 0){

											
											foreach($articles_info_arr as $key => $value){
												$mp_service->addon_id = $key;
												$article_name = $mp_service->readOne_addon();
												if(sizeof((array)$article_name) > 0){
												$articles_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. $article_name[0]->addon_service_name.'</h5><span class="span-scroll span_indent">'.$value.'</span></li>';
											}
										}
										}
	 }
	}
	/* Get Additional Info */
	$additional_info = '';
	if($mp_booking->additional_info!=''){
		
		$additional_info_arr = unserialize($mp_booking->additional_info);
		$additional_info = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'. __("Additional Information","mp").'</h5>';
		if(is_array($additional_info_arr) || is_object($additional_info_arr))
											{
		foreach($additional_info_arr['favorite'] as $addkey => $addvalue){			
			if($addvalue=='Y'){
				$additional_status = __('Yes','mp');
			}else{
				$additional_status = __('No','mp');
			}
			$moveto_additional_info->id = $addkey;
			$moveto_additional_info->readOne();
			$additional_info .='<li><label>'.$moveto_additional_info->additional_info.'</label><span class="span-scroll span_indent">'.$additional_status.'</span></li>';
		}
		}
	}

	
	$city = new moveto_city();	

	?>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<input type="hidden" class="form-control" id="pam_quote_booking_id" value="<?php echo $mp_booking->id; ?>" />

							<!--<div class="form-group">
								<label for="moving_service" class="form-label  col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Moving service","mp");?></label>
								<select id="service_of_movo_to_quote" class="selectpicker col-xs-6 col-sm-6 col-md-9 col-lg-9 np" data-size="10" style="display: none;" data-live-search="true">
									
									<option value="1" <?php if($mp_booking->service_id==1){ echo "selected"; }?>><?php echo __("Home","mp");?></option>
									<option  value="2" <?php if($mp_booking->service_id==2){ echo "selected"; }?>><?php echo __("Office","mp");?></option>
									<option  value="3" <?php if($mp_booking->service_id==3){ echo "selected"; }?>><?php echo __("Vehicle","mp");?></option>
									<option  value="3" <?php if($mp_booking->service_id==4){ echo "selected"; }?>><?php echo __("Pets","mp");?></option>
									<option  value="3" <?php if($mp_booking->service_id==5){ echo "selected"; }?>><?php echo __("Commercial","mp");?></option>
									<option  value="3" <?php if($mp_booking->service_id==6){ echo "selected"; }?>><?php echo __("Other","mp");?></option>
								</select>
							</div>-->
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Lead Date","mp");?></label>

								<input type="text" id="mp_lead_date" disabled  value="<?php echo $mp_booking->booking_date; ?>"class=" col-xs-6 col-sm-6 col-md-9 col-lg-9 custom-width" />
							</div>

							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Source","mp");?></label>
								<input type="text" id="mp_source_mp" disabled value="<?php echo base64_decode($mp_booking->source_city); ?>"class=" col-xs-6 col-sm-6 col-md-9 col-lg-9 custom-width" />
								
							</div>

							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Via","mp");?></label>
								<input type="text" id="mp_via_mp" disabled value="<?php if($mp_booking->via_city != ''){ echo base64_decode($mp_booking->via_city);}else{ echo "";} ?>"class=" col-xs-6 col-sm-6 col-md-9 col-lg-9 custom-width" />
								
							</div>

							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Destination","mp");?></label>
								<input type="text" id="mp_destination_mp" disabled value="<?php echo base64_decode($mp_booking->destination_city); ?>"class=" col-xs-6 col-sm-6 col-md-9 col-lg-9 custom-width" />


							<!-- 	<select id="mp_destination_mp" class="selectpicker col-xs-6 col-sm-6 col-md-9 col-lg-9 np" data-size="10" style="display: none;" data-live-search="true">
								<?php 	$mp_cities = $city->readAll();
										foreach($mp_cities as $mp_city){ ?>
									<option   <?php if(base64_encode($mp_city->destination_city)==$mp_booking->destination_city){ echo 'selected="selected"'; } ?> value="<?php echo $mp_city->destination_city;  ?>"><?php echo $mp_city->destination_city;?></option>
									<?php  }?>
								</select>
							 -->	
							</div>
							
							<!--<h4>Loading Information</h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Home Type","mp");?></label>
								<select id="loading_select" class="selectpicker col-xs-6 col-sm-6 col-md-9 col-lg-9 np" data-size="10" style="display: none;" data-live-search="true">
									<?php ?>
									<option value="1"><?php echo __("Appartment","mp");?></option>
									<option  value="2"><?php echo __("Bungalow","mp");?></option>
								</select>
								
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Bungalow Type","mp");?></label>
								<select id="mp_booking_filterservice" class="selectpicker col-xs-6 col-sm-6 col-md-9 col-lg-9 np" data-size="10" style="display: none;" data-live-search="true">					
									<option value="1"><?php echo $service_info_arr['loading_bunglow_type'];?></option>
									
								</select>
								<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Bungalow No","mp");?></label>
								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_quote_price" id="moving_service" placeholder="<?php echo $quote_price;?>" />
								<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
							</div>
							
							<h4>Unloading Information</h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Home Type","mp");?></label>
								<select id="mp_booking_filterservice" class="selectpicker col-xs-6 col-sm-6 col-md-9 col-lg-9 np" data-size="10" style="display: none;" data-live-search="true">					
									<option value="1"><?php echo __("Home","mp");?></option>
									<option  value="2"><?php echo __("Office","mp");?></option>
									<option  value="3"><?php echo __("Vehicle","mp");?></option>
								</select>
								<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Bungalow Type","mp");?></label>
								<select id="mp_booking_filterservice" class="selectpicker col-xs-6 col-sm-6 col-md-9 col-lg-9 np" data-size="10" style="display: none;" data-live-search="true">					
								<option value="1"><?php echo $service_info_arr['unloading_bunglow_type']; ?>></option>
									<option  value="2"><?php echo __("Office","mp");?></option>
									<option  value="3"><?php echo __("Vehicle","mp");?></option>
								</select>
								<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Bungalow No","mp");?></label>
								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_quote_price" id="moving_service" placeholder="<?php echo $quote_price;?>" />
								<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
							</div>
							<h4>Service Articles</h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Door","mp");?></label>
								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_quote_price" id="moving_service" placeholder="<?php echo $quote_price;?>" />
								<label id="pam_quote_price_err" class="error" style="display:none;"><?php echo __("Please enter quote price.","mp"); ?></label>
							</div>
							<?php  ?>
							<h4>Additional Information</h4>
							-->
							<h4><?php echo __("Loading Details","mp");?></h4>
							<?php $loading_info = '';
							if($mp_booking->loading_info!=''){										
							$loading_info_arr = unserialize($mp_booking->loading_info);		 ?>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Loading Floor no","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["loading_floor_number"]; ?>" disabled name="loading_street_address" id="loading_street_address" placeholder="<?php echo $loading_info_arr["loading_floor_number"];?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Price Of Loading Floor","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9"  name="pam_loading_city" value="<?php echo $loading_info_arr["loading_floor_amt"]; ?>" disabled id="pam_loading_city" placeholder="<?php echo $loading_info_arr["loading_floor_amt"];?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Load Elevator ?","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["load_elevator_checked"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["load_elevator_checked"];?>" />
							</div>
							
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Load Elevator ?","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["load_elevator_checked"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["load_elevator_checked"];?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Load Elevator Price","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["load_elevator_amt"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["load_elevator_amt"];?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Load Packaging  ?","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["packaging_checked"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["packaging_checked"];?>" />
							</div>	
							
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Load Packaging Price","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["packaging_amt"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["packaging_amt"];?>" />
							</div>	
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Street Address","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["loading_address_one"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["loading_address_one"];?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("City","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["loading_address_two"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["loading_address_two"];?>" />
							</div><div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("State","mp");?></label>

								<input type="text" class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $loading_info_arr["loading_address_three"]; ?>"  disabled name="pam_loading_state" id="pam_loading_state" placeholder="<?php echo $loading_info_arr["loading_address_three"];?>" />
							</div>
							<?php } ?>
							<?php $unloading_info = '';
									if($mp_booking->unloading_info!=''){										
									$unloading_info_arr = unserialize($mp_booking->unloading_info);						 
							?>
							<h4><?php echo __("Unloading Details","mp");?></h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Unloading Floor no","mp");?></label>

								<input type="text"  disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_unloading_street_address" value="<?php echo $unloading_info_arr["unloading_floor_number"]; ?>" id="pam_unloading_street_address" placeholder="<?php echo $unloading_info_arr["unloading_floor_number"]?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Price of Unloading Floor ","mp");?></label>
								<input type="text"  disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unloading_floor_amt"];?>" name="pam_unloading_city" id="pam_unloading_city" placeholder="<?php echo $unloading_info_arr["unloading_floor_amt"];?>" />
								
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Unload Elevator ?","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unload_elevator_checked"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unload_elevator_checked"];?>" />
							</div>
							
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Unload Elevator Price","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unload_elevator_amt"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unload_elevator_amt"];?>" />
							</div><div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Unload Unpackaging ?","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unpackaging_checked"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unpackaging_checked"];?>" />
							</div><div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Unload Unpackaging Price","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unpackaging_amt"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unpackaging_amt"];?>" />
							</div><div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Street Address","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unloading_address_one"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unloading_address_one"];?>" />
							</div><div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("City","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unloading_address_two"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unloading_address_two"];?>" />
							</div><div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("State","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" value="<?php echo $unloading_info_arr["unloading_address_three"];?>" name="pam_unloading_state" id="pam_unloading_state" placeholder="<?php echo $unloading_info_arr["unloading_address_three"];?>" />
							</div>
							<?php } ?>


							<h4>Insurance</h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Insurance Rate","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="insurance_rate" value="<?php echo $mp_booking->insurance_rate;?>" id="insurance_rate" placeholder="<?php echo $mp_booking->insurance_rate;?>" />
							</div>
							<h4>Total Cubic Volume</h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Cubic Volume","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="cubic_feets_volume" value="<?php echo $mp_booking->cubic_feets_volume; ?>" id="cubic_feets_volume" placeholder="<?php echo $mp_booking->cubic_feets_volume; ?>" />
							</div>

							<h4>Customer</h4>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Name","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_name" value="<?php echo $pam_name;?>" id="pam_name" placeholder="<?php echo $pam_name;?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Email","mp");?></label>
								<input type="text" disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_email"  value="<?php echo $pam_email;?>" id="pam_email" placeholder="<?php echo $pam_email;?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Phone","mp");?></label>
								<input type="text"   disabled class="col-xs-6 col-sm-6 col-md-9 col-lg-9" name="pam_phone" value="<?php echo $pam_phone; ?>" id="pam_phone" placeholder="<?php echo $pam_phone;?>" />
							</div>
							<div class="form-group">
								<label for="moving_service" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"><?php echo __("Quote Description","mp");?></label>
								<textarea  id="pam_quote_description" class="form-label col-xs-6 col-sm-6 col-md-3 col-lg-3"> </textarea>
							</div>

							<div class="form-group">
								<label for="pam_quote_price" class="pam_quote_label"><?php echo __("Quote Price","mp");?></label>
								<label class="input-group-addon pam_quote_label"><?php echo get_option('moveto_currency_symbol'); ?></label>
								<input type="text" class=" col-xs-8 col-sm-8 col-md-11 col-lg-11 pam_quote_price" name="pam_quote_price"  id="pam_quote_price" placeholder="<?php echo $quote_price;?>" />
							</div>
							
		</div>
	<?php
}



/* Get Destination Cities */

if(isset($_POST['action'],$_POST['source_city']) && $_POST['action']=='get_destination_city' && $_POST['source_city']!='')
{
	$mp_city->source_city = $_POST['source_city'];

	$destination_cities = $mp_city->get_route_destination_cities();

	if(sizeof((array)$destination_cities)>0){ ?>

		<option value=""><?php echo __("Select Destination","mp");?></option>
		<?php
		
		foreach($destination_cities as $destination_citie){ 

			if(($destination_citie->source_city) == $_POST['source_city']){

				?><option value="<?php echo ($destination_citie->destination_city);?>"><?php echo $destination_citie->destination_city;?>
				<?php 

			}

			if(($destination_citie->destination_city) == $_POST['source_city']){

				?><option value="<?php echo ($destination_citie->source_city);?>"><?php echo $destination_citie->source_city;?></option>
				<?php 

			}			

		}
	
	}else{ 
	}	
}

/* moveto Add/Remove Sample Data */
if(isset($_REQUEST['general_ajax_action'],$_REQUEST['method']) && $_REQUEST['general_ajax_action']=='moveto_sampledata' && $_REQUEST['method']!=''){
	if($_REQUEST['method']=='Add'){
		$move_size_infoids	    = array();
		$additional_infoids		= array();
		$article_infoids		= array();
		$pricing_rules_infoids	= array();
		$mp_floors_infoids	    = array();
		$elevator_infoids	    = array();
		$packing_unpacking_infoids = array();
		$room_type_infoids	    = array();
		$art_cat_infoids        = array();
		$cubic_ids 				= array();
/* ------------------------------------------------------------------------------ */
		/* Insert movesize */
		$move_size_arr = array(array('move_size'=>'1BHK'),array('move_size'=>'2BHK'),array('move_size'=>'3BHK'));
		foreach($move_size_arr as $move_size_info){
			/* Add movesize Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_movesize (id,name,status) values( '','".$move_size_info['move_size']."','E')");
			$lastinsert_moveto_id = $wpdb->insert_id;
			$move_size_infoids[] = $lastinsert_moveto_id;
		}
		/* Additional Information */
		$additional_infos = array(array('info_name'=>'Packing material from us ?'),array('info_name'=>'Is material frisking ?'));
		foreach($additional_infos as $additional_info){
			/* Add Additional Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_additional_info (id,additional_info,status,type,price) values( '','".$additional_info['info_name']."','E','P','3')");
			$lastinsertid = $wpdb->insert_id;
			$additional_infoids[] = $lastinsertid;
		}
		
		/* Insert  Articles */
		$article_arr = array(
			array('article_name'=>'Single Bed','predefinedimage'=>'single-bed.png','cubicarea'=>'30'),
			array('article_name'=>'Double Bed','predefinedimage'=>'double-bed.png','cubicarea'=>'60'),
			array('article_name'=>'Table','predefinedimage'=>'study-table.png','cubicarea'=>'12'),
			array('article_name'=>'Fridge','predefinedimage'=>'fridge.png','cubicarea'=>'40'),
			array('article_name'=>'Washing Machine','predefinedimage'=>'washing-machine.png','cubicarea'=>'35'),
			array('article_name'=>'Fan','predefinedimage'=>'table-fan.png','cubicarea'=>'8'),
			array('article_name'=>'Bed Mattress','predefinedimage'=>'bed-mattress.png','cubicarea'=>'25'),
			array('article_name'=>'Bunk Bed','predefinedimage'=>'bunk-bed.png','cubicarea'=>'40'),
			array('article_name'=>'Computer Table','predefinedimage'=>'computer-table.png','cubicarea'=>'35'),
			array('article_name'=>'Computer','predefinedimage'=>'computer.png','cubicarea'=>'45'),
			array('article_name'=>'Dish Washer','predefinedimage'=>'dish-washer.png','cubicarea'=>'35'),
			array('article_name'=>'Dressing Table','predefinedimage'=>'dressing-table.png','cubicarea'=>'58'),
			array('article_name'=>'Foldable Chair','predefinedimage'=>'foldable-chair.png','cubicarea'=>'20'),
			array('article_name'=>'Home Theater','predefinedimage'=>'home-theater.png','cubicarea'=>'15'),
			array('article_name'=>'Mixer Grinder','predefinedimage'=>'mixer-grinder.png','cubicarea'=>'15'),
			array('article_name'=>'Music System','predefinedimage'=>'music-system.png','cubicarea'=>'19'),
			array('article_name'=>'Non-foldable Chair','predefinedimage'=>'non-foldable-chair.png','cubicarea'=>'20'),
			array('article_name'=>'Oven','predefinedimage'=>'oven.png','cubicarea'=>'15'),
			array('article_name'=>'Plastic Chair','predefinedimage'=>'plastic-chair.png','cubicarea'=>'15'),
			array('article_name'=>'Split Ac','predefinedimage'=>'split-ac.png','cubicarea'=>'40'),
			array('article_name'=>'Study Table','predefinedimage'=>'study-table.png','cubicarea'=>'25'),
			array('article_name'=>'Television Table','predefinedimage'=>'television-table.png','cubicarea'=>'30'),
			array('article_name'=>'Trolley Bag','predefinedimage'=>'trolley-bag.png','cubicarea'=>'8'),
			array('article_name'=>'Steel Wardrobe','predefinedimage'=>'wardrobe-steel.png','cubicarea'=>'10'),
			array('article_name'=>'Wooden Wardrobe','predefinedimage'=>'wardrobe-wooden.png','cubicarea'=>'10'),
			array('article_name'=>'Water Purifier','predefinedimage'=>'water-purifier.png','cubicarea'=>'15'),
			array('article_name'=>'Air Cooler','predefinedimage'=>'air-cooler.png','cubicarea'=>'45'),
			array('article_name'=>'Dining Table','predefinedimage'=>'dining-table-chairs.png','cubicarea'=>'45'),
			array('article_name'=>'3-Seater Sofa','predefinedimage'=>'sofa-seater-3.png','cubicarea'=>'50'),
			array('article_name'=>'Bicycle','predefinedimage'=>'Bicycle.png','cubicarea'=>'10'),
			array('article_name'=>'Bike-Bullet','predefinedimage'=>'Bike-bullet.png','cubicarea'=>'10'),
			array('article_name'=>'Bike-With-Gear','predefinedimage'=>'Bike-with-gear.png','cubicarea'=>'10'),
			array('article_name'=>'Car-Hatchback','predefinedimage'=>'Car-hatchback.png','cubicarea'=>'20'),
			array('article_name'=>'Car-Sedan','predefinedimage'=>'Car-sedan.png','cubicarea'=>'20'),
			array('article_name'=>'Car-Suv','predefinedimage'=>'Car-suv.png','cubicarea'=>'20'),
			array('article_name'=>'Moped','predefinedimage'=>'Moped.png','cubicarea'=>'10')
		);
		foreach($article_arr as $article_arr_info){
			/* Add movesize Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_articles (id,article_name,type,price,predefinedimage,max_qty,status,cubicarea) values( '','".$article_arr_info['article_name']."','P','5','".$article_arr_info['predefinedimage']."','5','E','".$article_arr_info['cubicarea']."')");
			$lastinsert_article_id = $wpdb->insert_id;
			$article_infoids[] = $lastinsert_article_id;
		}
		
		/* Insert Pricing rules */
		
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (id,distance,rules,price,distance_id) values( '','10','G','10','1')");
			$pricing_rules_infoids[]= $wpdb->insert_id;
			
			$stmt1 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (id,distance,rules,price,distance_id) values( '','50','G','60','1')");
			$pricing_rules_infoids[]= $wpdb->insert_id;
			
			$stmt2 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (id,distance,rules,price,distance_id) values( '','80','G','100','1')");
			$pricing_rules_infoids[]= $wpdb->insert_id;
			
		/* Insert Floor */
			$stmtt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','0','1','0')");
			$mp_floors_infoids[]= $wpdb->insert_id;

			$stmtt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','10','1','1')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt2 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','12','1','2')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt3 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','14','1','3')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt4 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','16','1','4')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt5 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','17','1','5')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
		/* Insert loading/ unloading elevator */	
			$stmtt_ele = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_elevator (id,discount,distance_id) values( '','3','1')");
			$elevator_infoids[]= $wpdb->insert_id;
			
		/* Insert packing/ unpacking */	
			$stmtt_ele = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_packaging_unpackagin (id,cost_packing,cost_unpacking,distance_id) values( '1','3','2','1')");
			$packing_unpacking_infoids[]= $wpdb->insert_id;	
			
		/* Insert roomtypes */	
			$roomtypes_arr = array(array('name'=>'Bedroom'),array('name'=>'Livingroom'),array('name'=>'Kitchen'),array('name'=>'Vehicles'));
			foreach($roomtypes_arr as $roomtypes_arr_info){
			/* Add roomtypes Info */
			$stmty = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_room_type (id,name,status) values( '','".$roomtypes_arr_info['name']."','E')");
			$roomtypes_id = $wpdb->insert_id; 
			$room_type_infoids[] = $roomtypes_id;
		}		 
		/* Add article category */
			/*$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_article_category'."";
			$stmt = $wpdb->get_results($queryString); 
			if(count((array)$stmt)==0){*/
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Furniture','E','".$article_infoids[0].",".$article_infoids[1].",".$article_infoids[2].",".$article_infoids[6].",".$article_infoids[7].",".$article_infoids[8].",".$article_infoids[11].",".$article_infoids[12].",".$article_infoids[16].",".$article_infoids[18].",".$article_infoids[20].",".$article_infoids[21].",".$article_infoids[23].",".$article_infoids[24]."','".$room_type_infoids[0]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Furniture','E','".$article_infoids[2].",".$article_infoids[8].",".$article_infoids[11].",".$article_infoids[12].",".$article_infoids[16].",".$article_infoids[18].",".$article_infoids[20].",".$article_infoids[21].",".$article_infoids[24].",".$article_infoids[27].",".$article_infoids[28]."','".$room_type_infoids[1]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Furniture','E','".$article_infoids[2].",".$article_infoids[12].",".$article_infoids[16].",".$article_infoids[24].",".$article_infoids[27]."','".$room_type_infoids[2]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Electronic','E','".$article_infoids[5].",".$article_infoids[9].",".$article_infoids[15].",".$article_infoids[19].",".$article_infoids[26]."','".$room_type_infoids[0]."')");
				$art_cat_infoids[] = $wpdb->insert_id;
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Electronic','E','".$article_infoids[5].",".$article_infoids[9].",".$article_infoids[13].",".$article_infoids[15].",".$article_infoids[19].",".$article_infoids[26]."','".$room_type_infoids[1]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Electronic','E','".$article_infoids[3].",".$article_infoids[5].",".$article_infoids[10].",".$article_infoids[14].",".$article_infoids[17].",".$article_infoids[17].",".$article_infoids[4]."','".$room_type_infoids[2]."')");
				$art_cat_infoids[] = $wpdb->insert_id;
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Vehicles','E','".$article_infoids[29].",".$article_infoids[30].",".$article_infoids[31].",".$article_infoids[32].",".$article_infoids[33].",".$article_infoids[34].",".$article_infoids[35]."','".$room_type_infoids[3]."')");
				$art_cat_infoids[] = $wpdb->insert_id;
			/*}*/	


			/* Cubic Meter */
			$cubic_meter_arr = array(array('cubicfeets_range'=>'0','cubicfeets_range_price'=>'3'),array('cubicfeets_range'=>'50','cubicfeets_range_price'=>'5'),array('cubicfeets_range'=>'150','cubicfeets_range_price'=>'8'),array('cubicfeets_range'=>'250','cubicfeets_range_price'=>'10'),array('cubicfeets_range'=>'400','cubicfeets_range_price'=>'15'));
			foreach($cubic_meter_arr as $cubic_meter_arr_val){
			/* Add Cubic Meter Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_cubicfeets (id,cubicfeets_range,cubicfeets_range_price) values( '','".$cubic_meter_arr_val['cubicfeets_range']."','".$cubic_meter_arr_val['cubicfeets_range_price']."')");
			$cubic_id = $wpdb->insert_id; 
			$cubic_ids[] = $cubic_id;
			}

		
/* ---------------------------------------------------------------------------- */
/* Booking details data */
		
		/* Getting last order_id */
		$next_order_id = 0;
		$mp_booking->get_last_order_id();
		if(isset($mp_booking->last_order_id) && $mp_booking->last_order_id!=''){
			$next_order_id = $mp_booking->last_order_id + 1;	
		}else{
			$next_order_id = 1001;
		}
		$serviceinfo1 = 'a:7:{i:0;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:1:"7";s:14:"get_article_id";s:1:"7";s:16:"get_article_name";s:10:"Single Bed";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:1;s:0:"";i:2;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:2:"10";s:14:"get_article_id";s:2:"12";s:16:"get_article_name";s:11:"Ceiling Fan";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:3;a:7:{s:19:"get_article_room_id";s:1:"5";s:18:"get_article_cat_id";s:1:"8";s:14:"get_article_id";s:1:"9";s:16:"get_article_name";s:5:"Table";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:4;s:0:"";i:5;s:0:"";i:6;a:7:{s:19:"get_article_room_id";s:1:"6";s:18:"get_article_cat_id";s:2:"12";s:14:"get_article_id";s:2:"10";s:16:"get_article_name";s:6:"Fridge";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}}';
		
		$loadinginfo1 = 'a:9:{s:17:"loading_floor_amt";d:2;s:20:"loading_floor_number";s:1:"1";s:17:"load_elevator_amt";d:0.59999999999999997779553950749686919152736663818359375;s:21:"load_elevator_checked";s:3:"Yes";s:13:"packaging_amt";d:0.59999999999999997779553950749686919152736663818359375;s:17:"packaging_checked";s:3:"Yes";s:19:"loading_address_one";s:13:"12 Fox Street";s:19:"loading_address_two";s:6:"Denver";s:21:"loading_address_three";s:8:"Colorado";}';
		
		$unloadinginfo1 = 'a:9:{s:19:"unloading_floor_amt";d:2.79999999999999982236431605997495353221893310546875;s:22:"unloading_floor_number";s:1:"3";s:19:"unload_elevator_amt";d:0.59999999999999997779553950749686919152736663818359375;s:23:"unload_elevator_checked";s:3:"Yes";s:15:"unpackaging_amt";d:0.40000000000000002220446049250313080847263336181640625;s:19:"unpackaging_checked";s:3:"Yes";s:21:"unloading_address_one";s:19:"23 SW Market Street";s:21:"unloading_address_two";s:8:"Portland";s:23:"unloading_address_three";s:6:"Oregon";}';
		
		$additionalinfo1 = 'a:1:{s:8:"favorite";a:1:{i:0;s:1:"3";}}';
		
		$userinfo1 = 'a:3:{s:4:"name";s:13:"Jamie L smith";s:5:"email";s:15:"jamie@gmail.com";s:5:"phone";s:10:"9056565666";}';
		
		



		$serviceinfo2 = 'a:5:{i:0;s:0:"";i:1;s:0:"";i:2;s:0:"";i:3;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:1:"7";s:14:"get_article_id";s:1:"8";s:16:"get_article_name";s:10:"Double Bed";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:4;a:7:{s:19:"get_article_room_id";s:1:"6";s:18:"get_article_cat_id";s:1:"9";s:14:"get_article_id";s:1:"9";s:16:"get_article_name";s:5:"Table";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}}';	
		
		$loadinginfo2 = 'a:9:{s:17:"loading_floor_amt";d:1.1999999999999999555910790149937383830547332763671875;s:20:"loading_floor_number";s:1:"2";s:17:"load_elevator_amt";d:0.299999999999999988897769753748434595763683319091796875;s:21:"load_elevator_checked";s:3:"Yes";s:13:"packaging_amt";d:0.299999999999999988897769753748434595763683319091796875;s:17:"packaging_checked";s:3:"Yes";s:19:"loading_address_one";s:9:"5 K ST NW";s:19:"loading_address_two";s:8:"Downtown";s:21:"loading_address_three";s:10:"Washington";}';	
		$unloadinginfo2 = 'a:9:{s:19:"unloading_floor_amt";d:1.600000000000000088817841970012523233890533447265625;s:22:"unloading_floor_number";s:1:"4";s:19:"unload_elevator_amt";d:0.299999999999999988897769753748434595763683319091796875;s:23:"unload_elevator_checked";s:3:"Yes";s:15:"unpackaging_amt";d:0.200000000000000011102230246251565404236316680908203125;s:19:"unpackaging_checked";s:3:"Yes";s:21:"unloading_address_one";s:10:"6 South St";s:21:"unloading_address_two";s:13:"Filter Square";s:23:"unloading_address_three";s:12:"Philadelphia";}';
		$additionalinfo2 = 'a:1:{s:8:"favorite";a:1:{i:0;s:1:"4";}}';
		$userinfo2 = 'a:3:{s:4:"name";s:5:"Linda";s:5:"email";s:20:"lindasmith@gmail.com";s:5:"phone";s:10:"9876543210";}';
		
		


		$serviceinfo3 = 'a:4:{i:0;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:2:"10";s:14:"get_article_id";s:2:"12";s:16:"get_article_name";s:11:"Ceiling Fan";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:1;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:1:"7";s:14:"get_article_id";s:1:"7";s:16:"get_article_name";s:10:"Single Bed";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:2;a:7:{s:19:"get_article_room_id";s:1:"5";s:18:"get_article_cat_id";s:1:"8";s:14:"get_article_id";s:1:"9";s:16:"get_article_name";s:5:"Table";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:3;s:0:"";}';
		$loadinginfo3 = 'a:9:{s:17:"loading_floor_amt";d:2.54999999999999982236431605997495353221893310546875;s:20:"loading_floor_number";s:1:"5";s:17:"load_elevator_amt";d:0;s:21:"load_elevator_checked";s:2:"No";s:13:"packaging_amt";d:0.450000000000000011102230246251565404236316680908203125;s:17:"packaging_checked";s:3:"Yes";s:19:"loading_address_one";s:10:"21 W 35 St";s:19:"loading_address_two";s:7:"Chicago";s:21:"loading_address_three";s:8:"Illinois";}';
		$unloadinginfo3 = 'a:9:{s:19:"unloading_floor_amt";d:2.100000000000000088817841970012523233890533447265625;s:22:"unloading_floor_number";s:1:"3";s:19:"unload_elevator_amt";d:0.450000000000000011102230246251565404236316680908203125;s:23:"unload_elevator_checked";s:3:"Yes";s:15:"unpackaging_amt";d:0;s:19:"unpackaging_checked";s:2:"No";s:21:"unloading_address_one";s:18:"32 Columbia Street";s:21:"unloading_address_two";s:7:"Seattle";s:23:"unloading_address_three";s:10:"Washington";}';
		
		$additionalinfo3 = 'a:1:{s:8:"favorite";a:2:{i:0;s:1:"3";i:1;s:1:"4";}}';
		$userinfo3 = 'a:3:{s:4:"name";s:4:"John";s:5:"email";s:19:"johnmartin@demo.com";s:5:"phone";s:10:"7463543210";}';

		$booking_custom_cubic1 = 'a:2:{i:1;a:3:{s:15:"cubicfeet_title";s:3:"bed";s:15:"cubicfeet_count";s:3:"140";s:6:"moreid";s:4:"row2";}i:2;a:3:{s:15:"cubicfeet_title";s:5:"Table";s:15:"cubicfeet_count";s:2:"18";s:6:"moreid";s:4:"row3";}}';
		


		$mp_clientinfo = array(array('client_name'=>'Jamie L smith','client_email'=>'jamie@gmail.com','client_phone'=>'+19056565666'),array('client_name'=>'Linda','client_email'=>'lindasmith@gmail.com','client_phone'=>'+19876543210'),array('client_name'=>'John','client_email'=>'johnmartin@demo.com','client_phone'=>'+17463543210'));
		






		$mpclientids = array();
		
		foreach($mp_clientinfo as $mp_clientsinfo){
		/* Get order id of user */
			

			$mp_user_info = array(
						'user_login'    =>   $mp_clientsinfo['client_name'],
						'user_email'    =>   $mp_clientsinfo['client_email'],
						'user_pass'     =>   '',
						'first_name'    =>   $mp_clientsinfo['client_name'],
						'last_name'     =>   '',
						'nickname'      =>  '',
						'role' => 'subscriber'
						);	
	   
			$new_mp_user = wp_insert_user( $mp_user_info );
			$mpclientids[] =  $new_mp_user;
			/*$user = new WP_User($new_mp_user);
			$user->add_cap('read');
			$user->add_cap('mp_client'); 
			$user->add_role('mp_users');
			$user_id = $new_mp_user;
			$user_login =  $mp_clientsinfo['client_name'];
			add_user_meta( $new_mp_user, '#','#');*/
			
		}
		
		$bookingdate1 = date_i18n('Y-m-d',strtotime('+1 day',strtotime(date_i18n('Y-m-d'))));
		$bookingdate2 = date_i18n('Y-m-d',strtotime('+3 day',strtotime(date_i18n('Y-m-d'))));
		$bookingdate3 = date_i18n('Y-m-d',strtotime('+5 day',strtotime(date_i18n('Y-m-d'))));
		
		$orderidbooking2 = $next_order_id+1;
		$orderidbooking3 = $next_order_id+2;
		
		
		$bookingids = array();
		$orderid = array();
		$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_bookings (`id`,`order_id`, `client_id`,`source_city`, `via_city`,`destination_city`,`move_size`, `booking_date`, `quote_price`, `quote_detail`, `service_info`, `loading_info`, `unloading_info`, `additional_info`, `user_info`, `booking_status`, `reminder`, `notification`, `lastmodify`,`insurance_rate`,`cubic_feets_article`,`cubic_feets_volume`) VALUES ('','".$next_order_id."','".$mpclientids[0]."','RGVudmVy','UGFsaXNhZGU=','UG9ydGxhbmQ=','1BHK','".$bookingdate1."','125.6','','".$serviceinfo1."','".$loadinginfo1."','".$unloadinginfo1."','".$additionalinfo1."','".$userinfo1."','P','0','0','".date_i18n('Y-m-d H:i:s')."','','".$booking_custom_cubic1."','')");
		$bookingids[] = $wpdb->insert_id;
		$query1="INSERT INTO ".$wpdb->prefix."mp_order_client_info (`id`, `order_id`, `client_name`, `client_email`, `client_phone`, `client_personal_info`) VALUES ('', '".$next_order_id."', 'Jamie L smith', 'jamie@gmail.com', '9056565666', '');";
		$wpdb->query($query1);
		$orderid[] = $wpdb->insert_id;
		
		/* Add 2nd Bookings */
		$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_bookings (`id`,`order_id`, `client_id`, `source_city`, `via_city`,`destination_city`,`move_size`, `booking_date`, `quote_price`, `quote_detail`, `service_info`, `loading_info`, `unloading_info`, `additional_info`, `user_info`, `booking_status`, `reminder`, `notification`, `lastmodify`,`insurance_rate`,`cubic_feets_article`,`cubic_feets_volume`) VALUES ('','".$orderidbooking2."','".$mpclientids[1]."','V2FzaGluZ3Rvbg==','QmFsdGltb3Jl','UGhpbGFkZWxwaGlh','2BHK','".$bookingdate2."','112.7','','".$serviceinfo2."','".$loadinginfo2."','".$unloadinginfo2."','".$additionalinfo2."','".$userinfo2."','P','0','0','".date_i18n('Y-m-d H:i:s')."','','".$booking_custom_cubic1."','')");
		$bookingids[] = $wpdb->insert_id;
		
		/* order_client_info */
		$query1="INSERT INTO ".$wpdb->prefix."mp_order_client_info (`id`, `order_id`, `client_name`, `client_email`, `client_phone`, `client_personal_info`) VALUES ('', '".$orderidbooking2."', 'Linda', 'lindasmith@gmail.com', '9876543210', '');";
		$wpdb->query($query1);
		$orderid[] = $wpdb->insert_id;
		
		/* Add 3rd Bookings */
		$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_bookings (`id`,`order_id`, `client_id`, `source_city`, `via_city`,`destination_city`,`move_size`, `booking_date`, `quote_price`, `quote_detail`, `service_info`, `loading_info`, `unloading_info`, `additional_info`, `user_info`, `booking_status`, `reminder`, `notification`, `lastmodify`,`insurance_rate`,`cubic_feets_article`,`cubic_feets_volume`) VALUES ('','".$orderidbooking3."','".$mpclientids[2]."','Q2hpY2Fnbw==','TWlubmVhcG9saXM=','U2VhdHRsZQ==','3BHK','".$bookingdate3."','119.65','','".$serviceinfo3."','".$loadinginfo3."','".$unloadinginfo3."','".$additionalinfo3."','".$userinfo3."','P','0','0','".date_i18n('Y-m-d H:i:s')."','','".$booking_custom_cubic1."','')");
		$bookingids[] = $wpdb->insert_id; 
		$query1="INSERT INTO ".$wpdb->prefix."mp_order_client_info (`id`, `order_id`, `client_name`, `client_email`, `client_phone`, `client_personal_info`) VALUES ('', '".$orderidbooking3."', 'John', 'johnmartin@demo.com', '7463543210', '');";
		$wpdb->query($query1);
		$orderid[] = $wpdb->insert_id;
		
		
		$sampledataids = array('roomtype'=>implode(',',$room_type_infoids),'article_info'=>implode(',',$article_infoids),'additional_infoids'=>implode(',',$additional_infoids),'movesize'=>implode(',',$move_size_infoids),'pricingrules'=>implode(',',$pricing_rules_infoids),'floorinfo'=>implode(',',$mp_floors_infoids),'elevatorinfo'=>implode(',',$elevator_infoids),'pack_unpack'=>implode(',',$packing_unpacking_infoids),'art_cat_infoids'=>implode(',',$art_cat_infoids),'mpclients'=>implode(',',$mpclientids),'bookinginfo'=>implode(',',$bookingids),'orderinfo'=>implode(',',$orderid),'cubic_feets'=>implode(',',$cubic_ids));
		
		add_option('moveto_sample_dataids',serialize($sampledataids));
		$sampledata_st = get_option('moveto_sample_status');	
		if(isset($sampledata_st)){
			update_option('moveto_sample_status','N');	
		}else{
			add_option('moveto_sample_status','N');	
		}

		$min_rate_moveto = get_option('moveto_min_rate_service');	
		if(isset($min_rate_moveto)){
			update_option('moveto_min_rate_service','230');	
		}else{
			add_option('moveto_min_rate_service','230');	
		}

		$insurance_rate_moveto = get_option('moveto_insurance_rate');	
		if(isset($insurance_rate_moveto)){
			update_option('moveto_insurance_rate','30');	
		}else{
			add_option('moveto_insurance_rate','30');	
		}

		$insurance_permission_moveto = get_option('moveto_insurance_permission');	
		if(isset($insurance_permission_moveto)){
			update_option('moveto_insurance_permission','1');	
		}else{
			add_option('moveto_insurance_permission','1');	
		}
		
		
		

		$cubic_meter_moveto = get_option('moveto_cubic_meter');	
		if(isset($cubic_meter_moveto)){
			update_option('moveto_cubic_meter','ft_3');	
		}else{
			add_option('moveto_cubic_meter','ft_3');	
		}
		
	
	}else{ 
	
		/* Remove Sample Data */
		$sampledata_info = unserialize(get_option('moveto_sample_dataids'));
	//	$service_articleids = explode(",",$sampledata_info['service_articleids']);
		$additional_infoid = explode(",",$sampledata_info['additional_infoids']);
		$bookinginfo = explode(",",$sampledata_info['bookinginfo']);
		$roomtypeids = explode(",",$sampledata_info['roomtype']);
		$article_infoid = explode(",",$sampledata_info['article_info']);
		$art_cat_infoid = explode(",",$sampledata_info['art_cat_infoids']);
		$movesize = explode(",",$sampledata_info['movesize']);
		$pricingrules = explode(",",$sampledata_info['pricingrules']);
		$floorinfo = explode(",",$sampledata_info['floorinfo']);
		$elevatorinfo = explode(",",$sampledata_info['elevatorinfo']);
		$pack_unpack = explode(",",$sampledata_info['pack_unpack']);
		$orderinfo = explode(",",$sampledata_info['orderinfo']);
		$mpclients = explode(",",$sampledata_info['mpclients']);
		$cubic_feets = explode(",",$sampledata_info['cubic_feets']);

		/* Delete Sample Service Articles */
		/*foreach($service_articleids as $service_articleid){
			$wpdb->query("Delete from ".$wpdb->prefix."mp_services_addon where id='".$service_articleid."'");
		}*/
		/* Delete Sample Additional Info */
		foreach($additional_infoid as $additional_info){
			$wpdb->query("Delete from ".$wpdb->prefix."mp_additional_info where id='".$additional_info."'");
		}
		foreach($roomtypeids as $roomtypeids_data){
			$qry_roomtype="Delete from ".$wpdb->prefix."mp_room_type where id='".$roomtypeids_data."'";
			$wpdb->query($qry_roomtype);
		}
		foreach($article_infoid as $article_infoids_data){
		    $qry="Delete from ".$wpdb->prefix."mp_articles where id='".$article_infoids_data."'";
			$wpdb->query($qry);
			
		}
		foreach($movesize as $movesize_data){
		    $qry="Delete from ".$wpdb->prefix."mp_movesize where id='".$movesize_data."'";
			$wpdb->query($qry);
			
		}
		foreach($pricingrules as $pricingrules_data){
		    $qry="Delete from ".$wpdb->prefix."mp_pricing_rules where id='".$pricingrules_data."'";
			$wpdb->query($qry);
			
		}
		foreach($floorinfo as $floorinfo_data){
		    $qry="Delete from ".$wpdb->prefix."mp_floors where id='".$floorinfo_data."';";
			$wpdb->query($qry);
			
		}
		foreach($elevatorinfo as $elevatorinfo_data){
		    $qry="Delete from ".$wpdb->prefix."mp_elevator where id='".$elevatorinfo_data."'";
			$wpdb->query($qry);
			
		}
		foreach($pack_unpack as $pack_unpack_data){
		    $qry="Delete from ".$wpdb->prefix."mp_packaging_unpackagin where id='".$pack_unpack_data."'";
			$wpdb->query($qry);
			
		}
		 foreach($art_cat_infoid as $art_cat_data){
		    $qry="Delete from ".$wpdb->prefix."mp_article_category where id='".$art_cat_data."'";
			$wpdb->query($qry);
			
		} 

		foreach($bookinginfo as $bookingid){
			$qry_booking="Delete from ".$wpdb->prefix."mp_bookings where id='".$bookingid."';";
			$wpdb->query($qry_booking);
		}

		foreach($orderinfo as $orderidss){
			$qry_booking="Delete from ".$wpdb->prefix."mp_order_client_info where id='".$orderidss."';";
			$wpdb->query($qry_booking);
		}

		foreach($mpclients as $mpclientidsss){
			$qry_booking="Delete from ".$wpdb->prefix."users where id ='".$mpclientidsss."'";
			$wpdb->query($qry_booking);
		}
		foreach($mpclients as $mpclientidss){
			$qry_booking="Delete from ".$wpdb->prefix."usermeta where user_id ='".$mpclientidss."'";
			$wpdb->query($qry_booking);
		}
		foreach ($cubic_feets as $cubic_ids) {
			$qry_cubic="Delete from ".$wpdb->prefix."mp_cubicfeets where id ='".$cubic_ids."'";
			$wpdb->query($qry_cubic);
		}
		update_option('moveto_min_rate_service','');
		update_option('moveto_insurance_rate','');
		update_option('moveto_insurance_permission','');
		update_option('moveto_cubic_meter','');
		delete_option('moveto_sample_dataids');
		update_option('moveto_sample_status','Y');
	}
	
}
