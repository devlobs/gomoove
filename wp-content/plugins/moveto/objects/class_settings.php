<?php
class moveto_settings{
	
   /** Appearance Settings **/
   public $moveto_primary_color;
   public $moveto_secondary_color;
   public $moveto_text_color;
   public $moveto_bg_text_color;  
   public $moveto_admin_color_primary;  
   public $moveto_admin_color_secondary;  
   public $moveto_admin_color_text;  
   public $moveto_admin_color_bg_text;  
   public $moveto_guest_user_checkout; 
   public $moveto_postalcode;       
   public $moveto_cabs;       
   public $moveto_country_flag;       
   public $moveto_show_provider;
   public $moveto_show_provider_avatars;
   public $moveto_show_services;
   public $moveto_show_service_desc;
   public $moveto_show_coupons;
   public $moveto_hide_booked_slot;
   public $moveto_cart;
   public $moveto_max_cartitem_limit;
   public $moveto_reviews_status;
   public $moveto_auto_confirm_reviews;
   public $moveto_frontend_custom_css;
   
   public $moveto_countrycodes_withflags;    
   public $moveto_default_country_short_code;
   public $moveto_thankyou_page;
   public $moveto_thankyou_page_rdtime;
   public $moveto_frontend_loader;
   public $moveto_country_flags;
   public $moveto_version;
   
 
   
   /* moveto General Settings */
   public $moveto_multi_location;
   public $moveto_zipcode_booking;
   public $moveto_booking_zipcodes;
   public $moveto_booking_time_interval;
   public $moveto_minimum_advance_booking;
   public $moveto_maximum_advance_booking;
   public $moveto_booking_padding_time;
   public $moveto_cancellation_buffer_time;
   public $moveto_reschedule_buffer_time;
   public $moveto_currency;
   public $moveto_currency_symbol;
   public $moveto_currency_symbol_position;
   public $moveto_price_format_decimal_places;
   public $moveto_price_format_comma_separator;      
   public $moveto_multiple_booking_sameslot;  	
   public $moveto_slot_max_booking_limit;
   public $moveto_appointment_auto_confirm;
   public $moveto_dayclosing_overlap;   
   public $moveto_client_as_wordpress_user_role;
   public $moveto_taxvat_status;
   public $moveto_taxvat_type;
   public $moveto_taxvat_amount;
   public $moveto_pd_type;
   public $moveto_partial_deposit_amount;
   public $moveto_partial_deposit_type;
   public $moveto_partial_deposit_status;
   public $moveto_partial_deposit_message;
   public $moveto_location_sortby;
   public $booking_cart_description;
   public $moveto_datepicker_format;
   public $moveto_cancelation_policy_status;
   public $moveto_cancelation_policy_header;
   public $moveto_cancelation_policy_text;
  
   public $moveto_allow_terms_and_conditions;
   public $moveto_allow_terms_and_conditions_url;
   public $moveto_allow_privacy_policy;
   public $ct_privacy_policy_url;
   public $moveto_thankyou_page_message;
	
    /*Company Settings*/
	public $moveto_company_name;
	public $moveto_company_email;
	public $moveto_company_address;
	public $moveto_company_city;
	public $moveto_company_state;
	public $moveto_company_zip;
	public $moveto_company_country;
	public $moveto_company_logo;
	public $moveto_company_country_code;
	public $moveto_company_phone;
	public $default_company_country_flag;
	
  
	/* Payment Settings */
	public $moveto_payment_gateways_status; 
	public $moveto_locally_payment_status;  
  public $moveto_payment_method_Paypal;
  public $moveto_payment_method_Stripe;
	public $moveto_payment_method_2Checkout;
	public $moveto_payment_method_Authorizenet; 
	public $moveto_payment_method_Paystack; 
   /* Paypal */
   public $moveto_paypal_title;
   public $moveto_paypal_direct_cc_dc_payment;
   public $moveto_paypal_description;
   public $moveto_paypal_merchant_email;
   public $moveto_paypal_api_username;
   public $moveto_paypal_api_password;
   public $moveto_paypal_api_signature;
   public $moveto_paypal_testing_mode; 
   public $moveto_paypal_guest_checkout;   
   /* Stripe */
   public $moveto_stripe_secretKey;
   public $moveto_stripe_publishableKey; 
	 
	  /* Paystack */
   public $moveto_paystack_public_key;
   public $moveto_paystack_secret_key; 
	 
   /* Authorize.Net */
   public $moveto_authorizenet_title;
   public $moveto_authorizenet_desc;
   public $moveto_authorizenet_api_loginid;
   public $moveto_authorizenet_transaction_key;
   public $moveto_authorizenet_testing_mode;
    /* 2Checkout */
	public $moveto_2checkout_publishablekey;
	public $moveto_2checkout_privateKey;
	public $moveto_2checkout_sellerid;
	public $moveto_2checkout_testing_mode;
	
	/* Payumoney */
	public $moveto_payumoney_testing_mode;
	public $moveto_payment_method_Payumoney;
	public $moveto_payumoney_merchantkey;
	public $moveto_payumoney_saltkey;
	
	/* Paytm */
	public $moveto_paytm_testing_mode;
	public $moveto_payment_method_Paytm;
	public $moveto_paytm_merchantkey;
	public $moveto_paytm_merchantid;
	public $moveto_paytm_website;
	public $moveto_paytm_industryid;
	public $moveto_paytm_channelid;
	
	/* Email Settings */
	public $moveto_email_sender_name;
	public $moveto_email_sender_address;
	public $moveto_admin_email_notification_status;
	public $moveto_manager_email_notification_status;
	public $moveto_service_provider_email_notification_status;
	public $moveto_client_email_notification_status;
	public $moveto_email_reminder_buffer;	
	
   /* SMS Reminder Settings */
   public $moveto_sms_reminder_status;
   
   public $moveto_sms_noti_twilio;
   public $moveto_twilio_sid;
   public $moveto_twilio_auth_token;
   public $moveto_twilio_number;
   public $moveto_twilio_client_sms_notification_status;
   public $moveto_twilio_service_provider_sms_notification_status;
   public $moveto_twilio_admin_sms_notification_status;
   public $moveto_twilio_admin_phone_no;
   public $moveto_twilio_ccode;
   public $moveto_twilio_ccode_alph;

   
   public $moveto_sms_noti_plivo;
   public $moveto_plivo_number;
   public $moveto_plivo_sid;
   public $moveto_plivo_auth_token;
   public $moveto_plivo_service_provider_sms_notification_status;
   public $moveto_plivo_client_sms_notification_status;
   public $moveto_plivo_admin_sms_notification_status;
   public $moveto_plivo_admin_phone_no;
   public $moveto_plivo_ccode;
   public $moveto_plivo_ccode_alph;
   
   
   public $moveto_sms_noti_nexmo;
   public $moveto_nexmo_apikey;
   public $moveto_nexmo_api_secret;
   public $moveto_nexmo_form;
   public $moveto_nexmo_send_sms_client_status;
   public $moveto_nexmo_send_sms_sp_status;
   public $moveto_nexmo_send_sms_admin_status;
   public $moveto_nexmo_admin_phone_no;
   public $moveto_nexmo_ccode;
   public $moveto_nexmo_ccode_alph;
   public $moveto_sms_noti_textlocal;
   public $moveto_textlocal_apikey;
   public $moveto_textlocal_sender;
   public $moveto_textlocal_service_provider_sms_notification_status;
   public $moveto_textlocal_client_sms_notification_status;
   public $moveto_textlocal_admin_sms_notification_status;
   public $moveto_textlocal_admin_phone_no;
   public $moveto_textlocal_ccode;
   public $moveto_textlocal_ccode_alph;
   public $moveto_max_distance;
   public $moveto_google_api_key;
   public $moveto_distance_unit;
   public $moveto_tax;
   public $moveto_google_map_country;
   public $moveto_google_map_center_restrictions;
   public $moveto_company_origin_destination_distance_status;
   public $moveto_destination_company_distance;
   public $moveto_company_origin_address;
   public $moveto_company_destination_address;
   public $moveto_discount_type;
   public $moveto_discount;
   public $moveto_min_rate_service;
   public $moveto_insurance_rate;
   public $moveto_insurance_permission;
   public $move_pricing_status;
   public $moveto_cubic_meter;
   public $moveto_sample_status;


   
   /* Constructor Function to set the default values */
  public function __construct() {
			
			if(!get_option('moveto_booking_time_interval')) {
					$admin_email = get_option('admin_email');
					$moveto_options = array(					
					   /* Appearance Settings */
					   'moveto_version'=>'4.1',
					   'moveto_primary_color'=>'#fd3943',
					   'moveto_secondary_color'=>'#fd3943',
					   'moveto_text_color'=>'#3d3d3d',
					   'moveto_bg_text_color'=>'#FFFFFF',  
					   'moveto_admin_color_primary'=>'#232833',					   
					   'moveto_admin_color_secondary'=>'#fd3943',					   
					   'moveto_admin_color_text'=>'#3d3d3d',					   
					   'moveto_admin_color_bg_text'=>'#FFFFFF',					   
					   'moveto_firststep_indications'=>'E',
					   'moveto_datepicker_format'=>'m-d-Y',					  					 
					   'moveto_guest_user_checkout'=>'E',
					   'moveto_postalcode'=>'E',
					   'moveto_cabs'=>'D',
					   'moveto_country_flag'=>'E',
					   'moveto_single_column_view'=>'D',	
					   'moveto_timeslots_legends'=>'E',
					   'moveto_countrycodes_withflags'=>'E',
					   'moveto_default_country_short_code'=>'us',
					   
										
						'moveto_show_provider'=>'E',
						'moveto_show_provider_avatars'=>'E',
						'moveto_show_services'=>'E',
						'moveto_show_service_desc'=>'E',
						'moveto_show_coupons'=>'E',
						'moveto_hide_booked_slot'=>'E',
						'moveto_cart'=>'E',
						'moveto_max_cartitem_limit'=>'5',
						'moveto_reviews_status'=>'D',
						'moveto_auto_confirm_reviews'=>'D',
						'moveto_frontend_custom_css'=>'',
						'moveto_frontend_loader'=>'',
						'moveto_country_flags'=>'',
						
						/* Default General Settings */
						'moveto_multi_location'=>'E',
						'moveto_zipcode_booking'=>'D',
						'moveto_booking_zipcodes'=>'',
						'moveto_booking_time_interval'=>'30',
						'moveto_minimum_advance_booking'=>'360',
						'moveto_maximum_advance_booking'=>'6',
						'moveto_booking_padding_time'=>'',
						'moveto_cancellation_buffer_time'=>'',
						'moveto_reschedule_buffer_time'=>'',
						'moveto_currency'=>'USD',
						'moveto_currency_symbol'=>'$',
						'moveto_currency_symbol_position'=>'B',
						'moveto_price_format_decimal_places'=>'2',						
						'moveto_price_format_comma_separator'=>'N',						
						'moveto_multiple_booking_sameslot'=>'E',									
						'moveto_slot_max_booking_limit'=>'0',						
						'moveto_appointment_auto_confirm'=>'D',
						'moveto_dayclosing_overlap'=>'D',
						'moveto_thankyou_page'=>'',
						'moveto_thankyou_page_message'=>'Your mover request is submitted successfully,  You will be notified with quote price soon',
						'moveto_thankyou_page_rdtime'=>'5000',
						'moveto_main_container_background'=>'transparent linear-gradient(to right, #EDEDED 0%, rgba(237, 237, 237, 0.72) 50%, #F6F6F6 50%, #F6F6F6 100%) repeat scroll 0% 0% !important',
						'moveto_client_as_wordpress_user_role'=>'appointment_client',
						'moveto_taxvat_status'=>'D',
						'moveto_taxvat_type'=>'P',
						'moveto_taxvat_amount'=>'',
						'moveto_pd_type'=>'P',
						'moveto_partial_deposit_amount'=>'',
						'moveto_partial_deposit_type'=>'P',
						'moveto_partial_deposit_status'=>'D',
						'moveto_partial_deposit_message'=>'You only need to pay a deposit to confirm your booking. The remaining amount needs to be paid on arrival.',
						'moveto_location_sortby'=>'state',
						'booking_cart_description'=>'E',
						'moveto_cancelation_policy_status'=>'D',
						'moveto_cancelation_policy_header'=>'',
						'moveto_cancelation_policy_text'=>'',
						'moveto_allow_terms_and_conditions'=>'D',
						'moveto_allow_terms_and_conditions_url'=>'',
						'moveto_allow_privacy_policy'=>'D',
						'moveto_allow_privacy_policy_url'=>'',
						
												
						/* Default Company Settings */
						'moveto_company_name'=>'',
						'moveto_company_email'=>'',
						'moveto_company_address'=>'',
						'moveto_company_city'=>'',
						'moveto_company_state'=>'',
						'moveto_company_zip'=>'',
						'moveto_company_country'=>'',
						'moveto_company_logo'=>'',
						'moveto_company_country_code'=>'+1',
						'moveto_company_phone'=>'',
						'default_company_country_flag'=>'us',

						
						/* Payment Settings */
						'moveto_payment_method_Paypal'=>'D', 
						'moveto_payment_method_Stripe'=>'D',						
						'moveto_payment_method_Authorizenet'=>'D',
						'moveto_locally_payment_status'=>'E',
						'moveto_payment_gateways_status'=>'D',	
						'moveto_payment_method_2Checkout'=>'D',
						'moveto_payment_method_Paytm'=>'D',
						'moveto_payment_method_Paystack'=>'D',
						
						/* Paypal */
						'moveto_paypal_direct_cc_dc_payment'=>'N',
						'moveto_paypal_title'=>'Paypal',
						'moveto_paypal_description'=>'you can pay with your credit card if you don\'t have a paypal account',
						'moveto_paypal_merchant_email'=>'you@youremail.com',
						'moveto_paypal_testing_mode'=>'D',						
						'moveto_paypal_guest_checkout'=>'D',						
						'moveto_paypal_api_username'=>'',
						'moveto_paypal_api_password'=>'',
						'moveto_paypal_api_signature'=>'',						
						/* Stripe */
						'moveto_stripe_secretKey'=>'',
						'moveto_stripe_publishableKey'=>'',		
						/* Paystack */
						
						'moveto_paystack_public_key'=>'',
						'moveto_paystack_secret_key'=>'',			
						/* Authorize.Net */
						'moveto_authorizenet_title'=>'Authorize.Net',
						'moveto_authorizenet_desc'=>'',
						'moveto_authorizenet_api_loginid'=>'',
						'moveto_authorizenet_transaction_key'=>'',
						'moveto_authorizenet_testing_mode'=>'D',
						/* 2Checkout */
						'moveto_2checkout_publishablekey'=>'',
						'moveto_2checkout_privateKey'=>'',
						'moveto_2checkout_sellerid'=>'',
						'moveto_2checkout_testing_mode'=>'D',
						/* Payumoney */
						'moveto_payment_method_Payumoney'=>'D',
						'moveto_payumoney_merchantkey'=>'',
						'moveto_payumoney_saltkey'=>'',
						'moveto_payumoney_testing_mode'=>'D',
						
						/* Paytm */
						'moveto_payment_method_Paytm'=>'D',
						'moveto_paytm_merchantkey'=>'',
						'moveto_paytm_merchantid'=>'',
						'moveto_paytm_website'=>'',
						'moveto_paytm_channelid'=>'',
						'moveto_paytm_industryid'=>'',
						'moveto_paytm_testing_mode'=>'D',
						
						/* Email Settings */ 
						'moveto_email_sender_name'=>'Moveto',
						'moveto_email_sender_address'=>$admin_email,
						'moveto_admin_email_notification_status'=>'E',
						'moveto_manager_email_notification_status'=>'E',
						'moveto_service_provider_email_notification_status'=>'E',
						'moveto_client_email_notification_status'=>'E',
						'moveto_email_reminder_buffer'=>'',
						/* SMS Reminder Settings */
						'moveto_sms_reminder_status'=>'D',
						
						'moveto_sms_noti_twilio'=>'D',
						'moveto_twilio_number'=>'',
						'moveto_twilio_sid'=>'',
						'moveto_twilio_auth_token'=>'',
						'moveto_twilio_client_sms_notification_status'=>'D',
						'moveto_twilio_service_provider_sms_notification_status'=>'D',
						'moveto_twilio_admin_sms_notification_status'=>'D',
						'moveto_twilio_admin_phone_no'=>'',
						'moveto_twilio_ccode'=>'+1',
						'moveto_twilio_ccode_alph'=>'us',
			
						'moveto_sms_noti_plivo'=>'D',
						'moveto_plivo_number'=>'',
						'moveto_plivo_sid'=>'',
						'moveto_plivo_auth_token'=>'',
						'moveto_plivo_service_provider_sms_notification_status'=>'D',
						'moveto_plivo_client_sms_notification_status'=>'D',
						'moveto_plivo_admin_sms_notification_status'=>'D',
						'moveto_plivo_admin_phone_no'=>'',
						'moveto_plivo_ccode'=>'+1',
						'moveto_plivo_ccode_alph'=>'us',

						'moveto_sms_noti_nexmo'=>'D',
						'moveto_nexmo_apikey'=>'',
						'moveto_nexmo_api_secret'=>'',
						'moveto_nexmo_form'=>'',
						'moveto_nexmo_send_sms_client_status'=>'D',
						'moveto_nexmo_send_sms_sp_status'=>'D',
						'moveto_nexmo_send_sms_admin_status'=>'D',
						'moveto_nexmo_admin_phone_no'=>'',
						'moveto_nexmo_ccode'=>'+1',
						
						'moveto_sms_noti_textlocal'=>'D',
						'moveto_textlocal_apikey'=>'',
						'moveto_textlocal_sender'=>'',
						'moveto_textlocal_service_provider_sms_notification_status'=>'D',
						'moveto_textlocal_client_sms_notification_status'=>'D',
						'moveto_textlocal_admin_sms_notification_status'=>'D',
						'moveto_textlocal_admin_phone_no'=>'',
						'moveto_textlocal_ccode'=>'+1',
						'moveto_textlocal_ccode_alph'=>'us',
						'moveto_distance_unit'=>'',
						'moveto_google_api_key'=>'',
						'moveto_max_distance'=>'10000',
						'moveto_tax'=>'',
						'moveto_google_map_country'=>'',
						'moveto_google_map_center_restrictions'=>'',
						'moveto_company_origin_destination_distance_status'=>'D',
						'moveto_destination_company_distance'=>'D',
						'moveto_company_origin_address'=>'',
						'moveto_company_destination_address'=>'',
						'moveto_discount_type'=>'',
						'moveto_discount'=>'',
						'moveto_min_rate_service'=>'',
						'moveto_insurance_rate'=>'3',
						'moveto_insurance_permission'=>'0',
						'move_pricing_status'=>'Y',
						'moveto_cubic_meter'=>'ft_3',
						'moveto_sample_status'=>'Y',
						

						/* GC Settings */
						
						'mp_gc_status'=>'N',
						'mp_gc_two_way_sync_status'=>'N',
						'mp_gc_token'=>'',
						'mp_gc_id'=>'',
						'mp_gc_client_id'=>'',
						'mp_gc_client_secret'=>'',
						'mp_gc_frontend_url'=>'',
						'mp_gc_admin_url'=>''
						
					);	
						
					foreach($moveto_options as $option_key => $option_value) {
						add_option($option_key,$option_value);
					}
			
			}
   
   }
   

	
	/** ReadAll Settings **/
	function readAll(){
	
			/** Default Appearance Settings **/
			$this->moveto_primary_color = get_option('moveto_primary_color');
			$this->moveto_secondary_color = get_option('moveto_secondary_color');
			$this->moveto_text_color = get_option('moveto_text_color');
			$this->moveto_bg_text_color = get_option('moveto_bg_text_color');					
			$this->moveto_admin_color_primary = get_option('moveto_admin_color_primary');
			$this->moveto_admin_color_secondary = get_option('moveto_admin_color_secondary');
			$this->moveto_admin_color_text = get_option('moveto_admin_color_text');
			$this->moveto_admin_color_bg_text = get_option('moveto_admin_color_bg_text');
			$this->moveto_guest_user_checkout = get_option('moveto_guest_user_checkout');	
			$this->moveto_postalcode = get_option('moveto_postalcode');	
			$this->moveto_cabs = get_option('moveto_cabs');	
			$this->moveto_country_flag = get_option('moveto_country_flag');	
			$this->moveto_show_provider = get_option('moveto_show_provider');
			$this->moveto_show_provider_avatars = get_option('moveto_show_provider_avatars');
			$this->moveto_show_services = get_option('moveto_show_services');
			$this->moveto_show_service_desc = get_option('moveto_show_service_desc');
			$this->moveto_show_coupons = get_option('moveto_show_coupons');				
			$this->moveto_hide_booked_slot = get_option('moveto_hide_booked_slot');
			
			$this->moveto_countrycodes_withflags = get_option('moveto_countrycodes_withflags');				
			$this->moveto_default_country_short_code = get_option('moveto_default_country_short_code');	
			$this->moveto_cart = get_option('moveto_cart');	
			$this->moveto_max_cartitem_limit = get_option('moveto_max_cartitem_limit');	
			$this->moveto_reviews_status = get_option('moveto_reviews_status');	
			$this->moveto_auto_confirm_reviews = get_option('moveto_auto_confirm_reviews');	
			$this->moveto_frontend_custom_css = get_option('moveto_frontend_custom_css');	
			$this->moveto_frontend_loader = get_option('moveto_frontend_loader');	
			$this->moveto_country_flags = get_option('moveto_country_flags');	
			/*** End ***/
			
			/* General Settings */
			$this->moveto_multi_location = get_option('moveto_multi_location');
			$this->moveto_zipcode_booking = get_option('moveto_zipcode_booking');
			$this->moveto_booking_zipcodes = get_option('moveto_booking_zipcodes');
			$this->moveto_booking_time_interval = get_option('moveto_booking_time_interval');
			$this->moveto_minimum_advance_booking = get_option('moveto_minimum_advance_booking');
			$this->moveto_maximum_advance_booking = get_option('moveto_maximum_advance_booking');
			$this->moveto_booking_padding_time = get_option('moveto_booking_padding_time');
			$this->moveto_cancellation_buffer_time = get_option('moveto_cancellation_buffer_time');
			$this->moveto_reschedule_buffer_time = get_option('moveto_reschedule_buffer_time');
			$this->moveto_currency = get_option('moveto_currency');
			$this->moveto_currency_symbol = get_option('moveto_currency_symbol');
			$this->moveto_currency_symbol_position = get_option('moveto_currency_symbol_position');
			$this->moveto_price_format_decimal_places = get_option('moveto_price_format_decimal_places');
			$this->moveto_price_format_comma_separator = get_option('moveto_price_format_comma_separator');
			$this->moveto_multiple_booking_sameslot = get_option('moveto_multiple_booking_sameslot');	
			$this->moveto_slot_max_booking_limit = get_option('moveto_slot_max_booking_limit');
			$this->moveto_appointment_auto_confirm = get_option('moveto_appointment_auto_confirm');
			$this->moveto_dayclosing_overlap = get_option('moveto_dayclosing_overlap');
			$this->moveto_thankyou_page = get_option('moveto_thankyou_page');
			$this->moveto_thankyou_page_rdtime = get_option('moveto_thankyou_page_rdtime');
			$this->moveto_main_container_background = get_option('moveto_main_container_background');	
			$this->moveto_taxvat_status = get_option('moveto_taxvat_status');
			$this->moveto_pd_type = get_option('moveto_pd_type');
			$this->moveto_thankyou_page_message = get_option('moveto_thankyou_page_message');
			$this->moveto_partial_deposit_amount = get_option('moveto_partial_deposit_amount');
			$this->moveto_partial_deposit_type= get_option('moveto_partial_deposit_type');
			$this->moveto_partial_deposit_status= get_option('moveto_partial_deposit_status');
			$this->moveto_partial_deposit_message= get_option('moveto_partial_deposit_message');
			$this->moveto_taxvat_type = get_option('moveto_taxvat_type');
			$this->moveto_taxvat_amount = get_option('moveto_taxvat_amount');
			$this->moveto_location_sortby = get_option('moveto_location_sortby');
			$this->booking_cart_description = get_option('booking_cart_description');
			$this->moveto_datepicker_format = get_option('moveto_datepicker_format');
			$this->moveto_cancelation_policy_status = get_option('moveto_cancelation_policy_status');
			$this->moveto_cancelation_policy_header = get_option('moveto_cancelation_policy_header');
			$this->moveto_cancelation_policy_text = get_option('moveto_cancelation_policy_text');
			$this->moveto_allow_terms_and_conditions = get_option('moveto_allow_terms_and_conditions');
			
			$this->moveto_allow_terms_and_conditions_url = get_option('moveto_allow_terms_and_conditions_url');
			$this->moveto_allow_privacy_policy = get_option('moveto_allow_privacy_policy');
			$this->moveto_allow_privacy_policy_url = get_option('moveto_allow_privacy_policy_url');
						
			/*** End ***/
				
			/** Company Settings **/
			$this->moveto_company_name = get_option('moveto_company_name');
			$this->moveto_company_email = get_option('moveto_company_email');
			$this->moveto_company_address = get_option('moveto_company_address');
			$this->moveto_company_city = get_option('moveto_company_city');
			$this->moveto_company_state = get_option('moveto_company_state');
			$this->moveto_company_zip = get_option('moveto_company_zip');
			$this->moveto_company_country = get_option('moveto_company_country');
			$this->moveto_company_logo = get_option('moveto_company_logo');
			$this->moveto_company_country_code = get_option('moveto_company_country_code');
			$this->moveto_company_phone = get_option('moveto_company_phone');
			$this->default_company_country_flag = get_option('default_company_country_flag');
			/*** End ***/
				
			/** Payment Settings **/
			$this->moveto_payment_method_Paypal = get_option('moveto_payment_method_Paypal');
			$this->moveto_payment_method_Stripe = get_option('moveto_payment_method_Stripe');
			$this->moveto_payment_method_Authorizenet = get_option('moveto_payment_method_Authorizenet');
			$this->moveto_payment_method_2Checkout = get_option('moveto_payment_method_2Checkout');
			$this->moveto_locally_payment_status= get_option('moveto_locally_payment_status');
			$this->moveto_payment_gateways_status= get_option('moveto_payment_gateways_status');			
			$this->moveto_payment_method_Payumoney= get_option('moveto_payment_method_Payumoney');			
			$this->moveto_payment_method_Paytm= get_option('moveto_payment_method_Paytm');			
			$this->moveto_payment_method_Paystack= get_option('moveto_payment_method_Paystack');			
			//Paypal
			$this->moveto_paypal_direct_cc_dc_payment = get_option('moveto_paypal_direct_cc_dc_payment');
			$this->moveto_paypal_title = get_option('moveto_paypal_title');
			$this->moveto_paypal_description = get_option('moveto_paypal_description');
			$this->moveto_paypal_merchant_email = get_option('moveto_paypal_merchant_email');
			$this->moveto_paypal_testing_mode = get_option('moveto_paypal_testing_mode');
			$this->moveto_paypal_guest_checkout = get_option('moveto_paypal_guest_checkout');			
			$this->moveto_paypal_api_username = get_option('moveto_paypal_api_username');
			$this->moveto_paypal_api_password = get_option('moveto_paypal_api_password');
			$this->moveto_paypal_api_signature = get_option('moveto_paypal_api_signature');		
			//Stripe
			$this->moveto_stripe_secretKey =  get_option('moveto_stripe_secretKey');
			$this->moveto_stripe_publishableKey =  get_option('moveto_stripe_publishableKey');	

			//Paystack
			$this->moveto_paystack_public_key =  get_option('moveto_paystack_public_key');
			$this->moveto_paystack_secret_key =  get_option('moveto_paystack_secret_key');		
			
			//Authorize.Net
			$this->moveto_authorizenet_title = get_option('moveto_authorizenet_title');
			$this->moveto_authorizenet_desc = get_option('moveto_authorizenet_desc');
			$this->moveto_authorizenet_api_loginid = get_option('moveto_authorizenet_api_loginid');
			$this->moveto_authorizenet_transaction_key = get_option('moveto_authorizenet_transaction_key');
			$this->moveto_authorizenet_testing_mode = get_option('moveto_authorizenet_testing_mode');
			/* 2Checkout */		
			$this->moveto_2checkout_publishablekey = get_option('moveto_2checkout_publishablekey');
			$this->moveto_2checkout_privateKey = get_option('moveto_2checkout_privateKey');
			$this->moveto_2checkout_sellerid = get_option('moveto_2checkout_sellerid');
			$this->moveto_2checkout_testing_mode = get_option('moveto_2checkout_testing_mode');
			/* Payumoney */
			$this->moveto_payumoney_merchantkey = get_option('moveto_payumoney_merchantkey');
			$this->moveto_payumoney_saltkey = get_option('moveto_payumoney_saltkey');
			$this->moveto_payumoney_testing_mode = get_option('moveto_payumoney_testing_mode');
			/* Paytm */
			$this->moveto_paytm_merchantkey = get_option('moveto_paytm_merchantkey');
			$this->moveto_paytm_merchantid = get_option('moveto_paytm_merchantid');
			$this->moveto_paytm_website = get_option('moveto_paytm_website');
			$this->moveto_paytm_channelid = get_option('moveto_paytm_channelid');
			$this->moveto_paytm_industryid = get_option('moveto_paytm_industryid');
			$this->moveto_paytm_testing_mode = get_option('moveto_paytm_testing_mode');

			/* Email Settings */
			$this->moveto_admin_eamil_address = get_option('moveto_admin_eamil_address');
			$this->moveto_email_sender_name = get_option('moveto_email_sender_name');
			$this->moveto_email_sender_address = get_option('moveto_email_sender_address');
			$this->moveto_admin_email_notification_status = get_option('moveto_admin_email_notification_status');
			$this->moveto_manager_email_notification_status = get_option('moveto_manager_email_notification_status');			
			$this->moveto_service_provider_email_notification_status = get_option('moveto_service_provider_email_notification_status');
			$this->moveto_client_email_notification_status = get_option('moveto_client_email_notification_status');
			$this->moveto_email_reminder_buffer = get_option('moveto_email_reminder_buffer'); 
			
		
			/* Social Login Settings */
			$this->moveto_fb_social_login_status = get_option('moveto_fb_social_login_status');
			$this->moveto_fb_appid = get_option('moveto_fb_appid');
			$this->moveto_fb_appsecret = get_option('moveto_fb_appsecret');
			
			/* SMS Reminder Settings */
			$this->moveto_sms_reminder_status = get_option('moveto_sms_reminder_status');

			$this->moveto_sms_noti_twilio = get_option('moveto_sms_noti_twilio');
			$this->moveto_twilio_number = get_option('moveto_twilio_number');
			$this->moveto_twilio_sid = get_option('moveto_twilio_sid');
			$this->moveto_twilio_auth_token = get_option('moveto_twilio_auth_token');
			$this->moveto_twilio_client_sms_notification_status = get_option('moveto_twilio_client_sms_notification_status');
			$this->moveto_twilio_service_provider_sms_notification_status = get_option('moveto_twilio_service_provider_sms_notification_status');
			$this->moveto_twilio_admin_sms_notification_status = get_option('moveto_twilio_admin_sms_notification_status');
			$this->moveto_twilio_admin_phone_no = get_option('moveto_twilio_admin_phone_no');
			$this->moveto_twilio_ccode = get_option('moveto_twilio_ccode');
			$this->moveto_twilio_ccode_alph = get_option('moveto_twilio_ccode_alph');
						
			$this->moveto_sms_noti_plivo = get_option('moveto_sms_noti_plivo');
			$this->moveto_plivo_number = get_option('moveto_plivo_number');
			$this->moveto_plivo_sid = get_option('moveto_plivo_sid');
			$this->moveto_plivo_auth_token = get_option('moveto_plivo_auth_token');
			$this->moveto_plivo_service_provider_sms_notification_status = get_option('moveto_plivo_service_provider_sms_notification_status');
			$this->moveto_plivo_client_sms_notification_status = get_option('moveto_plivo_client_sms_notification_status');
			$this->moveto_plivo_admin_sms_notification_status = get_option('moveto_plivo_admin_sms_notification_status');
			$this->moveto_plivo_admin_phone_no = get_option('moveto_plivo_admin_phone_no');
			$this->moveto_plivo_ccode = get_option('moveto_plivo_ccode');
			$this->moveto_plivo_ccode_alph = get_option('moveto_plivo_ccode_alph');

			$this->moveto_sms_noti_nexmo = get_option('moveto_sms_noti_nexmo');
			$this->moveto_nexmo_apikey = get_option('moveto_nexmo_apikey');
			$this->moveto_nexmo_api_secret = get_option('moveto_nexmo_api_secret');
			$this->moveto_nexmo_form = get_option('moveto_nexmo_form');
			$this->moveto_nexmo_send_sms_client_status = get_option('moveto_nexmo_send_sms_client_status');
			$this->moveto_nexmo_send_sms_sp_status = get_option('moveto_nexmo_send_sms_sp_status');
			$this->moveto_nexmo_send_sms_admin_status = get_option('moveto_nexmo_send_sms_admin_status');
			$this->moveto_nexmo_admin_phone_no = get_option('moveto_nexmo_admin_phone_no');
			$this->moveto_nexmo_ccode = get_option('moveto_nexmo_ccode');
			$this->moveto_nexmo_ccode_alph = get_option('moveto_nexmo_ccode_alph');
			
			$this->moveto_sms_noti_textlocal = get_option('moveto_sms_noti_textlocal');
			$this->moveto_textlocal_apikey = get_option('moveto_textlocal_apikey');
			$this->moveto_textlocal_sender = get_option('moveto_textlocal_sender');
			$this->moveto_textlocal_service_provider_sms_notification_status = get_option('moveto_textlocal_service_provider_sms_notification_status');
			$this->moveto_textlocal_client_sms_notification_status = get_option('moveto_textlocal_client_sms_notification_status');
			$this->moveto_textlocal_admin_sms_notification_status = get_option('moveto_textlocal_admin_sms_notification_status');
			$this->moveto_textlocal_admin_phone_no = get_option('moveto_textlocal_admin_phone_no');
			$this->moveto_textlocal_ccode = get_option('moveto_textlocal_ccode');
			$this->moveto_textlocal_ccode_alph = get_option('moveto_textlocal_ccode_alph');
			$this->moveto_max_distance = get_option('moveto_max_distance');
			$this->moveto_google_api_key = get_option('moveto_google_api_key');
			$this->moveto_distance_unit = get_option('moveto_distance_unit');
			$this->moveto_tax = get_option('moveto_tax');
			$this->moveto_google_map_country = get_option('moveto_google_map_country');
			$this->moveto_google_map_center_restrictions = get_option('moveto_google_map_center_restrictions');
			$this->moveto_company_origin_destination_distance_status = get_option('moveto_company_origin_destination_distance_status');
			$this->moveto_destination_company_distance = get_option('moveto_destination_company_distance');
			$this->moveto_company_destination_address = get_option('moveto_company_destination_address');
			$this->moveto_company_origin_address = get_option('moveto_company_origin_address');
			$this->moveto_discount_type = get_option('moveto_discount_type');
			$this->moveto_discount = get_option('moveto_discount');
			$this->moveto_min_rate_service = get_option('moveto_min_rate_service');
			$this->moveto_insurance_rate = get_option('moveto_insurance_rate');
			$this->moveto_insurance_permission = get_option('moveto_insurance_permission');
			$this->move_pricing_status = get_option('move_pricing_status');
			$this->moveto_cubic_meter = get_option('moveto_cubic_meter');
			$this->moveto_sample_status = get_option('moveto_sample_status');

	}
	

}
?>