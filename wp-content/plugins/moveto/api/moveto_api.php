<?php 
require_once('../../../../wp-config.php');
global $wpdb;

	header_remove('Access-Control-Allow-Origin');
	header('Access-Control-Allow-Origin: *'); 
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');

$_POST = json_decode( file_get_contents( 'php://input' ), true );


/* API for the admin login */
if(isset($_POST['action']) && $_POST['action'] == 'user_login'){
    $creds = array();
    $creds['user_login'] = $_POST['username'];
    $creds['user_password'] = $_POST['password'];
    $creds['remember'] = true;
	$user = wp_signon( $creds, false );
   
    if ( is_wp_error($user) ){   
        $invalid = ['status' => "false", "statuscode" => 404, 'response' => "Please check your username and password"];
		echo wp_send_json( $invalid,404 );
    }else{
    	$valid = ['status' => "true", "statuscode" => 200, 'response' => $user];
    	echo wp_send_json( $valid,200 );
    }
}

/* API for the list of quotation */
if(isset($_POST['action']) && $_POST['action'] == 'get_quotation_list'){
$result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_bookings order by lastmodify desc");
	if(empty($result)){
		$invalid = ['status' => "false", "statuscode" => 404, 'response' => "Data not found"];
		echo wp_send_json( $invalid,404 );
	}else{
		$arr  = array();

		foreach ($result as $value) {
			
			
		$value->source_city = base64_decode($value->source_city);
		if($value->via_city){
			$value->via_city = base64_decode($value->via_city);
		}else{
			$value->via_city = "";
		}
		
		$value->destination_city = base64_decode($value->destination_city);
		array_push($arr, $value);
		}
		
		$valid = ['status' => "true", "statuscode" => 200, 'response' => $arr];
    	echo wp_send_json( $valid,200 );
	}

}



/* API for the quotation details for the single quote */
if(isset($_POST['action']) && $_POST['action'] == 'get_quotation_detail'){
	$order_id = $_POST['order_id'];
$result = $wpdb->get_results("SELECT * FROM  ".$wpdb->prefix."mp_bookings 	where order_id=".$order_id );	
	if(empty($result)){
		$invalid = ['status' => "false", "statuscode" => 404, 'response' => "Data not found"];
		echo wp_send_json( $invalid,404 );
	}else{
		$result[0]->source_city = base64_decode($result[0]->source_city);
		if($result[0]->via_city){
			$result[0]->via_city = base64_decode($result[0]->via_city);
		}else{
			$result[0]->via_city = "";
		}
		$result[0]->destination_city = base64_decode($result[0]->destination_city);
		
		$valid = ['status' => "true", "statuscode" => 200, 'response' => $result];
    	echo wp_send_json( $valid,200 );
	}

}

/* API for the send quote reply */
if(isset($_POST['action']) && $_POST['action'] == 'send_quote'){
	$quote_price = $_POST['quote_price'];
	$quote_details = $_POST['quote_comment'];
	$id = $_POST['id'];
	$queryString = "UPDATE  ".$wpdb->prefix."mp_bookings  SET quote_price='".$quote_price."',quote_detail='".$quote_detail."' WHERE id=".$id;
	$result= $wpdb->query($queryString);

	if ( is_wp_error($user) ){   
        $invalid = ['status' => "false", "statuscode" => 404, 'response' => "Something is wrong"];
		echo wp_send_json( $invalid,404 );
    }else{
    	$valid = ['status' => "true", "statuscode" => 200, 'response' => "Reply sent to customer"];
    	echo wp_send_json( $valid,200 );
    }

}








    



		
