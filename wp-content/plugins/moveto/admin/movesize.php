<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Location */
	$room_type = new moveto_room_type();
	$move_size = new moveto_size();
	$general = new moveto_general();
	$mp_image_upload= new moveto_image_upload();
	$mp_currency_symbol = get_option('moveto_currency_symbol');

?>
<div id="mp-services-panel" class="panel tab-content table-fixed">
<div id="mp-service-addon-panel" class="panel tab-content table-fixed">
<div class="panel-body">
				
<!-- 		*************************Movesize**************************** -->

<div class="mp-service-details tab-content col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<ul class="breadcrumb">
                <li><?php echo __("Move Size","mp"); ?></li>
            </ul>
			<div class="mp-service-top-header">
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					
				</div>
				<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					<button id="mp-add-new-service-addons" class="btn btn-success pull-right" value="add new service"><i class="fa fa-plus icon-space "></i><?php echo __("Add Move Size","mp");?></button>
				</div>
			</div>
			<div id="hr"></div>
			<div class="tab-pane active" id="">
				<div class="tab-content mp-services-right-details">
					<div class="tab-pane active col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="accordion" class="panel-group">
						<ul class="nav nav-tab nav-stacked" id="sortable-movesize" > <!-- sortable-services -->
							<?php 
							
							$mp_move_size = $move_size->readAll_movesize();							
							if(sizeof((array)$mp_move_size)>0){
							foreach($mp_move_size as $mp_move_sizes){  ?>
							<li id="service_detail_<?php echo $mp_move_sizes->id; ?>" class="panel panel-default mp-services-panel" >							
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="col-lg-9 col-sm-10 col-xs-12">
											<div class="pull-left">
												<i class="fa fa-th-list"></i>
											</div>	
											<span class="mp-service-title-name f-letter-capitalize"><?php echo $mp_move_sizes->name; ?></span>
											
											

										</div>
										<div class="col-lg-3 col-sm-2 col-xs-12 np">	
												<div class="mp-col6 np">
												</div>
												<div class="mp-col4 np">
													<label for="sevice-endis-<?php echo $mp_move_sizes->id; ?>">
														<input data-room_t_id="<?php echo $mp_move_sizes->id; ?>" type="checkbox" class="update_move_size_status" id="sevice-endis-<?php echo $mp_move_sizes->id; ?>" <?php if($mp_move_sizes->status=='E'){echo 'checked'; } ?> data-toggle="toggle" data-size="small" data-on="<?php echo __("Enable","mp");?>" data-off="<?php echo __("Disable","mp");?>" data-onstyle="success" data-offstyle="danger" >
													</label>
												</div>								
											<div class="pull-right">
												
												<div class="mp-col1 np">
												<a data-poid="mp-popover-delete-service<?php echo $mp_move_sizes->id; ?>" id="mp-delete-service<?php echo $mp_move_sizes->id; ?>" class="pull-right btn-circle btn-danger btn-sm mp-delete-popover" rel="popover" data-placement='bottom' title="<?php echo __("Delete This Move Size?","mp");?>"><i class="fa fa-trash" title="<?php echo __("Delete Addon","mp");?>"></i></a>
													<div class="mp-popover" id="mp-popover-delete-service<?php echo $mp_move_sizes->id; ?>" style="display: none;">
														<div class="arrow"></div>
														<table class="form-horizontal" cellspacing="0">
															<tbody>
																<tr>
																	<td>
																		<?php /* if($service->total_service_bookings()>0){ */if(0>0){?>
																		<span class="mp-popover-title"><?php echo __("Unable to delete article,having lead","mp");?></span>
																		<?php }else{?>		
																		<button data-id="<?php echo $mp_move_sizes->id; ?>" value="Delete" class="btn btn-danger btn-sm mr-10 delete_movesize" type="submit"><?php echo __("Yes","mp");?></button>
																		<button data-poid="mp-popover-addon<?php echo $mp_move_sizes->id; ?>" class="btn btn-default btn-sm mp-close-popover-delete" href="javascript:void(0)"><?php echo __("Cancel","mp");?></button><?php } ?>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>											
												<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" class="mp-show-hide-checkbox " id="<?php echo $mp_move_sizes->id; ?>" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="<?php echo $mp_move_sizes->id; ?>"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="package-detail" class="package-detail service_detail panel-collapse collapse detail-id_<?php echo $mp_move_sizes->id; ?>">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form data-sid="<?php echo $mp_move_sizes->id; ?>" id="mp_create_movesize_frm<?php echo $mp_move_sizes->id; ?>" method="post" type="" class="slide-toggle mp_create_movesize_frm"  >
												<table class="mp-create-service-table">
													<tbody>
														<input type="hidden" name="addon_service_id" id="addon_service_id" value="1" />
														<tr>
															<td><label for="mp-service-title<?php echo $mp_move_sizes->id; ?>"><?php echo __("Name","mp");?></label></td>
															<td><input type="text" name="movesize_title" class="form-control" id="movesize_title<?php echo $mp_move_sizes->id; ?>" value="<?php echo $mp_move_sizes->name; ?>" /></td>
														</tr>	
												<tr>
													<td></td>
													<td>
														<button data-update_move_sizes_id="<?php echo $mp_move_sizes->id; ?>" name="" class="btn btn-success mp-btn-width update_move_size" type="button"><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>	</tbody>
												</table>
											</form>
											
										</div>											
									</div>
								</div>
							</li>
							<?php }
							}else{
								echo __("No Move Size Found.","mp");
							} ?>
							<!-- add new service pop up -->
							<li>
							<div class="panel panel-default mp-services-panel mp-add-new-service-addons">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="mp-col6">
											<span class="mp-service-title-name"><?php echo __("Add New Move Size","mp");?></span>		
										</div>
										<div class="pull-right mp-col6">					
											<div class="pull-right">
													<div class="mp-show-hide pull-right">
													<input type="checkbox" name="mp-show-hide" checked="checked" class="mp-show-hide-checkbox" id="addservice" ><!--Added Serivce Id-->
													<label class="mp-show-hide-label" for="addservice"></label>
												</div>
											</div>
										</div>										
									</h4>
								</div>
								<div id="package-detail" class="package-detail service_detail panel-collapse collapse in detail-id_addservice">
									<div class="panel-body">
										<div class="mp-service-collapse-div col-sm-6 col-md-6 col-lg-6 col-xs-12">
											<form id="mp_create_movesize_frm" method="post" type="" class="slide-toggle" >
												<table class="mp-create-service-table">
													<tbody>
														<input type="hidden" name="addon_service_id" id="addon_service_id" value="1" />
														<tr>
															<td><label for="mp-service-title"><?php echo __("Name","mp");?></label></td>
															<td><input type="text" name="movesize_title" class="form-control" id="movesize_title" /></td>
														</tr>
														
												<tr>
													<td></td>
													<td>
														<button id="mp_create_movesize" name="mp_create_movesize" class="btn btn-success mp-btn-width col-md-offset-4" type="button" ><?php echo __("Save","mp");?></button>
														<button type="reset" class="btn btn-default mp-btn-width ml-30"><?php echo __("Reset","mp");?></button>
													</td>
												</tr>
										
										</tbody>
												</table>
											
										</div>
										
										</form>									
									</div>
								</div>
							</div>
							</li>
							
							</ul>
						</div>	
					</div>

				</div>
			</div>
			
		</div>
		<!-- **************************************************** -->
	</div>
</div>
</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
<script>
	var serviceObj={"plugin_path":"<?php echo $plugin_url_for_ajax;?>"}
</script>