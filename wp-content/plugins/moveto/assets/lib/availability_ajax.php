<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	$general = new moveto_general();
	$schedule_offdays = new moveto_schedule_offdays();
	$mp_currency_symbol = get_option('moveto_currency_symbol');
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));

/** Ajax Response Releated To Staff Offdays **/
if(isset($_POST['staff_action'],$_POST['staff_id']) && $_POST['staff_action']=='staff_add_offdays' && $_POST['staff_id']!=''){
		$schedule_offdays->provider_id = $_POST['staff_id'];
		if(isset($_POST['off_year_month'])) {
			$schedule_offdays->off_year_month = $_POST['off_year_month'];
			$schedule_offdays->create_monthoff();
		}else{ 
			$schedule_offdays->off_date = $_POST['off_date'];
			$schedule_offdays->create(); 
		} 			
} 
if(isset($_POST['staff_action'],$_POST['staff_id']) && $_POST['staff_action']=='staff_delete_offdays' && $_POST['staff_id']!=''){
		$schedule_offdays->provider_id = $_POST['staff_id'];
		if(isset($_POST['off_year_month'])) {
			$schedule_offdays->off_year_month = $_POST['off_year_month'];
			$schedule_offdays->delete_monthoff();
	    }else{ 
		   $schedule_offdays->off_date = $_POST['off_date'];
		   $schedule_offdays->delete_offday();
		} 			
} 
