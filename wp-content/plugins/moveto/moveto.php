<?php
/*
Plugin Name: MoveTo
Plugin URI: http://skymoonlabs.com/
Description: Moveto is price estimator tool for movers and moving companies. You can embed this quote form on your wordpress page and get leads from users and can send quote from admin area easily. Shortcode is [moveto] for quote form.
Version: 4.1
Author: Skymoonlabs
Author URI: http://skymoonlabs.com/
*/

	add_action('init', 'moveto_init');
	add_action( 'admin_enqueue_scripts', 'moveto_admin_scripts');
	add_action('admin_menu','moveto_admin_menu');
	add_filter('wp_head', 'viewport_meta_moveto');
	
	/* lower letters Capital Shortcode */
	add_shortcode('moveto','mp_front');
	add_shortcode('"moveto"','mp_front');
	add_shortcode("'moveto'",'mp_front');
	/* Capital letters Capital Shortcode */
	add_shortcode('MoveTo','mp_front');
	add_shortcode('"MoveTo"','mp_front');
	add_shortcode("'MoveTo'",'mp_front');


	/* Multi Language */
	add_action( 'plugins_loaded', 'moveto_load_textdomain' );
	add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'mp_action_links' );
	
	add_action('wp_ajax_check_username_bd','check_username_mp_callback');
	add_action('wp_ajax_check_email_bd','check_email_mp_callback');
	add_action('wp_ajax_check_route','check_route_callback');
	add_action('wp_ajax_check_generatecoupon_bd','check_generatecoupon_mp_callback');
	add_action( 'wp_ajax_nopriv_check_username_bd', 'check_username_mp_callback' );
	add_action( 'wp_ajax_nopriv_check_email_bd', 'check_email_mp_callback' );

	/* Activation Hook */
	 register_activation_hook( __FILE__, 'moveto_activate' ); 


	/* function view port meta in case its not defined */
	function viewport_meta_moveto() { ?>
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />
		<link rel="icon" href="<?php echo plugins_url("assets/images/icons/fevicon.png",__FILE__); ?>" type="image/png" size="16x16">
		<script src="<?php echo plugins_url("assets/js/popper.min.js",__FILE__); ?>" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<?php }
	
	/* set plugin textdomain */
	function moveto_load_textdomain() {
		$locale = apply_filters('plugin_locale', get_locale(),'mp');
		load_textdomain('mp', WP_LANG_DIR.'/mp-'.$locale.'.mo');
		load_plugin_textdomain( 'mp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
	}

	
	/* plugin settings link */
	function mp_action_links( $links ) {
	  // $links[] = '<a href="'. get_admin_url(null, 'options-general.php?page=settings_submenu') .'">Settings</a>';
	   return $links;
	}
	/* moveto Admin Menu Icon */
	function moveto_adminmenu_icon(){
		echo '<style>li#toplevel_page_moveto_menu .dashicons-admin-generic:before {
			content: "" !important;	background: url("'.plugins_url("assets/images/menu1.png",__FILE__).'") no-repeat;		position: relative;	top: 7px;}			
			li#toplevel_page_verify .dashicons-admin-generic:before {
			content: "" !important;	background: url("'.plugins_url("assets/images/menu1.png",__FILE__).'") no-repeat;		position: relative;	top: 7px;}
			li#toplevel_page_provider_submenu .dashicons-admin-generic:before {
			content: "" !important;	background: url("'.plugins_url("assets/images/menu1.png",__FILE__).'") no-repeat;		position: relative;	top: 7px;}</style><link rel="icon" href="" type="image/png" size="16x16">';
	}
	
   /* Plugin init function 
   */	
   function moveto_init(){	 		 
		global $wpdb;	  	

		/* Check plugin updtes */
		$today_date = date("Y-m-d");
		$first_month_date = date("Y-m-15");
		if($today_date == $first_month_date){
			include_once('objects/class_autoupdate.php');
			$wptuts_plugin_current_version = '4.1';
			$wptuts_plugin_remote_path = 'http://skymoonlabs.com/moveto/update.php?cv='.$wptuts_plugin_current_version;
			$wptuts_plugin_slug = plugin_basename(__FILE__);
			new moveto_auto_update ($wptuts_plugin_current_version, $wptuts_plugin_remote_path, $wptuts_plugin_slug);
		}
		/* Load moveto Admin Menu Icon */
		add_action( 'admin_print_scripts','moveto_adminmenu_icon');		
	  	/* error_reporting(0);			 		  */
		$host =  $_SERVER['HTTP_HOST'];		 
		$host_uri = $_SERVER['REQUEST_URI'];		 
		$cur_rul= $host.$host_uri;
		if(!session_id() || session_id() != '') { @session_start(); }		
	   if(isset($_SESSION['booking_home']) and $_SESSION['booking_home']!=''){			
		$redirect_url = $_SESSION['booking_home'];		 
	   } else {			
		$redirect_url = site_url();		 
	   }		 		 
	   
	   if(get_option('moveto_thankyou_page')==$cur_rul  || is_numeric(strpos($cur_rul,'mp-thankyou'))){   		   
		ob_start();
		echo '<script>setTimeout(function(){ window.location = "'.$redirect_url.'"; }, 5000);</script>';		 
	   }		 
		
					
			/* Thankyou page creation */
			
			$the_page_title = 'Thank you';
			$the_page_name = 'mp-thankyou';

			$the_page = get_page_by_title( $the_page_title );

				if ( ! $the_page ) {

				   /* Create post object */
				   $_p = array();
				   $_p['post_title'] = $the_page_title;
				   $_p['post_name'] = $the_page_name;
				   $_p['post_content'] = "
				   
				   <div class='th-wrapper'>
						<div class='th-div'>
						<span style='display:block;'>Thankyou! for booking appointment.<br/>You will be notified by email with details of appointment(s).</span><br/><br/>					
						</div>
				   </div>
				   
				   ";
				   $_p['post_status'] = 'publish';
				   $_p['post_type'] = 'page';
				   $_p['comment_status'] = 'closed';
				   $_p['ping_status'] = 'closed';
				   $_p['post_category'] = array(1); /* the default 'Uncatrgorised' */

				   /* Insert the post into the database */
				   $the_page_id = wp_insert_post( $_p );
			   
			}
			

			/* add data base tables */
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			
	
				/* include all objects files while loding */
				 include_once('objects/class_update.php');
				 include_once('objects/class_move_size.php');
				 include_once('objects/class_general.php');
				 include_once('objects/class_service.php');
				 include_once('objects/class_cities.php');
				 include_once('objects/class_schedule_dayoffs.php');
				 include_once('objects/class_provider.php');
				 include_once('objects/class_additional_info.php');
				 include_once('objects/class_settings.php');
				 include_once('objects/class_email_templates.php');
				 include_once('objects/class_sms_templates.php');
				 include_once('objects/class_booking.php');
				 include_once('objects/class_order.php');
				 include_once('objects/class_front_moveto_first_step.php');
				 include_once('objects/class_clients.php');
				 include_once('objects/class_image_upload.php');
				// include_once('objects/class_email_template_settings.php');
				 include_once('objects/class_room_type.php');
				 include_once('objects/class_article_category.php');
			     include_once('objects/class_distance.php');
			     include_once('objects/class_cubicfeets.php');
			
				/* Set default settings options via class constructor */
				$general = new moveto_general();
				$movesize = new moveto_size();
				$settings = new moveto_settings(); 			
				$email_templates = new moveto_email_template(); 
				$sms_templates = new moveto_sms_template();
				$city = new moveto_city();
				$service = new moveto_service();
				$additional_info = new moveto_additional_info();
				$bookings = new moveto_booking();
				$order_client_info = new moveto_order();
				$schedule_offdays = new moveto_schedule_offdays();			
				//$email_template_settings = new moveto_email_template_settings();				
				$mp_update = new moveto_update();  /* update constructor */
				$room_type = new moveto_room_type();
				$article_category = new moveto_article_category();
				$distance = new moveto_distance();
	}
	
	/* Username checking  */	
	function check_username_mp_callback(){
	global $wpdb;

	if (validate_username($_POST['username']) && username_exists($_POST['username']) == Null && ctype_alnum($_POST['username']) ) {
    echo json_encode("true");
	} else {
		echo json_encode("Username is already exists or not alphanumeric.");
	}die();

	}	

	
	/* email checking  */	
	function check_email_mp_callback(){
	global $wpdb;
	if(isset($_POST['add_provider']) && $_POST['add_provider']=='yes'){
	$admin_email=get_option('admin_email');
	$cmp_result = strcmp($admin_email,$_POST['email']);
	
	if($cmp_result==0){ echo "true"; }else{
		if (!email_exists($_POST['email'])) {
		echo "true";
		} else {
			echo json_encode("Email is already exists.");
		}die();
	}
	}else{
			if (!email_exists($_POST['email'])) {
			echo "true";
			} else {
				echo json_encode("Email is already exists.");
			}die();
	}

	}
	/* Coupon checking  */	
	function check_generatecoupon_mp_callback(){
		global $wpdb;
		$coupon = new moveto_coupons();
			if(isset($_POST['update_coupon_code'],$_POST['mp_coupon_code'])){	
				if($_POST['update_coupon_code']!='ongenration'){
					if($_POST['mp_coupon_code']!=$_POST['update_coupon_code']){
							$coupon->coupon_code=$_POST['mp_coupon_code'];
							$coupon_info = $coupon->readOne();
						if(sizeof((array)$coupon_info)>0){
							echo json_encode("Coupon code already exists.");
						}else{
							echo "true";
						}
						}else{
						echo "true";
						}
					}else{
						$coupon->coupon_code=$_POST['mp_coupon_code'];
							$coupon_info = $coupon->readOne();
							if(sizeof((array)$coupon_info)>0){
							echo json_encode("Coupon code already exists.");
					}else{
						echo "true";
					}
				}
			}
		die();

	}
	
	
	/* City Routes checking  */	
	function check_route_callback(){
		global $wpdb;
		$city = new moveto_city();
		$city->source_city= trim($_POST['source_city']);
		$city->destination_city= trim($_POST['destination_city']);
		echo $city->check_route();die();

	}

	 
	/* Admin Menu  */	
	function moveto_admin_menu(){
		/* WooCommerce condition */
		if ( class_exists( 'WooCommerce' )) {
			$cuser = wp_get_current_user();
			$cuser->add_cap('view_admin_dashboard');
		}
		if(current_user_can('mp_client') && !current_user_can('mp_provider') && !current_user_can('manage_options')) {
			
			add_menu_page('MoveTo','MoveTo', 'mp_client','moveto_menu','mp_current_user_bookings','','80.01');
			
		} else {
			add_menu_page('MoveTo','MoveTo', 'manage_options','moveto_menu','moveto_settings_page','','80.01');
		}
		/* adding submenu */		
		add_submenu_page(null,'Calender','Calender','manage_options','appointments_submenu','mp_appointments');
		add_submenu_page(null,'All Bookings','All Bookings','manage_options','all_booking_submenu','mp_bookings');
		add_submenu_page(null,'Move Size','Move Size','manage_options','movesize_submenu','mp_movesize');
		add_submenu_page(null,'Dashboard','Dashboard','manage_options','dashboard_submenu','mp_dashboard');
		add_submenu_page(null,'Provider','Provider','manage_options','availability_submenu','mp_availability');
		add_submenu_page(null,'Services','Services','manage_options','services_submenu','mp_services');
		add_submenu_page(null,'Routes','Routes','manage_options','routes_submenu','mp_cities');
		add_submenu_page(null,'Additional Information','Additional Information','manage_options','additional_info_submenu','mp_additional_info');
		add_submenu_page(null,'Service Addons','Service Addons','manage_options','service_addons','mp_service_addons');
		add_submenu_page(null,'Distance','Distance','manage_options','distance','mp_distance');
		add_submenu_page(null,'Package Addons','Package Addons','manage_options','package_addons','mp_package_addons');
		add_submenu_page(null,'Settings','Settings','manage_options','settings_submenu','mp_settings');	
		add_submenu_page(null,'Clients','Clients','manage_options','clients_submenu','mp_clients');	
		add_submenu_page(null,'Export','Export','manage_options','export_submenu','mp_export');
		add_submenu_page(null,'Whats_New','Whats_New','manage_options','whats_new_submenu','mp_whats_new');
		add_submenu_page(null,'Forntend_Shortcode','Forntend_Shortcode','manage_options','frontend_shortcode_submenu','mp_front_shortcode');
		add_submenu_page(null,'room_article_category','room_article_category','manage_options','room_article_category','mp_room_article_category');
		add_submenu_page(null,'articles_list','articles_list','manage_options','articles_list','mp_room_articles_list');
			
	}
	
	/* Admin Menu functions */
	function moveto_settings_page(){ include_once 'admin/dashboard.php';}
	function mp_bookings(){ include_once 'admin/all_bookings.php';}
	function mp_movesize(){ include_once 'admin/movesize.php';}
	function mp_cities(){ include_once 'admin/cities.php';	}
	function mp_availability(){ include_once 'admin/availability.php';	}
	function mp_appointments(){	include_once 'admin/calendar.php';	}
	function mp_dashboard(){	include_once 'admin/dashboard.php';	}	
	function mp_services(){	include_once 'admin/services.php'; }
	function mp_additional_info(){	include_once 'admin/additional_info.php'; }
	function mp_service_addons(){include_once 'admin/service_addons.php'; }
	function mp_distance(){include_once 'admin/distance.php'; }
	function mp_package_addons(){include_once 'admin/package.php'; }
	function mp_settings(){	include_once 'admin/general_settings.php';}	
	function mp_clients(){include_once 'admin/clients.php';	}	
	function mp_guest_clients(){include_once 'admin/list_guest_client.php'; }
	function mp_export(){include_once 'admin/export.php';}
	function mp_current_user_bookings() { include_once 'admin/client_dashboard.php';}
	function mp_invoice() {  include_once 'admin/download_invoice.php';}	
	function mp_sp_settings(){include_once 'admin/service_provider_settings.php';}
	function mp_whats_new(){include_once 'admin/moveto-welcome.php';}
	function mp_front_shortcode(){include_once 'admin/front_shortcode.php';}
	function mp_room_article_category(){include_once 'admin/room_article_category.php';}
	function mp_room_articles_list(){include_once 'admin/articles_list.php';}


	
	/* Shortcode Function */
	function mp_front(){
			ob_start();
			wp_enqueue_script('jquery');
			
			wp_register_style('mp_frontend', plugins_url('assets/mp-frontend.css', __FILE__) );	
			wp_register_style('mp_responsive', plugins_url('assets/mp-responsive.css', __FILE__) );	
			wp_register_style('mp_common', plugins_url('assets/mp-common.css', __FILE__) );	
			wp_register_style('mp_reset_min', plugins_url('assets/mp-reset.min.css', __FILE__) );	
			wp_register_style('mp_jquery_ui_min', plugins_url('assets/jquery-ui.min.css', __FILE__) );	
			wp_register_style('mp_intlTelInput', plugins_url('assets/intlTelInput.css', __FILE__) );
			wp_register_style('mp_tooltipster', plugins_url('assets/tooltipster.bundle.min.css', __FILE__) );	
			wp_register_style('mp_tooltipster_sideTip_shadow', plugins_url('assets/tooltipster-sideTip-shadow.min.css', __FILE__) );
			wp_register_style('mp_simple_line_icons', plugins_url('assets/line-icons/simple-line-icons.css', __FILE__) );
			wp_register_style('bootstrap_min_css', plugins_url('assets/bootstrap/bootstrap.min.css', __FILE__) );
			wp_register_style('bootstrap_toggle_min_css', plugins_url('assets/bootstrap/bootstrap-toggle.min.css', __FILE__) );
			wp_register_style('bootstrap_select_min_css', plugins_url('assets/bootstrap/bootstrap-select.min.css', __FILE__) );
			wp_register_style('multi_select_css', plugins_url('assets/multi-select.css', __FILE__) );
			wp_register_style('bootstrap_datepicker_min_css', plugins_url('assets/bootstrap-datepicker.min.css', __FILE__) );
			wp_register_style('font_awesome_min_css', plugins_url('assets/font-awesome/css/font-awesome.min.css', __FILE__) );
			wp_register_style('font_line_icon_min_css', plugins_url('assets/line-icons/LineIcons.min.css', __FILE__) );
			
			if(is_rtl()){
				wp_register_style('mp_rtl_css', plugins_url('assets/mp-rtl.css', __FILE__) );
				wp_enqueue_style('mp_rtl_css');
			}
			
			wp_enqueue_style('mp_frontend');
			wp_enqueue_style('mp_responsive');
			wp_enqueue_style('mp_common');
			wp_enqueue_style('mp_reset_min');
			/*wp_enqueue_style('mp_jquery_ui_min');*/
			wp_enqueue_style('mp_intlTelInput');
			wp_enqueue_style('mp_tooltipster');
			wp_enqueue_style('mp_tooltipster_sideTip_shadow');
			wp_enqueue_style('mp_simple_line_icons');
			wp_enqueue_style('bootstrap_min_css');
			wp_enqueue_style('bootstrap_toggle_min_css' );
			wp_enqueue_style('bootstrap_select_min_css' );
			wp_enqueue_style('multi_select_css' );
			wp_enqueue_style('bootstrap_datepicker_min_css' );
			wp_enqueue_style('font_awesome_min_css' );
			wp_enqueue_style('font_line_icon_min_css' );
		
			wp_register_script('moveto_validate_js',plugins_url('assets/js/jquery.validate.min.js',  __FILE__) );
			wp_register_script('moveto_jquery_ui_js',plugins_url('assets/js/jquery-ui.min.js',  __FILE__) );
			wp_register_script('moveto_intlTelInput_js',plugins_url('assets/js/intlTelInput.js',  __FILE__) );
			wp_register_script('moveto_tooltipster_js',plugins_url('assets/js/tooltipster.bundle.min.js',  __FILE__) );
			wp_register_script('moveto_payment_js',plugins_url('assets/js/jquery.payment.min.js',  __FILE__) );
			wp_register_script('bootstrap_min_js',plugins_url('assets/js/bootstrap.min.js',  __FILE__) );
			wp_register_script('moveto_common_front_js',plugins_url('assets/js/common-front.js',  __FILE__) );
			wp_register_script('bootstrap_datepicker_min_js',plugins_url( '/assets/js/bootstrap-datepicker.min.js',  __FILE__) );
			wp_register_script('bootstrap_select_min_js',plugins_url( '/assets/js/bootstrap-select.min.js',  __FILE__) );
			wp_register_script('bootstrap_toggle_min_js',plugins_url( '/assets/js/bootstrap-toggle.min.js',  __FILE__) );
			wp_register_script('jquery_maskedinput',plugins_url( '/assets/js/jquery.maskedinput.js',  __FILE__) );
			
			
			
			wp_enqueue_script('moveto_validate_js');	
			wp_enqueue_script('moveto_jquery_ui_js');	
			wp_enqueue_script('moveto_intlTelInput_js');	
			wp_enqueue_script('moveto_tooltipster_js');	
			wp_enqueue_script('moveto_payment_js');	
			wp_enqueue_script('bootstrap_min_js');	
			wp_enqueue_script('moveto_common_front_js');	
			wp_enqueue_script('bootstrap_datepicker_min_js' );
			wp_enqueue_script('bootstrap_select_min_js' );
			wp_enqueue_script('bootstrap_toggle_min_js' );
			wp_enqueue_script('jquery_maskedinput' );
			include_once 'frontend/mp_firstep.php';
			$output = ob_get_clean();
			return $output;
		}
	
	
	/* style n scripts for moveto admin panel */
	function moveto_admin_scripts($hook) {
		ob_start();
		global $submenu;
		global $wp_styles;
		$parent='';
		$mp_pages = array();	 
		if ( (is_array( $submenu ) && isset( $submenu[$parent] )) || $hook=='toplevel_page_moveto_menu' ) {
			$mp_pages[] = 'toplevel_page_moveto_menu';
			$mp_pages[] = 'toplevel_page_provider_submenu';
			$mp_pages[] = 'toplevel_page_verify';
			if(!empty($submenu)){
				foreach ($submenu[$parent] as $item) {	$mp_pages[] = 'admin_page_'.$item[2];}
			}
		}
		if( !in_array($hook,$mp_pages) )
		return;
		
		
		$moveto_plugin_url = plugins_url('',  __FILE__);
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-sortable');
		wp_register_script('jquery_ui_min',$moveto_plugin_url . '/assets/js/jquery-ui.min.js','','',true);
		wp_register_script('moveto_validate_js',plugins_url('assets/js/jquery.validate.min.js',  __FILE__) ,'','',true);
		wp_register_script('moment_min',$moveto_plugin_url . '/assets/js/moment.min.js','','',true);
		wp_register_script('mp_common_admin_jquery',$moveto_plugin_url . '/assets/js/mp-common-admin-jquery.js?'.time(),'','',true);
		wp_register_script('form_builder_min',$moveto_plugin_url . '/assets/js/form-builder.min.js','','',true);
		wp_register_script('form_render_min',$moveto_plugin_url . '/assets/js/form-render.min.js','','',true);
		wp_register_script('intlTelInput',$moveto_plugin_url . '/assets/js/intlTelInput.js','','',true);
		wp_register_script('bootstrap_min',$moveto_plugin_url . '/assets/js/bootstrap.min.js','','',true);
		wp_register_script('bootstrap_toggle_min',$moveto_plugin_url . '/assets/js/bootstrap-toggle.min.js','','',true);
		wp_register_script('bootstrap_select_min',$moveto_plugin_url . '/assets/js/bootstrap-select.min.js','','',true);
		wp_register_script('bootstrap_daterangepicker_js',$moveto_plugin_url . '/assets/js/daterangepicker.js','','',true);
		wp_register_script('chart',$moveto_plugin_url . '/assets/js/Chart.js','','',true);
		wp_register_script('jquery_minicolors_min',$moveto_plugin_url . '/assets/js/jquery.minicolors.min.js','','',true);
		wp_register_script('jquery_jcrop',$moveto_plugin_url . '/assets/js/jquery.Jcrop.min.js','','',true);
		wp_register_script('jquery_dataTables_min',$moveto_plugin_url . '/assets/js/datatable/jquery.dataTables.min.js','','',true);
		wp_register_script('dataTables_responsive_min',$moveto_plugin_url . '/assets/js/datatable/dataTables.responsive.min.js','','',true);
		wp_register_script('dataTables_bootstrap_min',$moveto_plugin_url . '/assets/js/datatable/dataTables.bootstrap.min.js','','',true);
		wp_register_script('dataTables_buttons_min',$moveto_plugin_url . '/assets/js/datatable/dataTables.buttons.min.js','','',true);
		wp_register_script('jszip_min',$moveto_plugin_url . '/assets/js/datatable/jszip.min.js','','',true);
		wp_register_script('pdfmake_min',$moveto_plugin_url . '/assets/js/datatable/pdfmake.min.js','','',true);
		wp_register_script('vfs_fonts',$moveto_plugin_url . '/assets/js/datatable/vfs_fonts.js','','',true);
		wp_register_script('buttons_html5_min',$moveto_plugin_url . '/assets/js/datatable/buttons.html5.min.js','','',true);
		wp_register_script('bootstrap_editable_min',$moveto_plugin_url . '/assets/js/bootstrap-editable.min.js','','',true);
		wp_register_script('pace_min',$moveto_plugin_url . '/assets/js/pace.min.js','','',true);
		
		if(is_rtl()){
			wp_register_style('mp_admin_rtl_bootstrap_css', plugins_url('assets/bootstrap/bootstrap-rtl.min.css', __FILE__) );
			wp_register_style('mp_admin_rtl_responsive_css', plugins_url('assets/mp-admin-rtl-responsive.css', __FILE__) );
			wp_enqueue_style('mp_admin_rtl_bootstrap_css');
			wp_enqueue_style('mp_admin_rtl_responsive_css');
			wp_register_style('mp_admin_rtl_css', plugins_url('assets/mp-admin-rtl.css', __FILE__) );
			wp_enqueue_style('mp_admin_rtl_css');
			wp_register_style('mp_main_rtl_css', plugins_url('assets/rtl.css', __FILE__) );
			wp_enqueue_style('mp_main_rtl_css');
		}
		wp_enqueue_script('jquery_ui_min');
		wp_enqueue_script('moment_min');
		wp_enqueue_script('moveto_validate_js');		
		wp_enqueue_script('form_builder_min');
		wp_enqueue_script('form_render_min');
		wp_enqueue_script('intlTelInput');
		wp_enqueue_script('bootstrap_min');
		wp_enqueue_script('bootstrap_toggle_min');
		wp_enqueue_script('bootstrap_select_min');
		wp_enqueue_script('bootstrap_daterangepicker_js');
		wp_enqueue_script('chart');
		wp_enqueue_script('jquery_minicolors_min');
		wp_enqueue_script('jquery_jcrop');
		wp_enqueue_script('jquery_dataTables_min');
		wp_enqueue_script('dataTables_responsive_min');
		wp_enqueue_script('dataTables_bootstrap_min');
		wp_enqueue_script('dataTables_buttons_min');
		wp_enqueue_script('jszip_min');
		wp_enqueue_script('pdfmake_min');
		wp_enqueue_script('vfs_fonts');
		wp_enqueue_script('buttons_html5_min');
		wp_enqueue_script('bootstrap_editable_min');
		wp_enqueue_script('mp_common_admin_jquery');
		wp_enqueue_script('pace_min');
		
		 
		
		
		wp_register_style( 'mp_admin_style',$moveto_plugin_url . '/assets/mp-admin-style.css');		
		wp_register_style( 'mp_admin_common', $moveto_plugin_url . '/assets/mp-admin-common.css' );
		wp_register_style( 'mp_admin_responsive',$moveto_plugin_url .'/assets/mp-admin-responsive.css' );
		wp_register_style( 'mp_admin_reset',$moveto_plugin_url .'/assets/mp-reset.min.css' );
		
		wp_register_style( 'bootstarp_min_css',$moveto_plugin_url . '/assets/bootstrap/bootstrap.min.css');
		wp_register_style( 'bootstrap_daterangepicker', $moveto_plugin_url . '/assets/daterangepicker.css' );
		wp_register_style('moveto_phone_codes', plugins_url('assets/intlTelInput.css', __FILE__) );
		wp_register_style('bootstrap_theme_min', plugins_url('assets/bootstrap/bootstrap-theme.min.css', __FILE__) );
		wp_register_style('bootstrap_toggle_min', plugins_url('assets/bootstrap/bootstrap-toggle.min.css', __FILE__) );
		wp_register_style('bootstrap_select_min', plugins_url('assets/bootstrap/bootstrap-select.min.css', __FILE__) );
		wp_register_style('jquery_jcrop', plugins_url('assets/jquery.Jcrop.min.css', __FILE__) );
		wp_register_style('jquery_minicolors', plugins_url('assets/jquery.minicolors.css', __FILE__) );
		wp_register_style('jquery_dataTables_min', plugins_url('assets/jquery.dataTables.min.css', __FILE__) );
		wp_register_style('responsive_dataTables_min', plugins_url('assets/responsive.dataTables.min.css', __FILE__) );
		wp_register_style('dataTables_bootstrap_min', plugins_url('assets/dataTables.bootstrap.min.css', __FILE__) );
		wp_register_style('buttons_dataTables_min', plugins_url('assets/buttons.dataTables.min.css', __FILE__) );
		wp_register_style('bootstrap_editable', plugins_url('assets/bootstrap-editable.css', __FILE__) );
		wp_register_style('jquery_ui_min', plugins_url('assets/jquery-ui.min.css', __FILE__) );
		wp_register_style('form_builder_min', plugins_url('assets/form-builder.min.css', __FILE__) );
		wp_register_style('form_render_min', plugins_url('assets/form-render.min.css', __FILE__) );
		wp_register_style('font_awesome_min', plugins_url('assets/font-awesome/css/font-awesome.min.css', __FILE__) );
		wp_register_style('simple_line_icons', plugins_url('assets/line-icons/LineIcons.min.css', __FILE__) );
			
		
		wp_enqueue_style( 'mp_admin_style' );
		wp_enqueue_style( 'mp_admin_common' );
		wp_enqueue_style( 'mp_admin_responsive' );
		wp_enqueue_style( 'mp_admin_reset' );
		wp_enqueue_style( 'bootstarp_min_css' );
		wp_enqueue_style( 'bootstrap_daterangepicker' );
		wp_enqueue_style( 'moveto_phone_codes' );
		wp_enqueue_style( 'bootstrap_theme_min' );
		wp_enqueue_style( 'bootstrap_toggle_min' );
		wp_enqueue_style( 'bootstrap_select_min' );
		wp_enqueue_style( 'jquery_jcrop' );
		wp_enqueue_style( 'jquery_minicolors' );
		wp_enqueue_style( 'jquery_dataTables_min' );
		wp_enqueue_style( 'responsive_dataTables_min' );
		wp_enqueue_style( 'dataTables_bootstrap_min' );
		wp_enqueue_style( 'buttons_dataTables_min' );
		wp_enqueue_style( 'bootstrap_editable' );
		wp_enqueue_style( 'jquery_ui_min' );
		wp_enqueue_style( 'form_builder_min' );
		wp_enqueue_style( 'form_render_min' );
		wp_enqueue_style( 'font_awesome_min' );
		wp_enqueue_style( 'simple_line_icons' );
		
	
		if ( 'admin_page_appointments_submenu' == $hook || 'toplevel_page_moveto_menu' == $hook ) {
			 wp_register_script('moment_min_js',$moveto_plugin_url . '/assets/js/moment.min.js');
			 wp_enqueue_script('moment_min_js');
			 wp_register_script('fc_min_js',$moveto_plugin_url . '/assets/js/fullcalendar.min.js');
			 wp_enqueue_script('fc_min_js');
			 wp_register_script('fc_language_js',$moveto_plugin_url . '/assets/js/lang-all.js');
			 wp_enqueue_script('fc_language_js');
			 wp_register_style( 'fc_min_css',$moveto_plugin_url . '/assets/fullcalendar.min.css');
			 wp_enqueue_style( 'fc_min_css' );
			
						  
			
		}
			
		if('admin_page_clients_submenu'==$hook){
		
		}
	
		if('admin_page_appearance_submenu'==$hook || 'admin_page_add_service_submenu'==$hook || 'admin_page_update_service_submenu'==$hook) {
				wp_register_script('jscolor_js',$moveto_plugin_url . '/assets/js/jscolor.js');
				wp_enqueue_script('jscolor_js');
		}
	 
	}
	function moveto_activate() {
	
	 global $wpdb;
	 require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	 include_once('objects/class_booking.php');
	 include_once('objects/class_move_size.php');
	 include_once('objects/class_general.php');
	 include_once('objects/class_service.php');
	 include_once('objects/class_cities.php');
	 include_once('objects/class_schedule_dayoffs.php');
	 include_once('objects/class_provider.php');
	 include_once('objects/class_additional_info.php');
	 include_once('objects/class_settings.php');
	 include_once('objects/class_email_templates.php');
	 include_once('objects/class_sms_templates.php');
	 include_once('objects/class_booking.php');
	 include_once('objects/class_order.php');
	 include_once('objects/class_front_moveto_first_step.php');
	 include_once('objects/class_clients.php');
	 include_once('objects/class_image_upload.php');
	 include_once('objects/class_room_type.php');
	 include_once('objects/class_article_category.php');
	 include_once('objects/class_distance.php');
	 include_once('objects/class_cubicfeets.php');



	/* Set default settings options via class constructor */
	$movesize = new moveto_size();
	$general = new moveto_general();
	$settings = new moveto_settings(); 			
	$email_templates = new moveto_email_template(); 
	$sms_templates = new moveto_sms_template();
	$city = new moveto_city();
	$service = new moveto_service();
	$additional_info = new moveto_additional_info();
	$bookings = new moveto_booking();
	$order_client_info = new moveto_order();
	$schedule_offdays = new moveto_schedule_offdays();
	$room_type = new moveto_room_type();
	$article_category = new moveto_article_category();
	$pricing_rule = new moveto_distance();
	$distance = new moveto_distance();
	$elevator = new moveto_distance();
	$floors = new moveto_distance();
	$pac_unpac = new moveto_distance();
	$cubicfeets = new moveto_cubicfeets();

	
	

	$city_table_create = $city->create_table();
	$room_type_table_create = $room_type->create_table_room_type();
	$article_category_table_create = $article_category->create_table_article_category();
	$addons_service_table_create = $service->create_table_addons();
	$additional_info_table_create = $additional_info->create_table();
	$email_templates_table_create = $email_templates->create_table();
	$sms_templates_table_create = $sms_templates->create_table();
	$bookings_table_create = $bookings->create_table();
	$client_orderinfo_table_create = $order_client_info->create_table();
	$schedule_offdays_table_create = $schedule_offdays->create_table();
	$pricing_rules_table_create = $pricing_rule->create_table();
	$distance_table_create = $pricing_rule->create_table_distance();
	$elevator_table_create = $elevator->create_table_elevator();
	$floors_table_create = $floors->create_table_floors();
	$pac_unpac_table_create = $pac_unpac->create_table_pac_unpac();
	$movesize_table_create = $movesize->create_table_move_size();
	$move_cubicfeets = $cubicfeets->create_table_cubicfeets();
	
	$tablecreation=array($city_table_create,$room_type_table_create,$city_table_create,$email_templates_table_create,$sms_templates_table_create,$bookings_table_create,$client_orderinfo_table_create,$schedule_offdays_table_create,$addons_service_table_create,$additional_info_table_create,$pricing_rules_table_create,$elevator_table_create,$floors_table_create,$pac_unpac_table_create,$distance_table_create,$movesize_table_create,$move_cubicfeets);
	if ( is_multisite()) {

			$current_blog = $wpdb->blogid;

			$blog_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
			for($i=0;$i<sizeof((array)$tablecreation);$i++){
			foreach ( $blog_ids as $blog_id) {
				switch_to_blog( $blog_id );
				$tablecreation[$i];
				restore_current_blog();
				}
			}
		} 
		
	$email_templates_create = $email_templates->moveto_create_email_template();
	$sms_templates_create   = $sms_templates->moveto_create_sms_template();

if(get_option('moveto_sample_status') == 'Y'){
	$move_size_infoids	    = array();
		$additional_infoids		= array();
		$article_infoids		= array();
		$pricing_rules_infoids	= array();
		$mp_floors_infoids	    = array();
		$elevator_infoids	    = array();
		$packing_unpacking_infoids = array();
		$room_type_infoids	    = array();
		$art_cat_infoids        = array();
		$cubic_ids 				= array();
/* ------------------------------------------------------------------------------ */
		/* Insert movesize */
		$move_size_arr = array(array('move_size'=>'1BHK'),array('move_size'=>'2BHK'),array('move_size'=>'3BHK'));
		foreach($move_size_arr as $move_size_info){
			/* Add movesize Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_movesize (id,name,status) values( '','".$move_size_info['move_size']."','E')");
			$lastinsert_moveto_id = $wpdb->insert_id;
			$move_size_infoids[] = $lastinsert_moveto_id;
		}
		/* Additional Information */
		$additional_infos = array(array('info_name'=>'Packing material from us ?'),array('info_name'=>'Is material frisking ?'));
		foreach($additional_infos as $additional_info){
			/* Add Additional Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_additional_info (id,additional_info,status,type,price) values( '','".$additional_info['info_name']."','E','P','3')");
			$lastinsertid = $wpdb->insert_id;
			$additional_infoids[] = $lastinsertid;
		}
		
		/* Insert  Articles */
		$article_arr = array(
			array('article_name'=>'Single Bed','predefinedimage'=>'single-bed.png','cubicarea'=>'30'),
			array('article_name'=>'Double Bed','predefinedimage'=>'double-bed.png','cubicarea'=>'60'),
			array('article_name'=>'Table','predefinedimage'=>'study-table.png','cubicarea'=>'12'),
			array('article_name'=>'Fridge','predefinedimage'=>'fridge.png','cubicarea'=>'40'),
			array('article_name'=>'Washing Machine','predefinedimage'=>'washing-machine.png','cubicarea'=>'35'),
			array('article_name'=>'Fan','predefinedimage'=>'table-fan.png','cubicarea'=>'8'),
			array('article_name'=>'Bed Mattress','predefinedimage'=>'bed-mattress.png','cubicarea'=>'25'),
			array('article_name'=>'Bunk Bed','predefinedimage'=>'bunk-bed.png','cubicarea'=>'40'),
			array('article_name'=>'Computer Table','predefinedimage'=>'computer-table.png','cubicarea'=>'35'),
			array('article_name'=>'Computer','predefinedimage'=>'computer.png','cubicarea'=>'45'),
			array('article_name'=>'Dish Washer','predefinedimage'=>'dish-washer.png','cubicarea'=>'35'),
			array('article_name'=>'Dressing Table','predefinedimage'=>'dressing-table.png','cubicarea'=>'58'),
			array('article_name'=>'Foldable Chair','predefinedimage'=>'foldable-chair.png','cubicarea'=>'20'),
			array('article_name'=>'Home Theater','predefinedimage'=>'home-theater.png','cubicarea'=>'15'),
			array('article_name'=>'Mixer Grinder','predefinedimage'=>'mixer-grinder.png','cubicarea'=>'15'),
			array('article_name'=>'Music System','predefinedimage'=>'music-system.png','cubicarea'=>'19'),
			array('article_name'=>'Non-foldable Chair','predefinedimage'=>'non-foldable-chair.png','cubicarea'=>'20'),
			array('article_name'=>'Oven','predefinedimage'=>'oven.png','cubicarea'=>'15'),
			array('article_name'=>'Plastic Chair','predefinedimage'=>'plastic-chair.png','cubicarea'=>'15'),
			array('article_name'=>'Split Ac','predefinedimage'=>'split-ac.png','cubicarea'=>'40'),
			array('article_name'=>'Study Table','predefinedimage'=>'study-table.png','cubicarea'=>'25'),
			array('article_name'=>'Television Table','predefinedimage'=>'television-table.png','cubicarea'=>'30'),
			array('article_name'=>'Trolley Bag','predefinedimage'=>'trolley-bag.png','cubicarea'=>'8'),
			array('article_name'=>'Steel Wardrobe','predefinedimage'=>'wardrobe-steel.png','cubicarea'=>'10'),
			array('article_name'=>'Wooden Wardrobe','predefinedimage'=>'wardrobe-wooden.png','cubicarea'=>'10'),
			array('article_name'=>'Water Purifier','predefinedimage'=>'water-purifier.png','cubicarea'=>'15'),
			array('article_name'=>'Air Cooler','predefinedimage'=>'air-cooler.png','cubicarea'=>'45'),
			array('article_name'=>'Dining Table','predefinedimage'=>'dining-table-chairs.png','cubicarea'=>'45'),
			array('article_name'=>'3-Seater Sofa','predefinedimage'=>'sofa-seater-3.png','cubicarea'=>'50'),
			array('article_name'=>'Bicycle','predefinedimage'=>'Bicycle.png','cubicarea'=>'10'),
			array('article_name'=>'Bike-Bullet','predefinedimage'=>'Bike-bullet.png','cubicarea'=>'10'),
			array('article_name'=>'Bike-With-Gear','predefinedimage'=>'Bike-with-gear.png','cubicarea'=>'10'),
			array('article_name'=>'Car-Hatchback','predefinedimage'=>'Car-hatchback.png','cubicarea'=>'20'),
			array('article_name'=>'Car-Sedan','predefinedimage'=>'Car-sedan.png','cubicarea'=>'20'),
			array('article_name'=>'Car-Suv','predefinedimage'=>'Car-suv.png','cubicarea'=>'20'),
			array('article_name'=>'Moped','predefinedimage'=>'Moped.png','cubicarea'=>'10')
		);
		foreach($article_arr as $article_arr_info){
			/* Add movesize Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_articles (id,article_name,type,price,predefinedimage,max_qty,status,cubicarea) values( '','".$article_arr_info['article_name']."','P','5','".$article_arr_info['predefinedimage']."','5','E','".$article_arr_info['cubicarea']."')");
			$lastinsert_article_id = $wpdb->insert_id;
			$article_infoids[] = $lastinsert_article_id;
		}
		
		/* Insert Pricing rules */
		
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (id,distance,rules,price,distance_id) values( '','10','G','10','1')");
			$pricing_rules_infoids[]= $wpdb->insert_id;
			
			$stmt1 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (id,distance,rules,price,distance_id) values( '','50','G','60','1')");
			$pricing_rules_infoids[]= $wpdb->insert_id;
			
			$stmt2 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_pricing_rules (id,distance,rules,price,distance_id) values( '','80','G','100','1')");
			$pricing_rules_infoids[]= $wpdb->insert_id;
			
		/* Insert Floor */
			$stmtt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','0','1','0')");
			$mp_floors_infoids[]= $wpdb->insert_id;

			$stmtt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','10','1','1')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt2 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','12','1','2')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt3 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','14','1','3')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt4 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','16','1','4')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
			$stmtt5 = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_floors (id,price,distance_id,floor_no) values( '','17','1','5')");
			$mp_floors_infoids[]= $wpdb->insert_id;
			
		/* Insert loading/ unloading elevator */	
			$stmtt_ele = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_elevator (id,discount,distance_id) values( '','3','1')");
			$elevator_infoids[]= $wpdb->insert_id;
			
		/* Insert packing/ unpacking */	
			$stmtt_ele = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_packaging_unpackagin (id,cost_packing,cost_unpacking,distance_id) values( '1','3','2','1')");
			$packing_unpacking_infoids[]= $wpdb->insert_id;	
			
		/* Insert roomtypes */	
			$roomtypes_arr = array(array('name'=>'Bedroom'),array('name'=>'Livingroom'),array('name'=>'Kitchen'),array('name'=>'Vehicles'));
			foreach($roomtypes_arr as $roomtypes_arr_info){
			/* Add roomtypes Info */
			$stmty = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_room_type (id,name,status) values( '','".$roomtypes_arr_info['name']."','E')");
			$roomtypes_id = $wpdb->insert_id; 
			$room_type_infoids[] = $roomtypes_id;
		}		 
		/* Add article category */
			/*$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_article_category'."";
			$stmt = $wpdb->get_results($queryString); 
			if(count($stmt)==0){*/
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Furniture','E','".$article_infoids[0].",".$article_infoids[1].",".$article_infoids[2].",".$article_infoids[6].",".$article_infoids[7].",".$article_infoids[8].",".$article_infoids[11].",".$article_infoids[12].",".$article_infoids[16].",".$article_infoids[18].",".$article_infoids[20].",".$article_infoids[21].",".$article_infoids[23].",".$article_infoids[24]."','".$room_type_infoids[0]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Furniture','E','".$article_infoids[2].",".$article_infoids[8].",".$article_infoids[11].",".$article_infoids[12].",".$article_infoids[16].",".$article_infoids[18].",".$article_infoids[20].",".$article_infoids[21].",".$article_infoids[24].",".$article_infoids[27].",".$article_infoids[28]."','".$room_type_infoids[1]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Furniture','E','".$article_infoids[2].",".$article_infoids[12].",".$article_infoids[16].",".$article_infoids[24].",".$article_infoids[27]."','".$room_type_infoids[2]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Electronic','E','".$article_infoids[5].",".$article_infoids[9].",".$article_infoids[15].",".$article_infoids[19].",".$article_infoids[26]."','".$room_type_infoids[0]."')");
				$art_cat_infoids[] = $wpdb->insert_id;
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Electronic','E','".$article_infoids[5].",".$article_infoids[9].",".$article_infoids[13].",".$article_infoids[15].",".$article_infoids[19].",".$article_infoids[26]."','".$room_type_infoids[1]."')");
				$art_cat_infoids[] = $wpdb->insert_id;	
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Electronic','E','".$article_infoids[3].",".$article_infoids[5].",".$article_infoids[10].",".$article_infoids[14].",".$article_infoids[17].",".$article_infoids[17].",".$article_infoids[4]."','".$room_type_infoids[2]."')");
				$art_cat_infoids[] = $wpdb->insert_id;
				$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_article_category (name,status,article_id,room_type_id)values('Vehicles','E','".$article_infoids[29].",".$article_infoids[30].",".$article_infoids[31].",".$article_infoids[32].",".$article_infoids[33].",".$article_infoids[34].",".$article_infoids[35]."','".$room_type_infoids[3]."')");
				$art_cat_infoids[] = $wpdb->insert_id;
			/*}*/	


			/* Cubic Meter */
			$cubic_meter_arr = array(array('cubicfeets_range'=>'0','cubicfeets_range_price'=>'3'),array('cubicfeets_range'=>'50','cubicfeets_range_price'=>'5'),array('cubicfeets_range'=>'150','cubicfeets_range_price'=>'8'),array('cubicfeets_range'=>'250','cubicfeets_range_price'=>'10'),array('cubicfeets_range'=>'400','cubicfeets_range_price'=>'15'));
			foreach($cubic_meter_arr as $cubic_meter_arr_val){
			/* Add Cubic Meter Info */
			$stmt = $wpdb->query("INSERT INTO ".$wpdb->prefix."mp_cubicfeets (id,cubicfeets_range,cubicfeets_range_price) values( '','".$cubic_meter_arr_val['cubicfeets_range']."','".$cubic_meter_arr_val['cubicfeets_range_price']."')");
			$cubic_id = $wpdb->insert_id; 
			$cubic_ids[] = $cubic_id;
			}

		
/* ---------------------------------------------------------------------------- */
/* Booking details data */
		
		/* Getting last order_id */
		$next_order_id = 0;
		$bookings->get_last_order_id();
		if(isset($bookings->last_order_id) && $bookings->last_order_id!=''){
			$next_order_id = $bookings->last_order_id + 1;	
		}else{
			$next_order_id = 1001;
		}
		$serviceinfo1 = 'a:7:{i:0;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:1:"7";s:14:"get_article_id";s:1:"7";s:16:"get_article_name";s:10:"Single Bed";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:1;s:0:"";i:2;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:2:"10";s:14:"get_article_id";s:2:"12";s:16:"get_article_name";s:11:"Ceiling Fan";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:3;a:7:{s:19:"get_article_room_id";s:1:"5";s:18:"get_article_cat_id";s:1:"8";s:14:"get_article_id";s:1:"9";s:16:"get_article_name";s:5:"Table";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:4;s:0:"";i:5;s:0:"";i:6;a:7:{s:19:"get_article_room_id";s:1:"6";s:18:"get_article_cat_id";s:2:"12";s:14:"get_article_id";s:2:"10";s:16:"get_article_name";s:6:"Fridge";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}}';
		
		$loadinginfo1 = 'a:9:{s:17:"loading_floor_amt";d:2;s:20:"loading_floor_number";s:1:"1";s:17:"load_elevator_amt";d:0.59999999999999997779553950749686919152736663818359375;s:21:"load_elevator_checked";s:3:"Yes";s:13:"packaging_amt";d:0.59999999999999997779553950749686919152736663818359375;s:17:"packaging_checked";s:3:"Yes";s:19:"loading_address_one";s:13:"12 Fox Street";s:19:"loading_address_two";s:6:"Denver";s:21:"loading_address_three";s:8:"Colorado";}';
		
		$unloadinginfo1 = 'a:9:{s:19:"unloading_floor_amt";d:2.79999999999999982236431605997495353221893310546875;s:22:"unloading_floor_number";s:1:"3";s:19:"unload_elevator_amt";d:0.59999999999999997779553950749686919152736663818359375;s:23:"unload_elevator_checked";s:3:"Yes";s:15:"unpackaging_amt";d:0.40000000000000002220446049250313080847263336181640625;s:19:"unpackaging_checked";s:3:"Yes";s:21:"unloading_address_one";s:19:"23 SW Market Street";s:21:"unloading_address_two";s:8:"Portland";s:23:"unloading_address_three";s:6:"Oregon";}';
		
		$additionalinfo1 = 'a:1:{s:8:"favorite";a:1:{i:0;s:1:"3";}}';
		
		$userinfo1 = 'a:3:{s:4:"name";s:13:"Jamie L smith";s:5:"email";s:15:"jamie@gmail.com";s:5:"phone";s:10:"9056565666";}';
		
		



		$serviceinfo2 = 'a:5:{i:0;s:0:"";i:1;s:0:"";i:2;s:0:"";i:3;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:1:"7";s:14:"get_article_id";s:1:"8";s:16:"get_article_name";s:10:"Double Bed";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:4;a:7:{s:19:"get_article_room_id";s:1:"6";s:18:"get_article_cat_id";s:1:"9";s:14:"get_article_id";s:1:"9";s:16:"get_article_name";s:5:"Table";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}}';	
		
		$loadinginfo2 = 'a:9:{s:17:"loading_floor_amt";d:1.1999999999999999555910790149937383830547332763671875;s:20:"loading_floor_number";s:1:"2";s:17:"load_elevator_amt";d:0.299999999999999988897769753748434595763683319091796875;s:21:"load_elevator_checked";s:3:"Yes";s:13:"packaging_amt";d:0.299999999999999988897769753748434595763683319091796875;s:17:"packaging_checked";s:3:"Yes";s:19:"loading_address_one";s:9:"5 K ST NW";s:19:"loading_address_two";s:8:"Downtown";s:21:"loading_address_three";s:10:"Washington";}';	
		$unloadinginfo2 = 'a:9:{s:19:"unloading_floor_amt";d:1.600000000000000088817841970012523233890533447265625;s:22:"unloading_floor_number";s:1:"4";s:19:"unload_elevator_amt";d:0.299999999999999988897769753748434595763683319091796875;s:23:"unload_elevator_checked";s:3:"Yes";s:15:"unpackaging_amt";d:0.200000000000000011102230246251565404236316680908203125;s:19:"unpackaging_checked";s:3:"Yes";s:21:"unloading_address_one";s:10:"6 South St";s:21:"unloading_address_two";s:13:"Filter Square";s:23:"unloading_address_three";s:12:"Philadelphia";}';
		$additionalinfo2 = 'a:1:{s:8:"favorite";a:1:{i:0;s:1:"4";}}';
		$userinfo2 = 'a:3:{s:4:"name";s:5:"Linda";s:5:"email";s:20:"lindasmith@gmail.com";s:5:"phone";s:10:"9876543210";}';
		
		


		$serviceinfo3 = 'a:4:{i:0;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:2:"10";s:14:"get_article_id";s:2:"12";s:16:"get_article_name";s:11:"Ceiling Fan";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:1;a:7:{s:19:"get_article_room_id";s:1:"4";s:18:"get_article_cat_id";s:1:"7";s:14:"get_article_id";s:1:"7";s:16:"get_article_name";s:10:"Single Bed";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:2;a:7:{s:19:"get_article_room_id";s:1:"5";s:18:"get_article_cat_id";s:1:"8";s:14:"get_article_id";s:1:"9";s:16:"get_article_name";s:5:"Table";s:5:"count";s:1:"1";s:17:"get_article_price";s:1:"5";s:17:"tot_article_price";i:5;}i:3;s:0:"";}';
		$loadinginfo3 = 'a:9:{s:17:"loading_floor_amt";d:2.54999999999999982236431605997495353221893310546875;s:20:"loading_floor_number";s:1:"5";s:17:"load_elevator_amt";d:0;s:21:"load_elevator_checked";s:2:"No";s:13:"packaging_amt";d:0.450000000000000011102230246251565404236316680908203125;s:17:"packaging_checked";s:3:"Yes";s:19:"loading_address_one";s:10:"21 W 35 St";s:19:"loading_address_two";s:7:"Chicago";s:21:"loading_address_three";s:8:"Illinois";}';
		$unloadinginfo3 = 'a:9:{s:19:"unloading_floor_amt";d:2.100000000000000088817841970012523233890533447265625;s:22:"unloading_floor_number";s:1:"3";s:19:"unload_elevator_amt";d:0.450000000000000011102230246251565404236316680908203125;s:23:"unload_elevator_checked";s:3:"Yes";s:15:"unpackaging_amt";d:0;s:19:"unpackaging_checked";s:2:"No";s:21:"unloading_address_one";s:18:"32 Columbia Street";s:21:"unloading_address_two";s:7:"Seattle";s:23:"unloading_address_three";s:10:"Washington";}';
		
		$additionalinfo3 = 'a:1:{s:8:"favorite";a:2:{i:0;s:1:"3";i:1;s:1:"4";}}';
		$userinfo3 = 'a:3:{s:4:"name";s:4:"John";s:5:"email";s:19:"johnmartin@demo.com";s:5:"phone";s:10:"7463543210";}';

		$booking_custom_cubic1 = 'a:2:{i:1;a:3:{s:15:"cubicfeet_title";s:3:"bed";s:15:"cubicfeet_count";s:3:"140";s:6:"moreid";s:4:"row2";}i:2;a:3:{s:15:"cubicfeet_title";s:5:"Table";s:15:"cubicfeet_count";s:2:"18";s:6:"moreid";s:4:"row3";}}';
		


		$mp_clientinfo = array(array('client_name'=>'Jamie L smith','client_email'=>'jamie@gmail.com','client_phone'=>'+19056565666'),array('client_name'=>'Linda','client_email'=>'lindasmith@gmail.com','client_phone'=>'+19876543210'),array('client_name'=>'John','client_email'=>'johnmartin@demo.com','client_phone'=>'+17463543210'));
		






		$mpclientids = array();
		
		foreach($mp_clientinfo as $mp_clientsinfo){
		/* Get order id of user */
			

			$mp_user_info = array(
						'user_login'    =>   $mp_clientsinfo['client_name'],
						'user_email'    =>   $mp_clientsinfo['client_email'],
						'user_pass'     =>   '',
						'first_name'    =>   $mp_clientsinfo['client_name'],
						'last_name'     =>   '',
						'nickname'      =>  '',
						'role' => 'subscriber'
						);	
	   
			$new_mp_user = wp_insert_user( $mp_user_info );
			$mpclientids[] =  $new_mp_user;
			/*$user = new WP_User($new_mp_user);
			$user->add_cap('read');
			$user->add_cap('mp_client'); 
			$user->add_role('mp_users');
			$user_id = $new_mp_user;
			$user_login =  $mp_clientsinfo['client_name'];
			add_user_meta( $new_mp_user, '#','#');*/
			
		}
		
		$bookingdate1 = date_i18n('Y-m-d',strtotime('+1 day',strtotime(date_i18n('Y-m-d'))));
		$bookingdate2 = date_i18n('Y-m-d',strtotime('+3 day',strtotime(date_i18n('Y-m-d'))));
		$bookingdate3 = date_i18n('Y-m-d',strtotime('+5 day',strtotime(date_i18n('Y-m-d'))));
		
		$orderidbooking2 = $next_order_id+1;
		$orderidbooking3 = $next_order_id+2;
		
		
		$bookingids = array();
		$orderid = array();
		$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_bookings (`id`,`order_id`, `client_id`,`source_city`, `via_city`,`destination_city`,`move_size`, `booking_date`, `quote_price`, `quote_detail`, `service_info`, `loading_info`, `unloading_info`, `additional_info`, `user_info`, `booking_status`, `reminder`, `notification`, `lastmodify`,`insurance_rate`,`cubic_feets_article`,`cubic_feets_volume`) VALUES ('','".$next_order_id."','".$mpclientids[0]."','RGVudmVy','UGFsaXNhZGU=','UG9ydGxhbmQ=','1BHK','".$bookingdate1."','125.6','','".$serviceinfo1."','".$loadinginfo1."','".$unloadinginfo1."','".$additionalinfo1."','".$userinfo1."','P','0','0','".date_i18n('Y-m-d H:i:s')."','','".$booking_custom_cubic1."','')");
		$bookingids[] = $wpdb->insert_id;
		$query1="INSERT INTO ".$wpdb->prefix."mp_order_client_info (`id`, `order_id`, `client_name`, `client_email`, `client_phone`, `client_personal_info`) VALUES ('', '".$next_order_id."', 'Jamie L smith', 'jamie@gmail.com', '9056565666', '');";
		$wpdb->query($query1);
		$orderid[] = $wpdb->insert_id;
		
		/* Add 2nd Bookings */
		$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_bookings (`id`,`order_id`, `client_id`, `source_city`, `via_city`,`destination_city`,`move_size`, `booking_date`, `quote_price`, `quote_detail`, `service_info`, `loading_info`, `unloading_info`, `additional_info`, `user_info`, `booking_status`, `reminder`, `notification`, `lastmodify`,`insurance_rate`,`cubic_feets_article`,`cubic_feets_volume`) VALUES ('','".$orderidbooking2."','".$mpclientids[1]."','V2FzaGluZ3Rvbg==','QmFsdGltb3Jl','UGhpbGFkZWxwaGlh','2BHK','".$bookingdate2."','112.7','','".$serviceinfo2."','".$loadinginfo2."','".$unloadinginfo2."','".$additionalinfo2."','".$userinfo2."','P','0','0','".date_i18n('Y-m-d H:i:s')."','','".$booking_custom_cubic1."','')");
		$bookingids[] = $wpdb->insert_id;
		
		/* order_client_info */
		$query1="INSERT INTO ".$wpdb->prefix."mp_order_client_info (`id`, `order_id`, `client_name`, `client_email`, `client_phone`, `client_personal_info`) VALUES ('', '".$orderidbooking2."', 'Linda', 'lindasmith@gmail.com', '9876543210', '');";
		$wpdb->query($query1);
		$orderid[] = $wpdb->insert_id;
		
		/* Add 3rd Bookings */
		$wpdb->query("INSERT INTO ".$wpdb->prefix."mp_bookings (`id`,`order_id`, `client_id`, `source_city`, `via_city`,`destination_city`,`move_size`, `booking_date`, `quote_price`, `quote_detail`, `service_info`, `loading_info`, `unloading_info`, `additional_info`, `user_info`, `booking_status`, `reminder`, `notification`, `lastmodify`,`insurance_rate`,`cubic_feets_article`,`cubic_feets_volume`) VALUES ('','".$orderidbooking3."','".$mpclientids[2]."','Q2hpY2Fnbw==','TWlubmVhcG9saXM=','U2VhdHRsZQ==','3BHK','".$bookingdate3."','119.65','','".$serviceinfo3."','".$loadinginfo3."','".$unloadinginfo3."','".$additionalinfo3."','".$userinfo3."','P','0','0','".date_i18n('Y-m-d H:i:s')."','','".$booking_custom_cubic1."','')");
		$bookingids[] = $wpdb->insert_id; 
		$query1="INSERT INTO ".$wpdb->prefix."mp_order_client_info (`id`, `order_id`, `client_name`, `client_email`, `client_phone`, `client_personal_info`) VALUES ('', '".$orderidbooking3."', 'John', 'johnmartin@demo.com', '7463543210', '');";
		$wpdb->query($query1);
		$orderid[] = $wpdb->insert_id;
		
		
		$sampledataids = array('roomtype'=>implode(',',$room_type_infoids),'article_info'=>implode(',',$article_infoids),'additional_infoids'=>implode(',',$additional_infoids),'movesize'=>implode(',',$move_size_infoids),'pricingrules'=>implode(',',$pricing_rules_infoids),'floorinfo'=>implode(',',$mp_floors_infoids),'elevatorinfo'=>implode(',',$elevator_infoids),'pack_unpack'=>implode(',',$packing_unpacking_infoids),'art_cat_infoids'=>implode(',',$art_cat_infoids),'mpclients'=>implode(',',$mpclientids),'bookinginfo'=>implode(',',$bookingids),'orderinfo'=>implode(',',$orderid),'cubic_feets'=>implode(',',$cubic_ids));
		
		add_option('moveto_sample_dataids',serialize($sampledataids));
		$sampledata_st = get_option('moveto_sample_status');	
		if(isset($sampledata_st)){
			update_option('moveto_sample_status','N');	
		}else{
			add_option('moveto_sample_status','N');	
		}

		$min_rate_moveto = get_option('moveto_min_rate_service');	
		if(isset($min_rate_moveto)){
			update_option('moveto_min_rate_service','230');	
		}else{
			add_option('moveto_min_rate_service','230');	
		}

		$insurance_rate_moveto = get_option('moveto_insurance_rate');	
		if(isset($insurance_rate_moveto)){
			update_option('moveto_insurance_rate','30');	
		}else{
			add_option('moveto_insurance_rate','30');	
		}

		$insurance_permission_moveto = get_option('moveto_insurance_permission');	
		if(isset($insurance_permission_moveto)){
			update_option('moveto_insurance_permission','1');	
		}else{
			add_option('moveto_insurance_permission','1');	
		}

		$cubic_meter_moveto = get_option('moveto_cubic_meter');	
		if(isset($cubic_meter_moveto)){
			update_option('moveto_cubic_meter','ft_3');	
		}else{
			add_option('moveto_cubic_meter','ft_3');	
		}
		
	}


}