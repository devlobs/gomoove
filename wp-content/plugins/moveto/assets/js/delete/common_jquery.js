
 /* location */
jQuery("#location").on('change',function(){
	var ajaxurl = locationObj.plugin_path;
	var	location_id=this.value;
	var datastring={location_id:location_id,action:"get_services_view"};
	jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
			data:datastring,
			success:function(response){
			 jQuery(".get_service").html(response);
			}
	});
});


/* service */
 jQuery("#service").on('change',function(){
	var ajaxurl = locationObj.plugin_path;
	var	service_id=this.value;
	var datastring={service_id:service_id,action:"get_staff_view"};
	jQuery.ajax({
			type:"POST",
			url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
			
			data:datastring,
			success:function(response){
			var splitted = response.split("####");	
			jQuery(".get_staff").html(splitted[0]);
			jQuery(".get_view_addons").html(splitted[1]);
			}
	});
}); 

 /* staff */
 jQuery(document).on('change',"#staff",function(){
	var ajaxurl = locationObj.plugin_path;
	var	service_id=this.value;
	var datastring={action:"get_cal_on_staff_change"};
	jQuery.ajax({
		type:"POST",
		url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
		data:datastring,
		success:function(res){
			jQuery('.load_calendar').html(res);
		}
	});
}); 

jQuery(document).on('click','.selected_date', function() {
	alert('date');
	var ajaxurl = locationObj.plugin_path;
	jQuery('.bdp-week').each(function(){
	jQuery(this).removeClass('active');		
	jQuery('.bdp-show-time').hide();
	jQuery('.bdp-show-time').removeClass('shown');
	});	
	jQuery('.content_width').removeClass('col-lg-12');
	jQuery('.content_width').removeClass('col-md-12');
	jQuery('.content_width').removeClass('col-sm-12');
	
	jQuery('.content_width').addClass('col-lg-8');
	jQuery('.content_width').addClass('col-md-7');
	jQuery('.content_width').addClass('col-sm-6');		
	 
	jQuery(this).addClass('active');
	jQuery('.bdp-show-time').show( "blind", {direction: "vertical"}, 500 );
	jQuery('.bdp-show-time').addClass('shown');
	
	var id = jQuery(this).data('id');
	var selected_dates = jQuery(this).data('selected_dates');
	var s_date = jQuery(this).data('s_date');
    var cur_dates = jQuery(this).data('cur_dates');
    var c_date = jQuery(this).data('c_date');
	var getdata =  {id:id,selected_dates:selected_dates,action:'get_slots_on_date_selection'};
	
	jQuery('.bdp-show-time').html('');
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			jQuery('.time_slot_box').hide();
            jQuery('.display_selected_date_slots_box'+id).html(response);
            jQuery('.display_selected_date_slots_box'+id).show();
		}
	});
});



 jQuery(document).on('click','.month_change', function() {
	 var ajaxurl = locationObj.plugin_path;
	 var next_year = jQuery(this).data('next_month_year');
	 var next_month = jQuery(this).data('next_month');
	 
	
	 var getdata =  {year:next_year,month:next_month,action:'get_cal_on_cal_prev_next'}	
	 
	 jQuery.ajax({
			url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
			type : 'POST',
			data : getdata, 
			dataType : 'html',
			success  : function(response) {
				
			 jQuery('.load_calendar').html(response);
			}
		});
	 
 });



/* jQuery(".get_addons").on('change',function(){
	
	
	var ajaxurl = locationObj.plugin_path;
	var addons_service_id = this.value;
	alert('hello');
	var ajaxurl = obj_locations.ajaxurl;
	var dataString = {addons_service_id : addons_service_id,action :'get_addon_view'};

	 jQuery.ajax({
		type:"POST",
		data:dataString,
		url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
		success:function(response){
		
		alert(response);
	 	jQuery('.mp_show_addons'+addons_service_id).html(response);		
		jQuery('.mp_show_addons'+addons_service_id).toggle('slow'); 
		
		
			}
		
	}); 
});  */

jQuery(document).on('click','.addons_checkbox',function() {
	var ajaxurl = locationObj.plugin_path;
	var allVals =  [];
	var allVals2 = [];
	var allprice = [];

	jQuery('.addons_checkbox:checked').each(function() {
		allVals.push(jQuery(this).val());
		allVals2.push(jQuery(this).data('a_id'));
		allprice.push(jQuery(this).data('a_price')); 
		var addons_title = allVals.join(",");
		var addons_id = allVals2.join(",");
		var addons_price  = allprice.join(',');
	
		var dataString = {addons_title : addons_title,addons_id:addons_id,addons_price:addons_price,action :'get_addons_value'}
		jQuery.ajax({
		type:"POST",
		data:dataString,
		url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
		success:function(response){	
			}
			});
		});
	});

jQuery(document).on('click','.time_slotss',function() {
	 var ajaxurl = locationObj.plugin_path;
	 var slot_db_date_time = jQuery(this).data('slot_db_date_time');
	
	
	jQuery('.mp-loading-main').show();
	var ajaxurl = locationObj.plugin_path;
	jQuery('.bdp_datetime_slots').each(function(){
		jQuery(this).removeClass('selected');		
	});	
	jQuery('#'+jQuery(this).data('proid')).addClass('selected');
	// jQuery("#bdp_next_btn").css("display","none");
	/* top progress bar */
	jQuery('.bdp-date-time').removeClass('is-active');
	jQuery('.bdp-date-time').addClass('is-complete');
	jQuery('.bdp-details').addClass('is-active');	
	var slot_db_date_time = jQuery(this).data('slot_db_date_time');
	
	var getdata =  {slot_db_date_time:slot_db_date_time, action:'get_personal_details_view'}
	/* var getdata =  {slot_db_date_time:slot_db_date_time, action:'get_cart_view'} */
	jQuery('.bdp-loader').css("display","block");
	jQuery.ajax({
		url  : ajaxurl+"/assets/lib/front_demo_ajax.php",
		type : 'POST',
		data : getdata, 
		dataType : 'html',
		success  : function(response) {
			/*
			var cp = jQuery.trim(response);
			var cp_details = cp.split('###########');
			jQuery('#bdp_wrapper_inner').html(cp_details[0]);
			jQuery('.cart_section').html(cp_details[1]);
			jQuery(".content-header").html("5. Personal Details & Payment");
			jQuery('.bdp-wrapper-inner-class').effect("slide", { direction: "right" }, 500);
			jQuery('#add_more_services').show();
			jQuery('#total_item').html(jQuery('.hdn_total').val());
			jQuery('.mp-loading-main').hide(); 
			*/
		}
	}); 
		jQuery('.bdp-loader').css("display","none");
});
	

