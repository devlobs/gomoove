<?php
class moveto_room_type
{    
    /* object properties */
    public $id;
	public $name;
	public $status;
	public $room_type_id;
	
    
	/**
     * create room type table
     */ 
	function create_table_room_type() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_room_type';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(50) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	dbDelta($sql);     
			}
	}
	/* Real All room type */
	function readAll_room_type()
	{
		global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_room_type'."  ORDER BY  id ASC";
		$stmt = $wpdb->get_results($queryString); 
		return $stmt;
	}
	
	function readAll_room_type_for_front()
	{
		global $wpdb;
		$queryString = "SELECT `mrt`.`id`,`mrt`.`name` FROM `".$wpdb->prefix."mp_room_type` as `mrt`,`".$wpdb->prefix."mp_article_category` as `mac` where `mrt`.`id`=`mac`.`room_type_id` AND `mrt`.`status`='E' GROUP BY `mrt`.`id`,`mrt`.`name`,`mac`.`room_type_id` ORDER BY `mrt`.`id` ASC";
		$stmt = $wpdb->get_results($queryString); 
		return $stmt;
	}
	
	/* Add New room_type */
	function insert_room_type()
	{
		global $wpdb;	
		  $ins_room_type = $wpdb->query("INSERT INTO ".$wpdb->prefix .'mp_room_type'." (name)values('".$this->name."')");
		 return $ins_room_type;
	}
	/* update room_type */
	function room_type_update()
    {
        global $wpdb;
		echo "UPDATE ".$wpdb->prefix."mp_room_type SET name='".$this->name."' WHERE	id = " . $this->id;
		$update_room_type = $wpdb->query("UPDATE ".$wpdb->prefix."mp_room_type SET name='".$this->name."' WHERE	id = " . $this->id); 
		return $update_room_type;		
    }
	/* delete room_type */
	function room_type_delete()
    {
        global $wpdb;
		$delete_article_category   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_room_type  WHERE id =" . $this->id);
		$delete_article_category   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_article_category  WHERE room_type_id =" . $this->id);
        return $delete_article_category;
    }
	
	/* delete article_category */
	function article_cat_delete()
    {
        global $wpdb;
		$delete_article_category   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_article_category  WHERE id =" . $this->id);
        return $delete_article_category;
    }

    function get_article()    
    {        
		global $wpdb;        		        
		$queryString = "SELECT	* FROM  ".$wpdb->prefix."mp_articles as article 
		LEFT JOIN ".$wpdb->prefix."mp_article_category as article_cat  on (article.id = article_cat.article_id)
		WHERE	room_type_id='".$this->id."'";	

		$result = $wpdb->get_results($queryString);        
		return $result;   
	}

	function get_article_type()
	{
		global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix ."mp_article_category WHERE status='E' AND room_type_id='".$this->id."'";
		$stmt = $wpdb->get_results($queryString); 
		return $stmt;
	}
	
	function get_subarticle()
	{
			global $wpdb;
	$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_articles'." WHERE status='E' AND id='".$this->id1."'";
	$stmt = $wpdb->get_results($queryString); 
			return $stmt;
	}

}
?>