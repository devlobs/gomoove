<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	$additional_info = new moveto_additional_info();
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	
	
/* Delete Additional Info Permanently */		
if(isset($_POST['additional_info_action'],$_POST['additional_info_id']) && $_POST['additional_info_action']=='delete_additional_info' && $_POST['additional_info_id']!=''){
		$additional_info->id = $_POST['additional_info_id'];
		$additional_info->delete(); 
}	

/* Get Additional Info Title  */		
if(isset($_POST['additional_info_action'],$_POST['additional_info_id']) && $_POST['additional_info_action']=='get_additional_info_title' && $_POST['additional_info_id']!=''){
		$additional_info->id= $_POST['additional_info_id'];
		$additional_info->readName(); 
		echo $additional_info->additional_info;die();
}	
				
/* Create New Additional Info */	
if(isset($_POST['additional_info_action'],$_POST['additional_info_title']) && $_POST['additional_info_action']=='create_additional_info' && $_POST['additional_info_title']!=''){
		$additional_info->status= 'E';
		$additional_info->additional_info= $_POST['additional_info_title'];
		$additional_info->type= $_POST['additional_type'];
		$additional_info->price= $_POST['additional_price'];
		$additional_info->create();
}
/* Update Additional Info Status*/	
if(isset($_POST['additional_info_action'],$_POST['additional_info_title']) && $_POST['additional_info_action']=='update_additional_info' && $_POST['additional_info_title']!=''){
		$additional_info->id= $_POST['additional_info_id'];
		$additional_info->additional_info= $_POST['additional_info_title'];
		$additional_info->type= $_POST['additional_type'];
		$additional_info->price= $_POST['additional_price'];
		$additional_info->update();
}

/* Change Additional Info Status*/	
if(isset($_POST['additional_info_action'],$_POST['additional_info_id']) && $_POST['additional_info_action']=='update_additional_info_status' && $_POST['additional_info_id']!=''){
		$additional_info->id = $_POST['additional_info_id'];
		$additional_info->status = $_POST['additional_info_status'];
		$additional_info->update_status();
}