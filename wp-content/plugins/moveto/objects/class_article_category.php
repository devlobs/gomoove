<?php
class moveto_article_category
{    
    /* object properties */
    public $id;
	public $name;
	public $status;
	public $article_id;
	public $room_type_id;

	/**
     * create Article_category table
     */ 
	function create_table_article_category() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_article_category';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(50) NOT NULL,
			`status` enum('E','D') NOT NULL DEFAULT 'E',
			`article_id` varchar(50) NOT NULL,
			`room_type_id` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	dbDelta($sql);     
			}
	}
	/* Add New article category */
	function insert_article_category()
	{
		global $wpdb;	
		
		  $ins_room_type = $wpdb->query("INSERT INTO ".$wpdb->prefix .'mp_article_category'." (name,room_type_id)values('".$this->name."','".$this->room_type_id."')");
		 return $ins_room_type;
	}
	
function readOne_room_type()
	{
		global $wpdb;
		 $queryString = "SELECT * FROM ".$wpdb->prefix .'mp_room_type'."  where id='".$this->id."'";
		$stmt = $wpdb->get_results($queryString); 
		return $stmt;
	}
	/* Real All article_category */
	function readAll_article_category()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_article_category'." where room_type_id='".$this->room_type_id."' ORDER BY  room_type_id ASC";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    }
	/* Real All article_category_by_art */
	function readAll_article_category_by_art()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_article_category'." where id='".$this->id."' ORDER BY  room_type_id ASC";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    }
function readone_roomm_type()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_room_type'." where id='".$this->room_type_id."'";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    }
	function readone_roomm_type_art_cat_id()
    {
        global $wpdb;
		$queryString = "SELECT * FROM ".$wpdb->prefix .'mp_article_category'." where id='".$this->id."'";
		$stmt = $wpdb->get_results($queryString); 
        return $stmt;
    }
	/* update article_category */
	function article_category_update()
    {
        global $wpdb;
		$update_addons = $wpdb->query("UPDATE ".$wpdb->prefix."mp_article_category SET name='".$this->name."' WHERE	id = " . $this->id);  
    }
	/* delete article_category */
	function article_category_delete()
    {
        global $wpdb;
		$delete_article_category   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_article_category  WHERE id =" . $this->id);
        return $delete_article_category;
    }
	
}
?>