<?php 
include(dirname(__FILE__).'/header.php');
$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));

	global $current_user;
	$current_user = wp_get_current_user();
	
	$mp_service = new moveto_service();
	$moveto_additional_info = new moveto_additional_info();
	$bookings = new moveto_booking();
	$clients = new moveto_clients();
	
	$bookings->client_id=$current_user->ID;
	$current_user_bookings=$bookings->get_distinct_bookings_of_client();	
	$total_rows = sizeof((array)$current_user_bookings);
if($total_rows > 0){ ?>
<div id="mp-user-appointments">

	<div class="panel-body">	
		<div class="tab-content">
			<h4 class="header4"><?php echo __("My Quote Requests","mp");?>
			<!-- <span class="pull-right header3"><?php //echo $curr_bal;?> : <?php //echo __("Loyalty Points","mp");?></span> --> <span><a class="btn btn-default" href="<?php echo site_url();?>"><?php echo __("Back to site","mp");?></a></span></h4>
		<form>
					<div class="table-responsive">
						<table id="user-profile-booking-table" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th><?php echo __("id","mp");?></th>
									<th><?php echo __("Service","mp");?></th>
									<th><?php echo __("Source","mp");?></th>
									<th><?php echo __("Destination","mp");?></th>
									<th><?php echo __("Lead Date","mp");?></th>
									<th><?php echo __("Actions","mp");?></th>
								</tr>
							</thead>
							<tbody>
							<?php 
							
							foreach($current_user_bookings as $current_user_booking){	
									
									if($current_user_booking->service_id==1){
										$service_title = __('Home','mp');
									}elseif($current_user_booking->service_id==2){
										$service_title = __('Office','mp');
									}elseif($current_user_booking->service_id==3){
										$service_title = __('Vehicle','mp');
									}elseif($current_user_booking->service_id==4){
										$service_title = __('Pets','mp');
									}elseif($current_user_booking->service_id==6){
										$service_title = __('Other','mp');
									}else{
										$service_title = __('Commercial','mp');
									}
									  
									if($current_user_booking->booking_status=='D'){
										$booking_status = __('Completed','mp');
									}else{
										$booking_status = __('Pending','mp');
									}	
									
									$booking_date = date_i18n('Y-m-d',strtotime($current_user_booking->booking_date));
									$source_city = base64_decode($current_user_booking->source_city);
									$destination_city = base64_decode($current_user_booking->destination_city);
									
			
							
									?>								
									<tr data-oid="<?php echo ($current_user_booking->order_id);?>">
										<td><?php echo __($current_user_booking->order_id,"mp");?></td>
										<td><?php echo __($service_title,"mp");?></td>
										<td><?php echo __($source_city,"mp");?></td>
										<td><?php echo __($destination_city,"mp");?></td>
										<td><?php echo date_i18n(get_option('date_format'),strtotime($booking_date));?></td>
										
										<td>
										
											<a href="#user-booking-details" data-bid="<?php echo $current_user_booking->id;?>" data-toggle="modal" data-target="#user-booking-details" class="mp-my-booking-user btn btn-info moveto_client_bookings"><i class="fa fa-eye icon-space"></i><?php echo __("View Detail","mp");?></a>
										</td>										
								</tr>
									<?php } ?>															
							</tbody>
						</table>
					</div>	
				
					<div id="user-booking-details" class="modal fade booking-details-calendar" tabindex="-1" role="dialog" aria-hidden="true"> <!-- modal pop up start -->
						<div class="vertical-alignment-helper">
							<div class="modal-dialog modal-md vertical-align-center">
								<div class="modal-content">
									<div class="modal-header">
									
										<button type="button" class="close close_booking_detail_modal" data-dismiss="modal" aria-hidden="true">×</button>
										<h4 class="modal-title" style="margin-top: 10px; margin-bottom: 0;"><?php echo __("Lead Details","mp");?> </h4>
										
										
									</div>
									<div class="modal-body">
										<ul class="list-unstyled mp-cal-booking-details">
											<li>
												<label><?php echo __("Lead Status","mp");?></label>
												<div class="mp-booking-status"><span class="badge animated pulse span-scroll" style="background-color: #31bf57;"></span></div>
											</li>							
											<li>
												<label><?php echo __("Moving service","mp");?></label>
												<span class="mp_service span-scroll span_indent"> </span>
											</li>
											<li>
												<label><?php echo __("Lead Date","mp");?></label>
												<span class="mp_booking_date span-scroll span_indent"> </span>
											</li>
											<li>
												<label><?php echo __("Source","mp");?></label>
												<span class="mp_source_city span-scroll span_indent"></span>
											</li>
											<li>
												<label><?php echo __("Destination","mp");?></label>
												<span class="mp_destination_city span-scroll span_indent"></span>
											</li>
											
											<li class="service_info"></li>
											<li class="articles_info"></li>
											<li class="additional_info"></li>
											<li class="loading_info"></li>
											<li class="unloading_info"></li>
											
											<li><h5 class="mp-customer-details-hr" style="font-weight: 600;"><?php echo __("Customer","mp");?></h5>
											</li>
											<li>
												<label><?php echo __("Name","mp");?></label>
												<span class="client_name span-scroll span_indent"></span>
											</li>
											<li>
												<label><?php echo __("Email","mp");?></label>
												<span class="client_email span-scroll span_indent"></span>
											</li>
											<li>
												<label><?php echo __("Address","mp");?></label>
												<span class="client_address span-scroll span_indent"></span>
											</li>
											<li>
												<label><?php echo __("Phone","mp");?></label>
												<span class="client_phone span-scroll span_indent"></span>
											</li>							
											<li>
												<label><?php echo __("Notes","mp");?></label>
												<span class="client_notes span-scroll span_indent"></span>
											</li>
											<li class="quo_price">
												<label><?php echo __("Quote Price","mp");?></label>
												<span class="quote_price span-scroll span_indent"></span>
											</li>
											<span class="custom_info">								
											</span>											
										</ul>
									</div>
									<div class="modal-footer">
										<div class="mp-col12 mp-footer-popup-btn">								
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>				
				</div>
				</form>
		</div>
	</div>
<?php }else{ ?> 
<div><?php echo __("No Quote Requests Found.","mp");?></div>
<?php 
}
	include(dirname(__FILE__).'/footer.php');
?>
