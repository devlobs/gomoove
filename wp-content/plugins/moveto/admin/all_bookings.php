<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Location */
	
	$bookings = new moveto_booking();	
	$mp_service = new moveto_service();
	$moveto_additional_info = new moveto_additional_info();
	$all_bookings = $bookings->readAll_listing_bookings();
	$roomtype_obj=new moveto_room_type();
	$moveto_article_category_obj=new moveto_article_category();
?>
<script>
jQuery(document).bind('ready ajaxComplete', function(){
	jQuery('#add1').hide();
});	
</script>
<div id="mp-export-details" class="panel tab-content">
	<div class="panel panel-default">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#booking-info-export"><?php echo __("All Leads","mp");?></a></li>			
		</ul>
		
		<div class="tab-content">
			<!-- booking infomation export -->
			<div id="booking-info-export" class="tab-pane fade in active">
				<h3><?php echo __("Lead Information","mp");?></h3>
				<div id="accordion" class="panel-group">					
					<form id="" name="" class="" method="post">						
						<div class="table-responsive">
							<table id="mp_export_bookings" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>	
										<th style="width: 9px  !important;"><?php echo __("id","mp");?></th>
										<th style="width: 9px  !important;"><?php echo __("Move Size","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Source","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Via","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Destination","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Lead Date","mp");?></th>
										<th style="width: 72px !important;"><?php echo __("Article Info","mp");?></th>						
										<th style="width: 65px !important;"><?php echo __("Additional Info","mp");?></th>
										<th style="width: 44px !important;"><?php echo __("Loading Details","mp");?></th>
										<th style="width: 39px !important;"><?php echo __("Unloading Details","mp");?></th>	
										<th style="width: 39px !important;"><?php echo __("Total Cubic Volume","mp");?></th>
										<th style="width: 39px !important;"><?php echo __("Insurance Rate","mp");?></th>	
										
										<th style="width: 257px !important;"><?php echo __("Lead Client Info","mp");?></th>
										<th style="width: 257px !important;"><?php echo __("Quote Price","mp");?></th>
										<th style="width: 257px !important;"><?php echo __("Payment","mp");?></th>
										<th style="width: 257px !important;"><?php echo __("Actions","mp");?></th>
										
									</tr>
								</thead>
								<tbody id="mp_export_bookings_data">
									<?php foreach($all_bookings as $single_booking){ 
										  
										if($single_booking->booking_status=='D'){
											$booking_status = __('Completed','mp');
										}else{
											$booking_status = __('Pending','mp');
										}	
										
										$booking_date = date_i18n('Y-m-d',strtotime($single_booking->booking_date));
										$source_city = base64_decode($single_booking->source_city);
										$via_city = "";
										if($single_booking->via_city != ""){
											$via_city = base64_decode($single_booking->via_city);
										}else{
											$via_city = "N/A";
										}
										
										$destination_city = base64_decode($single_booking->destination_city);
										
										  
										if($single_booking->booking_status=='D'){
											$bookingstatus = __('Completed','mp');
										}else{
											$bookingstatus = __('Pending','mp');
										}
										/* Get User Info */
										if($single_booking->user_info!=''){
											$userinfo = unserialize($single_booking->user_info);
											if($userinfo != '')
											{
												$pam_email = $userinfo['email'];
												$pam_first_name = ucwords($userinfo['name']);
												$pam_phone = $userinfo['phone'];
											}
										}else{
											$pam_email = '';		$pam_first_name = '';		$pam_phone = '';
											$pam_custom_fields = array();
										}
										/* Get Service Info */

										$service_info = '';
										$home_type_info = '';

										if($single_booking->service_info!=''){
											if($single_booking->move_size){
												$home_type_info = $single_booking->move_size;	
											}else{
												$home_type_info = "N/A";
											}
											

											$service_info_arr = unserialize($single_booking->service_info);
											
											$service_info_cubic_arr = unserialize($single_booking->cubic_feets_article);
											
											//$service_custom_cubic = $service_info_arr['article_cubic'];
												$service_cubic = '';
											if(is_array($service_info_cubic_arr) || is_object($service_info_cubic_arr))
											{
												$service_cubic .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Custom Article :',"mp").'</h5></li>';

												foreach ($service_info_cubic_arr as $value) 
												{
													if(isset($value['cubicfeet_title'])) {
														$service_cubic .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Name',"mp").'</h5><span class="span-scroll span_indent">'.$value['cubicfeet_title'].'</li>';

														$service_cubic .= '<li style = "list-style: none;margin-left: 10%;"><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Quanty',"mp").'</h5><span class="span-scroll span_indent">1</li>';
														
														$service_cubic .= '<li style = "list-style: none;margin-left: 10%;"><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Cubic Volume',"mp").'</h5><span class="span-scroll span_indent">'.$value['cubicfeet_count'].'</li>';


													}
												}
											}

											$service_total_cubic_volume = $single_booking->cubic_feets_volume;
											$service_total_cubicvolume = '';
											if($service_total_cubic_volume!='')
											{
												/*$service_total_cubicvolume = '<li><span class="span-scroll span_indent">'.$service_total_cubic_volume.'</li>';*/

												/*$service_total_cubicvolume = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Insurance',"mp").'</h5><span class="span-scroll span_indent">'.$service_total_cubic_volume.'</li>';*/
												 
											}

											$service_insurance = $single_booking->insurance_rate;
											$service_total_insurance = '';
											if($service_insurance!='')
											{
													$service_total_insurance = '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Insurance',"mp").'</h5><span class="span-scroll span_indent">'.$service_insurance.'</li>';
												 
											}
											
											 


											/* $article_name = $service_info_arr['get_article_name']; */

											foreach ($service_info_arr as $key => $value) {
												 
												if(count((array)$value) > 0){
												if(isset($value['get_article_room_id'])) {
													 $room_id=$value['get_article_room_id'];
												}
												if(isset($value['get_article_id'])) {
													$article_id=$value['get_article_id'];
												}	
												if(isset($value['get_article_price'])) {
													$article_price=$value['get_article_price'];
												}	
													
													$moveto_article_category_obj->id=$room_id;
													$room_type_name_array= $moveto_article_category_obj->readOne_room_type();
													
											/* 		$room_type_name= $room_type_name_array[0]->name;
													if(isset($room_type_name)){ */
													/*$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Room Type',"mp").'</h5><span class="span-scroll span_indent">'.$room_type_name.'</span></li>';*/
											/* 	} */
													//$service_info .= '<li><span class="span-scroll span_indent">'.$value['get_article_name'].'</span><span class="span-scroll span_indent">'.$value['count'].'</span></li>';	
													if(isset($value['get_article_name'])) {
													$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Name',"mp").'</h5><span class="span-scroll span_indent">'.$value['get_article_name'].'</li>';
													}
													if(isset($value['count'])) {													
													$service_info .= '<li style = "list-style: none;margin-left: 10%;"><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Quanty',"mp").'</h5><span class="span-scroll span_indent">'.$value['count'].'</li>';
													}
													if(isset($value['per_quibcfeets_volume'])) { 
													$service_info .= '<li style = "list-style: none;margin-left: 10%;"><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Cubic feets Volume',"mp").'</h5><span class="span-scroll span_indent">'.$value['per_quibcfeets_volume'].'</li>';
													}
													/*if(isset($value['tot_article_price'])) {
													$service_info .= '<li style = "list-style: none;margin-left: 10%;"><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Total Price',"mp").'</h5><span class="span-scroll span_indent">'.$value['tot_article_price'].'</li>';
													}*/

													
													}
													}
												}
										
										/* Get Additional Info */

											$additional_info = '';
											if($single_booking->additional_info!=''){
												
												$additional_info_arr = unserialize($single_booking->additional_info);
												$i=1;
												if(sizeof((array)$additional_info_arr['favorite'])>0){

														$returndata = $moveto_additional_info->readAll();

														$additionalArr = array();
														foreach ($additional_info_arr['favorite'] as $value) {
														array_push($additionalArr, $value);
														}
															foreach($returndata as $additional){

																if(in_array($additional->id, $additionalArr)){
																	$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$additional->additional_info.'</h5><span class="span-scroll span_indent">Yes</span></li>';
																}else
																{

																	$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$additional->additional_info.'</h5><span class="span-scroll span_indent">No</span></li>';
																}

															}

													}	
													
											}
										/* Get Loading Info */

										$loading_info = '';
										if($single_booking->loading_info!=''){										
											$loading_info_arr = unserialize($single_booking->loading_info);	
												
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Loading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_number"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price Of Loading Floor',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_checked"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator Price',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Packaging  ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["packaging_checked"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Packaging Price ',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["packaging_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Street Address',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_one"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_two"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_three"].'</span></li>';
																				
										}
										/* Get UnLoading Info */
										$unloading_info = '';
										if($single_booking->unloading_info!=''){										
											$unloading_info_arr = unserialize($single_booking->unloading_info);	
																				
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unloading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_number"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price of Unloading Floor ',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_checked"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator Price',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Unpackaging ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_checked"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Unpackaging Price',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Street Address',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_one"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_two"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_three"].'</span></li>';
											
										}
										
										/* Get Booking Client Info */
										
										$user_info = '';
									
										if($single_booking->user_info!=''){										
											$userinfo = unserialize($single_booking->user_info);
											$pam_email = $userinfo['email'];
											$pam_name = ucfirst($userinfo['name']);
											$pam_phone = $userinfo['phone'];
											
											/* $user_notes	= $user_info_arr["user_notes"];		 */							
											$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Name',"mp").'</h5><span class="span-scroll span_indent">'.$pam_name.'</span></li>';
											$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Email',"mp").'</h5><span class="span-scroll span_indent">'.$pam_email.'</span></li>';
											$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Phone',"mp").'</h5><span class="span-scroll span_indent">'.$pam_phone.'</span></li>';
										
											/* $pam_custom_fields = unserialize($user_info_arr['custom_fields_details']);
											if(sizeof($pam_custom_fields)>0){
												if (is_array($pam_custom_fields) || is_object($pam_custom_fields))
												{
													foreach($pam_custom_fields as $fieldkey => $fieldvalue){
														$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$fieldkey.'</h5><span class="span-scroll span_indent">'.$fieldvalue.'</span></li>';
													}
												}												
											} */
													
										}
										if($single_booking->payment_method != ""){
											$payment = ucwords(str_replace("_", " ", strtolower($single_booking->payment_method)));
										}else{
											$payment = "Unpaid";
										}

										$service_info_all = $service_info."</br>".$service_cubic;
										
										?>
										<tr>
											<td><?php echo $single_booking->order_id; ?></td>
											<td><?php echo $home_type_info;?></td>
											<td><?php echo $source_city; ?></td>
											<td><?php echo $via_city; ?></td>
											<td><?php echo $destination_city; ?></td>
											<td><?php echo date_i18n(get_option('date_format'),strtotime($booking_date)); ?></td>
											<td><?php echo $service_info_all; ?></td>
											<td><?php echo $additional_info; ?></td>
											<td><?php echo $loading_info; ?></td>
											<td><?php echo $unloading_info; ?></td>
											<td><?php echo $service_total_cubicvolume; ?></td>
											<td><?php echo $service_insurance; ?></td>
											<td><?php echo $user_info; ?></td>
											<?php if($single_booking->quote_price!=""){
												?>
												<td><?php echo get_option('moveto_currency_symbol').$single_booking->quote_price; ?></td>
												<?php
											}else{?>
											<td><?php echo "-"; ?></td>
											<?php } ?>
											<td><?php echo $payment; ?></td>
											<td><a href="#pam_send_quotes" data-id="<?php echo $single_booking->id; ?>" role="button" class="btn btn-large btn-primary pam_send_quote" data-toggle="modal"><?php echo __("Send Quote","mp");?></a>
											</td>
										</tr>
										<?php } ?>
								</tbody>
							</table>	
						</div>	
					</form>	
				</div>
			</div>						
		</div>
	</div>	
</div>		
<?php 
	if(get_option('moveto_currency_symbol_position')=='P'){
		$quote_price = '1000'.get_option('moveto_currency_symbol'); 
	}else{
		$quote_price = get_option('moveto_currency_symbol').'1000'; 
	}
?>

<div id="pam_send_quotes" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo __("Send Quote","mp");?></h4>
            </div>
           <form id="pam_send_quote_form" method="post">
						<div class="modal-body">
							<div class="update_appointment_res my_response">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pam_quote_cancel" data-dismiss="modal"><?php echo __("Cancel","mp");?></button>
							<button type="button" id="pam_submit_quote" class="btn btn-primary"><?php echo __("Send Now","mp");?></button>
						</div>
					</form>
        </div>
    </div>
</div>	
<?php 
	include(dirname(__FILE__).'/footer.php');
?>