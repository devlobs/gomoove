<?php 

	/* if uninstall not called from WordPress exit */
	if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
		exit;
	}

	include_once( 'objects/class_uninstall.php' );
	$uninstaller = new moveto_uninstall();
	
	/* remove ak wordpress roles */
	$uninstaller->remove_mp_roles();
	
	/* remove ak tables */
	$uninstaller->remove_mp_mysql_tables();
	
	/* remove ak wp options */
	$uninstaller->remove_mp_wp_options();
	
	/* remove ak wp Pages */
	$uninstaller->remove_mp_wp_pages();
	
	
?>