<?php 
session_start();
$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
			if (file_exists($root.'/wp-load.php')) {
			require_once($root.'/wp-load.php');
}
if ( ! defined( 'ABSPATH' ) ) exit;  /* direct access prohibited  */

	
	$article_category = new moveto_article_category();
	$general = new moveto_general();
	$staff = new moveto_staff();
	$plugin_url_for_ajax = plugins_url('',dirname(dirname(__FILE__)));
	$mp_currency_symbol = get_option('moveto_currency_symbol');
	
/* insert article category */	
if(!empty($_POST['action']) && $_POST['action']=='add_article_category'){
	$article_category->room_type_id = $_POST['room_type_iid'];
	$article_category->name = $_POST['category_title'];
	$article_category->insert_article_category();  
}
/* update article category */	
if(!empty($_POST['action']) && $_POST['action']=='update_article_cat'){
	$article_category->id = $_POST['article_cat_id'];
	$article_category->name = $_POST['article_cat_title'];
	$article_category->article_category_update();  
}
/* delete article category */	
if(!empty($_POST['action']) && $_POST['action']=='delete_category'){
	$article_category->id = $_POST['article_cat_id'];
	$article_category->article_category_delete();  
}

/* update room type status */
if(isset($_POST['action']) && $_POST['action'] == "article_cat_update_status"){
	$article_cat_id = $_POST['article_cat_id'];
	$status = $_POST['status'];	
	global $wpdb;
    $query = "UPDATE ".$wpdb->prefix."mp_article_category SET status ='$status' WHERE	id =$article_cat_id";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}


/* update room type art_id */
if(isset($_POST['action']) && $_POST['action'] == "article_cat_up_us"){
	$article_cat_id = $_POST['article_cat_id'];
	$selected_article = $_POST['selected_article'];	
	global $wpdb;
    $query = "UPDATE ".$wpdb->prefix."mp_article_category SET article_id ='$selected_article' WHERE	id =$article_cat_id";
	$get_resss = $wpdb->get_var($query);
	if($get_resss == 0){
		echo "true";
	}else{
		echo "false";
	}
}

 ?>		