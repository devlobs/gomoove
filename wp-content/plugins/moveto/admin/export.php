<?php 
	include(dirname(__FILE__).'/header.php');
	$plugin_url_for_ajax = plugins_url('', dirname(__FILE__));
	
	/* Create Location */
	
	$bookings = new moveto_booking();	
	$mp_service = new moveto_service();
	$moveto_additional_info = new moveto_additional_info();
	$roomtype_obj=new moveto_room_type();
	$moveto_article_category_obj=new moveto_article_category();
	$all_bookings = $bookings->readAll('','','','','Export');

?>
<script>
jQuery(document).bind('ready ajaxComplete', function(){
	jQuery('#add1').hide();
});	
</script>
<div id="mp-export-details" class="panel tab-content">
	<div class="panel panel-default">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#booking-info-export"><?php echo __("Lead Information","mp");?></a></li>			
		</ul>
		
		<div class="tab-content">
			<!-- booking infomation export -->
			<div id="booking-info-export" class="tab-pane fade in active">
				<h3><?php echo __("Lead Information","mp");?></h3>
				<div id="accordion" class="panel-group">
					
					<form id="" name="" class="" method="post">
						<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 mb-10">
							<label><?php echo __("Select option to show lead","mp");?></label>
							<div id="mp_reportrange" class="form-control" >
								<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
								<span></span> <i class="fa fa-caret-down"></i>
							</div>
							<input type="hidden" id="mp_booking_startdate" value="" />
							<input type="hidden" id="mp_booking_enddate" value="" />	
						</div>			
						<div class="col-md-2 col-sm-6 col-xs-12 col-lg-2 mb-10">
							<button type="button" id="mp_filtered_bookings" class="form-group btn btn-info mp-btn-width mp-submit-btn mt-20" name=""><?php echo __("Submit","mp");?></button>
						</div>
						<hr id="hr" />
						<div class="table-responsive">
							<table id="mp_export_bookings" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
								<thead>
									<tr>	
										<th style="width: 9px !important;"><?php echo __("Id","mp");?></th>
										<th style="width: 9px !important;"><?php echo __("Move Size","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Source","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Destination","mp");?></th>
										<th style="width: 48px !important;"><?php echo __("Lead Date","mp");?></th>
										<th style="width: 67px !important;"><?php echo __("Service Info","mp");?></th>						
										<th style="width: 70px !important;"><?php echo __("Additional Info","mp");?></th>
										<th style="width: 44px !important;"><?php echo __("Loading Details","mp");?></th>
										<th style="width: 39px !important;"><?php echo __("Unloading Details","mp");?></th>			
										<th style="width: 257px !important;"><?php echo __("Lead Client Info","mp");?></th>
										<th style="width: 257px !important;"><?php echo __("Quote Price","mp");?></th>
										</tr>
								</thead>
								<tbody id="mp_export_bookings_data">
									<?php foreach($all_bookings as $single_booking){ 
										  
										if($single_booking->booking_status=='D'){
											$booking_status = __('Completed','mp');
										}else{
											$booking_status = __('Pending','mp');
										}	
										
										$booking_date = date_i18n('Y-m-d',strtotime($single_booking->booking_date));
										$source_city = base64_decode($single_booking->source_city);
										$destination_city = base64_decode($single_booking->destination_city);
										  
										if($single_booking->booking_status=='D'){
											$bookingstatus = __('Completed','mp');
										}else{
											$bookingstatus = __('Pending','mp');
										}
										/* Get User Info */
										if($single_booking->user_info!=''){

											$userinfo = unserialize($single_booking->user_info);
											$pam_email = $userinfo['email'];
											$pam_first_name = ucwords($userinfo['name']);
											$pam_phone = $userinfo['phone'];
											
										}else{
											$pam_email = ''; $pam_first_name = '';	$pam_phone = '';
										}
										
										/* Get Service Info */
										
										$service_info = '';
										if($single_booking->service_info!=''){
											$home_type_info = $single_booking->move_size;

											$service_info_arr = unserialize($single_booking->service_info);

											foreach ($service_info_arr as $key => $value) {
												if(count((array)$value) > 0){
												if(isset($value['get_article_room_id'])) {
													 $room_id=$value['get_article_room_id'];
												}
												if(isset($value['get_article_id'])) {
													 $article_id=$value['get_article_id'];
												}
												if(isset($value['get_article_price'])) {
													 $article_price=$value['get_article_price'];
												}
													 

													$moveto_article_category_obj->id=$room_id;
													$room_type_name_array= $moveto_article_category_obj->readOne_room_type();
													if(!empty($room_type_name_array))
													{
														$room_type_name= $room_type_name_array[0]->name;
														$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Room Type',"mp").'</h5><span class="span-scroll span_indent">'.$room_type_name.'</span></li>';
												
													}
													if(isset($value['get_article_name'])) {
													$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Name',"mp").'</h5><span class="span-scroll span_indent">'.$value['get_article_name'].'</li>';
													}
													if(isset($value['count'])) {
													$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Qty',"mp").'</h5><span class="span-scroll span_indent">'.$value['count'].'</li>';
													}
													if(isset($value['get_article_price'])) {
													$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Base Price',"mp").'</h5><span class="span-scroll span_indent">'.$value['get_article_price'].'</li>';
													
													}
													if(isset($value['tot_article_price'])) {
													$service_info .= '<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Article Total Price',"mp").'</h5><span class="span-scroll span_indent">'.$value['tot_article_price'].'</li>';
													}

													
												}
											}
												}
										
											/* Get Additional Info */

											$additional_info = '';
											if($single_booking->additional_info!=''){
												
												$additional_info_arr = unserialize($single_booking->additional_info);
												
												if(sizeof((array)$additional_info_arr['favorite'])>0){

														$returndata = $moveto_additional_info->readAll();

														$additionalArr = array();
														foreach ($additional_info_arr['favorite'] as $value) {
														array_push($additionalArr, $value);
														}
															foreach($returndata as $additional){

																if(in_array($additional->id, $additionalArr)){
																	$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$additional->additional_info.'</h5><span class="span-scroll span_indent">Yes</span></li>';
																}else
																{

																	$additional_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.$additional->additional_info.'</h5><span class="span-scroll span_indent">No</span></li>';
																}

															}

													}	
													
											}
									
									/* Get Loading Info */

										$loading_info = '';
										if($single_booking->loading_info!=''){										
											$loading_info_arr = unserialize($single_booking->loading_info);	
												
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Loading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_number"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price Of Loading Floor',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_floor_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_checked"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Elevator Price',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["load_elevator_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Packaging  ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["packaging_checked"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Load Packaging Price ?',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["packaging_amt"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Street Address',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_one"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_two"].'</span></li>';
											$loading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$loading_info_arr["loading_address_three"].'</span></li>';
																				
										}
										/* Get UnLoading Info */
										$unloading_info = '';
										if($single_booking->unloading_info!=''){										
											$unloading_info_arr = unserialize($single_booking->unloading_info);	
																				
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unloading Floor no',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_number"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Price of Unloading Floor ',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_floor_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_checked"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Elevator Price',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unload_elevator_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Unpackaging ?',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_checked"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Unload Unpackaging Price',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unpackaging_amt"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Street Address',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_one"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('City',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_two"].'</span></li>';
											
											$unloading_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('State',"mp").'</h5><span class="span-scroll span_indent">'.$unloading_info_arr["unloading_address_three"].'</span></li>';
											
										}
										
										/* Get Booking Client Info [Lead Client Info:] */
							
										$user_info = '';
										if($single_booking->user_info!=''){										
											$userinfo = unserialize($single_booking->user_info);
											$pam_email = $userinfo['email'];
											$pam_name =ucfirst($userinfo['name']);
											$pam_phone = $userinfo['phone'];
											/* $firstn = ucwords($bookings->client_name);	 */
																			
											$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Name',"mp").'</h5><span class="span-scroll span_indent">'.$pam_name.'</span></li>';
											$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Email',"mp").'</h5><span class="span-scroll span_indent">'.$pam_email.'</span></li>';
											$user_info .='<li><h5 class="mp-customer-details-hr" style="font-weight: 600;">'.__('Phone',"mp").'</h5><span class="span-scroll span_indent">'.$pam_phone.'</span></li>';									
										}
										?>
										<tr>
											<td><?php echo $single_booking->order_id; ?></td>
											<td><?php echo $single_booking->move_size; ?></td>
											<td><?php echo $source_city; ?></td>
											<td><?php echo $destination_city; ?></td>
											<td><?php echo date_i18n(get_option('date_format'),strtotime($booking_date)); ?></td>
											<td><?php echo $service_info; ?></td>
											<td><?php echo $additional_info; ?></td>
											<td><?php echo $loading_info; ?></td>
											<td><?php echo $unloading_info; ?></td>
											<td><?php echo $user_info; ?></td>
											<?php if($single_booking->quote_price!=""){
												?>
												<td><?php echo $single_booking->quote_price; ?></td>
												<?php
											}else{?>
											<td><?php echo "-"; ?></td>
											<?php } ?>
										</tr>
										<?php } ?>
								</tbody>
							</table>	
						</div>	
					</form>	
				</div>
			</div>						
		</div>
	</div>	
</div>		
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
