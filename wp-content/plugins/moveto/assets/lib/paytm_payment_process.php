<?php
    //require_once("class_encdec_paytm.php");
    $a = session_id();  if(empty($a)) session_start();
		
		if($_SERVER['REQUEST_METHOD'] === 'POST'){
			$_SESSION['paytm_post_session'] = json_encode($_POST);
		}
		
    $root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__)))))); 
    if (file_exists($root.'/wp-load.php')) {
        require_once($root.'/wp-load.php'); 
    }   
    require(dirname(dirname(dirname(__FILE__))).'/objects/class_encdec_paytm.php');

    $plugin_url = plugins_url('',  dirname(__FILE__));
    $base =   dirname(dirname(dirname(__FILE__))); 
    
    /** Paytm - define credentials start**/
    $paytm_merchant_key = get_option('moveto_paytm_merchantkey');
    $paytm_merchant_id = get_option('moveto_paytm_merchantid');
    $paytm_website_url = get_option('moveto_paytm_website');

    define("PAYTM_MERCHANT_KEY",$paytm_merchant_key);
    define("PAYTM_MERCHANT_MID",$paytm_merchant_id); 
    define("PAYTM_MERCHANT_WEBSITE",$paytm_website_url);
		
  //  define("merchantMid", "DIY12386817555501617");
    // Key in your staging and production MID available in your dashboard
    //define("merchantKey", "bKMfNxPPf_QdZppa");
    // Key in your staging and production merchant key available in your dashboard
	$digits = 5;
	$digit_ok = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    
    $currency_code = get_option('octabook_currency'); /* 'USD'; */
    $ORDER_ID = 'ct'.time();
    $CUST_ID = mt_rand(1000, 9999).'_'.$_POST['fname'].'_'.$_POST['lname'];
    $INDUSTRY_TYPE_ID = get_option('moveto_paytm_industryid');
    $CHANNEL_ID = get_option('moveto_paytm_channelid');

    define("orderId", 'ct'.time());
    define("channelId", $CHANNEL_ID);
    define("custId", $CUST_ID);
    define("mobileNo", $_POST['phone']);
    define("email", $_POST['user_email']);
    define("txnAmount", $amt);
   define("website", $paytm_website_url);
   // define("currency", "INR");
    // This is the staging value. Production value is available in your dashboard
    define("industryTypeId", $INDUSTRY_TYPE_ID);
    // This is the staging value. Production value is available in your dashboard
    $paytm_return_url = $plugin_url.'/lib/paytm_success.php';
   // define("callbackUrl", "http://localhost/paytm/paytm_success.php");
    define("callbackUrl", $paytm_return_url);
		
		$amt = $_SESSION['final_amt'];
    $paytmParams = array();
    $paytmParams["MID"] = PAYTM_MERCHANT_MID;
    $paytmParams["ORDER_ID"] = orderId;
    $paytmParams["CUST_ID"] = $CUST_ID;
    $paytmParams["INDUSTRY_TYPE_ID"] =  $INDUSTRY_TYPE_ID;
    $paytmParams["CHANNEL_ID"] =  $CHANNEL_ID;
    $paytmParams["TXN_AMOUNT"] = $amt;
    /* $paytmParams["CURRENCY"] = currency; */
    $paytmParams["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
    $paytmParams["CALLBACK_URL"] = callbackUrl;
    $paytmParams["MOBILE_NO"] = mobileNo;
    $paytmParams["EMAIL"] = email;
    $paytmChecksum = getChecksumFromArray($paytmParams, PAYTM_MERCHANT_KEY);
    $paytmParams["CHECKSUMHASH"] = $paytmChecksum;
    $transactionURL = "https://securegw-stage.paytm.in/theia/processTransaction"; 
    $my_array['PAYTM_TXN_URL'] = $transactionURL;
    /*$transactionURL = "https://pguat.paytm.com/oltp-web/processTransaction";*/

    /* $transactionURL = "https://securegw.paytm.in/theia/processTransaction"; // for production */
    $Extra_form_fields = '';
    foreach($paytmParams as $name => $value) {
        $Extra_form_fields .= '<input type="hidden" name="' . $name .'" value="' . $value . '">';
    }
    $my_array['Extra_form_fields'] = $Extra_form_fields;
    echo json_encode($my_array);die;
?>