<?php 

/**
 * Class moveto Uninstall
 * Uninstalling moveto deletes user roles, tables, and options.
 *
 * @author      TeamBI
 */
 
	class moveto_uninstall{


		  /* remove all roles */
		  function remove_mp_roles() {
			   remove_role('mp_staff'); 
			   remove_role('mp_manager'); 
			   remove_role('mp_client');
		  }  
		  
		  
		  /* Remove database tables */
		  function remove_mp_mysql_tables() {			
				global $wpdb;				
			 /*	$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_bookings" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_categories" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_coupons" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_email_templates" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_locations" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_order_client_info" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_payments" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_providers_services" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_schdeule_offtimes" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_schedule" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_schedule_breaks" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_schedule_dayoffs" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_services" );
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_service_schedule_price");
				$wpdb->query( "DROP TABLE IF EXISTS ".$wpdb->prefix."mp_sms_templates");*/
		  }
		  /* remove wordpress options by moveto */
		  function remove_mp_wp_options() {
			global $wpdb;
			$wpdb->query("DELETE FROM ".$wpdb->prefix."options WHERE option_name LIKE 'moveto_%'");
		   }
		   
		   
		   /* remove moveto pages */
		  function remove_mp_wp_pages() {
				$pageTY = get_page_by_title( 'thankyou' );
				if($pageTY!=''){
					wp_trash_post($pageTY->ID);
				}
		  }		  
		 
	  
	}
?>