<?php
class moveto_city
{

    
    /* object properties */
    public $id;
    public $source_city;
    public $destination_city;
    public $price;

    
	
	/**
     * create moveto City table
     */ 
	function create_table() {
	global $wpdb;

	$table_name = $wpdb->prefix .'mp_cities';
	
	if( $wpdb->get_var( "show tables like '{$table_name}'" ) != $table_name ) {		
	
	$sql = "CREATE TABLE IF NOT EXISTS ".$table_name." (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `source_city` varchar(500) NOT NULL,
					  `destination_city` varchar(500) NOT NULL,					 
					  `price` varchar(250) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
	
	
	dbDelta($sql);     
			}
	} 
			
	/* Create New City */
    function create()
    {
        global $wpdb;
		
        $stmt = $wpdb->query("INSERT INTO  ".$wpdb->prefix."mp_cities	(id,source_city,destination_city,price)	values('','".$this->source_city ."','".$this->destination_city ."','".$this->price."')");
				
		$serice_id = $wpdb->insert_id;
        return $serice_id;
    }
    
 	/* Get All Cities */
    function readAll()
    {
        global $wpdb;
		
        $queryString = "SELECT * FROM ".$wpdb->prefix."mp_cities  ORDER BY	id ASC";
		
        $stmt = $wpdb->get_results($queryString); 
        return $stmt;
    }
    
	/* Update City Detail */
	 function update()
    {
        global $wpdb;
        $stmt = $wpdb->query("UPDATE ".$wpdb->prefix."mp_cities SET source_city='".$this->source_city."',destination_city ='" . $this->destination_city . "', price ='".$this->price . "' WHERE	id = " . $this->id);
		
        if ($stmt) {
            return true;
        } else {
            return false;
        }
    }
	
	
	/* Count All Cities */
    public function countAll()
    {
        
        global $wpdb;
        $stmt = $wpdb->get_results("SELECT id FROM  ".$wpdb->prefix."mp_cities");
        $num  = sizeof((array)$stmt);
        return $num;
    }
	  
    /* Read one city */    
    function readOne()
    {
        global $wpdb;
		$stmt = $wpdb->get_results("SELECT
						*
					FROM
						".$wpdb->prefix."mp_cities 
					WHERE
						id =" . $this->id . " 
					LIMIT
						0,1");
        foreach ($stmt as $row) {
			$this->id      			  = $row->id;
            $this->source_city        = $row->source_city;
            $this->destination_city   = $row->destination_city;
            $this->price    		  = $row->price;            
        }
    }
    
   
    
    /* delete the city */
    function delete()
    {
        global $wpdb;
        $stmt   = $wpdb->query("DELETE FROM ".$wpdb->prefix."mp_cities  WHERE id =" . $this->id);
        $result = $stmt;
        if ($result) {
            return true;
        } else {
            return false;
        }
    } 
	
	/* Check the city route */
    function check_route()
    {
        global $wpdb;
		
        $mainroute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cities  WHERE source_city ='".$this->source_city."' and destination_city='".$this->destination_city."'");
		  
	  
		$reverseroute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cities  WHERE source_city ='".$this->destination_city."' and destination_city='".$this->source_city."'");
		
		if(sizeof((array)$mainroute)>0 || sizeof((array)$reverseroute)>0){			
			echo  json_encode("Route is already exists.");
		}else{
			echo "true";
		}
    }
	
	/* Check the city route csv */
    function check_route_csv()
    {
        global $wpdb;
		
        $mainroute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cities  WHERE source_city ='".$this->source_city."' and destination_city='".$this->destination_city."'");
		  
	  
		$reverseroute = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."mp_cities  WHERE source_city ='".$this->destination_city."' and destination_city='".$this->source_city."'");
		
		if(sizeof((array)$mainroute)>0 || sizeof((array)$reverseroute)>0){			
			 return false;
		}else{
			 return true;
		}
    }
		
	
	/* Get Destination City of route csv */
    function get_route_destination_cities()
    {
        global $wpdb;
				
        $destination_routes = $wpdb->get_results("SELECT `source_city`,`destination_city` FROM ".$wpdb->prefix."mp_cities  WHERE source_city ='".$this->source_city."' OR destination_city='".$this->source_city."'");

		
		return $destination_routes;
    }
}
?>