<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

include(dirname(__FILE__).'/header.php');
$plugin_url_for_ajax = plugins_url('',  dirname(__FILE__));
?>

<script type="text/javascript">
    var calenderObj = {"plugin_path":"<?php echo $plugin_url_for_ajax;?>","ak_wp_lang":"<?php echo $ak_wplang[0]; ?>",'cal_first_day':"<?php echo get_option('start_of_week'); ?>",
	'time_format':"<?php echo $wpTimeFormat; ?>"};
</script>
	
<div id="mp-calendar-all">
	<div class="panel-body">
	<div class="ct-legends-main">
        <div class="ct-legends-inner col-md-12">
            <h4><?php echo __("Legends","mp");?>:</h4>
            <ul class="list-inline">
                <li><i class="fa fa-check txt-success icon-space"></i><?php echo __("Completed","mp");?></li>             
                <li><i class="fa fa-info-circle txt-warning icon-space"></i><?php echo __("Pending","mp");?></li>
           </ul>
        </div>
    </div>
	<hr id="hr" />
	<div class="mp-calendar-top-bar">
		<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 mb-10">
			<label class="custom-width"><?php echo __("Select Option To Show Leads","mp");?></label>
			<div id="mp_reportrange" class="form-control custom-width" >
				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				<span></span> <i class="fa fa-caret-down"></i>
			</div>
			<input type="hidden" id="mp_booking_startdate" value="" />
			<input type="hidden" id="mp_booking_enddate" value="" />
		</div>
			
		<div class="col-md-2 col-sm-6 col-xs-12 col-lg-2 pull-right mb-10">
			<button type="button" id="mp_filter_appointments" class="form-group btn btn-info mp-btn-width mp-submit-btn mt-20" name=""><?php echo __("Submit","mp");?></button>
		</div>
	</div>
	
	</div>

	<div id="mp_calendar" class=""></div> 		
	</div>
<?php 
	include(dirname(__FILE__).'/footer.php');
?>
